// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
const url_server = 'https://192.168.1.2:8811/';
// const url_server = 'https://sihweb.sihoteles.com:8811/';
// const url_php = 'https://sihpdf.192.168.1.2.xip.io';
const url_php = 'https://sihweb.sihoteles.com'
export const environment = {
  production: true,
  ipPhp: url_php,
  apiUrl: url_server + 'front/rest',
  ipServer: url_server + 'front/static/index.html',
  ipLogin: url_server + 'front/static/pms',
  ipLogout: url_server + 'front/static/auth/j_spring_security_logout',
  ipLoginProgress: url_server + 'front/static/auth/j_spring_security_check',
  /*urlMenu: 'menuUsr',*/
  urlMenu: 'menuUsr',
};
