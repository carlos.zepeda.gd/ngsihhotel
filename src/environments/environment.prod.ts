const url_server = 'https://sihweb.sihoteles.com:8811/';
export const environment = {
  production: true,
  ipPhp: 'https://sihweb.sihoteles.com',
  apiUrl:   url_server + 'front/rest',
  ipServer: url_server + 'front/static/index.html',
  ipLogin:  url_server + 'front/static/auth/login.html',
  ipLogout: url_server + 'front/static/auth/j_spring_security_logout',
  ipLoginProgress: '/front/static/auth/j_spring_security_check',
  urlMenu:  'menuUsr',
};
