// Imports Angular
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
// Servicios
import {ManttoUsuariosService} from '../pms/manttos/mantto-usuarios/mantto-usuarios.service';
// Rutas
import {POS_ROUTING} from './pos.routes';
// Modules
import {ReservacionesModule} from '../pms/reservaciones/reservaciones.module';
import { HuespedModule } from 'app/pms/recepcion/huesped/huesped.module';
import {CheckOutModule} from 'app/pms/caja/check-out/checkout.module';
import { ConsultasModule } from 'app/pms/consultas/consultas.module';
import {AmallavesModule} from '../pms/ama-llaves/amallaves.module';
import {ChequeModule} from './puntos-venta/cheque/cheque.module';
import {ReportesPosModule} from './reportes/reportespos.module';
import {ArchitectModule} from '../_architect/architect.module';
import {SharedAppsModule} from '../shared/shared-apps.module';
import {ManttosPosModule} from './manttos/manttospos.module';
import {ManttosModule} from '../pms/manttos/manttos.module';
import {KendouiModule} from '../shared/kendo.module';

// Componentes
import {CambiosChequesComponent} from './puntos-venta/cambios-cheques/cambios-cheques.component';
import {HomePosComponent} from './components/home/home-pos.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        KendouiModule,
        POS_ROUTING,
        ChequeModule,
        ReportesPosModule,
        ManttosPosModule,
        ManttosModule,
        SharedAppsModule,
        CheckOutModule,
        AmallavesModule,
        RouterModule,
        ArchitectModule,
        ReservacionesModule,
        ConsultasModule,
        HuespedModule
    ],
    declarations: [
        HomePosComponent,
        CambiosChequesComponent,
    ],
    exports: [
        CambiosChequesComponent,
    ],
    providers: [
        ManttoUsuariosService
    ],
})
export class PosModule {
}
