import { RouterModule, Routes } from '@angular/router';
import { ReporteTurnoComponent } from './reporte-turno/reporte-turno.component';
import { ReporteComandasComponent } from './reporte-comandas/reporte-comandas.component';
import { ReportePropinasComponent } from './reporte-propinas/reporte-propinas.component';
import { ReporteVentasComponent } from './reporte-ventas/reporte-ventas.component';
import { ReporteMeserosComponent } from './reporte-meseros/reporte-meseros.component';
import { ReportePlatillosComponent } from './reporte-platillos/reporte-platillos.component';
import { BaseLayoutComponent } from '../../_architect/Layout/base-layout/base-layout.component';
import { VerifyInfociaGuardService } from '../../services/verify-infocia-guard.service';
import { RoutesGuard } from '../../services/routes-guard.service';

const REPORTS_POS_ROUTES: Routes = [
    {
        path: '',
        component: BaseLayoutComponent,
        children: [
            { path: 'reporte-turno', component: ReporteTurnoComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'reporte-propinas', component: ReportePropinasComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'reporte-comandas', component: ReporteComandasComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'reporte-ventas', component: ReporteVentasComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'reporte-meseros', component: ReporteMeserosComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'reporte-platillos', component: ReportePlatillosComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } }
        ],
        canActivateChild: [
            VerifyInfociaGuardService
        ],
    }
];

export const REPORTS_POS_ROUTING = RouterModule.forChild(REPORTS_POS_ROUTES);
