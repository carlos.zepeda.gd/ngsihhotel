// Librerias Angular
import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
// Librerias Kendo
import {DataStateChangeEvent} from '@progress/kendo-angular-grid';
import {
  SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor,
  GroupDescriptor, process, aggregateBy, State
} from '@progress/kendo-data-query';
// Servicios
import {ReportesPosService} from '../reportes-pos.service';
import {ChequeAyudaService} from '../../puntos-venta/cheque/services/cheque-ayuda.service';
import {InfociaGlobalService} from '../../../services/infocia.service';
import {ManttoUsuariosService} from '../../../pms/manttos/mantto-usuarios/mantto-usuarios.service';
// Modelo
import {Tgeneral} from '../../../models/tbl-general';
// Librerias
import moment from 'moment';
import XLSX from 'xlsx';
import {ManttoMenuService} from '../../../pms/manttos/mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import { GLOBAL } from 'app/services/global';
const swal = GLOBAL.swal;

declare var jsPDF: any;
// Esta función se ejecuta cuando en la grid el parametro [filterable] es igual a true

@Component({
  selector: 'app-reporte-meseros',
  templateUrl: './reporte-meseros.component.html',
  /*styleUrls: ['./reporte-meseros.component.css']*/
})
export class ReporteMeserosComponent implements OnInit {
  heading = 'Reporte de Meseros';
  subheading = 'Point of Sales System.';
  icon = 'icon-gradient bg-tempting-azure';

  public formSend: Tgeneral;
  public formSearch: FormGroup
  // Variables para vincular con componentes de Kendo
  public dataGrid: any = [];
  public dataComplete: any = [];
  public dsUsuarios: any = [];
  public dsResumen: any = [];
  public dsClases: any = [];
  public dsPuntos: any = [];
  public dsTurnos: any = [];
  public dsCodigos: any = [];
  // Variables para obtener información del usuario y del hotel
  public ttUsuario: any;
  public ttInfocia: any;
  // Variables para funciones de Kendo Grid
  public filter: CompositeFilterDescriptor;
  public groups: GroupDescriptor[] = [];
  public sort: SortDescriptor[] = [{field: 'tcheq', dir: 'asc'}]; // Order asc o desc
  public aggregates: any[] = [ // Totalizar columnas de la Grid
    {field: 'tmonto', aggregate: 'sum'},
    {field: 'tprop', aggregate: 'sum'},
    {field: 'tserv', aggregate: 'sum'},
    {field: 'tdesc', aggregate: 'sum'}
  ];
  // Agrupar por un campo en especifico
  public state: State = {group: [{field: 'tpunto', aggregates: this.aggregates}]};
  // Data en codigo duro

  public dsEstatusCheque: any[] = [
    {field: 'Abierto', value: 'A'},
    {field: 'Impreso', value: 'I'},
    {field: 'Cerrado', value: 'C'},
    {field: 'Cancelado', value: 'X'}
  ];
  // Banderas para ocultar o mostrar
  public flagChart: boolean;
  public flagViewDetail: boolean;
  public total: any;
  public loading: boolean;

  constructor(private _reporte: ReportesPosService,
              private _info: InfociaGlobalService,
              private _cheque: ChequeAyudaService,
              private _user: ManttoUsuariosService,
              private fb: FormBuilder,
              private _menuServ: ManttoMenuService,
              private _router: Router) {
  }

  ngOnInit() {
    this._user.getUsuarios().subscribe(data => {
      this.dsUsuarios = data;
      // Concatena Nombre y Apellido de toos los usuarios dsUsuarios
      this.dsUsuarios.forEach(user => user.cUserNomb = user.cUserNomb + ' ' + user.cUserApel);
    });
    this._cheque.getClases().subscribe(data => this.dsClases = data);
    this._cheque.getPuntos().subscribe(data => this.dsPuntos = data);
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.createForm(); // Construcción de Reactive Form

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  public getReporte() {
    this.loading = true;
    const val = this.formSearch.value;
    const joinEstatus = [];
    for (const item of val.estatus) {
      joinEstatus.push(item.value);
    }
    let reporte = 1;
    if (!val.rturno) {
      reporte = 2;
    }
    const users = val.usuario.join(',');
    const estatus = joinEstatus.join(',');

    const date1 = moment(val.fechaIni).format(GLOBAL.formatIso);
    const date2 = moment(val.fechaFin).format(GLOBAL.formatIso);

    this.formSend = new Tgeneral(
      '', '', '', '', '', '', '', '', '', '', '',
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      date1, date2, null, null, null, null, null, null, null, null, null,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no',
      null, null, null, null, null, null, null, null, null, null, null
    );
    this._reporte.getReporteMeseros(this.formSend).subscribe(data => {
      if (data.ttTmpmov) {
        this.dataGrid = data.ttTmpmov;
        this.dataComplete = data.ttTmpmov;
        this.dsCodigos = data.ttTcod;
        this.dataGrid = {
          data: orderBy(this.dataGrid, this.sort),
          total: this.dataGrid.length
        };
        this.total = aggregateBy(this.dataComplete, this.aggregates);
      } else {
        this.dataGrid = [];
        this.dataComplete = [];
        this.dsResumen = [];
        swal('Error!', 'No se encontraron registros!!', 'error');
      }
      this.loading = false;
    });
  }

  createForm() {
    this.formSearch = this.fb.group({
      turno: [null],
      fechaIni: [moment(this.ttInfocia.fday).toDate(), Validators.required],
      fechaFin: [moment(this.ttInfocia.fday).toDate(), Validators.required],
      punto: [''],
      clase: [''],
      rturno: [true, Validators.required],
      usuario: [[]],
      estatus: [this.dsEstatusCheque, Validators.required]
    });
    // Cuando se realizan cambios en el formularios se ejecuta esta función
    this.onChanges();
  }

  onChanges() {
    this.formSearch.valueChanges.subscribe(change => {
      if (change.punto) {
        this._cheque.getTurnos(change.punto).subscribe(data => this.dsTurnos = data);
      }
    });
  }

  viewDetail() {
    this.flagViewDetail = true;
  }

  generateChart() {
    this.flagChart = true;
  }

  exportExcel() {
    const tbl = document.getElementById('tbl1');
    const wb = XLSX.utils.table_to_book(tbl);
    XLSX.writeFile(wb, 'Reporte-Meseros.xlsx');
  }

  exportPDF() {
    const d = new Date();
    const hotel = this.ttInfocia.cnombre;
    const fecha = this.ttInfocia.fday;
    const operador = this.ttUsuario.cUserId;
    let restaurante: any;
    this.dsPuntos.forEach(punto => {
      if (this.formSearch.value.punto = punto.Punto) {
        restaurante = punto;
      }
    });
    const header1 = 'Reporte de Meseros ' + restaurante.pdesc + ' (' + fecha + ')';
    const header2 = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() + ' Ope: ' + operador;
    const doc = new jsPDF('p', 'pt', 'A4');

    const res = doc.autoTableHtmlToJson(document.getElementById('tbl1'));
    doc.page = 1;
    doc.autoTable(res.columns, res.data, {
      addPageContent: function (data) {
        doc.setFontSize(8);
        doc.text(hotel, 45, 20);
        doc.text(header1, 45, 35);
        doc.text(header2, 345, 35);
        doc.setFontType('normal');
        doc.text(345, 20, 'Hoja ' + doc.page);
        doc.setTextColor(100);
        doc.page++;
      },
      styles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 7,
        font: 'helvetica', // helvetica, times, courier
        lineColor: 200,
        lineWidth: 0,
        fontStyle: 'normal', // normal, bold, italic, bolditalic
        overflow: 'ellipsize', // visible, hidden, ellipsize or linebreak
        fillColor: false, // false for transparent or a color as described below
        textColor: 20,
        halign: 'left', // left, center, right
        valign: 'middle', // top, middle, bottom
        columnWidth: 'auto' // 'auto', 'wrap' or a number
      }
    });
    doc.save('Reporte de Meseros.pdf');
    // this.getReporte();
  }

  // Funciones utilizadas por componentes de Kendo

  // Cierra ventana en Kendo Window
  public close(component) {
    this[component + 'Opened'] = false;
    this.flagChart = false;
    this.flagViewDetail = false;
  }

  // Procesa la grid a mostrar cuando se agrupa por un campo. Grid debe tener Groupable = true
  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.dataGrid = process(this.dataComplete, {group: this.groups});
  }

  // Procesa la grid cuando se filtra en un campo. Grid debe tener Filterable = true
  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  // Procesa la grid en asc o desc en el campo seleccionado
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    if (state && state.group) {
      state.group.map(group => group.aggregates = this.aggregates);
    }
    this.state = state;
    this.dataGrid = process(this.dataComplete, this.state);
  }

}
