import {Component, OnInit} from '@angular/core';
import {
  SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor,
  GroupDescriptor, process, aggregateBy, State
} from '@progress/kendo-data-query';
import {ChequeAyudaService} from '../../puntos-venta/cheque/services/cheque-ayuda.service';
import {ReportesPosService} from '../reportes-pos.service';
import {InfociaGlobalService} from '../../../services/infocia.service';
import {DataStateChangeEvent} from '@progress/kendo-angular-grid';
import {ManttoMenuService} from '../../../pms/manttos/mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import swal from 'sweetalert2';
import XLSX from 'xlsx';

declare var $: any;
declare var jsPDF: any;

@Component({
  selector: 'app-reporte-propinas',
  templateUrl: './reporte-propinas.component.html',
  /*styleUrls: ['./reporte-propinas.component.css']*/
})
export class ReportePropinasComponent implements OnInit {
  heading = 'Reporte de Propinas';
  subheading = 'Point of Sales System.';
  icon = 'icon-gradient bg-tempting-azure';

  public dataGrid: any = [];
  public dataComplete: any = [];
  public dsMeseros: any = [];
  public dsClases: any = [];
  public dsPuntos: any = [];
  public dsTurnos: any = [];
  public ttUsuario: any;
  public ttInfocia: any;
  public filter: CompositeFilterDescriptor;
  public groups: GroupDescriptor[] = [];
  public sort: SortDescriptor[] = [{field: 'cedo', dir: 'asc'}];
  public meseroSelected: any = [];
  public claseSelected: any = [];
  public puntoSelected: any = [];
  public turnoSelected: number;
  public aggregates: any[] = [{field: 'tprop', aggregate: 'sum'}];
  public state: State = {group: [{field: 'mesero', aggregates: this.aggregates}]};
  public total: any;
  public flagChart: boolean;
  public flagPdf: boolean;
  public loading: boolean;
  flagExportExcel: boolean;

  tTsub = [];
  grades = [];

  constructor(private _reporte: ReportesPosService,
              private _info: InfociaGlobalService,
              private _cheque: ChequeAyudaService,
              private _menuServ: ManttoMenuService,
              private _router: Router) {
  }

  ngOnInit() {
    this._cheque.getMeseros().subscribe(data => this.dsMeseros = data);
    this._cheque.getClases().subscribe(data => this.dsClases = data);
    this._cheque.getPuntos().subscribe(data => this.dsPuntos = data);
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    // this._cheque.getTurnos().subscribe(data => this.dsTurnos = data );

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  public getReporte() {
    const value = {dsRepProp: {tTmes: this.convertObject()}};
    this._reporte.getReportePropinas(value, this.turnoSelected).subscribe(data => {
      if (data !== undefined) {

        for (let cont = 0; cont < data.length; cont++) {
          this.tTsub.push({mesero: data[cont].mesero, monto: data[cont].tTsub[0].tmonto})
        }
        let grades = [];
        this.tTsub.forEach(function (item) {
          let grade = grades[item.mesero] = grades[item.mesero] || {};
          grade['mesero'] = item.mesero;
          grade['monto'] = item.monto;
        });
        let result = Object.keys(grades).map(function (key, index) {
          return grades[key];
        });
        this.grades = result;

        if (data[0].cMensTxt !== undefined) {
          swal('Error!', data[0].cMensTxt, 'error');
          return;
        }
        this.dataGrid = data;
        this.dataComplete = data;
        this.dataGrid = {
          data: orderBy(this.dataGrid, this.sort),
          total: this.dataGrid.length
        };
        this.total = aggregateBy(this.dataComplete, this.aggregates);
      } else {
        swal('No hay movimientos!!', '', 'warning');
      }
    });

  }

  public close(component) {
    this[component + 'Opened'] = false;
    this.flagChart = false;
  }

  printTicket() {
    const infoTicket = [];
    infoTicket.push({hotel: this.ttInfocia.cnombre});
    infoTicket.push({user: this.ttUsuario.cUserId});
    infoTicket.push({hora: this.ttInfocia.chora});
    infoTicket.push({fecha: this.ttInfocia.fday});
    infoTicket.push({fecha: this.ttInfocia.clogo});
    const json = {propina: this.dataGrid.data, info: infoTicket};
    this._reporte.putPrintTicket(json).subscribe();
  }

  convertObject() {
    const array = [];
    for (const i of this.meseroSelected) {
      array.push({tipo: 'M', valor: i})
    }
    for (const i of this.claseSelected) {
      array.push({tipo: 'C', valor: i})
    }
    for (const i of this.puntoSelected) {
      array.push({tipo: 'P', valor: i})
    }
    return array;
  }

  exportPDF() {
    this.flagPdf = true;
    const d = new Date();
    const hotel = this.ttInfocia.cnombre;
    const fecha = this.ttInfocia.fday;
    const operador = this.ttUsuario.cUserId;

    const header1 = 'Reporte de Propinas ' + hotel + ' Ope: ' + operador;
    const header2 = 'Hora: ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() + ' Fecha: ' + fecha + '';
    const doc = new jsPDF('p', 'pt', 'A4');
    const res = doc.autoTableHtmlToJson(document.getElementById('tbl1'), {sheet: 'Sheet JS'});
    doc.page = 1;
    doc.autoTable(res.columns, res.data, {
      addPageContent: function (data) {
        doc.setFontSize(8);
        doc.text(hotel, 45, 20);
        doc.text(header1, 45, 35);
        doc.text(header2, 345, 35);
        doc.setFontType('normal');
        doc.text(345, 20, 'Hoja ' + doc.page);
        doc.setTextColor(100);
        doc.page++;
      },
      styles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 7,
        font: 'helvetica', // helvetica, times, courier
        lineColor: 200,
        lineWidth: 0,
        fontStyle: 'normal', // normal, bold, italic, bolditalic
        overflow: 'ellipsize', // visible, hidden, ellipsize or linebreak
        fillColor: false, // false for transparent or a color as described below
        textColor: 20,
        halign: 'left', // left, center, right
        valign: 'middle', // top, middle, bottom
        columnWidth: 'auto' // 'auto', 'wrap' or a number
      }
    });
    doc.save('Reporte de Propinas.pdf');
    this.flagPdf = false;
    // this.getReporte();
  }

  exportExcel() {
    this.flagExportExcel = true;
    const tbl = document.getElementById('tbl1');
    const wb = XLSX.utils.table_to_book(tbl);
    XLSX.writeFile(wb, 'Reporte-Propinas.xlsx');
    setTimeout(() => {
      this.flagExportExcel = false;
    }, 1000);
  }

  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.loadProducts();
  }

  private loadProducts(): void {
    this.dataGrid = process(this.dataComplete, {group: this.groups});
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataStructure();
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    if (state && state.group) {
      state.group.map(group => group.aggregates = this.aggregates);
    }

    this.state = state;

    this.dataGrid = process(this.dataComplete, this.state);
  }

  private dataStructure(): void {
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

}
