import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-chart-propinas',
  templateUrl: './chart-propinas.component.html',
  styleUrls: ['./chart-propinas.component.css']
})
export class ChartPropinasComponent implements OnInit, OnChanges {

  @Input() dataGrid: any = [];
  public single: any = [];
  public multi: any[];
  public view: any[] = [700, 400];
  public colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor() {
  }
  ngOnInit() {
  }

  changeMoney(value) {
    return '$' + value + '.00'; 
  }
  ngOnChanges(change: SimpleChanges) {
    if (change.dataGrid !== undefined) {
      if (change.dataGrid.currentValue) {
        const allPuntos = this.dataGrid.data.map(item => item.tpunto).filter((value, index, self) => self.indexOf(value) === index);
        let total = 0;
        for (const punto of allPuntos) {
          for (const i in this.dataGrid.data) {
            if (punto === this.dataGrid.data[i].tpunto) {
               total += this.dataGrid.data[i].tmonto;
              }
            }
            this.single.push({ name: punto, value: total });
            total = 0;
          }
          Object.assign(this.single);
      }
    }
  }
  onSelect(event) {
  }
}
