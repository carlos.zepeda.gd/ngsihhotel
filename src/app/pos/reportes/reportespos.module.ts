// Angular Modules
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {NgxChartsModule} from '@swimlane/ngx-charts';
// Apps Modules
import {KendouiModule} from '../../shared/kendo.module';
import {SharedAppsModule} from '../../shared/shared-apps.module';
// Servicios
import {ReportesPosService} from './reportes-pos.service';
// Components Module
import {ReportePropinasComponent} from './reporte-propinas/reporte-propinas.component';
import {ReporteTurnoComponent} from './reporte-turno/reporte-turno.component';
import {ReporteComandasComponent} from './reporte-comandas/reporte-comandas.component';
import {ReporteMeserosComponent} from './reporte-meseros/reporte-meseros.component';
import {ReportePlatillosComponent} from './reporte-platillos/reporte-platillos.component';
import {ReporteVentasComponent} from './reporte-ventas/reporte-ventas.component';
import {ChartPropinasComponent} from './reporte-propinas/chart-propinas/chart-propinas.component';
import {ChartComandasComponent} from './reporte-comandas/chart-comandas/chart-comandas.component';
import {ChartTurnosComponent} from './reporte-turno/chart-turnos/chart-turnos.component';
import {ChartMeserosComponent} from './reporte-meseros/chart-meseros/chart-meseros.component';
import {REPORTS_POS_ROUTING} from './reportespos.routes';
import {ArchitectModule} from '../../_architect/architect.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedAppsModule,
        NgxChartsModule,
        KendouiModule,
        REPORTS_POS_ROUTING,
        ArchitectModule
    ],
    declarations: [
        ReporteComandasComponent,
        ReportePropinasComponent,
        ReporteTurnoComponent,
        ReporteMeserosComponent,
        ReportePlatillosComponent,
        ReporteVentasComponent,
        ChartPropinasComponent,
        ChartComandasComponent,
        ChartTurnosComponent,
        ChartMeserosComponent
    ],
    providers: [
        ReportesPosService
    ]
})
export class ReportesPosModule {
}
