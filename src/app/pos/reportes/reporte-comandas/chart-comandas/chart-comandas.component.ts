import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-chart-comandas',
  templateUrl: './chart-comandas.component.html',
  styleUrls: ['./chart-comandas.component.css']
})
export class ChartComandasComponent implements  OnChanges {
  @Input() dataGrid: any = [];
  public single: any = []; // Objeto que va a leer la grafica
  public view: any[] = [700, 400]; // Tamaño de la grafica
  public colorScheme = { // Colores de la grafica
    domain: ['#FF7F50', '#20B2AA', '#808000', '#FF4500']
  };

  // Formatea el valor recibido por la grafica
  changeMoney(value) {
    return '$' + value + '.00'; 
  }
  // Se detectan cambios en el Input: dataGrid
  ngOnChanges(change: SimpleChanges) {
    if (change.dataGrid !== undefined) {
      if (change.dataGrid.currentValue) {
        this.generateData(this.dataGrid.data)
      }
    }
  }
  generateData(data) {
    // Función para obtener todos los registros que son diferentes
    let monto1 = 0;
    let monto2 = 0;
    let monto3 = 0
    for (const monto of data) {
      if (monto.tmont1) { monto1 += monto.tmont1; }
      if (monto.tmont2) { monto2 += monto.tmont2; }
      if (monto.tmont3) { monto3 += monto.tmont3; }
    }
    this.single = [
      { name: 'Alimentos', value: monto1 },
      { name: 'Bebidas',   value: monto2 },
      { name: 'Varios',    value: monto3 }
    ];
    // Se asigna como objeto para que la grafica lo interprete
    Object.assign(this.single);
  }
  // Obtiene la data del registro seleccionado en la grafica
  onSelect(event) {
  }
}
