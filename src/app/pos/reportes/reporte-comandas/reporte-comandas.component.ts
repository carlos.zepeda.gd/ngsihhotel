import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
// Librerias Kendo
import { DataStateChangeEvent } from '@progress/kendo-angular-grid';
import {
  SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor,
  GroupDescriptor, process, aggregateBy, State
} from '@progress/kendo-data-query';
// Servicios
import { ReportesPosService } from '../reportes-pos.service';
import { ChequeAyudaService } from '../../puntos-venta/cheque/services/cheque-ayuda.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
// Modelo
import { Tgeneral } from '../../../models/tbl-general';
// Librerias
import moment from 'moment';
import XLSX from 'xlsx';
import { ManttoMenuService } from '../../../pms/manttos/mantto-menu/mantto-menu.service';
import { Router } from '@angular/router';
import { GLOBAL } from '../../../services/global';
const swal = GLOBAL.swal;
declare var jsPDF: any;
// Esta función se ejecuta cuando en la grid el parametro [filterable] es igual a true

@Component({
  selector: 'app-reporte-comandas',
  templateUrl: './reporte-comandas.component.html',
  /*styleUrls: ['./reporte-comandas.component.css']*/
})
export class ReporteComandasComponent implements OnInit {
  heading = 'Reporte de Comandas';
  subheading = 'Point of Sales System.';
  icon = 'icon-gradient bg-tempting-azure';

  public formSend: Tgeneral;
  public formSearch: FormGroup;
  public dsClases: any = [];
  public dsPuntos: any = [];
  public dsResumen: any = [];
  public dsMeseros: any = [];
  // Variables para vincular con componentes de Kendo
  public dataGrid: any = [];
  public dataComplete: any = [];
  // Variables para obtener información del usuario y del hotel
  public ttUsuario: any = [];
  public ttInfocia: any = [];
  // Variables para funciones de Kendo Grid
  public filter: CompositeFilterDescriptor;
  public groups: GroupDescriptor[] = [];
  public sort: SortDescriptor[] = [{ field: 'tcheq', dir: 'asc' }]; // Order asc o desc
  public sort2: SortDescriptor[] = [{ field: 'tcheq', dir: 'asc' }]; // Order asc o desc
  public aggregates: any[] = [ // Totalizar columnas de la Grid
    { field: 'tmont1', aggregate: 'sum' },
    { field: 'tmont2', aggregate: 'sum' },
    { field: 'tmont3', aggregate: 'sum' }
  ];
  // Agrupar por un campo en especifico
  public state: State = { group: [{ field: 'tpunto', aggregates: this.aggregates }] };
  // Banderas para ocultar o mostrar
  public flagChart: boolean;
  public flagForm: boolean = true;
  public flagViewDetail: boolean;
  public total: any;
  public windowLeft: 0;
  public groupable: boolean = true;
  // Data en codigo duro
  public dsTipoReporte: any[] = [
    { field: 'Punto de Venta', value: true },
    { field: 'Mesero', value: false }
  ];
  dsTotalPv: any;
  public loading: boolean;

  constructor(private _reporte: ReportesPosService,
    private _info: InfociaGlobalService,
    private _cheque: ChequeAyudaService,
    private fb: FormBuilder,
    private _menuServ: ManttoMenuService,
    private _router: Router) {
  }

  ngOnInit() {
    this._cheque.getClases().subscribe(data => this.dsClases = data);
    this._cheque.getPuntos().subscribe(data => this.dsPuntos = data);
    this._cheque.getMeseros().subscribe(data => this.dsMeseros = data);
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.buildFormGroup(); // Construcción de Reactive Form

    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  public getReporte() {
    this.loading = true;
    const val = this.formSearch.value;
    let reporte = 1;
    let codigo = val.punto;
    if (!val.tipo) {
      codigo = val.mesero;
      reporte = 2;
    }
    const date1 = moment(val.fechaIni).format(GLOBAL.formatIso);
    const date2 = moment(val.fechaFin).format(GLOBAL.formatIso);

    this.formSend = new Tgeneral(
      codigo, val.clase, '', '', '', '', '', '', '', '', '',
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      date1, date2, null, null, null, null, null, null, null, null, null,
      reporte, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      val.descuentos, val.borrados, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no',
      null, null, null, null, null, null, null, null, null, null, null
    );
    this._reporte.putReporteComandas(this.formSend).subscribe(data => {
      if (data.ttTcom) {
        this.dataGrid = data.ttTdet;
        this.dataComplete = data.ttTdet;
        this.dsResumen = data.ttTcom;
        this.dsTotalPv = data.ttTpv;
        this.dataGrid = {
          data: orderBy(this.dataGrid, this.sort),
          total: this.dataGrid.length
        };
        this.dsResumen = {
          data: orderBy(this.dsResumen, this.sort2),
          total: this.dsResumen.length
        };
        this.total = aggregateBy(this.dataComplete, this.aggregates);
        this.flagForm = false;
      } else {
        this.dataGrid = [];
        this.dataComplete = [];
        this.dsResumen = [];
        swal('Error!', 'No se encontraron registros!!', 'error');
      }
      this.loading = false;
    });
  }

  buildFormGroup() {
    const defaultDate: any = moment(this.ttInfocia.fday).add(3, 'days');
    this.formSearch = this.fb.group({
      fechaIni: [this._info.toDate(defaultDate._i), Validators.required],
      fechaFin: [this._info.toDate(defaultDate._i), Validators.required],
      tipo: [true, Validators.required],
      mesero: [''],
      punto: [''],
      clase: [''],
      descuentos: [false],
      detalle: [false],
      borrados: [false]
    });
    // Cuando se realizan cambios en el formularios se ejecuta esta función
    this.onChanges();
  }

  onChanges() {
    // this.formSearch.valueChanges.subscribe();
  }

  viewDetail() {
    this.flagViewDetail = true;
  }

  generateChart() {
    this.flagChart = true;
    this.groupable = false;
  }

  exportExcel() {
    const tbl = document.getElementById('tblExcel');
    const wb = XLSX.utils.table_to_book(tbl);
    XLSX.writeFile(wb, 'Reporte-Comandas.xlsx');
  }

  exportPDF() {
    const d = new Date();
    const hotel = this.ttInfocia.cnombre;
    const fecha = this.ttInfocia.fday;
    const operador = this.ttUsuario.cUserId;
    let restaurante: any;
    this.dsPuntos.forEach(punto => {
      if (this.formSearch.value.punto = punto.Punto) {
        restaurante = punto;
      }
    });
    const header1 = 'Reporte de Comandas ' + restaurante.pdesc + ' (' + fecha + ')';
    const header2 = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() + ' Ope: ' + operador;
    const doc = new jsPDF('l', 'pt', 'A4');
    const res = doc.autoTableHtmlToJson(document.getElementById('tbl1'));
    doc.page = 1;
    doc.autoTable(res.columns, res.data, {
      addPageContent: function (data) {
        doc.setFontSize(8);
        doc.text(hotel, 45, 20);
        doc.text(header1, 45, 35);
        doc.text(header2, 345, 35);
        doc.setFontType('normal');
        doc.text(345, 20, 'Hoja ' + doc.page);
        doc.setTextColor(100);
        doc.page++;
      },
      styles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 7,
        font: 'helvetica', // helvetica, times, courier
        lineColor: 200,
        lineWidth: 0,
        fontStyle: 'normal', // normal, bold, italic, bolditalic
        overflow: 'ellipsize', // visible, hidden, ellipsize or linebreak
        fillColor: false, // false for transparent or a color as described below
        textColor: 20,
        halign: 'left', // left, center, right
        valign: 'middle', // top, middle, bottom
        columnWidth: 'auto' // 'auto', 'wrap' or a number
      }
    });
    const res2 = doc.autoTableHtmlToJson(document.getElementById('tbl2'));
    doc.autoTable(res2.columns, res2.data, {
      startY: doc.autoTableEndPosY() + 20,
      styles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 7,
        font: 'helvetica', // helvetica, times, courier
        lineColor: 200,
        lineWidth: 0,
        fontStyle: 'normal', // normal, bold, italic, bolditalic
        overflow: 'ellipsize', // visible, hidden, ellipsize or linebreak
        fillColor: false, // false for transparent or a color as described below
        textColor: 20,
        halign: 'left', // left, center, right
        valign: 'middle', // top, middle, bottom
        columnWidth: 'auto' // 'auto', 'wrap' or a number
      }
    });
    doc.save('Reporte de Comandas.pdf');
    // this.getReporte();
  }

  // Cierra ventana en Kendo Window
  public close(component) {
    this[component + 'Opened'] = false;
    this.flagChart = false;
    this.flagViewDetail = false;
  }

  // Procesa la grid a mostrar cuando se agrupa por un campo. Grid debe tener Groupable = true
  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.dataGrid = process(this.dataComplete, { group: this.groups });
  }

  // Procesa la grid cuando se filtra en un campo. Grid debe tener Filterable = true
  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  // Procesa la grid en asc o desc en el campo seleccionado
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

  // Procesa la grid en asc o desc en el campo seleccionado
  public sortChange2(sort: SortDescriptor[]): void {
    this.sort2 = sort;
    this.dsResumen = {
      data: orderBy(this.dsResumen.data, this.sort2),
      total: this.dsResumen.data.length
    };
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    if (state && state.group) {
      state.group.map(group => group.aggregates = this.aggregates);
    }
    this.state = state;
    this.dataGrid = process(this.dataComplete, this.state);
  }
}
