import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, HttpModule} from '@angular/http';
import {GLOBAL} from '../../services/global';
import 'rxjs/add/operator/map';

const headers = new Headers({'Content-Type': 'application/json'});
const esp = GLOBAL.char174;

@Injectable()
export class ReportesPosService {

  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  getReporteTurno(data) {
    const json = JSON.stringify(data);
    const params = '{"dsTgeneral":{"ttTgeneral":[' + json + ']}}';

    return this.http.put(this.url + '/pos/reportes/turno', params, {headers: headers})
      .map(res => {
        const r = res.json().response.siReporte;
        if (r.dsMensajes.ttMensError !== undefined) {
          return r.dsMensajes.ttMensError;
        }
        if (r.dsReportes === {}) {
          return [];
        } else {
          return r.dsReportes;
        }

      });
  }

  getReportePropinas(data, turno) {
    const json = JSON.stringify(data);
    const params = '{"dsRepProp":{"tTmov":[' + json + ']}}';

    return this.http.put(this.url + '/pos/reportes/propina/' + turno, json, {headers: headers})
      .map(res => {
        const success = res.json().response.siPropina.dsPropina.tTmov;
        const error = res.json().response.siPropina.dsMensajes.ttMensError;
        if (error) {
          return error;
        } else {
          return success;
        }
      });
  }

  getCambiosCheques(clase, punto, cheque) {
    return this.http.get(this.url + '/pos/ayuda/cambiosch' + esp + clase + esp + punto + esp + cheque)
      .map(res => res.json().response.siPosayuda.dsPosayuda);
  }

  putReporteComandas(data) {
    const json = JSON.stringify(data);
    const params = '{"dsTgeneral":{"ttTgeneral":[' + json + ']}}';

    return this.http.put(this.url + '/pos/reportes/comanda', params, {headers: headers})
      .map(res => {
        const success = res.json().response.siReporte.dsReportes;
        if (success === {}) {
          return [];
        } else {
          return success;
        }

      });
  }

  getReporteMeseros(data) {
    const json = JSON.stringify(data);
    const params = '{"dsTgeneral":{"ttTgeneral":[' + json + ']}}';

    return this.http.put(this.url + '/pos/reportes/meseros', params, {headers: headers})
      .map(res => {
        const success = res.json().response.siReporte.dsReportes;
        if (success === {}) {
          return [];
        } else {
          return success;
        }
      });
  }

  getReporteVentas(data) {
    const json = JSON.stringify(data);
    const params = '{"dsTgeneral":{"ttTgeneral":[' + json + ']}}';

    return this.http.put(this.url + '/pos/reportes/ventas', params, {headers: headers})
      .map(res => {
        const success = res.json().response.siReporte.dsReportes;
        if (success === {}) {
          return [];
        } else {
          return success;
        }
      });
  }

  putReportePlatillos(data) {
    const json = JSON.stringify(data);
    const params = '{"dsTgeneral":{"ttTgeneral":[' + json + ']}}';

    return this.http.put(this.url + '/pos/reportes/platillos', params, {headers: headers})
      .map(res => {
        const success = res.json().response.siReporte.dsReportes;
        if (success === {}) {
          return [];
        } else {
          return success;
        }
      });
  }

  putPrintTicket(data) {
    const json = JSON.stringify(data);
    return this.http.post('http://192.168.1.100:4300/pos/v1/ticket/propinas', json, {headers: headers})
      .map(res => res.json());
  }

  postReporteTurnoPdf(data) {
    const json = JSON.stringify(data);
    return this.http.post('http://192.168.1.100:4300/reports/pdf/turno', json, {headers: headers})
      .map(res => res.json());
  }

  postEstado(data) {
    const json = JSON.stringify(data);
    const params = '{"dsEstados":{"ttEstados":[' + json + ']}}';

    return this.http.post(this.url + '/manttos/estados', params, {headers: headers})
      .map(res => {
        const succes = res.json().response.siEstado.dsEstados.ttEstados;
        const error = res.json().response.siEstado.dsMensajes.ttMensError;
        if (error) {
          return error;
        } else {
          return succes;
        }
      });
  }

  putEstado(data) {
    const json = JSON.stringify(data);
    const params = '{"dsEstados":{"ttEstados":[' + json + ']}}';

    return this.http.put(this.url + '/manttos/estados', params, {headers: headers})
      .map(res => res.json().response.siEstado.dsEstados.ttEstados);
  }

  deleteEstado(data) {
    const json = JSON.stringify(data);
    const params = '{"ttEstados":[' + json + ']}';

    return this.http.delete(this.url + '/manttos/estados',
      new RequestOptions({headers: headers, body: params}))
      .map(res => res.json().response.siEstado.dsEstados.ttEstados);
  }

  postFunction(data) {
    const json = JSON.stringify(data);
    this.http.get('url').map(res => res.json());
    this.http.post('url', json, {headers: headers}).map(res => res.json());
    this.http.delete('url', new RequestOptions({headers: headers, body: data})).map(res => res.json());
  }
}
