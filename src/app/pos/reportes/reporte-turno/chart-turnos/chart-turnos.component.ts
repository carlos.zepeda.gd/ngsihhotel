import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-chart-turnos',
  templateUrl: './chart-turnos.component.html',
  styleUrls: ['./chart-turnos.component.css']
})
export class ChartTurnosComponent implements  OnChanges {
  @Input() dataGrid: any = [];
  public single: any = []; // Objeto que va a leer la grafica
  public view: any[] = [700, 400]; // Tamaño de la grafica
  public colorScheme = { // Colores de la grafica
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // Formatea el valor recibido por la grafica
  changeMoney(value) {
    return '$' + value + '.00'; 
  }
  // Se detectan cambios en el Input: dataGrid
  ngOnChanges(change: SimpleChanges) {
    if (change.dataGrid !== undefined) {
      if (change.dataGrid.currentValue) {
        this.generateData(this.dataGrid.data)
      }
    }
  }
  generateData(data) {
    // Función para obtener todos los registros que son diferentes
    const diferent = data.map(item => item.tpunto).filter((value, index, self) => self.indexOf(value) === index);
    let total = 0;
    // Se recorren todos los reigstros diferentes y se totalizan por el campo seleccionado.
    for (const item of diferent) {
      for (const i in data) {
        if (item === data[i].tpunto) {
           total += data[i].tmonto;
          }
        }
        this.single.push({ name: item, value: total });
        total = 0;
      }
      // Se asigna como objeto para que la grafica lo interprete
      Object.assign(this.single);
  }
  // Obtiene la data del registro seleccionado en la grafica
  onSelect(event) {
    console.log(event);
  }
}
