// Librerias Angular
import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
// Librerias Kendo
import { DataStateChangeEvent } from '@progress/kendo-angular-grid';
import {
  SortDescriptor, CompositeFilterDescriptor, GroupDescriptor, process, aggregateBy, State
} from '@progress/kendo-data-query';
// Servicios
import { ReportesPosService } from '../reportes-pos.service';
import { ChequeAyudaService } from '../../puntos-venta/cheque/services/cheque-ayuda.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { ManttoUsuariosService } from '../../../pms/manttos/mantto-usuarios/mantto-usuarios.service';
// Modelo
import { Tgeneral } from '../../../models/tbl-general';
// Librerias
import moment from 'moment';
import XLSX from 'xlsx';
import { ManttoMenuService } from '../../../pms/manttos/mantto-menu/mantto-menu.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GLOBAL } from '../../../services/global';
const swal = GLOBAL.swal;
declare var jsPDF: any;

@Component({
  selector: 'app-reporte-turno',
  templateUrl: './reporte-turno.component.html',
  /*styleUrls: ['./reporte-turno.component.css']*/
})
export class ReporteTurnoComponent implements OnInit, AfterContentChecked {
  heading = 'Reporte de Turno';
  subheading: string;
  icon: string;
  public formatKendo = localStorage.getItem('formatKendo');
  public formSend: Tgeneral;
  public formSearch: FormGroup
  // Variables para vincular con componentes de Kendo
  public dataGrid: any = [];
  public dataComplete: any = [];
  public dsUsuarios: any = [];
  public dsResumen: any = [];
  public dsClases: any = [];
  public dsPuntos: any = [];
  public dsTurnos: any = [];
  public dsCodigos: any = [];
  public dsCodigosComplete: any[];
  // Variables para obtener información del usuario y del hotel
  public ttUsuario: any;
  public ttInfocia: any;
  // Variables para funciones de Kendo Grid
  public filter: CompositeFilterDescriptor;
  public groups: GroupDescriptor[] = [];
  public sort: SortDescriptor[] = [{ field: 'tcheq', dir: 'asc' }]; // Order asc o desc
  public aggregates: any[] = [ // Totalizar columnas de la Grid
    { field: 'tmonto', aggregate: 'sum' },
    { field: 'tprop', aggregate: 'sum' },
    { field: 'tserv', aggregate: 'sum' },
    { field: 'tTotal', aggregate: 'sum' },
    { field: 'tdesc', aggregate: 'sum' }
  ];
  // Agrupar por un campo en especifico
  public state: State = { group: [{ field: 'tpunto', aggregates: this.aggregates }] };
  // Data en codigo duro
  public dsTipoReporte: any[] = [
    { field: 'Reporte de Turno', value: 1 },
    { field: 'Reporte de Turno FP', value: 2 }
  ];
  public dsEstatusCheque: any[] = [
    { field: 'Abierto', value: 'A' },
    { field: 'Impreso', value: 'I' },
    { field: 'Cerrado', value: 'C' },
    { field: 'Cancelado', value: 'X' }
  ];
  public total: any;
  resultForm: any;
  loading = false;
  norecords = GLOBAL.noRecords;
  loadingColor: any = GLOBAL.loadingColor;
  dsCuadra: any;
  toast = GLOBAL.toast;

  constructor(private _reporte: ReportesPosService,
    private _info: InfociaGlobalService,
    private _cheque: ChequeAyudaService,
    private _user: ManttoUsuariosService,
    private _fb: FormBuilder,
    private _menuServ: ManttoMenuService,
    private _modalService: NgbModal,
    private _router: Router) {
  }

  ngOnInit() {
    this._user.getUsuarios().subscribe(data => {
      this.dsUsuarios = data;
      this.dsUsuarios.forEach(user => user.cUserNomb = user.cUserNomb + ' ' + user.cUserApel);
    });
    this._cheque.getClases().subscribe(data => this.dsClases = data);
    this._cheque.getPuntos().subscribe(data => this.dsPuntos = data);
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.createForm();

    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  public getReporte() {
    this.loading = true;
    this.resultForm = this.formSearch.value;
    this.dsCodigosComplete = [];
    const joinEstatus = [];
    for (const item of this.resultForm.estatus) {
      joinEstatus.push(item.value);
    }

    const users = this.resultForm.usuario.join(',');
    const estatus = joinEstatus.join(',');
    const date1 = moment(this.resultForm.fechaIni).format(GLOBAL.formatIso);
    const date2 = moment(this.resultForm.fechaFin).format(GLOBAL.formatIso);

    this.formSend = new Tgeneral(
      this.resultForm.clase, 'T', estatus, users, '', '', '', '', '', '', '',
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      date1, date2, null, null, null, null, null, null, null, null, null,
      this.resultForm.punto, this.resultForm.turno, this.resultForm.rturno, 0, 0, 0, 0, 0, 0, 0, 0,
      'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no',
      null, null, null, null, null, null, null, null, null, null, null
    );
    this._reporte.getReporteTurno(this.formSend).subscribe(data => {
      if (data.tTres) {
        this.dataGrid = data.ttMovi;
        this.dsResumen = data.tTres;
        this.dsCodigos = data.tTcod;
        this.dsCuadra = data.TotalMov;
        this.dataComplete = [];
        data.ttMovi.forEach(item => {
          if (item.tea !== 'X') {
            this.dataComplete.push(item);
          }
        });

        if (data.tTcod) {
          this.dsCodigosComplete = data.tTcod;
          const totalFp = JSON.parse(JSON.stringify(data.tTcod[0]));
          totalFp.tdesc = 'TOTAL:'
          totalFp.tcod = '';
          totalFp.tmonto = 0;
          data.tTcod.forEach(item => {
            if (item.tcod) {
              totalFp.tmonto += item.tmonto;
            }
          });
          this.dsCodigosComplete.push(totalFp);
        }
        this.total = aggregateBy(this.dataComplete, this.aggregates);
      }
      if (!data.tTres) {
        this.toast('No hay Movimentos!!', '', 'warning');
      }
      if (data[0] !== undefined) {
        this.toast(data[0].cMensTit, data[0].cMensTxt, 'warning');
        this.dataGrid = [];
        this.dataComplete = [];
        this.dsResumen = [];
      }
      this.loading = false;
    });
  }

  openModal(template) {
    this._modalService.open(template, { size: 'xl' });
  }

  createForm() {
    this.formSearch = this._fb.group({
      turno: [null, Validators.required],
      fechaIni: [moment(this.ttInfocia.fday).toDate(), Validators.required],
      fechaFin: [moment(this.ttInfocia.fday).toDate(), Validators.required],
      punto: ['', Validators.required],
      clase: ['', Validators.required],
      rturno: [1, Validators.required],
      usuario: [[]],
      estatus: [this.dsEstatusCheque, Validators.required]
    });
  }

  changeTipoReporte() {
    const form = this.formSearch;
    if (form.value.rturno === 2) {
      Object.keys(form.controls).forEach(key => {
        form.get(key).clearValidators();
        form.get(key).updateValueAndValidity();
      });
    }
    if (form.value.rturno === 1) {
      form.get('turno').setValidators(Validators.required);
      form.get('fechaIni').setValidators(Validators.required);
      form.get('fechaFin').setValidators(Validators.required);
      form.get('punto').setValidators(Validators.required);
      form.get('clase').setValidators(Validators.required);
      form.get('rturno').setValidators(Validators.required);
      form.get('estatus').setValidators(Validators.required);
      Object.keys(form.controls).forEach(key => {
        form.get(key).updateValueAndValidity();
      });
    }
  }

  changePunto(punto) {
    this._cheque.getTurnos(punto).subscribe(data => this.dsTurnos = data);
  }

  exportExcel() {
    const tbl = document.getElementById('tblExcel');
    const wb = XLSX.utils.table_to_book(tbl);
    XLSX.writeFile(wb, 'Reporte-Turno.xlsx');
  }

  ngAfterContentChecked() {
    if (this.dsResumen.length) {
      this.dsCodigosComplete = [];
      this.dsCodigosComplete = this.dsCodigos;
    }
  }

  exportPDF() {
    const d = new Date();
    const hotel = this.ttInfocia.cnombre;
    const fecha = this.ttInfocia.fday;
    const operador = this.ttUsuario.cUserId;
    const turno = this.resultForm.turno;
    const clase = this.resultForm.clase;
    let restaurante: any;
    this.dsPuntos.forEach(punto => {
      if (this.formSearch.value.punto = punto.Punto) {
        restaurante = punto;
      }
    });
    const header1 = restaurante.pdesc + ' [ Turno: ' + turno + ' (' + clase + ') ] ' + fecha;
    const header2 = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() + ' Ope: ' + operador;
    const doc = new jsPDF('l', 'pt', 'A4');
    const res = doc.autoTableHtmlToJson(document.getElementById('tbl1'));
    doc.page = 1;
    doc.autoTable(res.columns, res.data, {
      addPageContent: function (data) {
        doc.setFontSize(8);
        doc.text(hotel, 45, 20);
        doc.text(header1, 45, 35);
        doc.text(header2, 345, 35);
        doc.setFontType('normal');
        doc.text(345, 20, 'Hoja ' + doc.page);
        doc.setTextColor(100);
        doc.page++;
      },
      styles: {
        cellPadding: 2, // a number, array or object (see margin below)
        fontSize: 7,
        font: 'helvetica', // helvetica, times, courier
        lineColor: 200,
        lineWidth: 0,
        fontStyle: 'normal', // normal, bold, italic, bolditalic
        overflow: 'ellipsize', // visible, hidden, ellipsize or linebreak
        fillColor: false, // false for transparent or a color as described below
        textColor: 20,
        halign: 'right', // left, center, right
        valign: 'middle', // top, middle, bottom
        columnWidth: 'auto' // 'auto', 'wrap' or a number
      }
    });
    const res2 = doc.autoTableHtmlToJson(document.getElementById('tbl2'));
    doc.autoTable(res2.columns, res2.data, {
      startY: doc.autoTableEndPosY() + 20,
      styles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 7,
        font: 'helvetica', // helvetica, times, courier
        lineColor: 200,
        lineWidth: 0,
        fontStyle: 'normal', // normal, bold, italic, bolditalic
        overflow: 'ellipsize', // visible, hidden, ellipsize or linebreak
        fillColor: false, // false for transparent or a color as described below
        textColor: 20,
        halign: 'right', // left, center, right
        valign: 'middle', // top, middle, bottom
        columnWidth: 'auto' // 'auto', 'wrap' or a number
      }
    });
    const res3 = doc.autoTableHtmlToJson(document.getElementById('tbl3'));
    doc.autoTable(res3.columns, res3.data,
      {
        startY: doc.autoTableEndPosY() + 20,
        styles: {
          cellPadding: 5, // a number, array or object (see margin below)
          fontSize: 7,
          font: 'helvetica', // helvetica, times, courier
          lineColor: 200,
          lineWidth: 0,
          fontStyle: 'bold', // normal, bold, italic, bolditalic
          overflow: 'ellipsize', // visible, hidden, ellipsize or linebreak
          fillColor: false, // false for transparent or a color as described below
          textColor: 20,
          halign: 'left', // left, center, right
          valign: 'middle', // top, middle, bottom
          columnWidth: 'wrap' // 'auto', 'wrap' or a number
        }
      });
    doc.save('Reporte de Turno.pdf');
  }

  // Procesa la grid a mostrar cuando se agrupa por un campo. Grid debe tener Groupable = true
  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.dataGrid = process(this.dataComplete, { group: this.groups });
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    if (state && state.group) {
      state.group.map(group => group.aggregates = this.aggregates);
    }
    this.state = state;
    this.dataGrid = process(this.dataComplete, this.state);
  }

}
