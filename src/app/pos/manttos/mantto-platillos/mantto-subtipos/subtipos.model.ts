export class Subtipos {
  constructor(
    public iSubTipo: number,
    public cDesc: string,
    public cImagen= '',
    public cColor= '',
    public Inactivo: false,
    public cRowID: string,
    public lError = false,
    public cErrDesc = ''
  ) {}
  }
