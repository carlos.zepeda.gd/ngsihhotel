import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { GLOBAL } from '../../../../services/global';


const headers = new Headers({'Content-Type': 'application/json'});

@Injectable()
export class ManttoSubtiposService {

  public url: string;

  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }

  getSubtipos() {
    return this.http.get<any>( this.url + '/pos/manttos/psubtipos')
    .map(res => res.response.siPsubtipo);
}
// getSubtipo() {
//   return this.http.get( this.url + '/pos/manttos/psubtipos')
//                   .map( res => res.json().response.siPsubtipo.dsPsubtipos.ttPsubtipos );
// }

postSubtipo(subt) {
  const json = JSON.stringify(subt);
  const params = '{"dsPsubtipos":{"ttPsubtipos":[' + json + ']}}';
  return this.http.post<any>( this.url + '/pos/manttos/psubtipos', params, GLOBAL.httpOptions)
  .map(res => res.response.siPsubtipo);
}

putSubtipo(subt) {
  const json = JSON.stringify(subt);
  const params = '{"ttPsubtipos":[' + json + ']}';
  return this.http.put<any>( this.url + '/pos/manttos/psubtipos', params, GLOBAL.httpOptions)
  .map(res => res.response.siPsubtipo);
}
deleteSubtipo(subt) {
  const httpOptionsDelete = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    body: subt
  };
  return this.http.delete<any>( this.url + '/pos/manttos/psubtipos', httpOptionsDelete );
  }

}
