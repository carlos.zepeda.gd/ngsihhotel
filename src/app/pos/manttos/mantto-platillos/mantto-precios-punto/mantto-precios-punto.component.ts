import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import {ChequeAyudaService} from '../../../puntos-venta/cheque/services/cheque-ayuda.service';
import {ManttoPreciosPuntoService} from './mantto-precios-punto.service';
import swal from 'sweetalert2';
import {InfociaGlobalService} from '../../../../services/infocia.service';
import {ManttoMenuService} from '../../../../pms/manttos/mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import { GLOBAL } from '../../../../services/global';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';

declare var jsPDF: any;
const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-precios-punto',
  templateUrl: './mantto-precios-punto.component.html',
  styleUrls: ['./mantto-precios-punto.component.css']
})
export class ManttoPreciosPuntoComponent implements OnInit {
  heading = 'Precios de platillos por Punto de Venta';
  subheading = 'Point Of Sale.';
  icon = 'icon-gradient bg-tempting-azure';

  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public ttInfocia: any = [];
  public flagShowForm: boolean;
  flagEdit: boolean;
  public dataComplete: any = [];
  public checked: boolean;
  public groups: GroupDescriptor[] = [];
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{field: 'punto', dir: 'asc'}];
  public dsPuntos: any = [];
  public dsPuntosComplete: any [];
  public dsTipoPlatillos: any = [];
  public dsTipoPlatillosComplete: any = [];
  public dsPlatillos: any = [];
  public dsPlatillosComplete: any = [];
  public dsMonedas: any = [];

  loading = false;
  modalTitle = '';
  toast = GLOBAL.toast;
  dataFilter: any[];

  constructor(private fb: FormBuilder,
              private _info: InfociaGlobalService,
              private _mantto: ManttoPreciosPuntoService,
              private _cheque: ChequeAyudaService,
              private _modal: NgbModal,
              private _menuServ: ManttoMenuService,
              private _router: Router) {
      this.dataFilter = filterBy([this.dataGrid], this.filter);
      this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    this.ttInfocia = this._info.getSessionInfocia();
    this._cheque.getPuntos().subscribe(data => {
      this.dsPuntos = data;
      this.dsPuntosComplete = data;
    });
    this._cheque.getMonedas().subscribe(data => this.dsMonedas = data);
    this._cheque.getTipos().subscribe(data => {
      this.dsTipoPlatillos = data;
      this.dsTipoPlatillosComplete = data;
    });
    // Enviar, tipo, subtipo o platillo
    this._cheque.getPlatillos(0, 0, '').subscribe(data => {
      this.dsPlatillos = data;
      this.dsPlatillosComplete = data;
    });
    this.getDataSource();
    this.buildFormGroup();

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  private getDataSource() {
    this.loading = true;
    this._mantto.getPreciosPunto().subscribe(data => {
      if (!data) {
        data = [];
      }
      this.dataGrid = data;
      this.dataComplete = data;
      this.dataGrid = {
        data: orderBy(this.dataGrid, this.sort),
        total: this.dataGrid.length
      };
      this.loading = false;
      console.log(this.dataGrid);
    });
  }

  private buildFormGroup() {
    this.myFormGroup = this.fb.group({
      punto: [0, [Validators.required]],
      cPdesc: [''],
      tipo: [0, [Validators.required]],
      cTdesc: [''],
      platillo: [0, [Validators.required]],
      cPldesc: [''],
      dpprecio: [0, [Validators.required]],
      dpprop: [0, [Validators.required]],
      moneda: ['', [Validators.required]],
      cErrDesc: [''],
      lError: [false],
      cRowID: ['']
    });
  }

  private rebuildFormGroup() {
    this.myFormGroup.reset({
      punto: 0, cPdesc: '', tipo: 0, cTdesc: '', platillo: 0, cPldesc: '',
      dpprecio: 0, dpprop: 0, moneda: '', cErrDesc: '', lError: false, cRowID: ''
    });
  }

  public newRow() {
    this.flagShowForm = true;
    this.flagEdit = false;
  }

  public guardarForm() {
    if (this.flagEdit) {
      this.updateForm();
    } else {
      this.saveRow();
    }
  }

  public editRow({sender, rowIndex, dataItem}) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
  }

  private updateForm() {
    const form = this.myFormGroup.value;
    if (form.moneda.Moneda) {
      form.moneda = form.moneda.Moneda
    }
    if (form.platillo.platillo) {
      form.platillo = form.platillo.platillo
    }
    if (form.punto.Punto) {
      form.punto = form.punto.Punto
    }
    if (form.tipo.tipo) {
      form.tipo = form.tipo.tipo
    }
    this._mantto.putPreciosPunto(form).subscribe(data => {
      if (data[0].cMensTxt) {
        this.toast(GLOBAL.textError, data[0].cMensTxt, 'error');
      } else {
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        });
      }
      this.getDataSource();
      this.closeForm()
    });
  }

  public deleteRow({dataItem}) {
    this._mantto.deletePreciosPunto(dataItem).subscribe(data => {
      if (data) {
        this.toast(GLOBAL.textError, data[0].cMensTxt, 'error');
      } else {
        this.toast({
          type: 'success',
          title: GLOBAL.textDeleteSuccess
        });
      }
      this.getDataSource();
      this.closeForm();
    })
  }

  private saveRow() {
    const form = this.myFormGroup.value;
    if (form.moneda.Moneda) {
      form.moneda = form.moneda.Moneda
    }
    if (form.platillo.platillo) {
      form.platillo = form.platillo.platillo
    }
    if (form.punto.Punto) {
      form.punto = form.punto.Punto
    }
    if (form.tipo.tipo) {
      form.tipo = form.tipo.tipo
    }
    this._mantto.postPreciosPunto(form).subscribe(data => {
      if (data[0].cMensTxt) {
        this.toast(GLOBAL.textError, data[0].cMensTxt, 'error');
      } else {
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        });
      }
      this.getDataSource();
      this.closeForm()
    });
  }

  public closeForm() {
    this.flagShowForm = false;
    this.flagEdit = false;
    this.rebuildFormGroup();
  }

  public close(component) {
    this[component + 'Opened'] = false;
    this.closeForm();
  }

  public printPDF() {
    const encabezado = this.ttInfocia.encab;
    const columns = [
      {title: 'Punto', dataKey: 'punto'},
      {title: 'Tipo', dataKey: 'tipo'},
      {title: 'Platillo', dataKey: 'platillo'},
      {title: 'Precio', dataKey: 'dpprecio'},
      {title: 'Porcentaje Servicio', dataKey: 'dpprop'},
      {title: 'Moneda', dataKey: 'moneda'}
    ];
    const rows = this.dataGrid.data;
    const doc = new jsPDF('p', 'pt');
    doc.setFontSize(8);
    doc.text(encabezado, 40, 30);
    doc.setFontType('normal');
    doc.setFontSize(8);
    doc.setTextColor(100);
    doc.autoTable(columns, rows, {
      theme: 'striped',
      styles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 8,
        font: 'helvetica' // helvetica, times, courier
      },
    });
    doc.output('save', 'Precios por Punto.pdf');
  }

  // Filter Combo Box Kendo Ui
  puntoFilter(value) {
    this.dsPuntos = this.dsPuntosComplete.filter((s) => s.pdesc.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  tipoFilter(value) {
    this.dsTipoPlatillos = this.dsTipoPlatillosComplete.filter((s) => s.tdesc.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  platillosFilter(value) {
    this.dsPlatillos = this.dsPlatillosComplete.filter((s) => s.pdesc.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  // Funciones para la Grid de Kendo
  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.dataGrid = process(this.dataComplete, {group: this.groups});
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || {logic: 'and', filters: []};
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

  openModal(content, name: string) {
    this.modalTitle = name;
    this._modal.open(content, { size: 'sm', centered: true });
  }

  public allData(): ExcelExportData {
    if (this.filter) {
      const result: ExcelExportData = {
        data: process(this.dataFilter, { sort: [{ field: 'punto', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    } else {
      const result: ExcelExportData = {
        data: process(this.dataGrid.data, { sort: [{ field: 'punto', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    }
  }

}
