import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GLOBAL } from '../../../../services/global';
import 'rxjs/add/operator/map';
const headers = new Headers({'Content-Type': 'application/json'});

@Injectable()
export class ManttoPreciosPuntoService {

  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }
  getPreciosPunto() {
    return this.http.get( this.url + '/pos/manttos/punprec')
                    .map( res => res.json().response.siPunprec.dsPunprec.ttPunprec );
  }
  postPreciosPunto(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPunprec":{"ttPunprec":[' + json + ']}}';

    return this.http.post( this.url + '/pos/manttos/punprec',  params, { headers: headers })
                    .map( res => {
                      const succes = res.json().response.siPunprec.dsPunprec.ttPunprec;
                      const error  = res.json().response.siPunprec.dsMensajes.ttMensError;
                      if (error) {
                          return error;
                      }else {
                          return succes;
                      }

                    });
  }
  putPreciosPunto(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPunprec":{"ttPunprec":[' + json + ']}}';

    return this.http.put( this.url + '/pos/manttos/punprec',  params, { headers: headers })
                    .map( res => {
                      const succes = res.json().response.siPunprec.dsPunprec.ttPunprec;
                      const error  = res.json().response.siPunprec.dsMensajes.ttMensError;
                      if (error) {
                          return error;
                      }else {
                          return succes;
                      }
                    });
  }
  deletePreciosPunto(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPunprec":{"ttPunprec":[' + json + ']}}';

    return this.http.delete( this.url + '/pos/manttos/punprec',
                    new RequestOptions({ headers: headers, body: params }))
                    .map( res => {
                      if (res.json().response.siPunprec.dsPunprec === {}) {
                        return [];
                      }
                      const error = res.json().response.siPunprec.dsMensajes.ttMensError;
                      if (error) {
                        return error
                      } else {
                        return;
                      }
                    });
  }

}
