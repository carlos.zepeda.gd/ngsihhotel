import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { GLOBAL } from '../../../services/global';
import 'rxjs/add/operator/map';
const headers = new Headers({ 'Content-Type': 'application/json' });
const esp = GLOBAL.char174;
const api = GLOBAL.url;

@Injectable()
export class ManttoPlatillosService {

  constructor(private http: Http) { }

  servicePlatillo(data, method) {
    const json = '{"ttPplatillos":[' + JSON.stringify(data) + ']}';
    const url = api + '/pos/manttos/platillos';
    const header = { headers: headers };
    switch (method) {
      case 'get': return this.http.get(url + '/' + esp + esp + '').map(r => this.response(r.json().response.siPplatillo));
      case 'post': return this.http.post(url, json, header).map(r => this.response(r.json().response.siPplatillo));
      case 'put': return this.http.put(url, json, header).map(r => this.response(r.json().response.siPplatillo));
      case 'delete': return this.http.delete(url, { body: json, headers }).map(r => this.response(r.json().response.siPplatillo));
    }
  }
  response(res) {
    const error = res.dsMensajes.ttMensError;
    if (error) { return error[0]; }
    return res.dsPplatillos.ttPplatillos;
  }
  getUnidadMedida() {
    return this.http.get(api + '/manttos/medidas')
      .map(res => res.json().response.siMedida.dsMedidas.ttMedidas);
  }
}
