import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CompositeFilterDescriptor, filterBy, process } from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoDesglosePlatillosService } from '../mantto-desglose-platillos/mantto-desglose-platillos.service';
import { ManttoPlatillosService } from './mantto-platillos.service';
import { ManttoTiposVentasService } from './mantto-tipos-ventas/mantto-tipos-ventas.service';
import { ManttoSubtiposService } from './mantto-subtipos/mantto-subtipos.service';
import { ManttoTurnosService } from '../mantto-turnos/mantto-turnos.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { ManttoMenuService } from '../../../pms/manttos/mantto-menu/mantto-menu.service';
import { ChequeAyudaService } from '../../puntos-venta/cheque/services/cheque-ayuda.service'
import { GLOBAL } from '../../../services/global';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
declare var jsPDF: any;

@Component({
    selector: 'app-mantto-platillos',
    templateUrl: './mantto-platillos.component.html',
    styleUrls: ['./mantto-platillos.component.css']
})
export class ManttoPlatillosComponent implements OnInit {
    heading = 'Mantenimiento de Platillos';
    subheading = 'Point Of Sale.';
    icon = 'icon-gradient bg-tempting-azure';
    filter: CompositeFilterDescriptor;
    public myFormGroup: FormGroup;
    public dataGrid: any = [];
    public dataComplete: any = [];
    public dsClasificacion: any = [
        { clasif: 'A', cdesc: 'Alimentos' },
        { clasif: 'B', cdesc: 'Bebidas' },
        { clasif: 'V', cdesc: 'Varios' }
    ]
    public dsCodigos: any = [];
    public dsMonedas: any = [];
    public dsUnidadMedida: any = [];
    public dsPsCfd: any = [];
    public dsDesglose: any = [];
    public dsSubtipos: any = [];
    public dsTipos: any = [];
    public ttInfocia: any = [];
    public flagEdit: boolean;
    public checked: boolean;
    public flagShowForm: boolean;
    public dataExand: any;
    public generatePdf: boolean;
    public flagModificadores: boolean;
    public platilloSelected: any = [];
    public loading: boolean = true;
    pageSize = 10;
    skip = 0;
    closeResult: string;
    toast = GLOBAL.toast;
    dataFilter: any[];

    constructor(public _info: InfociaGlobalService,
        private pf: FormBuilder,
        private _desglose: ManttoDesglosePlatillosService,
        private _cheque: ChequeAyudaService,
        private _turnos: ManttoTurnosService,
        private _subtipo: ManttoSubtiposService,
        private _tipos: ManttoTiposVentasService,
        private _mantto: ManttoPlatillosService,
        private _menuServ: ManttoMenuService,
        private _router: Router,
        private modalService: NgbModal) {
        this.buildFormGroup();
        this.dataFilter = filterBy([this.dataGrid], this.filter);
        this.allData = this.allData.bind(this);
        this.getDataSource();
        this.ttInfocia = this._info.getSessionInfocia();
        this._tipos.getVentas().subscribe(data => this.dsTipos = data);
        this._desglose.getDesgloses().subscribe(data => this.dsDesglose = data);
        this._cheque.getMonedas().subscribe(data => this.dsMonedas = data);
        this._subtipo.getSubtipos().subscribe(data => this.dsSubtipos = data);
        this._turnos.getCodigos('D').subscribe(data => this.dsCodigos = data);
        this._mantto.getUnidadMedida().subscribe(data => this.dsUnidadMedida = data);
    }

    ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    openModalPlatillo(content) {
        this.modalService.open(content, {
            size: 'lg', centered: true
        });
    }

    getDataSource() {
      this.loading = true;
        this._mantto.servicePlatillo(null, 'get').subscribe(data => {
            this.dataGrid = data;
            this.dataComplete = data;
        });
        this.loading = false;
    }

    buildFormGroup() {
        this.myFormGroup = this.pf.group({
            tipo: [0, [Validators.required]],
            pprecio: [0],
            platillo: [0, [Validators.required]],
            pdesc: ['', [Validators.required]],
            pclas: [''],
            Medida: [null, [Validators.required]],
            Moneda: [null, [Validators.required]],
            codigo: [''],
            cfdPS: [''],
            dplat: [''],
            iSubtipo: [0],
            cColor: [''],
            cImagen: [''],
            Tiempo: [0],
            lModif: [false],
            nimp: [0],
            Enviar: [false],
            Inactivo: [false],
            cRowID: [''],
            cErrDesc: [''],
            lError: [false]
        });
    }

    private rebuildFormGroup() {
        this.myFormGroup.reset({
            tipo: 0, pprecio: 0, platillo: 0, pdesc: '', pclas: '', Medida: null, Moneda: null, codigo: '',
            dplat: '', iSubtipo: 0, cColor: '', cImagen: '', Tiempo: 0, lModif: false, nimp: 0,
            Enviar: false, Inactivo: false, cRowID: '', cErrDesc: '', lError: false
        });
    }

    public newRow() {
        this.flagShowForm = true;
        this.flagEdit = false;
    }

    public editRow(dataItem) {
        console.log(dataItem);
        this.myFormGroup.setValue(dataItem);
        this.flagEdit = true;
        this.flagShowForm = true;
    }

    public guardarForm() {
        if (this.flagEdit) {
            this.updateForm();
        } else {
            this.saveRow();
        }
    }

    private updateForm() {
      this.loading = true;
        const form = this.myFormGroup.value;
        if (form.Medida.cMedida) {
            form.Medida = form.Medida.cMedida;
        }
        if (form.Moneda.Moneda) {
            form.Moneda = form.Moneda.Moneda;
        }
        if (form.codigo.codigo) {
            form.codigo = form.codigo.codigo;
        }
        if (form.iSubtipo.cDesc) {
            form.iSubtipo = form.iSubtipo.iSubTipo;
        }
        if (form.pclas.clasif) {
            form.pclas = form.pclas.clasif;
        }
        if (form.tipo.tipo) {
            form.tipo = form.tipo.tipo;
        }
        if (form.dplat.dplat) {
            form.dplat = form.dplat.dplat;
        }
        this._mantto.servicePlatillo(form, 'put').subscribe(data => {
            if (data.cMensTxt) {
                this.toast(data.cMensTit, data.cMensTxt, 'error');
                this.loading = false;
            } else {
                this.getDataSource();
                this.closeForm();
                this.toast({
                  type: 'success',
                  title: GLOBAL.textSuccess
                });
            }
        });
    }

    private saveRow() {
        this.loading = true;
        const form = this.myFormGroup.value;
        if (form.Medida.cMedida) {
            form.Medida = form.Medida.cMedida
        }
        if (form.Moneda.Moneda) {
            form.Moneda = form.Moneda.Moneda
        }
        if (form.codigo.codigo) {
            form.codigo = form.codigo.codigo
        }
        if (form.iSubtipo.iSubTipo) {
            form.iSubtipo = form.iSubtipo.iSubTipo
        }
        // if (form.pclas.clasif) {
        //     form.pclas = form.pclas.clasif
        // }
        if (form.tipo.tipo) {
            form.tipo = form.tipo.tipo
        }
        if (form.dplat.dplat) {
            form.dplat = form.dplat.dplat
        }
        this._mantto.servicePlatillo(form, 'post').subscribe(data => {
            if (data.cMensTxt) {
                this.toast(data.cMensTit, data.cMensTxt, 'error');
                this.loading = false;
            } else {
                this.getDataSource();
                this.closeForm();
              this.toast({
                type: 'success',
                title: GLOBAL.textSuccess
              });
            }
        });
    }

    public deleteRow(dataItem) {
        this.loading = true;
        this._mantto.servicePlatillo(dataItem, 'delete').subscribe(data => {
            if (data) {
                if (data.cMensTxt) {
                    this.toast({
                        type: 'error', title: data.cMensTit, text: data.cMensTxt,
                    })
                    this.loading = false;
                } else {
                  this.toast({
                    type: 'success',
                    title: GLOBAL.textDeleteSuccess
                  });
                    this.getDataSource();
                }
            }
        });
    }

    btnModificadores(e) {
        this.platilloSelected = e;
        this.flagModificadores = true;
    }

    public closeForm() {
        this.flagShowForm = false;
        this.flagEdit = false;
        this.flagModificadores = false;
        this.rebuildFormGroup();
        this.loading = false;
    }

    public close(component) {
        this[component + 'Opened'] = false;
        this.closeForm();
    }

    public printPDF() {
        const encabezado = this.ttInfocia.encab;
        const columns = [
            { title: 'Tipo', dataKey: 'tipo' },
            { title: 'Platillo', dataKey: 'platillo' },
            { title: 'Descripción', dataKey: 'pdesc' },
            { title: 'Clasif', dataKey: 'pclas' },
            { title: 'Precio', dataKey: 'pprecio' },
            { title: 'Mon', dataKey: 'Moneda' },
            { title: '#Impresora', dataKey: 'nimp' },
            { title: 'Tiempo', dataKey: 'Tiempo' },
            { title: 'Inactivo', dataKey: 'Inactivo' }
        ];
        const rows = this.dataGrid.data;
        const doc = new jsPDF('p', 'pt');
        doc.setFontSize(8);
        doc.text(encabezado, 40, 30);
        doc.setFontType('normal');
        doc.setFontSize(8);
        doc.setTextColor(100);
        doc.autoTable(columns, rows, {
            theme: 'striped',
            styles: {
                cellPadding: 5, // a number, array or object (see margin below)
                fontSize: 8,
                font: 'helvetica' // helvetica, times, courier
            },
        });
        doc.output('save', 'Platillos.pdf');
    }

    public allData(): ExcelExportData {
      if (this.filter) {
        const result: ExcelExportData = {
          data: process(this.dataFilter, { sort: [{ field: 'tipo', dir: 'asc' }], filter: this.filter }).data
        };
        return result;
      } else {
        const result: ExcelExportData = {
          data: process(this.dataGrid, { sort: [{ field: 'tipo', dir: 'asc' }], filter: this.filter }).data
        };
        return result;
      }
    }

}
