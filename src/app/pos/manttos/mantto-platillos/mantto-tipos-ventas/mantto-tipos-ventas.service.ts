import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from '../../../../services/global';
import 'rxjs/add/operator/map';

const headers = new Headers({'Content-Type': 'application/json'});

@Injectable()
export class ManttoTiposVentasService {

  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  getVentas() {
    return this.http.get( this.url + '/pos/manttos/ptipos')
                    .map( res => res.json().response.siPtipo.dsPtipos.ttPtipos );
}
getVenta() {
  return this.http.get( this.url + '/pos/manttos/ptipos')
                  .map( res => res.json().response.siPtipo.dsPtipos.ttPtipos );
}

postVenta(ven) {
  const json = JSON.stringify(ven);
  const params = '{"dsPtipos":{"ttPtipos":[' + json + ']}}';
  return this.http.post( this.url + '/pos/manttos/ptipos', params, { headers: headers })
                  .map( (res: Response) => {
                    const succes = res.json().response.siPtipo.dsPtipos.ttPtipos;
                    const error  = res.json().response.siPtipo.dsMensajes.ttMensError;
                    if (error) {
                        return error;
                      } else {
                        return succes;
                      }
                  });
}
putVentas(ven) {
  const json = JSON.stringify(ven);
  const params = '{"ttPtipos":[' + json + ']}';
  return this.http.put( this.url + '/pos/manttos/ptipos', params, { headers: headers })
                  .map( (res: Response) => {
                      const succes = res.json().response.siPtipo.dsPtipos.ttPtipos;
                      const error  = res.json().response.siPtipo.dsMensajes.ttMensError;
                      if (error) {
                        return error;
                      }  else {
                        return succes;
                      }
                  });
}
deleteVenta(ven) {
  const json = JSON.stringify(ven);
  const params = '{"ttPtipos":[' + json + ']}';
  const options = new RequestOptions({ headers: headers });
  return this.http.delete( this.url + '/pos/manttos/ptipos', { body: params, headers } )
                  .map( (res: Response) => {
                        const data = [];
                        if (!res) {
                         return data;
                        } else {
                          return res.json().response.siPtipo.dsPtipos.ttPtipos;
                        }
                  });
  }

}

