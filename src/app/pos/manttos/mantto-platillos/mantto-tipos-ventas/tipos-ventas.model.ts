export class TiposVentas {
  constructor(
    public tipo: number,
    public tdesc: string,
    public tclas: string,
    public Inactivo: false,
    public cRowID: string,
    public lError= false,
    public cErrDesc= ''
  ) {}
  }
