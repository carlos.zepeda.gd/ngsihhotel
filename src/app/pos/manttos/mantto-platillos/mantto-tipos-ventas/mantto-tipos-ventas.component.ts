import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ManttoTiposVentasService} from './mantto-tipos-ventas.service';
import {InfociaGlobalService} from '../../../../../app/services/infocia.service';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import {ManttoMenuService} from '../../../../pms/manttos/mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import { GLOBAL } from '../../../../services/global';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';

declare var jsPDF: any;
const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-tipos-ventas',
  templateUrl: './mantto-tipos-ventas.component.html',
  styleUrls: ['./mantto-tipos-ventas.component.css']
})
export class ManttoTiposVentasComponent implements OnInit {
  heading = 'Mantenimiento de Tipos de Venta';
  subheading = 'Point Of Sale.';
  icon = 'icon-gradient bg-tempting-azure';

  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public dataComplete: any = [];
  public ttInfocia: any = [];
  flagEdit: boolean;
  public flagShowForm: boolean;
  public checked: boolean;
  public groups: GroupDescriptor[] = [];
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{field: 'tipo', dir: 'asc'}];
  public dstiposventas: Array<{ categoryName: string, categoryId: string }> = [
    {categoryName: 'Alimentos', categoryId: 'A'},
    {categoryName: 'Bebidas', categoryId: 'B'},
    {categoryName: 'Varios', categoryId: 'V'}
  ];

  loading = false;
  modalTitle = '';
  toast = GLOBAL.toast;
  dataFilter: any[];

  constructor(public _info: InfociaGlobalService,
              private pf: FormBuilder,
              private _mantto: ManttoTiposVentasService,
              private _modal: NgbModal,
              private _menuServ: ManttoMenuService,
              private _router: Router) {
    this.buildFormGroup();
    this.dataFilter = filterBy([this.dataGrid], this.filter);
    this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    this.getDataSource();
    this.ttInfocia = this._info.getSessionInfocia();

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  private getDataSource() {
    this.loading = true;
    this._mantto.getVentas().subscribe(data => {
      this.dataGrid = data;
      this.dataComplete = data;
      this.dataGrid = {
        data: orderBy(this.dataGrid, this.sort),
        total: this.dataGrid.length
      };
      this.loading = false;
    });
  }

  private buildFormGroup() {
    this.myFormGroup = this.pf.group({
      tipo: [0, [Validators.required]],
      tdesc: ['', [Validators.required]],
      tclas: ['', [Validators.required]],
      lsubtipo: [false],
      Inactivo: [false],
      cImagen: [''],
      cColor: [''],
      cRowID: [''],
      cErrDesc: [''],
      lError: [false]
    });
  }

  private rebuildFormGroup() {
    this.myFormGroup.reset({
      tipo: 0, tdesc: '', tclas: '', lsubtipo: false,
      Inactivo: false, cRowID: '', cErrDesc: '', lError: false
    });
  }

  public newRow() {
    this.flagShowForm = true;
    this.flagEdit = false;
  }

  public editRow({sender, rowIndex, dataItem}) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
  }

  public guardarForm() {
    if (this.flagEdit) {
      this.updateForm();
    } else {
      this.saveRow();
    }
  }

  private updateForm() {
    const form = this.myFormGroup.value;
    if (form.tclas.categoryId) {
      form.tclas = form.tclas.categoryId
    }
    this._mantto.putVentas(form).subscribe(data => {
      this.getDataSource();
      this.closeForm()
    });
  }

  private saveRow() {
    const form = this.myFormGroup.value;
    if (form.tclas.categoryId) {
      form.tclas = form.tclas.categoryId
    }
    this._mantto.postVenta(form).subscribe(data => {
      if (data && data[0].cMensTxt) {
        this.toast(GLOBAL.textError, data[0].cMensTxt, 'error');
      } else {
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        })
        this.getDataSource();
      }
      this.closeForm()
    });
  }

  public deleteRow({dataItem}) {
    this._mantto.deleteVenta(dataItem).subscribe(data => {
      if (data && data[0].cMensTxt) {
        this.toast(GLOBAL.textError, data[0].cMensTxt, 'error');
      } else {
        this.getDataSource();
        this.closeForm();
        this.toast({
          type: 'success',
          title: GLOBAL.textDeleteSuccess
        })
      }
    });
  }

  public closeForm() {
    this.flagShowForm = false;
    this.flagEdit = false;
    this.rebuildFormGroup();
  }

  public close(component) {
    this[component + 'Opened'] = false;
    this.closeForm();
  }

  public printPDF() {
    const encabezado = this.ttInfocia.encab;
    const columns = [
      {title: 'Tipo', dataKey: 'tipo'},
      {title: 'Descripcion', dataKey: 'tdesc'},
      {title: 'Clase', dataKey: 'tclas'},
      {title: 'Inactivo', dataKey: 'Inactivo'}
    ];
    const rows = this.dataGrid.data;
    const doc = new jsPDF('p', 'pt');
    doc.setFontSize(8);
    doc.text(encabezado, 40, 30);
    doc.setFontType('normal');
    doc.setFontSize(8);
    doc.setTextColor(100);
    doc.autoTable(columns, rows, {
      theme: 'striped',
      styles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 8,
        font: 'helvetica' // helvetica, times, courier
      },
    });
    doc.output('save', 'Tipos de Ventas.pdf');
  }

  // Funciones para la Grid de Kendo
  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.dataGrid = process(this.dataComplete, {group: this.groups});
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || {logic: 'and', filters: []};
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

  openModal(content, name: string) {
    this.modalTitle = name;
    this._modal.open(content, { size: 'sm', centered: true });
  }

  public allData(): ExcelExportData {
    if (this.filter) {
      const result: ExcelExportData = {
        data: process(this.dataFilter, { sort: [{ field: 'tipo', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    } else {
      const result: ExcelExportData = {
        data: process(this.dataGrid.data, { sort: [{ field: 'tipo', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    }
  }

}
