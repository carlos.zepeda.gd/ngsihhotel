import {Component, OnInit, Input, OnChanges, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ManttoModificadoresService} from '../../mantto-modificadores/mantto-modificadores.service';
import {ChequeAyudaService} from 'app/pos/puntos-venta/cheque/services/cheque-ayuda.service';
import {AddModificadorService} from './add-modificador.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-add-modificador',
  templateUrl: './add-modificador.component.html',
  styleUrls: ['./add-modificador.component.css']
})
export class AddModificadorComponent implements OnInit, OnChanges {
  @Input() data: any = [];
  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public dsModificadores: any [];
  private flagEdit: boolean;
  public flagShowForm: boolean;

  constructor(
    private fb: FormBuilder,
    private _mantto: AddModificadorService,
    private _modif: ManttoModificadoresService,
    private _cheque: ChequeAyudaService
  ) {
  }

  ngOnInit() {
    this.buildFormGroup();
    this._modif.getModificadores().subscribe(data => {
      this.dsModificadores = data;
    });
  }

  ngOnChanges(change: SimpleChanges) {
    if (change.data) {
      this.getDataSource();
    }
  }

  private buildFormGroup() {
    this.myFormGroup = this.fb.group({
      tipo: [0],
      platillo: [0],
      modificador: ['', [Validators.required, Validators.maxLength(10)]],
      Secuencia: [0],
      pprecio: [0],
      psel: [''],
      dmodif: [''],
      cErrDesc: [''],
      lError: [false],
      cRowID: ['']
    });
  }

  private rebuildForm() {
    this.myFormGroup.reset({
      tipo: this.data.tipo, platillo: this.data.platillo, modificador: '', Secuencia: 0, pprecio: 0,
      psel: '', dmodif: '', cErrDes: '', lError: false, cRowID: ''
    });
  }

  private getDataSource() {
    this._cheque.getModificadores(this.data.tipo, this.data.platillo, '').subscribe(data => this.dataGrid = data);
  }

  public newRow() {
    this.flagShowForm = true;
    this.flagEdit = false;
    this.rebuildForm();
  }

  public guardarForm() {
    if (this.flagEdit) {
      this.updateForm()
    } else {
      this.saveRow();
    }
  }

  public editRow({sender, rowIndex, dataItem}) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
  }

  private updateForm() {
    const form = this.myFormGroup.value;
    if (form.modificador.Modificador) {
      form.modificador = form.modificador.Modificador
    }
    this._mantto.putModificadorPlatillo(form).subscribe(data => this.closeForm());
  }

  private saveRow() {
    const form = this.myFormGroup.value;
    if (form.modificador.Modificador) {
      form.modificador = form.modificador.Modificador
    }
    this._mantto.postModificadorPlatillo(form).subscribe(data => {
      if (data[0].cErrDesc) {
        swal({
          position: 'center',
          type: 'error',
          title: data[0].cErrDesc,
          showConfirmButton: false,
          timer: 2000
        });
        return;
      }
      this.closeForm();
    })
  }

  public deleteRow({dataItem}) {
    this._mantto.deleteModificadorPlatillo(dataItem).subscribe(data => {
      if (data[0].cErrDesc) {
        swal({
          position: 'center',
          type: 'error',
          title: data[0].cErrDesc,
          showConfirmButton: false,
          timer: 2000
        })
      }
      this.closeForm()
    });
  }

  public closeForm() {
    this.flagEdit = false;
    this.flagShowForm = false;
    this.rebuildForm();
    this.getDataSource();
  }

}
