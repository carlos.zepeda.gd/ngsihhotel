import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { GLOBAL } from 'app/services/global';
const headers = new Headers({'Content-Type': 'application/json'});
const esp = GLOBAL.char174;
@Injectable()
export class AddModificadorService {

  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  getModificadorPlatillo() {
    return this.http.get( this.url + '/pos/manttos/platmodif' )
                    .map( res => res.json().response.siPlatmodif.dsPlatmodif.ttPlatmodif);
}

postModificadorPlatillo(mes) {
  const json = JSON.stringify(mes);
  const params = '{"ttPlatmodif":[' + json + ']}';
  return this.http.post( this.url + '/pos/manttos/platmodif', params, { headers: headers })
                  .map( (res: Response) => res.json().response.siPlatmodif.dsPlatmodif.ttPlatmodif);
}
putModificadorPlatillo(mes) {
  const json = JSON.stringify(mes);
  const params = '{"ttPlatmodif":[' + json + ']}';
  return this.http.put( this.url + '/pos/manttos/platmodif', params, { headers: headers })
                  .map( (res: Response) => res.json().response.siPlatmodif.dsPlatmodif.ttPlatmodif);
}
deleteModificadorPlatillo(mes) {
  const json = JSON.stringify(mes);
  const params = '{"ttPlatmodif":[' + json + ']}';
  const options = new RequestOptions({ headers: headers });
  return this.http.delete( this.url + '/pos/manttos/platmodif', { body: params, headers } )
                  .map( (res: Response) => res.json().response.siPlatmodif.dsPlatmodif.ttPlatmodif);
  }
}
