import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process } from '@progress/kendo-data-query';
import { ManttoUsuariosService } from '../../../pms/manttos/mantto-usuarios/mantto-usuarios.service';
import { ManttoMenuService } from '../../../pms/manttos/mantto-menu/mantto-menu.service';
import { ManttoClasesService } from '../mantto-clases/mantto-clases.service';
import { ManttoPuntosService } from './mantto-puntos.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { GLOBAL } from '../../../services/global';
import swal from 'sweetalert2';

declare var jsPDF: any;
const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-puntos',
  templateUrl: './mantto-puntos.component.html',
  styleUrls: ['./mantto-puntos.component.css']
})
export class ManttoPuntosComponent implements OnInit {
  heading = 'Mantenimiento Puntos de Venta';
  subheading = 'Point Of Sale.';
  icon = 'icon-gradient bg-tempting-azure';
  @ViewChild('modal') _modal: TemplateRef<any>;
  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public dataComplete: any = [];
  public dsCodigos: any = [];
  public dsIngAgrupados: any = [];
  public ttInfocia: any = [];
  public flagEdit: boolean;
  public checked: boolean;
  public groups: GroupDescriptor[] = [];
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'Punto', dir: 'asc' }];
  public dataExpand: any;
  public flagShowDetail: boolean;
  public dataDetail: any = [];
  public imagenPlano: string = '';
  dsClases: any = [];
  dsImpresoras: any = [];
  loading = false;
  toast = GLOBAL.toast;

  constructor(public _info: InfociaGlobalService,
    private _pf: FormBuilder,
    private _clases: ManttoClasesService,
    private _modalService: NgbModal,
    private _user: ManttoUsuariosService,
    private _mantto: ManttoPuntosService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _http: HttpClient) {
    this.buildFormGroup();
    this.getDataSource();
    this.ttInfocia = this._info.getSessionInfocia();
    this._mantto.getCodigos('D', 8).subscribe(data => this.dsCodigos = data);
    this._clases.getCheques().subscribe(data => this.dsClases = data);
    this._user.getIngAgrupado().subscribe(data => this.dsIngAgrupados = data);
    this._http.get<any>(GLOBAL.url + '/ayuda/impresoras')
      .subscribe(data => this.dsImpresoras = data.response.siAyuda.dsAyuda.ttImpresoras);
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  private getDataSource() {
    this.loading = true;
    this._mantto.getPuntos()
      .map(data => {
        data.forEach(punto => punto.cImagen = punto.cImagen.split(','));
        return data;
      })
      .subscribe(data => {
        this.dataGrid = data;
        this.dataComplete = data;
        this.dataGrid = {
          data: orderBy(this.dataGrid, this.sort),
          total: this.dataGrid.length
        };
        this.loading = false;
      });
  }

  private buildFormGroup() {
    this.myFormGroup = this._pf.group({
      Punto: [0, Validators.required],
      pdesc: ['', Validators.required],
      codigo: ['', Validators.required],
      plinea: [false],
      pnombre: [false],
      pcomanda: [false],
      ppimp: [false],
      pprop: [0],
      pnturnos: [0],
      pfc: [''],
      pvalmes: [false],
      pdias: [0],
      pmre: [0],
      pnotas1: [''],
      pnotas2: [''],
      pnotas3: [''],
      pnc1: [false],
      pnc2: [false],
      pnc3: [false],
      pic1: [''],
      pic2: [''],
      pic3: [''],
      Inactivo: [false],
      pturno: [0],
      iagrp: [''],
      Clase: [''],
      cImagen: [[]],
      cImpresora: [''],
      cRowID: [''],
      cErrDesc: [''],
      lError: [false]
    });
  }

  private rebuildFormGroup() {
    this.myFormGroup.reset({
      Punto: 0, pdesc: '', codigo: '', plinea: false, pnombre: false, pcomanda: false, ppimp: false, pprop: 0,
      pnturnos: 0, pfc: '', pvalmes: false, pdias: 0, pmre: 0, pnotas1: '', pnotas2: '', pnotas3: '', pnc1: false,
      pnc2: false, pnc3: false, pic1: '', pic2: '', pic3: '', Inactivo: false, pturno: 0, iagrp: '', cImagen: '',
      cImpresora: '', Clase: '', cRowID: '', cErrDesc: '', lError: false
    });
  }

  public newRow() {
    this.flagEdit = false;
    this.openModal(this._modal);
  }

  public editRow({ sender, rowIndex, dataItem }) {
    this._mantto.getPuntoClases(dataItem.Punto).subscribe(data => this.dataDetail = data);
    if (dataItem.cImagen[0]) {
      this.imagenPlano = dataItem.cImagen[0];
    }
    this.myFormGroup.patchValue(dataItem);
    this.flagEdit = true;
    this.openModal(this._modal);
  }

  openModal(template) {
    this._modalService.open(template, { size: 'xl' });
  }

  public guardarForm() {
    if (this.flagEdit) {
      this.updateForm();
    } else {
      this.saveRow();
    }
  }

  private updateForm() {
    const form = this.myFormGroup.value;
    form.cImagen[0] = this.imagenPlano;
    form.cImagen = form.cImagen.join(',');
    this._mantto.putPunto(form).subscribe(data => {
      if (data && data[0].cErrDesc) {
        this.toast(GLOBAL.textError, data[0].cErrDesc, 'error');
      } else {
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        })
        this.closeForm();
        this.getDataSource();
      }
    });
  }

  private saveRow() {
    const form = this.myFormGroup.value;
    form.cImagen = `${this.imagenPlano}, 400, 400`;
    this._mantto.postPunto(form).subscribe(data => {
      if (data && data[0].cErrDesc) {
        this.toast(GLOBAL.textError, data[0].cErrDesc, 'error');
      } else {
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        });
        this.getDataSource();
        this.closeForm();
      }
    });
  }

  public deleteRow({ dataItem }) {
    if (dataItem.cImagen.length) {
      dataItem.cImagen = dataItem.cImagen.join(',');
    }
    this._mantto.deletePunto(dataItem).subscribe(data => {
      if (!data) {
        this.getDataSource();
        this.toast({
          type: 'success',
          title: GLOBAL.textDeleteSuccess
        })
        return;
      }
      if (data[0].cMensTit !== undefined) {
        this.toast(data[0].cMensTit, data[0].cMensTxt, 'error');
        return;
      }
      if (data[0].cErrDesc) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: data[0].cErrDesc
        })
      }
    });
  }

  public closeForm() {
    this.flagShowDetail = false;
    this.flagEdit = false;
    this._modalService.dismissAll();
    this.rebuildFormGroup();
  }

  public close(component) {
    this[component + 'Opened'] = false;
    this.closeForm();
  }

  public printPDF() {
    const encabezado = this.ttInfocia.encab;
    const columns = [
      { title: 'Punto', dataKey: 'Punto' },
      { title: 'Descripción', dataKey: 'pdesc' },
      { title: 'Código', dataKey: 'cod' },
      { title: 'Linea F-O', dataKey: 'plinea' },
      { title: 'Nombre', dataKey: 'pnombre' },
      { title: '#Turnos', dataKey: 'pnturnos' },
      { title: 'Com.Aut.', dataKey: 'pcomanda' },
      { title: 'Prop.C/IVA ', dataKey: 'ppimp' },
      { title: 'Porc. Prop ', dataKey: 'pprop' },
      { title: 'Max Dia Reserv ', dataKey: 'pmre' },
      { title: 'Inactivo', dataKey: 'Inactivo' }
    ];
    const rows = this.dataGrid.data;
    const doc = new jsPDF('l', 'pt');
    doc.setFontSize(8);
    doc.text(encabezado, 40, 30);
    doc.setFontType('normal');
    doc.setFontSize(7);
    doc.setTextColor(100);
    doc.autoTable(columns, rows, {
      theme: 'striped',
      styles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 8,
        font: 'helvetica' // helvetica, times, courier
      },
    });
    doc.output('save', 'Puntos.pdf');
  }

  // Funciones para la Grid de Kendo

  public detailExpand({ dataItem }) {
    this.dataExpand = dataItem;
  }

  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.dataGrid = process(this.dataComplete, { group: this.groups });
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || { logic: 'and', filters: [] };
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

}
