import {Component, OnInit, OnChanges, Input, SimpleChanges} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ManttoPuntosService} from '../mantto-puntos.service';
import {InfociaGlobalService} from 'app/services/infocia.service';
import moment from 'moment';
import { GLOBAL } from 'app/services/global';

@Component({
  selector: 'app-detalle-punto',
  templateUrl: './detalle-punto.component.html',
  styleUrls: ['./detalle-punto.component.css']
})
export class DetallePuntoComponent implements OnInit, OnChanges {
  @Input() data: any = [];
  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public dsModificadores: any [];
  private flagEdit: boolean;
  public flagShowForm: boolean;

  fechaEdit: any;

  constructor(private fb: FormBuilder,
              private _mantto: ManttoPuntosService,
              private _info: InfociaGlobalService) {
    this.buildFormGroup();
  }

  ngOnInit() {
  }

  ngOnChanges(change: SimpleChanges) {
    if (change.data) {
      this.getDataSource();
    }
  }

  private buildFormGroup() {
    this.myFormGroup = this.fb.group({
      cErrDesc: [''],
      cPdesc: [''],
      cRowID: [''],
      lError: [false],
      pfecha: [''],
      pscom: [0],
      pturno: [0],
      punto: [0],
      sigturno: [0],
    });
  }

  private rebuildForm() {
    this.myFormGroup.reset({
      punto: this.data.Punto, cPdesc: '', pfecha: '', pscom: 0, pturno: 0,
      cErrDes: '', lError: false, cRowID: ''
    });
  }

  private getDataSource() {
    this._mantto.getPuntoDetalle(this.data.Punto).subscribe(data => this.dataGrid = data);
  }

  public newRow() {
    this.flagShowForm = true;
    this.flagEdit = false;
    this.rebuildForm();
  }

  public editRow({sender, rowIndex, dataItem}) {
    this.fechaEdit = moment(dataItem.pfecha).toDate();
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
  }

  public guardarForm() {
    if (this.flagEdit) {
      this.updateForm()
    } else {
      // this.saveRow();
    }
  }

  private updateForm() {
    const form = this.myFormGroup.value;
    form.pfecha = moment(form.pfecha).format(GLOBAL.formatIso);
    this._mantto.putPuntoClase(form).subscribe(data => this.closeForm());
  }

  public closeForm() {
    this.flagEdit = false;
    this.flagShowForm = false;
    this.rebuildForm();
    this.getDataSource();
  }

}
