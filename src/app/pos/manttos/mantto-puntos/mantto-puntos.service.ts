import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from '../../../services/global';
import 'rxjs/add/operator/map';
const headers = new Headers({'Content-Type': 'application/json'});
const esp = GLOBAL.char174;
@Injectable()
export class ManttoPuntosService {

  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  getPuntos() {
    return this.http.get( this.url + '/pos/manttos/puntos')
                    .map( res => res.json().response.siPunto.dsPuntos.ttPuntos);
  }
  postPunto(mes) {
    const json = JSON.stringify(mes);
    const params = '{"ttPuntos":[' + json + ']}';
    return this.http.post( this.url + '/pos/manttos/puntos', params, { headers: headers })
                    .map( (res: Response) => res.json().response.siPunto.dsPuntos.ttPuntos);
  }
  getCodigos(tipo, opcion) {
      return this.http.get( this.url + '/ayuda/codigos' + esp + tipo + esp + opcion + esp + 'y' )
                      .map( res => res.json().response.siAyuda.dsAyuda.ttCodigos );
  }
  putPunto(mes) {
    const json = JSON.stringify(mes);
    const params = '{"ttPuntos":[' + json + ']}';
    return this.http.put( this.url + '/pos/manttos/puntos', params, { headers: headers })
                    .map( (res: Response) => res.json().response.siPunto.dsPuntos.ttPuntos);
  }
  deletePunto(mes) {
    const json = JSON.stringify(mes);
    const params = '{"ttPuntos":[' + json + ']}';
    const options = new RequestOptions({ headers: headers });
    return this.http.delete( this.url + '/pos/manttos/puntos', { body: params, headers } )
                    .map( (res: Response) => {
                        if (!res.json().response.siPunto.dsPuntos) {
                          return [];
                        }
                        const error = res.json().response.siPunto.dsMensajes.ttMensError;
                        const success = res.json().response.siPunto.dsPuntos.ttPuntos;
                        if (error) {
                          return error;
                        }else {
                          return success;
                        }
                    });
  }

  getPuntoDetalle(punto) {
    return this.http.get( this.url + '/pos/manttos/pundet/' + punto)
                    .map( res => res.json().response.siPundet.dsPundet.ttPundet);
  }
  getPuntoClases(punto) {
    return this.http.get( this.url + '/pos/manttos/puncla/' + punto + esp)
                    .map( res => res.json().response.siPuncla.dsPuncla.ttPuncla);
  }
  putPuntoClase(data) {
    const json = JSON.stringify(data);
    const params = '{"ttPundet":[' + json + ']}';
    return this.http.put( this.url + '/pos/manttos/pundet', params, { headers: headers })
                    .map( (res: Response) => res.json().response.siPundet.dsPundet.ttPundet);
  }
}
