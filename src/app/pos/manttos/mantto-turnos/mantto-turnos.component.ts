import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import {ManttoTurnosService} from './mantto-turnos.service';
import {ChequeAyudaService} from '../../puntos-venta/cheque/services/cheque-ayuda.service';
import {InfociaGlobalService} from '../../../services/infocia.service';
import {ManttoMenuService} from '../../../pms/manttos/mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { GLOBAL } from 'app/services/global';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';

declare var jsPDF: any;

@Component({
  selector: 'app-mantto-turnos',
  templateUrl: './mantto-turnos.component.html',
  styleUrls: ['./mantto-turnos.component.css']
})
export class ManttoTurnosComponent implements OnInit {
  heading = 'Mantenimiento de Turnos';
  subheading = 'Point Of Sale.';
  icon = 'icon-gradient bg-tempting-azure';

  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public dataComplete: any = [];
  public ttInfocia: any = [];
  public dsTurnos: any = [];
  public dsPuntos: any = [];
  flagEdit: boolean;
  public flagShowForm: boolean;
  public groups: GroupDescriptor[] = [];
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{field: 'Turno', dir: 'asc'}];
  public dataExand: any;

  loading = false;
  toast = GLOBAL.toast;
  dataFilter: any[];

  constructor(private fb: FormBuilder,
              public _info: InfociaGlobalService,
              private _mantto: ManttoTurnosService,
              private _cheque: ChequeAyudaService,
              private _menuServ: ManttoMenuService,
              public modalService: NgbModal,
              private _router: Router) {
    this.dataFilter = filterBy([this.dataGrid], this.filter);
    this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    this.ttInfocia = this._info.getSessionInfocia();
    this.buildFormGroup();
    this.getDataSource();
    this._cheque.getPuntos().subscribe(data => this.dsPuntos = data);

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  private buildFormGroup() {
    this.myFormGroup = this.fb.group({
      Punto: [0, Validators.required],
      Turno: [0, Validators.required],
      tdesc: ['', Validators.required],
      cErrDesc: [''],
      lError: [false],
      cRowID: ['']
    });
  }

  private rebuildFormGroup() {
    this.myFormGroup.reset({Punto: 0, Turno: 0, tdesc: '', cErrDes: '', lError: false, cRowID: ''});
  }

  private getDataSource() {
    this.loading = true;
    this._mantto.getTurnos().subscribe(data => {
      this.dataGrid = data;
      this.dsTurnos = this.dataGrid;
      this.dataComplete = data;
      this.dataGrid = {
        data: orderBy(this.dataGrid, this.sort),
        total: this.dataGrid.length
      };
      this.loading = false;
    });
  }

  openModal(content) {
    this.modalService.open(content, {
      size: 'md'
    });
  }

  public newRow() {
    this.flagShowForm = true;
    this.flagEdit = false;
  }

  public editRow(dataItem) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
  }

  public guardarForm() {
    if (this.flagEdit) {
      this.updateForm()
    } else {
      this.saveRow();
    }
  }

  private updateForm() {
    const form = this.myFormGroup.value;
    if (form.Punto.Punto) {
      form.Punto = form.Punto.Punto;
    }
    this._mantto.putTurno(form).subscribe(data => {
      if (data[0].cMensTxt){
        this.toast(GLOBAL.textError, data[0].cMensTxt, 'error');
      }else{
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        });
      }
      this.getDataSource();
      this.closeForm()
    });
  }

  private saveRow() {
    const form = this.myFormGroup.value;
    if (form.Punto.Punto) {
      form.Punto = form.Punto.Punto;
    }
    this._mantto.postTurno(form).subscribe(res => {
        if (res[0].cMensTxt){
          this.toast(GLOBAL.textError, res[0].cMensTxt, 'error');
        }else{
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          });
        }
        this.getDataSource();
        this.closeForm()
    });
  }

  public deleteRow(dataItem) {
    this._mantto.deleteTurno(dataItem).subscribe(data => {
      if (data) {
        if (data.cMensTxt) {
          this.toast(data.cMensTit, data.cMensTxt, 'error');
        } else {
          this.toast({
            type: 'success',
            title: GLOBAL.textDeleteSuccess
          });
          this.getDataSource();
          this.rebuildFormGroup();
        }
      }
    });
  }

  public closeForm() {
    this.flagShowForm = false;
    this.flagEdit = false;
    this.rebuildFormGroup();
  }

  public printPDF() {
    const encabezado = this.ttInfocia.encab;
    const columns = [
      {title: 'Punto', dataKey: 'Punto'},
      {title: 'Turno', dataKey: 'Turno'},
      {title: 'Descripción', dataKey: 'tdesc'}
    ];
    const rows = this.dataGrid.data;
    const doc = new jsPDF('p', 'pt');
    doc.setFontSize(8);
    doc.text(encabezado, 40, 30);
    doc.setFontType('normal');
    doc.setFontSize(8);
    doc.setTextColor(100);
    doc.autoTable(columns, rows, {
      theme: 'striped',
      styles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 8,
        font: 'helvetica' // helvetica, times, courier
      },
    });
    doc.output('save', 'Turnos.pdf');
  }

  public close(component) {
    this[component + 'Opened'] = false;
    this.closeForm();
  }

  public puntoFilter(value) {
    this.dsPuntos = this.dataComplete.filter((s) => s.pdesc.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  public turnoFilter(value) {
    this.dsTurnos = this.dataComplete.filter((s) => s.tdesc.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.dataGrid = process(this.dataComplete, {group: this.groups});
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

  public allData(): ExcelExportData {
    if (this.filter) {
      const result: ExcelExportData = {
        data: process(this.dataFilter, { sort: [{ field: 'Turno', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    } else {
      const result: ExcelExportData = {
        data: process(this.dataGrid.data, { sort: [{ field: 'Turno', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    }
  }

}
