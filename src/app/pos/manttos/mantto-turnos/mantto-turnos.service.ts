import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { GLOBAL } from '../../../services/global';
const headers = new Headers({ 'Content-Type': 'application/json' });
const esp = GLOBAL.char174;
@Injectable()
export class ManttoTurnosService {

  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }
  getTurnos() {
    return this.http.get(this.url + '/pos/ayuda/pturnos')
      .map(res => res.json().response.siPosayuda.dsPosayuda.ttPturnos);
  }
  getCodigos(tipo) {
    return this.http.get(this.url + '/ayuda/codigos' + esp + tipo + esp + '' + esp + 'y')
      .map(res => res.json().response.siAyuda.dsAyuda.ttCodigos);
  }
  getClasesxTurno(data, clase?) {
    return this.http.get(this.url + '/pos/manttos/pturcla/' + data.Punto + esp + data.Turno + esp)
      .map(res => res.json().response.siPturcla.dsPturcla.ttPturcla);
  }
  putClasesxTurno(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPturcla":{"ttPturcla":[' + json + ']}}';

    return this.http.put(this.url + '/pos/manttos/pturcla', params, { headers: headers })
      .map(res => res.json().response.siPturcla.dsPturcla.ttPturcla);
  }
  postClasesxTurno(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPturcla":{"ttPturcla":[' + json + ']}}';

    return this.http.post(this.url + '/pos/manttos/pturcla', params, { headers: headers })
      .map(res => res.json().response.siPturcla.dsPturcla.ttPturcla);
  }
  deleteClasesxTurno(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPturcla":{"ttPturcla":[' + json + ']}}';

    return this.http.delete(this.url + '/pos/manttos/pturcla', new RequestOptions({ headers: headers, body: params }))
      .map(res => res.json().response.siPturcla.dsPturcla.ttPturcla);
  }
  postTurno(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPturnos":{"ttPturnos":[' + json + ']}}';

    return this.http.post(this.url + '/pos/manttos/pturnos', params, { headers: headers })
      .map(res => res.json().response.siPturno.dsPturnos.ttPturnos);
  }
  putTurno(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPturnos":{"ttPturnos":[' + json + ']}}';

    return this.http.put(this.url + '/pos/manttos/pturnos', params, { headers: headers })
      .map(res => res.json().response.siPturno.dsPturnos.ttPturnos);
  }

  deleteTurno(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPturnos":{"ttPturnos":[' + json + ']}}';

    return this.http.delete(this.url + '/pos/manttos/pturnos', new RequestOptions({ headers: headers, body: params }))
      .map(res => {
        const r = res.json().response.siPturno;
        if (r.dsMensajes.ttMensError) {
          return r.dsMensajes.ttMensError[0];
        } else {
          return r.dsPturnos.ttPturnos;
        }
      });
  }

}
