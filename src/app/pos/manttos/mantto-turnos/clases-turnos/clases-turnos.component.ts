import { Component, OnInit, Output, Input, OnChanges,  SimpleChanges, EventEmitter } from '@angular/core';
import { ManttoTurnosService } from '../mantto-turnos.service';
import { ChequeAyudaService } from '../../../puntos-venta/cheque/services/cheque-ayuda.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import swal from 'sweetalert2';
import { GLOBAL } from 'app/services/global';
declare var jsPDF: any;

@Component({
  selector: 'app-clases-turnos',
  templateUrl: './clases-turnos.component.html',
  styleUrls: ['./clases-turnos.component.css']
})
export class ClasesTurnosComponent implements OnInit, OnChanges {
  @Output() new = new EventEmitter;
  @Output() edit = new EventEmitter;
  @Input() data: any = [];
  public myFormGroup: FormGroup;
  public dsCredito: any = [];
  public dsDebito: any = [];
  public dsTurnos: any = [];
  public dsPuntos: any = [];
  public dsCreditoComplete: any = [];
  public dsDebitoComplete: any = [];
  public dsClases: any = [];
  public dsClasesComplete: any = [];
  public dataGrid: any = [];
  private flagEdit: boolean;
  public flagShowForm: boolean;
  toast = GLOBAL.toast;

  constructor(
    private fb: FormBuilder,
    private _mantto: ManttoTurnosService,
    private _cheque: ChequeAyudaService
  ) { }

  ngOnInit() {
    this.buildFormGroup();
    this.getDataSource();
    this._mantto.getTurnos().subscribe(data => this.dsTurnos = data);
    this._cheque.getPuntos().subscribe(data => this.dsPuntos = data);
    this._mantto.getCodigos('C').subscribe(data => {
      this.dsCredito = data.slice();
      this.dsCreditoComplete = data;
    });
    this._mantto.getCodigos('D').subscribe(data => {
      this.dsDebito = data.slice();
      this.dsDebitoComplete = data;
    });
  }
  getDataSource() {
    this._cheque.getClases().subscribe(data => {
      this.dsClases = data.slice();
      this.dsClasesComplete = data;
    });
  }
  buildFormGroup() {
    this.myFormGroup = this.fb.group ({
      Punto:      [],
      cPdesc:     [''],
      Turno:      [],
      cTdesc:     [''],
      Clase:      ['', Validators.required ],
      cCdesc:     [''],
      talimentos: ['', Validators.required ],
      cAlidesc:   [''],
      tbebidas:   ['', Validators.required ],
      cBebdesc:   [''],
      tvarios:    ['', Validators.required ],
      cVardesc:   [''],
      thuespedes: ['',  Validators.required ],
      cHuedesc:   [''],
      tcaja:      ['', Validators.required ],
      cCajadesc:  [''],
      cErrDesc:   [''],
      lError:     [false],
      cRowID:     ['']
    });
  }
  rebuildForm() {
    this.myFormGroup.reset({
        Punto:  this.data.Punto, cPdes: '', Turno: this.data.Turno, cTdesc: '', Clase: '', cCdesc: '', talimentos: '', cAlidesc: '',
        tbebidas: '', cBebdesc: '', tvarios: '', cVardesc: '', thuespedes: '', cHuedesc: '', tcaja: '', cCajadesc: '',
        tdesc: '', cErrDes: '', lError: false, cRowID: '' });
  }
  ngOnChanges(change: SimpleChanges) {
    if (change.data) {
      this.getClasesTurnos();
    }
  }
  getClasesTurnos() {
    this._mantto.getClasesxTurno(this.data).subscribe(res => this.dataGrid = res);
  }
  guardarForm() {
    if (this.flagEdit) {
      this.updateForm()
    }else {
      this.saveRow();
    }
  }
  saveRow() {
    const form = this.myFormGroup.value;
    if (form.Clase.clase) { form.Clase = form.Clase.clase; }
    if (form.talimentos.Codigo) { form.talimentos = form.talimentos.Codigo; }
    if (form.tbebidas.Codigo) { form.tbebidas = form.tbebidas.Codigo; }
    if (form.tvarios.Codigo) { form.tvarios = form.tvarios.Codigo; }
    if (form.thuespedes.Codigo) { form.thuespedes = form.thuespedes.Codigo; }
    if (form.tcaja.Codigo) { form.tcaja = form.tcaja.Codigo; }
    this._mantto.postClasesxTurno(form).subscribe(data => {
      if (data[0].cErrDesc) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError
        })
      }else {
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess,
        })
      }
      this.cerrarForm();
    })
  }
  updateForm() {
    const form = this.myFormGroup.value;
    if (form.talimentos.Codigo) { form.talimentos = form.talimentos.Codigo; }
    if (form.tbebidas.Codigo) { form.tbebidas = form.tbebidas.Codigo; }
    if (form.tvarios.Codigo) { form.tvarios = form.tvarios.Codigo; }
    if (form.thuespedes.Codigo) { form.thuespedes = form.thuespedes.Codigo; }
    if (form.tcaja.Codigo) { form.tcaja = form.tcaja.Codigo; }
    this._mantto.putClasesxTurno(form).subscribe(data => this.cerrarForm());
  }
  newRecord() {
    this.flagShowForm = true;
    this.flagEdit = false;
    this.rebuildForm();
  }
  cerrarForm() {
    this.flagEdit = false;
    this.flagShowForm = false;
    this.rebuildForm();
    this.getClasesTurnos();
  }
  public editRow(dataItem) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
  }
  public deleteRow(dataItem) {
    this._mantto.deleteClasesxTurno(dataItem).subscribe(data => this.cerrarForm());
  }
  public claseFilter(value) {
    this.dsClases = this.dsClasesComplete.filter((s) => s.cdesc.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
  public debitoFilter(value) {
    this.dsDebito = this.dsDebitoComplete.filter((s) => s.cdesc.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
  public creditoFilter(value) {
    this.dsCredito = this.dsCreditoComplete.filter((s) => s.cdesc.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
}
