import {Component, OnInit, ViewChild, TemplateRef} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ManttoClasesService} from './mantto-clases.service';
import {InfociaGlobalService} from '../../../../app/services/infocia.service';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import {ManttoMenuService} from '../../../pms/manttos/mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { GLOBAL } from '../../../services/global';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';

declare var jsPDF: any;
const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-clases',
  templateUrl: './mantto-clases.component.html',
  styleUrls: ['./mantto-clases.component.css']
})
export class ManttoClasesComponent implements OnInit {
  heading = 'Mantenimiento Clases de Cheques';
  subheading = 'Point Of Sale.';
  icon = 'icon-gradient bg-tempting-azure';
  @ViewChild('modalForm') modalForm: TemplateRef<any>;
  public dataGrid: any = [];
  public dataComplete: any = [];
  public myFormGroup: FormGroup;
  public ttInfocia: any;
  public flagEdit: boolean;
  public checked: boolean;
  public groups: GroupDescriptor[] = [];
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{field: 'clase', dir: 'asc'}];

  loading = false;
  dataFilter: any[];
  toast = GLOBAL.toast;

  constructor(public _info: InfociaGlobalService,
              private fb: FormBuilder,
              private _mantto: ManttoClasesService,
              private _menuServ: ManttoMenuService,
              private _router: Router,
              private modalService: NgbModal) {
    this.buildFormGroup();
    this.dataFilter = filterBy([this.dataGrid], this.filter);
    this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    this.getDataSource();
    this.ttInfocia = this._info.getSessionInfocia();

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  private buildFormGroup() {
    this.myFormGroup = this.fb.group({
      clase: ['', Validators.required],
      cdesc: ['', Validators.required],
      civa: [false],
      cprop: [false],
      cdescto: [false],
      ccuarto: [false],
      cporc: [0],
      ccheque: [false],
      Inactivo: [false],
      cRowID: [''],
      cErrDesc: [''],
      lError: [false]
    });
  }

  private rebuildFormGroup() {
    this.myFormGroup.reset({
      clase: '', cdesc: '', civa: false, cprop: false, cdescto: false, ccuarto: false,
      cporc: 0, ccheque: false, Inactivo: false, cRowID: '', cErrDesc: '', lError: false
    });
  }

  private getDataSource() {
    this.loading = true;
    this._mantto.getCheques().subscribe(data => {
      this.dataGrid = data;
      this.dataComplete = data;
      this.dataGrid = {
        data: orderBy(this.dataGrid, this.sort),
        total: this.dataGrid.length
      };
      this.loading = false;
    });
  }

  public editRow({sender, rowIndex, dataItem}) {
    this.openModal(this.modalForm);
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
  }

  public guardarForm() {
    if (this.flagEdit) {
      this.updateForm();
    } else {
      this.saveRow();
    }
  }

  private updateForm() {
    const form = this.myFormGroup.value;
    this._mantto.putCheques(form).subscribe(data => {
      if (data[0].cMensTxt){
        this.toast(GLOBAL.textError, data[0].cMensTxt, 'error');
      }else{
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        });
      }
      this.getDataSource();
      this.closeForm()
    });
  }

  private saveRow() {
    const form = this.myFormGroup.value;
    this._mantto.postCheque(form).subscribe(data => {
      if (data[0].cMensTxt){
        this.toast(GLOBAL.textError, data[0].cMensTxt, 'error');
      }else{
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        });
      }
      this.getDataSource();
      this.closeForm()
    });
  }

  public deleteRow({dataItem}) {
    this._mantto.deleteCheque(dataItem).subscribe(data => {
      if (data) {
        if (data[0].cErrDesc) {
          this.toast(GLOBAL.textError, data[0].cErrDesc, 'error');
        }else {
          this.toast({
            type: 'success',
            title: GLOBAL.textDeleteSuccess
          })
        }
        this.getDataSource();
        this.closeForm();
      }
    });
  }

  openModal(content) {
    this.modalService.open(content, {
      size: 'md'
    });
  }

  closeForm() {
    this.flagEdit = false;
    this.modalService.dismissAll();
    this.rebuildFormGroup();
  }

  printPDF() {
    const encabezado = this.ttInfocia.encab;
    const columns = [
      {title: 'Clase', dataKey: 'clase'},
      {title: 'Descripcion', dataKey: 'cdesc'},
      {title: 'IVA', dataKey: 'civa'},
      {title: 'Propina', dataKey: 'cprop'},
      {title: 'Cuarto', dataKey: 'ccuarto'},
      {title: 'Descuento', dataKey: 'cdescto'},
      {title: 'Procentaje', dataKey: 'cporc'},
      {title: 'Inactivo', dataKey: 'Inactivo'}
    ];
    const rows = this.dataGrid.data;
    const doc = new jsPDF('p', 'pt');
    doc.setFontSize(8);
    doc.text(encabezado, 40, 30);
    doc.setFontType('normal');
    doc.setFontSize(8);
    doc.setTextColor(100);
    doc.autoTable(columns, rows, {
      theme: 'striped',
      styles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 8,
        font: 'helvetica' // helvetica, times, courier
      },
    });
    doc.output('save', 'Clases de Cheques.pdf');
  }

  // Funciones para la Grid de Kendo
  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.dataGrid = process(this.dataComplete, {group: this.groups});
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || {logic: 'and', filters: []};
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

  public allData(): ExcelExportData {
    if (this.filter) {
      const result: ExcelExportData = {
        data: process(this.dataFilter, { sort: [{ field: 'clase', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    } else {
      const result: ExcelExportData = {
        data: process(this.dataGrid.data, { sort: [{ field: 'clase', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    }
  }

}
