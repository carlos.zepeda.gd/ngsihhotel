import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { GLOBAL } from '../../../services/global';
import 'rxjs/add/operator/map';

const headers = new Headers({'Content-Type': 'application/json'});

@Injectable()
export class ManttoClasesService {

  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  getCheques() {
    return this.http.get( this.url + '/pos/manttos/pclases')
                    .map( res => res.json().response.siPclase.dsPclases.ttPclases );
        
}
getCheque() {
  return this.http.get( this.url + '/pos/manttos/pclases')
                  .map( res => res.json().response.siPclase.dsPclases.ttCheques );
}
putCheques(che) {
  const json = JSON.stringify(che);
  const params = '{"ttPclases":[' + json + ']}';

  return this.http.put( this.url + '/pos/manttos/pclases', params, { headers: headers })
                  .map( (res: Response) => {
                      const succes = res.json().response.siPclase.dsPclases.ttPclases;
                      const error  = res.json().response.siPclase.dsMensajes.ttMensError;
                      if (error) {
                        return error;
                      }  else {
                        return succes;
                      }
                  });
}
postCheque(che) {
  const json = JSON.stringify(che);
  const params = '{"ttPclases":[' + json + ']}';

  return this.http.post( this.url + '/pos/manttos/pclases', params, { headers: headers })
                  .map( (res: Response) => {
                      const succes = res.json().response.siPclase.dsPclases.ttPclases;
                      const error  = res.json().response.siPclase.dsMensajes.ttMensError;
                      if (error) {
                        return error;
                      } else {
                        return succes;
                      }
                  });
}
deleteCheque(che) {
  const json = JSON.stringify(che);
  const params = '{"dsPclases":{"ttPclases":[' + json + ']}}';
  const options = new RequestOptions({ headers: headers });
  return this.http.delete( this.url + '/pos/manttos/pclases', { body: params, headers } )
                  .map( (res: Response) => {
                        const data = [];
                        if (!res) {
                         return data;
                        } else {
                          return res.json().response.sIPclase.dsPclases.ttPclases;
                          // Cannot read property 'dsPclases' of undefined
                        }
                  });
  }
}

