export class ClaseCheques {
    constructor(
        public clase: string,
        public cdesc: string,
        public civa: false,
        public cprop: false,
        public cdescto: false,
        public ccuarto: false,
        public cporc: number,
        public ccheque: false,
        public Inactivo: false,
        public cRowID: string,
        public cErrDes = '',
        public lError = false
    ) {}
}

