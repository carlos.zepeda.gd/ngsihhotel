import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process } from '@progress/kendo-data-query';
import { ManttoModificadoresService } from './mantto-modificadores.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { ManttoMenuService } from '../../../pms/manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../services/global';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

declare var jsPDF: any;
const flatten = GLOBAL.flatten;
const swal = GLOBAL.swal;

@Component({
  selector: 'app-mantto-modificadores',
  templateUrl: './mantto-modificadores.component.html',
  styleUrls: ['./mantto-modificadores.component.css']
})
export class ManttoModificadoresComponent implements OnInit {
  heading = 'Mantenimiento de Modificadores';
  subheading = 'Point Of Sale.';
  icon = 'icon-gradient bg-tempting-azure';

  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public dataComplete: any = [];
  public ttInfocia: any = [];
  public flagShowForm: boolean;
  flagEdit: boolean;
  public checked: boolean;
  public groups: GroupDescriptor[] = [];
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'Modificador', dir: 'asc' }];
  loading = false;
  modalTitle = '';
  toast = GLOBAL.toast;
  dataFilter: any[];

  constructor(private _fb: FormBuilder,
    public _info: InfociaGlobalService,
    private _mantto: ManttoModificadoresService,
    private _menuServ: ManttoMenuService,
    private _modal: NgbModal,
    private _router: Router) {
      this.buildFormGroup();
      this.dataFilter = filterBy([this.dataGrid], this.filter);
      this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    this.getDataSource();
    this.ttInfocia = this._info.getSessionInfocia();

    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  getDataSource() {
    this.loading = true;
    this._mantto.getModificadores().subscribe(data => {
      this.dataGrid = data;
      this.dataComplete = data;
      this.dataGrid = {
        data: orderBy(this.dataGrid, this.sort),
        total: this.dataGrid.length
      };
      this.loading = false;
    })
  }

  private buildFormGroup() {
    this.myFormGroup = this._fb.group({
      Modificador: ['', [Validators.required]],
      Descripcion: ['', [Validators.required]],
      Inactivo: [false],
      cErrDesc: [''],
      lError: [false],
      cRowID: ['']
    });
  }

  private rebuildForm() {
    this.myFormGroup.reset({
      Modificador: '', Descripcion: '', Inactivo: false,
      cErrDesc: '', lError: false, cRowID: ''
    });
  }

  public newRow() {
    this.flagShowForm = true;
    this.flagEdit = false;
  }

  public editRow({ sender, rowIndex, dataItem }) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
  }

  public guardarForm() {
    if (this.flagEdit) {
      this.updateRow()
    } else {
      this.saveRow();
    }
  }

  private updateRow() {
    const form = this.myFormGroup.value;
    this._mantto.putModificador(form).subscribe(data => {
      if (data && data[0].cMensTxt) {
        this.toast(GLOBAL.textError, data[0].ttMensError.cMensTxt, 'error');
      } else {
        this.toast({
          type: 'success', title: GLOBAL.textSuccess
        });
      }
      this.closeForm();
      this.getDataSource();
    });
  }

  private saveRow() {
    const form = this.myFormGroup.value;
    this._mantto.postModificador(form).subscribe(data => {
      if (data && data[0].cMensTxt) {
        this.toast(GLOBAL.textError, data[0].ttMensError.cMensTxt, 'error');
      } else {
        this.toast({
          type: 'success', title: GLOBAL.textSuccess
        });
      }
      this.closeForm();
      this.getDataSource();
    })
  }

  public deleteRow({ dataItem }) {
    this._mantto.deleteModificador(dataItem).subscribe(data => {
      if (data && data[0].cMensTxt) {
        this.toast(GLOBAL.textError, data[0].cErrDesc, 'error');
      } else {
        this.toast({
          type: 'success',
          title: GLOBAL.textDeleteSuccess
        });
        this.closeForm();
        this.getDataSource();
      }
    });
  }

  public closeForm() {
    this.flagShowForm = false;
    this.rebuildForm();
  }

  public close(component) {
    this[component + 'Opened'] = false;
    this.closeForm();
  }

  public printPDF() {
    const encabezado = this.ttInfocia.encab;
    const columns = [
      { title: 'Modificador', dataKey: 'Modificador' },
      { title: 'Descripcion', dataKey: 'Descripcion' },
      { title: 'Inactivo', dataKey: 'Inactivo' }
    ];
    const rows = this.dataGrid.data;
    const doc = new jsPDF('p', 'pt');
    doc.setFontSize(8);
    doc.text(encabezado, 40, 30);
    doc.setFontType('normal');
    doc.setFontSize(8);
    doc.setTextColor(100);
    doc.autoTable(columns, rows, {
      theme: 'striped',
      styles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 8,
        font: 'helvetica' // helvetica, times, courier
      },
    });
    doc.output('save', 'Modificadores.pdf');
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || { logic: 'and', filters: [] };
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  openModal(content, name: string) {
    this.modalTitle = name;
    this._modal.open(content, { size: 'sm', centered: true });
  }

  public allData(): ExcelExportData {
    if (this.filter) {
      const result: ExcelExportData = {
        data: process(this.dataFilter, { sort: [{ field: 'Modificador', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    } else {
      const result: ExcelExportData = {
        data: process(this.dataGrid.data, { sort: [{ field: 'Modificador', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    }
  }

}
