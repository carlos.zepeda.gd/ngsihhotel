import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { GLOBAL } from '../../../services/global';
const headers = new Headers({'Content-Type': 'application/json'});
const esp = GLOBAL.char174;
@Injectable()
export class ManttoModificadoresService {

  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }
  getModificadores() {
    return this.http.get( this.url + '/pos/manttos/pmodificadores')
                    .map( res => res.json().response.siPmodif.dsPmodif.ttPmodif );
  }
  putModificador(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPmodif":{"ttPmodif":[' + json + ']}}';

    return this.http.put( this.url + '/pos/manttos/pmodificadores',  params, { headers: headers })
                    .map( res => res.json().response.siPmodif.dsPmodif.ttPmodif );
  }
  postModificador(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPmodif":{"ttPmodif":[' + json + ']}}';

    return this.http.post( this.url + '/pos/manttos/pmodificadores',  params, { headers: headers })
                    .map( res => res.json().response.siPmodif.dsPmodif.ttPmodif );
  }
  deleteModificador(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPmodif":{"ttPmodif":[' + json + ']}}';

    return this.http.delete( this.url + '/pos/manttos/pmodificadores', new RequestOptions({ headers: headers, body: params }))
                    .map( res => res.json().response.siPmodif.dsPmodif.ttPmodif );
  }
}
