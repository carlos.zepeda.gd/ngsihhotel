// Angular Modules
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {DragDropModule} from '@angular/cdk/drag-drop';
// Apps Modules
import {KendouiModule} from '../../shared/kendo.module';
import {ResizableModule} from 'angular-resizable-element';
import {SharedAppsModule} from '../../shared/shared-apps.module';
// Servicios
import {ManttoPreciosPuntoService} from './mantto-platillos/mantto-precios-punto/mantto-precios-punto.service';
import {ManttoTiposVentasService} from './mantto-platillos/mantto-tipos-ventas/mantto-tipos-ventas.service';
import {ManttoDesglosePlatillosService} from './mantto-desglose-platillos/mantto-desglose-platillos.service';
import {ManttoSubtiposService} from './mantto-platillos/mantto-subtipos/mantto-subtipos.service';
import {ManttoModificadoresService} from './mantto-modificadores/mantto-modificadores.service';
import {ManttoMeserosService} from './mantto-meseros/mantto-meseros.service';
import {ManttoTurnosService} from './mantto-turnos/mantto-turnos.service';
import {ManttoClasesService} from './mantto-clases/mantto-clases.service';
import {ManttoMesasService} from './mantto-mesas/mantto-mesas.service';
import {InfociaGlobalService} from '../../services/infocia.service';
// Components Module
import {ManttoPreciosPuntoComponent} from './mantto-platillos/mantto-precios-punto/mantto-precios-punto.component';
import {ManttoDesglosePlatillosComponent} from './mantto-desglose-platillos/mantto-desglose-platillos.component';
import {ManttoTiposVentasComponent} from './mantto-platillos/mantto-tipos-ventas/mantto-tipos-ventas.component';
import {ManttoSubtiposComponent} from './mantto-platillos/mantto-subtipos/mantto-subtipos.component';
import {ManttoModificadoresComponent} from './mantto-modificadores/mantto-modificadores.component';
import {ClasesTurnosComponent} from './mantto-turnos/clases-turnos/clases-turnos.component';
import {ManttoMeserosComponent} from './mantto-meseros/mantto-meseros.component';
import {ManttoTurnosComponent} from './mantto-turnos/mantto-turnos.component';
import {ManttoClasesComponent} from './mantto-clases/mantto-clases.component';
import {ManttoMesasComponent} from './mantto-mesas/mantto-mesas.component';
import {
    DetalleDesglosePlatillosComponent
} from './mantto-desglose-platillos/detalle-desglose-platillos/detalle-desglose-platillos.component';
import {MANTTOS_POS_ROUTING} from './manttos-pos.routes';
import {ManttoPlatillosComponent} from './mantto-platillos/mantto-platillos.component';
import {MesaTurnosComponent} from './mantto-mesas/mesa-turnos/mesa-turnos.component';
import {ManttoPlatillosService} from './mantto-platillos/mantto-platillos.service';
import {AddModificadorComponent} from './mantto-platillos/add-modificador/add-modificador.component';
import {AddModificadorService} from './mantto-platillos/add-modificador/add-modificador.service';
import {ManttoPuntosComponent} from './mantto-puntos/mantto-puntos.component';
import {ManttoPuntosService} from './mantto-puntos/mantto-puntos.service';
import {DetallePuntoComponent} from './mantto-puntos/detalle-punto/detalle-punto.component';
import { ArchitectModule } from 'app/_architect/architect.module';
import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedAppsModule,
        MANTTOS_POS_ROUTING,
        KendouiModule,
        DragDropModule,
        ResizableModule,
        ArchitectModule,
        GridModule,
        ExcelModule,
    ],
    declarations: [
        ManttoTurnosComponent,
        ClasesTurnosComponent,
        ManttoClasesComponent,
        ManttoMesasComponent,
        ManttoMeserosComponent,
        ManttoTiposVentasComponent,
        ManttoSubtiposComponent,
        ManttoModificadoresComponent,
        ManttoDesglosePlatillosComponent,
        DetalleDesglosePlatillosComponent,
        ManttoPreciosPuntoComponent,
        ManttoPlatillosComponent,
        MesaTurnosComponent,
        AddModificadorComponent,
        ManttoPuntosComponent,
        DetallePuntoComponent,
    ],
    providers: [
        ManttoTurnosService,
        ManttoMesasService,
        ManttoMeserosService,
        ManttoClasesService,
        ManttoSubtiposService,
        ManttoTiposVentasService,
        ManttoDesglosePlatillosService,
        ManttoPreciosPuntoService,
        ManttoModificadoresService,
        ManttoPlatillosService,
        ManttoPuntosService,
        InfociaGlobalService,
        AddModificadorService
    ]
})
export class ManttosPosModule {
}
