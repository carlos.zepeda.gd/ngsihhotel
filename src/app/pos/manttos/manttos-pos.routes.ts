import { RouterModule, Routes } from '@angular/router';
import { ManttoPreciosPuntoComponent } from './mantto-platillos/mantto-precios-punto/mantto-precios-punto.component';
import { ManttoDesglosePlatillosComponent } from './mantto-desglose-platillos/mantto-desglose-platillos.component';
import { ManttoTiposVentasComponent } from './mantto-platillos/mantto-tipos-ventas/mantto-tipos-ventas.component';
import { ManttoSubtiposComponent } from './mantto-platillos/mantto-subtipos/mantto-subtipos.component';
import { ManttoUsuariosComponent } from '../../pms/manttos/mantto-usuarios/mantto-usuarios.component';
import { ManttoModificadoresComponent } from './mantto-modificadores/mantto-modificadores.component';
import { ManttoRolesComponent } from '../../pms/manttos/mantto-roles/mantto-roles.component';
import { ManttoMenuComponent } from '../../pms/manttos/mantto-menu/mantto-menu.component';
import { ManttoPlatillosComponent } from './mantto-platillos/mantto-platillos.component';
import { ManttoMeserosComponent } from './mantto-meseros/mantto-meseros.component';
import { ManttoTurnosComponent } from './mantto-turnos/mantto-turnos.component';
import { ManttoClasesComponent } from './mantto-clases/mantto-clases.component';
import { ManttoMesasComponent } from './mantto-mesas/mantto-mesas.component';
import { ManttoPuntosComponent } from './mantto-puntos/mantto-puntos.component';
import { BaseLayoutComponent } from '../../_architect/Layout/base-layout/base-layout.component';
import { VerifyInfociaGuardService } from '../../services/verify-infocia-guard.service';
import { RoutesGuard } from '../../services/routes-guard.service';

const MANTTOS_POS_ROUTES: Routes = [
    {
        path: '',
        component: BaseLayoutComponent,
        canActivateChild: [VerifyInfociaGuardService],
        children: [
            { path: 'mantto-desglose-platillos', component: ManttoDesglosePlatillosComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'mantto-modificadores', component: ManttoModificadoresComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'mantto-precios-punto', component: ManttoPreciosPuntoComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'mantto-tipos-venta', component: ManttoTiposVentasComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'mantto-platillos', component: ManttoPlatillosComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'mantto-usuarios', component: ManttoUsuariosComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'mantto-subtipos', component: ManttoSubtiposComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'mantto-meseros', component: ManttoMeserosComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'mantto-clases', component: ManttoClasesComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'mantto-turnos', component: ManttoTurnosComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'mantto-puntos', component: ManttoPuntosComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'mantto-mesas', component: ManttoMesasComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'mantto-roles', component: ManttoRolesComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'mantto-menu', component: ManttoMenuComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } }
        ],

    }
];

export const MANTTOS_POS_ROUTING = RouterModule.forChild(MANTTOS_POS_ROUTES);
