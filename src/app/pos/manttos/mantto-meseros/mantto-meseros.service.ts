import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from '../../../services/global';
import 'rxjs/add/operator/map';

const headers = new Headers({'Content-Type': 'application/json'});

@Injectable()
export class ManttoMeserosService {

  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  getMeseros() {
    return this.http.get( this.url + '/pos/manttos/meseros')
                    .map( res => res.json().response.siPmesero.dsPmeseros.ttPmeseros);
}
getMesero() {
  return this.http.get( this.url + '/pos/manttos/meseros')
                  .map( res => res.json().response.siPmesero.dsPmeseros.ttPMeseros );
}

postMesero(mes) {
  const json = JSON.stringify(mes);
  const params = '{"ttPmeseros":[' + json + ']}';
  return this.http.post( this.url + '/pos/manttos/meseros', params, { headers: headers })
                  .map( (res: Response) => {
                    const succes = res.json().response.siPmesero.dsPmeseros.ttPmeseros;
                    const error  = res.json().response.siPmesero.dsMensajes.ttMensError;
                      if (error) {
                        return error;
                      } else {
                        return succes;
                      }
                  });
}
putMeseros(mes) {
  const json = JSON.stringify(mes);
  const params = '{"ttPmeseros":[' + json + ']}';
  return this.http.put( this.url + '/pos/manttos/meseros', params, { headers: headers })
                  .map( (res: Response) => {
                      const succes = res.json().response.siPmesero.dsPmeseros.ttPmeseros;
                      const error  = res.json().response.siPmesero.dsMensajes.ttMensError;
                      if (error) {
                        return error;
                      }  else {
                        return succes;
                      }
                  });
}
deleteMesero(mes) {
  const json = JSON.stringify(mes);
  const params = '{"ttPmeseros":[' + json + ']}';
  const options = new RequestOptions({ headers: headers });
  return this.http.delete( this.url + '/pos/manttos/meseros', { body: params, headers } )
                  .map( (res: Response) => {
                        const data = res.json().response.siPmesero;
                        if (data.dsMensajes.ttMensError) {
                         return data.dsMensajes.ttMensError[0];
                        } else {
                          return data.dsPmeseros.ttPmeseros;
                        }
                  });
  }
}
