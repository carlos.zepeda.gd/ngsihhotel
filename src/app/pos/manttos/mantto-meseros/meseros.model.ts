export class Meseros {
    constructor(
        public Mesero: string,
        public mnombre: string,
        public Inactivo: false,
        public cRowID: string,
        public cErrDesc = '',
        public lError = false,
    ) {}
}

