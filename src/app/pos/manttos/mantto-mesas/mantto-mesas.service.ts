import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from '../../../services/global';
import 'rxjs/add/operator/map';
const esp = GLOBAL.char174;
const headers = new Headers({'Content-Type': 'application/json'});

@Injectable()
export class ManttoMesasService {
  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }
  
  getMesas() {
    return this.http.get( this.url + '/pos/manttos/mesas')
                    .map( res => res.json().response.siPmesa.dsPmesas.ttPmesas );
                  
}
getMesa() {
  return this.http.get( this.url + '/pos/manttos/mesas')
                  .map( res => res.json().response.siPmesa.dsPmesas.ttPmesas );
}
putMesas(mesa) {
  const json = JSON.stringify(mesa);
  const params = '{"ttPmesas":[' + json + ']}';
  return this.http.put( this.url + '/pos/manttos/mesas', params, { headers: headers })
                  .map( (res: Response) => {
                      const succes = res.json().response.siPmesa.dsPmesas.ttPmesas;
                      const error  = res.json().response.siPmesa.dsMensajes.ttMensError;
                      if (error) {
                        return error;
                      }  else {
                        return succes;
                      }
                  });
}
  postMesa(mesa) {
    const json = JSON.stringify(mesa);
    const params = '{"ttPmesas":[' + json + ']}';
    return this.http.post( this.url + '/pos/manttos/mesas', params, { headers: headers })
                    .map( (res: Response) => {
                        const error  = res.json().response.siPmesa.dsMensajes.ttMensError;
                        const succes = res.json().response.siPmesa.dsPmesas.ttPmesas;
                        if (error) {
                          return error;
                        } else {
                          return succes;
                        }
                    });
  }
  deleteMesa(mes) {
    const json = JSON.stringify(mes);
    const params = '{"ttPmesas":[' + json + ']}';
    const options = new RequestOptions({ headers: headers });
    return this.http.delete( this.url + '/pos/manttos/mesas', { body: params, headers } )
                    .map( (res: Response) =>  res.json().response.siPmesa.dsPmesas.ttPmesas);
    }
  getMesaTurno(punto, mesa) {
    return this.http.get( this.url + '/pos/manttos/mesatur/' + punto + esp + mesa  )
                    .map( res => res.json().response.siPmesatur.dsPmesatur.ttPmesatur );
  }
  deleteMesaTurno(mes) {
    const json = JSON.stringify(mes);
    const params = '{"ttPmesatur":[' + json + ']}';
    const options = new RequestOptions({ headers: headers });
    return this.http.delete( this.url + '/pos/manttos/mesatur', { body: params, headers } )
                    .map( (res: Response) => {
                          const data = [];
                          if (!res) {
                          return data;
                          } else {
                            const error = res.json().response.siPmesatur.dsMensajes.ttMensError;
                            const success = res.json().response.siPmesatur.dsPmesatur.ttPmesatur;
                            if (error) { return error; } else { return success; }
                          }
                    });
  }
  postMesaTurno(mesa) {
    const json = JSON.stringify(mesa);
    const params = '{"ttPmesatur":[' + json + ']}';
    return this.http.post( this.url + '/pos/manttos/mesatur', params, { headers: headers })
                    .map( (res: Response) =>  res.json().response.siPmesatur.dsPmesatur.ttPmesatur);
  }
}
