export class Mesas {
  constructor(
    public Punto: number,
    public cMesa: number,
    public cMdesc: string,
    public iMcap: number,
    public iOldMesa: number,
    public Inactivo: boolean,
    public cRowID: string,
    public cErrDesc = '',
    public lError = false
  ) {}
  }
