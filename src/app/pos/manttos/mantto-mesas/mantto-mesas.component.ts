import { Component, OnInit, ViewChild, TemplateRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SortDescriptor, CompositeFilterDescriptor, GroupDescriptor } from '@progress/kendo-data-query';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ResizeEvent } from 'angular-resizable-element';
import { ChequeAyudaService } from '../../puntos-venta/cheque/services/cheque-ayuda.service';
import { ManttoPuntosService } from '../mantto-puntos/mantto-puntos.service';
import { ManttoMenuService } from '../../../pms/manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { ManttoMesasService } from './mantto-mesas.service';
import { GLOBAL } from 'app/services/global';
declare var jsPDF: any;

@Component({
    selector: 'app-mesas',
    templateUrl: './mantto-mesas.component.html',
    styleUrls: ['./mantto-mesas.component.css']
})
export class ManttoMesasComponent implements OnInit, OnDestroy {
    heading = 'Mantenimiento de Mesas';
    subheading = 'Point Of Sale.';
    icon = 'icon-gradient bg-tempting-azure';
    @ViewChild('modalMapa') modalPlano: TemplateRef<any>;
    public myFormGroup: FormGroup;
    public dataGrid: any = [];
    public dataComplete: any = [];
    public dsPuntos: any = [];
    public ttInfocia: any = [];
    public flagEdit: boolean;
    public groups: GroupDescriptor[] = [];
    public filter: CompositeFilterDescriptor;
    public sort: SortDescriptor[] = [{ field: 'Punto', dir: 'asc' }];
    public dataExand: any;
    public flagMapaMesas: boolean = true;
    public puntoSelected: any;
    public dataPlanoMesas: any = [];
    public flagMoverMesa: boolean = true;
    public flagResizeMesa: boolean;
    public planoX: number;
    public planoY: number;
    public planoBg: string = 'plano-mesas.jpg';
    public switchInactivo: boolean = true;
    public loading: boolean;
    public swal = GLOBAL.swal;

    constructor(public _info: InfociaGlobalService,
        private _pf: FormBuilder,
        private _mantto: ManttoMesasService,
        private _punto: ManttoPuntosService,
        private _cheque: ChequeAyudaService,
        private _menuServ: ManttoMenuService,
        private _config: NgbModalConfig,
        private _modalService: NgbModal,
        private _router: Router) {
        this._config.backdrop = 'static';
        this._config.keyboard = false;
        this.buildFormGroup();
        this.ttInfocia = this._info.getSessionInfocia();
        this._cheque.getPuntos().subscribe(data => this.dsPuntos = data);
        this.getDataSource();
    }
    ngOnDestroy(): void {
        this._modalService.dismissAll();
    }

    ngOnInit() {
        this.openModal(this.modalPlano, 'xl');
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    btnMapaMesas() {
        this.openModal(this.modalPlano, 'xl');
        this.flagMapaMesas = true;
        this.getDataSource();
    }

    changeTemplate() {
        const cImagen = [this.planoBg, this.planoX, this.planoY];
        this.puntoSelected.cImagen = cImagen.join(',');
        this.guardarTemplatePunto(this.puntoSelected);
    }

    buscarPlano(mesas?) {
        this.dataPlanoMesas = [];
        if (this.puntoSelected) {
            if (this.puntoSelected.cImagen && this.puntoSelected.cImagen.length !== 3) {
                this.puntoSelected.cImagen = this.puntoSelected.cImagen.split(',');
            }
            if (this.puntoSelected.cImagen[0]) {
                this.planoBg = this.puntoSelected.cImagen[0];
            } else {
                this.planoBg = 'plano-mesas.jpg';
            }
            if (this.puntoSelected.cImagen[1]) {
                this.planoX = Number(this.puntoSelected.cImagen[1]);
            }
            if (this.puntoSelected.cImagen[2]) {
                this.planoY = Number(this.puntoSelected.cImagen[2]);
            }
            if (mesas) {

            } else {
                this._cheque.getMesas(this.puntoSelected.Punto, '', true).subscribe(data => {
                    if (data) {
                        data.forEach(item => {
                            if (item.cCoordenada) {
                                item.cCoordenada = item.cCoordenada.split(',');
                            } else {
                                item.cCoordenada = [0, 0, 45, 45];
                            }
                        });
                        this.dataPlanoMesas = data;
                    } else {
                        this.dataPlanoMesas = [];
                    }
                });
            }

        }
    }


    onResizeEndTemplate(event: ResizeEvent) {
        this.planoX = event.rectangle.width;
        this.planoY = event.rectangle.height;
        const cImagen = [this.planoBg, this.planoX, this.planoY];
        this.puntoSelected.cImagen = cImagen.join(',');
        this.guardarTemplatePunto(this.puntoSelected);
    }

    guardarTemplatePunto(punto) {
        this._punto.putPunto(punto).subscribe(data => {
            this.buscarPlano();
        });
    }

    onDragEnded(event, dataItem) {
        const element = event.source.getRootElement();
        const boundingClientRect = element.getBoundingClientRect();
        const parentPosition = this.getPosition(element);
        const oldX: number = Number(dataItem.cCoordenada[0]);
        const oldY: number = Number(dataItem.cCoordenada[1]);
        const oldW: number = Number(dataItem.cCoordenada[2]);
        const oldH: number = Number(dataItem.cCoordenada[3]);
        let x: number = 0;
        let y: number = 0;
        const sum1 = oldX + Number(boundingClientRect.x) - Number(parentPosition.left);
        const sum2 = oldY + Number(boundingClientRect.y) - Number(parentPosition.top);
        if (sum1 >= 0) {
            x = sum1;
        }
        if (sum2 >= 0) {
            y = sum2;
        }
        const mesa = JSON.parse(JSON.stringify(dataItem));
        mesa.cCoordenada = `${x},${y},${oldW},${oldH}`;
        this.guardarPosicion(mesa);
    }

    onResizeEnd(event: ResizeEvent, dataItem): void {
        dataItem.cCoordenada[0] = Number(dataItem.cCoordenada[0]);
        dataItem.cCoordenada[1] = Number(dataItem.cCoordenada[1]);
        dataItem.cCoordenada[2] = Number(event.rectangle.width);
        dataItem.cCoordenada[3] = Number(event.rectangle.height);
        const mesa = JSON.parse(JSON.stringify(dataItem));
        mesa.cCoordenada = mesa.cCoordenada.join(',');
        this.guardarPosicion(mesa);
    }

    changeAction(e) {
        this.buscarPlano();
        if (e === 'r') {
            this.flagMoverMesa = false;
            this.flagResizeMesa = true;
        } else {
            this.flagMoverMesa = true;
            this.flagResizeMesa = false;
        }
    }
    guardarPosicion(mesa) {
        this._mantto.putMesas(mesa).subscribe(data => {
            if (this.dataPlanoMesas) {
                this.dataPlanoMesas.forEach(item => {
                    if (item.cRowID === data[0].cRowID) {
                        data[0].cCoordenada = data[0].cCoordenada.split(',');
                        item = data[0];
                    }
                });
            } else {
                this.buscarPlano();
            }
        });
    }

    getPosition(el) {
        let x = 0;
        let y = 0;
        while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
            x += el.offsetLeft - el.scrollLeft;
            y += el.offsetTop - el.scrollTop;
            el = el.offsetParent;
        }
        return { top: y, left: x };
    }

    private getDataSource() {
        this.loading = true;
        this._mantto.getMesas().subscribe(data => {
            if (data) {
                data.forEach(item => {
                    if (item.cCoordenada) {
                        item.cCoordenada = item.cCoordenada.split(',');
                    }
                });
                this.dataGrid = data;
            }
            this.loading = false;
        });
    }

    private buildFormGroup() {
        this.myFormGroup = this._pf.group({
            Punto: [0, Validators.required],
            cMesa: ['', Validators.required],
            cMdesc: [''],
            iMcap: [0],
            iOldMesa: [0],
            cCoordenada: [''],
            Inactivo: [false],
            cRowID: [''],
            cErrDesc: [''],
            lError: [false]
        });
    }

    private rebuildFormGroup() {
        this.myFormGroup.reset({
            Punto: 0, cMesa: 0, cMdesc: '', iMcap: 0, iOldMesa: 0,
            Inactivo: false, cRowID: '', cErrDesc: '', lError: false
        });
    }

    public newRow(modal) {
        if (this.puntoSelected) {
            this.myFormGroup.patchValue({ Punto: this.puntoSelected });
        }
        this.flagEdit = false;
        this.openModal(modal, 'md');
    }

    public editRow(dataItem, modal) {
        this.myFormGroup.setValue(dataItem);
        this.flagEdit = true;
        this.openModal(modal, 'md');
    }

    public editGridRow({ dataItem }, modal) {
        this.editRow(dataItem, modal);
    }

    public guardarForm() {
        if (this.flagEdit) {
            this.updateForm();
        } else {
            this.saveRow();
        }
    }

    private updateForm() {
        const form = JSON.parse(JSON.stringify(this.myFormGroup.value));
        form.cCoordenada = form.cCoordenada.join(',');
        if (form.Punto.Punto) {
            form.Punto = form.Punto.Punto
        }
        ;
        this._mantto.putMesas(form).subscribe(data => {
            if (this.flagMapaMesas) {
                this.buscarPlano();
            } else {
                this.getDataSource();
            }
            this.closeForm();
        });
    }

    private saveRow() {
        const form = JSON.parse(JSON.stringify(this.myFormGroup.value));
        if (form.Punto.Punto) {
            form.Punto = form.Punto.Punto
        }
        ;
        if (this.dataPlanoMesas.length) {
            // form.cCoordenada = this.dataPlanoMesas[this.dataPlanoMesas.length - 1].cCoordenada.join(',');
            form.cCoordenada = '0,0,45,45';
        } else {
            form.cCoordenada = '0,10,45,45';
        }
        this._mantto.postMesa(form).subscribe(data => {
            if (data[0].cMensTxt) {
                this.swal('Error!', data[0].cMensTxt, 'error');
            } else {
                if (this.flagMapaMesas) {
                    this.buscarPlano();
                }
                this.getDataSource();
                this.closeForm();
            }
        });
    }

    public deleteGridRow({ dataItem }) {
        this.deleteRow(dataItem);
    }

    public deleteRow(dataItem) {
        dataItem.cCoordenada = dataItem.cCoordenada.join(',');
        this._mantto.deleteMesa(dataItem).subscribe(data => {
            if (data && data[0].cErrDesc) {
                this.swal({
                    type: 'error', title: 'Transacción no realizada!!!',
                    text: data[0].cErrDesc
                });
            } else {
                this.buscarPlano();
                this.getDataSource();
                this.closeForm();
            }
        });
    }

    public closeForm() {
        this.flagEdit = false;
        this.rebuildFormGroup();
        this.getDataSource();
    }

    openModal(modal, size) {
        this.flagMapaMesas = true;
        this._modalService.open(modal, { size: size, centered: true });
    }

    closeMapa() {
        this.flagMapaMesas = false;
        this.puntoSelected = null;
        this.getDataSource();
    }

    public printPDF() {
        const encabezado = this.ttInfocia.encab;
        const columns = [
            { title: 'Punto', dataKey: 'Punto' },
            { title: 'Mesa', dataKey: 'cMesa' },
            { title: 'Ubicacion', dataKey: 'cMdesc' },
            { title: 'Capacidad', dataKey: 'iMcap' },
            { title: 'Inactivo', dataKey: 'Inactivo' }
        ];
        const rows = this.dataGrid;
        const doc = new jsPDF('p', 'pt');
        doc.setFontSize(8);
        doc.text(encabezado, 40, 30);
        doc.setFontType('normal');
        doc.setFontSize(8);
        doc.setTextColor(100);
        doc.autoTable(columns, rows, {
            theme: 'striped',
            styles: {
                cellPadding: 5, // a number, array or object (see margin below)
                fontSize: 8,
                font: 'helvetica' // helvetica, times, courier
            },
        });
        doc.output('save', 'Mesas.pdf');
    }

    public detailExpand({ dataItem }) {
        this.dataExand = dataItem;
    }

}
