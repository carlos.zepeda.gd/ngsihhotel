import { Component, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { ManttoTurnosService } from '../../mantto-turnos/mantto-turnos.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ManttoMesasService } from '../mantto-mesas.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-mesa-turnos',
  templateUrl: './mesa-turnos.component.html',
  styleUrls: ['./mesa-turnos.component.css']
})
export class MesaTurnosComponent implements OnInit, OnChanges {
  @Input() data: any = [];
  public dataGrid: any = [];
  public dsTurnos: any = [];
  public dsTurnosComplete: any = [];
  public myFormGroup: FormGroup;
  public flagEdit: any;
  public flagShowForm: boolean;

  constructor(
    private _turno: ManttoTurnosService, 
    private _mantto: ManttoMesasService,
    private fb: FormBuilder
  ) {
    this.buildFormGroup();
   }

  ngOnInit() {
    this._turno.getTurnos().subscribe(data => {
      this.dsTurnos = data;
      this.dsTurnosComplete = data;
    });
  }
  getDataSource() {
    this._mantto.getMesaTurno(this.data.Punto , this.data.cMesa).subscribe(data => this.dataGrid = data);
  }
  ngOnChanges(change: SimpleChanges) {
    if (change.data) {
      this.getDataSource();
    }
  }
  private buildFormGroup() {
    this.myFormGroup = this.fb.group ({
      Punto:      [0],
      cMesa:      [''],
      Turno:      [0],
      OldMesa:    [0],
      cRowID:     [''],
      cErrDesc:   [''],
      lError:     [false]
    });
  }
  private rebuildForm() {
    this.myFormGroup.reset({ 
      Punto: 0, cMesa: '', Turno: 0, OldMesa: 0, cRowID: '', tdesc: '', cErrDes: '', lError: false });
  }
  newRow() {
    this.flagShowForm = true;
    this.flagEdit = false;
    this.rebuildForm();
  }
  public editRow({sender, rowIndex, dataItem}) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
  }
  public deleteRow({dataItem}) {
    this._mantto.deleteMesaTurno(dataItem).subscribe(data => this.cerrarForm());
  }
  guardarForm() {
      this.saveRow();
  }
  saveRow() {
    const form = this.myFormGroup.value;
    form.cMesa = this.data.cMesa;
    form.Punto = this.data.Punto;
    if (form.Turno.Turno) { form.Turno = form.Turno.Turno; }
    this._mantto.postMesaTurno(form).subscribe(data => {
      if (data[0].cErrDesc) {
        swal({
          position: 'center',
          type: 'error',
          title: 'Error!!',
          text: data[0].cErrDesc,
          showConfirmButton: false,
          timer: 2000
        })
      }else {
        swal({
          position: 'center',
          type: 'success',
          title: 'Registro guardado!!',
          showConfirmButton: false,
          timer: 2000
        })
      }
      this.cerrarForm();
    })
  }
  cerrarForm() {
    this.flagEdit = false;
    this.flagShowForm = false;
    this.rebuildForm();
    this.getDataSource();
  }
}
