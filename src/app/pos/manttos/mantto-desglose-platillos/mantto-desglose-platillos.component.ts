import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import {ManttoDesglosePlatillosService} from './mantto-desglose-platillos.service';
import {InfociaGlobalService} from '../../../services/infocia.service';
import swal from 'sweetalert2';
import {ManttoMenuService} from '../../../pms/manttos/mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import { GLOBAL } from '../../../services/global';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';

declare var jsPDF: any;
const flatten = GLOBAL.flatten;

@Component({
    selector: 'app-mantto-desglose-platillos',
    templateUrl: './mantto-desglose-platillos.component.html',
    styleUrls: ['./mantto-desglose-platillos.component.css']
})

export class ManttoDesglosePlatillosComponent implements OnInit {
    heading = 'Mantenimiento Desglose de Platillos';
    subheading = 'Point Of Sale.';
    icon = 'icon-gradient bg-tempting-azure';

    noRecords = GLOBAL.noRecords;

    public myFormGroup: FormGroup;
    public dataGrid: any = [];
    public dataComplete: any = [];
    public ttInfocia: any = [];
    public dataExpand: any = [];
    flagEdit: boolean;
    public flagShowForm: boolean;
    public checked: boolean;
    public groups: GroupDescriptor[] = [];
    public filter: CompositeFilterDescriptor;
    public sort: SortDescriptor[] = [{field: 'dplat', dir: 'asc'}];
    modalTitle = '';
    toast = GLOBAL.toast;
    dataFilter: any[];
    loading = false;

    constructor(private fb: FormBuilder,
                private _info: InfociaGlobalService,
                private _mantto: ManttoDesglosePlatillosService,
                private _menuServ: ManttoMenuService,
                private _modal: NgbModal,
                private _router: Router) {
      this.dataFilter = filterBy([this.dataGrid], this.filter);
      this.allData = this.allData.bind(this);
    }

    ngOnInit() {
        this.ttInfocia = this._info.getSessionInfocia();
        this.buildFormGroup();
        this.getDataSource();

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    private buildFormGroup() {
        this.myFormGroup = this.fb.group({
            dplat: ['', [Validators.required]],
            ddesc: ['', [Validators.required]],
            duso: [false, [Validators.required]],
            Inactivo: [false, [Validators.required]],
            cErrDesc: [''],
            lError: [false],
            cRowID: ['']
        });
    }

    private rebuildForm() {
        this.myFormGroup.reset({dplat: '', ddesc: '', duso: false, Inactivo: false, cErrDes: '', lError: false, cRowID: ''});
    }

    private getDataSource() {
      this.loading = true;
        this._mantto.getDesgloses().subscribe(data => {
            if (data) {
                this.dataGrid = data;
                this.dataComplete = data;
                this.dataGrid = {
                    data: orderBy(this.dataGrid, this.sort),
                    total: this.dataGrid.length
                };
            }
        });
        this.loading = false;
    }

    public newRecord() {
        this.flagShowForm = true;
        this.flagEdit = false;
    }

    public guardarForm() {
        if (this.flagEdit) {
            this.updateRow()
        } else {
            this.saveRow();
        }
    }

    private saveRow() {
      this.loading = true;
        const form = this.myFormGroup.value;
        this._mantto.postDesglose(form).subscribe(data => {
            if (data[0].cMensTxt) {
                this.toast(GLOBAL.textError, data[0].cMensTxt, 'error');
            } else {
                this.toast({
                    type: 'success',
                    title: GLOBAL.textSuccess
                })
                this.rebuildForm();
                this.getDataSource();
                this.closeForm();
            }
        })
    }

    public deleteRow({dataItem}) {
      this.loading = true;
        this._mantto.deleteDesglose(dataItem).subscribe(data => {
            if (!data) {
                this.toast({
                  type: 'success',
                  title: GLOBAL.textDeleteSuccess
                })
                this.closeForm();
                this.rebuildForm();
                this.getDataSource();
            } else {
                if (data[0].cMensTxt !== undefined) {
                    this.toast(data[0].cMensTxt, data[0].cMensTit, 'error');
                    return;
                }

            }
            this.closeForm();
        });
    }

    public editRow({sender, rowIndex, dataItem}) {
        this.myFormGroup.setValue(dataItem);
        this.flagEdit = true;
        this.flagShowForm = true;
    }

    private updateRow() {
      this.loading = true;
        const form = this.myFormGroup.value;
        this._mantto.putDesglose(form).subscribe(data => {
          if (data[0].cMensTxt) {
              this.toast(GLOBAL.textError, data[0].cMensTxt, 'error');
          } else {
              this.toast({
                  type: 'success',
                  title: GLOBAL.textSuccess
              })
              this.rebuildForm();
              this.getDataSource();
              this.closeForm();
          }
        });
    }

    public detailExpand({dataItem}) {
        this.dataExpand = dataItem;
    }

    public close(component) {
        this[component + 'Opened'] = false;
        this.closeForm();
    }

    public closeForm() {
        this.flagShowForm = false;
        this.flagEdit = false;
    }

    public printPDF() {
        const encabezado = this.ttInfocia.encab;
        const columns = [
            {title: 'Desglose', dataKey: 'dplat'},
            {title: 'Descripción', dataKey: 'ddesc'},
            {title: 'Uso', dataKey: 'duso'},
            {title: 'Inactivo', dataKey: 'Inactivo'}
        ];
        const rows = this.dataGrid.data;
        const doc = new jsPDF('p', 'pt');
        doc.setFontSize(8);
        doc.text(encabezado, 40, 30);
        doc.setFontType('normal');
        doc.setFontSize(8);
        doc.setTextColor(100);
        doc.autoTable(columns, rows, {
            theme: 'striped',
            styles: {
                cellPadding: 5, // a number, array or object (see margin below)
                fontSize: 8,
                font: 'helvetica' // helvetica, times, courier
            },
        });
        doc.output('save', 'Desglose de platillos.pdf');
    }

    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.dataGrid = process(this.dataComplete, {group: this.groups});
    }

    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.dataGrid.data = filterBy(this.dataComplete, filter);
    }

    public switchChange(checked: boolean): void {
        const root = this.filter || {logic: 'and', filters: []};
        const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
        if (!filter) {
            root.filters.push({
                field: 'Inactivo',
                operator: 'eq',
                value: checked
            });
        } else {
            filter.value = checked;
        }
        this.checked = checked;
        this.filterChange(root);
    }

    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.dataGrid = {
            data: orderBy(this.dataGrid.data, this.sort),
            total: this.dataGrid.data.length
        };
    }

    openModal(content, name: string) {
      this.modalTitle = name;
      this._modal.open(content, { size: 'sm', centered: true });
    }

    public allData(): ExcelExportData {
      if (this.filter) {
        const result: ExcelExportData = {
          data: process(this.dataFilter, { sort: [{ field: 'dplat', dir: 'asc' }], filter: this.filter }).data
        };
        return result;
      } else {
        const result: ExcelExportData = {
          data: process(this.dataGrid.data, { sort: [{ field: 'dplat', dir: 'asc' }], filter: this.filter }).data
        };
        return result;
      }
    }

}
