import { Component, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { ManttoDesglosePlatillosService } from '../mantto-desglose-platillos.service';
import { ManttoTurnosService } from '../../mantto-turnos/mantto-turnos.service';
import swal from 'sweetalert2';
import { GLOBAL } from 'app/services/global';
import { CompositeFilterDescriptor, filterBy } from '@progress/kendo-data-query';
declare var jsPDF: any;
const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-detalle-desglose-platillos',
  templateUrl: './detalle-desglose-platillos.component.html',
  styleUrls: ['./detalle-desglose-platillos.component.css']
})
export class DetalleDesglosePlatillosComponent implements OnInit, OnChanges {
  @Input() data: any = [];
  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public dataComplete: any = [];
  public dsCodigos: any = [];
  public dsTipoMovimiento: any = [
    { tipo: 'A', desc: 'Alimentos' },
    { tipo: 'B', desc: 'Bebidas'   },
    { tipo: 'V', desc: 'Varios'    },
    { tipo: 'S', desc: 'Servicios' }
  ];
  public dsCodigosComplete: any = [];
  flagEdit: boolean;
  public flagShowForm: boolean;
  toast = GLOBAL.toast;
  public filter: CompositeFilterDescriptor;
  public checked: boolean;

  constructor(
    private fb: FormBuilder,
    private _mantto: ManttoDesglosePlatillosService,
    private _turno: ManttoTurnosService
  ) { }

  ngOnInit() {
    this.buildFormGroup();
    this._turno.getCodigos('D').subscribe(data => {
      this.dsCodigos = data;
      this.dsCodigosComplete = data;
    });
  }
  ngOnChanges(change: SimpleChanges) {
    if (change.data) {
      this.getDataSource();
    }
  }
  getDataSource() {
    this._mantto.getDetalleDesgloses(this.data.dplat).subscribe(data => {
      this.dataGrid = data;
      this.dataComplete = data;
    });
  }
  newRecord() {
    this.flagShowForm = true;
    this.flagEdit = false;
    this.rebuildForm();
  }
  guardarForm() {
    if (this.flagEdit) {
      this.updateForm()
    }else {
      this.saveRow();
    }
  }
  public editRow({sender, rowIndex, dataItem}) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
  }
  updateForm() {
    const form = this.myFormGroup.value;
    if (form.codigo.Codigo) { form.codigo = form.codigo.Codigo }
    if (form.tmov.tipo) { form.tmov = form.tmov.tipo }
    this._mantto.putDetalleDesglose(form).subscribe(data => this.closeForm());
  }
  saveRow() {
    const form = this.myFormGroup.value;
    if (form.codigo.Codigo) { form.codigo = form.codigo.Codigo }
    if (form.tmov.tipo) { form.tmov = form.tmov.tipo }

    this._mantto.postDetalleDesglose(form).subscribe(data => {
      if (data[0].cErrDesc) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError
        })
      }else {
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        })
      }
      this.closeForm();
    })
  }
  public deleteRow({dataItem}) {
    this._mantto.deleteDetalleDesglose(dataItem).subscribe(data => this.closeForm());
  }
  buildFormGroup() {
    this.myFormGroup = this.fb.group ({
      dplat:      [''],
      cDdesc:     [''],
      tmov:       ['',    Validators.required ],
      codigo:     ['',    Validators.required ],
      cCdesc:     [''],
      Monto:      [0,     Validators.required ],
      dporc:      [false, Validators.required ],
      cErrDesc:   [''],
      lError:     [false],
      cRowID:     ['']
    });
  }
  rebuildForm() {
    this.myFormGroup.reset({
      dplat:  this.data.dplat, cDdesc: '', tmov: '', codigo: '', cCdesc: '',
      Monto: 0, dporc: false, cErrDes: '', lError: false, cRowID: ''
    });
  }
  closeForm() {
    this.flagEdit = false;
    this.flagShowForm = false;
    this.rebuildForm();
    this.getDataSource();
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || {logic: 'and', filters: []};
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
        root.filters.push({
            field: 'Inactivo',
            operator: 'eq',
            value: checked
        });
    } else {
        filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
}

public filterChange(filter: CompositeFilterDescriptor): void {
  this.filter = filter;
  this.dataGrid.data = filterBy(this.dataComplete, filter);
}

}
