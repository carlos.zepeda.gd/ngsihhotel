import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { GLOBAL } from '../../../services/global';
const headers = new Headers({ 'Content-Type': 'application/json' });
const esp = GLOBAL.char174;
@Injectable()
export class ManttoDesglosePlatillosService {

  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }
  getDesgloses() {
    return this.http.get(this.url + '/pos/manttos/pdesgloses').map(res => {
      const success = res.json().response.siPdesglose.dsPdesgloses.ttPdesgloses;
      if (success) {
        return success;
      } else {
        return [];
      }
    });
  }
  putDesglose(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPdesgloses":{"ttPdesgloses":[' + json + ']}}';

    return this.http.put(this.url + '/pos/manttos/pdesgloses', params, { headers: headers })
      .map(res => {
        const error = res.json().response.siPdesglose.dsMensajes.ttMensError;
        const success = res.json().response.siPdesglose.dsPdesgloses.ttPdesgloses;
        if (error) {
          return error;
        } else {
          return success;
        }
      });
  }
  postDesglose(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPdesgloses":{"ttPdesgloses":[' + json + ']}}';

    return this.http.post(this.url + '/pos/manttos/pdesgloses', params, { headers: headers })
      .map(res => {
        const error = res.json().response.siPdesglose.dsMensajes.ttMensError;
        const success = res.json().response.siPdesglose.dsPdesgloses.ttPdesgloses;
        if (error) {
          return error;
        } else {
          return success;
        }
      });
  }

  deleteDesglose(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPdesgloses":{"ttPdesgloses":[' + json + ']}}';

    return this.http.delete(this.url + '/pos/manttos/pdesgloses', new RequestOptions({ headers: headers, body: params }))
      .map(res => {
        if (res.json().response.siPdesglose.dsPdesgloses === {}) {
          return [];
        }
        const error = res.json().response.siPdesglose.dsMensajes.ttMensError;
        const success = res.json().response.siPdesglose.dsPdesgloses.ttPdesgloses;
        if (error) {
          return error
        } else {
          return success;
        }
      });
  }

  // DETALLE DESGLOSE DE PLATILLO

  getDetalleDesgloses(desglose) {
    return this.http.get(this.url + '/pos/manttos/pdesgdet/' + desglose)
      .map(res => res.json().response.siPdesgdet.dsPdesgdet.ttPdesgdet);
  }
  putDetalleDesglose(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPdesgdet":{"ttPdesgdet":[' + json + ']}}';

    return this.http.put(this.url + '/pos/manttos/pdesgdet', params, { headers: headers })
      .map(res => res.json().response.siPdesgdet.dsPdesgdet.ttPdesgdet);
  }
  postDetalleDesglose(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPdesgdet":{"ttPdesgdet":[' + json + ']}}';

    return this.http.post(this.url + '/pos/manttos/pdesgdet', params, { headers: headers })
      .map(res => res.json().response.siPdesgdet.dsPdesgdet.ttPdesgdet);
  }
  deleteDetalleDesglose(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPdesgdet":{"ttPdesgdet":[' + json + ']}}';

    return this.http.delete(this.url + '/pos/manttos/pdesgdet', new RequestOptions({ headers: headers, body: params }))
      .map(res => res.json().response.siPdesgdet.dsPdesgdet.ttPdesgdet);
  }
}
