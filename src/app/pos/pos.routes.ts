import { RouterModule, Routes } from '@angular/router';
import { BaseLayoutComponent } from '../_architect/Layout/base-layout/base-layout.component';
import { PerfilUserComponent } from '../components/perfil-user/perfil-user.component';
import { HomePosComponent } from './components/home/home-pos.component';
import { ChequeComponent } from './puntos-venta/cheque/cheque.component';
import { CheckOutComponent } from '../pms/caja/check-out/check-out.component';
import { HuespedComponent } from '../pms/recepcion/huesped/huesped.component';
import { ReservacionComponent } from '../pms/reservaciones/reservacion/reservacion.component';
import { DisponibilidadComponent } from '../pms/consultas/disponibilidad/disponibilidad.component';
import { RequerimientosEspComponent } from '../pms/ama-llaves/requerimientos/requerimientos-esp.component';
import { CambiosChequesComponent } from './puntos-venta/cambios-cheques/cambios-cheques.component';
import { VerifyInfociaGuardService } from '../services/verify-infocia-guard.service';
import { AuthGuard } from 'app/services/auth-guard.service';
import { RoutesGuard } from 'app/services/routes-guard.service';

const POS_ROUTES: Routes = [
  {
    path: '',
    component: BaseLayoutComponent,
    children: [
      { path: 'home', component: HomePosComponent, data: { extraParameter: '' }, canActivate: [RoutesGuard] },
      { path: 'perfil', component: PerfilUserComponent, data: { extraParameter: '' } },
      { path: 'cheque', component: ChequeComponent, data: { extraParameter: '', uso: 'H' } },
      { path: 'cambios-cheque', component: CambiosChequesComponent, data: { extraParameter: '' } },
      { path: 'consulta-huesped', component: HuespedComponent, data: { uso: 'H', extraParameter: '', consulta: true } },
      // Consultas
      { path: 'disponibilidad', component: DisponibilidadComponent, data: { extraParameter: '' } },
      { path: 'consulta-cuentas', component: CheckOutComponent, data: { uso: 'H', consulta: true, extraParameter: '' } },
      {
        path: 'consulta-reservacion', component: ReservacionComponent,
        data: { uso: 'R', consulta: true, extraParameter: '' }
      },
      { path: 'requerimientos', component: RequerimientosEspComponent, data: { extraParameter: '' } },
      { path: 'Reportes', loadChildren: './reportes/reportespos.module#ReportesPosModule', data: { extraParameter: '' } },
      { path: 'Mantenimientos', loadChildren: './manttos/manttospos.module#ManttosPosModule', data: { extraParameter: '' } }
    ],
    canActivateChild: [
      VerifyInfociaGuardService
    ],
    canActivate: [
      AuthGuard,
      RoutesGuard
    ]
  }
];

export const POS_ROUTING = RouterModule.forChild(POS_ROUTES);
