import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-home-pos',
    templateUrl: './home-pos.component.html',
    /*styleUrls: ['./home-pos.component.css']*/
})
export class HomePosComponent implements OnInit {
    heading = 'Dashboard';
    subheading = 'Point of Sales System.';
    icon = 'pe-7s-home icon-gradient bg-tempting-azure';

    constructor() {
    }

    ngOnInit() {
    }

}
