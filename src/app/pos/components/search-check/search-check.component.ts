import { Component, Output, EventEmitter, Input, SimpleChanges, OnChanges, OnInit } from '@angular/core';
import { SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { GLOBAL } from '../../../services/global';
import { ChequeService } from '../../puntos-venta/cheque/services/cheque.service';
const toast = GLOBAL.toast;

@Component({
  selector: 'app-search-check',
  templateUrl: './search-check.component.html',
  styleUrls: ['./search-check.component.css']
})
export class SearchCheckComponent implements OnInit, OnChanges {
  @Output() cheque = new EventEmitter;
  @Output() close = new EventEmitter;
  @Input() mantto: any = [];
  @Input() mesero: string;
  @Input() punto: any = [];
  @Input() ruta: string;
  @Input() btnPlano: boolean = true;

  public dataGrid: any = [];
  public dataComplete: any = [];
  public filter: CompositeFilterDescriptor;
  public flagCerrar: boolean;
  public sort: SortDescriptor[] = [{
    field: 'cMesa',
    dir: 'asc'
  }];
  ttUsuario: any = [];

  ngOnInit() {
  }
  constructor(private _cheque: ChequeService) { }

  ngOnChanges(change: SimpleChanges) {
    if (change.punto !== undefined) {
      if (change.punto.currentValue) {
        this.getCheques('A');
      }
    }
    if (change.ruta !== undefined) {
      if (change.ruta.currentValue === 'cambios-cheques') {
        this.flagCerrar = true;
      }
    }
  }
  consultarCheques(estatus) {
    switch (estatus) {
      case 'A': // Abiertos
        this.getCheques('A');
        break;
      case 'C': // Cerrados
        this.getCheques('C');
        break;
      case 'X': // Cancelados
        this.getCheques('X');
        break;
      default: // Todos
        this.getCheques('*');
        break;
    }
  }
  getCheques(estatus) {
    this.dataComplete = [];
    this.dataGrid = [];
    this._cheque.getCheques('*', estatus, this.punto).subscribe(data => {
      if (data) {
        this.dataGrid = data;
        this.dataComplete = data;
        this.dataGrid = {
          data: orderBy(this.dataGrid, this.sort),
          total: this.dataGrid.length
        };
      }
    });
  }
  selectRow({ selectedRows }) {
    const item = selectedRows[0].dataItem;
    if (item.lView) {
      this.cheque.emit(selectedRows[0].dataItem);
      this.cerrar();
    } else {
      toast({ type: 'info', title: 'Cheque de otro mesero!!!' });
    }
  }
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.length
    };
  }
  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }
  cerrar() {
    this.close.emit(true);
  }
}
