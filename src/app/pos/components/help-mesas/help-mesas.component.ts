import { Component, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ChequeAyudaService } from '../../puntos-venta/cheque/services/cheque-ayuda.service';
import { GLOBAL } from '../../../services/global';
const swal = GLOBAL.swal;

@Component({
  selector: 'app-help-mesas',
  templateUrl: './help-mesas.component.html',
  styleUrls: ['./help-mesas.component.css']
})
export class HelpMesasComponent implements OnChanges {

  @Output() mesa = new EventEmitter;
  @Output() cheque = new EventEmitter;
  @Output() close = new EventEmitter;
  @Output() verPlano = new EventEmitter;
  @Input() punto: any = [];
  @Input() inMesa: any;
  public dataGrid: any = [];
  public dsCheques: any = [];
  public mesaSelected: any;
  private isSingleClick: boolean = true;
  public planoBg: string = 'plano-mesas.jpg';
  loading: boolean;

  constructor(private _cheque: ChequeAyudaService) { }

  ngOnChanges(change: SimpleChanges) {
    if (change.punto && change.punto.currentValue) {
      this.crearPlano();
    }
    if (change.inMesa !== undefined) {
      if (change.inMesa.currentValue) {
        this.mesaSelected = change.inMesa.currentValue;
      } else {
        this.mesaSelected = [];
      }
    }
  }
  crearPlano() {
    if (this.punto.cImagen) {
      this.loading = true;
      if (this.punto.cImagen.length !== 3) {
        this.punto.cImagen = this.punto.cImagen.split(',');
      }
      // EL campo cErrDesc nos ayudara a indentificar el estado actual de la mesa
      if (this.punto.cImagen[0]) {
        this.planoBg = this.punto.cImagen[0];
      } else {
        this.planoBg = 'plano-mesas.jpg';
      }
      this._cheque.getMesas(this.punto.Punto, '', true).subscribe(mesas => {
        if (mesas) {
          this.verPlano.emit('mesas-si');
          this._cheque.getCheques('*', 'A', this.punto.Punto).subscribe(cheques => {
            if (cheques) {
              if (cheques.cMensTit !== undefined) {
                swal(cheques.cMensTit, cheques.cMensTxt, 'error');
              } else {
                this.dsCheques = cheques;
                mesas.forEach(item => {
                  item.iOldMesa = 0;
                  const totalCheques = [];
                  if (item.cCoordenada) {
                    item.cCoordenada = item.cCoordenada.split(',');
                  }
                  if (this.dsCheques.length) {
                    this.dsCheques.forEach(cheque => {
                      if (cheque.pea === 'A' || cheque.pea === 'I') {
                        if (item.cMesa === cheque.cMesa) {
                          totalCheques.push(cheque);
                          item.cErrDesc = cheque.pea;
                          item['cheqtotal'] = totalCheques.length;
                          if (item.cMesa === cheque.cMesa) {
                            item.iOldMesa += cheque.pper;
                          }
                        }
                      }
                    });
                  }
                  if (item.cErrDesc === '') {
                    item.iOldMesa = item.iMcap;
                  }
                  if (!item.cheqtotal) {
                    item.cheqtotal = 0;
                  }
                });
                this.dataGrid = mesas;
              }
            } else {
              this.dsCheques = [];
              mesas.forEach(item => {
                if (item.cCoordenada[0]) { item.cCoordenada = item.cCoordenada.split(','); }
                item['cheqtotal'] = 0;
              });
              this.dataGrid = mesas;
            }
          });
        } else {
          mesas = [];
          this.verPlano.emit('mesas-no');
        }
        this.loading = false;
      });
    }else {
      this.verPlano.emit('mesas-no');
    }
  }
  getCheques(estatus) {
    this._cheque.getCheques('*', 'A', this.punto).subscribe(data => {
      if (data) { this.dataGrid = data; }
    });
  }
  nuevoCheque(e) {
    this.isSingleClick = false;
    if (e.length) {
      this.mesa.emit(e[0]);
    } else {
      this.mesa.emit(e);
    }

    this.mesaSelected = e.cMesa;
  }
  btnMesaSel(e) {
    this.isSingleClick = true;
    setTimeout(() => {
      if (this.isSingleClick) {
        const arrayFolios = [];
        this.dsCheques.forEach(cheque => {
          if (cheque.cMesa === e.cMesa) { arrayFolios.push(cheque); }
        });
        if (arrayFolios.length) {
          // Esta mesa tiene folios
          this.cheque.emit(arrayFolios);
        } else {
          // mesa disponible
          this.mesa.emit(e);
          this.mesaSelected = e.cMesa;
        }
      }
      setTimeout(() => this.cerrar());
    }, 350);
  }
  selectRow(event) {
    const i = event.index;
    this.mesa.emit(this.dataGrid[i].cMesa);
    setTimeout(() => this.cerrar());
  }


  cerrar() {
    this.close.emit(true);
  }
}
