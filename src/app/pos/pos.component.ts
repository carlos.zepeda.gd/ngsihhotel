import {Component, OnInit} from '@angular/core';
import {InfociaGlobalService} from '../services/infocia.service';
import {GLOBAL} from '../services/global';

declare var $: any;

@Component({
    selector: 'app-pos',
    templateUrl: './pos.component.html',
    styleUrls: ['./pos.component.css']
})
export class PosComponent implements OnInit {
    public navbar: boolean = true;
    public showCheque: boolean;
    public ttInfocia: any;
    public loading: boolean = true;
    public encabezado: any = [];
    public ttUsuarios: any = [];

    constructor(public _info: InfociaGlobalService) {
        this.sidebarCollapse();
    }

    ngOnInit() {
        this.ttInfocia = this._info.getSessionInfocia();
        this.ttUsuarios = this._info.getSessionUser();

        this.loading = false;
        this.encabezado = this.ttInfocia.encab.split('®');
        this.loading = false;
    }

    sidebarCollapse() {
        $('#sidebar, #content').toggleClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    }

    chequeCollapse() {
        this.showCheque = !this.showCheque;
        this.showCheque ? $('.table-class').hide('fast') : $('.table-class').show('fast');
    }

    endSession() {
        // this.router.navigate(['login']);
        this._info.logoutProgress().subscribe();
    }
}
