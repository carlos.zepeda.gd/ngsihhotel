import {RouterModule, Routes} from '@angular/router';
import {ImprimirChequeComponent} from './components/imprimir-cheque/imprimir-cheque.component';


const CHEQUE_ROUTES: Routes = [
    {
        path: 'menu', component: ImprimirChequeComponent,
        children: [
            {path: '**', pathMatch: 'full', redirectTo: 'menu'}
        ]
    },
];

export const CHEQUE_ROUTING = RouterModule.forChild(CHEQUE_ROUTES);
