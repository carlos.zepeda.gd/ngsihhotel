import { Injectable } from '@angular/core';
import { GLOBAL } from '../../../../services/global';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
const headers = new Headers({ 'Content-Type': 'application/json' });
const esp = GLOBAL.char174;
const url = GLOBAL.url;
const urlFact = GLOBAL.urlPhp;
@Injectable()
export class ChequeFacturaService {

  private notify = new Subject<any>();
  notifyObservable$ = this.notify.asObservable();

  constructor(private http: Http) {
  }
  public notifyOther(data: any) {
    if (data) { this.notify.next(data); }
  }
  public validarInputs(api: string, val: string) {
    return this.http.get(url + '/ayuda/' + api + '/' + val).map(res => res.json().response);
  }
  public getParametros(parametro) {
    return this.http.get(url + '/ayuda/parametros' + esp + parametro)
      .map(res => res.json().response.siAyuda.dsAyuda.ttParametros);
  }
  putFactura(cheque, factura) {
    const jsonFactura = JSON.stringify(factura);
    const params = '{"dsTgeneral":{"ttTgeneral":[' + jsonFactura + ']}}';

    return this.http.put(url + '/pos/manttos/pcheques/facturar', params, { headers: headers })
      .map(res => res.json().response.siFactura);
  }
  putFactura2(huesped, movtos, factura) {
    const jhuesp = JSON.stringify(huesped);
    const jmovtos = JSON.stringify(movtos);
    const jfact = JSON.stringify(factura);
    const params = '{"dsFacturaH":{"bttTgeneral":[' + jfact + '], "bttHuespedes": [' + jhuesp + '], "bttTmpco": ' + jmovtos + '}}';
    return this.http.put(url + '/caja/checkout/factura', params, { headers: headers })
      .map(res => res.json().response.siFacturaH);

  }
  postGenerarFactura3(data) {
    return this.http.post(urlFact + '/api/pdf', data, { headers: headers }).map((res: any) => res);

  }
  getIntFactHoy() {
    return this.http.get(url + '/parametros/sistint/FE®FACTHOY').map(res => res.json().response.siSistint.dsSistint.ttSistint);
  }
  getCFD() {
    return this.http.get(url + '/ayuda/cfd/usos').map(res => res.json().response.siCfdayuda.dsCfdayuda.ttCfdUsos);
  }
  getConsecutivos(origin) {
    return this.http.get(url + '/manttos/consecutivos/' + origin)
      .map(res => res.json().response.siConsecutivo.dsConsecutivos.ttConsec);
  }
  postCliente(data) {
    const json = JSON.stringify(data);
    const params = '{"dsRfccias":{"ttRfccias":[' + json + ']}}';

    return this.http.post(url + '/manttos/rfc', params, { headers: headers })
      .map(res => res.json().response.siRfccia.dsRfccias.ttRfccias);
  }
  putCliente(data) {
    const json = JSON.stringify(data);
    const params = '{"dsRfccias":{"ttRfccias":[' + json + ']}}';

    return this.http.put(url + '/manttos/rfc', params, { headers: headers })
      .map(res => res.json().response.siRfccia.dsRfccias.ttRfccias);
  }
}
