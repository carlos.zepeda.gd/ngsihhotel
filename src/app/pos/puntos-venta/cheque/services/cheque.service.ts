import { Injectable } from '@angular/core';
import { GLOBAL } from '../../../../services/global';
import { Http, Headers } from '@angular/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/catch';
const headers = new Headers({ 'Content-Type': 'application/json' });
const esp = GLOBAL.char174;


@Injectable()
export class ChequeService {

  private url: string;
  private notify = new Subject<any>();
  public notifyObservable$ = this.notify.asObservable();
  public fday: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
    this.fday = JSON.parse(localStorage.getItem('globalInfocia')).fday;
  }
  public notifyOther(data: any) {
    if (data) { this.notify.next(data); }
  }
  putTransferirCheque(cheque, comanda) {
    const jsonCheque = JSON.stringify(cheque);
    const jsonComanda = JSON.stringify(comanda);
    const params = '{"dsPcheques":{"ttPcheques":[' + jsonCheque + '],"ttPcheqdet": ' + jsonComanda + '}}';
    return this.http.put(this.url + '/pos/manttos/pcheques/transferir', params, { headers: headers })
      .map(res => res.json().response.siPcheque.dsPcheques);
  }
  getCheques(mesero, estatus, punto) {
    const link = esp + mesero + esp + this.fday + esp + this.fday + esp + estatus + esp + punto;
    return this.http.get(this.url + '/pos/ayuda/cheques' + link)
      .map(res => res.json().response.siPosayuda.dsPosayuda.ttPcheques);
  }
 
  saveCheque(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPcheques":{"ttPcheques":[' + json + ']}}';

    return this.http.post(this.url + '/pos/manttos/pcheques', params, { headers: headers })
      .map(res => {
        return res.json().response.siPcheque.dsPcheques.ttPcheques;
        // if (res.json().response.siPcheque.dsMensajes.ttMensError !== undefined) {
        //   return res.json().response.siPcheque.dsMensajes.ttMensError;
        // }else {
        //   return res.json().response.siPcheque.dsPcheques.ttPcheques;
        // }
      });

  }
  updateCheque(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPcheques":{"ttPcheques":[' + json + ']}}';
    return this.http.put(this.url + '/pos/manttos/pcheques', params, { headers: headers })
      .map(res => res.json().response.siPcheque.dsPcheques.ttPcheques);
  }
  enviarComanda(cheque, tipo) {
    return this.http.get(this.url + '/pos/manttos/pcheques/envia/' + cheque.clase + esp + cheque.punto + esp + cheque.iCheque + esp + tipo)
      .map(res => {
        if (res.json().response.siComanda.dsMensajes.ttMensError !== undefined) {
          return res.json().response.siComanda.dsMensajes.ttMensError;
        } else {
          return res.json().response.siComanda.dsComanda.tComan;
        }
      });
  }
  enviarNodejs(body) {
    const headers2 = new Headers();
    headers2.append('Access-Control-Allow-Origin', '*');
    headers2.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    const json = JSON.stringify(body);
    return this.http.post(this.url + 'ticket', json, { headers: headers }).map(res => res.json());
  }
  postDetalle(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPcheqdet":{"ttPcheqdet":[' + json + ']}}';
    return this.http.post(this.url + '/pos/manttos/pcheqdet', params, { headers: headers })
      .map(res => res.json().response.siPcheqdet.dsPcheqdet.ttPcheqdet);
  }
  updateDetalle(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPcheqdet":{"ttPcheqdet":[' + json + ']}}';

    return this.http.put(this.url + '/pos/manttos/pcheqdet', params, { headers: headers })
      .map(res => res.json().response.siPcheqdet.dsPcheqdet.ttPcheqdet);
  }
  separarComanda(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPcheqdet":{"ttPcheqdet":' + json + '}}';

    return this.http.post(this.url + '/pos/manttos/pcheqdet', params, { headers: headers })
      .map(res => res.json().response.siPcheqdet.dsPcheqdet.ttPcheqdet);
  }
  deleteDetalle(data) {
    const json = JSON.stringify(data);
    const params = '{"dsPcheqdet":{"ttPcheqdet":[' + json + ']}}';
    return this.http.delete(this.url + '/pos/manttos/pcheqdet', { body: params, headers })
      .map(res => res.json().response.siPcheqdet.dsPcheqdet.ttPcheqdet);
  }
  getCheque(mesero, clase, punto, icheque) {
    const link = esp + mesero + esp + clase + esp + punto + esp + icheque;
    return this.http.get(this.url + '/pos/ayuda/cheqsel' + link)
      .map(res => res.json().response.siPosayuda.dsPosayuda.ttPcheques);
  }
  getDetalle(clase, punto, icheque) {
    const link = esp + clase + esp + punto + esp + icheque;
    return this.http.get(this.url + '/pos/ayuda/cheqdet' + link).map(res => res.json().response.siPosayuda.dsPosayuda.ttPcheqdet);
  }
  grabarPagoCheque(data) {
    const json = JSON.stringify(data);
    const params = '{"ttPcheqfp":' + json + '}';

    return this.http.post(this.url + '/pos/manttos/pcheqfp', params, { headers: headers }).map(res => {
      const r = res.json().response.siPcheqfp;
      if (r.dsMensajes.ttMensError) {
        return r.dsMensajes.ttMensError[0];
      }
      return r.dsPcheqfp.ttPcheqfp;
    });
  }
  getChequeFp(clase, punto, icheque) {
    const link = esp + clase + esp + punto + esp + icheque;
    return this.http.get(this.url + '/pos/ayuda/cheqfp' + link).map(res => res.json().response.siPosayuda.dsPosayuda.ttPcheqfp);
  }
}
