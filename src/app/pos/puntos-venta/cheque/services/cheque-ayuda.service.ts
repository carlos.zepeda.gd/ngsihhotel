import { Injectable } from '@angular/core';
import { GLOBAL } from '../../../../services/global';
import { Http, Headers } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/catch';
const esp = GLOBAL.char174;
const headers = new Headers({ 'Content-Type': 'application/json' });
@Injectable()
export class ChequeAyudaService {

  private url: string;
  private notify = new Subject<any>();
  notifyObservable$ = this.notify.asObservable();
  fday: string;

  constructor(private http: Http) {
    this.fday = JSON.parse(localStorage.getItem('globalInfocia')).fday;
    this.url = GLOBAL.url;
  }
  public notifyOther(data: any) {
    if (data) { this.notify.next(data); }
  }
  getTurnos(punto) {
    return this.http.get(this.url + '/pos/ayuda/pturnos' + esp + punto)
      .map(res => {
        const val = res.json().response.siPosayuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsPosayuda.ttPturnos;
        }
      });
  }
  getPuntoTurno(punto) {
    return this.http.get(this.url + '/pos/manttos/pundet/' + punto)
      .map(res => res.json().response.siPundet.dsPundet.ttPundet[0]);
  }
  putPuntoTurno(data) {
    const params = '{"dsPundet":{"ttPundet":[' + JSON.stringify(data) + ']}}';
    return this.http.put(this.url + '/pos/manttos/pundet', params, { headers: headers })
      .map(res => res.json().response.siPundet.dsPundet.ttPundet[0]);
  }
  getMonedas() {
    return this.http.get(this.url + '/ayuda/monedas')
      .map(res => {
        const val = res.json().response.siAyuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsAyuda.ttMonedas;
        }
      });
  }
  getPuntos() {
    return this.http.get(this.url + '/pos/ayuda/puntos' + esp)
      .map(res => {
        const val = res.json().response.siPosayuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsPosayuda.ttPuntos;
        }
      });
  }
  getClases() {
    return this.http.get(this.url + '/pos/ayuda/pclases' + esp)
      .map(res => {
        const val = res.json().response.siPosayuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsPosayuda.ttPclases;
        }
      });
  }
  getMeseros() {
    return this.http.get(this.url + '/pos/ayuda/pmeseros' + esp)
      .map(res => {
        const val = res.json().response.siPosayuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsPosayuda.ttPmeseros;
        }
      });
  }
  getMesas(punto, mesa, inactivo?) {
    if (inactivo) { inactivo = 'n'; } else { inactivo = 'y' }
    const link = esp + punto + esp + mesa + esp + inactivo;
    return this.http.get(this.url + '/pos/ayuda/pmesas' + link)
      .map(res => {
        const val = res.json().response.siPosayuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsPosayuda.ttPmesas;
        }
      });
  }
  getTipos() {
    return this.http.get(this.url + '/pos/ayuda/tipos' + esp)
      .map(res => {
        const val = res.json().response.siPosayuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsPosayuda.ttPtipos;
        }
      });
  }

  getSubtipos(tipo: number) {
    return this.http.get(this.url + '/pos/ayuda/ptipos' + esp + tipo)
      .map(res => {
        const val = res.json().response.siPosayuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsPosayuda.ttPsubtipos;
        }
      });
  }
  getPlatillos(_tipo: number, _subtipo: number, _platillo: string) {
    if ((_tipo !== 0 && !_tipo) && (_subtipo !== 0 && !_subtipo)) {

    }
    const filters = '/' + _tipo + esp + _subtipo + esp + 'N' + esp + _platillo;
    return this.http.get(this.url + '/pos/manttos/platillos' + filters)
      .map(res => {
        const val = res.json().response.siPplatillo;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsPplatillos.ttPplatillos;
        }
      });
  }
  getPreciosPlatillos(_punto: number, _tipo: number, _subtipo: number, _platillo: string) {
    const filters = esp + _punto + esp + _tipo + esp + _subtipo + esp + _platillo;
    return this.http.get(this.url + '/pos/ayuda/preciosplatillos' + filters)
      .map(res => {
        const val = res.json().response.siPosayuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsPosayuda.ttPplatillos;
        }
      });
  }
  getCheques(mesero, estatus, punto) {
    const link = esp + '*' + esp + this.fday + esp + this.fday + esp + estatus + esp + punto;
    return this.http.get(this.url + '/pos/ayuda/cheques' + link)
      .map(res => {
        const val = res.json().response.siPosayuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsPosayuda.ttPcheques;
        }
      });
  }
  getCheque(mesero, clase, punto, icheque) {
    const link = esp + mesero + esp + clase + esp + punto + esp + icheque;
    return this.http.get(this.url + '/pos/ayuda/cheqsel' + link)
      .map(res => {
        const val = res.json().response.siPosayuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsPosayuda.ttPcheques;
        }
      });
  }
  getDetalle(clase, punto, icheque) {
    const link = esp + clase + esp + punto + esp + icheque;
    return this.http.get(this.url + '/pos/ayuda/cheqdet' + link)
      .map(res => {
        const val = res.json().response.siPosayuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsPosayuda.ttPcheqdet;
        }
      });
  }
  getModificadores(punto, tipo, platillo) {
    const link = esp + punto + esp + tipo + esp + platillo + esp + '';
    return this.http.get(this.url + '/pos/ayuda/modificador' + link)
      .map(res => {
        const val = res.json().response.siPosayuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsPosayuda.ttPlatmodif;
        }
      });
  }
  getCodigos() {
    return this.http.get(this.url + '/ayuda/codigos' + esp + 'c' + esp + '8' + esp + 'y')
      .map(res => {
        const val = res.json().response.siAyuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsAyuda.ttCodigos;
        }
      });
  }
  getChequeFp(clase, punto, icheque) {
    const link = esp + clase + esp + punto + esp + icheque;
    return this.http.get(this.url + '/pos/ayuda/cheqfp' + link)
      .map(res => {
        const val = res.json().response.siPosayuda;
        if (val.dsMensajes.ttMensError !== undefined) {
          return val.dsMensajes.ttMensError[0];
        } else {
          return val.dsPosayuda.ttPcheqfp;
        }
      });
  }
  getParametrosPv() {
    return this.http.get(this.url + '/ayuda/parametros' + esp + 'pv')
      .map(res => res.json().response.siAyuda.dsAyuda.ttParametros);
  }
  getCalculaCheque(clase, punto, icheque) {
    const link = esp + clase + esp + punto + esp + icheque;
    return this.http.get(this.url + '/pos/ayuda/calCheque' + link)
      .map(res => {
        if (!res.json().response.siPosayuda.dsPosayuda) {
          return [];
        } else {
          return res.json().response.siPosayuda.dsPosayuda.ttTgeneral;
        }
      });
  }
  postLimiteHuesped(data) {
    const json = JSON.stringify(data);
    const params = '{"dsTgeneral":{"ttTgeneral":[' + json + ']}}';
    return this.http.put(this.url + '/pos/manttos/pcheques/limite', params, { headers: headers })
      .map(res => res.json().response.siTgeneral.dsTgeneral.ttTgeneral);
  }
}
