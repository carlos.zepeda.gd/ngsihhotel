import { Component, OnInit, ViewChild, OnDestroy, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { AccionesChequeComponent } from './components/acciones-cheque/acciones-cheque.component';
import { BuscarService } from '../../../pms/recepcion/check-in/services/childs/buscar.service';
import { NavbarChequeComponent } from './components/navbar-cheque/navbar-cheque.component';
import { ManttoMenuService } from '../../../pms/manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { ChequeAyudaService } from './services/cheque-ayuda.service';
import { ChequeService } from './services/cheque.service';
import { ChequeDetalle } from './models/cheque-detalle';
import { GLOBAL } from '../../../services/global';
import { Platillo } from './models/platillo';
import { Cheque } from './models/cheque';
import { Tipo } from './models/tipo';
import { Subscription } from 'rxjs/Subscription';
import swal from 'sweetalert2';
import { ValidationsService } from '../../../pms/recepcion/check-in/services/service-checkin.index';

declare var $: any;

@Component({
    selector: 'app-cheque',
    templateUrl: './cheque.component.html',
    styleUrls: ['./cheque.component.css'],
    providers: [NgbModalConfig]
})
export class ChequeComponent implements OnInit, OnDestroy {
    heading = 'Cheque';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';
    formatKendo = localStorage.getItem('formatKendo');
    @ViewChild(NavbarChequeComponent) navCheque: NavbarChequeComponent;
    @ViewChild(AccionesChequeComponent) actionsCheque: AccionesChequeComponent;
    public reiniciar: string;
    public numColumn = 3;
    public flagShowTotal: boolean;
    public flagEnvioCom: boolean = true;
    public subscription: Subscription;
    // Separar platillo por porciones
    public separarActive: boolean;
    public porciones: any;
    public errorPorciones: string;
    public validatorZero: boolean = true;
    public totalPorciones: any;
    public arrayPorciones: Array<number> = [];
    public desglosePorciones: boolean;
    // Transferir platillos
    public transferir: boolean;
    public buscarChequeTransf: boolean;
    public comandaTransfer: any[];
    // pago de Cheque
    public listaPagos: any;
    public listaPagosTab: any;
    public totalCalculado: any;
    public flagPagar: boolean;
    // Parametros de bd
    public parametrosPv: any;
    public manttoCheque: any;
    // Impresiones
    public infoPrint: { descuento: number; notas: string; voucher: string; propina: number; };
    public windowPrint: boolean;
    public imprimirCheque: boolean;
    public flagImprimirFp: boolean;
    // Campos para ordenar
    public orderComanda = 'iCom';
    public orderPlatillo = 'pdesc';
    public orderTipos = 'tdesc';
    public orderSubtipo = 'cDesc';
    // Colección de datos
    public huesped: any;
    private ttUsuario: any;
    public ttInfocia: any;
    public dsMesas: any = [];
    public dsClases: Array<{ cdesc: string, clase: string, cdescto: boolean }> = [];
    public dsMeseros: Array<{ mnombre: string, Mesero: string }> = [];
    public claseChequeSelect: any = [];
    public tipos: Tipo[];
    public subtipos: any[] = [];
    public subtipoSelected: any;
    public platillos: Platillo[] = [];
    public iNumero: number = 1;
    public tipoSeleccionado: number;
    public tipo: any;
    public selectedRow: any = [];
    // Variables para grabar cheque
    public formCheque: any = [];
    public puntoSelect: any;
    public chequeForm: Cheque;
    public estatusCheque: string;
    public chequeSelected: any;
    public readOnlyField: boolean = true;
    // Variables para obtener el menu
    public comanda: any = [];
    public items: number = 0;
    public comandaAut: boolean;
    // Variables para ayudas
    public flagBuscarMesa: boolean = true;
    public flagBuscarCheque: boolean;
    public flagBuscarHuesped: boolean;
    public loading: boolean = true;
    // Acciones del cheque, ocultar y mostrar
    public nuevo: boolean;
    public editar: boolean;
    public cancelar: boolean;
    public reactivar: boolean;
    public reabrir: boolean;
    public facturar: boolean;
    public flagVerMenu: boolean;
    public flagShowTabs: boolean;
    // Totales del cheque
    public total: number = 0;
    public subtotal: number = 0;
    public descuento: number = 0;
    public propina: number = 0;
    private porcDescuento: number = 0;
    descuentoPlatillo: any;
    descuentoGlobal: boolean = true;
    public envioPrint: any;
    mesaSelected: any = [];
    totalCheques: any = [];
    flagCambioMesa: boolean;
    flagBtnSeparar: boolean;
    public chequeFp: any = [];
    @ViewChild('modalFactura') modalFactura: TemplateRef<any>;
    @ViewChild('modalDetalle') modalDetalle: TemplateRef<any>;
    @ViewChild('modalAuth') modalAuth: TemplateRef<any>;
    nuevoTurno: any;
    public bPlatilloValue: string;
    toast = GLOBAL.toast;
    userAuthValid: boolean;
    messageUserAuth: string;
    getAuth: boolean;
    userAuth: any;
    detalleCancelar: any;
    loadingClick: boolean = false;

    constructor(private _cheque: ChequeService,
        private _ayuda: ChequeAyudaService,
        public _info: InfociaGlobalService,
        private _buscar: BuscarService,
        private _router: Router,
        private _valid: ValidationsService,
        private _menuServ: ManttoMenuService,
        private _modalService: NgbModal,
        private _config: NgbModalConfig) {
        this.ttInfocia = this._info.getSessionInfocia();
        this.ttUsuario = this._info.getSessionUser();
        this.manttoCheque = this.ttUsuario.cVal4.split('');
        if (localStorage.getItem('num-columns')) {
            this.numColumn = Number(localStorage.getItem('num-columns'));
        }
        this._ayuda.getClases().subscribe(data => this.dsClases = data);
        this._ayuda.getMeseros().subscribe(data => this.dsMeseros = data);
        this._ayuda.getTipos().subscribe(data => {
            if (data) {
                this.tipos = data;
                this.selectTipo(this.tipos[0], null);
            }
        });
        this._ayuda.getParametrosPv().subscribe(data => {
            if (data) {
                this.parametrosPv = data[0];
                if (!this.descuentoGlobal) {
                    this.descuentoPlatillo = this.parametrosPv.pchr4;
                } else {
                    this.descuentoPlatillo = 'no';
                }
                // if (this.descuentoPlatillo === 'yes') {
                //     this.descuentoGlobal = false;
                // }
            }
        });
        this.puntoSelect = JSON.parse(localStorage.getItem('puntoSelect'));
        if (this.puntoSelect) {
            this.puntoSelected(this.puntoSelect, 'default');
        } else {
            this.getPuntos();
        }
        this.subscription = this._info.notify.subscribe(res => {
            if (this.chequeSelected) {
                if (res.option === 'calcularTotal') {
                    this.calcularTotal();
                }
            }
        });
        this.btnBuscarMesa();
    }

    ngOnDestroy(): void {
        this.reiniciarCheque();
    }

    ngOnInit() {
        this._config.backdrop = 'static';
        this._config.keyboard = false;
        const routeAux = this._router.url.split('/');
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            if (data.length !== 0) {
                data.forEach(cMenu => {
                    if (cMenu.SubMenu !== '') {
                        cMenu.SubMenu.forEach(SubMenu => {
                            if (SubMenu.Opcion && SubMenu.Opcion !== '') {
                                SubMenu.Opcion.forEach(Opcion => {
                                    if (Opcion.cPrograma === routeAux[routeAux.length - 1]) {
                                        this.icon += ' ' + cMenu.cIcon;
                                    }
                                });
                            } else if (SubMenu.cPrograma === routeAux[routeAux.length - 1]) {
                                this.icon += ' ' + cMenu.cIcon;
                            }
                        });
                    }
                });
            }
        });
    }

    trackByIndex(index: number, obj: any): any {
        return index;
    }
    // ----------------------------------- PUNTOS DE VENTA ------------------------------- //
    getPuntos() {
        this._ayuda.getPuntos().subscribe(data => {
            if (!data) {
                swal('Atención!', 'No hay puntos de venta activos!!', 'warning').then(() => this._router.navigate(['./pos/home']));
            } else {
                if (!this.puntoSelect) {
                    this.puntoSelected(data[0], 'cambio-navbar');
                }
            }
        });
    }

    puntoSelected(punto, asign?) {
        if (!punto) {
            return;
        }
        this._ayuda.getPuntoTurno(punto.Punto).subscribe(det => {
            this.nuevoTurno = det;
            this.formCheque.turno = this.nuevoTurno.pturno;
            if (asign === 'cambio-navbar') {
                this.puntoSelect = punto;
                this.flagCambioMesa = false;
                this.nuevo = false;
            }
            localStorage.setItem('puntoSelect', JSON.stringify(punto));
            if (this.puntoSelect) {
                this.comandaAut = this.puntoSelect.pcomanda; // La comanda es Automatica
                if (this.nuevo) {
                    this.formCheque.turno = this.nuevoTurno.pturno;
                    this.formCheque.punto = this.puntoSelect.pdesc;
                }
            }
            if (this.editar) {
                this.reiniciarCheque();
            }
            if (this.puntoSelect && !this.puntoSelect.cImagen) {
                this.flagBuscarCheque = true;
                this.flagBuscarMesa = false;
            } else {
                this.flagBuscarMesa = true;
                this.flagBuscarHuesped = false;
            }
        });
    }

    // ----------------------------------- METODOS DE CHEQUE ------------------------------- //

    // Output, resultado de la busqueda de cheques

    limpiarCheque() {
        if (this.puntoSelect) {
            if (this.puntoSelect.Clase) {
                this.dsClases.forEach(clase => {
                    if (clase.clase === this.puntoSelect.Clase) {
                        this.claseChequeSelect = clase;
                        if (this.claseChequeSelect.cdescto) {
                            this.descuentoGlobal = true;
                        } else {
                            this.descuentoGlobal = false;
                        }
                    } else {
                        if (JSON.parse(localStorage.getItem('clase-selected'))) {
                            this.claseChequeSelect = JSON.parse(localStorage.getItem('clase-selected'));
                        }
                    }
                });
            }
            let mesa = '';
            if (this.mesaSelected) {
                mesa = this.mesaSelected.cMesa;
            }
            this.formCheque = {
                icheque: 0, punto: this.puntoSelect.pdesc, turno: this.nuevoTurno.pturno, fecha: this.ttInfocia.fday,
                estatus: '', clase: this.claseChequeSelect, mesero: this.ttUsuario.Mesero, personas: 1,
                mesa: mesa, cuarto: '', descuento: 0, voucher: 0
            }
        }
    }

    resultCheques(cheques) {
        const tmp = [];
        // this.loading = true;
        if (this.flagCambioMesa) {
            this.formCheque.mesa = cheques[0].cMesa;
            return;
        }

        if (cheques.length) {
            cheques.forEach(item => {
                if (item.lView) {
                    tmp.push(item);
                }
            });
            if (!tmp.length) {
                return this.toast({ type: 'info', title: 'Cheque de otro mesero!!!' });
            }
            this.totalCheques = tmp;
            this.chequeSelected = tmp[0];
        } else {
            if (cheques.lView) {
                this.chequeSelected = cheques;
            } else {
                return this.toast({ type: 'info', title: 'Cheque de otro mesero!!!' });
            }
        }
        this.editar = true;
        this.flagBuscarMesa = false;
        this.flagCambioMesa = false;
        this.flagBuscarHuesped = false;
        this.flagVerMenu = true;
        // const value = new Tgeneral(
        //   '', '', '', '', '', '', '', '', '', '', '',
        //   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        //   null, null, null, null, null, null, null, null, null, null, null,
        //   this.chequeSelected.pfolio, this.chequeSelected.punto, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        //   'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no',
        //   null, null, null, null, null, null, null, null, null, null, null
        // );
        // this._ayuda.postLimiteHuesped(value).subscribe(data => {
        // })
        this.calcularTotal();
        localStorage.setItem('cheque-selected', JSON.stringify(this.chequeSelected))
        if (this.editar) {
            this.chequeSelected.pea !== 'A' ? this.readOnlyField = true : this.readOnlyField = false;

            // Se recorre para obtener todo el detalle de la clase del cheque seleccionado
            if (this.dsClases) {
                this.dsClases.forEach(item => {
                    if (item.clase === this.chequeSelected.clase) {
                        this.claseChequeSelect = item;
                        this.descuentoGlobal = item.cdescto;
                    }
                });
            }
            if (this.descuentoPlatillo === 'yes') {
                this.descuentoGlobal = false;
            }
            this.formCheque = {
                icheque: this.chequeSelected.iCheque,
                punto: this.chequeSelected.cPundesc,
                turno: this.chequeSelected.pturno,
                fecha: this.chequeSelected.pfecha,
                estatus: this.chequeSelected.pea,
                clase: this.claseChequeSelect,
                mesero: this.chequeSelected.Mesero,
                personas: this.chequeSelected.pper,
                mesa: this.chequeSelected.cMesa,
                cuarto: '',
                descuento: this.porcDescuento,
                voucher: this.chequeSelected.pncheq
            }
            if (this.chequeSelected.pfolio) {
                this.buscarFolioHuesped(this.chequeSelected.pfolio);
            }
        }
        // this.formCheque = this.formCheque;
        this.verDetalleCheque();
        if (this.chequeSelected.pea === 'I') {
            setTimeout(() => this.pagarCheque());
        } else {
            if (this.chequeSelected.pea === 'A') {
                // this.flagVerMenu = true;
                // this.flagPagar = false;
                setTimeout(() => {
                    this.tab2();
                    this.changeTab(2);
                });
            }
            if (this.chequeSelected.pea === 'C') {
                this.flagVerMenu = false;
                this.flagBuscarMesa = true;
                setTimeout(() => {
                    this.tab2();
                    this.changeTab(2);
                });
            }
        }
        this.flagShowTabs = true;
    }

    nuevoCheque() {
        this.reiniciarCheque();
        this.reiniciar = 'n';
        this.nuevo = true;
        this.comanda = [];
        setTimeout(() => this.changeTab(1), 500);
    }

    editarCheque() {
        this.reiniciarCheque();
        setTimeout(() => this.changeTab(1), 100);
        this.editar = true;
    }

    reabrirCheque() {
        let text = '¿Re-Abrir Cheque?';
        if (this.chequeSelected.ffiscal) {
            text = 'Cheque Facturado!! ¿Re-Abrir Cheque?'
        }
        swal({
            title: text, text: '# Cheque: ' + this.chequeSelected.iCheque, type: 'warning',
            showCancelButton: true, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Re-Abrir!', cancelButtonText: 'No, Salir!',
            confirmButtonClass: 'btn btn-success', cancelButtonClass: 'btn btn-light ml-2', buttonsStyling: false,
        }).then((result) => result.value ? this.updateReAbrir('Cheque Re-Abierto!!') : this.reabrir = false);
    }

    reactivarCheque() {
        swal({
            title: '¿Re-Activar Cheque?', text: '# Cheque: ' + this.chequeSelected.iCheque, type: 'warning',
            showCancelButton: true, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Re-Activar!', cancelButtonText: 'No, Salir!',
            confirmButtonClass: 'btn btn-success', cancelButtonClass: 'btn btn-light ml-2', buttonsStyling: false,
        }).then((result) => result.value ? this.updateReAbrir('Cheque Re-Activado!!') : this.reactivar = false);
    }

    resultFactura(e) {
        if (e === 'close') {
            this.facturar = false;
        }
        if (e === 'facturado') {
            this.reiniciarCheque();
            this.editarCheque();
        }
    }

    facturarCheque(codigo?) {
        if (this.chequeSelected.ffiscal) {
            this.facturarChequeConfirma();
        } else {
            if (codigo !== 'HH') {
                this.facturar = true;
                this.openModal(this.modalFactura);
                setTimeout(() => this.changeTab(4), 100);
            }
        }
    }

    facturarChequeConfirma() {
        swal({
            title: '¿Cancelar Factura?', text: '# Cheque: ' + this.chequeSelected.iCheque, type: 'warning',
            showCancelButton: true, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Cancelar!', cancelButtonText: 'No, Salir!',
            confirmButtonClass: 'btn btn-success', cancelButtonClass: 'btn btn-light ml-2', buttonsStyling: false,
        }).then((result) => result.value ? this.facturar = true : this.facturar = false);
    }

    cerrarFactura() {
        this.flagVerMenu = true;
        this.facturar = false;
        this.changeTab(1);
    }

    // Proceso asincrono para la cancelación de Cheque. SI / NO
    async cancelarCheque(auth?) {
        if (!auth && this.chequeSelected.pea !== 'C' && this.manttoCheque[0] === 'N') {
            this.openModal(this.modalAuth, 'sm');
            this.messageUserAuth = 'Cancelar Cheque';
            return;
        }
        if (!auth && this.chequeSelected.pea === 'C' && this.manttoCheque[1] === 'N') {
            this.openModal(this.modalAuth, 'sm');
            this.messageUserAuth = 'Cancelar Cheque Pagado';
            return;
        }
        let question = '¿Cancelar Cheque ';

        if (this.chequeSelected.ffiscal) {
            question = 'Cheque Facturado!! ¿Cancelar Cheque ';
        }
        const { value: text } = await swal({
            input: 'textarea', inputPlaceholder: 'Razón de la cancelación...', showCancelButton: true
        })

        if (text) {
            swal({
                title: question + '#' + this.chequeSelected.iCheque + '?', text: 'Razón: ' + text, type: 'warning',
                showCancelButton: true, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Cancelar!', cancelButtonText: 'No, Salir!', confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-light ml-2', buttonsStyling: false,
            }).then(async (result) => {
                if (result.value) {
                    this.chequeSelected.pea = 'X';
                    this.chequeSelected.pnotas = text;
                    if (auth) {
                        this.chequeSelected.Usuario = auth;
                    } else {
                        this.chequeSelected.Usuario = '';
                    }
                    this.chequeSelected.dTotal = this.subtotal;
                    this.flagVerMenu = false;
                    this._cheque.updateCheque(this.chequeSelected).subscribe(cheque => {
                        if (cheque[0].cErrDesc) {
                            swal('Error!!!', cheque[0].cErrDesc, 'error');
                        } else {
                            swal({
                                position: 'center', type: 'success', title: 'Cheque Cancelado!!',
                                showConfirmButton: false, timer: 1500
                            }).then(() => {
                                this.reiniciarCheque();
                                this.flagBuscarMesa = true;
                                this.flagCambioMesa = false;
                            });
                        }
                    });
                }
            });
        }
    }

    guardarCheque(val) {
        this.loading = true;
        let huesped = 0;
        if (val.cuarto) {
            huesped = this.huesped.hfolio;
        }

        if (this.editar) {
            this.chequeSelected.cMesa = val.mesa;
            this.chequeSelected.pper = val.personas;
            this.chequeSelected.Mesero = val.mesero;
            this.chequeSelected.pfolio = huesped;
            this.chequeSelected.pporc = val.descuento;
            this.chequeSelected.dTotal = this.subtotal;
            this.updateCheque(this.chequeSelected);
        }
        if (this.nuevo) {
            if (!this.estatusCheque) {
                this.estatusCheque = 'A';
            }
            let clase = val.clase;
            if (clase) {
                if (clase.clase !== undefined) {
                    clase = val.clase.clase;
                }
            }
            this.chequeForm = new Cheque(clase, this.puntoSelect.Punto, 0, val.mesero, val.mesa,
                val.personas, 0, '', huesped, '', 1, this.ttInfocia.fday, this.ttUsuario.cUserId,
                this.estatusCheque, 0, val.descuento, '');

            this._cheque.saveCheque(this.chequeForm).subscribe(cheque => {
                this.loading = false;
                if (cheque[0].cErrDesc) {
                    swal(cheque[0].cErrDesc, '', 'error');
                    return;
                }
                this.chequeSelected = cheque[0];
                this.formCheque.icheque = cheque[0].iCheque;
                this.formCheque.estatus = cheque[0].pea;
                if (this.formCheque.icheque) {
                    this.notifyChangeCheque();
                    this.reiniciarBanderas();
                    this.calcularTotal();
                    this.consultarCheque();
                    this.flagVerMenu = true;
                    this.flagCambioMesa = false;
                    this.editar = true;
                    this.nuevo = false;
                    setTimeout(() => this.changeTab(2), 500);
                } else {
                    swal('Error al grabar el cheque!!', '', 'error');
                }
            });
        }
    }

    private totalChequesMesa(cheque) {
        this.totalCheques = [];
        this._cheque.getCheques('*', 'A', this.puntoSelect.Punto).subscribe(data => {
            if (data) {
                data.forEach(item => {
                    if (cheque.cMesa === item.cMesa) {
                        this.totalCheques.push(item);
                    }
                });
            }
        });
    }
    consultarCheque() {
        const c = this.chequeSelected;
        this._ayuda.getCheque(c.Mesero, c.clase, c.punto, c.iCheque).subscribe(data => {
            if (data) {
                this.chequeSelected = data[0];
                this.verDetalleCheque();
                this.totalChequesMesa(this.chequeSelected);
                localStorage.setItem('cheque-selected', JSON.stringify(this.chequeSelected));
            }
        });
    }
    updateCheque(data) {
        this._cheque.updateCheque(data).subscribe(cheque => {
            this.loading = false;
            if (cheque[0].cErrDesc) {
                swal('Error!!!', cheque[0].cErrDesc, 'error');
                return;
            }
            this.chequeSelected = cheque[0];
            if (this.formCheque.icheque) {
                this.notifyChangeCheque();
                this.reiniciarBanderas();
                this.calcularTotal();
                this.consultarCheque();
                this.flagCambioMesa = false;
                this.flagVerMenu = true;
                setTimeout(() => this.changeTab(2), 500);
            } else {
                swal('Error al grabar el cheque!!', '', 'error');
            }
        });
    }

    updateReAbrir(text) {
        this.flagVerMenu = false;
        this.chequeSelected.dTotal = this.subtotal;
        this.chequeSelected.pea = 'A';
        this._cheque.updateCheque(this.chequeSelected).subscribe(cheque => {
            if (cheque[0].cErrDesc) {
                return swal('Error!!!', cheque[0].cErrDesc, 'error');
            }
            this.consultarCheque();
            swal({ position: 'center', type: 'success', title: text, showConfirmButton: false, timer: 1500 });
            this.cerrarPago();
            setTimeout(() => this.changeTab(2));
            this.formCheque.estatus = 'A';
            this.reabrir = false;
            this.readOnlyField = false;
            this.verMenu();
        });
    }

    resultChequeUpdate(e) {
        this.chequeSelected = e;
    }

    accionCheque(e) {
        switch (e) {
            case 'nuevo':
                if (this.comanda.length && this.puntoSelect.pic1 && this.chequeSelected.pea === 'A' && this.flagEnvioCom) {
                    swal({
                        title: '¿Enviar Platillos?', text: 'Hay platillos pendientes para enviar...', type: 'question',
                        showCancelButton: true, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33',
                        confirmButtonText: 'Si, Enviar!', cancelButtonText: 'No, Reiniciar', timer: 6000,
                        confirmButtonClass: 'btn btn-success', cancelButtonClass: 'btn btn-light ml-2', buttonsStyling: false,
                    }).then((result) => {
                        if (result.value) {
                            this.enviarComanda();
                        }
                        this.nuevoCheque();
                        this.flagBuscarMesa = false;
                        this.flagCambioMesa = true;
                        setTimeout(() => this.flagShowTabs = true, 100);
                    });
                } else {
                    if (this.pagarCheque) {
                        this.reiniciarCheque();
                        setTimeout(() => this.flagShowTabs = true, 100);
                    }
                    this.nuevoCheque();
                    this.flagBuscarMesa = false;
                    this.flagCambioMesa = true;
                    setTimeout(() => this.flagShowTabs = true, 100);
                }
                break;
            case 'editar':
                this.editarCheque();
                break;
            case 'imprimir':
                if (!this.comanda.length) {
                    return swal('No hay platillos!!', '', 'warning');
                } else {
                    let count = 0;
                    this.comanda.forEach(platillo => {
                        if (platillo.dPorcion) {
                            count++;
                        }
                    });
                    if (!count) {
                        return swal('No hay platillos!!', '', 'warning');
                    }
                }
                if (this.formCheque.estatus === 'I') {
                    swal('Cheque ya esta impreso!!', '', 'warning').then(() => {
                        if (this.manttoCheque[6] === 'S') {
                            this.reabrirCheque();
                        }
                    });
                    return;
                }
                if (this.transferir) {
                    this.cerrarTransferencia();
                }
                if (this.separarActive) {
                    this.cerrarDesglosePorciones();
                }
                this.calcularTotal();
                if (this.totalCalculado) {
                    this.infoPrint = {
                        descuento: this.porcDescuento, notas: this.chequeSelected.pnotas,
                        voucher: this.chequeSelected.pncheq, propina: this.totalCalculado.dDec2.toFixed(2)
                    }
                    this.windowPrint = true;
                }
                break;
            case 'cancelar':
                this.cancelarCheque();
                break;
            case 'pagar':
                this.pagarCheque();
                break;
            case 'facturar':
                if (this.chequeFp && this.chequeFp.codigo === 'HH') {
                    return swal('', 'Cheque con Cargo Habitación!!', 'info');
                }
                this.facturarCheque();
                break;
            case 'reabrir':
                this.reabrirCheque();
                break;
            case 'reactivar':
                this.reactivarCheque();
                break;
            case 'reiniciar':
                if (this.comanda.length && this.puntoSelect.pic1 && this.chequeSelected && this.chequeSelected.pea === 'A' && this.flagEnvioCom) {
                    swal({
                        title: '¿Enviar Platillos?', text: 'Hay platillos pendientes para enviar...', type: 'question',
                        showCancelButton: true, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33',
                        confirmButtonText: 'Si, Enviar!', cancelButtonText: 'No, Reiniciar!', timer: 6000,
                        confirmButtonClass: 'btn btn-success', cancelButtonClass: 'btn btn-light ml-2', buttonsStyling: false,
                    }).then((result) => {
                        if (result.value) {
                            this.enviarComanda();
                        }
                        this.reiniciarCheque();
                        this.flagCambioMesa = false;
                    });
                } else {
                    this.reiniciarCheque();
                    this.flagCambioMesa = false;
                }
                break;
        }
    }

    reiniciarCheque() {
        localStorage.removeItem('cheque-selected');
        this.reiniciar = 's';
        this.flagShowTabs = false;
        this.chequeSelected = [];
        this.huesped = [];
        this.listaPagos = [];
        this.listaPagosTab = [];
        this.totalCheques = [];
        this.limpiarCheque();
        this.cerrarTransferencia();
        this.cerrarDesglosePorciones();
        this.reiniciarBanderas();
        this.cerrarFactura();
        this.cancelarPago();
        this.btnBuscarMesa();
        this.reabrir = false;
        this.reactivar = false;
        this.cancelar = false;
        this.editar = false;
        this.nuevo = false;
        this.readOnlyField = false;
        this.imprimirCheque = false;
        this.mesaSelected = [];
        this.comanda = [];
    }

    verGridCheques() {
        this.flagBuscarCheque = true;
        this.flagBuscarMesa = false;
    }
    closeHelpMesas() {
        this.reiniciarBanderas();
        this.flagBuscarMesa = true;
    }
    verCheques(e) {
        if (e === 'mesas-si') {
            this.flagBuscarMesa = true;
            this.flagBuscarCheque = false;
        } else {
            this.nuevoCheque();
            this.flagShowTabs = true;
            this.flagBuscarMesa = false;
            this.flagCambioMesa = false;
            this.flagBuscarHuesped = false;
            this.flagBuscarCheque = true;
        }
    }

    // Mostrar detalle del cheque seleccionado
    verDetalleCheque() {
        this._ayuda.getDetalle(this.chequeSelected.clase, this.chequeSelected.punto, this.chequeSelected.iCheque).subscribe(data => {
            this.comanda = [];
            if (data) {
                if (data.cMensTit !== undefined) {
                    swal(data.cMensTit, data.cMensTxt, 'error');
                } else {
                    const secuencias = [];

                    this.comanda = data;
                    this.comanda.filter(det => secuencias.push(det.iNumero));
                    this.iNumero = Math.max.apply(null, secuencias);
                    // Muestra los totales en la pestaña de Comanda.
                    this.enviarComandaFilter();
                    this.calcularTotal();
                    if (this.chequeSelected.pea === 'A') {
                        this.flagVerMenu = true;
                    } else {
                        this.flagVerMenu = false;
                    }
                }
            }
        });
    }

    enviarComandaFilter() {
        this.flagEnvioCom = false;
        this.comanda.filter(item => {
            if (item.dPorcion) {
                if (!item.lEnviar) {
                    this.flagEnvioCom = true;
                }
            }
        });
    }

    continuarImpresionCheque() {
        this.chequeSelected.pnotas = this.infoPrint.notas;
        this.chequeSelected.pporc = this.infoPrint.descuento;
        this.formCheque.descuento = this.infoPrint.descuento;
        this.chequeSelected.pprop = this.infoPrint.propina;
        this.chequeSelected.dTotal = this.subtotal;
        this.chequeSelected.pea = 'I';
        this.windowPrint = false;
        this._cheque.updateCheque(this.chequeSelected).subscribe(cheque => {
            if (cheque[0].cErrDesc) {
                return swal('Error!!', cheque[0].cErrDesc, 'error');
            }
            this.consultarCheque();
            this.formCheque.estatus = 'I';
            this.readOnlyField = true;
            this.calcularTotal();
            this.imprimirCheque = !this.imprimirCheque;
            this.flagVerMenu = false;
            this.flagCambioMesa = false;
            this.flagBuscarHuesped = false;
            setTimeout(() => this.imprimirCheque = !this.imprimirCheque, 1000);
        });
    }

    printChequeFinish() {
        this.tab1();
        this.changeTab(1);
        this.flagBuscarMesa = true;
    }

    reiniciarBanderas(e?) {
        this.flagBuscarMesa = false;
        this.flagVerMenu = false;
        this.flagPagar = false;
        this.flagImprimirFp = false;
        this.flagBuscarCheque = false;
        this.flagBuscarHuesped = false;
        if (e === 'ver-plano') {
            this.flagBuscarMesa = true;
        }
        if (e === 'salir-huesped') {
            if (this.formCheque.estatus === 'A') {
                this.flagVerMenu = true;
                this.flagCambioMesa = false;
            }
            if (this.formCheque.estatus === '') {
                if (!this.puntoSelect.pvalmes) {
                    this.flagBuscarCheque = true;
                } else {
                    this.flagCambioMesa = true;
                }
            }
        }
    }

    // -----------------------------------  CHANGES FORMULARIO CHEQUE ------------------------------- //
    btnBuscarHuesped() {
        this.flagBuscarMesa = false;
        this.flagCambioMesa = false
        this.flagVerMenu = false;
        this.flagBuscarCheque = false;
        this.flagBuscarHuesped = true;
        this.buscarChequeTransf = false;
    }

    abrirInfoHuesped(modal) {
        this.openModal(modal, 'md');
    }

    buscarFolioHuesped(folio) {
        this._buscar.getHuesped(folio, 'folio').subscribe(data => {
            if (data) {
                this.huesped = data[0];
                this.formCheque.cuarto = this.huesped.cuarto;
                this.notifyChangeCheque();
            }
        });
    }

    changeBuscarCuarto(modal) {
        this._buscar.getHuesped(this.formCheque.cuarto, 'cuarto').subscribe(data => {
            if (!data) {
                swal('Cuarto invalido!!', '', 'warning').then(() => this.formCheque.cuarto = '');
            } else {
                if (data.length > 1) {
                    data.forEach(item => {
                        if (item.heactual === 'CI') {
                            this.huesped = item;
                        }
                    });
                } else {
                    if (data[0].heactual === 'CO') {
                        this.formCheque.cuarto = '';
                        return swal('Folio Checkout!!', '', 'warning');
                    }
                    this.huesped = data[0];
                }
                this.openModal(modal, 'md');
            }
        });
    }

    // Output, resultado de la busqueda de huesped
    resultFolio(folio, modal) {
        if (folio.heactual === 'CO') {
            return swal('Folio Check-Out!!', '', 'warning');
        }
        this.huesped = folio;
        this.notifyChangeCheque();
        this.openModal(modal, 'md');
        this.formCheque.cuarto = this.huesped.cuarto;
        this.reiniciarBanderas('salir-huesped');
    }

    btnBuscarMesa(btn?) {
        if (btn && this.formCheque.estatus === 'A' || this.formCheque.estatus === '') {
            this.flagCambioMesa = true;
        }
        this.flagBuscarCheque = false;
        this.flagBuscarMesa = false;
        if (this.puntoSelect && !this.puntoSelect.cImagen) {
            this.flagBuscarCheque = true;
        } else {
            this.flagBuscarMesa = true;
        }
        this.flagVerMenu = false;
        this.flagBuscarHuesped = false;
        this.buscarChequeTransf = false;
        this.loading = false;
    }

    // Output, resultado de la busqueda de mesas
    resultMesas(e) {
        this.mesaSelected = e;
        if (this.flagBuscarMesa && this.formCheque.estatus === 'A') {
            return this.formCheque.mesa = e.cMesa;
        }
        if (this.nuevo && this.formCheque.estatus === '') {
            if (e.length) {
                this.formCheque.mesa = e[0].cMesa;
            } else {
                this.formCheque.mesa = e.cMesa;
            }
            return;
        }
        if (this.editar && this.formCheque.estatus === 'A') {
            return this.formCheque.mesa = e.cMesa;
        }
        this.nuevoCheque();
        this.flagShowTabs = true;
    }

    changeMesa() {
        if (!this.formCheque.mesa) {
            return;
        }
        if (this.puntoSelect.pvalmes) {
            this._ayuda.getMesas(this.puntoSelect.Punto, this.formCheque.mesa).subscribe(data => {
                if (!data) {
                    swal('Mesa Invalida!!', 'Busca la mesa mediante la ayuda!!', 'warning');
                    this.formCheque.mesa = '';
                }
            });
        }
    }

    // Al cambiar la clase del cheque se envia el evento que indica si se puede aplicar descuento
    changeClaseCheque(e) {
        if (e) {
            this.claseChequeSelect = e;
            if (!this.claseChequeSelect.cdescto) {
                this.descuentoGlobal = false;
            } else {
                this.descuentoGlobal = true;
            }
            localStorage.setItem('clase-selected', JSON.stringify(this.claseChequeSelect))
        } else {
            swal('Sin clases!!', 'No se encontraron clases de cheque activas!!', 'warning');
        }
    }

    changePersonas() {
        this.formCheque.personas = Math.round(this.formCheque.personas);
        if (this.formCheque.personas <= 0) {
            this.formCheque.personas = 1;
        }
        if (this.formCheque.personas > 100) {
            this.formCheque.personas = 100;
        }
    }

    btnChangeDescuento(value) {
        if (value === 'plus') {
            this.formCheque.descuento = this.formCheque.descuento + 5;
            if (this.formCheque.descuento > 100) {
                this.formCheque.descuento = 100;
                return;
            }
        } else {
            if (this.formCheque.descuento === 0) {
                return;
            }
            this.formCheque.descuento = this.formCheque.descuento - 5;
        }
    }

    changeDescuento() {
        if (this.formCheque.descuento < 0) {
            this.formCheque.descuento = 0;
        }
        if (this.formCheque.descuento > 100) {
            this.formCheque.descuento = 100;
        }
        // this.formCheque.descuento = Math.round(this.formCheque.descuento)
    }

    changeDescuentoPrint() {
        if (this.infoPrint.descuento < 0) {
            this.infoPrint.descuento = 0;
        }
        // this.infoPrint.descuento = Math.round(this.infoPrint.descuento);
    }

    changeServicioPrint() {
        const prop: any = Number(this.infoPrint.propina.toFixed(2));
        this.infoPrint.propina = prop;
    }
    // -----------------------------------  MENU DE PUNTO DE VENTA ------------------------------- //
    regresar() {
        if (this.tipos.length && this.subtipos.length && !this.platillos.length) {
            this.flagVerMenu = false;
            this.changeTab(1);
        }
        if (this.tipos.length && this.subtipos.length && this.platillos.length) {
            this.platillos = [];
        }
        if (this.tipos.length && !this.tipo.lsubtipo) {
            this.flagVerMenu = false;
            this.changeTab(1);
        }
    }

    selectTipo(tipo, index) {
        if (this.navCheque) {
            this.navCheque.bPlatilloValue = '';
        }
        this.platillos = [];
        this.tipoSeleccionado = index;
        this.tipo = tipo;
        if (this.tipo && this.tipo.lsubtipo) {
            this._ayuda.getSubtipos(tipo.tipo).subscribe(data => {
                if (!data) {
                    this.subtipos = [];
                } else {
                    this.subtipos = data;
                }
            });
        } else {
            this.getPlatillos(this.tipo, 0);
        }
    }

    selectSubtipo(subtipo, index) {
        this.subtipoSelected = subtipo.iSubTipo;
        this.getPlatillos(this.tipo, subtipo.iSubTipo);
        this.navCheque.bPlatilloValue = '';
    }

    getPlatillos(tipo, subtipo) {
        if (this.puntoSelect) {
            this._ayuda.getPreciosPlatillos(this.puntoSelect.Punto, tipo.tipo, subtipo, '').subscribe(data => {
                if (!data) {
                    this.platillos = [];
                } else {
                    this.platillos = data;
                }
            });
        }
    }

    buscarPlatillos(e) {
        if (this.puntoSelect) {
            this._ayuda.getPreciosPlatillos(this.puntoSelect.Punto, 0, 0, e).subscribe(data => {
                if (!data) {
                    data = [];
                }
                this.platillos = data;
            });
        }
    }

    // Selecciona un platillo de la comanda, se obtienen los valores y se asignan al objeto de detalle.
    selectPlatillo(platillo, index) {
        if (this.loadingClick) {
            return;
        }
        this.loadingClick = true;
        this.navCheque.bPlatilloValue = '';
        if (this.comandaAut) {
            this.nuevoPlatillo(platillo);
        } else {
            platillo.iCom = 0;
            this.nuevoPlatillo(platillo);
        }
    }

    verMenu() {
        this.flagBuscarHuesped = false;
        this.flagCambioMesa = false;
        this.flagVerMenu = true;
        this.flagBuscarMesa = false;
    }

    verTiposMenu() {
        if (this.formCheque.estatus === 'A') {
            this.flagVerMenu = true;
        }
    }


    // ----------------- TRANSFERIR PLATILLOS  ----------------- //

    transferirThis(detalle) {
        for (const item of this.comandaTransfer) {
            if (detalle.cRowID === item.cRowID) {
                item.cSelec ? item.cSelec = false : item.cSelec = true;
            }
        }
    }

    transferenciaPlatillo() {
        this.transferir = true;
        setTimeout(() => this.changeTab(3));
        this.comandaTransfer = [];
        for (const item of this.comanda) {
            if (item.Platillo && item.Tipo) {
                this.comandaTransfer.push(item);
            }
        }
    }

    transferirConfirma(e) {
        if (this.loadingClick) {
            return;
        }
        this.loadingClick = true;

        swal({
            title: '¿Transferir Platillos?', text: '# Cheque: ' + e.iCheque, type: 'question',
            showCancelButton: true, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Transferir!', cancelButtonText: 'No, Salir!',
            confirmButtonClass: 'btn btn-success', cancelButtonClass: 'btn btn-light ml-2', buttonsStyling: false,
        }).then((result) => {
            if (result.value) {
                this.transferirPlatillo(e.iCheque);
            } else {
                this.loadingClick = false;
            }
        });
    }

    transferirPlatillo(value) {
        this.chequeSelected.iTransf = value;
        this.chequeSelected.dTotal = this.subtotal;
        this.chequeSelected.pporc = this.porcDescuento;
        const send: any = [];
        this.comandaTransfer.forEach(item => {
            if (item.cSelec) {
                send.push(item);
            }
        })
        this._cheque.putTransferirCheque(this.chequeSelected, send).subscribe(data => {
            this.loadingClick = false;
            if (data.ttPcheques[0].cErrDesc) {
                swal(data.ttPcheques[0].cErrDesc, '', 'error');
            } else {
                this.chequeSelected = data.ttPcheques[0];
                this.consultarCheque();
                if (this.chequeSelected.iTransf) {
                    const text = '# Cheque: ' + this.chequeSelected.iTransf;
                    swal('Transferencia Exitosa!!', text, 'success').then(() => {
                        this.calcularTotal();
                        if (data.ttPcheqdet) {
                            this.comanda = data.ttPcheqdet;
                        }
                        this.cerrarTransferencia();
                    });
                }
            }
        });
    }

    cerrarTransferencia() {
        this.transferir = false;
        this.comandaTransfer = null;
        setTimeout(() => this.changeTab(2));
    }

    // ----------------- SEPARAR PLATILLOS  ----------------- //
    btnSepararPlatillos() {

        this.separarActive = true;
        if (this.selectedRow.dPorcion > 1) {
            this.desglosarPorciones(this.selectedRow.dPorcion);
        }
    }

    cerrarDesglosePorciones() {
        this.separarActive = false;
        this.flagBtnSeparar = false;
        this.errorPorciones = '';
        this.validatorZero = true;
        this.selectedRow = [];
        this.desglosePorciones = false;
        this.arrayPorciones = [];
        this.loadingClick = false;
    }

    onChangePorciones(e, items) {
        let acumulador = 0;
        for (const porcion of this.arrayPorciones) {
            if (porcion === 0) {
                this.validatorZero = true;
            }
            acumulador += porcion;
        }
        if (acumulador !== this.selectedRow.dPorcion) {
            this.errorPorciones = 'Porciones sumadas: ' + acumulador + ' Total de porciones del platillo: ' + this.selectedRow.dPorcion;
            this.validatorZero = true;
        } else {
            this.errorPorciones = '';
            this.validatorZero = false;
        }
    }

    desglosarPorciones(porciones) {
        this.porciones = porciones;
        this.validatorZero = false;
        this.arrayPorciones = [];
        let total = 0;
        let acumulador = 0;
        this.desglosePorciones = true;
        total = this.selectedRow.dPorcion;
        for (let i = 0; i < porciones - 1; i++) {
            if (Math.floor(total / porciones) === 0) {
                this.validatorZero = true;
            }
            this.arrayPorciones.push(Math.floor(total / porciones));
            acumulador += Math.floor(total / porciones);
        }
        this.arrayPorciones.push(Math.ceil(total - acumulador));
    }

    // Separa un registro de platillo que contiene mas de una porción
    separarPlatillos() {
        if (this.loadingClick) {
            return;
        }
        this.loadingClick = true;
        let decimal = 0;
        const objects = [];
        if (this.selectedRow.dPorcion % 1 === 0) { // Es un numero entero
            this.selectedRow.dPorcion = this.arrayPorciones[0];
        } else {  // Es un numero decimal
            decimal = this.selectedRow.dPorcion - Math.trunc(this.selectedRow.dPorcion);
        }
        // Recorre el numero de porciones y actualiza el primer registro a 1 porcion o a decimal
        // El resto de platillos los graba con 1 porcioN
        this.selectedRow.dPorcion = this.arrayPorciones[this.arrayPorciones.length - 1];
        this.selectedRow.cTipo = 'S';
        if (this.selectedRow) {
            this._cheque.updateDetalle(this.selectedRow).subscribe(data => {
                if (data[0].cErrDesc) {
                    return swal('Error!!', data[0].cErrDesc, 'error');
                }
            });
        } else {
            swal('Error!!', 'No fue posible separar este platillo, intentelo de nuevo!!', 'error')
        }

        for (let i = 0; i < this.arrayPorciones.length - 1; i++) {
            this.selectedRow.dPorcion = this.arrayPorciones[i];
            // this.selectedRow.cTipo = 'S';
            objects.push(this.selectedRow);
        }
        setTimeout(() => {
            if (objects) {
                this._cheque.separarComanda(objects).subscribe(comanda => {
                    if (!comanda) {
                        swal('Error!!', 'No fue posible separar este platillo, intentelo de nuevo!!', 'error');
                    }
                    if (comanda[0].cErrDesc) {
                        swal(comanda[0].cErrDesc, '', 'error');
                    } else {
                        this.comanda = comanda;
                        this.consultarCheque();
                        this.calcularTotal();
                    }
                });
            } else {
                swal('Error!!', 'No fue posible separar este platillo, intentelo de nuevo!!', 'error')
            }
        }, 500);
        this.cerrarDesglosePorciones();
    }

    // -----------------  PLATILLOS  ----------------- //
    // Mostrar detalle de un platillo. Este se muestra en una ventana.
    detallePlatillo(detalle) {
        $('#popupError').popover('hide')
        if (detalle.cEactual === 'X') {
            return;
        }
        this.selectedRow = detalle;
        this.porciones = this.selectedRow.dPorcion;
        this.desglosarPorciones(this.porciones);
        if (this.separarActive) {
            if (this.selectedRow.dPorcion > 9) {
                this.totalPorciones = Array(9).fill(0).map((x, i) => i);
            } else {
                this.totalPorciones = Array(this.selectedRow.dPorcion - 1).fill(0).map((x, i) => i);
            }
            if (this.selectedRow.dPorcion === 1) {
                // this.selectedRow = [];
                $('#popupError').popover('show');
                setTimeout(() => $('#popupError').popover('hide'), 1000);
                return;
            }

            this.flagBtnSeparar = true;
        }
        if (this.transferir) {
            // this.transferirPlatillo(detalle);
            return;
        }
        if (!this.separarActive && !this.transferir) {
            if (this.selectedRow.iCom === 0) {
                if (this.comanda.length === 1) {
                    this.selectedRow.iCom = this.comanda[0].iCom;
                } else {
                    this.comanda.forEach(item => {
                        if (item.iCom > 0) {
                            this.selectedRow.iCom = item.iCom;
                        }
                    });
                }
            }
            this.openModal(this.modalDetalle, 'md');
        }
    }

    // Se crea el objeto json con el detalle que sera enviado a grabar
    nuevoPlatillo(p) {
        let huesped = 0;
        let tCambio = 1;
        let clase = '';
        const form = JSON.parse(JSON.stringify(this.formCheque));
        let descuento = 0;
        if (form.clase) {
            if (form.clase.clase) {
                clase = form.clase.clase;
            } else {
                form.clase = '';
            }
        }
        if (this.huesped) {
            huesped = this.huesped.hfolio;
        }
        if (!this.descuentoGlobal && this.formCheque.descuento) { // Si hay un descuento Global, enviar al platillo el porcentaje
            descuento = this.porcDescuento;
        }
        if (this.iNumero) {
            this.iNumero = this.iNumero + 1;
        }
        this._info.getTipoCambio().subscribe(data => {
            data.filter(tcambio => {
                if (tcambio.cMoneda === p.Moneda) {
                    tCambio = tcambio.dTc;
                }
            });
        });

        const detalle = new ChequeDetalle(
            clase, this.puntoSelect.Punto, form.icheque, p.tipo, p.platillo, p.iCom, 1,
            p.pprecio, p.Moneda, tCambio, descuento, p.cNotas, p.pdesc, '', this.iNumero, '', huesped,
            form.estatus, this.ttUsuario.cUserId, false, p.Tiempo, p.Medida, '', p.cSelec, this.subtipoSelected);

        if (this.separarActive) {
            this.cerrarDesglosePorciones();
        }
        if (this.transferir) {
            this.cerrarTransferencia();
        }
        if (detalle.Platillo && detalle.dPorcion) {
            this._cheque.postDetalle(detalle).subscribe(data => {
                if (data[0].cErrDesc) {
                    swal(data[0].cErrDesc, '', 'error');
                } else {
                    this.comanda = data;
                    this.consultarCheque();
                    this.calcularTotal();
                    this.enviarComandaFilter();
                    this.comanda.forEach(det => {
                        if (detalle.Tipo === det.Tipo && detalle.Platillo === det.Platillo) {
                            this.detallePlatillo(det);
                        }
                    });
                }
                this.loadingClick = false;
            });
        }
    }

    outputActualizarPlatillo(platillo) {
        this._modalService.dismissAll();
        platillo.cUsuario = this.ttUsuario.cUserId;
        this.actualizarPlatillo(platillo);
    }

    // Se actualiza el platillo al presionar el boton Guardar. Dentro de la ventana de Detalle de Platillo
    actualizarPlatillo(platillo) {
        this._cheque.updateDetalle(platillo).subscribe(data => {
            if (data[0].cErrDesc) {
                return swal('Error!!', data[0].cErrDesc, 'error');
            }
            if (platillo.cEactual === 'X') {
                this.getEnviaComanda();
            }
            this.detalleCancelar = null;
            this.comanda = data;
            this.consultarCheque();
            this.calcularTotal();
        });
    }

    // Borrar un platillo de la comanda
    borrarPlatillo(detalle) {
        if (detalle.cRowID) {
            // detalle.dDesc = this.formCheque.descuento;
            this._cheque.deleteDetalle(detalle).subscribe(data => {
                if (!data) {
                    this.comanda = [];
                } else {
                    this.comanda = data;
                    this.consultarCheque();
                    this.calcularTotal();
                }
                if (data[0].cErrDesc) {
                    swal('Error!!!', data[0].cErrDesc, 'error');
                }
            });
        } else {
            const i = this.comanda.indexOf(detalle);
            if (i !== -1) {
                this.comanda.splice(i, 1);
            }
            this.calcularTotal();
        }
    }

    // Cancelar platillo
    async cancelarPlatillo(detalle, auth?) {

        if (this.manttoCheque[2] === 'N' && !auth) {
            this.detalleCancelar = detalle;
            this.openModal(this.modalAuth, 'sm');
            this.messageUserAuth = 'Cancelar Comanda';
            return;
        } else {
            const { value: text } = await swal({
                input: 'textarea', inputPlaceholder: 'Razón de la cancelación...', showCancelButton: true
            })
            if (text) {
                detalle.cEactual = 'X';
                this.envioPrint = 'X';
                detalle.cNotas = text;
                detalle.lEnviar = false;
                if (auth) {
                    detalle.cUsuario = auth;
                } else {
                    detalle.cUsuario = '';
                }
                this.actualizarPlatillo(detalle);
            }
        }
    }

    // Se envia a comanda el registro. Desde el Back se cambiara la bandera lEnviar a true.
    enviarComanda() {
        this.envioPrint = 'P';
        this.getEnviaComanda();
    }

    getEnviaComanda() {
        this._cheque.enviarComanda(this.chequeSelected, this.envioPrint).subscribe(data => {
            if (data) {
                // this._cheque.enviarNodejs(data).subscribe(body => {
                // })
                swal({
                    position: 'center', type: 'success', title: 'Comanda Enviada!!',
                    showConfirmButton: false, timer: 1500
                }).then(() => this.consultarCheque());
                // this.enviarComandaFilter();
            }
        });
    }

    async comandaManual(platillo) {
        const { value: comanda } = await swal({
            title: 'Ingresa número de comanda', input: 'number', inputPlaceholder: '0', showCancelButton: true,
            inputValidator: (value) => {
                return !value && 'Necesitas ingresar un número!'
            }
        })
        if (comanda) {
            platillo.iCom = comanda;
            this.nuevoPlatillo(platillo);
        }
    }

    async cambiarPrecioPlatillo(detalle) {
        if (Number(detalle.dPrecio) !== 0) {
            this.detallePlatillo(detalle);
            return;
        }
        const { value: precio } = await swal({
            title: 'Precio del platillo', input: 'number', inputPlaceholder: '$0.00', showCancelButton: true,
            inputValidator: (value) => {
                return !value && 'Necesitas ingresar un precio!'
            }
        })
        if (precio) {
            detalle.dPrecio = Number(precio);
            this.actualizarPlatillo(detalle);
        }
    }

    // -----------------  FORMA DE PAGO  ----------------- //
    pagarCheque() {
        if (!this.flagPagar) {
            setTimeout(() => this.changeTab(4), 100);
            this.flagPagar = true;
            this.listaPagosTab = [];
            this.listaPagos = [];
            this.flagVerMenu = false;
            this.flagBuscarMesa = false;
            this.flagCambioMesa = false;
            this.flagBuscarHuesped = false;
        }
    }

    guardarFpCheque(e) {
        if (e) {
            let propina = 0;
            const codigos = [];
            this.listaPagos.forEach(item => {
                propina += item.dPropTotal
                codigos.push(item.codigo);
            });
            if (propina) {
                this.chequeSelected.pprop = propina;
            }
            this.chequeSelected.dTotal = this.subtotal;
            this.chequeSelected.cNhuesp = codigos.join(',');
            this._cheque.updateCheque(this.chequeSelected).subscribe(cheque => {
                if (cheque[0].cErrDesc) {
                    swal('Registro No Modificado!!', cheque[0].cErrDesc, 'error');
                    this.chequeSelected.pea = 'I';
                    return;
                } else {
                    this.consultarCheque();
                    this._cheque.grabarPagoCheque(this.listaPagos).subscribe(data => {
                        if (data) {
                            if (data.cMensTit) {
                                swal(data.cMensTit, data.cMensTxt, 'error');
                                this.chequeSelected.pea = 'I';
                                return;
                            }
                            this.chequeSelected.pea = 'C';
                            this.formCheque.estatus = 'C';
                            swal({
                                position: 'center',
                                type: 'success',
                                title: 'Cheque Pagado!!',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(() => {
                                // Parametro PV-CHEQUES decide si se imprime cheque al cierre
                                if (this.parametrosPv && this.parametrosPv.pchr3 === 'yes') {
                                    this.flagImprimirFp = true;
                                    this.imprimirCheque = true;
                                }
                            });
                            this.chequeFp = data[0];
                            this.consultarCheque();
                            this.cerrarPago();
                            this.reiniciarCheque();
                            this.flagCambioMesa = false;
                            // this.facturarCheque(this.chequeFp.codigo);
                            // this.openModal(this.modalFactura);
                        } else {
                            swal('Error!!', 'Error al grabar la Forma de Pago!!', 'error');
                        }
                    });
                }
            });
        }
    }

    openModal(modal, size?) {
        if (!size) {
            size = 'xl';
        }
        this._modalService.open(modal, { size: size, centered: true, backdrop: 'static', keyboard: false });
    }

    pagosAgregados(e) {
        this.listaPagosTab = e;
        setTimeout(() => {
            this.listaPagos = e;
            setTimeout(() => this.changeTab(4), 100);
        });
    }

    cerrarPago() {
        if (this.formCheque.icheque) {
            this.listaPagos = [];
            this.listaPagosTab = [];
            this.flagPagar = false;
            this.flagVerMenu = false;
            setTimeout(() => this.changeTab(2), 500);
        }
    }

    cancelarPago() {
        if (this.formCheque.estatus !== 'C') {
            if (JSON.parse(localStorage.getItem('cheque-selected'))) {
                const cheque = JSON.parse(localStorage.getItem('cheque-selected'));
                this.savePropina(cheque.pprop);
            }
        }
        this.listaPagos = [];
        this.listaPagosTab = [];
        this.flagPagar = false;
        this.flagVerMenu = false;
        if (this.formCheque.estatus !== 'A') {
            this.flagBuscarMesa = true;
        }
        setTimeout(() => this.changeTab(2), 500);
    }

    deleteTabPagos(e) {
        this.listaPagosTab = [];
        setTimeout(() => this.listaPagosTab = e);
    }

    // Calcula el total, numero de platillos y el subtotal cada que se agrega o se modifica un detalle.
    calcularTotal() {
        this._ayuda.getCalculaCheque(this.chequeSelected.clase, this.chequeSelected.punto, this.chequeSelected.iCheque).subscribe(data => {
            if (!data) {
                this.comanda = [];
                return;
            }
            this.totalCalculado = data[0];
            if (this.comanda) {
                this.items = 0;
                this.subtotal = data[0].dDec1;
                this.propina = data[0].dDec2;
                this.descuento = data[0].dDec3;
                this.total = data[0].dDec4;
                this.porcDescuento = data[0].dDec5;
                this.formCheque.descuento = data[0].dDec5;
                this.chequeSelected.pporc = data[0].dDec5;
                this.comanda.forEach(item => {
                    if (item.Platillo && item.Tipo && item.cEactual !== 'X') {
                        this.items += parseFloat(item.dPorcion);
                    }
                });
            }
        });
    }

    savePropina(propina) {
        if (propina) {
            this.chequeSelected.dTotal = this.subtotal;
            this.chequeSelected.pprop = propina;
            if (this.chequeSelected) {
                this._cheque.updateCheque(this.chequeSelected).subscribe(cheque => {
                    if (cheque[0].cErrDesc) {
                        swal('Error!!', cheque[0].cErrDesc, 'error');
                        return;
                    }
                    this.consultarCheque();
                    this.calcularTotal();
                });
            }
        }
    }

    // Notifica a todos los componentes los cambios en el cheque y el huesped
    notifyChangeCheque() {
        this._info.notifyOther({ option: 'chequeSelected', value: this.chequeSelected });
        this._info.notifyOther({ option: 'huesped', value: this.huesped });
    }

    btnNumColums(value) {
        if (value === 'plus') {
            this.numColumn = this.numColumn + 1;
            this.count();
            if (this.numColumn > 8) {
                this.numColumn = 8;
                this.count();
                return;
            }
        } else {
            if (this.numColumn === 1) { return; }
            this.numColumn = this.numColumn - 1;
            this.count();
        }
    }
    count() {
        // this.columns.emit(this.numColumn.toFixed());
        localStorage.setItem('num-columns', this.numColumn.toFixed());
    }

    // Boton para aumentar el valor de in input
    btnCount(value) {
        if (value === 'plus') {
            this.formCheque.personas = this.formCheque.personas + 1;
        } else {
            this.formCheque.personas = this.formCheque.personas - 1;
            if (!this.formCheque.personas) {
                this.formCheque.personas = 1;
                return;
            }
        }
    }

    tab1() {
        if (this.separarActive) {
            this.cerrarDesglosePorciones();
        }
        if (this.transferir) {
            this.cerrarTransferencia();
        }
        if (this.formCheque.icheque) {
            this.navCheque.bPlatilloValue = '';
        }
    }

    tab2() {
        if (this.formCheque.icheque) {
            if (this.transferir) {
                this.cerrarTransferencia();
            }
            this.reiniciarBanderas('salir-huesped');
            if (this.formCheque.estatus === 'I' || this.formCheque.estatus === 'C') {
                this.flagBuscarMesa = true;
            }
        }
    }

    changeTab(tab) {
        $('#myTab a[href="#tab' + tab + '"]').tab('show');
    }

    getUserAuth(user, close) {
        this.userAuth = user;
        this._valid.getAuthorization(user.user, user.pass, 4, 1, 3, 'usuario').subscribe(async data => {
            const cval = data.pcCod.split('');
            if (cval[0] === 'S' && cval[1] === 'S') {
                switch (this.messageUserAuth) {
                    case 'Cancelar Cheque':
                        if (cval[2] === 'S') {
                            this.cancelarCheque(user.user);
                            close();
                        } else {
                            this.toast('No Autorizado!!!', '', 'info');
                        }
                        break;
                    case 'Cancelar Cheque Pagado':
                        if (cval[3] === 'S') {
                            this.cancelarCheque(user.user);
                            close();
                        } else {
                            this.toast('No Autorizado!!!', '', 'info');
                        }
                        break;
                    case 'Cancelar Comanda':
                        if (cval[4] === 'S') {
                            this.cancelarPlatillo(this.detalleCancelar, user.user);
                            close();
                        } else {
                            this.toast('No Autorizado!!!', '', 'info');
                        }
                        break;
                }
            } else {
                if (cval[0] !== 'S') {
                    this.toast('Usuario no existe!!!', '', 'warning');
                }
                if (cval[1] !== 'S') {
                    this.toast('Clave incorrecta!!!', '', 'warning');
                }
            }
        });
    }
}

