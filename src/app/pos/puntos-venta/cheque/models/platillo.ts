export class Platillo {
    constructor(
        public tipo: number,
        public platillo: number,
        public pdesc: string,
        public pclas: string,
        public pprecio: number,
        public Moneda: string,
        public codigo: string,
        public Inactivo: boolean,
        public Enviar: boolean,
        public Tiempo: number,
        public nimp: number,
        public Medida: string,
        public dplat: string,
        public cColor: string,
        public cImagen: string,
        public iSubtipo: number,
        public cRowID: string,
        public lError: boolean,
        public cErrDesc: string
    ) {}
}
