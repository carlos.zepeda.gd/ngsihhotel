export class Cheque {
	constructor(
		public clase: string, // id clase
		public punto: number, // id punto
		public iCheque: number, // numero cheque
		public Mesero: number, // mesero id
		public cMesa: string, // mesa
		public pper: number, // personas
		public pprop: number, // porpina
		public pnotas: string, // notas del cheque
		public pfolio: number, // folio huesped
		public rfc: string, // rfc cliente
		public pturno: number, // turno del cheque
		public pfecha: string, // fecha del cheque
		public usuario: string, // usuario
		public pea: string,
		public pdesc: number, 
		public pporc: number,
		public cRowID: string,
		public cpundesc: string = '', // descripcion punto
		public cclasdesc: string = '', // desc clase
		public cMesnom: string = '', // mesero nombre
		public pncheq: number = 0,
		public oldmesa: number = 0,
		public lError: boolean = false,
		public cErrDesc: string = ''
	) {}
}
