export class Punto {
	constructor(
		// tslint:disable-next-line:no-shadowed-variable
		public Punto: number,
		public pdesc: string,
		public codigo: string,
		public plinea: boolean,
		public pnombre: boolean,
		public pcomanda: boolean,
		public ppimp: boolean,
		public pprop: number,
		public pnturnos: number,
		public pfc: string,
		public pvalmes: boolean,
		public pdias: number,
		public pmre: number,
		public pnotas1: string,
		public pnotas2: string,
		public pnotas3: string,
		public pnc1: boolean,
		public pnc2: boolean,
		public pnc3: boolean,
		public pic1: string,
		public pic2: string,
		public pic3: string,
		public Inactivo: boolean,
		public pturno: number,
		public iagrp: string,
		public cRowID: string,
		public lError: boolean,
		public cErrDesc: string
	) {}
}
