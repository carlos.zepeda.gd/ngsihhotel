export class ChequeDetalle {
	constructor(
		public Clase: string, // pclas
		public punto: number, // punto de venta
		public iCheque: number, // numero de cheque
		public Tipo: number, // tipo
		public Platillo: number, // pdes
		public iCom: number, // # comanda
		public dPorcion: number,
		public dPrecio: number, // pprecio
		public Moneda: string, // Moneda
		public dTc: number, // Tipo de cambio
		public dDesc: number, // Descuento
		public cNotas: string, // Notas platillo
		public cDescripcion: string, // Descripcion platillo
		public cRFC: string,
		public iNumero: number,
		public cVoucher: string, // Voucher
		public iFolio: number,
		public cEactual: string, // Estado actual
		public cUsuario: string, // usuario
		public lEnviar: boolean, // Enviar a cocina
		public iTiempo: number, // Tiempo
		public Medida: string, // Medida 
		public cRowID: string,
		public cSelec: string,
		public iSub: number = 0,
		public cCon: string = '',
		public iPaquete: number = 0, // Paquete
		public cMotivo: string = '',
		public cPlat: string = '',
		public iPorcion: number = 0, // porciones
		public Modificador: string = '',
		public cCuenta: string = '',
		public cTipo: string = '', // descripcion tipo
		public cPlatillo: string = '', 
		public dProp: number = 0, // Propina
		public tModificador: number = 0,
		public cSubtipo: string = '', // iSubtipo
		public lError: boolean = false,
		public cErrDesc: string = ''
	) {}
}
