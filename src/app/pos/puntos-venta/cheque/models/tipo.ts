export class Tipo {
    constructor(
        public tipo: number,
        public tdesc: string,
        public tclas: string,
        public Inactivo: boolean,
        public cRowID: string,
        public lError: boolean,
        public cErrDesc: string
    ) {}
}
