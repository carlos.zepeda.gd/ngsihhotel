export class Clase {
	constructor(
		public clase: string,
		public cdesc: string,
		public civa: boolean,
		public cprop: boolean,
		public cdescto: boolean,
		public ccuarto: boolean,
		public cporc: number,
		public ccheque: boolean,
		public Inactivo: boolean,
		public cRowId: string,
		public LError: boolean,
		public cErrDesc: boolean
		) {}
}
