import { Component, ViewEncapsulation, Output, EventEmitter, Input, SimpleChanges, OnChanges } from '@angular/core';
import { orderBy, SortDescriptor } from '@progress/kendo-data-query';
import { ChequeService } from '../../services/cheque.service';

@Component({
  selector: 'app-cheques-abiertos',
  templateUrl: './cheques-abiertos.component.html',
  styles: []
})
export class ChequesAbiertosComponent implements OnChanges {

  @Output() cheque = new EventEmitter;
  @Output() close = new EventEmitter;
  @Input() mesero: string;
  @Input() punto: string;
  public dataGrid: any = [];
  public sort: SortDescriptor[] = [{
    field: 'iCheque',
    dir: 'asc'
  }];
  
  constructor(private _cheque: ChequeService) { }

  ngOnChanges(change: SimpleChanges) {
    if (change.punto !== undefined) {
      if (change.punto.currentValue) {
        this.getCheques('*');
      }
    }
  }
  getCheques(estatus) {
    this._cheque.getCheques(this.mesero, estatus, this.punto).subscribe( data => {
        if (!data) { data = ''; }
        this.dataGrid = data;
        this.dataGrid = {
          data: orderBy(this.dataGrid, this.sort),
          total: this.dataGrid.length
        };
    });
  }
  selectRow(event) {
    const i = event.index;
    this.cheque.emit(this.dataGrid.data[i]);
    setTimeout(() => this.cerrar()); 
  }
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataStructure();
  }
  private dataStructure(): void {
    this.dataGrid = {
        data: orderBy(this.dataGrid.data, this.sort),
        total: this.dataGrid.data.length
    };
  }
  cerrar() {
    this.close.emit(true);
  }

}
