import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { NotasService } from '../../../../../pms/recepcion/check-in/services/service-checkin.index';
import { InfociaGlobalService } from '../../../../../services/infocia.service';

@Component({
  selector: 'app-info-huesped',
  templateUrl: './info-huesped.component.html',
  styleUrls: ['./info-huesped.component.css']
})
export class InfoHuespedComponent implements OnInit, OnChanges {
  @Input() huesped: any;
  @Output() close = new EventEmitter;
  public ttInfocia: any = [];
  public fday: string;
  formatKendo = localStorage.getItem('formatKendo');
  dataNotas: any = [];
  
  constructor(private _info: InfociaGlobalService,
    private _notas: NotasService) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.fday = this.ttInfocia.fday;
  }

  ngOnInit() {
  }
  
  ngOnChanges(change: SimpleChanges) {
    if (change['huesped']['currentValue']) {
      this._notas.getNotes(this.huesped.hfolio, 'H').subscribe(data => {
        if (data && data.ttNotas) {
          this.dataNotas = data.ttNotas;
        }
      });
    }
  }

  closeDialog() {
    this.close.emit(true);
  }
}
