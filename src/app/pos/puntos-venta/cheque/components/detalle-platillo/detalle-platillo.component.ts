import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-detalle-platillo',
  templateUrl: './detalle-platillo.component.html',
  styleUrls: ['./detalle-platillo.component.css']
})
export class DetallePlatilloComponent implements OnInit, OnChanges {
  @Input() descuento: any;
  @Input() detalle: any; // Detalle del platillo
  @Input() cheque: any; // Cheque actual
  @Input() comandaAut: boolean;
  @Output() close = new EventEmitter;
  @Output() update = new EventEmitter;
  public totalModif: number = 1;
  public descuentoPlatillo: boolean;
  public modificado: any;
  public detallePlatillo: {
    dPorcion: number; dPrecio: number, iTiempo: number; iCom: number; dDesc: number; cNotas: any; cEactual: string; lEnviar: boolean
  };
  public viewModificadores: boolean;

  constructor() { }

  ngOnInit() {

  }
  ngOnChanges(change: SimpleChanges) {
    if (change.detalle && change.detalle.currentValue) {
      this.mostrarDetallePlatillo(this.detalle);
    }
    if (change.descuento && change.descuento.currentValue) {
      if (this.descuento !== 'yes') {
        this.descuentoPlatillo = false;
      }else {
        this.descuentoPlatillo = true;
      }
    }
  }
  closeDialog() {
    this.close.emit(true);
  }
  totalModificadores(e) {
    this.totalModif = e;
  }
  resultModificadores(e) {
    this.detalle.Modificador = e.Modificador;
  }
  mostrarDetallePlatillo(detalle) {
    this.detallePlatillo = {
      dPorcion: detalle.dPorcion,
      iTiempo: detalle.iTiempo,
      iCom: detalle.iCom,
      dDesc: detalle.dDesc,
      cNotas: detalle.cNotas,
      cEactual: detalle.cEactual,
      lEnviar: detalle.lEnviar,
      dPrecio: detalle.dPrecio
    }
  }
  verModificadores() {
    this.viewModificadores = true;
  }
  formActualizarDetalle(form) {
    const val = form.value;
    this.detalle.cNotas = val.cNotas;
    if (this.descuentoPlatillo && val.dDesc) {
      this.detalle.dDesc = val.dDesc;
    } else {
      this.detalle.dDesc = 0;
    }
    this.detalle.dPorcion = val.dPorcion;
    this.detalle.iCom = val.iCom;
    this.detalle.iTiempo = val.iTiempo;
    this.detalle.dPrecio = val.dPrecio;
    this.update.emit(this.detalle);
  }
  // Al presionar en un registro de la comanda aparece una ventana donde se puede cambiar las porciones del platillo
  cambiarPorcion() {
    // this.detallePlatillo.dPorcion = Math.round(this.detallePlatillo.dPorcion);
    if (!this.detallePlatillo.dPorcion) { this.detallePlatillo.dPorcion = 1; }
    if (this.detallePlatillo.dPorcion >= 1000) {
      this.detallePlatillo.dPorcion = 999;
    }
  }
  // Al presionar en un registro de la comanda aparece una ventana donde se puede cambiar el Tiempo del platillo
  cambiarTiempo() {
    this.detallePlatillo.iTiempo = Math.round(this.detallePlatillo.iTiempo);
    if (!this.detallePlatillo.iTiempo) { this.detallePlatillo.iTiempo = 1; }
  }
  cambiarComanda() {
    this.detallePlatillo.iCom = Math.round(this.detallePlatillo.iCom);

    if (this.detallePlatillo.iCom < 0) {
      this.detallePlatillo.iCom = 0;
    }
    if (this.detallePlatillo.iCom > 9999) {
      this.detallePlatillo.iCom = 9999;
    }
  }
  // Boton para aumentar el valor de in input
  btnPlus(value) {
    switch (value) {
      case 'porcion':
        this.detallePlatillo.dPorcion = this.detallePlatillo.dPorcion + 1;
        break;
      case 'tiempo':
        this.detallePlatillo.iTiempo = this.detallePlatillo.iTiempo + 1;
        break;
    }
  }
  // Boton para decrementar el valor de in input
  btnMinus(value) {
    switch (value) {
      case 'porcion':
        this.detallePlatillo.dPorcion = this.detallePlatillo.dPorcion - 1;
        if (!this.detallePlatillo.dPorcion) {
          this.detallePlatillo.dPorcion = 1;
        }
        break;
      case 'tiempo':
        this.detallePlatillo.iTiempo = this.detallePlatillo.iTiempo - 1;
        if (!this.detallePlatillo.iTiempo) {
          this.detallePlatillo.iTiempo = 1;
          return;
        }
        break;
    }
  }
}
