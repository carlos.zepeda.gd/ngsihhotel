import { Component, OnInit, Input, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { ChequeAyudaService } from '../../services/cheque-ayuda.service';

@Component({
  selector: 'app-modificadores-cheque',
  templateUrl: './modificadores-cheque.component.html',
  styleUrls: ['./modificadores-cheque.component.css']
})
export class ModificadoresChequeComponent implements OnInit, OnChanges {
  @Input() platillo: any;
  @Input() cheque: any;
  @Output() close = new EventEmitter;
  @Output() updateModif = new EventEmitter;
  @Output() total = new EventEmitter;
  public allModif: Array<any> = [];
  public modificadores: any;
  public viewModificadores: boolean;
  loading: boolean;

  constructor(private _cheque: ChequeAyudaService) { }

  ngOnInit() {
  }
  ngOnChanges(change: SimpleChanges) {
    if (change.platillo && change.platillo.currentValue) {
      this.verModificadores(this.platillo);
    }
  }
  closeDialog() {
    if (this.allModif) { this.allModif = []; }
    this.close.emit(true);
  }
  verModificadores(platillo) {
    if (platillo.Modificador) {
      this.allModif = platillo.Modificador.split(',');
    }
    this.loading = true;
    this._cheque.getModificadores(platillo.Tipo, platillo.Platillo, 0).subscribe(data => {
      if (data) {
        this.modificadores = data;
        this.total.emit(this.modificadores.length);
        this.updateModif.emit(this.platillo);
        this.viewModificadores = true;
        if (this.allModif.length) {
          this.modificadores.forEach(item1 => {
            this.allModif.forEach(item2 => {
              if (item1.modificador === item2) { 
                item1.psel = 'true'; 
              }
            });
          });
        }
      } else {
        this.total.emit(0);
      }
      this.loading = false;
    });
  }
  seleccionarModificador(modificador) {
    // Si ya existe el registro lo elimina
    if (!this.platillo.lEnviar && this.cheque.pea === 'A') {
      for (const mod of this.allModif) {
        if (mod === modificador.modificador) {
          const i = this.allModif.indexOf(mod);
          if (i !== -1) {
            this.allModif.splice(i, 1);
            // this.platillo.dPrecio = this.platillo.dPrecio - modificador.pprecio;
          }
          this.platillo.Modificador = this.allModif.toString();
          this.verModificadores(this.platillo);
          return;
        }
      }
      // Agregar un nuevo modificador a la tabla de cheqdet
      if (this.allModif.length >= 0) {
        this.allModif.push(modificador.modificador);
        // this.platillo.dPrecio = this.platillo.dPrecio + modificador.pprecio;
        this.platillo.Modificador = this.allModif.toString();
        this.verModificadores(this.platillo);
        return;
      }
    }
  }
}
