import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ChequeAyudaService } from '../../services/cheque-ayuda.service';
import { Subscription } from 'rxjs/Subscription';
import swal from 'sweetalert2';

@Component({
  selector: 'app-tab-pagos',
  templateUrl: './tab-pagos.component.html',
  styleUrls: ['./tab-pagos.component.css'],
  providers: [
  ]
})
export class TabPagosComponent implements OnInit, OnChanges, OnDestroy {
  balance: number = 0;
  totalPagado: number = 0;
  propinaPagado: number = 0;
  subtotalPagado: number = 0;
  total: number = 0;
  descuento: number = 0;
  propina: number = 0;
  subtotal: number = 0;
  items: number = 0;
  @Input() pagos: any = [];
  @Input() cheque: any;
  @Output() delete = new EventEmitter;
  @Output() cerrar = new EventEmitter;
  @Output() result = new EventEmitter;
  @Output() fpago = new EventEmitter;
  public listaPagos: any = [];
  public subscription: Subscription;
  public flagIntoFp: boolean;
  public enabledBtnAceptar: boolean;
  private porcDescuento: number = 0;
  loadingClick: boolean;

  constructor(private _cheque: ChequeAyudaService) {
    this.subscription = this._cheque.notifyObservable$.subscribe((res) => {
      if (res.option === 'calcularTotal') { this.calcularTotal('subscription'); }
    });
  }
  ngOnInit() {
  }
  borrarPago(index) {
    this.pagos.splice(index, 1);
    this.delete.emit(this.pagos);
    this.calcularTotal('borrar Pago');
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.pagos) {
      if (this.cheque.pea === 'C') {
        this.mostrarFormaPago();
      }
      this.listaPagos = changes.pagos.currentValue;
      if (changes.pagos.currentValue) {
        this.listaPagos = changes.pagos.currentValue;
        this.calcularTotal('pagos onchanges');
        this.enabledBtnAceptar = true;
      } else {
        this.listaPagos = changes.pagos.currentValue;
      }
    }
    if (changes.cheque) {
      if (changes.cheque.currentValue) {
        if (this.cheque.pea === 'C') {
          this.mostrarFormaPago();
        }
        this.calcularTotal('cheque change');
      }
    }
  }
  mostrarFormaPago() {
    this._cheque.getChequeFp(this.cheque.clase, this.cheque.punto, this.cheque.iCheque)
      .subscribe(data => {
        if (data) {
          this.pagos = data;
          this.calcularTotal('mostrar forma pago');
        }
      });
  }
  cerrarPago() {
    this.flagIntoFp = false;
    this.cerrar.emit(true);
    this.enabledBtnAceptar = false;
  }
  ngOnDestroy(): void {
    this.cerrar.emit(true);
    this.pagos = [];
  }
  aceptarPago() {
    if (this.balance === 0) {
      this.cheque.pporc = this.porcDescuento;
      this.result.emit(this.cheque);
      this.enabledBtnAceptar = false;
    } else {
      swal('Revisar balance', 'Balance debe estar en 0!!', 'warning').then(() => this.calcularTotal('balance 0'));
    }
  }
  calcularTotal(value) {
    if (this.cheque) {
      this.subtotalPagado = 0;
      this.propinaPagado = 0;
      this.totalPagado = 0;
      this.balance = 0;
      if (this.pagos) {
        this.pagos.forEach(pago => {
          this.subtotalPagado += pago.dMonto;
          this.propinaPagado += pago.dPropTotal;
        });
      }
      this.totalPagado = Number(this.subtotalPagado) + Number(this.propinaPagado);
      this.totalPagado = Number(this.totalPagado.toFixed(2));
      this._cheque.getCalculaCheque(this.cheque.clase, this.cheque.punto, this.cheque.iCheque).subscribe(data => {
        this.loadingClick = false;
        if (data) {
          this.subtotal = data[0].dDec1;
          this.propina = data[0].dDec2;
          this.descuento = data[0].dDec3;
          this.total = data[0].dDec4;
          this.porcDescuento = data[0].dDec5;
          this.balance = Number(this.total - this.totalPagado);
        }
      });
    }

  }
}
