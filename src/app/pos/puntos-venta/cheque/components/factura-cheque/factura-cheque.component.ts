import {Component, OnInit, Input, Output, OnChanges, SimpleChanges, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ChequeFacturaService} from '../../services/cheque.factura.service';
import {Tgeneral} from '../../../../../models/tbl-general';
import {GLOBAL} from '../../../../../services/global';
import swal from 'sweetalert2';

const esp = GLOBAL.char174;

@Component({
    selector: 'app-factura-cheque',
    templateUrl: './factura-cheque.component.html',
    styleUrls: ['./factura-cheque.component.css'],
    providers: [ChequeFacturaService]
})
export class FacturaChequeComponent implements OnInit, OnChanges {
    @Input() facturar: boolean;
    @Input() cheque: any;
    @Output() finished = new EventEmitter;
    facturaForm: FormGroup;
    dataCliente: any;
    dsEstados: any;
    flagState: boolean;
    estadoSelected: any = {};
    flagRfc: boolean;
    dsConsecutivos: any;
    formGeneral: any;
    dsTiposCfd: any = [];
    formEnviar: Tgeneral;
    usocfdSel: any;
    public maskRfc = '?LLL-000000-???';
    public maskPhone = '(000) 000-0000???';
    rfcSelected: any;
    dsParametroRfc: any = [];
    etiquetaRfc: string = 'R.F.C.';

    constructor(
        private fb: FormBuilder,
        private _fact: ChequeFacturaService
    ) {
        this._fact.getCFD().subscribe(data => {
            data.forEach(item => item.cDesc = item.cDesc + ' (' + item.cClave + ')');
            this.dsTiposCfd = data;
            this.usocfdSel = this.dsTiposCfd[0];
        });
        this._fact.getConsecutivos('p').subscribe(consec => this.dsConsecutivos = consec);
        this._fact.getParametros('rfc').subscribe(data => {
            if (data) {
                this.dsParametroRfc = data[0];
                this.etiquetaRfc = data[0].pchr1;
                this.maskRfc = data[0].pchr2.replace(/X/g, 'a');
            }
        });
    }

    ngOnInit() {
        this.createForm();
        this.createTableGeneral();
    }

    estadoValue(e) {
        this.estadoSelected = e;
        this.flagState = false;
        this.facturaForm.value.estado = e.Estado;
    }

    enviarEmail() {
        if (this.rfcSelected) {
            if (!this.formGeneral.enviar) {
                this.formGeneral.emailEnv = this.rfcSelected.remail;
            } else {
                this.formGeneral.emailEnv = '';
            }
        }
    }

    rfcValue(e) {
        this.rfcSelected = e;
        this.facturaForm.setValue(e);
        this.flagRfc = false;
    }

    buscarEstados() {
        this.flagState = true;
    }

    buscarRfc() {
        this.flagRfc = true;
    }

    consecutivoSelected(e) {
        this.formGeneral.factura = e.iNum;
    }

    createTableGeneral() {
        this.formGeneral = {
            consecutivo: '', usoCfd: 'G03', factura: '',
            emailEnv: '', consumo: '', enviar: false
        };
    }

    createForm() {
        this.facturaForm = this.fb.group({
            rfc: [''],
            Sucursal: [''],
            rrsocial: [''],
            rdireccion1: [''],
            rdireccion2: [''],
            exterior: [''],
            interior: [''],
            rciudad: [''],
            estado: [''],
            rcp: [''],
            rtelefono1: [''],
            rtelefono2: [''],
            rfax: [''],
            rcontacto: [''],
            rpuesto: [''],
            remail: [''],
            rnotas: [''],
            Inactivo: [false],
            renv: [''],
            Usuario: [''],
            cRowID: [''],
            lError: [false],
            cErrDes: ['']
        });
    }

    onSubmit() {
        this.dataCliente = this.facturaForm.value;
        if (this.facturaForm.value.rfc && this.rfcSelected) {
            this._fact.putCliente(this.dataCliente).subscribe(data => {
                if (data) {
                    this.resultFactura(data);
                }
            });
        }
        if (!this.facturaForm.value.rfc && !this.rfcSelected) {
            this.facturarFolio();
        }
        if (this.facturaForm.value.rfc && !this.rfcSelected) {
            this._fact.postCliente(this.dataCliente).subscribe(data => {
                if (data) {
                    this.resultFactura(data);
                }
            });
        }
    }

    resultFactura(data) {
        this.dataCliente = data[0];
        if (this.dataCliente.cErrDes) {
            this.cerrarFactura();
            swal('Error al grabar R.F.C.!', this.dataCliente.cErrDes, 'warning');
        } else {
            this.facturarFolio();
        }
    }

    facturarFolio() {
        const val = this.formGeneral;
        let sucursal = '';
        let rfcSucursal = '';
        if (this.rfcSelected !== undefined) {
            sucursal = this.rfcSelected.Sucursal;
        }
        if (this.facturaForm.value.rfc) {
            rfcSucursal = this.facturaForm.value.rfc + '|' + sucursal;
        }
        this.formEnviar = new Tgeneral(
            val.consecutivo.cCon + esp + this.cheque.iCheque + esp + rfcSucursal +
            esp + 'P,"",' + val.usoCfd, '', '', '', '', '',
            this.cheque.clase + ',' + this.cheque.punto + ',' + this.cheque.iCheque, // char 7
            this.facturaForm.value.rnotas // char 8
        );
        if (this.formGeneral.consumo) {
            this.formEnviar.lLog1 = this.formGeneral.consumo;
        } else {
            this.formEnviar.lLog1 = 'no';
        }
        this.formEnviar.lLog2 = true;
        this._fact.putFactura(this.cheque, this.formEnviar).subscribe(factura => {
            const sihFact = factura.dsFactura;
            if (sihFact.BttTgeneral[0].cChr9) {
                swal(sihFact.BttTgeneral[0].cChr9, '', 'warning');
            } else {
                if (sihFact.BttRfccias) {
                    this.rebuildForm();
                    this.finished.emit('facturado');
                    const fileName = factura.dsTgeneral.ttTgeneral[0];
                    this.crearFactura2(sihFact, fileName);
                }
            }
            this.cerrarFactura();
        });
    }

    cerrarFactura() {
        this.rebuildForm();
        this.finished.emit('close');
    }

    rebuildForm() {
        this.facturaForm.reset({
            rfc: '', Sucursal: '', rrsocial: '', rdireccion1: '', rdireccion2: '',
            exterior: '', interior: '', rciudad: '', estado: '', rcp: '', rtelefono1: '',
            rtelefono2: '', rfax: '', rcontacto: '', rpuesto: '', remail: '', rnotas: '',
            Inactivo: false, renv: '', Usuario: '', cRowID: '', lError: false, cErrDes: ''
        });
    }

    crearFactura2(val, file) {
        let enviarEmail = true;
        if (!this.formGeneral.emailEnv) {
            enviarEmail = false;
        }
        const json = {
            propiedad: 'SIH',
            config: '003FACT',
            nombrepdf: file.cChr11,
            xml: '<xml></xml>',
            BttConsec: val.BttConsec[0],
            ttCheq: val.ttCheq[0],
            ttCheqd: val.ttCheqd,
            BttRfccias: val.BttRfccias[0],
            ttCias: val.ttCias[0],
            email: {
                enviar: enviarEmail,
                para: this.formGeneral.emailEnv
            }
        }
        this._fact.postGenerarFactura3(JSON.stringify(json)).subscribe(data => {
            const result = JSON.parse(data._body);
            const data64 = result.pdf.split(/\n/);
            let cadena = '';

            for (let i = 6; i < data64.length; i++) {
                cadena += data64[i];
            }
            const linkSource = 'data:application/pdf;base64,' + cadena;
            const download = document.createElement('a');
            // Concatenar extensión factura
            const fileName = file.cChr11 + '.pdf';
            download.href = linkSource;
            download.download = fileName;
            download.click();
        });
    }
 
    ngOnChanges(change: SimpleChanges) {
        if (change.facturar !== undefined) {
            if (change.facturar.currentValue) {

            }
        }
        if (change.cheque !== undefined) {

        }
    }

}
