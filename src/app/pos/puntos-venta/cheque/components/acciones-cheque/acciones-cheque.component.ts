import { Component, OnInit, Input, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import swal from 'sweetalert2';
import { InfociaGlobalService } from '../../../../../services/infocia.service';

declare var $: any;

@Component({
  selector: 'app-acciones-cheque',
  templateUrl: './acciones-cheque.component.html',
  styleUrls: ['./acciones-cheque.component.css']
})
export class AccionesChequeComponent implements OnInit, OnChanges {
  @Input() reiniciar: string = 'n';
  @Input() formCheque: any;
  @Input() total: number;
  @Output() accion = new EventEmitter;
  public nuevo: boolean; public editar: boolean;
  private ttUsuario: any; 
  public manttoCheque: any;
  public showAuth: boolean;
  
  constructor(private _info: InfociaGlobalService) { }

  ngOnInit() {
    this.ttUsuario = this._info.getSessionUser();
    this.manttoCheque = this.ttUsuario.cVal4.split('');
  }
  ngOnChanges(change: SimpleChanges) {
    if (change['reiniciar'] !== undefined) {
      if (change.reiniciar.currentValue) {
        this.reiniciar === 's' ? this.nuevo = false : this.nuevo = true;
      }
    }
    if (change['formCheque']) {
      if (change.formCheque.currentValue.icheque) {
        this.nuevo = false;
      }
    }
  }
  accionCheque(accion: string) {
    if (accion === 'nuevo') { this.nuevo = true; }
    if (accion === 'editar') { this.editar = true; }
    if (accion === 'pagar') { if (this.manttoCheque[3] === 'S') {} }
    this.accion.emit(accion);
  }
  closeAuth() {
    this.showAuth = false;
  }
  userAuth($event) {
    this.closeAuth();
  }
}
