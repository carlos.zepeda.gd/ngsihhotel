import { Component, OnInit, Input, Output, SimpleChanges, OnChanges, EventEmitter } from '@angular/core';
import { InfociaGlobalService } from '../../../../../services/infocia.service';
import { ChequeAyudaService } from '../../services/cheque-ayuda.service';

@Component({
  selector: 'app-navbar-cheque',
  templateUrl: './navbar-cheque.component.html',
  styleUrls: ['./navbar-cheque.component.css']
})
export class NavbarChequeComponent implements OnInit, OnChanges {
  @Input() chequesMesa: any = [];
  @Input() huesped: any;
  @Input() punto: any;
  @Input() view: boolean;
  @Input() borrar: boolean;
  @Input() reiniciar: string = 'n';
  @Input() formCheque: any;
  @Input() total: number;
  @Output() result = new EventEmitter;
  @Output() columns = new EventEmitter;
  @Output() backTo = new EventEmitter;
  @Output() buscar = new EventEmitter;
  @Output() accion = new EventEmitter;
  @Output() changeCheque = new EventEmitter;
  @Output() turno = new EventEmitter;
  public nuevo: boolean;
  public editar: boolean;
  public manttoCheque: any;
  public numColumn = 3;
  public puntos: any = [];
  public bPlatilloValue: string;
  nuevoTurno: any;

  constructor(private _cheque: ChequeAyudaService, private _info: InfociaGlobalService) {
    this._cheque.getPuntos().subscribe(data => {
      if (data) {
        this.puntos = data;
        const point = JSON.parse(localStorage.getItem('pointSelected'));

        point ? this.listPointSelected(point) : this.listPointSelected(data[0]);
        if (localStorage.getItem('num-columns')) {
          this.numColumn = Number(localStorage.getItem('num-columns'));
        }
      } else {
        this.puntos = [];
      }
    });
  }
  ngOnInit() {
    const ttUsuario = this._info.getSessionUser();
    this.manttoCheque = ttUsuario.cVal4.split('');
  }
  ngOnChanges(changes: SimpleChanges): void {

    if (changes.borrar && changes.borrar.currentValue) {
      this.bPlatilloValue = '';
    }
    if (changes.reiniciar && changes.reiniciar.currentValue) {
      this.reiniciar === 's' ? this.nuevo = false : this.nuevo = true;
    }
    if (changes.formCheque) {
      if (changes.formCheque.currentValue.icheque) {
        this.nuevo = false;
      }
    }
  }
  changeFolio(e) {
    this.changeCheque.emit(e);
  }
  cambiarTurno() {
    this.nuevoTurno.pturno = this.nuevoTurno.sigturno;
    this._cheque.putPuntoTurno(this.nuevoTurno).subscribe(data => {
      this.nuevoTurno = data;
      this.listPointSelected(this.punto);
    })
  }
  listPointSelected(value) {
    this.punto = value;
    this._cheque.getPuntoTurno(this.punto.Punto).subscribe(det => {
      this.nuevoTurno = det;
      this.result.emit(value);
    });
  }
  btnNumColums(value) {
    if (value === 'plus') {
      this.numColumn = this.numColumn + 1;
      this.count();
      if (this.numColumn > 8) {
        this.numColumn = 8;
        this.count();
        return;
      }
    } else {
      if (this.numColumn === 1) { return; }
      this.numColumn = this.numColumn - 1;
      this.count();
    }
  }
  count() {
    this.columns.emit(this.numColumn.toFixed());
    localStorage.setItem('num-columns', this.numColumn.toFixed());
  }
  buscarPlatillos(e) {
    this.buscar.emit(e);
  }
  regresar() {
    this.backTo.emit(true);
  }
  accionCheque(accion: string) {
    if (accion === 'nuevo') { this.nuevo = true; }
    if (accion === 'editar') { this.editar = true; }
    if (accion === 'pagar') { if (this.manttoCheque[3] === 'S') { } }
    this.accion.emit(accion);
  }
}
