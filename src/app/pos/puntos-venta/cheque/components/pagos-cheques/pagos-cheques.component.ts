import { Component, OnInit, Input, Output, OnChanges, SimpleChanges, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { SelectableSettings } from '@progress/kendo-angular-grid';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BuscarService } from '../../../../../pms/recepcion/check-in/services/childs/buscar.service';
import { InfociaGlobalService } from '../../../../../services/infocia.service';
import { ChequeAyudaService } from '../../services/cheque-ayuda.service';
import { ChequeService } from '../../services/cheque.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-pagos-cheques',
  templateUrl: './pagos-cheques.component.html',
  styleUrls: ['./pagos-cheques.component.css']
})
export class PagosChequesComponent implements OnInit, OnChanges {
  @ViewChild('modalBuscar') modalBuscar: TemplateRef<any>;
  @ViewChild('modalHuesped') modalHuesped: TemplateRef<any>;
  tipoCambio: any;
  ttInfocia: any;
  codigoSelect: any;
  fpSelected: boolean;
  monedaCia: string;
  @Input() cheque: any;
  @Input() comanda: any;
  @Input() clase: any;
  @Input() parametros: any;
  @Input() listaPagos: any;
  @Output() update = new EventEmitter;
  @Output() pagos = new EventEmitter;
  public rowActive: any;
  public eventGrid: any;
  private ttUsuario: any;
  public updatePago: boolean;
  public pagoSelected: any;
  public dataCodigos: Array<{ cdesc: string, Codigo: string }> = [];
  public dataMonedas: Array<{ mdesc: string, Moneda: string }> = [];
  public formPago: { descuento: number; notas: string; nombre: string; servicio: number; };
  public huesped: any;
  public habitacion: { cuarto: string, folio: string, hfp: string, limite: number }
  public dataGrid: any = [];
  public verFormPago: boolean;
  public selectableSettings: SelectableSettings;
  public checkboxOnly: boolean;
  public mode: any = 'single';
  private chequeFp: {
    Clase: string; punto: number; iCheque: number; cCuenta: string; codigo: string; dMonto: number;
    dProp: number; Moneda: string; dTc: number; cUsuario: string; cVoucher: string; dPropTotal: number; dDescTotal: number;
    cNotas: string; cRowID: string; lError: boolean; cErrDesc: string;
  };
  public totalPago: { subtotal: number; descuento: number; servicio: number; total: number; }
  public totalPagado: { subtotal: number; servicio: number; total: number; balance: number; }
  public nuevoPago: { pCodigo: string; voucher: string; subtotal: number; servicio: number; moneda: string; tipocambio: number; }
  codigoHabitacion: any;
  private cambioFp: boolean;
  public codigofpRepetido = [];
  porcDescuento: number = 0;
  loadingClick: boolean;

  constructor(
    private _buscar: BuscarService,
    private _cheque: ChequeService,
    private _info: InfociaGlobalService,
    private _ayuda: ChequeAyudaService,
    private _modalService: NgbModal
  ) {
    this.setSelectableSettings();
    this.setInfoCheque();
    this.habitacion = { cuarto: '', folio: '', hfp: '', limite: 0 };
    this.ttUsuario = this._info.getSessionUser();
    this.ttInfocia = this._info.getSessionInfocia();
    this._ayuda.getCodigos().subscribe(data => {
      if (data) {
        this.dataCodigos = data;
        this.dataCodigos.forEach(item => {
          if (item.Codigo === 'HH') {
            this.codigoHabitacion = item;
          }
        });
      }
    });
    this._ayuda.getMonedas().subscribe(data => this.dataMonedas = data);
    this.pagoSelected = JSON.parse(localStorage.getItem('pointSelected'));
  }

  ngOnInit() {
    this.monedaCia = this.ttInfocia.Moneda;
    this._info.getTipoCambio().subscribe(data => {
      if (data) {
        data.filter(tcambio => {
          if (tcambio.cMoneda === this.monedaCia) { this.tipoCambio = tcambio.dTc; }
        });
      }
    });
  }

  ngOnChanges(change: SimpleChanges) {
    if (change.cheque !== undefined) {
      if (change.cheque.currentValue) {
        this.formPago.notas = this.cheque.pnotas;
        this.formPago.descuento = this.porcDescuento;
        this.formPago.servicio = this.cheque.pprop;
        if (this.cheque.pea === 'C' && !this.cambioFp) {
          this.mostrarFormaPago();
        } else {
          this.listaPagos = [];
        }
        if (this.cheque.pfolio !== this.habitacion.folio) {
          this.getHuesped();
        } else {
          // this.huesped = [];
        }
        // Se ejecuta una vez que se llama al numero de cheque
        this.calcularTotalPago('Onchanges change.cheque');
        this.fpSelected = true;
      }
    }
    if (change.parametros !== undefined) {
      if (change.parametros.currentValue) {
        // Descuento por platillo
        if (this.parametros.pchr4 === 'yes') {
          this.clase.cdescto = false;
        }
      }
    }
    if (change.listaPagos !== undefined) {
      if (change.listaPagos.currentValue) {
        this.dataGrid = change.listaPagos.currentValue;
        this.calcularTotalPago('onchanges lista pagos if');
      } else {
        this.calcularTotalPago('onchanges lista pagos else');
      }
    } else {
      this.calcularTotalPago('onchanges lista pagos else 2');
    }
    if (change.comanda !== undefined) {
      if (change.comanda.currentValue) {
        this.calcularTotalPago('comanda');
      }
    }
  }

  consultarCheque(notify?) {
    const c = this.cheque;
    this._ayuda.getCheque(c.Mesero, c.clase, c.punto, c.iCheque).subscribe(data => {
      if (data) {
        this.cheque = data[0];
        this.update.emit(data[0]);
      }
    });
  }

  mostrarFormaPago() {
    this._cheque.getChequeFp(this.cheque.clase, this.cheque.punto, this.cheque.iCheque).subscribe(data => {
      if (data) {
        this.dataGrid = data;
        this.calcularTotalPago('mostrar forma pago');
      }
    });
  }

  openModal(modal, size?) {
    if (!size) {
      size = 'xl';
    }
    this._modalService.open(modal, { size: size });
  }

  btnInfoHuesped() {
    if (this.huesped.hfolio) {
      this.openModal(this.modalHuesped);
    }
    this.cheque.pfolio = this.huesped.hfolio;
    this.cheque.pporc = this.formPago.descuento;
    this.cheque.dTotal = Number(this.totalPago.subtotal);
    this._cheque.updateCheque(this.cheque).subscribe(data => {
      if (data[0].cErrDesc) {
        swal('Error!!', data[0].cErrDesc, 'error');
      } else {
        this.consultarCheque();
      }
    });
  }

  getHuesped() {
    if (this.cheque.pfolio) {
      this._buscar.getHuesped(this.cheque.pfolio, 'folio').subscribe(data => {
        if (data) {
          this.huesped = data[0];
          this.setHabitacion();
        } else {
          this.huesped = [];
          swal('Error!!', 'Folio incorrecto!!', 'error');
        }
      });
    }
  }
  setHabitacion() {
    if (this.huesped) {
      this.habitacion = {
        cuarto: this.huesped.cuarto, folio: this.huesped.hfolio,
        hfp: this.huesped.hfp, limite: this.huesped.hlimite
      }
      this.formPago.nombre = this.huesped.hapellido + ' ' + this.huesped.hnombre;
    }
    if (this.totalPagado.balance) {
      this.seleccionaFp(this.codigoHabitacion);
    }
  }
  changeBuscarCuarto(value) {
    if (!this.habitacion.cuarto) {
      this.huesped = [];
      this.codigoSelect = null;
      this.btnInfoHuesped();
    } else {
      this._buscar.getHuesped(this.habitacion.cuarto, 'cuarto').subscribe(data => {
        if (!data) {
          swal('Error!!', 'Cuarto incorrecto!!', 'error').then(() => this.habitacion.cuarto = '');
        } else {
          this.huesped = data[0];
          this.cheque.pfolio = this.huesped.hfolio;
          this.setHabitacion();
          this.btnInfoHuesped();
        }
      });
    }
  }
  
  resultHuesped(e) {
    this.huesped = e;
    this.cheque.pfolio = this.huesped.hfolio;
    this.setHabitacion();
    this.btnInfoHuesped();
  }
  
  currencyInputChanged(value) {
    const num = value.replace(/[$,]/g, '');
    return Number(num);
  }

  cambiarServicio(servicio) {
    this.cambioFp = true;
    if (servicio < 0) {
      servicio = 0;
      this.formPago.servicio = 0;
    }
    if (!servicio) {
      servicio = 0;
      this.formPago.servicio = 0;
    }
    this.cheque.pprop = servicio;
    this.cheque.pporc = this.formPago.descuento;
    this.cheque.dTotal = Number(this.totalPago.subtotal);
    this._cheque.updateCheque(this.cheque).subscribe(data => {
      if (data[0].cErrDesc) {
        swal('Error!!', data[0].cErrDesc, 'error');
      } else {
        this.consultarCheque();
        this.calcularTotalPago('cambiar servicio');
        this._cheque.notifyOther({ option: 'calcularTotal', value: true });
      }
    });
  }

  cambioDescuento(descuento) {
    this.cambioFp = true;
    if (descuento > 100) {
      descuento = 100;
      this.formPago.descuento = 100;
    }
    if (!descuento) {
      descuento = 0;
      this.formPago.descuento = 0;
    }
    this.cheque.pporc = descuento;
    this.cheque.dTotal = Number(this.totalPago.subtotal);
    this._cheque.updateCheque(this.cheque).subscribe(data => {
      if (data[0].cErrDesc) {
        swal('Error!!', data[0].cErrDesc, 'error');
      } else {
        this.consultarCheque('notify');
        this.calcularTotalPago('cambiar descuento');
        this._cheque.notifyOther({ option: 'calcularTotal', value: true });
      }
    });
  }

  setInfoCheque() {
    this.formPago = { descuento: 0, notas: 'Gracias por su visita', nombre: '', servicio: 0 }
    this.totalPago = { subtotal: 0, descuento: 0, servicio: 0, total: 0 }
    this.totalPagado = { subtotal: 0, servicio: 0, total: 0, balance: 0 }
  }

  addFormPago() { // Rellena los inputs para pagar
    this.cambioFp = true;
    let codigo = '';
    if (this.habitacion.folio) { codigo = 'HH'; }
    let subtotal = (this.totalPago.subtotal - this.totalPago.descuento) - this.totalPagado.subtotal;
    subtotal = Number(subtotal.toFixed(2));
    let servicio = this.totalPago.servicio - this.totalPagado.servicio;
    servicio = Number(servicio.toFixed(2));
    this.verFormPago = true;
    this.nuevoPago = {
      pCodigo: codigo, voucher: '', subtotal: subtotal, servicio: servicio,
      moneda: this.monedaCia, tipocambio: this.tipoCambio
    }
  }

  borrarFormaPago() {
    this.cambioFp = true;
    const i = this.eventGrid.index;
    this.dataGrid.splice(i, 1);
    this.calcularTotalPago('borrar forma pago');
  }

  cambioPagoServicio() {
    this.cambioFp = true;
    if (this.nuevoPago.servicio < 0) { this.nuevoPago.servicio = 0; }
    if (!this.nuevoPago.servicio) { this.nuevoPago.servicio = 0; }
  }

  cambioPagoSubtotal() {
    this.cambioFp = true;
    if (this.nuevoPago.subtotal < 0) { this.nuevoPago.subtotal = 0; }
    if (!this.nuevoPago.subtotal) { this.nuevoPago.subtotal = 0; }
  }

  saveRecord(val) {
    this.cambioFp = true;
    let codigo = val.pCodigo;
    if (codigo && codigo.Codigo !== undefined) {
      codigo = val.pCodigo.Codigo;
    }
    this.chequeFp = {
      Clase: this.cheque.clase,
      punto: this.cheque.punto,
      iCheque: this.cheque.iCheque,
      cCuenta: '',
      codigo: codigo,
      dMonto: val.subtotal,
      dProp: val.servicio,
      Moneda: val.moneda.Moneda,
      dTc: val.tipocambio,
      cUsuario: this.ttUsuario.cUserId,
      cVoucher: val.voucher,
      dPropTotal: val.servicio,
      dDescTotal: this.totalPago.descuento,
      cNotas: '',
      cRowID: '',
      lError: false,
      cErrDesc: ''
    }
    this.dataGrid.push(this.chequeFp);
    this.cerrarDialog();
    this.calcularTotalPago('save record');
  }

  seleccionaFp(value) {
    this.codigofpRepetido = [];
    if (value) {
      if (value.Codigo === 'HH' && !this.habitacion.cuarto) {
        swal('Cargo a Huesped!!', 'Ingresar el cuarto para aplicar el cargo', 'warning').then(() => {
          this.openModal(this.modalBuscar);
          this.codigoSelect = null;
        });
      } else {
        this.codigoSelect = value;
        this.dataGrid.forEach(item => {
          if (item.codigo === this.codigoSelect.Codigo) {
            this.codigofpRepetido.push(item);
          }
        });
        this.fpSelected = true;
        this.addFormPago();
      }
    }
  }

  savePago() {
    this.loadingClick = true;
    if (this.huesped && this.huesped.heactual === 'CO' && this.codigoSelect.Codigo === 'HH') {
      swal('Error!!', 'Huesped check-out', 'error');
    } else {
      this.cambioFp = true;
      let countRepeted = 0;
      this.chequeFp = {
        Clase: this.cheque.clase,
        punto: this.cheque.punto,
        iCheque: this.cheque.iCheque,
        cCuenta: '',
        codigo: this.codigoSelect.Codigo,
        dMonto: this.nuevoPago.subtotal,
        dProp: this.totalPago.descuento,
        Moneda: this.monedaCia,
        dTc: this.tipoCambio,
        cUsuario: this.ttUsuario.cUserId,
        cVoucher: this.nuevoPago.voucher,
        dPropTotal: this.nuevoPago.servicio,
        dDescTotal: this.totalPago.descuento,
        cNotas: this.cheque.pnotas,
        cRowID: '',
        lError: false,
        cErrDesc: ''
      }
      this.codigofpRepetido.forEach(item => {
        if (item.codigo === this.chequeFp.codigo) {
          if (item.cVoucher === this.chequeFp.cVoucher) {
            countRepeted++;
          }
        }
      });
      if (!countRepeted) {
        this.cheque.pporc = this.porcDescuento;
        this.cheque.dTotal = this.totalPago.subtotal;
        this.cheque.cNhuesp = this.codigoSelect.Codigo;
        this._cheque.updateCheque(this.cheque).subscribe(data => {
          this.loadingClick = false;
          if (data[0].cErrDesc) {
            swal('Error!!', data[0].cErrDesc, 'error');
          } else {
            const c = data[0];
            this._ayuda.getCheque(c.Mesero, c.clase, c.punto, c.iCheque).subscribe(cheque => {
              if (cheque) {
                this.cheque = cheque[0];
                this.cheque.pporc = this.formPago.descuento;
                this.update.emit(this.cheque);
                this.dataGrid.push(this.chequeFp);
                this.calcularTotalPago('save pago 1');
                this.nuevoPago.subtotal = (this.totalPago.subtotal - this.totalPago.descuento) - this.totalPagado.subtotal;
                this.nuevoPago.servicio = this.totalPago.servicio - this.totalPagado.servicio;
                this.pagos.emit(this.dataGrid);
                this.codigoSelect = null;
                this.calcularTotalPago('save pago 2');
                this._cheque.notifyOther({ option: 'calcularTotal', value: true });
              }
            });
          }
        });
      } else {
        this.loadingClick = false;
        swal('Voucher ya existe!!', '', 'warning');
      }
    }
  }

  calcularTotalPago(value) {
    this._ayuda.getCalculaCheque(this.cheque.clase, this.cheque.punto, this.cheque.iCheque).subscribe(data => {
      if (data) {
        this.totalPago.subtotal = data[0].dDec1.toFixed(2);
        this.totalPago.servicio = data[0].dDec2.toFixed(2);
        this.totalPago.descuento = data[0].dDec3.toFixed(2);
        this.totalPago.total = data[0].dDec4.toFixed(2);
        this.porcDescuento = data[0].dDec5;
        this.formPago.descuento = this.porcDescuento;
        this.formPago.servicio = this.totalPago.servicio;
        if (this.nuevoPago) {
          this.nuevoPago.servicio = this.totalPago.servicio - this.totalPagado.servicio;
          this.nuevoPago.subtotal = (this.totalPago.subtotal - this.totalPago.descuento) - this.totalPagado.subtotal;
          this.nuevoPago.subtotal = Number(this.nuevoPago.subtotal.toFixed(2));
        }
        this.totalPagado.subtotal = 0;
        this.totalPagado.servicio = 0;
        this.totalPagado.total = 0;
        this.totalPagado.balance = Number(this.totalPago.total) - Number(this.totalPagado.total);
        this.dataGrid.forEach(item => {
          this.totalPagado.subtotal += item.dMonto;
          this.totalPagado.servicio += item.dPropTotal;
          this.totalPagado.total = Number(this.totalPagado.subtotal) + Number(this.totalPagado.servicio);
          this.totalPagado.total = Number(this.totalPagado.total.toFixed(2));
          this.totalPagado.balance = Number(this.totalPago.total) - Number(this.totalPagado.total);
        });
      }
    });
  }
  public setSelectableSettings(): void {
    this.selectableSettings = { checkboxOnly: this.checkboxOnly, mode: this.mode };
  }
  cerrarDialog() {
    this.verFormPago = false;
    this.updatePago = false;
    this.pagoSelected = null;
  }
  // selectRow(event) {
  //   this.eventGrid = event;
  //   this.rowActive = event.selected;
  //   if (event.selected) {
  //     const i = event.index;
  //     this.pagoSelected = this.dataGrid[i];
  //     this.updatePago = true;
  //     this.verFormPago = true;
  //     this.nuevoPago = {
  //         pCodigo: this.pagoSelected.codigo,
  //         voucher: this.pagoSelected.cVoucher,
  //         subtotal: this.pagoSelected.dMonto,
  //         servicio: this.pagoSelected.dPropTotal,
  //         moneda: this.pagoSelected.Moneda,
  //         tipocambio: this.pagoSelected.dTc
  //     }
  //     this.tipoCambio();
  //   }
  // }
}
