import { Component, Input, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { InfociaGlobalService } from '../../../../../services/infocia.service';
import { BuscarService } from '../../../../../pms/recepcion/check-in/services/childs/buscar.service';
import { ChequeAyudaService } from '../../services/cheque-ayuda.service';
import moment from 'moment';
declare var $: any;

@Component({
  selector: 'app-imprimir-cheque',
  templateUrl: './imprimir-cheque.component.html',
  styleUrls: ['./imprimir-cheque.component.css']
})
export class ImprimirChequeComponent implements OnChanges {
  public totalCalculado: any;
  public huesped: any;
  public windowPrint: boolean;
  public ttUsuario: any;
  public ttInfocia: any;
  public fieldOrder: string = 'iCom'
  @Input() cheque: any;
  @Input() comanda: any;
  @Input() imprimir: any;
  @Input() punto: any;
  @Input() printfp: string;
  @Output() finished = new EventEmitter;
  flagFpago: boolean;
  arrFpago: any[] = [];
  listaPagos: any;
  flagShowFp: boolean;
  formatMoment: string;

  constructor(
    private _info: InfociaGlobalService,
    private _buscar: BuscarService,
    private _cheque: ChequeAyudaService
  ) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.formatMoment = localStorage.getItem('formatMoment');

  }
  async ngOnChanges(change: SimpleChanges) {
    if (change.imprimir !== undefined) {
      if (change.imprimir.currentValue) {
        setTimeout(() => {
          this.imprimirCheque();
        }, 500);
      }
    }
    if (change.cheque !== undefined) {
      if (change.cheque.currentValue) {
        if (this.cheque.pfolio) {
          this._buscar.getHuesped(this.cheque.pfolio, 'folio').subscribe(data => {
            if (data) {
              this.huesped = data[0];
            }
          });
          // this.cheque.pfecha = moment(this.cheque.pfecha).format(this.formatMoment);
          if (!this.cheque.cMesnom) {
            this.cheque.cMesnom = this.cheque.Mesero;
          }
        }
      }
    }
    if (change.printfp !== undefined) {
      if (change.printfp.currentValue) {
        this._cheque.getChequeFp(this.cheque.clase, this.cheque.punto, this.cheque.iCheque).subscribe(pagos => {
          if (pagos) {
            this._cheque.getCodigos().subscribe(codigos => {
              for (const cod of codigos) {
                for (const list of pagos) {
                  if (list.codigo === cod.Codigo) {
                    this.arrFpago.push({
                      codigo: cod.Codigo, voucher: list.cVoucher, desc: cod.cdesc,
                      monto: (list.dMonto + list.dPropTotal) - list.dDescTotal
                    });
                  }
                }
              }
            });
          }
        });
      }
    }
  }
  imprimirCheque() {
    this._cheque.getCalculaCheque(this.cheque.clase, this.cheque.punto, this.cheque.iCheque).subscribe(data => {
      if (data) {
        this.totalCalculado = data[0];
        setTimeout(() => {
          const mywindow = window.open('', 'spos_print', 'height=500,width=700');
          mywindow.document.write('<html><head><title>Configuración de Impresión</title>');
          mywindow.document.write('</head><body>');
          mywindow.document.write($('#print-cheque').html());
          mywindow.document.write('</body></html>');
          mywindow.document.close();
          mywindow.print();
          this.cheque = [];
          mywindow.onafterprint = () => {
            this.finished.emit(true);
            mywindow.close();
          }
        });
      }
    });
  }
}
