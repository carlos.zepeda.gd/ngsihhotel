// Angular Modules
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// Apps Modules
import {SharedAppsModule} from '../../../shared/shared-apps.module';
import {KendouiModule} from '../../../shared/kendo.module';
// Services
import {ChequeService} from './services/cheque.service';
import {ChequeAyudaService} from './services/cheque-ayuda.service';
import {BuscarService} from '../../../pms/recepcion/check-in/services/childs/buscar.service';
// Component
import {ModificadoresChequeComponent} from './components/modificadores-cheque/modificadores-cheque.component';
import {ChequesAbiertosComponent} from './components/cheques-abiertos/cheques-abiertos.component';
import {DetallePlatilloComponent} from './components/detalle-platillo/detalle-platillo.component';
import {ImprimirChequeComponent} from './components/imprimir-cheque/imprimir-cheque.component';
import {AccionesChequeComponent} from './components/acciones-cheque/acciones-cheque.component';
import {FacturaChequeComponent} from './components/factura-cheque/factura-cheque.component';
import {SearchCheckComponent} from '../../components/search-check/search-check.component';
import {PagosChequesComponent} from './components/pagos-cheques/pagos-cheques.component';
import {InfoHuespedComponent} from './components/info-huesped/info-huesped.component';
import {HelpMesasComponent} from '../../components/help-mesas/help-mesas.component';
import {TabPagosComponent} from './components/tab-pagos/tab-pagos.component';
import {ChequeComponent} from './cheque.component';
import {InfociaGlobalService} from '../../../services/infocia.service';
import {NavbarChequeComponent} from './components/navbar-cheque/navbar-cheque.component';
import {TotalPlatillosComponent} from './components/total-platillos/total-platillos.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {ArchitectModule} from '../../../_architect/architect.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        KendouiModule,
        SharedAppsModule,
        DragDropModule,
        ArchitectModule
    ],
    declarations: [
        ChequeComponent,
        HelpMesasComponent,
        ImprimirChequeComponent,
        AccionesChequeComponent,
        InfoHuespedComponent,
        ModificadoresChequeComponent,
        DetallePlatilloComponent,
        PagosChequesComponent,
        ChequesAbiertosComponent,
        TabPagosComponent,
        FacturaChequeComponent,
        NavbarChequeComponent,
        TotalPlatillosComponent,
        SearchCheckComponent
    ],
    exports: [
        ChequeComponent,
        SearchCheckComponent
    ],
    providers: [
        ChequeService,
        ChequeAyudaService,
        BuscarService
    ]
})
export class ChequeModule {
}
