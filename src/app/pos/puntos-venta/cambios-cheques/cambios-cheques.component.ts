import { Component, OnInit } from '@angular/core';
import { DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import {
    SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process, State
} from '@progress/kendo-data-query';
import { ChequeAyudaService } from '../cheque/services/cheque-ayuda.service';
import { ReportesPosService } from '../../reportes/reportes-pos.service';
import { InfociaGlobalService } from 'app/services/infocia.service';
import { ManttoMenuService } from '../../../pms/manttos/mantto-menu/mantto-menu.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import moment from 'moment';

@Component({
    selector: 'app-cambios-cheques',
    templateUrl: './cambios-cheques.component.html',
    styleUrls: ['./cambios-cheques.component.css']
})
export class CambiosChequesComponent implements OnInit {
    heading = 'Modificaciones Realizadas a Cheques';
    subheading = 'Point of Sales System.';
    icon = 'icon-gradient bg-tempting-azure';
    public formatMoment = localStorage.getItem('formatMoment');
    public formSearch: FormGroup;
    public dsPuntos: any = [];
    public dsClases: any = [];
    public dsResumen: any = [];
    // Variables para vincular con componentes de Kendo
    public dataGrid: any = [];
    public dataComplete: any = [];
    // Variables para funciones de Kendo Grid
    public filter: CompositeFilterDescriptor;
    public groups: GroupDescriptor[] = [];
    public sort: SortDescriptor[] = [{ field: 'pcheque', dir: 'asc' }]; // Order asc o desc
    public sort2: SortDescriptor[] = [{ field: 'pcom', dir: 'asc' }]; // Order asc o desc
    // Agrupar por un campo en especifico
    public state: State = { group: [{ field: 'tpunto' }] };
    public ttUsuario: any = [];

    constructor(private _reporte: ReportesPosService,
        private _cheque: ChequeAyudaService,
        public _info: InfociaGlobalService,
        private fb: FormBuilder,
        private _menuServ: ManttoMenuService,
        private _modalService: NgbModal,
        private _router: Router) {
        this._cheque.getClases().subscribe(data => this.dsClases = data);
        this._cheque.getPuntos().subscribe(data => this.dsPuntos = data);
    }

    ngOnInit() {
        this.buildFormGroup(); // Construcción de Reactive Form
        this.ttUsuario = this._info.getSessionUser();

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    chequeSelected(e) {
        e.pfecha = moment(e.pfecha).format(this.formatMoment);
        this.formSearch.patchValue(e);
        this._modalService.dismissAll();
        this.getReporte();
    }

    openModal(template) {
        this._modalService.open(template, { size: 'lg' });
    }
    openModalCheques(template) {
        this._modalService.open(template, { size: 'lg' });
    }
    getReporte() {
        const clase = this.formSearch.value.clase;
        const punto = this.formSearch.value.punto;
        const cheque = this.formSearch.value.iCheque;
        this._reporte.getCambiosCheques(clase, punto, cheque).subscribe(data => {
            if (data.ttPcdet) {
                this.dataGrid = data.ttPcdet;
                this.dataComplete = data.ttPcdet;
            }
            if (data.ttPcdet) {
                this.dsResumen = data.ttPcdet;
                this.dsResumen = {
                    data: orderBy(this.dsResumen, this.sort2),
                    total: this.dsResumen.length
                };
            }
            if (data.ttPcheques && data.ttPcdet) {
                data.ttPcheques[0].pfecha = moment(data.ttPcheques[0].pfecha).format(this.formatMoment);
                this.formSearch.patchValue(data.ttPcheques[0]);
                this.dataGrid = {
                    data: orderBy(this.dataGrid, this.sort),
                    total: this.dataGrid.length
                };
            } else {
                swal({ position: 'center', type: 'warning', title: 'Cheque sin movimientos!!!', showConfirmButton: false, timer: 1500 });
            }
        })
    }
    changeCheque() {
        const fg = this.formSearch.value;
        const punto = fg.punto;
        if (fg.iCheque) {
            this._cheque.getCheque(this.ttUsuario.Mesero, fg.clase, punto, fg.iCheque).subscribe(data => {
                if (data) {
                    if (data.cMensTit) {
                        swal(data.cMensTit, data.cMensTxt, 'error');
                        this.formSearch.reset({ punto: punto });
                        this.dataGrid = [];
                        this.dataComplete = [];
                    } else {
                        this.chequeSelected(data[0]);
                    }
                }
            });
        }
    }
    buildFormGroup() {
        this.formSearch = this.fb.group({
            clase: [''],
            cCladesc: [''],
            cAdjunta: [''],
            dTotal: [0],
            pporc: [0],
            punto: [1, Validators.required],
            cPundesc: [''],
            iCheque: ['', Validators.required],
            iTransf: [null],
            Mesero: [''],
            cMesnom: [''],
            cMesa: [''],
            pper: [null],
            pprop: [null],
            pdesc: [null],
            pnotas: [''],
            pfolio: [null],
            cNhuesp: [''],
            rfc: [''],
            pturno: [null],
            cTurdesc: [''],
            pncheq: [null],
            pea: [''],
            pfecha: [''],
            Usuario: [''],
            oldmesa: [null],
            cRowID: [''],
            lError: [false],
            cErrDesc: ['']
        });
    }

    // Procesa la grid a mostrar cuando se agrupa por un campo. Grid debe tener Groupable = true
    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.dataGrid = process(this.dataComplete, { group: this.groups });
    }

    // Procesa la grid cuando se filtra en un campo. Grid debe tener Filterable = true
    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.dataGrid.data = filterBy(this.dataComplete, filter);
    }

    // Procesa la grid en asc o desc en el campo seleccionado
    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.dataGrid = {
            data: orderBy(this.dataGrid.data, this.sort),
            total: this.dataGrid.data.length
        };
    }

    // Procesa la grid en asc o desc en el campo seleccionado
    public sortChange2(sort: SortDescriptor[]): void {
        this.sort2 = sort;
        this.dsResumen = {
            data: orderBy(this.dsResumen.data, this.sort2),
            total: this.dsResumen.data.length
        };
    }

    public dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        this.dataGrid = process(this.dataComplete, this.state);
    }

}
