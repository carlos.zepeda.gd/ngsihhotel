import { Component, HostBinding, OnInit, OnDestroy } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable, Subscription } from 'rxjs';
import { ThemeOptions } from '../../../theme-options';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import moment from 'moment';
import { Router } from '@angular/router';
import 'rxjs/Rx';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  providers: [InfociaGlobalService]
})
export class HeaderComponent implements OnInit, OnDestroy {
  ttInfocia: any;
  ttUsuarios: any;
  encabezado: any = [];
  userSys: any;
  isActive: boolean;
  fechaH = '';
  mensajes = [];
  @select('config') public config$: Observable<any>;
  public subscription: Subscription;
  loading: boolean;
  flagMsjs: boolean;
  isConnected: Observable<boolean>;
  flagConnect: boolean;

  constructor(public globals: ThemeOptions,
    private _router: Router,
    private _info: InfociaGlobalService,
  ) {}

  ngOnInit() {
    this.subscription = this._info.notifyObservable$.subscribe((res) => {
      if (res.option === 'change-header') {
        this.init();
      }
    });
    this.init();
  }

  @HostBinding('class.isActive')
  get isActiveAsGetter() {
    return this.isActive;
  }

  init() {
    this._info.getUserCia().subscribe(data => {
      this.ttInfocia = data.ttInfocia[0];
      this.ttUsuarios = data.ttUsuarios[0];
      this.mensajes = this.ttInfocia.cmensaje.split('®');
      this.encabezado = this.ttInfocia.encab.split('®');
      this.fechaH = moment(this.ttInfocia.fday).locale('es').format(localStorage.getItem('formatMoment') + ' dddd');
      this.userSys = this.ttUsuarios.cVal32.split('®');
      if (this.ttInfocia.cmensaje || this.ttInfocia.cNotifica) {
        this.flagMsjs = true;
      } else {
        this.flagMsjs = false;
      }
    });
  }


  toggleSidebarMobile() {
    this.globals.toggleSidebarMobile = !this.globals.toggleSidebarMobile;
  }

  toggleHeaderMobile() {
    this.globals.toggleHeaderMobile = !this.globals.toggleHeaderMobile;
  }

  endSession() {
    localStorage.setItem('logout', 'si');
    this.loading = true;
    setTimeout(() => {
      this._info.deleteTempRsrv().subscribe(data => {
        this.loading = false;
        this._info.logoutProgress().subscribe(response => {
          this._router.navigate(['/login']);
        }, error => {
          this._router.navigate(['/login']);
        });
      }, error => {
        this._router.navigate(['/login']);
      }
      );
    }, 1000);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
  cambiarClave() {
  }
}
