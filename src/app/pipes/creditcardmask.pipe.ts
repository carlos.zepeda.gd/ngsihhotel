import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'creditCardMask'
})
export class CreditCardMaskPipe implements PipeTransform {
  transform(plainCreditCard: string, visibleDigits: number = 4): string {
    // const visibleDigits = 4;
    const maskedSection = plainCreditCard.slice(0, -visibleDigits);
    const visibleSection = plainCreditCard.slice(-visibleDigits);
    return maskedSection.replace(/./g, '*').replace(/(\d{4})/g, '$1 ').trim() + visibleSection;
    // return maskedSection.replace(/./g, '*') + visibleSection;
  }
      // return plainCreditCard.replace(/\s+/g, '').replace(/(\d{4})/g, '$1 ').trim();

}
