import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  transform(value: string): string {
    let estatus = '';
    switch (value) {
      case 'A':
        estatus = 'Abierto';
        break;
      case 'I':
        estatus = 'Impreso';
        break;
      case 'C':
        estatus = 'Cerrado';
        break;
      case 'X':
        estatus = 'Cancelado';
        break;
    }
    return estatus;
  }

}
