// Modulos Angular
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// Modulos Apps
import { SystemModule } from './shared/system.module';
// Componentes
import { AppComponent } from './app.component';
// Rutas
import { APP_ROUTING } from './app.routes';
// Servicios
import { CheckinService } from './pms/recepcion/check-in/services/checkin.service';
import { ChequeService } from './pos/puntos-venta/cheque/services/cheque.service';
import { ManttoMenuService } from './pms/manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from './services/infocia.service';
import { MenuPmsService } from './services/menu.service';
import { LoginComponent } from './login/login.component';
import { VerifySessionService } from './services/verify-session';
<<<<<<< HEAD
=======
import { ExportAsModule } from 'ngx-export-as';
import { LoginLayoutComponent } from './_architect/Layout/login-layout/login-layout.component';
import { LoginComponent } from './components/login/login.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptorService } from './services/auth-interceptor.service';
import { IntlModule } from '@progress/kendo-angular-intl';
import { MessageService } from '@progress/kendo-angular-l10n';
import { MyMessageService } from './services/my-message.service';
// import '@progress/kendo-angular-intl/locales/de/all';
// import '@progress/kendo-angular-intl/locales/es/all';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/es-MX';
import '@progress/kendo-angular-intl/locales/es-MX/all';
import { InfoSihComponent } from './components/info-sih/info-sih.component';
>>>>>>> dev-alberto

import { ExportAsModule } from 'ngx-export-as';

@NgModule({
<<<<<<< HEAD
  declarations: [
    AppComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
    SystemModule,
    APP_ROUTING,
    ExportAsModule
  ],
  providers: [
    InfociaGlobalService,
    CheckinService,
    MenuPmsService,
    ChequeService,
    ManttoMenuService,
    VerifySessionService
  ],
  bootstrap: [AppComponent]
=======
    declarations: [
        AppComponent,
        LoginComponent,
        LoginLayoutComponent,
        InfoSihComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        SystemModule,
        APP_ROUTING,
        ExportAsModule,
        HttpClientModule,
        IntlModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptorService,
            multi: true
        },
        {
            provide: MessageService, useClass: MyMessageService
        },
        {
            provide: LOCALE_ID,
            useValue: 'es-MX'
        },
        InfociaGlobalService,
        CheckinService,
        MenuPmsService,
        ChequeService,
        ManttoMenuService,
        VerifySessionService
    ],
    bootstrap: [AppComponent]
>>>>>>> dev-alberto
})
export class AppModule { }
