import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GLOBAL } from './../services/global';
import 'rxjs/add/operator/map';
const headers = new Headers({ 'Content-Type': 'application/json' });

@Injectable()
export class CrudService {

  public url: string;
  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  getData(urlSec: string) {
    return this.http.get(this.url + urlSec)
      .map(res => res.json().response);
  }

  putInfo(data, urlSec: string, ds: string, tt: string) {
    const json = JSON.stringify(data);
    const params = '{"' + ds + '":{"' + tt + '":[' + json + ']}}';

    return this.http.put(this.url + urlSec, params, { headers: headers })
      .map(res => res.json().response);
  }
  putInfo2(data, urlSec: string) {
    return this.http.put(this.url + urlSec, data, { headers: headers }).map(res => res.json().response);
  }
  putInfoArray(data, urlSec: string, ds: string, tt: string) {
    const json = JSON.stringify(data);
    const params = '{"' + ds + '":{"' + tt + '":' + json + '}}';

    return this.http.put(this.url + urlSec, params, { headers: headers })
      .map(res => res.json().response);
  }
  postInfo(data, urlSec: string, ds: string, tt: string) {
    const json = JSON.stringify(data);
    const params = '{"' + ds + '":{"' + tt + '":[' + json + ']}}';

    return this.http.post(this.url + urlSec, params, { headers: headers })
      .map(res => res.json().response);
  }

  deleteData(data, urlSec: string, tt: string) {
    const json = JSON.stringify(data);
    const params = '{"' + tt + '":[' + json + ']}';

    return this.http.delete(this.url + urlSec,
      new RequestOptions({ headers: headers, body: params }))
      .map(res => res.json().response);
  }

  // var action : acciones  Buscar "B", Asignar "A", Borrar "I"
  // var jsonParams : tablas Json Camarista: ttTcam, Ingreso Agrupado: ttTiagrp, Tipo Cuarto: ttTcto
  // Solo enviar JSON con los datos requeridos
  putInfoReporte(urlSec, jsonParams) {
    return this.http.put(this.url + urlSec, jsonParams, { headers: headers })
      .map(res => res.json().response);
  }
}
