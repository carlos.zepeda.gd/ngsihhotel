import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { BuscarService } from '../check-in/services/childs/buscar.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { CrudService } from '../../mantto-srv.services';
import { RecepcionService } from '../recepcion.service';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';
import swal from 'sweetalert2';

@Component({
    selector: 'app-fallar-huesped',
    templateUrl: './fallar-huesped.component.html'
})
export class FallarHuespedComponent implements OnInit {
    heading = 'Fallar Huesped ';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';
    public dataFolios: any = [];
    public folio: any = [];
    public ttInfocia: any = [];
    public dataGrid: any = [];
    public dayOfWeek1: string = '';
    public dayOfWeek2: string = '';
    public dayOfWeek3: string = '';
    public noches: number = 0;
    ngForm: FormGroup;
    public ttHuesfal = [{ hfolio: 0, hhotel: '', htarifa: 0, hfecreg: null, cRowID: '', lError: false, cErrDes: '' }];
    formatKendo: string = localStorage.getItem('formatKendo');
    formatMoment: string = localStorage.getItem('formatMoment');
    toast = GLOBAL.toast;

    constructor(private _buscar: BuscarService,
        private _info: InfociaGlobalService,
        private _rec: RecepcionService,
        private _crud: CrudService,
        private _menuServ: ManttoMenuService,
        private _modal: NgbModal,
        private _router: Router) {
        this.ttInfocia = this._info.getSessionInfocia();
        this.ttInfocia.fday = moment(this.ttInfocia.fday).toDate();
        this.consultar();
    }

    consultar() {
        this._rec.getEstatusHuesped('FA').subscribe(data => this.dataGrid = data);
    }

    ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
    }

    valueChange(value) {
        if (value.heactual === 'CO') {
            return this.toast('Huesped Check-Out!!!', '', 'error');
        }
        if (value.length < 4) {
            return this.toast('Cuarto Invalido!!!', '', 'error');
        }
        this._buscar.getHuesped(value, 'cuarto').subscribe(cuarto => {
            if (!cuarto) {
                this._buscar.getHuesped(value, 'folio').subscribe(folio => {
                    if (!folio) {
                        this.toast('Cuarto Invalido!!!', '', 'error');
                    } else {
                        this.resultData(folio);
                    }
                });
            } else {
                this.resultData(cuarto);
            }
        });
    }

    resultData(data) {
        if (data.length === 1) {
            this.resultHuesped(data[0]);
        } else {
            this.dataFolios = data;
            // Hay mas de un huesped con este cuarto, mostrar grid.
        }
    }

    public resultHuesped(folio) {
        if (folio.heactual === 'CO') {
            this.toast('Huesped Check-Out!!!', '', 'error');
        } else {
            this.dayOfWeek1 = moment(folio.hfent).locale('es').format('dddd');
            this.dayOfWeek2 = moment(folio.hfsal).locale('es').format('dddd');
            this.dayOfWeek3 = moment(folio.hfsal).locale('es').format('dddd');
            folio.hfent = moment(folio.hfent).toDate();
            folio.hfsal = moment(folio.hfsal).toDate();
            if (folio.hfecreg) {
                folio.hfecreg = moment(folio.hfecreg).toDate();
            }
            const a = moment(folio.hfent);
            const b = moment(folio.hfsal);
            this.noches = b.diff(a, 'days');
            this.folio = folio;
        }
    }

    public guardarForm() {
        const form = JSON.parse(JSON.stringify(this.ttHuesfal));
        if (!form[0].hfecreg) {
            return this.toast('Ingresa fecha de regreso', '', 'info');
        } else {
            this.folio.heactual = 'FA';
            form[0].hfecreg = moment(form[0].hfecreg).format(GLOBAL.formatIso);
            form[0].hfolio = this.folio.hfolio;
            this.folio.hfecreg = form[0].hfecreg;
            const json1 = JSON.stringify(this.folio);
            const json2 = JSON.stringify(form);
            const params = '{"dsHuesp":{"ttHuespedes":[' + json1 + '], "ttHuesfal": ' + json2 + '}}';
            this._crud.putInfo2(params, '/rec/huesped/fallar').subscribe(res => {
                const result1 = res.siHuesp.dsHuesp.ttHuesfal;
                const result2 = res.siHuesp.dsHuesp.ttHuespedes;
                if (result1 && result1[0].cErrDes) {
                    return this.toast('Error', result1[0].cErrDes, 'error');
                }
                if (result2 && result2[0].cErrDes) {
                    return this.toast('Error', result2[0].cErrDes, 'error');
                }
                this.toast('Registro guardado!.', '', 'success');
                this.reinciar();
            });
        }
    }

    reinciar() {
        this.consultar();
        this.folio = [];
        this.noches = 0;
        this.dayOfWeek1 = '';
        this.dayOfWeek2 = '';
        this.dayOfWeek3 = '';
        this.ttHuesfal = [{ hfolio: 0, hhotel: '', htarifa: 0, hfecreg: null, cRowID: '', lError: false, cErrDes: '' }];
    }

    openModal(modal) {
        this._modal.open(modal, { size: 'xl', centered: true });
    }

}
