import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { CompositeFilterDescriptor, distinct, filterBy, process } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { CrudService } from '../../mantto-srv.services';
import { RecepcionService } from '../recepcion.service';
import { TblGeneral } from '../../../models/tgeneral';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-auditoria-brazaletes',
  templateUrl: './auditoria-brazaletes.component.html',
})
export class AuditoriaBrazaletesComponent implements OnInit {
  heading = 'Auditoría de Brazaletes';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  ngForm: FormGroup;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];
  dataGlobal: any = [];
  dataTotal: any = [];
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  ttUsuario: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  filter: CompositeFilterDescriptor;
  fecha: any;
  api: Array<string> = ['/reportes/caja/AudBrazaletes', 'dsReprecepcion', 'ttTgeneral'];
  tipoBraz: any = [];
  loading = false;
  toast = GLOBAL.toast;
  swal = GLOBAL.swal;
  tblGeneral: TblGeneral;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _crudservice: CrudService,
    private _formBuild: FormBuilder,
    private _https: RecepcionService,
    private _http: HttpClient,
  ) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.crudTable();
    this.ngForm = this.createForm();
    this.allData = this.allData.bind(this);
    this._crudservice.getData('/manttos/tbandas').subscribe(res => {
      if (res.siTbanda.dsTbandas.ttTbandas) {
        this.tipoBraz = res.siTbanda.dsTbandas.ttTbandas;
      } else {
        this.tipoBraz = [];
      }
      this.loading = false;
    });
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  crudTable() {
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.iInt1 = 2;
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).toDate();
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }

  public buscarInfo() {
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
    this._https.putInfo(temp, this.api[0], this.api[1], this.api[2], GLOBAL.httpOptions).subscribe(data => {
      const result = data.siReprecepcion.dsReprecepcion;
      const resultExcel = data.siReprecepcion.dsReprecepcion.bttResp;
      if (!result.Brazaletes) {
        this.dataGrid = [];
        this.dataGlobal = [];
        this.dataTotal = [];
      } else {
        this.dataGrid = result.Brazaletes;
        this.dataGlobal = result.Brazalete;
        this.dataTotal = result.GlobalBrazalete[0];
      }
      this.loading = false;
    });
  }

  filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataFilter = filterBy(this.dataGrid, filter);
  }

  distinctPrimitive(fieldName: string) {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  public allData(): ExcelExportData {
    if (this.filter) {
      const result: ExcelExportData = {
        data: process(this.dataFilter, { sort: [{ field: 'Ope', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    } else {
      const result: ExcelExportData = {
        data: process(this.dataGrid, { sort: [{ field: 'Ope', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    }

  }

  exportPdf() {
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(this.formatMoment);
    form.fFec2 = moment(form.fFec2).format(this.formatMoment);
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: '002-AUDITORIA-BRAZ',
      dataGrid: this.dataGrid,
      global: this.dataGlobal,
      total: this.dataTotal,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      hora: moment().format('HH:mm:ss'),
      form: form,
    }
    this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = this.heading + '.pdf';
        download.click();
      },
      error => {
        this.swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
      });
  }
}
