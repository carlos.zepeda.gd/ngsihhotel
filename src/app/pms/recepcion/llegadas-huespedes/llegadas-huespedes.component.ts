import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CompositeFilterDescriptor, distinct, filterBy, process } from '@progress/kendo-data-query';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { BuscarService } from '../check-in/services/service-checkin.index';
import { ReservacionService } from '../../reservaciones/reservacion/services/reservacion.service';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { RecepcionService } from '../recepcion.service';
import { TblGeneral } from '../../../models/tgeneral';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-llegadas-huespedes',
  templateUrl: './llegadas-huespedes.component.html',
  providers: [DecimalPipe]
})
export class LlegadasHuespedesComponent implements OnInit {
  heading = 'Huéspedes / Llegadas';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  @ViewChild('modalFilter') modalFilter: TemplateRef<any>;
  formGeneral: TblGeneral;
  dataGrid: any = [];
  dataGlobal: any = [];
  dataTotales: any = [];
  dataTotalesFiltro: any = [];
  dataRealesFiltro: any = [];
  ttInfocia: any = [];
  fecha: any;
  dataBloqueo: any;
  filter: CompositeFilterDescriptor;
  pageable = '{buttonCount: 5, pageSizes: [10, 25, 50]}';
  pageSize = 15;
  skip = 0;
  totalFilter: number = 1;
  dsTotales: any = [
    { total: 1, tdesc: 'Tarifa Tipo' },
    { total: 2, tdesc: 'Tipo Cuarto' },
    { total: 3, tdesc: 'Segmento' }
  ];
  loadHelp: string;
  titleHelp: string;
  selectedRow: any;
  noFolio: any;
  loading: boolean = true;
  espFallar = ['N', 'N'];
  optionFilter: string;
  secuencia: { 'secuencia': any; 'reservacion': any; };
  consulta: boolean;
  uso: string;
  ngFormSelection = [];
  dataComplete: any = [];
  dataFilter: any[];
  ttUsuario: any;
  totalLlegadas: any = [];
  flagEnviando: boolean;
  flagEditar: boolean;
  swal = GLOBAL.swal;
  loadingColor: any = GLOBAL.loadingColor;

  constructor(private _rec: RecepcionService,
    private _info: InfociaGlobalService,
    private _reserv: ReservacionService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _buscarServ: BuscarService,
    private _modalService: NgbModal,
    private _active: ActivatedRoute,
    private _httpClient: HttpClient,
    private _decimal: DecimalPipe) {
    this._active.data.subscribe(data => {
      this.consulta = data.consulta;
      this.uso = data.uso;
      if (this.uso === 'R') {
        this.heading = 'Reservaciones / Llegadas';
      }
    });
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.fecha = moment(this.ttInfocia.fday).toDate();
    this.formGeneral = new TblGeneral();
    this.allData = this.allData.bind(this);
    this.dataFilter = filterBy([this.dataComplete], this.filter);
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
    this.buscarInfo('todas');
  }

  enviarConf() {
    const sec = [...[this.secuencia.secuencia]][0];
    const rsrv = [...[this.secuencia.reservacion]][0];
    const f1 = moment(sec.rfent);
    const f2 = moment(sec.rfsal);
    sec.rfent = f1.format(this.formatMoment);
    sec.rfsal = f2.format(this.formatMoment);
    sec['noches'] = moment(f2).diff(f1, 'days');
    sec.ttdia = this._decimal.transform(sec.ttdia, '1.2-2');
    sec.ttotal = this._decimal.transform(sec.ttotal, '1.2-2');
    sec.ddepositos = this._decimal.transform(sec.ddepositos, '1.2-2');
    let email = rsrv.emailconf;
    let enviar = true;
    if (!email && rsrv.remail) {
      email = rsrv.remail;
    }
    if (!rsrv.remail && !rsrv.emailconf) {
      email = '';
      enviar = false;
    }
    let config = '001CNF';
    if (this.noFolio.ridioma && this.noFolio.ridioma !== 'E') {
      config = '001CNF-I';
    }
    const json = {
      propiedad: this.ttInfocia.propiedad.toLowerCase(),
      config: config,
      nombrepdf: config + '_' + rsrv.rnreser,
      reservacion: rsrv,
      secuencia: sec,
      infocia: this.ttInfocia,
      email: {
        enviar: true,
        de: '',
        deNombre: this.ttInfocia.cnombre,
        para: [rsrv.remail],
        cc: [],
        asunto: '',
        msj: ''
      }
    }
    this._httpClient.post<any>(GLOBAL.urlPhp + '/api/pdf', json).subscribe(response => {
      if (response.errors && response.errors.message) {
        this.swal({ title: 'Error al enviar confirmación', text: response.errors.message, type: 'error' });
      } else {
        const linkSource = 'data:application/pdf;base64,' + response.pdf;
        const download = document.createElement('a');
        const fileName = 'CONF_' + rsrv.rnreser + '.pdf';
        download.href = linkSource;
        download.download = fileName;
        download.click();
      }
      this.flagEnviando = false;
    },
      error => {
        this.swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'warning');
        this.flagEnviando = false;
      }
    );
  }

  checkin(event) {
    if (this.noFolio.cErrDes && this.noFolio.rtmov !== 'G') {
      this.swal('Atención!', this.noFolio.cErrDes, 'warning');
    } else {
      this.titleHelp = 'Huespedes';
      this.loadHelp = event;
      this.openModal(this.modalFilter, 'xl');
    }
  }

  maestra(event) {
    if (this.noFolio.rtmov === 'G') {
      this.checkin(event);
    } else {
      this.swal('No es una reservación de grupo', '', 'info');
    }
  }

  habilitarBotones(boton) {
    switch (boton) {
      case 'checkin':
        if (this.uso !== 'H' || !this.selectedRow || this.selectedRow.tFent !== this.ttInfocia.fday || this.selectedRow.tEActual !== 'VI' || this.noFolio && this.noFolio.rtmov === 'G') {
          return true;
        } else {
          return false;
        }
      case 'maestra':
        if (this.uso !== 'H' || !this.selectedRow || this.selectedRow.tFent !== this.ttInfocia.fday || this.selectedRow.tEActual !== 'VI' || this.noFolio && this.noFolio.rtmov !== 'G') {
          return true;
        } else {
          return false;
        }
        break;
      case 'bloquear':

        break;
    }
  }

  abrirReserv(modal) {
    if (this.noFolio) {
      this.dataBloqueo = '';
      this.loadHelp = 'R';
      this.titleHelp = 'Reservación'
      this.openModal(modal, 'xl');
      this.flagEnviando = false;
    }
  }

  bloqueo() {
    if (this.selectedRow.tNctos === 0 && this.noFolio.rcomp) {
      this.swal('Reservación Compartida!!!', '', 'info');
    } else {
      this.flagEnviando = true;
      this.abrirReserv(this.modalFilter);
      this.dataBloqueo = this.secuencia;
    }
  }

  finallyEvent(event) {
    if (event === 'error') {
      this._modalService.dismissAll();
    }
    if (event === 'close') {
      this.deselectKey();
      return;
    }
    this.buscarFecha(this.formGeneral.fFec1);
  }

  resultHelp(res) {
    this.formGeneral.cChr5 = res;
    this.loadHelp = '';
  }

  buscarLlegadas() {
    const temp = JSON.parse(JSON.stringify(this.formGeneral));
    temp['fFec1'] = moment(temp['fFec1']).format(GLOBAL.formatIso);
    const json = '{"dsReporte":{"ttTgeneral":[' + JSON.stringify(temp) + ']}}';

    this._httpClient.put<any>(GLOBAL.url + '/reportes/general/llegadas', json, GLOBAL.httpOptions).subscribe(res => {

      const data = res.response.siReporte;
      const error = data.dsMensajes.ttMensError;

      if (error && error[0].cMensTit) {
        this.loading = false;
        return this.swal(error[0].cMensTit, error[0].cMensTxt, 'error');
      }
      this.loading = false;
      if (data.dsReporte.tTres) {
        this.dataGrid = data.dsReporte.tTres;
        this.dataGlobal = data.dsReporte.ttTotalGlobal[0];
        this.dataComplete = data.dsReporte.tTres;
        if (data.dsReporte.tTipos) {
          this.dataTotales = data.dsReporte.tTipos;
          this.changeTotales(1);
        } else {
          this.dataTotalesFiltro = [];
          this.dataRealesFiltro = [];
        }
      } else {
        this.dataGrid = [];
        this.dataGlobal = [];
        this.dataTotales = [];
      }
      if (temp.cChr2) {
        this.loadHelp = '';
      }
    }, error => {
      this.loading = false;
    });
  }

  buscarFecha(val, event?) {
    const fechaTmp = moment(this.formGeneral.fFec1).format(this.formatMoment);
    switch (val) {
      case 'fecha--':
        if (moment(this.ttInfocia.fday).format(this.formatMoment) === fechaTmp) {
          this.loading = false;
          return;
        }
        this.fecha = moment(this.formGeneral.fFec1).add(-1, 'days').toDate();
        this.formGeneral.fFec1 = moment(this.fecha).toDate();
        break;
      case 'fecha++':
        this.fecha = moment(this.formGeneral.fFec1).add(1, 'days').toDate();
        this.formGeneral.fFec1 = moment(this.fecha).toDate();
        break;
      case 'fecha':
        this.formGeneral.fFec1 = event;
        break;
    }
    this.buscarLlegadas();
  }

  buscarInfo(val1, val2?) {
    this.deselectKey();
    this.formGeneral.cChr1 = '';
    this.formGeneral.cChr2 = '';
    this.formGeneral.cChr3 = '';
    this.formGeneral.cChr4 = '';
    switch (val1) {
      case 'W':
        this.formGeneral.cChr1 = 'G,N,R,C,U,T';
        this.formGeneral.cChr2 = 'W';
        this.optionFilter = 'W';
        break;
      case 'C':
        this.formGeneral.cChr1 = 'C';
        this.optionFilter = 'C';
        break;
      case 'CXLD':
        this.formGeneral.cChr1 = 'G,N,R,C,U,L,T';
        this.formGeneral.cChr4 = 'CX';
        this.optionFilter = 'CX';
        break;
      case 'DIA':
        this.formGeneral.cChr1 = 'G,N,R,C,U,L,T';
        this.formGeneral.cChr4 = '';
        this.optionFilter = 'D';
        break;
      case 'LE':
        this.formGeneral.cChr1 = 'L';
        this.optionFilter = 'L';
        break;
      case 'TC':
        this.formGeneral.cChr1 = 'T';
        this.optionFilter = 'T';
        break;
      case 'UC':
        this.formGeneral.cChr1 = 'U';
        this.optionFilter = 'U';
        break;
      case 'NS':
        this.formGeneral.cChr1 = 'G,N,R,C,U,L,T';
        this.formGeneral.cChr3 = 'NS';
        this.optionFilter = 'NS';
        break;
      case 'SN':
        this.formGeneral.cChr1 = 'G,N,R,C,U,L,T';
        this.formGeneral.cChr3 = 'SN';
        this.optionFilter = 'SN';
        break;
      case 'todas':
        this.formGeneral.lLog1 = false;
        this.formGeneral.lLog2 = false;
        this.formGeneral.cChr1 = 'G,N,R,C,U,T';
        this.formGeneral.cChr2 = '';
        this.formGeneral.cChr3 = '';
        this.formGeneral.cChr4 = 'VI';
        this.formGeneral.cChr5 = '';
        this.formGeneral.fFec1 = moment(this.fecha).toDate();
        this.optionFilter = 'G,N,R,C,U,T';
        break;
      default:
        this.formGeneral.fFec1 = moment(this.fecha).toDate();
        break;
    }
    this.buscarLlegadas();
  }

  changeTotales(e) {
    this.dataTotalesFiltro = [];
    this.dataRealesFiltro = [];
    this.dataTotales.forEach(item => {
      if (item.ttSec === e) {
        this.dataTotalesFiltro.push(item);
      }
      if (item.ttSec === (e + 3)) {
        this.dataRealesFiltro.push(item);
      }
    });
    this.dataTotalesFiltro.forEach(item => {
      if (item.ttTipo === 'Total:') {
        this.totalLlegadas = item;
      }
    });
  }

  cuartoEspPF(button: string) {
    const folio = this.selectedRow.tfolio;
    const sec = this.selectedRow.tSec;
    if (button === 'Esp') {
      this.espFallar[0] === 'N' || !this.espFallar[0] ? this.espFallar[0] = 'S' : this.espFallar[0] = 'N';
    } else {
      this.espFallar[1] === 'N' || !this.espFallar[1] ? this.espFallar[1] = 'S' : this.espFallar[1] = 'N';
    }
    this._rec.statusEspFallar(folio, sec, this.espFallar).subscribe(res => {
      if (res.response.siMensaje.dsMensajes.length !== 0) {
        this.buscarFecha('fecha', this.formGeneral.fFec1);
      }
    });
  }

  deselectKey() {
    this.ngFormSelection = [];
    this.selectedRow = '';
    this.noFolio = null;
  }

  distinctPrimitive(fieldName: string) {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  selectRow({ selectedRows }) {
    if (selectedRows) {
      this.selectedRow = selectedRows[0].dataItem;
      if (this.selectedRow.toDatos) {
        this.espFallar = this.selectedRow.toDatos.split('');
      }
      this.getFolio(this.selectedRow, 'R');
    }
  }

  getFolio(folio, tipo) {
    if (folio) {
      if (tipo === 'R') {
        this._buscarServ.getReservacion(folio.tfolio, 'folio').subscribe(res => {
          if (res) {
            this.noFolio = res[0];
          }
          this._reserv.notificarInfo(folio.tfolio).subscribe(data => {
            if (data.cMensTit) {
              return;
            }
            this.noFolio.lnotas = data[0].lnotas;
            this._reserv.getSecuencias(folio.tfolio).subscribe(res1 => {
              if (res1) {
                res1.forEach(item => {
                  if (item.rsecuencia === folio.tSec) {
                    this.secuencia = { 'secuencia': item, 'reservacion': this.noFolio }
                  }
                });
              }
            });
          });
        });
      }
      if (tipo === 'H') {
        this._buscarServ.getHuesped(folio, 'folio').subscribe(res => {
          if (res) {
            this.noFolio = res[0];
          }
        });
      }
    }
  }

  openNotas() {
    this.loadHelp = 'N';
    this.titleHelp = 'Notas'
    this.openModal(this.modalFilter, 'lg');
  }

  openModal(modal, size) {
    this._modalService.open(modal, { size: size, centered: true });
  }

  filterChange(filter: CompositeFilterDescriptor): void {
    this.deselectKey();
    this.filter = filter;
    this.dataFilter = filterBy(this.dataComplete, filter);
  }

  exportPdf() {
    const form = JSON.parse(JSON.stringify(this.formGeneral));
    form.fFec1 = moment(form.fFec1).format(this.formatMoment);
    form.fFec2 = moment(form.fFec2).format(this.formatMoment);
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: '001-LLEGADAS',
      dataGrid: this.dataGrid,
      global: this.dataGlobal,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      hora: moment().format('HH:mm:ss'),
      form: form,
    }
    this._httpClient.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = this.heading + '.pdf';
        download.click();
      },
      error => {
        this.swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
      }
    );
  }

  public allData(): ExcelExportData {
    if (this.filter) {
      const result: ExcelExportData = {
        data: process(this.dataComplete, { sort: [{ field: 'tapellido', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    } else {
      const result: ExcelExportData = {
        data: process(this.dataComplete, { sort: [{ field: 'tapellido', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    }
  }

}
