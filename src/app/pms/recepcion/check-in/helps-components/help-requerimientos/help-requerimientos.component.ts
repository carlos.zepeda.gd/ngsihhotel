import { Component, EventEmitter, Output } from '@angular/core';
import { CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';
import { RequerimientosService } from '../../services/childs/requerimientos.service';
import { GLOBAL } from '../../../../../services/global';

@Component({
  selector: 'app-help-requerimientos',
  templateUrl: './help-requerimientos.component.html',
  styleUrls: ['./help-requerimientos.component.css']
})
export class HelpRequerimientosComponent {
  @Output() requerimiento: any = new EventEmitter;
  loading = false;
  pageSize = 10;
  skip = 0;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];

  public filter: CompositeFilterDescriptor;
  public dataComplete: any = [];

  constructor(private _req: RequerimientosService) {
    this.loading = true;
    this._req.getRequerimientos().subscribe(res => {
      this.dataGrid = res;
      this.loading = false;
    });
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  selectRow({ selectedRows }) {
    this.requerimiento.emit(selectedRows[0].dataItem);
  }

}
