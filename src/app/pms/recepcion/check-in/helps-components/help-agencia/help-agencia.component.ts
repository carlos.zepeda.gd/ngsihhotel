import { Component, Output, EventEmitter } from '@angular/core';
import { HelpsCheckInService } from '../../services/service-checkin.index';

@Component({
  selector: 'app-help-agencia',
  templateUrl: './help-agencia.component.html',
  styleUrls: ['./help-agencia.component.css']
})
export class HelpAgenciaComponent {

  @Output() agencia: any = new EventEmitter;
  public dataGrid: any = [];
  public loading: boolean;

  constructor(private help: HelpsCheckInService) {
  }

  selectRow({ selectedRows }) {
    this.agencia.emit(selectedRows[0].dataItem);
  }

  searchForName(e) {
    this.loading = true;
    this.help.getSearch('agennom', e).subscribe(data => {
      if (data.ttAgencias) {
        this.dataGrid = data.ttAgencias;
      }
      this.loading = false;
    });
  }
}
