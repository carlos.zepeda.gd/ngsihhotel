import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {GLOBAL} from '../../../../../services/global';
import {CompositeFilterDescriptor, distinct, orderBy} from '@progress/kendo-data-query';
import {CrudService} from '../../../../manttos/mantto-srv.services';
import {PageChangeEvent} from '@progress/kendo-angular-grid';

@Component({
  selector: 'app-help-bandas',
  templateUrl: './help-bandas.component.html',
  styleUrls: ['./help-bandas.component.css']
})
export class HelpBandasComponent implements OnInit {
  @Output() banda: any = new EventEmitter;
  loading = false;
  pageSize = 10;
  skip = 0;
  closeResult: string;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];

  selectedRow: any;

  public filter: CompositeFilterDescriptor;
  public dataComplete: any = [];

  constructor(private _crud: CrudService) {
    this._crud.getData('/manttos/tbandas')
      .map(data => data = data.siTbanda.dsTbandas.ttTbandas)
      .subscribe(data => {
        if (data !== undefined) {
          this.dataGrid = data;
        } else {
          this.dataGrid = [];
          this.dataComplete = [];
        }

        this.loading = false;
      });
  }

  ngOnInit() {
  }

  selectRow({selectedRows}) {
    this.selectedRow = selectedRows[0].dataItem;
  }

  emitData() {
    this.banda.emit(this.selectedRow);
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
  }

}
