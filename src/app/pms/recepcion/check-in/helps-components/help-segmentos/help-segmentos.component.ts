import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { orderBy, SortDescriptor, CompositeFilterDescriptor, filterBy } from '@progress/kendo-data-query';
import { HelpsCheckInService } from '../../services/service-checkin.index';
declare var $: any;

@Component({
  selector: 'app-help-segmentos',
  templateUrl: './help-segmentos.component.html',
  styleUrls: ['./help-segmentos.component.css']
})
export class HelpSegmentosComponent implements OnInit {

  @Output() result: any = new EventEmitter;
  public dataGrid: any = [];
  public dataComplete: any = [];
  public selected: boolean;
  public loading: boolean;
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'anombre', dir: 'asc' }];

  constructor(private help: HelpsCheckInService) {}

  ngOnInit() {
    this.loading = true;
    this.help.getSegmentos().subscribe( data => {
        if (data) {
          this.dataGrid = data;
          this.dataComplete = data;
          this.loading = false;
          this.dataGrid = {
            data: orderBy(this.dataGrid, this.sort),
            total: this.dataGrid.length
          };
        }
     });
  }
  selectRow({selectedRows}) {
    this.result.emit(selectedRows[0].dataItem);
    this.selected = true;
    $('#myTab a[href="#tab4"]').hide();
  }
  // Funciones para la Grid de Kendo
  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }
}
