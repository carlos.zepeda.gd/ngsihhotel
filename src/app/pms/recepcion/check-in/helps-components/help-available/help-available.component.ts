import { Component, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { HelpsCheckInService } from '../../services/service-checkin.index';
import { GLOBAL } from 'app/services/global';
import moment from 'moment';
import swal from 'sweetalert2';
const ca = GLOBAL.char174;
declare var $: any;

@Component({
  selector: 'app-help-available',
  templateUrl: './help-available.component.html',
  styleUrls: ['./help-available.component.css'],
})
export class HelpAvailableComponent implements OnChanges {
  @Output() typeRoom: any = new EventEmitter;
  @Input() value: any;
  @Input() reserva: any;
  @Input() secuencia: any;
  @Input() nights: number;
  public grid: Array<any> = [];
  public selected: boolean;
  selectedRow: any;
  urlServ: string;
  tcuarto: any;
  formatMoment = localStorage.getItem('formatMoment');

  constructor(private help: HelpsCheckInService) {
    this.selected = false;
  }
  ngOnChanges(change: SimpleChanges) {
    if (change.value && change.value.currentValue && change.value.firstChange) {
      this.disponibilidadCheckin();
    }
    if (change['secuencia'] && change['secuencia'].currentValue && change['secuencia'].firstChange) {
      this.disponibilidadReserva();
    }
  }
  disponibilidadCheckin() {
    const f1 = moment(this.value.hfent).format(GLOBAL.formatIso);
    const f2 = moment(this.value.hfsal).format(GLOBAL.formatIso);
    if (!this.value.hjunior) { this.value.hjunior = 0; }
    if (!this.value.hmenpag) { this.value.hmenpag = 0; }
    this.urlServ = `${f1 + '/' + f2 + '/' + '' + ca + this.value.hadultos + ca + this.value.hjunior + ca + this.value.hmenpag}`;
    this.getDisponibilidad(this.urlServ);
  }

  disponibilidadReserva() {
    const f1 = moment(this.secuencia.rfent).format(GLOBAL.formatIso);
    const f2 = moment(this.secuencia.rfsal).format(GLOBAL.formatIso);
    if (!this.secuencia.rjunior) { this.secuencia.rjunior = 0; }
    if (!this.secuencia.rmenpag) { this.secuencia.rmenpag = 0; }
    if (this.secuencia.rgru === 'G' || this.reserva.rtmov === 'G') {
      this.secuencia.radultos = 1;
      this.secuencia.rjunior = 0;
      this.secuencia.rmenpag = 0;
    }
    this.urlServ = `${f1 + '/' + f2 + '/' + '' + ca + this.secuencia.radultos + ca + this.secuencia.rjunior + ca + this.secuencia.rmenpag}`;
    this.getDisponibilidad(this.urlServ);
  }

  getDisponibilidad(service) {
    this.grid = [];
    this.help.getAvaiable2(service).subscribe(data => {
      if (data[0].cMensTxt) {
        return swal(data[0].cMensTit, data[0].cMensTxt, 'error').then(() => this.typeRoom.emit('error'));
      }
      this.grid = data;
      this.appendTable();
    });
  }

  appendTable() {
    this.secuencia ? this.tcuarto = this.secuencia.tcuarto : this.tcuarto = this.value.tcuarto;
    $('#theadDisp').append('<th id="menugrid" style="position:sticky;top:0;z-index:10;background: white;">Tipo Cuarto</th>');
    for (let i = 0; i < this.grid.length; i++) { // For para imprimir los cuartos
      $('#trbody').append('<tr class="dispField_" id="dispField_' + i + '">' +
        '<td style="width: 150px">' +
        '<button value="' + this.grid[i].ttcto + '" class="btn btn-secondary btn-block ' + this.grid[i].ttcto + '">' +
        this.grid[i].ttcto +
        '</button>' +
        '</td></tr>');
      i = (i + Number(this.nights)) - 1;
      const x = (i - Number(this.nights)) + 1;

      if (this.tcuarto === this.grid[i].ttcto) {
        $('.' + this.grid[i].ttcto).css({ 'background': '#ffc107', 'color': 'black' });
      }

      if (this.selectedRow) {
        if (this.tcuarto) {
          $('.' + this.tcuarto).css({ 'background': '#6c757d', 'color': 'white' });
        }
        $('.' + this.selectedRow.ttcto).css({ 'background': '#ffc107', 'color': 'black' });
      }
      $('#dispField_' + x).click(() => {
        this.selectedRow = this.grid[x];
        $('.dispField_').dblclick(() => {
          return this.aceptar();
        });
        setTimeout(() => {
          $('#theadDisp').empty();
          $('#trbody').empty();
          this.appendTable();
        });
      });
    }
    for (let b = 0; b < Number(this.nights); b++) { // For para imprimir los encabezados
      $('#theadDisp').append(
        '<th style="min-width:100px;text-align:center;position:sticky;top:0;z-index:10;background: white;">' +
        this.grid[b].tporc.toFixed(2) + '%' +
        '<br>' + moment(this.grid[b].tfec).format(this.formatMoment) + '' +
        '<br>' + this.grid[b].tdia + '' +
        '<br><span class="text-primary">' + this.grid[b].trest + '</span>' +
        '</th>'
      );
    }
    let cont = 0; // Contador para imprimir uno por uno cada registro
    let cont2 = 0; // Numero de veces que se imprime la disponibilidad en una fila (Depende del numero de noches)
    let c = 0; // Variable que imprime el numero de disponiblidad de cada fecha en cada cuarto
    for (c = 0; c < Number(this.nights); c++) { // For para imprimir la disponibilidad de cada cuarto
      for (let d = 0; d < this.grid.length; d++) {
        // tslint:disable-next-line:max-line-length
        $('#dispField_' + c).append('<td style="min-width:100px;text-align: center;" id="disp_' + cont + '">' + this.grid[cont].tcd + '</td>');
        if (this.grid[cont].tcd < 0) {
          $('#disp_' + cont).addClass('text-danger');
        }
        cont++;
        cont2++;
        if (cont2 === Number(this.nights)) {
          // Si nuestro cont2 es igual al numero de noches
          // sumamos a la variable c el numero de noches para imprimir la disponibilidad en el siguiente cuarto
          c = c + Number(this.nights); // Sumar el numero de noches a la variable c para dar el brinco de columna
          cont2 = 0;  // Regresamos nuestro contador a 0 para continuar el ciclo
        }
      }
    }
  }

  aceptar() {
    if (this.selectedRow) {
      this.typeRoom.emit(this.selectedRow);
    }
  }
}
