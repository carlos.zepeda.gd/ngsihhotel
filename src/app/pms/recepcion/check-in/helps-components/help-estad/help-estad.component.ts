import { Component, ViewEncapsulation, EventEmitter, Output, OnChanges, SimpleChanges, Input } from '@angular/core';
import { HelpsCheckInService } from '../../services/service-checkin.index';
import { orderBy, SortDescriptor, CompositeFilterDescriptor, filterBy } from '@progress/kendo-data-query';

@Component({
  selector: 'app-help-estad',
  templateUrl: './help-estad.component.html',
  styleUrls: ['./help-estad.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HelpEstadComponent implements OnChanges {

  @Output() estad: any = new EventEmitter;
  @Input() value: string;
  public dataGrid: any = [];
  public dataComplete: any = [];
  public selected: boolean;
  public loading: boolean;
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'estad', dir: 'asc' }];
  constructor(private help: HelpsCheckInService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if ( changes.value.currentValue && changes.value.firstChange ) {
      this.loading = true;
      this.help.getEstad(changes.value.currentValue).subscribe( data => {
          if (!data) { data = ''; }
          this.dataGrid = data;
          this.dataComplete = data;
          this.loading = false;
          this.dataGrid = {
            data: orderBy(this.dataGrid, this.sort),
            total: this.dataGrid.length
          };
      });
    }
  }

  selectRow({selectedRows}) {
    this.estad.emit(selectedRows[0].dataItem);
    this.selected = true;
  }
  // Funciones para la Grid de Kendo
  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }
}
