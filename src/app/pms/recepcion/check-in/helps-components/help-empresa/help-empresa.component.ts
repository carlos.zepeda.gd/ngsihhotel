import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HelpsCheckInService } from '../../services/service-checkin.index';

@Component({
  selector: 'app-help-empresa',
  templateUrl: './help-empresa.component.html',
  styleUrls: ['./help-empresa.component.css']
})
export class HelpEmpresaComponent {

  @Output() empresa: any = new EventEmitter;
  public dataGrid: any = [];
  public dataComplete: any = [];
  public loading: boolean;

  constructor(private help: HelpsCheckInService) { }

  public selectRow({ selectedRows }) {
    this.empresa.emit(selectedRows[0].dataItem);
  }
 
  searchForName(e) {
    this.loading = true;
    this.help.getSearch('empnom', e).subscribe(data => {
      if (data.ttEmpresas) {
        this.dataGrid = data.ttEmpresas;
      }
      this.loading = false;
    });
  }
}
