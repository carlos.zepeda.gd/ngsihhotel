import { Component, EventEmitter, Output } from '@angular/core';
import { GLOBAL } from '../../../../../services/global';
import { CrudService } from '../../../../manttos/mantto-srv.services';
import { PageChangeEvent } from '@progress/kendo-angular-grid';
import { CompositeFilterDescriptor, distinct, orderBy } from '@progress/kendo-data-query';

@Component({
  selector: 'app-help-tarifas',
  templateUrl: './help-tarifas.component.html',
  styleUrls: ['./help-tarifas.component.css']
})
export class HelpTarifasComponent {
  @Output() tarifas: any = new EventEmitter;
  loading = false;
  pageSize = 10;
  skip = 0;
  closeResult: string;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];

  selectedRow: any;

  public filter: CompositeFilterDescriptor;
  public dataComplete: any = [];


  constructor(private _crud: CrudService) {
    this._crud.getData('/manttos/tarifas/T®️Y').map(data => data).subscribe(data => {
      const res = data.siTarifa.dsTarifas.ttTarifas;
      if (res) {
        this.dataGrid = res;
      }
      this.loading = false;
    });
  }


  public selectRow({ selectedRows }) {
    this.tarifas.emit(selectedRows[0].dataItem);
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
  }

}
