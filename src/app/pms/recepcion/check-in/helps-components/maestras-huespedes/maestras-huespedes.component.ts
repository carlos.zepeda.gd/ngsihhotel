import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { HelpsCheckInService } from '../../services/service-checkin.index';
import { orderBy, SortDescriptor, CompositeFilterDescriptor, filterBy } from '@progress/kendo-data-query';
import { RecepcionService } from 'app/pms/recepcion/recepcion.service';
declare var $: any;

@Component({
  selector: 'app-maestras-huespedes',
  templateUrl: './maestras-huespedes.component.html',
  styleUrls: ['./maestras-huespedes.component.css']
})
export class MaestrasHuespedesComponent implements OnInit {

  @Output() result: any = new EventEmitter;
  public dataGrid: any = [];
  public dataComplete: any = [];
  public selected: boolean;
  public loading: boolean;
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'Grupo', dir: 'asc' }];
  
  constructor(private rec: RecepcionService) { }

  ngOnInit() {
    this.loading = true;
    this.rec.getEstatusHuesped('MA').subscribe( data => {
        if (!data) { data = ''; }
        this.dataGrid = data;
        this.dataComplete = data;
        this.loading = false;
        this.dataGrid = {
            data: orderBy(this.dataGrid, this.sort),
            total: this.dataGrid.length
        };
    });
  }
  selectRow({selectedRows}) {
    this.result.emit(selectedRows[0].dataItem);
    this.selected = true;
    $('#myTab a[href="#tab4"]').hide();
  }
  // Funciones para la Grid de Kendo
  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }
}
