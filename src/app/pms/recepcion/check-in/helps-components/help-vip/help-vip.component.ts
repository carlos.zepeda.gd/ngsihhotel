import { Component, OnInit, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { HelpsCheckInService } from '../../services/service-checkin.index';
import { orderBy, SortDescriptor, CompositeFilterDescriptor, filterBy } from '@progress/kendo-data-query';
declare var $: any;

@Component({
  selector: 'app-help-vip',
  templateUrl: './help-vip.component.html',
  styleUrls: ['./help-vip.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HelpVipComponent implements OnInit {

  @Output() vip: any = new EventEmitter;
  public dataGrid: any = [];
  public dataComplete: any = [];
  public selected: boolean;
  public loading: boolean;
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'vip', dir: 'asc' }];
  constructor(private help: HelpsCheckInService) {}

  ngOnInit() {
      this.loading = true;
      this.help.getVip().subscribe( data => {
            if (!data) { data = ''; }
            this.dataGrid = data;
            this.dataComplete = data;
            this.loading = false;
            this.dataGrid = {
                data: orderBy(this.dataGrid, this.sort),
                total: this.dataGrid.length
            };
      });
  }
  selectRow({selectedRows}) {
      this.vip.emit(selectedRows[0].dataItem);
      this.selected = true;
      $('#myTab a[href="#tab4"]').hide();
  }
  // Funciones para la Grid de Kendo
  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }
}
