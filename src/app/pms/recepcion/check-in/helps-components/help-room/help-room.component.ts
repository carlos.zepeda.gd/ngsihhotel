import { Component, ViewEncapsulation, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { HelpsCheckInService } from '../../services/service-checkin.index';
import { CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';
import { GLOBAL } from '../../../../../services/global';
import moment from 'moment';
import swal from 'sweetalert2';
import { CrudService } from 'app/pms/mantto-srv.services';
@Component({
  selector: 'app-help-room',
  templateUrl: './help-room.component.html',
  styleUrls: ['./help-room.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HelpRoomComponent implements OnChanges {
  @Output() room: any = new EventEmitter;
  @Input() secuencia: any = [];
  @Input() huesped: any = [];
  dataGrid: any = [];
  loading: boolean = true;
  filter: CompositeFilterDescriptor;
  pageSize = 10;
  skip = 0;
  dsCaracteristicas: any = [];
  tipoCuartoAnterior: any;
  constructor(private _help: HelpsCheckInService, private _crud: CrudService) {
    this._help.getCaracteristicas().subscribe(data => {
      this.dsCaracteristicas = distinct(data, 'caract').map(item => item['caract']);
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.secuencia && changes.secuencia.currentValue) {
      this.secuencia.rfent = moment(this.secuencia.rfent).format(GLOBAL.formatIso);
      this.secuencia.rfsal = moment(this.secuencia.rfsal).format(GLOBAL.formatIso);
      this._help.getHabDisponibles(this.secuencia.rfent, this.secuencia.rfsal, 'R').subscribe(data => {
        if (data) {
          data.forEach(item => {
            item.ccaract = item.ccaract.split(' ').sort();
            item.ccaract = item.ccaract.join(' ');
          });

          this.dataGrid = data;
          if (Array.isArray(this.secuencia.rcaract)) {
            this.secuencia.rcaract = this.secuencia.rcaract.join(' ');
          } else {
            this.secuencia.rcaract = '';
          }
          this.tipoCuartoAnterior = this.secuencia.tcuarto;
          this.filter = {
            logic: 'and',
            filters: [
              { field: 'tcuarto', operator: 'eq', value: this.secuencia.tcuarto },
              { field: 'cellaves', operator: 'eq', value: 'V' },
            ]
          };
          if (this.secuencia.rcaract) {
            this.filter.filters.push({ field: 'ccaract', operator: 'contains', value: this.secuencia.rcaract })
          }
        }
        this.loading = false;
      });
    }
    if (changes.huesped && changes.huesped.currentValue && changes.huesped.firstChange) {
      this.huesped.hfent = moment(this.huesped.hfent).format(GLOBAL.formatIso);
      this.huesped.hfsal = moment(this.huesped.hfsal).format(GLOBAL.formatIso);
      this._help.getHabDisponibles(this.huesped.hfent, this.huesped.hfsal, 'F').subscribe(data => {
        if (data) {
          data.forEach(item => {
            item.ccaract = item.ccaract.split(' ').sort();
            item.ccaract = item.ccaract.join(' ');
          });
          this.dataGrid = data;
          if (Array.isArray(this.huesped.hcaract)) {
            this.huesped.hcaract = this.huesped.hcaract.join(' ');
          } else {
            this.huesped.hcaract = '';
          }
          this.tipoCuartoAnterior = this.huesped.tcuarto;
          this.filter = {
            logic: 'and',
            filters: [
              { field: 'tcuarto', operator: 'eq', value: this.huesped.tcuarto },
              { field: 'cellaves', operator: 'eq', value: 'V' },
            ]
          };
          if (this.huesped.hcaract) {
            this.filter.filters.push({ field: 'ccaract', operator: 'contains', value: this.huesped.hcaract })
          }
        }

        this.loading = false;
      });
    }
  }
  selectRow({ selectedRows }) {
    const roomSelected = selectedRows[0].dataItem;
    roomSelected['calcular'] = false;
    if (roomSelected.tcuarto !== this.tipoCuartoAnterior) {
      swal({
        title: 'Tipo de cuarto es diferente', text: '¿Continuar?', type: 'question',
        showCancelButton: true, confirmButtonText: 'Si', cancelButtonText: 'No',
      }).then((result) => {
        if (result.value) {
          roomSelected['calcular'] = true;
          this._crud.getData('/manttos/tcuartos/' + roomSelected.tcuarto).subscribe(res => {
            this._crud.getData('/manttos/tcuartos/' + this.tipoCuartoAnterior).subscribe(res2 => {
              if (res.siTcuarto.dsTcuartos.ttTcuartos) {
                const tcuartoSelected = res.siTcuarto.dsTcuartos.ttTcuartos[0];
                const tcuartoAnterior = res2.siTcuarto.dsTcuartos.ttTcuartos[0];
                if (tcuartoSelected.tagrp === tcuartoAnterior.tcuarto || tcuartoAnterior.tagrp === tcuartoSelected.tcuarto) {
                  roomSelected['upgrade'] = false;
                } else {
                  roomSelected['upgrade'] = true;
                }
                this.room.emit(roomSelected);
              }
            });
          });
        }
      });
      return;
    }
    this.room.emit(roomSelected);
  }
  distinctPrimitive(fieldName: string) {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }
  distinct2Primitive(fieldName: string) {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }
}
