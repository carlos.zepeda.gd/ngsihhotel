import { Component, OnInit, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { HelpsCheckInService } from '../../services/service-checkin.index';
import { SortDescriptor, CompositeFilterDescriptor } from '@progress/kendo-data-query';

@Component({
  selector: 'app-help-estado',
  templateUrl: './help-estado.component.html',
  styleUrls: ['./help-estado.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HelpEstadoComponent implements OnInit {

  @Output() estado: any = new EventEmitter;
  public dataGrid: any = [];
  public selected: boolean;
  public loading: boolean = true;
  pageSize = 10;
  skip = 0;
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'Estado', dir: 'asc' }];

  constructor(private _help: HelpsCheckInService) {
    this._help.getEstados().subscribe(data => this.dataGrid = data);
    this.loading = false;
  }

  ngOnInit() {
  }

  selectRow({ selectedRows }) {
    this.estado.emit(selectedRows[0].dataItem);
    this.selected = true;
  }
}
