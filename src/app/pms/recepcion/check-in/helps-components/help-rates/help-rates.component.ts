import { Component, OnInit, ViewEncapsulation, Output, EventEmitter, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { HelpsCheckInService } from '../../services/service-checkin.index';
import { SortDescriptor, CompositeFilterDescriptor } from '@progress/kendo-data-query';

@Component({
  selector: 'app-help-rates',
  templateUrl: './help-rates.component.html',
  styleUrls: ['./help-rates.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HelpRatesComponent implements OnInit, OnChanges, OnDestroy {
  @Output() rate: any = new EventEmitter;
  @Input() value: any;
  @Input() reserva: any;
  @Input() tarifa: string;
  @Output() exit: any = new EventEmitter;
  public dataGrid: any = [];
  public selected: boolean;
  public loading: boolean;
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'ttarifa', dir: 'asc' }];
  selectedRow: any;
  existeTarifa: any;
  flagChangeTarifa: boolean;

  constructor(private _help: HelpsCheckInService) {
  }

  ngOnInit() {
  }
  ngOnDestroy() {
    if (!this.flagChangeTarifa) {
      this.exit.emit(this.existeTarifa);
    }
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['value'] && changes['value'].currentValue && changes['value'].firstChange) {
      this.loading = true;
      this._help.getRates(this.value).subscribe(data => this.responseTarifas(data));
    }
    if (changes['reserva'] && changes['reserva'].currentValue && changes['reserva'].firstChange) {
      this.loading = true;
      this._help.getRatesReserva(this.reserva).subscribe(data => this.responseTarifas(data));
    }
  }
  responseTarifas(data) {
    if (data) {
      this.dataGrid = data;
      if (this.tarifa) {
        this.dataGrid.ttTmptar.forEach(item => {
          if (item.ttarifa === this.tarifa) {
            this.existeTarifa = item;
          }
        });
      }
    }
    this.loading = false;
  }
  aceptar() {
    this.flagChangeTarifa = true;
    if (this.selectedRow) {
      this.rate.emit(this.selectedRow);
    }
  }
  selectRow({ selectedRows }) {
    this.selectedRow = selectedRows[0].dataItem;
    this._help.buscarTarifa(this.selectedRow).subscribe(data => {
      if (data) {
        this.selectedRow['Restringida'] = data[0].Restringida;
        if (this.reserva && this.reserva.secuencia) {
          this.selectedRow['secuencia'] = this.reserva.secuencia.rsecuencia;
        }
        this.selected = true;
      }
    });
  }
}
