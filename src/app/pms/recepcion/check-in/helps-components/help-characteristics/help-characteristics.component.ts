import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { orderBy, SortDescriptor, CompositeFilterDescriptor, filterBy } from '@progress/kendo-data-query';
import { HelpsCheckInService } from '../../services/service-checkin.index';
declare var $: any;

@Component({
  selector: 'app-help-characteristics',
  templateUrl: './help-characteristics.component.html',
  styleUrls: ['./help-characteristics.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HelpCharacteristicsComponent implements OnInit {

  @Output() charact: any = new EventEmitter;
  public dataGrid: any = [];
  public dataComplete: any = [];
  public selected: boolean;
  public loading: boolean;
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'caract', dir: 'asc' }];
  
  constructor(private help: HelpsCheckInService) {}

  ngOnInit() {
    this.loading = true;
    this.help.getCaracteristicas().subscribe(data => {
      if (!data) { data = ''; }
      this.dataComplete = data;
      this.dataGrid = data;
      this.loading = false;
      this.dataGrid = {
        data: orderBy(this.dataGrid, this.sort),
        total: this.dataGrid.length
      };
    });
  }
  public selectRow({selectedRows}) {
    this.charact.emit(selectedRows[0].dataItem);
    this.selected = true;
    $('#myTab a[href="#tab4"]').hide();
  }
  // Funciones para la Grid de Kendo
  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }
}
