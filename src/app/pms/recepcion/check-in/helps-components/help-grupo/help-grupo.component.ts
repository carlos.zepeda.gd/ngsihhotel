import { Component, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { HelpsCheckInService } from '../../services/service-checkin.index';

@Component({
  selector: 'app-help-grupo',
  templateUrl: './help-grupo.component.html',
  styleUrls: ['./help-grupo.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HelpGrupoComponent {

  @Output() grupo: any = new EventEmitter;
  public dataGrid: any = [];
  public loading: boolean;

  constructor(private help: HelpsCheckInService) { }

  selectRow({ selectedRows }) {
    this.grupo.emit(selectedRows[0].dataItem);
  }
  
  searchForName(e) {
    this.loading = true;
    this.help.getSearch('gponom', e).subscribe(data => {
      if (data.ttGrupos) {
        this.dataGrid = data.ttGrupos;
      }
      this.loading = false;
    });
  }
}
