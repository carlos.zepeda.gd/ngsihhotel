import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges, OnChanges } from '@angular/core';
import { orderBy, SortDescriptor, CompositeFilterDescriptor, filterBy } from '@progress/kendo-data-query';
import { HelpsCheckInService } from '../../services/service-checkin.index';
import { CrudService } from '../../../../mantto-srv.services';

@Component({
  selector: 'app-help-tipo-cuartos',
  templateUrl: './help-tipo-cuartos.component.html',
  styleUrls: ['./help-tipo-cuartos.component.css']
})
export class HelpTipoCuartosComponent implements OnInit, OnChanges {

  @Output() result: any = new EventEmitter;
  @Input() virtual: boolean;
  dataGrid: any = [];
  public dataComplete: any = [];
  public selected: boolean;
  public loading: boolean;
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'anombre', dir: 'asc' }];

  constructor(private _help: HelpsCheckInService) { 
    this.loading = true;
  }
  ngOnInit() {

  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.virtual && changes.virtual.currentValue) {
      this._help.getTipoCuartos().subscribe(data => {
        if (data) {
          data.forEach(item => {
            if (item.tvirtual) {
              this.dataGrid.push(item);
            }
          });
          this.dataComplete = this.dataGrid;
          this.loading = false;
          this.dataGrid = {
            data: orderBy(this.dataGrid, this.sort),
            total: this.dataGrid.length
          };
        }
      });
    }else {
      this._help.getTipoCuartos().subscribe(data => {
        if (data) {
          this.dataGrid = data;
          this.dataComplete = data;
          this.loading = false;
          this.dataGrid = {
            data: orderBy(this.dataGrid, this.sort),
            total: this.dataGrid.length
          };
        }
      });
    }
  }
  selectRow({ selectedRows }) {
    this.result.emit(selectedRows[0].dataItem);
    this.selected = true;
  }

  // Funciones para la Grid de Kendo
  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }
}

