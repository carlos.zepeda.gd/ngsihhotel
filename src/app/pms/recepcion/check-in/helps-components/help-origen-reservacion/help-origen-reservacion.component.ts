import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {GLOBAL} from '../../../../../services/global';
import {CompositeFilterDescriptor, distinct} from '@progress/kendo-data-query';
import {CrudService} from '../../../../manttos/mantto-srv.services';
import {PageChangeEvent} from '@progress/kendo-angular-grid';

@Component({
  selector: 'app-help-origen-reservacion',
  templateUrl: './help-origen-reservacion.component.html',
  styleUrls: ['./help-origen-reservacion.component.css']
})
export class HelpOrigenReservacionComponent implements OnInit {
  @Output() oResev: any = new EventEmitter;
  loading = false;
  pageSize = 10;
  skip = 0;
  closeResult: string;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];

  selectedRow: any;

  public filter: CompositeFilterDescriptor;
  public dataComplete: any = [];

  constructor(private _crud: CrudService) {
    this._crud.getData('/manttos/origenes')
      .subscribe(data => {

        if (data.siOrigen.dsOrigenes.ttOrigenes !== undefined) {
          this.dataGrid = data.siOrigen.dsOrigenes.ttOrigenes;
        } else {
          this.dataGrid = [];
        }

        this.loading = false;
      });
  }

  ngOnInit() {
  }

  selectRow({selectedRows}) {
    this.selectedRow = selectedRows[0].dataItem;
  }

  emitData() {
    this.oResev.emit(this.selectedRow);
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
  }

}
