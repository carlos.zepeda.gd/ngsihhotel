import { Component, OnInit, ViewEncapsulation, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-help-detailrates',
  templateUrl: './help-detailrates.component.html',
  styleUrls: ['./help-detailrates.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HelpDetailratesComponent implements OnInit, OnChanges {
  @Input() selected: any;
  @Input() detalle: any = [];
  public dataGrid: Array<any> = [];
  public formatKendo = localStorage.getItem('formatKendo');

  constructor() { 
  }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.detalle && changes.detalle.currentValue) {
      this.detalle.forEach(item => {
        if (item.ttarifa === this.selected.ttarifa) {
          this.dataGrid.push(item);
        }
      });
    }
  }
}
