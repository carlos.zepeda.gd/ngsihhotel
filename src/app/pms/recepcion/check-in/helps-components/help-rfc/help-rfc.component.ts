import { Component, Output, EventEmitter } from '@angular/core';
import { HelpsCheckInService } from '../../services/service-checkin.index';
import { SortDescriptor, CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { PageChangeEvent } from '@progress/kendo-angular-grid';

@Component({
  selector: 'app-help-rfc',
  templateUrl: './help-rfc.component.html'
})
export class HelpRfcComponent {
  @Output() rfc: any = new EventEmitter;
  public dataGrid: any = [];
  public dataComplete: any = [];
  public loading: boolean;
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'rrsocial', dir: 'asc' }];
  pageSize = 10;
  skip = 0;

  constructor(private help: HelpsCheckInService) {
  }

  public selectRow({ selectedRows }) {
    this.rfc.emit(selectedRows[0].dataItem);
  }

  public searchForName(event: string) {
    if (!event) {
      return;
    }
    this.loading = true;
    this.help.getRfc('N', event).subscribe(data => {
      this.loading = false;
      if (data) {
        this.dataGrid = data;
        this.dataComplete = data;
      }
    });
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
  }
}
