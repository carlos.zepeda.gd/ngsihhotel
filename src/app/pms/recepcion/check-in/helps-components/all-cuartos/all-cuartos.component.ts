import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';
import { GLOBAL } from '../../../../../services/global';
import { CrudService } from '../../../../manttos/mantto-srv.services';

@Component({
  selector: 'app-all-cuartos',
  templateUrl: './all-cuartos.component.html',
  styleUrls: ['./all-cuartos.component.css']
})
export class AllCuartosComponent implements OnInit {
  @Output() allCrts: any = new EventEmitter;
  loading: boolean;
  pageSize = 10;
  skip = 0;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];
  filter: CompositeFilterDescriptor;

  constructor(private _crud: CrudService) {
    this.loading = true;
    this._crud.getData('/manttos/ctofso/s').subscribe(data => {
      this.dataGrid = data.siCtofso.dsCtofso.ttCuartos;
      this.loading = false;
    });
  }

  ngOnInit() {
  }

  selectRow({ selectedRows }) {
    if (selectedRows && selectedRows[0].dataItem) {
      this.allCrts.emit(selectedRows[0].dataItem);
    }
  }

  distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

}
