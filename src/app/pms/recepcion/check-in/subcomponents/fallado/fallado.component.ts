import { Component, Input, EventEmitter, Output } from '@angular/core';
import { FalladoService } from '../../services/service-checkin.index';
import { Fallado } from '../../models/fallado.model';
import swal from 'sweetalert2';
import moment from 'moment';
import { GLOBAL } from 'app/services/global';
@Component({
  selector: 'app-fallado',
  templateUrl: './fallado.component.html',
  styleUrls: ['./fallado.component.css']
})
export class FalladoComponent {
  @Input() folio: any;
  @Output() result = new EventEmitter;
  public hhotel: string;
  public htarifa: number;
  public hfecha: Date;
  public hfecreg: string;
  public fallado: Fallado;
  formatKendo = localStorage.getItem('formatKendo');

  constructor(public fs: FalladoService) {
  }

  guardarFallado(e) {
    this.hhotel = e.hhotel;
    this.htarifa = e.htarifa;
    this.hfecreg = e.hfecreg;
    this.fallado = new Fallado(this.folio.hfolio, this.hhotel, this.htarifa, moment(e.hfecha).format(GLOBAL.formatIso), '', '?', '');
    this.fs.postFallado(this.fallado).subscribe(data => {
      if (data && data[0].cMensTxt) {
        swal(data[0].cMensTit, data[0].cMensTxt, 'error');
      } else {
        this.result.emit(data[0]);
      }
    });
  }
}
