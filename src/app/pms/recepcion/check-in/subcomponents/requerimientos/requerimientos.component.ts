import { Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy, ViewChild, TemplateRef, Output, EventEmitter } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ComboBoxComponent } from '@progress/kendo-angular-dropdowns';
import { RequerimientosService } from '../../services/service-checkin.index';
import { InfociaGlobalService } from '../../../../../services/infocia.service';
import { ReservacionService } from '../../../../reservaciones/reservacion/services/reservacion.service';
import swal from 'sweetalert2';

@Component({
    selector: 'app-requerimientos',
    templateUrl: './requerimientos.component.html',
    styleUrls: ['./requerimientos.component.css']
})
export class RequerimientosComponent implements OnInit, OnChanges, OnDestroy {
    @Input() readOnly: boolean;
    @Input() folio: any = [];
    @Input() consulta: boolean;
    @Input() uso: string;
    @Output() close = new EventEmitter;
    @ViewChild('focus') _focus: ComboBoxComponent;
    @ViewChild('modalDisp') _modalDisp: TemplateRef<any>;
    public myFormGroup: FormGroup;
    public gridData: Array<any> = [];
    public ttInfocia: any = [];
    public loading: boolean;
    public noFolio: number;
    public grabarReq: boolean = true;
    public dataSelectReq: Array<{ rdesc: string, rreqesp: string }> = [];
    public dataMonedas: Array<{ mdesc: string, Moneda: string }> = [];
    public dataCuentas: Array<{ tcuentadesc: string, tipoCuenta: string }> = [];
    public dataCargos: Array<{ tcdesc: string, tipoCargo: string }> = [];
    public dsSecuencias: any = [];
    public lectura: boolean;
    public showGrid: boolean = true;
    public method: string;
    dsRequerimientos: { rdesc: string; rreqesp: string; }[];
    dsDisp: any = [];
    flagCosto: boolean;

    constructor(public _req: RequerimientosService,
        public _info: InfociaGlobalService,
        private _active: ActivatedRoute,
        private _reserv: ReservacionService,
        private _pf: FormBuilder,
        private _modalService: NgbModal,
    ) {
        this._active.data.subscribe(data => {
            this.uso = data.uso;
            this.consulta = data.consulta;
            if (this.consulta) { // Si esta en consulta de huesped validar que pueda grabar mensajes.
                if (this.uso === 'H') {
                    const mantto = this._info.manttoConsultaHuespedes(this._info.getSessionUser());
                    this.grabarReq = mantto[2];
                } else {
                    const mantto = this._info.manttoConsultaReserv(this._info.getSessionUser());
                    this.grabarReq = mantto[2];
                }
            }
        });
        this.buildFormGroup();
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.readOnly && changes.readOnly.currentValue) {
            this.grabarReq = false;
            this.consulta = true;
        }
        if (changes.folio && changes.folio.currentValue) {
            if (this.folio.rnreser) {
                this.uso = 'R';
                this.noFolio = this.folio.rnreser;
                if (this.folio.reactual !== 'VI') {
                    this.grabarReq = false;
                }
                this.getRequerimientos();

            } else {
                this.uso = 'H';
                if (this.folio.heactual === 'CO') {
                    this.grabarReq = false;
                }
                this.noFolio = this.folio.hfolio;
                this.getRequerimientos();

            }
        }
    }

    ngOnDestroy(): void {
        if (this.uso === 'R') {
            this._info.notifyOther({ option: 'notify-change-reserv', value: this.noFolio });
        } else {
            this._info.notifyOther({ option: 'notify-change-huesped', value: this.noFolio });
        }
        this.close.emit(true);
    }

    nuevo() {
        this.showGrid = false;
        this.method = 'N';
        this.myFormGroup.patchValue({ hmon: this.ttInfocia.Moneda, hfolio: this.noFolio });
        setTimeout(() => this._focus.focus(), 300);
    }

    selectRow(dataItem) {
        if (dataItem.hentregado === 'T' || dataItem.hentregado === 'C' || !this.grabarReq) {
            this.lectura = true;
        } else {
            this.lectura = false;
        }
        this.method = 'M';
        this.showGrid = false;
        this.myFormGroup.patchValue(dataItem);
    }

    regresar() {
        this._req.getReq(this.noFolio, this.uso).subscribe(data => {
            if (data.ttHuesreq) {
                this.gridData = data.ttHuesreq;
            } else {
                this.gridData = [];
            }
            this.buildFormGroup();
            this.showGrid = true;
            this.loading = false;
        });
    }

    delete(dataItem) {
        const form = JSON.parse(JSON.stringify(dataItem));
        if (form && form.hreqesp.rcosto) {
            form.hreqesp = form.hreqesp.rcosto;
        }
        this.loading = true;
        this.method = 'B';
        this._req.serviceRequerimiento(form, this.method, this.uso).subscribe(data => {
            if (data) {
                if (data[0].cMensTxt) {
                    swal(data[0].cMensTit, data[0].cMensTxt, 'error');
                } else {
                    this.regresar();
                }
            }
        });
    }

    guardar() {
        const form = JSON.parse(JSON.stringify(this.myFormGroup.value));
        form.hreqesp = form.hreqesp.rreqesp;
        this.loading = true;
        this._req.serviceRequerimiento(form, this.method, this.uso).subscribe(data => {
            if (data) {
                if (data[0].cMensTxt) {
                    swal(data[0].cMensTit, data[0].cMensTxt, 'error');
                } else {
                    this.regresar();
                }
            }
            this.loading = false;
        });
    }

    changeCodigo(codigo) {
        if (codigo && codigo.rcosto) {
            this.flagCosto = true;
            this.myFormGroup.patchValue({ htcargo: 'E' });
        } else {
            this.flagCosto = false;
            this.myFormGroup.patchValue({ htcargo: 'N' });
        }
        if (codigo.rdisp) {
            this.getDisponibilidad(codigo);
        }
    }

    getDisponibilidad(codigo) {
        this._req.getDisponibilidad(codigo.rreqesp, this.folio.rfent, this.folio.rfsal).subscribe(data => {
            this.dsDisp = data;
            this.openModal(this._modalDisp, 'lg');
        });
    }

    buildFormGroup() {
        this.myFormGroup = this._pf.group({
            hreqesp: ['', Validators.required],
            hsecuencia: [1],
            htcargo: ['', Validators.required],
            hcantidad: [1],
            hmonto: [0],
            hmon: ['', Validators.required],
            Satisfaccion: [''],
            cuarto: [''],
            depto: [''],
            hdesc: [''],
            hentregado: [''],
            hfecha: [''],
            hfecreg: [''],
            hfolio: [0],
            hnenter: [''],
            hnespecial: ['', Validators.required],
            hnini: [''],
            hnotas: [''],
            hnsatisf: [''],
            hnterm: [''],
            hnumreq: [0],
            htfolio: [''],
            ubicacion: [''],
            cRowID: [''], lError: [false], cErrDes: ['']
        });
    }

    openModal(modal, size) {
        this._modalService.open(modal, { size: size, centered: true });
    }

    secuenciaSelected(e) {
        this.myFormGroup.patchValue({ hsecuencia: e.rsecuencia });
    }

    getRequerimientos() {
        this._req.getReq(this.noFolio, this.uso).subscribe(data => {
            if (data.ttHuesreq) {
                this.gridData = data.ttHuesreq;
            } else {
                this.gridData = [];
            }
            if (data.ttReqesp) {
                this.dataSelectReq = data.ttReqesp;
                data.ttReqesp.forEach(item => item.rdesc = item.rdesc + ' - ' + item.rreqesp);
                this.dsRequerimientos = data.ttReqesp;

            }
            this.dataMonedas = data.ttMonedas;
        });
        if (this.uso === 'R') {
            this._reserv.getSecuencias(this.noFolio).subscribe(data => {
                if (data) {
                    this.dsSecuencias = data;
                }
            });
        }
        this.dataCuentas = this._req.getCuentas();
        this.dataCargos = this._req.getCargos();
        this.ttInfocia = this._info.getSessionInfocia();
    }

    handleFilter(value) { // Incluirlo en un servicio para utilizarlo en Reservaciones y Huespedes
        this.dsRequerimientos = this.dataSelectReq.filter((s) => {
            const res = s.rreqesp.toLowerCase().indexOf(value.toLowerCase()) !== -1;
            if (!res) { return s.rdesc.toLowerCase().indexOf(value.toLowerCase()) !== -1; }
            return res;
        });
    }
}
