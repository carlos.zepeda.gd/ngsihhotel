import { Component, OnInit, Input, SimpleChanges, OnChanges, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SignaturePad } from 'angular2-signaturepad';
import { InfociaGlobalService } from '../../../../../services/infocia.service';
import { AdicionalesService } from '../../services/service-checkin.index';
import { GLOBAL } from '../../../../../services/global';
import swal from 'sweetalert2';
import moment from 'moment';

@Component({
  selector: 'app-adicionales',
  templateUrl: './adicionales.component.html',
  styleUrls: ['./adicionales.component.css']
})
export class AdicionalesComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  @Input() folio: any;
  @Input() consulta: boolean;
  @Input() uso: string;
  public formatKendo = localStorage.getItem('formatKendo');
  public formatMoment = localStorage.getItem('formatMoment');
  public noFolio: number;
  public grabarAdicional: boolean = true;
  public loading;
  public method: string;
  public myFormGroup: FormGroup;
  public gridData: any = [];
  public showForm = false;
  public dataTipoHuesped: Array<{ tdesc: string, tipo: string }> = [];
  public dsTitulos: Array<{ tdesc: string, Titulo: string }> = [];
  public dsPaises: Array<{ pnombre: string, Pais: string }> = [];
  public dsCuartos: Array<{ cuarto: string }> = [];
  public dsTipoDocumento: Array<{ nombre: string, tdoc: string }> = [];
  flagIsMobile: boolean;
  webcamImage: any = null;
  signatureImage: any;

  constructor(private _info: InfociaGlobalService,
    private _adi: AdicionalesService,
    private _pf: FormBuilder,
    private _active: ActivatedRoute,
    private _modal: NgbModal,
  ) {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      this.flagIsMobile = true;
    } else {
      this.flagIsMobile = false;
    }

    this._active.data.subscribe(data => {
      this.uso = data.uso;
      this.consulta = data.consulta;
      if (this.consulta) { // Si esta en consulta de huesped validar que pueda grabar adicionales.
        if (this.uso === 'H') {
          const mantto = this._info.manttoConsultaHuespedes(this._info.getSessionUser());
          this.grabarAdicional = mantto[5];
        } else {
          this.grabarAdicional = false;
        }
      }
    });
  }

  ngOnInit() {
    this.dsAdicional();
    this.buildFormGroup();
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (changes.folio.currentValue) {
      if (this.folio.rnreser) {
        this.uso = 'R';
        this.noFolio = this.folio.rnreser;
        if (this.folio.reactual !== 'VI') {
          this.grabarAdicional = false;
        }
      } else {
        this.uso = 'H';
        this.noFolio = this.folio.hfolio;
        if (this.folio.heactual === 'CO') {
          this.grabarAdicional = false;
        }
      }
      this.getAdicional();
    }
  }
  ngOnDestroy(): void {
    if (this.uso === 'R') {
      this._info.notifyOther({ option: 'notify-change-reserv', value: this.noFolio });
    } else {
      this._info.notifyOther({ option: 'notify-change-huesped', value: this.noFolio });
    }
    this.webcamImage = '';
    this.signatureImage = '';
  }

  activeSign(modal) {
    this.openModal(modal);
  }

  activeCam(modal) {
    this.openModal(modal);
  }

  dsAdicional() {
    this.dataTipoHuesped = this._adi.getTipoHuesped();
    this._adi.getCuartos().subscribe(data => this.dsCuartos = data);
  }

  getAdicional() {
    this._adi.getAdicionales(this.noFolio, this.uso).subscribe(data => {
      if (data.ttHuesadic) {
        data.ttHuesadic.forEach(item => {
          if (item.fnac) {
            item.fnac = moment(item.fnac).toDate();
          }
          if (item.fepais) {
            item.fepais = moment(item.fepais).toDate();
          }
        });
        this.gridData = data.ttHuesadic;
      } else {
        this.gridData = [];
      }
      if (data.ttTitulos) {
        this.dsTitulos = data.ttTitulos;
      }
      if (data.ttInfdoctos) {
        this.dsTipoDocumento = data.ttInfdoctos;
      }
      if (data.ttPaises) {
        this.dsPaises = data.ttPaises;
      }
    });
  }

  buildFormGroup() {
    this.myFormGroup = this._pf.group({
      Pais: [{ value: '', disabled: !this.grabarAdicional }],
      Pasaporte: [{ value: '', disabled: !this.grabarAdicional }],
      TA: [''],
      Titulo: [{ value: '', disabled: !this.grabarAdicional }],
      cErrDes: [''],
      cRowID: [''],
      cuarto: [{ value: '', disabled: !this.grabarAdicional }],
      ehuesp: [{ value: '', disabled: !this.grabarAdicional }],
      fepais: [{ value: null, disabled: !this.grabarAdicional }],
      fnac: [{ value: null, disabled: !this.grabarAdicional }],
      hc1: [''], hc2: [''], hc3: [''], hc4: [''], hc5: [''], hc6: [''],
      hc7: [''], hc8: [''], hc9: [''], hc10: [''], hc11: [''], hc12: [''],
      hc13: [''], hc14: [''], hc15: [''], hc16: [''], hc17: [''], hc18: [''],
      hc19: [''], hc20: [''],
      hcontrol: [{ value: 0, disabled: !this.grabarAdicional }],
      hedad: [{ value: 0, disabled: !this.grabarAdicional }],
      hemail: [{ value: '', disabled: !this.grabarAdicional }, Validators.email],
      hfolio: [0],
      hnombre: [{ value: '', disabled: !this.grabarAdicional }, Validators.required],
      hapellido: [{ value: '', disabled: !this.grabarAdicional }, Validators.required],
      htipo: [{ value: '', disabled: !this.grabarAdicional }, Validators.required],
      hnotas: [{ value: '', disabled: !this.grabarAdicional }],
      lError: [false],
      lnac: [{ value: '', disabled: !this.grabarAdicional }],
      ncte: [{ value: 0, disabled: true }],
      pent: [{ value: '', disabled: !this.grabarAdicional }],
      pnac: [{ value: '', disabled: !this.grabarAdicional }],
      propiedad: [''],
      tdoc: [{ value: '', disabled: !this.grabarAdicional }],
      iFirma: [null],
      iFoto: [null],
      iIdfrente: [null],
      iIdreverso: [null],
      iOtrosfrente: [null],
      iOtrosreverso: [null],
      iPassfrente: [null],
      iPassreverso: [null],
    });
  }

  nuevoAdicional() {
    this.method = 'N'; // Registro nuevo
    this.showForm = true;
    this.webcamImage = '';
    this.signatureImage = '';
    this.myFormGroup.patchValue({ hfolio: this.noFolio, hcontrol: this.gridData.length + 1, ncte: 0 });
  }

  delete(dataItem) {
    this.method = 'B';
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(dataItem));
    if (temp.fepais) {
      temp.fepais = moment(temp.fepais).format(GLOBAL.formatIso);
    }
    if (temp.fnac) {
      temp.fnac = moment(temp.fnac).format(GLOBAL.formatIso);
    }
    this._adi.putAdicional(temp, this.method, this.uso).subscribe(data => {
      if (data) {
        if (data[0].cMensTxt) {
          swal(data[0].cMensTit, data[0].cMensTxt, 'error');
          this.loading = false;
          return;
        }
      }
      this.regresar();
    });
  }

  selectRow(dataItem) {
    this.showForm = true;
    this.method = 'M'; // Registro en modo edición
    this.webcamImage = dataItem.iFoto;
    this.signatureImage = dataItem.iFirma;
    this.myFormGroup.patchValue(dataItem);
  }

  regresar() {
    this.getAdicional();
    this.showForm = false;
    this.loading = false;
    this.myFormGroup.reset();
    this.buildFormGroup();
  }
  abrirFoto(foto) {
    if (foto) {
      swal({ html: "<img src='" + foto + "' style='width:20rem;'>" });
    }
  }
  guardar(dataItem) {
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(dataItem));
    if (temp.fepais) {
      temp.fepais = moment(temp.fepais).format(GLOBAL.formatIso);
    }
    if (temp.fnac) {
      temp.fnac = moment(temp.fnac).format(GLOBAL.formatIso);
    }
    this._adi.putAdicional(temp, this.method, this.uso).subscribe(data => {
      if (data && data[0].cMensTxt) {
        swal(data[0].cMensTit, data[0].cMensTxt, 'error');
        this.loading = false;
      } else {
        this.regresar();
      }
    });
  }

  resultSignature(event) {
    this.signatureImage = event;
    this.myFormGroup.patchValue({ iFirma: this.signatureImage });
  }

  resultWebcam(event) {
    this.webcamImage = event.imageAsDataUrl;
    this.myFormGroup.patchValue({ iFoto: this.webcamImage });
  }

  handleUpload(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    if (file) {
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.webcamImage = reader.result;
        this.myFormGroup.patchValue({ iFoto: this.webcamImage });
      };
    }
  }
  eliminarFoto(foto) {
    if (foto === 'foto') {
      this.webcamImage = '';
      this.myFormGroup.patchValue({ iFoto: '' });
    } else {
      this.signatureImage = '';
      this.myFormGroup.patchValue({ iFirma: '' });
    }
  }
  openModal(modal) {
    this._modal.open(modal, { size: 'md', centered: true, backdrop: 'static' });
  }
}
