import { Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { InfociaGlobalService } from '../../../../../services/infocia.service';
import { SharedService } from '../../../../../services/shared.service';
import { CargosService } from '../../services/service-checkin.index';
import swal from 'sweetalert2';

@Component({
  selector: 'app-cargos',
  templateUrl: './cargos.component.html',
  styleUrls: ['./cargos.component.css']
})
export class CargosComponent implements OnInit, OnChanges, OnDestroy {
  @Input() readOnly: boolean;
  @Input() folio: any;
  @Input() cuarto: number;
  @Input() consulta: boolean;
  @Input() uso: string;
  @Output() close = new EventEmitter;

  public myFormGroup: FormGroup;
  public dataGrid: Array<any> = [];
  public gridFields = [];
  public ttUsuario: any = [];
  public ttInfocia: any = [];
  public noFolio: number;
  public dataCodigos: Array<{ cdesc: string, Codigo: string }> = [];
  public dataMonedas: Array<{ mdesc: string, Moneda: string }> = [];
  public dataCuentas: Array<{ tcuentadesc: string, tipoCuenta: string }> = [];
  public dataCargos: Array<{ tcdesc: string, tipoCargo: string }> = [];
  public grabarCargos: boolean = true;
  public requireCosto: boolean; // Si requiere costo habilitar campos (monto, moneda)
  public method: string; // M = Modificar, N = Nuevo, B = Borrar
  public lectura: boolean; // Solo lectura del formulario
  public loading: boolean;  // Mostrar spinner de espera
  public showGrid: boolean = true;  // Mostrar la grid de registros

  constructor(
    public _shared: SharedService,
    public _cserv: CargosService,
    public _info: InfociaGlobalService,
    private _pf: FormBuilder,
    private _active: ActivatedRoute,
  ) {
    this._active.data.subscribe(data => {
      this.uso = data.uso;
      this.consulta = data.consulta;
      this.ttInfocia = this._info.getSessionInfocia();
      this.ttUsuario = this._info.getSessionUser();
      if (this.consulta) {
        if (this.uso === 'H') {
          const mantto = this._info.manttoConsultaHuespedes(this.ttUsuario);
          this.grabarCargos = mantto[3];
        } else {
          this.grabarCargos = false;
        }
      }
      this.buildGrid();
    });
  }

  ngOnInit() {
    this.buildFormGroup();
    this.getCargos();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.folio.currentValue) {
      this.cuarto = this.folio.cuarto;
      if (this.folio.rnreser) {
        this.noFolio = this.folio.rnreser;
        if (this.folio.reactual !== 'VI') {
          this.grabarCargos = false;
        }
      } else {
        this.noFolio = this.folio.hfolio;
        if (this.folio.heactual === 'CO') {
          this.grabarCargos = false;
        }
      }
    }
  }
  
  ngOnDestroy(): void {
    if (this.uso === 'R') {
      this._info.notifyOther({ option: 'notify-change-reserv', value: this.noFolio });
    } else {
      this._info.notifyOther({ option: 'notify-change-huesped', value: this.noFolio });
    }
    this.close.emit(true);
  }

  getCargos() {
    if (this.noFolio) {
      this._cserv.getCargos(this.noFolio, this.uso).subscribe(data => {
        if (data) {
          this.dataGrid = data;
        } else {
          this.dataGrid = [];
        }
      });
    }

    this._cserv.getCodigos(this.noFolio).subscribe(data => this.dataCodigos = data);
    this._shared.getAyuda('monedas').subscribe(data => this.dataMonedas = data.ttMonedas);
    this.dataCuentas = this._shared.getCuentas();
    this.dataCargos = this._shared.getTipoCargos();
    delete this.dataCargos[3];
  }

  buildGrid() {
    this.gridFields = [
      { title: 'Código', dataKey: 'codigo', width: '100', filtro: true },
      { title: 'Tipo', dataKey: 'htcargo', width: '100', filtro: true },
      { title: 'Cta.', dataKey: 'htfolio', width: '100', filtro: true },
      { title: 'Monto', dataKey: 'hmonto', width: '120', filtro: true },
      { title: 'Mon.', dataKey: 'hmon', width: '100', filtro: true },
      { title: 'Notas', dataKey: 'hnotas', width: '', filtro: true }
    ];
  }

  nuevo() {
    this.showGrid = false;
    this.method = 'N';
    this.myFormGroup.patchValue({
      hfolio: this.noFolio,
      hfecult: this.ttInfocia.fday,
      hmon: this.ttInfocia.Moneda
    });
  }

  buildFormGroup() {
    this.myFormGroup = this._pf.group({
      hfolio: [0],
      hsecuencia: [1],
      htfolio: [''],
      codigo: ['', Validators.required],
      htcargo: ['', Validators.required],
      hmonto: [0],
      hmon: ['', Validators.required],
      hnotas: [''],
      hfecult: [''],
      cRowID: [''], lError: [false], cErrDes: ['']
    });
  }

  selectRow(dataItem) {
    this.myFormGroup.patchValue(dataItem);
    this.method = 'M';
    this.showGrid = false;
  }

  regresar() {
    this.myFormGroup.reset();
    this.showGrid = true;
    this.loading = false;
  }

  setError(data) {
    this.loading = false; 
    return swal(data[0].cMensTit, data[0].cMensTxt, 'error');
  }

  guardar(value) {
    this.loading = true; 
    value.hsecuencia = 1;
    this._cserv.serviceCargos(value, this.method, this.uso).subscribe(data => {
      if (data) {
        data[0].cMensTxt ? this.setError(data) : this.regresar();
        this.dataGrid = data;
        this.loading = false;
      }
    });
  }

  delete(dataItem) {
    this.method = 'B';
    this.loading = true; 
    this._cserv.serviceCargos(dataItem, this.method, this.uso).subscribe(data => {
      if (data) {
        if (data[0].cMensTxt) { this.setError(data); }
      }
      if (data) {
        this.dataGrid = data;
      } else {
        this.dataGrid = [];
      }
      this.loading = false;
    });
  }
}
