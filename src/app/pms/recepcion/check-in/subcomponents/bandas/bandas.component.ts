import { Component, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { InfociaGlobalService } from '../../../../../services/infocia.service';
import { Banda } from '../../models/bandas.model';
import { BandasService } from '../../services/service-checkin.index';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import moment from 'moment';

@Component({
  selector: 'app-bandas',
  templateUrl: './bandas.component.html',
  styleUrls: ['./bandas.component.css']
})
export class BandasComponent implements OnInit, OnChanges {
  @Input() folio: any;
  @Input() consulta: boolean;
  @Input() uso: string;
  ttUser: any;
  ttInfocia: any;
  grabarBandas = true;
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  // Arreglos para componentes de Kendo
  public dsTipoBanda: Array<{ tdesc: string, tbanda: string }> = [];
  public gridData: Array<any> = [];
  // Variables logicas para ngIf
  public actualizar = false; // Solo lectura del formulario
  public showGrid = true;  // Mostrar la grid de registros
  public cancelRow = false; // Cancelar banda
  // Valores obtenidos del servicio
  public noFolio: number;
  private cuarto: string;
  // Valores para enviar al servicio post y delete
  public banda: Banda;
  public selectedRow: Banda;
  // Variables para recuperar los valores enviados desde el formulario
  public hfecha: string;
  public hfvig: string;
  public hope: string;
  public tbanda: string;
  public hbanda: string;
  public hnotas: string;
  public cancelar: boolean;
  public cnota = '';

  constructor(
    public bandas: BandasService,
    public info: InfociaGlobalService,
    public _active: ActivatedRoute
  ) {
    this._active.data.subscribe(data => this.consulta = data.consulta);
    this.ttUser = this.info.getSessionUser();
  }

  ngOnInit() {
    if (this.consulta) { // Si esta en consulta de huesped validar que pueda grabar mensajes.
      const mantto = this.info.manttoConsultaHuespedes(this.ttUser);
      this.grabarBandas = mantto[6];
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.folio.currentValue) {
      this.noFolio = this.folio.hfolio;
      this.cuarto = this.folio.cuarto;
      this.ttInfocia = this.info.getSessionInfocia();
      this.getDataSource();
      this.bandas.getTipoBandas(this.noFolio).subscribe(data => this.dsTipoBanda = data);
    }
  }
  nuevo() {
    this.cancelRow = false;
    this.showGrid = false;
    this.hfecha = moment(this.ttInfocia.fday).format(this.formatMoment);
    this.hfvig = '';
    this.hope = this.ttUser.cUserId;
    this.tbanda = '';
    this.hbanda = '';
    this.hnotas = '';
  }

  public selectRow({ selectedRows }) {
    this.selectedRow = selectedRows.dataItem[0];
    this.showGrid = false;
    this.actualizar = true;
    this.hfecha = this.selectedRow.hfecha;
    this.hfvig = this.selectedRow.hfvig;
    this.hope = this.selectedRow.hope;
    this.tbanda = this.selectedRow.tbanda;
    this.hbanda = this.selectedRow.hbanda;
    this.hnotas = this.selectedRow.hnotas;
    if (this.selectedRow.hncx) { this.cancelRow = false; } else { this.cancelRow = true; }
  }

  regresar() {
    this.cancelar = false;
    this.showGrid = true;
    this.actualizar = false;
  }
  getDataSource() {
    this.bandas.getBandas(this.noFolio).subscribe(data => {
      if (data) {
        data.forEach(item => {
          item.hfecha = moment(item.hfecha).format(this.formatMoment);
          item.hfvig = moment(item.hfvig).format(this.formatMoment);
        });
        this.gridData = data;
      }
    });
  }
  guardarBanda(form) {
    this.banda = new Banda(this.noFolio, form.tbanda.tbanda, form.hbanda, form.hnotas, form.hfecha, form.hfvig, form.hope, '', '', '?', '');
    this.bandas.postBandas(this.banda).subscribe(data => {
      if (data[0].cMensTxt) {
        swal(data[0].cMensTit, data[0].cMensTxt, 'error');
      } else {
        this.getDataSource();
        this.regresar();
        form.reset();
      }
    });
  }

  async cancelarBanda({ dataItem }) {
    const { value: text } = await swal({
      input: 'textarea', inputPlaceholder: 'Razón de la Cancelación:', showCancelButton: true
    });
    if (text) {
      dataItem.hncx = text;
      this.selectedRow = dataItem;
      this.putBanda();
    }
  }

  putBanda() {
    this.bandas.putBandas(this.selectedRow).subscribe(data => {
      if (!data[0]) {
        this.regresar();
      } else {
        if (data[0].cMensTxt) {
          swal(data[0].cMensTit, data[0].cMensTxt, 'error');
        } else {
          this.getDataSource();
          this.regresar();
        }
      }
    });
  }
}
