import { Component, OnInit, Input, SimpleChanges, OnChanges, OnDestroy } from '@angular/core';
import { MassagesService } from '../../services/service-checkin.index';
import { InfociaGlobalService } from '../../../../../services/infocia.service';
import { Mensaje } from '../../models/mensajes.model';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
@Component({
  selector: 'app-mensajes',
  templateUrl: './mensajes.component.html',
  styleUrls: ['./mensajes.component.css']
})
export class MensajesComponent implements OnInit, OnChanges, OnDestroy {

  @Input() noFolio: any = [];
  @Input() readOnly: boolean;
  @Input() consulta: boolean;
  @Input() uso: string;
  public mensaje: Mensaje;
  public gridData: Array<any> = [];
  public ttUsuario: any = [];
  public ttInfocia: any = [];
  public grabarMsjs: boolean = true;
  public entregar: boolean = true;
  public formatKendo = localStorage.getItem('formatKendo');

  constructor(public aserv: MassagesService, public info: InfociaGlobalService, private _active: ActivatedRoute) {
    this.ttUsuario = this.info.getSessionUser();
    this.ttInfocia = this.info.getSessionInfocia();
    this._active.data.subscribe(data => {
      this.uso = data.uso;
      this.consulta = data.consulta;
      if (this.consulta) { // Si esta en consulta de huesped validar que pueda grabar mensajes.
        if (this.uso === 'H') {
          const mantto = this.info.manttoConsultaHuespedes(this.ttUsuario);
          this.grabarMsjs = mantto[0];
        } else {
          const mantto = this.info.manttoConsultaReserv(this.ttUsuario);
          this.grabarMsjs = mantto[0];
        }
      }
    });
  }
  ngOnInit() {

  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.readOnly && changes.readOnly.currentValue) {
      this.grabarMsjs = false;
      this.consulta = true;
    }
    if (changes.noFolio && changes.noFolio.currentValue) {
      if (this.noFolio.rnreser) {
        this.uso = 'R';
        this.noFolio = this.noFolio.rnreser;
        if (this.noFolio.reactual === 'CX') {
          this.grabarMsjs = false;
        }
        this.aserv.getMassagesReserv(this.noFolio).subscribe(data => {
          if (data) {
            this.gridData = data;
          }
        });
      } else {
        this.uso = 'H';
        if (this.noFolio.heactual === 'CO') {
          this.grabarMsjs = false;
        }
        this.noFolio = this.noFolio.hfolio;
        this.aserv.getMassages(this.noFolio).subscribe(data => {
          if (data) {
            this.gridData = data;
          }
        });
      }
    }
  }
  async nuevoMsj() {
    const { value: text } = await swal({
      input: 'textarea', inputPlaceholder: 'Mensaje para el huesped...', showCancelButton: true
    })
    if (text) {
      this.guardarMsj(text);
    }
  }
  entregarMsj(dataItem) {
    this.aserv.putMessage(dataItem).subscribe(data => {
      if (data) {
        this.masseges();
      }
    });
  }
  masseges() {
    if (this.uso === 'H') {
      this.aserv.getMassages(this.noFolio).subscribe(msjs => this.gridData = msjs);
    } else {
      this.aserv.getMassagesReserv(this.noFolio).subscribe(msjs => this.gridData = msjs);
    }
  }
  ngOnDestroy(): void {
    if (this.uso === 'R') {
      this.info.notifyOther({ option: 'notify-change-reserv', value: this.noFolio });
    } else {
      this.info.notifyOther({ option: 'notify-change-huesped', value: this.noFolio });
    }
  }
  guardarMsj(mensaje) {
    const mope = this.ttUsuario.cUserId;
    const day = this.ttInfocia.fday;
    const hour = '12:00:00';
    const reserv = this.noFolio;
    this.mensaje = new Mensaje('', '', null, '', day, hour, mensaje, reserv, mope);
    this.aserv.postMessage(this.mensaje).subscribe(data => {
      if (data) {
        this.masseges();
      }
    });
  }

}
