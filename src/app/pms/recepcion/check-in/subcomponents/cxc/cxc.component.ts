import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { InfociaGlobalService } from '../../../../../services/infocia.service';
import { CuentaPorCobrar } from '../../models/cxc.model';
import { CxcService } from '../../services/service-checkin.index';
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-cxc',
    templateUrl: './cxc.component.html'
})
export class CxcComponent implements OnChanges {
    @Input() folio: any;
    @Input() consulta: boolean;
    @Input() uso: string;
    // Arreglos para componentes de Kendo
    public dsCodigos: Array<{ cdesc: string, Codigo: string }> = [];
    public dsCuentas: Array<{ tcuenta: string }> = [];
    public dsCxc: Array<{ hdesc: string, hcxc: string }> = [];
    public gridData: Array<any> = [];
    // Variables logicas para ngIf
    public formReq: boolean; // Ocultar y mostrar formulario de req.
    public readOnly: boolean; // Solo lectura del formulario
    public loading: boolean;  // Mostrar spinner de espera
    public showGrid: boolean = true;  // Mostrar la grid de registros
    public updateRow: boolean; // Cancelar banda
    // Valores obtenidos del servicio
    public noFolio: number;
    // Valores para enviar al servicio post y delete
    public cxc: CuentaPorCobrar;
    public selectedRow: CuentaPorCobrar;
    // Variables para recuperar los valores enviados desde el formulario
    public tcuenta: string;
    public Codigo: string;
    public hcxc: string;
    public hdfp: string;

    constructor(public servcxc: CxcService, public info: InfociaGlobalService, private _active: ActivatedRoute) {
        this._active.data.subscribe(data => this.consulta = data.consulta);
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.folio.currentValue) {
            this.folio = changes.folio.currentValue;
            this.noFolio = this.folio.hfolio;
            this.servcxc.getCxc(this.noFolio).subscribe(data => {
                if (data) {
                    this.gridData = data
                }
            });
            this.servcxc.getCodigos(this.noFolio).subscribe(data => this.dsCodigos = data);
            this.dsCuentas = this.servcxc.getCuentas();
            this.dsCxc = this.servcxc.getCuentasPorCobrar();
        }
    }
    nuevo() {
        this.updateRow = false; this.formReq = true; this.showGrid = false; this.readOnly = false;
        this.tcuenta = ''; this.hcxc = ''; this.hdfp = ''; this.Codigo = '';
    }
    selectRow({ selectedRows }) {
        this.selectedRow = selectedRows[0].dataItem;
        this.formReq = true; this.showGrid = false;
        this.readOnly = true; this.updateRow = true;
        this.tcuenta = this.selectedRow.htfolio;
        this.hcxc = this.selectedRow.hcxc;
        this.hdfp = this.selectedRow.hdfp;
        this.Codigo = this.selectedRow.hfp;
    }
    regresar() {
        this.formReq = false;
        this.gridData = [];
        this.servcxc.getCxc(this.noFolio).subscribe(data => this.gridData = data);
        this.showGrid = true;
        this.loading = false;
    }
    guardarCxC(form) {
        let cRowID = ''; let htfolio = ''; let hdfp = '';
        let hfp = ''; let hcxc = ''; let cErrDes = '';
        if (this.updateRow) {
            cRowID = this.selectedRow.cRowID;
            htfolio = this.selectedRow.htfolio;
            cRowID = this.selectedRow.cRowID;
            cErrDes = this.selectedRow.cErrDes;
            hdfp = form.hdfp;
            hcxc = form.hcxc;
            hfp = form.Codigo;
        } else {
            htfolio = form.tcuenta;
            hdfp = form.hdfp;
            hcxc = form.hcxc;
            hfp = form.Codigo;
        }
        this.loading = true; // Lanzar spinner
        this.cxc = new CuentaPorCobrar(this.noFolio, htfolio, hfp, hdfp, hcxc, cRowID, '?', cErrDes);
        this.servcxc.postCxc(this.cxc).subscribe(data => {
            if (data[0].cMensTxt) {
                swal(data[0].cMensTit, data[0].cMensTxt, 'error');
                this.formReq = true;
            } else {
                this.regresar();
                form.reset();
            }
        });
    } // Final de guardar registro
    eliminar(dataItem) {
        this.servcxc.deleteCxc(dataItem).subscribe(data => {
            if (!data[0]) {
                this.regresar();
            } else {
                if (data[0].cMensTxt) {
                    swal(data[0].cMensTit, data[0].cMensTxt, 'error');
                } else {
                    this.regresar();
                }
            }
        });
    }
}
