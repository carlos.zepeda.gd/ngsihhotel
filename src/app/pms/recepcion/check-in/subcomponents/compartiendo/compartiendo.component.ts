import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CompartiendoService } from '../../services/service-checkin.index';

@Component({
  selector: 'app-compartiendo',
  templateUrl: './compartiendo.component.html',
  styles: [],
  providers: [
    CompartiendoService
  ]
})
export class CompartiendoComponent implements OnInit, OnChanges {
  @Input() folio: any;
  @Input() reserva: any;
  noFolio: number;
  public gridData: Array<any> = [];
  constructor(private _comp: CompartiendoService) { }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['folio'] && changes['folio'].currentValue) {
      this.noFolio = this.folio.hfolio;
      if (this.noFolio) {
        this._comp.getCompartiendo(this.noFolio).subscribe(data => {
          if (data) {
            this.gridData = data;
          }
        });
      }
    }
    if (changes['reserva'] && changes['reserva'].currentValue) {
      this.noFolio = this.reserva.rnreser;
      if (this.noFolio) {
        this._comp.getComparteRsrv(this.noFolio).subscribe(data => {
          if (data) {
            this.gridData = data;
          }
        });
      }
    }
  }
}
