import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { CuentasService, CheckinService, BuscarService, RequerimientosService } from '../../services/service-checkin.index';
import { InfociaGlobalService } from '../../../../../services/infocia.service';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Cuenta } from '../../models/cuentas.model';

@Component({
  selector: 'app-cuentas',
  templateUrl: './cuentas.component.html',
  styleUrls: ['./cuentas.component.css']
})
export class CuentasComponent implements OnInit, OnChanges {
  @Input() folio: any;
  @Input() readOnly: boolean;
  @Input() consulta: boolean;
  @Input() uso: string;
  public gridData: Array<any> = [];
  public ttInfocia: Array<any> = [];
  public noFolio: number;
  public noCuenta: string;
  public grabarCuentas = true;
  public subscription: Subscription;
  public selectedRow: any = [];
  public folioTransferir: any;
  public htfolio = '';
  public flagTraspasar: boolean;
  public flagBuscarFolio: boolean = false;
  public cuenta: any;
  public dsCuentas: any = [];
  public htfol: any = [];
  public htfola: number;
  public htfolb: number;
  public htfolc: number;
  public htfold: number;
  public htfole: number;
  public htfolf: number;
  method: string;

  constructor(
    public cserv: CuentasService,
    private info: InfociaGlobalService,
    public _actServ: CheckinService,
    public buscar: BuscarService,
    public rserv: RequerimientosService,
    private _active: ActivatedRoute
  ) {
    this._active.data.subscribe(data => {
      this.uso = data.uso;
      this.consulta = data.consulta;
      if (!this.consulta) {
        this.consulta = true;
      }
      if (this.consulta) { // Si esta en consulta de huesped validar que pueda grabar mensajes.
        if (this.uso === 'H') {
          const mantto = this.info.manttoConsultaHuespedes(this.info.getSessionUser());
          this.grabarCuentas = mantto[4];
        }
        if (this.uso === 'R') {
          const mantto = this.info.manttoConsultaReserv(this.info.getSessionUser());
          this.grabarCuentas = mantto[3];
        }
      }
    });
    this.dsCuentas = this.rserv.getCuentas();
    this.ttInfocia = this.info.getSessionInfocia();
  }

  public ngOnInit(): void {
    this.flagTraspasar = false;
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.readOnly && changes.readOnly.currentValue) {
      this.grabarCuentas = false;
      this.consulta = true;
    }
    if (changes.folio.currentValue) {
      if (this.folio.rnreser) {
        this.uso = 'R';
        this.noFolio = this.folio.rnreser;
        if (this.folio.reactual !== 'VI') {
          this.grabarCuentas = false;
        }
      } else {
        this.uso = 'H';
        this.noFolio = this.folio.hfolio;
        if (this.folio.heactual === 'CO') {
          this.grabarCuentas = false;
        }
      }
      this.getCuentas();
    }
  }
  getCuentas() {
    this.cserv.getCargos(this.noFolio, this.uso).subscribe(data => {
      this.gridData = data.ttHuesfol;
      if (this.uso === 'H') {
        const hues = data.ttHuespedes[0];
        this.htfola = hues.htfola;
        this.htfolb = hues.htfolb;
        this.htfolc = hues.htfolc;
        this.htfold = hues.htfold;
        this.htfole = hues.htfole;
        this.htfolf = hues.htfolf;
      } else {
        const rsrv = data.ttReservaciones[0];
        this.htfola = rsrv.rtfola;
        this.htfolb = rsrv.rtfolb;
        this.htfolc = rsrv.rtfolc;
        this.htfold = rsrv.rtfold;
        this.htfole = rsrv.rtfole;
        this.htfolf = rsrv.rtfolf;
      }
    });
    this.flagTraspasar = false;
  }
  selectRow({ selectedRows }) {
    this.selectedRow = selectedRows[0].dataItem;
    this.htfolio = this.selectedRow.htfolio;
    this.flagTraspasar = false;
  }
  changeCta(cta: string) {
    if (!this.selectedRow.htfolio) { return; }
    this.method = 'M';
    this.selectedRow.htfolio = cta;
    if (this.uso === 'H') {
      this.cserv.putCuentasHuesped(this.selectedRow, this.method).subscribe(data => {
        if (data && data[0].cMensTxt) {
          return swal(data[0].cMensTxt, data[0].cMensTit, 'error');
        }
        this.getCuentas();
      });
    } else {
      this.cserv.putCuentasReserv(this.selectedRow, this.method).subscribe(data => {
        if (data && data[0].cMensTxt) {
          return swal(data[0].cMensTxt, data[0].cMensTit, 'error');
        }
        this.getCuentas();
      });
    }
  }
  transferir() {
    this.flagTraspasar = true;
    this.method = 'T';
  }
  // Cuando se presiona el boton A, B o C se actualiza la cuenta
  changeFolio(cta: string) {
    this.flagBuscarFolio = true;
    this.noCuenta = cta;
  }
  resultFolio(e) {
    let folio: number;
    if (this.uso === 'H') {
      folio = e.hfolio;
    } else {
      folio = e.rnreser;
    }
    switch (this.noCuenta) {
      case 'A': this.htfola = folio; break;
      case 'B': this.htfolb = folio; break;
      case 'C': this.htfolc = folio; break;
      case 'D': this.htfold = folio; break;
      case 'E': this.htfole = folio; break;
      case 'F': this.htfolf = folio; break;
    }
    this.flagBuscarFolio = false;
  }
  guardar() {
    const val = this.gridData[0];
    this.cuenta = new Cuenta('', val.cRowID, val.hclase, val.hdesc, this.noFolio, this.htfola, this.htfolb,
      this.htfolc, this.htfold, this.htfole, this.htfolf, val.htfolio);
    if (this.uso === 'H') {
      this.cserv.putCuentasHuesped(this.cuenta, this.method).subscribe(data => {
        if (data && data[0].cMensTxt) {
          return swal(data[0].cMensTit, data[0].cMensTxt, 'error');
        }
        this.getCuentas();
      });
    } else {
      this.cserv.putCuentasReserv(this.cuenta, this.method).subscribe(data => {
        if (data && data[0].cMensTxt) {
          return swal(data[0].cMensTit, data[0].cMensTxt, 'error');
        }
        this.getCuentas();
      });
    }
  }
}
