import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HistoricoClientesServices } from '../../../../consultas/historico-clientes/historico-clientes.service';
import { InfociaGlobalService } from '../../../../../services/infocia.service';
import { NotasService } from '../../services/service-checkin.index';
import { GLOBAL } from '../../../../../services/global';
import { Notas } from '../../models/notas.model';
const swal = GLOBAL.swal;

@Component({
  selector: 'app-notas',
  templateUrl: './notas.component.html',
  styleUrls: ['./notas.component.css']
})
export class NotasComponent implements OnInit, OnChanges, OnDestroy {

  @Input() readOnly: boolean;
  @Input() noFolio: any;
  @Input() notas: any;
  @Input() tipos: any;
  @Input() cliente: any;
  @Input() consulta: boolean;
  @Input() uso: string;
  @Output() resultNotas = new EventEmitter;
  private ttInfocia: any = [];
  private ttUsuario: any = [];
  public grabarNotas: boolean = true;
  public folio: number;
  public gridData: Array<any> = [];
  public dataTipoNotas = [];
  private selectedRow: any = [];
  public nota: any;
  public formMassages: boolean;
  public cerrar: boolean;
  public guardar: boolean;
  public actualizar: boolean;
  public tnotas: string; // Notas del huesped
  public tinota: any; // Tipo de nota
  public cbCheckIn: boolean; // check
  public cbCheckOut: boolean;  // check
  private letter: string = 'N'; // registrar nuevo
  public title: string;
  private tipoMov: string;
  public method: string;
  public flagInactivas: boolean;
  formatKendo = localStorage.getItem('formatKendo');
  dataComplete: any;

  constructor(
    public nserv: NotasService,
    public info: InfociaGlobalService,
    public _hist: HistoricoClientesServices,
    private _active: ActivatedRoute
  ) {
    this.ttUsuario = this.info.getSessionUser();
    this.ttInfocia = this.info.getSessionInfocia();
    this._active.data.subscribe(data => {
      this.uso = data.uso;
      this.consulta = data.consulta;
      if (this.consulta) { // Si esta en consulta de huesped validar que pueda grabar mensajes.
        if (this.uso === 'H') {
          const mantto = this.info.manttoConsultaHuespedes(this.ttUsuario);
          this.grabarNotas = mantto[1];
        } else {
          const mantto = this.info.manttoConsultaReserv(this.ttUsuario);
          this.grabarNotas = mantto[1];
        }
      }
    });
  }
  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.readOnly && changes.readOnly.currentValue) {
      this.grabarNotas = false;
      this.consulta = true;
    }
    if (changes.noFolio && changes.noFolio.currentValue) {
      if (this.noFolio.rnreser) {
        this.uso = 'R';
        if (this.noFolio.reactual !== 'VI') {
          this.grabarNotas = false;
        }
      } else {
        this.uso = 'H';
        if (this.noFolio.heactual === 'CO') {
          this.grabarNotas = false;
        }
      }
      this.getNotas();
    }
    if (changes.notas && changes.notas.currentValue) {
      this.dataTipoNotas = this.tipos;
      this.gridData = this.notas;
      this.grabarNotas = true;
      this.letter = 'H';
    }
  }
  public selectRow({ selectedRows }) {
    this.selectedRow = selectedRows[0].dataItem;
    this.editMsj();
    // Mostramos en el formulario los valores del registro seleccionao
    this.tnotas = this.selectedRow.tnotas;
    this.tinota = this.selectedRow.ttnota;
    // Codigo para checkbox true and false
    const ci = this.selectedRow.tuso.substring(0, 1);
    const co = this.selectedRow.tuso.substring(1, 2);
    if (!this.selectedRow.tuso) { this.cbCheckIn = false; this.cbCheckIn = false; }
    if (ci === 'S') { this.cbCheckIn = true; } else { this.cbCheckIn = false; }
    if (co === 'S') { this.cbCheckOut = true; } else { this.cbCheckOut = false; }

  }
  nuevoMsj() {
    this.method = 'N';
    this.tnotas = '';
    this.cbCheckIn = false;
    this.cbCheckOut = false;
    this.formMassages = true;
    this.actualizar = false;
    this.cerrar = true;
    this.guardar = true;
    this.title = 'NUEVA NOTA';
  }
  editMsj() {
    this.method = 'M';
    this.formMassages = true;
    this.actualizar = true;
    this.cerrar = true;
    this.guardar = false;
    this.title = 'EDITAR NOTA';
  }
  verNotas() {
    this.gridData = [];
    if (this.flagInactivas) {
      this.dataComplete.forEach(item => {
        if (item.tcancel) {
          this.gridData.push(item);
        }
      });
    } else {
      this.dataComplete.forEach(item => {
        if (!item.tcancel) {
          this.gridData.push(item);
        }
      });
    }
  }
  regresar() {
    this.formMassages = false;
    this.guardar = false;
  }
  getNotas() {
    if (this.uso === 'H') {
      this.tipoMov = 'F';
      this.folio = this.noFolio.hfolio;
    } else {
      this.tipoMov = 'R';
      this.folio = this.noFolio.rnreser;
    }
    this.nserv.getNotes(this.folio, this.uso).subscribe(data => {
      if (data.ttNotas) {
        this.dataComplete = data.ttNotas;
        this.verNotas();
      } else {
        this.flagInactivas = false;
      }
      if (data.ttTnotas) {
        this.dataTipoNotas = data.ttTnotas;
        this.tinota = this.dataTipoNotas[1].tnota;
      }
    });
  }
  cancelar(dataItem) {
    dataItem.tcancel = this.ttInfocia.fday;
    this.nserv.postNotes(dataItem, 'M').subscribe(data => {
      if (data && data[0].cMensTxt) {
        swal(data[0].cMensTit, data[0].cMensTxt, 'warning');
      } else {
        this.getNotas();
        this.flagInactivas = false;
      }
    });
  }
  ngOnDestroy(): void {
    if (this.uso === 'R') {
      this.info.notifyOther({ option: 'notify-change-reserv', value: this.folio });
    } else {
      this.info.notifyOther({ option: 'notify-change-huesped', value: this.folio });
    }
  }
  guardarNota(value: any) {
    let checkin; let checkout;
    const notaEnviada = value.tnotas;
    const ttnota = value.tinota;
    if (value.cbCheckIn) { checkin = 'S'; } else { checkin = 'N'; }
    if (value.cbCheckOut) { checkout = 'S'; } else { checkout = 'N'; }
    const tuso = checkin + checkout;
    const mope = this.ttUsuario.cUserId;
    let reserv = this.folio;
    if (this.cliente) {
      reserv = this.cliente;
      this.tipoMov = 'H';
    }
    this.nota = new Notas(reserv, ttnota, '', '', mope, notaEnviada, '', tuso, this.letter, this.tipoMov, '', '', false);

    this.nserv.postNotes(this.nota, this.method).subscribe(data => {
      if (data && data[0].cMensTxt) {
        swal(data[0].cMensTit, data[0].cMensTxt, 'warning');
      } else {
        if (this.cliente) {
          this._hist.getCliente('', this.cliente).subscribe(nts => {
            if (nts.ttNotas !== undefined) {
              this.gridData = nts.ttNotas;
              this.resultNotas.emit(nts);
            }
          });
        }
        this.getNotas();
        this.formMassages = false;
        this.flagInactivas = false;
      }
    });
  }
}
