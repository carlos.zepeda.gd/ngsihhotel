import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ViajandoService, BuscarService } from '../../services/service-checkin.index';
import { NgForm } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-viajando',
  templateUrl: './viajando.component.html',
  styles: [],
  providers: [ 
    ViajandoService
  ]
})
export class ViajandoComponent implements OnChanges {
  @Input() folio: any;
  private huespedSelect: any;
  public showForm: boolean;
  public flagHelpFolio: boolean;
  public isNew: boolean;
  public noFolio: number;
  public dataGrid: Array<any> = [];
  public showGrid: boolean = true;
  public huesped: {
    hfolio: number; hfoliob: number; hapel: string; hnomb: string;
    heact: string; cRowID: string; lError: boolean; cErrDes: string;
  };

  constructor(private _viajando: ViajandoService, private _buscar: BuscarService) {
    this.reset();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.folio.currentValue) {
      this.noFolio = this.folio.hfolio;
      this.getDataGrid();
    }
  }
  public getDataGrid() {
    this.dataGrid = [];
    this._viajando.getViajandoCon(this.noFolio).subscribe(data => {
      if (data) {
        this.dataGrid = data
      }
    });
  }
  public newRecord() {
    this.reset();
    this.showGrid = false;
    this.showForm = true;
    this.isNew = true;
  }
  public saveRecord(data: NgForm) {
    const val = data.form.controls;
    this.huesped = {
      hfolio: this.noFolio, hfoliob: val.hfoliob.value, hapel: val.hapel.value,
      hnomb: val.hnomb.value, heact: this.huespedSelect.heact, cRowID: '',
      lError: false, cErrDes: ''
    }
    this._viajando.postViajandoCon(this.huesped).subscribe(res => {
      if (res[0].cMensTxt) {
        return swal(res[0].cMensTit, res[0].cMensTxt, 'error');
      }
      this.getDataGrid();
      data.reset();
    });
  }

  public deleteRecord(dataItem) {
    this._viajando.deleteViajando(dataItem).subscribe(res => {
      if (res[0].cMensTxt) {
        return swal(res[0].cMensTit, res[0].cMensTxt, 'error');
      }
      this.getDataGrid();
    });
  }
  public valueChange() {
    if (!this.huesped.hfoliob || this.huesped.hfoliob === this.noFolio) {
      return swal('Folio Invalido!!', '', 'error');
    }
    this._buscar.getHuesped(this.huesped.hfoliob, 'folio').subscribe(data => {
      if (data) {
        this.setValue(data[0]);
      } else {
        swal('Folio Invalido!!', '', 'error');
        this.reset();
      }
    });
  }
  public selectedFolio(folio) {
    this.huespedSelect = folio;
    this.flagHelpFolio = false;
    this.showForm = true;
    this.setValue(folio);
  }
  private setValue(folio) {
    this.huesped.hfolio = this.noFolio;
    this.huesped.hfoliob = folio.hfolio;
    this.huesped.hapel = folio.hapellido;
    this.huesped.hnomb = folio.hnombre;
  }
  public btnHelp() {
    this.flagHelpFolio = true;
    this.showForm = false;
    this.showGrid = false;
  }
  public goBack() {
    this.showGrid = true;
    this.showForm = false;
  }
  private reset() {
    this.huesped = {
      hfolio: 0, hfoliob: 0, hapel: '', hnomb: '',
      heact: '', cRowID: '', lError: false, cErrDes: ''
    }
  }
}
