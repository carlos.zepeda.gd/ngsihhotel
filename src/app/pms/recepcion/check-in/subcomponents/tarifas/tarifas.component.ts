import { Component, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { TarifasService, ValidationsService } from '../../services/service-checkin.index';
import { InfociaGlobalService } from '../../../../../services/infocia.service';
import { GLOBAL } from '../../../../../services/global';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import moment from 'moment';

@Component({
    selector: 'app-tarifas',
    templateUrl: './tarifas.component.html',
    styleUrls: ['./tarifas.component.css'],
    providers: [ValidationsService, NgbModalConfig]
})
export class TarifasComponent implements OnInit, OnChanges {
    @Input() folio: any = [];
    @Input() consulta: boolean;
    @Input() uso: string;
    public formatKendo = localStorage.getItem('formatKendo');
    public formatMoment = localStorage.getItem('formatMoment');
    public dataGrid: Array<any> = [];
    public huesped: any = [];
    public ttInfocia: any = [];
    public ttUsuario: any = [];
    public tarifaSelect: any = [];
    public flagEdit: boolean;
    public getAuth: boolean;
    public messageUserAuth: string;
    public userAuthValid: boolean;
    public tmanuales: string;
    public tsuplemento: string;
    maxAdultos: number;
    diez: boolean;
    nueve: boolean;
    ocho: boolean;
    siete: boolean;
    seis: boolean;
    cinco: boolean;
    cuatro: boolean;
    tres: boolean;
    dos: boolean;
    uno: boolean;
    loading: boolean;
    toast = GLOBAL.toast;
    swal = GLOBAL.swal;
    notaGlobal: string = '';
    ttotal: number;
    totalSuplemento: number;
    dataSuplementos: any = [];
    tarifaSupl: any;

    constructor(public _tarifa: TarifasService,
        public _info: InfociaGlobalService,
        private _config: NgbModalConfig,
        public _valid: ValidationsService,
        private _modalService: NgbModal,
        private _active: ActivatedRoute
    ) {
        this._config.backdrop = 'static';
        this._config.keyboard = false;
        this.ttInfocia = this._info.getSessionInfocia();
        this.ttUsuario = this._info.getSessionUser();
        this._active.data.subscribe(data => this.uso = data.uso);
        this.maxAdultos = this.ttInfocia.cact;
        this.switchTarifas(this.maxAdultos);
        this.manttoTarifas();
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['folio'] && changes['folio']['currentValue']) {
            this.consultaTarifas(this.folio.hfolio)
        }
    }

    changeTarifa({ selectedRows }) {
        if (selectedRows && selectedRows[0]) {
            this.tarifaSelect = selectedRows[0].dataItem;
        }
    }
    consultaTarifas(folio) {
        this.loading = true;
        this._tarifa.getTarifas(folio).subscribe(data => {
            if (data) {
                if (data.cMensTxt) {
                    this.swal(data.cMensTit, data.cMensTxt, 'error');
                } else {
                    this.ttotal = 0;
                    this.totalSuplemento = 0;
                    data.ttHuestar.forEach(item => {
                        this.ttotal += item.httar;
                        this.totalSuplemento += item.htsup;
                    });
                    this.dataGrid = data.ttHuestar;
                    if (this.dataGrid.length) {
                        this.tarifaSelect = data.ttHuestar[0];
                        this.tarifaSupl = {
                            proc: this.tarifaSelect.Proc,
                            Tarifa: this.tarifaSelect.tarifa,
                            ttipo: this.tarifaSelect.htipo,
                            moneda: this.folio.hmon,
                            folio: this.folio.hfolio,
                            sec: 0
                        }
                    }
                    this.huesped = data.ttHuespedes[0];
                    if (data.ttSuplementos) {
                        this.dataSuplementos = data.ttSuplementos;
                    }
                }
            } else {
                this.swal('Sin Registros!!', 'No hay tarifas para mostrar', 'warning');
            }
            this.loading = false;
        });
    }
    async updateRates() {
        if (this.tmanuales === 'S') {
            const temp = [...[this.tarifaSelect]][0];
            this.swal({
                input: 'textarea', inputPlaceholder: 'Razón de la modificación...', showCancelButton: true,
                inputValue: this.notaGlobal, showLoaderOnConfirm: true,
                preConfirm: (nota) => {
                    if (!nota) {
                        this.swal.showValidationMessage(`Ingrese una razón!`)
                    }
                }
            }).then(async (result) => {
                if (result.value) {
                    this.notaGlobal = result.value;
                    temp.hrazon = this.notaGlobal;
                    this.loading = true;
                    this._tarifa.updateRate(temp).subscribe(data => {
                        this.loading = false;
                        if (data[0].cMensTxt) {
                            this.swal('Tarifa no modificada!', data[0].cMensTxt, 'error');
                        } else {
                            this.toast({ type: 'success', title: 'Registros modificados!!' });
                            this.flagEdit = false;
                            this.consultaTarifas(this.folio.hfolio);
                        }
                    });
                }
            });
        } else {
            this.swal('Usuario No Autorizado!!', 'No tiene los permisos para modificar tarifas...', 'warning');
        }
    }

    btnSuplementos(modal) {
        this._modalService.open(modal, { size: 'xl', centered: true });
    }

    manttoTarifas() {
        const cVal = this.ttUsuario.cVal7.split('');
        this.tmanuales = cVal[0];
        this.tsuplemento = cVal[1];
    }

    editarTarifa(dataItem) {
        if (dataItem.hfecha < this.ttInfocia.fday) {
            return this.toast({ type: 'warning', title: 'Fecha Pasada!!' });
        }
        if (!this.consulta) {
            this.tarifaSelect = dataItem;
            if (this.tmanuales === 'S') {
                this.flagEdit = true;
            } else {
                if (this.userAuthValid) {
                    this.flagEdit = true;
                } else {
                    this.messageUserAuth = 'Cambios Manuales a Tarifas'
                    this.getAuth = true;
                    this.userAuthValid = false;
                }
            }
        }
    }

    userValue(user) {
        if (user.permisos[0] === 'S') {
            this.tmanuales = 'S';
            this.userAuthValid = true;
        }
    }

    allFalse() {
        this.uno = true;
        this.dos = true;
        this.tres = false;
        this.cuatro = false;
        this.cinco = false;
        this.seis = false;
        this.siete = false;
        this.ocho = false;
        this.nueve = false;
        this.diez = false;
    }
    switchTarifas(adultos) {
        switch (adultos) {
            case 2:
                this.allFalse();
                break;
            case 3:
                this.allFalse();
                this.tres = true;
                break;
            case 4:
                this.allFalse();
                this.tres = true;
                this.cuatro = true;
                break;
            case 5:
                this.allFalse();
                this.tres = true;
                this.cuatro = true;
                this.cinco = true;
                break;
            case 6:
                this.allFalse();
                this.tres = true;
                this.cuatro = true;
                this.cinco = true;
                this.seis = true;
                break;
            case 7:
                this.allFalse();
                this.tres = true;
                this.cuatro = true;
                this.cinco = true;
                this.seis = true;
                this.siete = true;
                break;
            case 8:
                this.allFalse();
                this.tres = true;
                this.cuatro = true;
                this.cinco = true;
                this.seis = true;
                this.siete = true;
                this.ocho = true;
                break;
            case 9:
                this.allFalse();
                this.tres = true;
                this.cuatro = true;
                this.cinco = true;
                this.seis = true;
                this.siete = true;
                this.ocho = true;
                this.nueve = true;
                break;
            case 10:
                this.uno = true;
                this.dos = true;
                this.tres = true;
                this.cuatro = true;
                this.cinco = true;
                this.seis = true;
                this.siete = true;
                this.ocho = true;
                this.nueve = true;
                this.diez = true;
                break;
        }
    }
}
