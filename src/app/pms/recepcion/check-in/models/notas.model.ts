export class Notas {
    constructor(
        public nnreser: number,
        public ttnota: string,
        public tfecha: string,
        public thora: string,
        public tope: string,
        public tnotas: string,
        public tcancel: string,
        public tuso: string,
        public tproc: string,
        public ttm: string,
        public cErrDes: string,
        public cRowID: string,
        public lError: boolean
    ) {}
}
