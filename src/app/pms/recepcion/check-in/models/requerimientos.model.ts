export class Requerimiento {
    constructor(
        public Satisfaccion: string,
        public cErrDes: string,
        public cRowID: string,
        public cuarto: string,
        public depto: any,
        public hcantidad: number,
        public hdesc: string,
        public hentregado: string,
        public hfecha: string,
        public hfecreg: string,
        public hfolio: number,
        public hmon: string,
        public hmonto: number,
        public hnenter: string,
        public hnespecial: string,
        public hnini: string,
        public hnsatisf: string,
        public hnterm: string,
        public hnumreq: number,
        public hreqesp: string,
        public htcargo: string,
        public htfolio: string,
        public lError: boolean,
        public ubicacion: string
    ) {}
}
