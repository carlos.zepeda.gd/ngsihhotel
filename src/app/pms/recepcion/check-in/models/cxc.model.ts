export class CuentaPorCobrar {
    constructor(
        public hfolio: number,
        public htfolio: string,
        public hfp: string,
        public hdfp: string,
        public hcxc: string,
        public cRowID: string,
        public lError: any,
        public cErrDes: any
    ) {}
}
