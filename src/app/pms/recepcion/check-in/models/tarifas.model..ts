export class Tarifa {
    constructor(
        public hfolio: number,
        public hfecha: string,
        public hta1: number,
        public hta2: number,
        public hta3: number,
        public hta4: number,
        public hta5: number,
        public hta6: number,
        public hta7: number,
        public hta8: number,
        public hta9: number,
        public hta10: number,
        public hta11: number,
        public htj11: number,
        public httar: number,
        public htsup: number,
        public detalle: string,
        public Proc: string,
        public tarifa: string,
        public htipo: string,
        public hrazon: string,
        public cRowID: string,
        public lError = false,
        public cErrDes = ''
    ) {}
}
