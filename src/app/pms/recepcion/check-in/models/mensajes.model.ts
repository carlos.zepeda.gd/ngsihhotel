export class Mensaje{
  constructor(
    public cErrDes: string,
    public cRowID: string,
    public lError: boolean,
    public mentregado: string,
    public mfecha: string,
    public mhora: string,
    public mmensaje: string,
    public mnreser: number,
    public mope: string
  ){}
}
