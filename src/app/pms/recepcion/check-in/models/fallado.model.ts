export class Fallado {
    constructor(
        public hfolio: number,
        public hhotel: string,
        public htarifa: number,
        public hfecreg: string,
        public cRowID: string,
        public lError: any,
        public cErrDes: any
    ) {}
}
