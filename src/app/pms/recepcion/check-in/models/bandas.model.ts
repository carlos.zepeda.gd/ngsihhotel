export class Banda {
    constructor(
        public hfolio: number,
        public tbanda: string,
        public hbanda: string,
        public hnotas: string,
        public hfecha: string,
        public hfvig: string,
        public hope: string,
        public hncx: string,
        public cRowID: string,
        public lError: any,
        public cErrDes: any
    ) {}
}
