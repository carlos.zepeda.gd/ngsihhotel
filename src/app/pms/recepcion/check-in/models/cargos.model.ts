export class Cargo {
    constructor(
        public cErrDes: string,
        public cRowID: string,
        public codigo: string,
        public hfecult: string,
        public hfolio: number,
        public hmon: string,
        public hmonto: number,
        public hnotas: string,
        public htcargo: string,
        public htfolio: string,
        public lError: boolean
    ) {}
}
