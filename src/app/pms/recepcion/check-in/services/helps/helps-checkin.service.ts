import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { GLOBAL } from '../../../../../services/global';
import moment from 'moment';
const ca = GLOBAL.char174;

@Injectable()

export class HelpsCheckInService {
  private url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  public getRfc(tipo, value?: string) {
    return this.http.get(this.url + '/ayuda/rfc' + GLOBAL.char174 + tipo + GLOBAL.char174 + value)
      .map(res => res.json().response.siAyuda.dsAyuda.ttRfccias);
  }

  public getEstados() {
    return this.http.get(this.url + '/ayuda/estados')
      .map(res => res.json().response.siAyuda.dsAyuda.ttEstados);
  }

  public getEmpresas() {
    return this.http.get(this.url + '/ayuda/empresas')
      .map(res => res.json().response.siAyuda.dsAyuda.ttEmpresas);
  }

  public getSearch(serv, value: any) {
    return this.http.get(this.url + '/ayuda/' + serv + ca + value)
      .map(res => res.json().response.siAyuda.dsAyuda);
  }

  public getVip() {
    return this.http.get(this.url + '/ayuda/vip')
      .map(res => res.json().response.siAyuda.dsAyuda.ttVips);
  }

  public getSegmentos() {
    return this.http.get(this.url + '/ayuda/segmentos')
      .map(res => res.json().response.siAyuda.dsAyuda.ttSegmentos);
  }

  public getAgencias() {
    return this.http.get(this.url + '/ayuda/agencias')
      .map(res => res.json().response.siAyuda.dsAyuda.ttAgencias);
  }

  public getTipoCuartos() {
    return this.http.get(this.url + '/ayuda/tcuartos')
      .map(res => res.json().response.siAyuda.dsAyuda.ttTcuartos);
  }

  public getGrupos() {
    return this.http.get(this.url + '/ayuda/grupos')
      .map(res => res.json().response.siAyuda.dsAyuda.ttGrupos);
  }

  public getEstad(value: string) {
    return this.http.get(this.url + '/ayuda/estad' + value)
      .map(res => res.json().response.siAyuda.dsAyuda.ttEstadall);
  }

  public getCaracteristicas() {
    return this.http.get(this.url + '/ayuda/ctocar').map(res => res.json().response.siAyuda.dsAyuda.ttCtocar);
  }

  public getHabDisponibles(date1: string, date2: string, param) {
    const services = `${ca + '' + ca + date1 + ca + date2 + ca + '' + ca + param + ca + ''}`;
    return this.http.get(this.url + '/ayuda/vacantes' + services)
      .map(res => res.json().response.siAyuda.dsAyuda.ttTctos);
  }

  public getHabDisponibles2(date1: string, date2: string, param, discrepancia) {
    const services = `${ca + '' + ca + date1 + ca + date2 + ca + '' + ca + param + ca + discrepancia}`;
    return this.http.get(this.url + '/ayuda/vacantes' + services)
      .map(res => res.json().response.siAyuda.dsAyuda.ttTctos);
  }

  public getAvaiable2(service: any) {
    return this.http.get(this.url + '/ayuda/disp/' + service).map(res => {
      const error = res.json().response.siTmpdisp.dsMensajes.ttMensError;
      return error ? error : res.json().response.siTmpdisp.dsTmpdisp.ttTmpdisp;
    });
  }

  public getRates(huesped: any) {
    let agencia = huesped.Agencia;
    let empresa = huesped.Ecuarto;
    const tipoCto = huesped.tcuarto;
    const date1 = moment(huesped.hfent).format(GLOBAL.formatIso);
    const date2 = moment(huesped.hfsal).format(GLOBAL.formatIso);
    const adultos = huesped.hadultos;
    let juniors = huesped.hjunior;
    let menores = huesped.hmenpag;
    const tarifa = '';
    if (!agencia) { agencia = ''; }
    if (!empresa) { empresa = ''; }
    if (!juniors) { juniors = 0; }
    if (!menores) { menores = 0; }
    const services = `${agencia + ca + empresa + ca + tipoCto + ca + date1 + ca +
      date2 + ca + adultos + ca + juniors + ca + menores + ca + tarifa
      }`;
    return this.http.get(this.url + '/ayuda/tarifas/' + services)
      .map(res => res.json().response.siTmptar.dsTmptar);
  }

  public getDatailRates(huesped: any, tarifa: any) {
    let agencia = huesped.Agencia;
    let empresa = huesped.Ecuarto;
    const tipoCto = huesped.tcuarto;
    const date1 = moment(huesped.hfent).format(GLOBAL.formatIso);
    const date2 = moment(huesped.hfsal).format(GLOBAL.formatIso);
    const adultos = huesped.hadultos;
    let juniors = huesped.hjunior;
    let menores = huesped.hmenpag;
    tarifa = tarifa.ttarifa;
    if (!agencia) { agencia = ''; }
    if (!empresa) { empresa = ''; }
    if (!juniors) { juniors = 0; }
    if (!menores) { menores = 0; }
    const services = `${agencia + ca + empresa + ca + tipoCto + ca + date1 + ca +
      date2 + ca + adultos + ca + juniors + ca + menores + ca + tarifa
      }`;
    return this.http.get(this.url + '/ayuda/tarifas/' + services)
      .map(res => res.json().response.siTmptar.dsTmptar.ttTmpdia);
  }
  public buscarTarifa(tarifa) {
    const service = ca + tarifa.tProc + ca + tarifa.ttarifa + ca + tarifa.ttt;
    return this.http.get(this.url + '/ayuda/BuscaTarifa' + service)
      .map(res => res.json().response.siAyuda.dsAyuda.ttTarifas);
  }
  public getRatesReserva(data: any) {
    const agencia = data.reserva.Agencia;
    const empresa = data.reserva.Empresa;
    const tarifa = '';
    const tipoCto = data.secuencia.tcuarto;
    const entrada = moment(data.secuencia.rfent).format(GLOBAL.formatIso);
    const salida = moment(data.secuencia.rfsal).format(GLOBAL.formatIso);
    const adultos = data.secuencia.radultos;
    let juniors = data.secuencia.rjunior;
    let menores = data.secuencia.rmenpag;
    if (!juniors) { juniors = 0; } if (!menores) { menores = 0; }
    const services = `${agencia + ca + empresa + ca + tipoCto + ca + entrada + ca +
      salida + ca + adultos + ca + juniors + ca + menores + ca + tarifa
      }`;
    return this.http.get(this.url + '/ayuda/tarifas/' + services)
      .map(res => res.json().response.siTmptar.dsTmptar);
  }

  public getDatailRatesReserva(reserva: any, tarifa: any) {
    let agencia = reserva.Agencia;
    let empresa = reserva.Ecuarto;
    const tipoCto = reserva.tcuarto;
    const date1 = moment(reserva.rfent).format(GLOBAL.formatIso);
    const date2 = moment(reserva.rfent).format(GLOBAL.formatIso);
    const adultos = reserva.radultos;
    let juniors = reserva.rjunior;
    let menores = reserva.rmenpag;
    tarifa = tarifa.ttarifa;
    if (!agencia) { agencia = ''; }
    if (!empresa) { empresa = ''; }
    if (!juniors) { juniors = 0; }
    if (!menores) { menores = 0; }
    const services = `${agencia + ca + empresa + ca + tipoCto + ca + date1 + ca +
      date2 + ca + adultos + ca + juniors + ca + menores + ca + tarifa
      }`;
    return this.http.get(this.url + '/ayuda/tarifas/' + services)
      .map(res => res.json().response.siTmptar.dsTmptar.ttTmpdia);
  }
}
