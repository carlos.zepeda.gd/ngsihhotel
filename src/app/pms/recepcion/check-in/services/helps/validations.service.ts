import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { GLOBAL } from '../../../../../services/global';
const esp = GLOBAL.char174;
@Injectable()

export class ValidationsService {
  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  public getAuthorization(user, pass, cval, ini, fin, huesped?) {
    if (huesped === undefined) {
      huesped = 'huesped'
    }
    // w = web, tipo, cval,  
    // valido usuario
    // valida password
    // validaciones cval
    const link = `${ esp + user + esp + pass + esp + 'W' + esp + cval + esp + ini + esp + fin }`
    return this.http.get( this.url + '/ayuda/usuario/' + huesped + link )
                .map( (res: Response) => res.json().response );
  }
  public validarTdc(folio: string, tipo: string, origen ?) {
    return this.http.get(this.url + '/ayuda/verFPD/' + tipo + esp + folio + esp + origen)
                    .map((res: Response) => res.json().response.siVerfpd.dsVerfpd.ttVerfpd[0].FPd);
  }
  public validateUserAuth(auth) {
        if (auth[0] === 'N') {
            return  'Usuario no existe!!!';
        }else if (auth[1] === 'N') {
            return  'Contraseña no valida!!!';
        }
  
  }
  public codigoRestriccion(codigo: any) {
    return this.http.get( this.url + '/ayuda/codrest/' + codigo.codigo + esp + codigo.pass )
                    .map( (res: Response) => res.json().response );
  }
  public validarEstado(estado: string) {
    return this.http.get( this.url + '/ayuda/estados/' + estado)
                    .map( (res: Response) => res.json().response );
  }
  public validarEmpresa(empresa: string) {
    return this.http.get( this.url + '/ayuda/empresas/' + empresa)
                    .map( (res: Response) => res.json().response );
  }
  public validarVip(vip: string) {
    return this.http.get( this.url + '/ayuda/vip/' + vip)
                    .map( (res: Response) => res.json().response  );
  }
  public validarAgencia(agencia: string) {
    return this.http.get( this.url + '/ayuda/agencias/' + agencia )
                    .map( (res: Response) => res.json().response );
  }
  public validarSubAgencia(agencia: string) {
    return this.http.get( this.url + '/ayuda/agencias/' + agencia )
                    .map( (res: Response) => res.json().response );
  }
  public validarSegmento(segmento: string) {
    return this.http.get( this.url + '/ayuda/segmentos/' + segmento )
                    .map( (res: Response) => res.json().response );
  }
  public validarGrupo(grupo: string) {
    return this.http.get( this.url + '/ayuda/grupos/' + grupo )
                    .map( (res: Response) => res.json().response );
  }
  public validarTipoHuesped(treser: string) {
    return this.http.get( this.url + '/ayuda/treser/' + treser )
                    .map( (res: Response) => res.json().response );
  }
  public validarOrigen(origen: string) {
    return this.http.get( this.url + '/ayuda/origen/' + origen )
                    .map( (res: Response) => res.json().response );
  }
  public validarVendedor(vendedor: string) {
    return this.http.get( this.url + '/ayuda/vendedores/' + vendedor )
                    .map( (res: Response) => res.json().response );
  }
  public validarFormaPago(codigo: string) {
    return this.http.get( this.url + '/ayuda/codigos/' + codigo )
                    .map( (res: Response) => res.json().response);
  }
  public validarEstad1(estad: string) {
    return this.http.get( this.url + '/ayuda/estad/' + estad )
                    .map( (res: Response) => res.json().response );
  }
  public validarCaract(ctocar: string) {
    return this.http.get( this.url + '/ayuda/ctocar/' + ctocar )
                    .map( (res: Response) => res.json().response );
  }
  public validarCuarto(cuarto: string) {
    return this.http.get( this.url + '/ayuda/vacantes/' + cuarto )
                    .map( (res: Response) => res.json().response );
  }
}
