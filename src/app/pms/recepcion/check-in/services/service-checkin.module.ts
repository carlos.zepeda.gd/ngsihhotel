import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    CheckinService,
    BuscarService,
    AdicionalesService,
    BandasService,
    CargosService,
    CuentasService,
    CxcService,
    FalladoService,
    MassagesService,
    NotasService,
    RequerimientosService,
    TarifasService,
    ViajandoService,
    CompartiendoService,
    HelpsCheckInService,
    ValidationsService
} from './service-checkin.index';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    CheckinService,
    BuscarService,
    AdicionalesService,
    BandasService,
    CargosService,
    CuentasService,
    CxcService,
    FalladoService,
    MassagesService,
    NotasService,
    RequerimientosService,
    TarifasService,
    ViajandoService,
    CompartiendoService,
    HelpsCheckInService,
    ValidationsService
  ],
  declarations: []
})
export class ServiceCheckInModule { }
