import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { GLOBAL } from '../../../../services/global';
import { Subject } from 'rxjs/Subject';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { filterBy } from '@progress/kendo-data-query';
const headers = new Headers({ 'Content-Type': 'application/json' });
import moment from 'moment';
const esp = GLOBAL.char174;
@Injectable()

export class CheckinService {
  public url: string;
  private notify = new Subject<any>();
  notifyObservable$ = this.notify.asObservable();
  api = ['response', 'siAyuda', 'dsAyuda'];

  constructor(private http: Http, private info: InfociaGlobalService) {
    this.url = GLOBAL.url;
  }
  public notifyOther(data: any) {
    if (data) { this.notify.next(data); }
  }
  postCheckIn(huesped: any, typeCi: string) {
    let register = 'N';
    if (typeCi === 'cambio') { register = 'M'; typeCi = 'M'; }
    huesped.hfent = moment(huesped.hfent).format(GLOBAL.formatIso);
    huesped.hfsal = moment(huesped.hfsal).format(GLOBAL.formatIso);
    if (huesped.hfecconf) {
      huesped.hfecconf = moment(huesped.hfecconf).format(GLOBAL.formatIso);
    }
    const json = JSON.stringify(huesped);
    const params = '{"dsHuesp":{"ttHuespedes":[' + json + ']}}';
    const service = `${register + esp + typeCi + esp + '' + esp + 'Prueba'}`

    return this.http.put(this.url + '/rec/huesped/' + service, params, { headers: headers })
      .map(res => {
        const success = res.json().response.siHuesp.dsHuesp.ttHuespedes;
        const error = res.json().response.siHuesp.dsMensajes.ttMensError;
        if (error) { return error; } else { return success; }
      });
  }
  public getEstados() {
    return this.http.get(this.url + '/ayuda/estados').map(res => this.filter(res, 'ttEstados'));
  }
  public getTarifas() {
    return this.http.get(this.url + '/ayuda/tarifas').map(res => this.filter(res, 'ttTarifas'));
  }
  public getTipoCuartos() {
    return this.http.get(this.url + '/ayuda/tcuartos').map(res => this.filter(res, 'ttTcuartos'));
  }
  public getIdiomas() {
    return this.http.get(this.url + '/ayuda/idiomas').map(res => this.filter(res, 'ttIdiomas'));
  }
  public getVip() {
    return this.http.get(this.url + '/ayuda/vip').map(res => this.filter(res, 'ttVips'));
  }
  public getSegmentos() {
    return this.http.get(this.url + '/ayuda/segmentos').map(res => this.filter(res, 'ttSegmentos'));
  }
  public getTipoHuesped() {
    return this.http.get(this.url + '/ayuda/treser' + GLOBAL.char174 + '2')
      .map(res => this.filter(res, 'ttTreser'));
  }
  public getFormaPago() {
    return this.http.get(this.url + '/ayuda/codigos' + esp + 'c' + esp + '2' + esp + 'y').map(res => this.filter(res, 'ttCodigos'));
  }
  public getOrigen() {
    return this.http.get(this.url + '/ayuda/origen').map(res => this.filter(res, 'ttOrigenes'));
  }
  public getVendedores() {
    return this.http.get(this.url + '/ayuda/vendedores').map(res => this.filter(res, 'ttVendedores'));
  }
  public getFieldsRequiered() {
    return this.http.get(this.url + '/ayuda/parametros' + esp + 'huespedes-direccion')
      .map(res => res.json()[this.api[0]][this.api[1]][this.api[2]]['ttParametros']);
  }
  public actionsNotification(folio: number) {
    return this.http.get(this.url + '/rec/huesped/' + folio + '/E')
      .map(res => res.json().response.sihuesped.dsHuespedes.ttHuespedes);
  }

  filter(res, tt) {
    const result = res.json()[this.api[0]][this.api[1]][this.api[2]][tt];
    if (result) {
      return filterBy(result, {
        logic: 'or', filters: [
          { field: 'Inactivo', operator: 'eq', value: false },
          { field: 'inactivo', operator: 'eq', value: false },
        ]
      });
    } else {
      return [];
    }
  }
}
