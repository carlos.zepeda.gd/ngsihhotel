import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { GLOBAL } from '../../../../../services/global';
import 'rxjs/add/operator/map';
import { CuentaPorCobrar } from '../../models/cxc.model';

@Injectable()
export class CxcService {

  private url: string;
  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  getCxc(folio: number) {
      return this.http.get( this.url + '/rec/huesped/' + folio + '/X')
                      .map( res => res.json().response.sihuesped.dsHuespedes.ttHuescont );
  }
  getCodigos(folio: number) {
    return this.http.get( this.url + '/rec/huesped/' + folio + '/C')
                    .map( res => res.json().response.sihuesped.dsHuespedes.ttCodigos );
  }
  getCuentas() {
    const data = [
        { tcuenta: 'A' },
        { tcuenta: 'B' },
        { tcuenta: 'C' },
        { tcuenta: 'D' },
        { tcuenta: 'E' },
        { tcuenta: 'F' }
    ];
    return data;
  }
  getCuentasPorCobrar() {
    const data = [
        { hcxc: 'A', hdesc: 'Agencia' },
        { hcxc: 'A', hdesc: 'Empresa' }
    ];
    return data;
  }
  postCxc(cxc: CuentaPorCobrar) {
    const json = JSON.stringify(cxc);
    const params = '{"dsHuescont":{"ttHuescont":[' + json + ']}}';
    const headers = new Headers({'Content-Type': 'application/json'});

    return this.http.put( this.url + '/rec/huescont/N', params, { headers: headers })
                    .map( (res: Response) => {
                       const succes = res.json().response.siHuescont.dsHuescont.ttHuescont;
                       const error = res.json().response.siHuescont.dsMensajes.ttMensError;
                       if (error) {
                          return error;
                       }else {
                          return succes;
                       }
                    });
  }
  deleteCxc(cxc: CuentaPorCobrar) {
    const json = JSON.stringify(cxc);
    const params = '{"dsHuescont":{"ttHuescont":[' + json + ']}}';
    const headers = new Headers({'Content-Type': 'application/json'});

    return this.http.put( this.url + '/rec/huescont/B', params, { headers: headers })
                    .map( (res: Response) => {
                       let succes = res.json().response.siHuescont.dsHuescont.ttHuescont;
                       const error = res.json().response.siHuescont.dsMensajes.ttMensError;
                       if (!succes) { succes = []; }
                       if (error) {
                          return error;
                       }else {
                          return succes;
                       }
                    });
  }
}

