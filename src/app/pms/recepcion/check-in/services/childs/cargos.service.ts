import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { GLOBAL } from '../../../../../services/global';
import { Cargo } from '../../models/cargos.model';
import 'rxjs/add/operator/map';
const headers = new Headers({ 'Content-Type': 'application/json' });
const esp = GLOBAL.char174;
const url = GLOBAL.url;
@Injectable()
export class CargosService {

  private notify = new Subject<any>();
  notifyObservable$ = this.notify.asObservable();

  constructor(private http: Http) { }

  public notifyOther(data: any) {
    if (data) { this.notify.next(data); }
  }
  getCargos(folio: number, uso: string) {
    if (uso === 'H') {
      return this.http.get(url + '/rec/huesped/' + folio + '/C')
        .map(res => res.json().response.sihuesped.dsHuespedes.ttHuescar);
    } else {
      return this.http.get(url + '/rsrv/reserv/' + folio + '/C')
        .map(res => res.json().response.siReservacion.dsReservaciones.ttHuescar);
    }
  }
  serviceCargos(cargo: Cargo, method: string, uso: string) {
    let api;
    const params = '{"ttHuescar":[' + JSON.stringify(cargo) + ']}';
    if (uso === 'H') {
      api = '/rec/huescar/';
    } else {
      api = '/rsrv/rescar/';
    }
    return this.http.put(url + api + method, params, { headers: headers })
      .map((res: Response) => {
        const success = res.json().response.siHuescar.dsHuescar.ttHuescar;
        const error = res.json().response.siHuescar.dsMensajes.ttMensError;
        return error ? error : success;
      });
  }
  getCodigos(folio?: number) {
    return this.http.get(url + '/ayuda/codigos' + esp + 'd' + esp + '2' + esp + 'y')
      .map(res => res.json().response.siAyuda.dsAyuda.ttCodigos);
  }
  getTipoCargos() {
    const dataTiposReq = [
      { tipoCargo: 'D', tcdesc: 'DIARIO' },
      { tipoCargo: 'E', tcdesc: 'ENTRADA' },
      { tipoCargo: 'S', tcdesc: 'SALIDA' },
    ];
    return dataTiposReq;
  }
}
