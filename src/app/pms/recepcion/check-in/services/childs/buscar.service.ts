import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from '../../../../../services/global';

@Injectable()
export class BuscarService {

    public url: string;
    private urlAyuda: string;
    private notify = new Subject<any>();
    notifyObservable$ = this.notify.asObservable();

    constructor(private http: Http) {
        this.url = GLOBAL.url;
    }

    public notifyOther(data: any) {
        if (data) {
            this.notify.next(data);
        }
    }

    getHuespedFolio(folio: string) {
        this.urlAyuda = `F${GLOBAL.char169 + folio}`;
        return this.http.get(this.url + '/ayuda/huespedes' + GLOBAL.char169 + this.urlAyuda)
            .map((res: Response) => res.json().response.siAyuda.dsAyuda.ttHuespedes)
            .catch(e => {
                if (e.status === 401) {
                    alert('No Autorizado!!... Sesión finalizada!!');
                    location.href = GLOBAL.iplogin;
                    return Observable.throw('Unauthorized');
                }
            });
    }

    getHuesped(value: any, field: string, value2?: string) {
        switch (field) {
            case 'nombre':
                this.urlAyuda = `N${GLOBAL.char169 + value + GLOBAL.char169 + value2 + '*'}`;
                break;
            case 'agencia':
                this.urlAyuda = `A${GLOBAL.char169 + value}`;
                break;
            case 'empresa':
                this.urlAyuda = `E${GLOBAL.char169 + value}`;
                break;
            case 'folio':
                this.urlAyuda = `F${GLOBAL.char169 + value}`;
                break;
            case 'conf':
                this.urlAyuda = `O${GLOBAL.char169 + value}`;
                break;
            case 'cuarto':
                this.urlAyuda = `C${GLOBAL.char169 + value}`;
                break;
            case 'grupo':
                this.urlAyuda = `G${GLOBAL.char169 + value}`;
                break;
            case 'contrato':
                this.urlAyuda = `O${GLOBAL.char169 + value}`;
                break;
            case 'email':
                this.urlAyuda = `M${GLOBAL.char169 + value}`;
                break;
            case 'cliente':
                this.urlAyuda = `L${GLOBAL.char169 + value}`;
                break;
            default:
                this.urlAyuda = `N${GLOBAL.char169 + '*' + GLOBAL.char169}`;
                break;
        }
        return this.http.get(this.url + '/ayuda/huespedes' + GLOBAL.char169 + this.urlAyuda)
            .map((res: Response) => {
                if (Object.keys(res.json().response.siAyuda.dsMensajes).length) {
                    return res.json().response.siAyuda.dsMensajes.ttMensError;
                } else {
                    return res.json().response.siAyuda.dsAyuda.ttHuespedes
                }
            })
            .catch(e => {
                if (e.status === 401) {
                    alert('No Autorizado!!... Sesión finalizada!!');
                    location.href = GLOBAL.iplogin;
                    return Observable.throw('Unauthorized');
                }
            });
    }

    getReservacion(value: string, field: string, value2?: string) {
        switch (field) {
            case 'nombre':
                this.urlAyuda = `N${GLOBAL.char174 + value + GLOBAL.char174 + value2 + GLOBAL.char174}`;
                break;
            case 'agencia':
                this.urlAyuda = `A${GLOBAL.char174 + value + GLOBAL.char174}`;
                break;
            case 'empresa':
                this.urlAyuda = `E${GLOBAL.char174 + value + GLOBAL.char174}`;
                break;
            case 'folio':
                this.urlAyuda = `F${GLOBAL.char174 + value + GLOBAL.char174}`;
                break;
            case 'conf':
                this.urlAyuda = `O${GLOBAL.char174 + value + GLOBAL.char174}`;
                break;
            case 'cuarto':
                this.urlAyuda = `C${GLOBAL.char174 + value + GLOBAL.char174}`;
                break;
            case 'grupo':
                this.urlAyuda = `G${GLOBAL.char174 + value + GLOBAL.char174}`;
                break;
            case 'contrato':
                this.urlAyuda = `O${GLOBAL.char169 + value}`;
                break;
            case 'email':
                this.urlAyuda = `M${GLOBAL.char169 + value}`;
                break;
            case 'cliente':
                this.urlAyuda = `L${GLOBAL.char169 + value}`;
                break;
        }
        return this.http.get(this.url + '/ayuda/reservaciones' + GLOBAL.char174 + this.urlAyuda)
            .map(res => {
                if (Object.keys(res.json().response.siAyuda.dsMensajes).length) {
                    return res.json().response.siAyuda.dsMensajes.ttMensError;
                } else {
                    return res.json().response.siAyuda.dsAyuda.ttReservaciones
                }
            })
            .catch(e => {
                if (e.status === 401) {
                    alert('No Autorizado!!... Sesión finalizada!!');
                    location.href = GLOBAL.iplogin;
                    return Observable.throw('Unauthorized');
                }
            });
    }
}
