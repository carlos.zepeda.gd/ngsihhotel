import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { GLOBAL } from '../../../../../services/global';
import 'rxjs/add/operator/map';
const url = GLOBAL.url;
const headers = new Headers({ 'Content-Type': 'application/json' });

@Injectable()
export class CuentasService {

    constructor(private http: Http) { }

    getCargos(folio: number, uso: string) {
        if (uso === 'H') {
            return this.http.get(url + '/rec/huesped/' + folio + '/f')
                .map((res: Response) => res.json().response.sihuesped.dsHuespedes);
        } else {
            return this.http.get(url + '/rsrv/reserv/' + folio + '/f')
                .map((res: Response) => res.json().response.siReservacion.dsReservaciones);
        }

    }
    getHuesped(folio: number, uso: string) {
        if (uso === 'H') {
            return this.http.get(url + '/rec/huesped/' + folio + '/f')
                .map((res: Response) => res.json().response.sihuesped.dsHuespedes.ttHuespedes);
        } else {
            return this.http.get(url + '/rsrv/reserv/' + folio + '/f')
                .map((res: Response) => res.json().response.sihuesped.dsHuespedes.ttHuespedes);
        }

    }
    putCuentasReserv(data: any, method: string) {
        const params = '{"ttHuesfol":[' + JSON.stringify(data) + ']}';

        return this.http.put(url + '/rsrv/resfol/' + method, params, { headers: headers }).map(res => {
            const success = res.json().response.siResfol.dsResfol.ttHuesfol;
            const error = res.json().response.siResfol.dsMensajes.ttMensError;
            return error ? error : success;
        });
    }
    putCuentasHuesped(data: any, method: string) {
        const params = '{"ttHuesfol":[' + JSON.stringify(data) + ']}';

        return this.http.put(url + '/rec/huesfol/' + method, params, { headers: headers }).map(res => {
            const success = res.json().response.siHuesfol.dsHuesfol.ttHuesfol;
            const error = res.json().response.siHuesfol.dsMensajes.ttMensError;
            return error ? error : success;
        });
    }
}
