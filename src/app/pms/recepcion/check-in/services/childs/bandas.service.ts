import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Banda } from '../../models/bandas.model';
import { GLOBAL } from '../../../../../services/global';
import 'rxjs/add/operator/map';
const headers = new Headers({'Content-Type': 'application/json'});

@Injectable()
export class BandasService {
  private url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  getBandas(folio: number) {
      return this.http.get( this.url + '/rec/huesped/' + folio + '/B')
                      .map( res => res.json().response.sihuesped.dsHuespedes.ttHuesban );
  }
  getTipoBandas(folio: number) {
    return this.http.get( this.url + '/rec/huesped/' + folio + '/B')
                    .map( res => res.json().response.sihuesped.dsHuespedes.ttTbandas );
  }
  postBandas(banda: Banda) {
    const json = JSON.stringify(banda);
    const params = '{"dsHuesban":{"ttHuesban":[' + json + ']}}';

    return this.http.put(this.url + '/rec/huesban/N', params, { headers: headers })
                    .map((res: Response) => {
                       const succes = res.json().response.siHuesban.dsHuesban.ttHuesban;
                       const error = res.json().response.siHuesban.dsMensajes.ttMensError;
                       if (error) {
                          return error;
                       }else {
                          return succes;
                       }
                    });
  }
  putBandas(banda: Banda) {
    const json = JSON.stringify(banda);
    const params = '{"dsHuesban":{"ttHuesban":[' + json + ']}}';

    return this.http.put(this.url + '/rec/huesban/X', params, { headers: headers })
                    .map((res: Response) => {
                        const succes = res.json().response.siHuesban.dsHuesban.ttHuesban;
                        const error = res.json().response.siHuesban.dsMensajes.ttMensError;
                        if (error) {
                            return error;
                        }else {
                            return succes;
                        }
                    });
  }
}
