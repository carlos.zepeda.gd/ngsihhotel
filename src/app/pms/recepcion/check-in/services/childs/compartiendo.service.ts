import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { GLOBAL } from '../../../../../services/global';

@Injectable()
export class CompartiendoService {

  constructor(private http: Http) {
  }

  getCompartiendo(folio: number) {
    return this.http.get(GLOBAL.url + '/rec/huesped/' + folio + '/O')
      .map(res => res.json().response.sihuesped.dsHuespedes.ttHuescomp);
  }
  getComparteRsrv(folio: number) {
    return this.http.get(GLOBAL.url + '/rsrv/reserv/' + folio + '/O')
      .map(res => res.json().response.siReservacion.dsReservaciones.ttHuescomp);
  }
  
}
