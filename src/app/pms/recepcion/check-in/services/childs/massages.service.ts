import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { GLOBAL } from '../../../../../services/global';
import { Mensaje } from '../../models/mensajes.model';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MassagesService {
  public url: string;
  private notify = new Subject<any>();
  notifyObservable$ = this.notify.asObservable();

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }
  public notifyOther(data: any) {
    if (data) { this.notify.next(data); }
  }
  getMassages(folio: number) {
      return this.http.get( this.url + '/rec/huesped/' + folio + '/M')
                      .map( res => res.json().response.sihuesped.dsHuespedes.ttMensajes );
  }
  getMassagesReserv(folio: number) {
    return this.http.get( this.url + '/rsrv/reserv/' + folio + '/M')
                    .map( res => res.json().response.siReservacion.dsReservaciones.ttMensajes );
  }
  postMessage(msj: Mensaje) {
    const json = JSON.stringify(msj);
    const params = '{"dsMessages":{"ttMensajes":[' + json + ']}}';
    const headers = new Headers({'Content-Type': 'application/json'});

    return this.http.put( this.url + '/rec/mensajes/N', params, { headers: headers })
                    .map( (res: Response) => res.json().response.siMessage.dsMessages.ttMensajes );
  }
  putMessage(msj: Mensaje) {
    const json = JSON.stringify(msj);
    const params = '{"dsMessages":{"ttMensajes":[' + json + ']}}';
    const headers = new Headers({'Content-Type': 'application/json'});

    return this.http.put( this.url + '/rec/mensajes/E', params, { headers: headers })
                    .map( (res: Response) => res.json().response.siMessage.dsMessages.ttMensajes );
  }
}
