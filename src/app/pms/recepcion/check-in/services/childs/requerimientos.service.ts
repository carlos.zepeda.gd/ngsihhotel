import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { GLOBAL } from '../../../../../services/global';
import { Subject } from 'rxjs/Subject';
import { Requerimiento } from '../../models/requerimientos.model';
import 'rxjs/add/operator/map';
const headers = new Headers({ 'Content-Type': 'application/json' });
const url = GLOBAL.url;

@Injectable()
export class RequerimientosService {

  private notify = new Subject<any>();
  public notifyObservable$ = this.notify.asObservable();

  constructor(private http: Http) {
  }
  public notifyOther(data: any) {
    if (data) { this.notify.next(data); }
  }
  getReq(folio: number, uso: string) {
    if (uso === 'R') {
      return this.http.get(url + '/rsrv/reserv/' + folio + '/Q').map(res => {
        const r = res.json().response.siReservacion;
        if (Object.keys(r.dsMensajes).length) {
          return r.dsMensajes.ttMensError[0];
        } else {
          return r.dsReservaciones;
        }
      });
    } else {
      return this.http.get(url + '/rec/huesped/' + folio + '/Q').map(res => {
        const r = res.json().response.sihuesped;
        if (Object.keys(r.dsMensajes).length) {
          return r.dsMensajes.ttMensError[0];
        } else {
          return r.dsHuespedes;
        }
      });
    }
  }
  getCuartos() {
    return this.http.get(url + '/manttos/ctofso/o')
      .map(res => res.json().response.siCtofso.dsCtofso.ttCuartos);
  }
  getRequerimientos() {
    return this.http.get(url + '/ayuda/reqesp' + GLOBAL.char174 + 3)
      .map(res => res.json().response.siAyuda.dsAyuda.ttReqesp);
  }
  getSelectReq(folio: number, activeUrl: string) {
    if (activeUrl === 'reservacion') {
      return this.http.get(url + '/rsrv/reserv/' + folio + '/Q')
        .map(res => res.json().response.siReservacion.dsReservaciones.ttReqesp);
    } else {
      return this.http.get(url + '/rec/huesped/' + folio + '/Q')
        .map(res => res.json().response.sihuesped.dsHuespedes.ttReqesp);
    }
  }
  getCargos() {
    const dataTiposReq = [
      { tipoCargo: 'D', tcdesc: 'DIARIO' },
      { tipoCargo: 'E', tcdesc: 'ENTRADA' },
      { tipoCargo: 'S', tcdesc: 'SALIDA' },
      { tipoCargo: 'N', tcdesc: 'NINGUNO' }
    ];
    return dataTiposReq;
  }
  getCuentas() {
    const dataCuentas = [
      { tipoCuenta: 'A', tcuentadesc: 'A', field: 'htfola' },
      { tipoCuenta: 'B', tcuentadesc: 'B', field: 'htfolb' },
      { tipoCuenta: 'C', tcuentadesc: 'C', field: 'htfolc' },
      { tipoCuenta: 'D', tcuentadesc: 'D', field: 'htfold' },
      { tipoCuenta: 'E', tcuentadesc: 'E', field: 'htfole' },
      { tipoCuenta: 'F', tcuentadesc: 'F', field: 'htfolf' }
    ];
    return dataCuentas;
  }
  serviceRequerimiento(req: Requerimiento, method: string, uso: string) {
    let api;
    const params = '{"ttHuesreq":[' + JSON.stringify(req) + ']}';
    if (uso === 'R') {
      api = '/rsrv/resreq/';
    } else {
      api = '/rec/huesreq/';
    }
    return this.http.put(url + api + method, params, { headers: headers })
      .map((res: Response) => {
        const success = res.json().response.siHuesreq.dsHuesreq.ttHuesreq;
        const error = res.json().response.siHuesreq.dsMensajes.ttMensError;
        return error ? error : success;
      });
  }
  entradaRequerimiento(req: Requerimiento) {
    const json = JSON.stringify(req);
    const params = '{"dsHuesreq":{"ttHuesreq":[' + json + ']}}';

    return this.http.put(url + '/rec/huesreq/1', params, { headers: headers })
      .map((res: Response) => {
        const success = res.json().response.siHuesreq.dsHuesreq.ttHuesreq;
        const error = res.json().response.siHuesreq.dsMensajes.ttMensError;
        if (error) {
          return error;
        } else {
          return success;
        }
      });
  }
  deleteReq(req: Requerimiento) {
    const json = JSON.stringify(req);
    const params = '{"ttHuesreq":[' + json + ']}';

    return this.http.put(url + '/rec/huesreq/B', params, { headers: headers })
      .map((res: Response) => {
        let data = res.json().response.siHuesreq.dsHuesreq.ttHuesreq;
        if (!data) { data = []; }
        return data;
      });
  }

  getDisponibilidad(req, inicio, fin) {
    const _ = GLOBAL.char174;
    return this.http.get(url + '/consultas/disponibilidad/E' + _ + req + _ + inicio + _ + fin)
    .map(res => res.json().response.siDispon.dsDispon.pReqesp);
  }
}
