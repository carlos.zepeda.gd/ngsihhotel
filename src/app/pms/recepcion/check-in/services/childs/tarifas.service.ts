import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { GLOBAL } from '../../../../../services/global';
import { Tarifa } from '../../models/tarifas.model.';
const esp = GLOBAL.char174;
const headers = new Headers({ 'Content-Type': 'application/json' });
import moment from 'moment';
const ca = GLOBAL.char174;

@Injectable()
export class TarifasService {

    private url: string;

    constructor(private http: Http) {
        this.url = GLOBAL.url;
    }
    public getRates(huesped: any) {
        let agencia = huesped.Agencia;
        let empresa = huesped.Ecuarto;
        const tipoCto = huesped.tcuarto;
        const date1 = moment(huesped.hfent).format(GLOBAL.formatIso);
        const date2 = moment(huesped.hfsal).format(GLOBAL.formatIso);
        const adultos = huesped.hadultos;
        let juniors = huesped.hjunior;
        let menores = huesped.hmenpag;
        const tarifa = '';
        if (!agencia) { agencia = ''; }
        if (!empresa) { empresa = ''; }
        if (!juniors) { juniors = 0; }
        if (!menores) { menores = 0; }
        const services = `${agencia + ca + empresa + ca + tipoCto + ca + date1 + ca +
          date2 + ca + adultos + ca + juniors + ca + menores + ca + tarifa
          }`;
        return this.http.get(this.url + '/ayuda/tarifas/' + services)
          .map(res => res.json().response.siTmptar.dsTmptar.ttTmptar);
      }
    getTarifas(folio: number) {
        return this.http.get(this.url + '/rec/huesped/' + folio + '/T')
            .map(res => {
                const r = res.json().response.sihuesped;
                if (r.dsMensajes.ttMensError !== undefined) {
                    return r.dsMensajes.ttMensError[0];
                } else {
                    return r.dsHuespedes;
                }
            });
    }
    getTarifasRsrv(folio) {
        return this.http.get(this.url + '/rsrv/reserv/' + folio.rnreser + '/T' + esp + folio.rsecuencia)
            .map(res => res.json().response.siReservacion.dsReservaciones);
    }
    getHuesped(folio: number) {
        return this.http.get(this.url + '/rec/huesped/' + folio + '/T')
            .map(res => res.json().response.sihuesped.dsHuespedes.ttHuespedes);
    }
    getSuplementos(folio: number) {
        return this.http.get(this.url + '/rec/huesped/' + folio + '/T')
            .map(res => res.json().response.sihuesped.dsHuespedes.ttSuplementos);
    }
    updateTarifaRsrv(rate: Tarifa, reserva) {
        const params = '{"ttHuestar":[' + JSON.stringify(rate) + '], "ttReservaciones": [' + JSON.stringify(reserva) + ']}';
        return this.http.put(this.url + '/rsrv/restar/M', params, { headers: headers })
            .map((res: Response) => {
                const success = res.json().response.siHuestar.dsHuestar;
                const error = res.json().response.siHuestar.dsMensajes.ttMensError;
                return error ? error : success;
            });
    }

    updateRate(rate: Tarifa) {
        const json = JSON.stringify(rate);
        const params = '{"dsHuestar":{"ttHuestar":[' + json + ']}}';

        return this.http.put(this.url + '/rec/huestar/M', params, { headers: headers })
            .map((res: Response) => {
                let success = res.json().response.siHuestar.dsHuestar.ttHuestar;
                const error = res.json().response.siHuestar.dsMensajes.ttMensError;
                if (!success) { success = []; }
                if (error) {
                    return error;
                } else {
                    return success;
                }
            });
    }
}

