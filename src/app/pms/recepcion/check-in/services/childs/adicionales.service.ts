import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { GLOBAL } from '../../../../../services/global';
import 'rxjs/add/operator/map';
const url = GLOBAL.url;
const headers = new Headers({ 'Content-Type': 'application/json' });

@Injectable()
export class AdicionalesService {

  constructor(private http: Http) {}

  getAdicionales(folio: number, uso: string) {
    if (uso === 'H') {
      return this.http.get(url + '/rec/huesped/' + folio + '/A').map(res => res.json().response.sihuesped.dsHuespedes);
    } else {
      return this.http.get(url + '/rsrv/reserv/' + folio + '/A').map(res => res.json().response.siReservacion.dsReservaciones);
    }
  }
  getCuartos() {
    return this.http.get(url + '/manttos/ctofso/o').map(res => res.json().response.siCtofso.dsCtofso.ttCuartos);
  }
  getTipoHuesped() {
    const dataTipoHuesped = [
      { tipo: 'A', tdesc: 'Adulto' },
      { tipo: 'J', tdesc: 'Junior' },
      { tipo: 'M', tdesc: 'Menor' },
      { tipo: 'I', tdesc: 'Infante' }
    ];
    return dataTipoHuesped;
  }
  putAdicional(data: any, action: string, uso: string) {
    let api;
    if (uso === 'H') {
      api = '/rec/huesadic/';
    } else {
      api = '/rsrv/resadic/';
    }
    const params = '{"ttHuesadic":[' + JSON.stringify(data) + ']}';
    return this.http.put(url + api + action, params, { headers: headers }).map(res => {
      const success = res.json().response.siHuesadic.dsHuesadic.ttHuesadic;
      const error = res.json().response.siHuesadic.dsMensajes.ttMensError;
      return error ? error : success;
    });
  }
}
