import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from '../../../../../services/global';
import 'rxjs/add/operator/map';
const headers = new Headers({'Content-Type': 'application/json'});

@Injectable()
export class ViajandoService {
  private url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  getViajandoCon(folio: number) {
      return this.http.get( this.url + '/rec/huesped/' + folio + '/V')
                      .map( res => res.json().response.sihuesped.dsHuespedes.ttHuesvc );
  }
  postViajandoCon(data: any) {
    const json = JSON.stringify(data);
    const params = '{"dsHuesvc":{"ttHuesvc":[' + json + ']}}';

    return this.http.put(this.url + '/rec/huesvc/N', params, { headers: headers })
                    .map((res: Response) => {
                        const error = res.json().response.siHuesvc.dsMensajes.ttMensError;
                        if (error) { 
                            return error; 
                        }else {
                            return res.json().response.siHuesvc.dsHuesvc.ttHuesvc;
                        }
                    });
  }
  deleteViajando(data: any) {
    const json = JSON.stringify(data);
    const params = '{"dsHuesvc":{"ttHuesvc":[' + json + ']}}';

    return this.http.put(this.url + '/rec/huesvc/B', params, { headers: headers })
                    .map((res: Response) => {
                        const error = res.json().response.siHuesvc.dsMensajes.ttMensError;
                        if (error) { 
                            return error; 
                        }else {
                            return res.json().response.siHuesvc.dsHuesvc.ttHuesvc;
                        }
                    });
  }
}
