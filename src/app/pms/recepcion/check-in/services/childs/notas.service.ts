import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { GLOBAL } from '../../../../../services/global';
import { Subject } from 'rxjs/Subject';
import { Notas } from '../../models/notas.model';
import 'rxjs/add/operator/map';
const headers = new Headers({ 'Content-Type': 'application/json' });
const url = GLOBAL.url;
@Injectable()
export class NotasService {

  private notify = new Subject<any>();
  notifyObservable$ = this.notify.asObservable();

  constructor(private http: Http) {
  }
  public notifyOther(data: any) {
    if (data) { this.notify.next(data); }
  }
  getNotes(folio: number, uso: string) {
    if (uso === 'H') {
      return this.http.get(url + '/rec/huesped/' + folio + '/N')
        .map(res => res.json().response.sihuesped.dsHuespedes);
    } else {
      return this.http.get(url + '/rsrv/reserv/' + folio + '/N')
        .map(res => res.json().response.siReservacion.dsReservaciones);
    }
  }
  postNotes(msj: Notas, method: string) {
    const params = '{"ttNotas":[' + JSON.stringify(msj) + ']}';
    return this.http.put(url + '/rec/notas/' + method, params, { headers: headers })
      .map(res => {
        const success = res.json().response.siNota.dsNotas.ttNotas;
        const error = res.json().response.siNota.dsMensajes.ttMensError;
        return error ? error : success;
      });
  }
}
