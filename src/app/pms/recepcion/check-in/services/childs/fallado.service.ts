import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { GLOBAL } from '../../../../../services/global';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

const headers = new Headers({'Content-Type': 'application/json'});

@Injectable()
export class FalladoService {
    public url: string;
    constructor(private http: Http) {
        this.url = GLOBAL.url;
    }
    getCargos(folio: number) {
        return this.http.get( this.url + '/rec/huesped/' + folio + '/f')
                        .map( (res: Response) => res.json().response.sihuesped.dsHuespedes.ttHuesfol );
    }
    postFallado(fallado) {
        const json = JSON.stringify(fallado);
        const params = '{"ttHuesfal":[' + json + ']}';

        return this.http.put( this.url + '/rec/huesfal', params, { headers: headers })
                        .map( (res: Response) => {
                            const success = res.json().response.siHuesfal.dsHuesfal.ttHuesfal;
                            const error = res.json().response.siHuesfal.dsMensajes.ttMensError;
                            if (error) {
                              return error;
                            }else {
                              return success;
                            }
                        });
    }
}
