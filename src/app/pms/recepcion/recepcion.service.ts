import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { GLOBAL } from '../../services/global';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';

const headers = new Headers({ 'Content-Type': 'application/json' });
const esp = GLOBAL.char174;
const url = GLOBAL.url;

interface ResInterface {
    'response': any;
}

@Injectable()

export class RecepcionService {

    public url: string;

    constructor(private http: Http,
        private httpC: HttpClient) {
        this.url = GLOBAL.url;
    }

    postCheckIn(huesped: any, typeCi: string) {
        let register = 'N';
        if (typeCi === 'cambio') {
            register = 'M';
            typeCi = 'M';
        }
        const json = JSON.stringify(huesped);
        const params = '{"dsHuesp":{"ttHuespedes":[' + json + ']}}';
        const service = `${register + esp + typeCi + esp + '' + esp + 'Prueba'}`

        return this.http.put(url + '/rec/huesped/' + service, params, { headers: headers })
            .map(res => {
                const success = res.json().response.siHuesp.dsHuesp.ttHuespedes;
                const error = res.json().response.siHuesp.dsMensajes.ttMensError;
                if (error) {
                    return error;
                } else {
                    return success;
                }
            });
    }

    public getEstatusHuesped(val) {
        return this.http.get(url + '/ayuda/status' + esp + val)
            .map(res => res.json().response.siAyuda.dsAyuda.ttHuespedes);
    }

    public getParametros(parametro) {
        return this.http.get(url + '/ayuda/parametros' + esp + parametro)
            .map(res => res.json().response.siAyuda.dsAyuda.ttParametros);
    }

    public llegadasHuespedes(data) {
        const params = '{"dsReporte":{"ttTgeneral":[' + JSON.stringify(data) + ']}}';

        return this.http.put(url + '/reportes/general/llegadas', params, { headers: headers })
            .map(res => {
                const r = res.json().response.siReporte;
                if (r.dsMensajes.ttMensError !== undefined) {
                    return r.dsMensajes.ttMensError[0];
                } else {
                    return r.dsReporte;
                }
            });

    }

    public statusEspFallar(folio, sec, status) {
        const value: string = status[0] + '' + status[1];
        return this.httpC.get<ResInterface>(url + '/reportes/llegadas/odatos®' + folio + '®' + sec + '®' + value);
    }

    getData(url: string) {
        return this.httpC.get<any>(this.url + url)
            .map(res => res.response);
    }

    putInfo(data, urlSec: string, ds: string, tt: string, { headers: HttpHeaders }) {
        const json = JSON.stringify(data);
        const params = '{"' + ds + '":{"' + tt + '":[' + json + ']}}';

        return this.httpC.put<any>(this.url + urlSec, params, GLOBAL.httpOptions)
            .map(res => res.response);
    }

    postInfo(data, urlSec: string, ds: string, tt: string) {
        const json = JSON.stringify(data);
        const params = '{"' + ds + '":{"' + tt + '":[' + json + ']}}';

        return this.httpC.post<any>(this.url + urlSec, params, GLOBAL.httpOptions)
            .map(res => res.response);
    }


}
