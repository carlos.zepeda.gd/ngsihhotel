import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { BuscarService } from '../check-in/services/childs/buscar.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { CrudService } from '../../mantto-srv.services';
import { RecepcionService } from '../recepcion.service';
import swal from 'sweetalert2';

@Component({
    selector: 'app-margen',
    templateUrl: './margen.component.html',
})
export class MargenComponent implements OnInit {
    heading = 'Cuentas al Margen ';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';
    public dataFolios: any = [];
    public folio: any = [];
    public ttInfocia: any = [];
    public dataGrid: any = [];
    public status = 'CM';
    public url = 'margen';

    constructor(private _buscar: BuscarService,
        private _info: InfociaGlobalService,
        private _rec: RecepcionService,
        private _crud: CrudService,
        private _menuServ: ManttoMenuService,
        private _router: Router,
        private modalService: NgbModal) {
        this.ttInfocia = this._info.getSessionInfocia();
        this.consultar();
    }

    consultar() {
        this._rec.getEstatusHuesped(this.status).subscribe(data => this.dataGrid = data);
    }

    ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
    }

    valueChange(value) {
        if (value.heactual === 'CO') {
            swal('Huesped Check-Out!!!', '', 'error');
            return;
        }
        if (value.length < 4) {
            swal('Cuarto Invalido!!!', '', 'error');
            return;
        }
        this._buscar.getHuesped(value, 'cuarto').subscribe(cuarto => {
            if (!cuarto) {
                this._buscar.getHuesped(value, 'folio').subscribe(folio => {
                    if (!folio) {
                        swal('Cuarto Invalido!!!', '', 'error');
                    } else {
                        this.resultData(folio);
                    }
                });
            } else {
                this.resultData(cuarto);
            }
        });
    }

    resultData(data) {
        if (data.length === 1) {
            this.resultHuesped(data[0]);
        } else {
            this.dataFolios = data;
            // Hay mas de un huesped con este cuarto, mostrar grid.
        }
    }

    resultHuesped(e) {
        if (e.heactual === 'CO') {
            swal('Huesped Check-Out!!!', '', 'error');
        } else {
            this.folio = e;
        }
    }

    guardarForm() {
        this.folio.heactual = this.status;
        this._crud.putInfo(this.folio, '/rec/huesped/' + this.url, 'dsHuesp', 'ttHuespedes').subscribe(data => {
            const result = data.siHuesp.dsHuesp.ttHuespedes;
            if (result) {
                if (result.cErrDes) {
                    swal(result.cErrDes, '', 'error');
                } else {
                    this.folio = [];
                    this.consultar();
                }
            }
        });
    }
    openModal(content) {
        this.modalService.open(content, { size: 'xl' });
    }
}
