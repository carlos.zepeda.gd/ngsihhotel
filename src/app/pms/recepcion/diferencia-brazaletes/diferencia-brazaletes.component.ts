import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RecepcionService } from '../recepcion.service';
import swal from 'sweetalert2';
import { ManttoMenuService } from 'app/pms/manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../services/global';
import { InfociaGlobalService } from 'app/services/infocia.service';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import moment from 'moment';
import { HttpClient } from '@angular/common/http';
import { TblGeneral } from 'app/models/tgeneral';


@Component({
  selector: 'app-diferencia-brazaletes',
  templateUrl: './diferencia-brazaletes.component.html',
})
export class DiferenciaBrazaletesComponent implements OnInit {


  heading = 'Diferencia de Brazaletes';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  ngForm: FormGroup;
  modalTitle = '';
  modalType = '';
  noRecords = GLOBAL.noRecords;
  closeResult: string;
  dataGrid: any = [];
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  ttUsuario: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  filter: CompositeFilterDescriptor;
  fecha: any;
  api: Array<string> = ['/reportes/caja/DifBrazaletes', 'dsReprecepcion', 'ttTgeneral'];
  Usuario = [];
  loading = false;
  base64 = '';
  base = '';
  toast = GLOBAL.toast;
  tblGeneral: TblGeneral;
  Fecha: any;


  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private formBuild: FormBuilder,
    private _https: RecepcionService,
    private http: HttpClient,
    ) {
      this.ttInfocia = _info.getSessionInfocia();
      this.ttUsuario = this._info.getSessionUser();
      this.crudTable();
      this.ngForm = this.createForm();
      this.buscarInfo();
    }

  ngOnInit() {
    this.ttInfocia = this._info.getSessionInfocia();
    this.Fecha = moment(this.ttInfocia.fday).toDate();
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  crudTable(){
    this.tblGeneral = new TblGeneral();
  }

  createForm() {
    return this.formBuild.group(this.tblGeneral);
  }

  public buscarInfo(){
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    this._https.putInfo(temp, this.api[0], this.api[1], this.api[2], GLOBAL.httpOptions).subscribe(data => {
      const result = data.siReprecepcion.dsReprecepcion;
      const resultExcel = data.siReprecepcion.dsReprecepcion.RespuestaExcel;
      if (!result.Huespedes)  {
        this.dataGrid = [];
        this.base64 = null;
        this.base = null;
      }else{

        this.dataGrid = result.Huespedes;
        this.base64 = resultExcel[0].Archivo;
        this.base = resultExcel;

      }
        this.loading = false;
      });
  }

  public exportExcel(){
    this.loading = true;
    if (this.base) {
      const fileName = this.base64;
      this.http.get<any>(GLOBAL.url + '/reportes/excel/' + fileName)
      .subscribe(res => {
        const result = res.response.siEnvioExcel.dsEnvioExcel.RespuestaExcel[0];
        const fileExcel = result.ArchivoExportado;
        const linkSource = 'data:application/vnd.ms-excel;base64,' + fileExcel;
        const download = document.createElement('a');
        const fileName = result.Archivo;
        download.href = linkSource;
        download.download = fileName;
        download.click();

      });
    }
        this.loading = false;
  }

  exportPdf() {
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(this.formatMoment);
    form.fFec2 = moment(form.fFec2).format(this.formatMoment);
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: '002-DIFERENCIA-BRAZ',
      dataGrid: this.dataGrid,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      hora: moment().format('HH:mm:ss'),
      form: form,
    }
    this.http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = this.heading + '.pdf';
        download.click();
      },
      error => {
        swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
      }
    );
  }


}
