import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { CompositeFilterDescriptor, distinct, filterBy, process } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { RecepcionService } from '../recepcion.service';
import { TblGeneral } from '../../../models/tgeneral';
import { GLOBAL } from '../../../services/global';
import swal from 'sweetalert2';
import moment from 'moment';

@Component({
  selector: 'app-cuartos-disp-virtuales',
  templateUrl: './cuartos-disp-virtuales.component.html',
})
export class CuartosDispVirtualesComponent implements OnInit {
  heading = 'Cuartos Disponibles Virtuales';
  subheading: string;
  icon: string;
  ngForm: FormGroup;
  noRecords = GLOBAL.noRecords;
  ttInfocia: any = [];
  ttUsuario: any = [];
  dataGrid: any = [];
  dataGlobal: any = [];
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  filter: CompositeFilterDescriptor;
  api: Array<string> = ['/reportes/recepcion/CtosDispVirt', 'dsReprecepcion', 'ttTgeneral'];
  loading = false;
  ctos: any = [];
  tblGeneral: TblGeneral;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private formBuild: FormBuilder,
    private _https: RecepcionService,
    private http: HttpClient,
  ) {
    this.ttInfocia = _info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.crudTable();
    this.ngForm = this.createForm();
    this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    this.ttInfocia = this._info.getSessionInfocia();
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  crudTable() {
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.iInt1 = 1;
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).add(1, 'days').toDate();
  }

  createForm() {
    return this.formBuild.group(this.tblGeneral);
  }


  public getInfo() {
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
    temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);
    this._https.putInfo(temp, this.api[0], this.api[1], this.api[2], GLOBAL.httpOptions).subscribe(data => {
      const result = data.siReprecepcion.dsReprecepcion.TotalCuartos;
      if (!result) {
        this.dataGrid = [];
      } else {
        this.dataGrid = result;
        this.ctos = this.dataGrid[0].ttTcb;
      }
      this.loading = false;
    });
  }


  filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataFilter = filterBy(this.dataGrid, filter);
  }

  distinctPrimitive(fieldName: string) {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  public allData(): ExcelExportData {
    if (this.filter) {
      const result: ExcelExportData = {
        data: process(this.dataFilter, { sort: [{ field: 'NoCuarto', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    } else {
      const result: ExcelExportData = {
        data: process(this.dataGrid, { sort: [{ field: 'NoCuarto', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    }

  }

  exportPdf() {
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(this.formatMoment);
    form.fFec2 = moment(form.fFec2).format(this.formatMoment);
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: '002-DISP-VIRTUALES',
      dataGrid: this.dataGrid,
      global: this.dataGlobal,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      dia: this.ctos,
      hora: moment().format('HH:mm:ss'),
      form: form,
    }
    this.http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = this.heading + '.pdf';
        download.click();
      },
      error => {
        swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
      }
    );
  }
}
