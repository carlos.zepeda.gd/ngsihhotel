import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { CompositeFilterDescriptor, distinct, filterBy, process } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { RecepcionService } from '../recepcion.service';
import { TblGeneral } from '../../../models/tgeneral';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-revision-cambios-huespedes',
  templateUrl: './revision-cambios-huespedes.component.html',
})
export class RevisionCambiosHuespedesComponent implements OnInit {
  heading = 'Revisión Cambios a Huéspedes';
  subheading: string;
  icon: string;
  ngForm: FormGroup;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  filter: CompositeFilterDescriptor;
  api: Array<string> = ['/consultas/varias/Cambios®CR®CambiosHuespedes', 'dsCambios', 'ttTgeneral'];
  loading = false;
  tblGeneral: TblGeneral;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: RecepcionService,
    ) {
      this.ttInfocia = this._info.getSessionInfocia();
      this.crudTable();
      this.ngForm = this.createForm();
      this.allData = this.allData.bind(this);
    }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  crudTable(){
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.iInt1 = 2;
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).subtract(1, 'days').toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).toDate();
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }

  public buscarInfo(){
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
    temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);
    this._https.putInfo(temp, this.api[0], this.api[1], this.api[2], GLOBAL.httpOptions).subscribe(data => {
      const result = data.siCambio.dsCambios;
      if (!result.tablaMovimientos)  {
        this.dataGrid = [];
      }else{
        this.dataGrid = result.tablaMovimientos;
      }
        this.loading = false;
      });
  }

  filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataFilter = filterBy(this.dataGrid, filter);
  }

  distinctPrimitive(fieldName: string) {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  public allData(): ExcelExportData {
    if (this.filter){
      const result: ExcelExportData =  {
        data: process(this.dataFilter, {sort: [{field: 'NoFolio', dir: 'asc'}], filter: this.filter}).data
      };
        return result;
    }else{
      const result: ExcelExportData =  {
        data: process(this.dataGrid, {sort: [{field: 'NoFolio', dir: 'asc'}], filter: this.filter}).data
      };
        return result;
    }
  }
}
