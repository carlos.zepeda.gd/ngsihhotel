import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { CompositeFilterDescriptor, distinct, process } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { TblGeneral } from '../../../../models/tgeneral';
import { ReportesService } from '../../reportes.service'
import { GLOBAL } from '../../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-tiempos-movimientos-operador',
  templateUrl: './tiempos-movimientos-operador.component.html',
})
export class TiemposMovimientosOperadorComponent implements OnInit {

  heading = 'Tiempos y Movimientos de Operadores';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  ngForm: FormGroup;
  modalTitle = '';
  modalType = '';
  noRecords = GLOBAL.noRecords;
  loadingColor: any = GLOBAL.loadingColor;
  closeResult: string;
  dataGrid: any = [];
  dataGlobal: any = [];
  dataFilter: any[];
  dataFilter2: any[];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  ttUsuario: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  fechaSys: Date;
  filter: CompositeFilterDescriptor;
  filter2: CompositeFilterDescriptor;
  fecha: any;
  api: Array<string> = ['/reportes/caja/Tiempos', 'dsReprecepcion', 'ttTgeneral'];
  Usuario = [];
  loading = false;
  toast = GLOBAL.toast;
  swal = GLOBAL.swal;
  tblGeneral: TblGeneral;
  sec = '';

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: ReportesService,
    private _http: HttpClient,
  ) {}
  
  ngOnInit() {
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
    this.crudTable();
    this.ngForm = this.createForm();
    this.fechaSys = moment(this.ttInfocia.fday).toDate();
    this.allData = this.allData.bind(this);
    this.allData2 = this.allData2.bind(this);
  }

  crudTable() {
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.iInt1 = 1;
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).subtract(1, 'days').toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).toDate();
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }

  public buscarInfo() {
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
    temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);
    this._https.putInfo(temp, this.api[0], this.api[1], this.api[2], GLOBAL.httpOptions).subscribe(data => {
      const result = data.siReprecepcion.dsReprecepcion;
      if (!result.tTres) {
        this.dataGrid = [];
        this.dataGlobal = [];
      } else {

        this.dataGrid = result.tTres;
        this.dataGlobal = result.Cuartos;

      }
      this.loading = false;
    });
  }

  distinctPrimitive(fieldName: string) {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  distinctPrimitive2(fieldName: string) {
    return distinct(this.dataGlobal, fieldName).map(item => item[fieldName]);
  }

  public allData(): ExcelExportData {
    if (this.filter) {
      const result: ExcelExportData = {
        data: process(this.dataFilter, { sort: [{ field: 'Operador', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    } else {
      const result: ExcelExportData = {
        data: process(this.dataGrid, { sort: [{ field: 'Operador', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    }

  }

  public allData2(): ExcelExportData {
    if (this.filter2) {
      const result: ExcelExportData = {
        data: process(this.dataFilter2, { sort: [{ field: 'Operador', dir: 'asc' }], filter: this.filter2 }).data
      };
      return result;
    } else {
      const result: ExcelExportData = {
        data: process(this.dataGlobal, { sort: [{ field: 'Operador', dir: 'asc' }], filter: this.filter2 }).data
      };
      return result;
    }
  }

  exportPdf() {
    // tslint:disable-next-line:triple-equals
    if (this.ngForm.value.iInt1 == 1) {
      this.sec = '005-MOV-OPE-Det';
    } else {
      this.sec = '005-MOV-OPE-Glob';
    }
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(this.formatMoment);
    form.fFec2 = moment(form.fFec2).format(this.formatMoment);
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: this.sec,
      dataGrid: this.dataGrid,
      global: this.dataGlobal,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      hora: moment().format('HH:mm:ss'),
      form: form,
    }
    this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = this.heading + '.pdf';
        download.click();
      },
      error => {
        this.swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
      }
    );
  }

}
