import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { ReportesService } from '../../reportes.service'
import { TblGeneral } from '../../../../models/tgeneral';
import { GLOBAL } from '../../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-requerimientos-especiales',
  templateUrl: './requerimientos-especiales.component.html',
})
export class RequerimientosEspecialesComponent implements OnInit {
  heading = 'Reporte Estadístico de Requerimientos';
  subheading: string;
  icon: string;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];
  dataGlobal: any = [];
  pageSize = GLOBAL.pageSize;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  filter: CompositeFilterDescriptor;
  api: Array<string> = ['/reportes/caja/EstReq', 'dsReprecepcion', 'ttTgeneral'];
  loading = false;
  base64 = '';
  base = '';
  toast = GLOBAL.toast;
  listDeptos: [];
  listUbicacion: [];
  listTrabajo: [];
  listOperador: [];
  listAgencia: [];
  listData: any = [];
  listFecha: any = [];
  loadingColor: any = GLOBAL.loadingColor;
  formSend: TblGeneral;
  ttUsuario: any;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: ReportesService,
    private _http: HttpClient,
  ) {
    this.ttInfocia = _info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.formSend = new TblGeneral();
    this.formSend.fFec1 = moment(this.ttInfocia.fday).subtract(1, 'days').toDate();
    this.formSend.fFec2 = moment(this.ttInfocia.fday).toDate();
    this.formSend.iInt1 = '1';
    this.formSend.iInt2 = '3';
    this.formSend.iInt3 = '4';
    this.getDataSource();
  }

  ngOnInit() {
    this.ttInfocia = this._info.getSessionInfocia();
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }


  createForm() {
    return this._formBuild.group(this.formSend);
  }

  public buscarInfo() {
    // if (!this.formSend.cChr1){
    //   this.toast({
    //     type: 'info',
    //     title: '',

    //   });
    // }
    this.loading = true;
    let fecDesde = null;
    let fecHasta = null;
    const temp = JSON.parse(JSON.stringify(this.formSend));
    temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
    temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);
    // tslint:disable-next-line:triple-equals
    if (this.formSend.iInt1 != 1) {
      temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);
      temp.fFec1 = null;
      // tslint:disable-next-line:triple-equals
    } else if (this.formSend.iInt1 == 1) {
      fecDesde = moment(temp.fFec1).dayOfYear();
      fecHasta = moment(temp.fFec2).dayOfYear();
      const restaFechas = fecHasta - fecDesde;
      if (restaFechas > 30) {
        temp.fFec2 = moment(temp.fFec1).add(30, 'days').format(GLOBAL.formatIso);
        temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
      } else {
        temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);
        temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
      }
    }
    this._https.putInfo(temp, this.api[0], this.api[1], this.api[2], GLOBAL.httpOptions).subscribe(data => {
      const result = data.siReprecepcion.dsReprecepcion;
      const resultExcel = data.siReprecepcion.dsReprecepcion.RespuestaExcel;
      if (!result.tDisp) {
        this.dataGrid = [];
        this.dataGlobal = [];
        this.listData = [];
        this.listFecha = [];
        this.base64 = null;
        this.base = null;
      } else {
        this.dataGrid = result.tDisp;
        this.dataGlobal = result.Global;
        this.listData = this.dataGrid[0].tTres;
        this.listFecha = result.tGlobal;
        this.base64 = resultExcel[0].Archivo;
        this.base = resultExcel;
      }
      this.loading = false;
    });
  }

  public exportExcel() {
    this.loading = true;
    if (this.base) {
      this._http.get<any>(GLOBAL.url + '/reportes/excel/' + this.base64).subscribe(res => {
        const result = res.response.siEnvioExcel.dsEnvioExcel.RespuestaExcel[0];
        const linkSource = 'data:application/vnd.ms-excel;base64,' + result.ArchivoExportado;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = result.Archivo;
        download.click();
      });
    }
    this.loading = false;
  }

  getDataSource() {
    this._https.getData('/manttos/deptos').map(data => data).subscribe(data => {
      if (data) {
        this.listDeptos = data.siDepto.dsDeptos.ttDeptos;
      }
    });
    this._https.getData('/manttos/ubicaciones').map(data => data).subscribe(data => {
      if (data) {
        this.listUbicacion = data.siUbicacion.dsUbicaciones.ttUbicaciones;
      }
    });
    this._https.getData('/manttos/trabajos').map(data => data).subscribe(data => {
      if (data) {
        this.listTrabajo = data.siTrabajo.dsTrabajos.ttTrabajos;
      }
    });
    this._https.getData('/manttos/camaristas').map(data => data).subscribe(data => {
      if (data) {
        this.listOperador = data.siCamarista.dsCamaristas.ttCamaristas;
      }
    });
    this._https.getData('/ayuda/agencias').map(data => data).subscribe(data => {
      if (data) {
        this.listAgencia = data.siAyuda.dsAyuda.ttAgencias;
      }
    });
  }


  exportPdf() {
    const form = JSON.parse(JSON.stringify(this.formSend));
     // tslint:disable-next-line:triple-equals
     if (this.formSend.iInt1 == 1) {
       form.fFec2 = moment(form.fFec2).format(GLOBAL.formatIso);
       form.fFec1 = moment(form.fFec1).format(GLOBAL.formatIso);
     // tslint:disable-next-line:triple-equals
     }else if (this.formSend.iInt1 == 3){
      const year = this.listFecha[0].Fecha.substring(0, 4);
      const fec1 = year + '-01-01';
      form.fFec2 = moment(form.fFec2).format(GLOBAL.formatIso);
      form.fFec1 = moment(fec1).format(GLOBAL.formatIso);
     // tslint:disable-next-line:triple-equals
     }else if (this.formSend.iInt1 == 2){
      const fecha = this.listFecha[0].Fecha;
      form.fFec2 = moment(form.fFec2).format(GLOBAL.formatIso);
      form.fFec1 = moment(fecha).format(GLOBAL.formatIso);
     }

    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: '005-ESTAD-REQ-ESP',
      dataGrid: this.dataGrid,
      dataLabel: this.listFecha,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      hora: moment().format('HH:mm:ss'),
      form: form,
      global: this.dataGlobal,
    }
    this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = this.heading + '.pdf';
        download.click();
      },
      error => {
        this.toast(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
      }
    );
  }

}
