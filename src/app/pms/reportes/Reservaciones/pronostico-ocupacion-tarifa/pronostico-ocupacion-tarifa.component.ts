import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { CompositeFilterDescriptor, distinct, filterBy, process } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { ReportesService } from '../../reportes.service';
import { GLOBAL } from '../../../../services/global';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { TblGeneral } from '../../../../models/tgeneral';
import moment from 'moment';

@Component({
  selector: 'app-pronostico-ocupacion-tarifa',
  templateUrl: './pronostico-ocupacion-tarifa.component.html',
})
export class PronosticoOcupacionTarifaComponent implements OnInit {
  heading = 'Pronóstico de Ocupación por Tarifa';
  subheading: string;
  icon: string;
  ngForm: FormGroup;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];
  dataGlobal: any = [];
  dataTotal: any = [];
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  ttUsuario: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  filter: CompositeFilterDescriptor;
  api: Array<string> = ['/reportes/Reservaciones/PROTAR', 'dsRepreserv', 'ttTgeneral'];
  loading = false;
  swal = GLOBAL.swal;
  tblGeneral: TblGeneral;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: ReportesService,
    private _http: HttpClient
  ) {
    this.ttInfocia = _info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.crudTable();
    this.ngForm = this.createForm();
    this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  crudTable() {
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.iInt1 = 1;
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).add(1, 'days').toDate();
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }

  public buscarInfo() {
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
    temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);
    this._https.postInfo(temp, this.api[0], this.api[1], this.api[2]).subscribe(data => {
      const result = data.siRepreserv.dsRepreserv;
      if (!result.Pronostico) {
        this.dataGrid = [];
      } else {
        this.dataGrid = result.Pronostico;
        this.dataGlobal = result.Tarifa;
        this.dataTotal = result.ttGlobal;
      }
      this.loading = false;
    });
  }

  filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataFilter = filterBy(this.dataGrid, filter);
  }

  distinctPrimitive(fieldName: string) {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  public allData(): ExcelExportData {
    if (this.filter) {
      const result: ExcelExportData = {
        data: process(this.dataFilter, { sort: [{ field: 'Fecha', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    } else {
      const result: ExcelExportData = {
        data: process(this.dataGrid, { sort: [{ field: 'Fecha', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    }
  }

  exportPdf() {
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(this.formatMoment);
    form.fFec2 = moment(form.fFec2).format(this.formatMoment);
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: '001-OCUPAC-TARIFA',
      dataGrid: this.dataGrid,
      global: this.dataGlobal,
      total: this.dataTotal,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      hora: moment().format('HH:mm:ss'),
      form: form,
    }
    this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = this.heading + '.pdf';
        download.click();
      },
      error => {
        this.swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
      }
    );
  }
}
