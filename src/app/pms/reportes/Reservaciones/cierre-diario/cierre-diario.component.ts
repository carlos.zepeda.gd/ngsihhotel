import { Component, OnInit } from '@angular/core';
import { CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { CrudService } from '../../../manttos/mantto-srv.services';
import { GLOBAL } from '../../../../services/global';
import { TblGeneral } from '../../../../models/tgeneral';
import moment from 'moment';

@Component({
  selector: 'app-cierre-diario',
  templateUrl: './cierre-diario.component.html',
  styleUrls: ['./cierre-diario.component.css']
})
export class CierreDiarioComponent implements OnInit {
  heading = 'Cierre Diario';
  subheading: string;
  icon: string;
  noRecords = GLOBAL.noRecords;
  loading: boolean;
  ngForm: FormGroup;
  dataGrid: any = [];
  filter: CompositeFilterDescriptor;
  dataExt: any = [];
  bancosList = [];
  swal = GLOBAL.swal;
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  pageSize = 10;
  skip = 0;
  fileExcel: any;
  dsCierreDiario: any;
  ttInfocia: any = [];
  ttUsuario: any = [];
  tblGeneral: TblGeneral;

  constructor(private _menuServ: ManttoMenuService,
    private _router: Router,
    private _crudservice: CrudService,
    private _formBuild: FormBuilder,
    private _info: InfociaGlobalService,
    private _http: HttpClient) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.crudTable();
    this.ngForm = this.createForm();
    this.loading = true;
    this._crudservice.getData('/manttos/bancos').subscribe(res => {
      this.bancosList = res.siBanco.dsBancos.ttBancos;
      this.loading = false;
    });
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  crudTable(){
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).subtract(1, 'days').toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).toDate();
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }

  buscar() {
    this.loading = true;
    this._crudservice.postInfo(this.ngForm.value, '/reportes/Reservaciones/CierreDiario', 'dsRepreserv', 'ttTgeneral').subscribe(res => {
      const result = res.siRepreserv;
      if (!result.dsRepreserv.Depositos){
        this.dataGrid = [];
        this.dataExt = [];
        this.dsCierreDiario = [];
        this.fileExcel = null;
      }else {
      this.dataGrid = result.dsRepreserv.Depositos;
      this.dataExt = result.dsRepreserv.ttTfp;
      this.dsCierreDiario = result.dsRepreserv.ttCierreDiario[0];
        if (result.dsEstadAgen.RespuestaExcel) {
          this.fileExcel = result.dsEstadAgen.RespuestaExcel[0];
        }
      }
      this.loading = false;
    });
  }

  exportExcel() {
    const linkSource = 'data:application/vnd.ms-excel;base64,' + this.fileExcel.ArchivoExportado;
    const download = document.createElement('a');
    const fileName = this.fileExcel.Archivo;
    download.href = linkSource;
    download.download = fileName;
    download.click();
  }
  exportPdf() {
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(this.formatMoment);
    form.fFec2 = moment(form.fFec2).format(this.formatMoment);
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: '001-CIERRE',
      dataGrid: this.dataGrid,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      hora: moment().format('HH:mm:ss'),
      form: form,
      fp: this.dataExt,
      global: this.dsCierreDiario
    }
    this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = this.heading + '.pdf';
        download.click();
      },
      error => {
        this.swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
      }
    );
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }


}
