import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { ReportesService } from '../../reportes.service';
import { TblGeneral } from '../../../../models/tgeneral';
import { GLOBAL } from '../../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-reporte-allotment',
  templateUrl: './reporte-allotment.component.html',
})
export class ReporteAllotmentComponent implements OnInit {
  heading = 'Reporte de Allotment';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  ngForm: FormGroup;
  noRecords = GLOBAL.noRecords;
  ttInfocia: any = [];
  dataGrid: any = [];
  dataTableGrid: any = [];
  dataGlobal: any = [];
  dataFechas: any = [];
  dataTableGlobal: any = [];
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  filter: CompositeFilterDescriptor;
  api: Array<string> = ['/reportes/Reservaciones/RepAll', 'dsRepreserv', 'ttTgeneral'];
  loading = false;
  base64 = '';
  base = '';
  maxDateDesde: Date;
  minDateHasta: Date;
  tblGeneral: TblGeneral;
  toast = GLOBAL.toast;
  swal = GLOBAL.swal;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: ReportesService,
    private _http: HttpClient,
  ) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.crudTable();
    this.ngForm = this.createForm();
    this.minDateHasta = new Date(moment(this.ttInfocia.fday).add(1, 'day').toDate());
    this.maxDateDesde = new Date(moment(this.ttInfocia.fday).toDate());
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  crudTable() {
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.iInt1 = 1;
    this.tblGeneral.iInt2 = 1;
    this.tblGeneral.iInt3 = 2;
    this.tblGeneral.iInt4 = 2;
    this.tblGeneral.iInt5 = 1;
    this.tblGeneral.lLog1 = true;
    this.tblGeneral.lLog2 = true;
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).add(1, 'days').toDate();
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }

  public getInfo() {
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
    temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);
    this._https.postInfo(temp, this.api[0], this.api[1], this.api[2]).subscribe(data => {
      const result = data.siRepreserv.dsRepreserv;
      const resultExcel = data.siRepreserv.dsEstadAgen.RespuestaExcel;
      if (!result.Disponibilidad) {
        this.dataGrid = [];
        this.dataGlobal = [];
        this.dataFechas = [];
        this.base64 = null;
        this.base = null;
      } else {
        this.dataGrid = result.Disponibilidad;
        this.dataTableGrid = result.cDispAllot;
        this.dataGlobal = result.tGlobal;
        this.dataFechas = result.ttGlobal;
        this.dataTableGlobal = result.cttGlobal;
        this.base64 = resultExcel[0].Archivo;
        this.base = resultExcel;
      }
      this.loading = false;
    });
  }

  public exportExcel() {
    this.loading = true;
    if (this.base) {
      this._http.get<any>(GLOBAL.url + '/reportes/excel/' + this.base64)
        .subscribe(res => {
          const result = res.response.siEnvioExcel.dsEnvioExcel.RespuestaExcel[0];
          const fileExcel = result.ArchivoExportado;
          const linkSource = 'data:application/vnd.ms-excel;base64,' + fileExcel;
          const download = document.createElement('a');
          download.href = linkSource;
          download.download = result.Archivo;
          download.click();
        });
    }
    this.loading = false;
  }

  fechaDesdeChange() {
    this.minDateHasta = new Date(moment(this.ngForm.value.fFec1).add(1, 'day').toDate());
  }

  fechaHastaChange() {
    this.maxDateDesde = new Date(moment(this.ngForm.value.fFec2).subtract(1, 'day').toDate());
  }
}
