import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { TblGeneral } from '../../../../models/tgeneral';
import { GLOBAL } from '../../../../services/global';
import moment from 'moment';

const httpOptions = GLOBAL.httpOptions;

@Component({
  selector: 'app-reporte-selectivo-reservacione',
  templateUrl: './reporte-selectivo-reservacione.component.html',
  styleUrls: ['./reporte-selectivo-reservacione.component.css']
})
export class ReporteSelectivoReservacioneComponent implements OnInit {
  heading = 'Reporte Selectivo de Reservaciones';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  noRecords = GLOBAL.noRecords;
  loading: boolean;
  ngForm: FormGroup;
  ngSelectivo: FormGroup;
  dataGrid: any = [];
  toast = GLOBAL.toast;
  swal = GLOBAL.swal;
  kendoFormat = localStorage.getItem('formatKendo');
  pageSize = 10;
  skip = 0;
  listTipoCuarto = [];
  listTipoMov = [];
  modalTitle = '';
  modalType = '';
  typeModal = 'age';
  ordenNumMax = 0;
  modalLabel = '';
  newModalLabel = '';
  etiquetas = [];
  api: string = GLOBAL.url + '/reportes/Reservaciones/SelRsrv';
  pegable = { buttonCount: 5, pageSizes: [10, 25, 50] }

  constructor(private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _modal: NgbModal,
    private _info: InfociaGlobalService,
    private _http: HttpClient) {
    const fecha = moment(this._info.getSessionInfocia().fday).toDate();
    this.ngForm = this.createttGeneral();
    this.ngForm.patchValue({ fFec1: fecha, fFec2: fecha, iInt1: 1, iInt2: 1, iInt3: 1 });
    this._http.get<any>(this.api).subscribe(res => {
      const selectivo = res.response.siRepreserv.dsRepreserv.ttSelectivo;
      if (selectivo) {
        this.ngSelectivo = this.createSelectivo(selectivo[0]);
      }
    });
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  createttGeneral() {
    return this._formBuild.group(new TblGeneral());
  }

  createSelectivo(selectivo) {
    return this._formBuild.group(selectivo);
  }

  changeOption(data) {
    this.ngForm.patchValue({ cChr1: '' });
    switch (data.target.id) {
      case 'ragencia':
        this.typeModal = 'age';
        break;
      case 'rsubagencia':
        this.typeModal = 'sag';
        break;
      case 'rempresa':
        this.typeModal = 'emp';
        break;
      case 'rtipocuarto':
        this.typeModal = 'tpc';
        break;
      default:
        this.typeModal = '';
        break;
    }
  }

  openModal(content, type: string, label?: string) {
    let modalSize: any = 'xl';
    switch (type) {
      case 'ctr':
        this.modalTitle = 'Tipo de Cuarto';
        this.modalType = 'crt';
        break;
      case 'age':
        this.modalTitle = 'Agencia';
        this.modalType = 'age';
        break;
      case 'sag':
        this.modalTitle = 'Subagencia';
        this.modalType = 'sag';
        break;
      case 'emp':
        this.modalTitle = 'Empresa';
        this.modalType = 'emp';
        break;
      case 'seg':
        this.modalTitle = 'Segmento';
        this.modalType = 'seg';
        break;
      case 'tm':
        this.modalTitle = 'Tipos de Movimientos';
        this.modalType = 'tm';
        break;
      case 'tpc':
        this.modalTitle = 'Tipo de Cuarto';
        this.modalType = 'tpc';
        break;

      case 'lbl':
        this.modalTitle = 'Cambiar Header';
        this.modalType = 'lbl';
        this.modalLabel = label;
        modalSize = 'sm';
        break;
    }
    this._modal.open(content, { ariaLabelledBy: 'modal-distri', size: modalSize });
  }


  getDataModal(data, type) {
    this._modal.dismissAll();

    switch (type) {
      case 'crt':
        this.listTipoCuarto.push(data);
        this.ngForm.patchValue({ valueTCuarto: JSON.parse(JSON.stringify(this.listTipoCuarto)) });
        break;
      case 'age':
        this.ngForm.patchValue({ cChr1: data.Agencia });
        break;
      case 'sag':
        this.ngForm.patchValue({ cChr1: data.Agencia });
        break;
      case 'emp':
        this.ngForm.patchValue({ cChr1: data.Empresa });
        break;
      case 'seg':
        this.ngForm.patchValue({ cChr1: data.Segmento });
        break;
      case 'tm':
        this.listTipoMov.push(data);
        this.ngForm.patchValue({ valueTMov: JSON.parse(JSON.stringify(this.listTipoMov)) });
        break;
      case 'tpc':
        this.ngForm.patchValue({ cChr1: data.tcuarto });
        break;
    }
  }

  checkChange(event) {
    const row = event.target.id.split('lSel');
    if (event.target.checked) {
      this.ordenNumMax += 1;
      const arr = '{"iInt' + row[1] + '":' + this.ordenNumMax + '}';
      this.ngSelectivo.patchValue(JSON.parse(arr));
    } else {
      if (this.ngSelectivo.get('iInt' + row[1]).value < this.ordenNumMax) {
        const numVal = this.ngSelectivo.get('iInt' + row[1]).value;

        let arr = '{"iInt' + row[1] + '":0}';
        this.ngSelectivo.patchValue(JSON.parse(arr));

        for (let cont = 1; cont <= 80; cont++) {
          if (this.ngSelectivo.get('iInt' + cont).value > numVal) {
            arr = '{"iInt' + cont + '":' + (this.ngSelectivo.get('iInt' + cont).value - 1) + '}';
            this.ngSelectivo.patchValue(JSON.parse(arr));
          }
        }
      } else {
        const arr = '{"iInt' + row[1] + '":0}';
        this.ngSelectivo.patchValue(JSON.parse(arr));
      }

      this.ordenNumMax -= 1;
    }
  }

  getNewHeader() {
    this._modal.dismissAll();
    const arr = '{"' + this.modalLabel + '":"' + this.newModalLabel + '"}';
    this.ngSelectivo.patchValue(JSON.parse(arr));
  }

  buscar() {
    this.loading = true;
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(GLOBAL.formatIso);
    form.fFec2 = moment(form.fFec2).format(GLOBAL.formatIso);

    let arr = '';
    arr += '{"dsRepreserv":{"ttTgeneral":[' + JSON.stringify(form) + '],';
    arr += '"ttSelectivo":[' + JSON.stringify(this.ngSelectivo.value) + ']';
    arr += '}}';

    this._http.post<any>(this.api, arr, httpOptions).subscribe(res => {
      const result = res.response.siRepreserv;
      this.dataGrid = result.dsRepreserv.ttSeleccionado;
      if (result.dsRepreserv.ttEtiquetas !== undefined) {
        this.etiquetas = result.dsRepreserv.ttEtiquetas[0].Etiquetas;
        const excelFile = result.dsEstadAgen.RespuestaExcel;
        if (excelFile && excelFile[0].ArchivoExportado) {
          const linkSource = 'data:application/vnd.ms-excel;base64,' + excelFile[0].ArchivoExportado;
          const download = document.createElement('a');
          const fileName = excelFile[0].Archivo;
          download.href = linkSource;
          download.download = fileName;
          download.click();
        } else {
          this.toast({ type: 'info', title: GLOBAL.textError, text: 'No hay información!!!' })
        }
      } else {
        // this.dataGrid = [];
      }
      this.loading = false;
    });
  }

  reiniciar() {
    this.ordenNumMax = 0;
    this.loading = false;
    this.dataGrid = [];
    Object.keys(this.ngSelectivo.controls).forEach(item => {
      if (item.substring(0, 4) === 'lSel') {
        this.ngSelectivo.get(item).setValue(false);
      }
      if (item.substr(0, 4) === 'iInt') {
        this.ngSelectivo.get(item).setValue(0);
      }
    });
  }
}
