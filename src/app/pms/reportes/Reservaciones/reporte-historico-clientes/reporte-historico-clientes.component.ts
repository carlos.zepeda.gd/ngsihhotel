import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { CrudService } from '../../../manttos/mantto-srv.services';
import { GLOBAL } from '../../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-reporte-historico-clientes',
  templateUrl: './reporte-historico-clientes.component.html',
  styleUrls: ['./reporte-historico-clientes.component.css']
})
export class ReporteHistoricoClientesComponent implements OnInit {
  heading = 'Reporte Histórico de Clientes';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  noRecords = GLOBAL.noRecords;
  loading: boolean;
  ngForm: FormGroup;
  ngSelectivo: FormGroup;
  dataGrid: any = [];
  toast = GLOBAL.toast;
  swal = GLOBAL.swal;
  kendoFormat = localStorage.getItem('formatKendo');
  pageSize = 10;
  skip = 0;
  listAgrupado = [];
  listTipoCuarto = [];
  listTipoMov = [];
  modalTitle = '';
  modalType = '';
  typeModal = 'age';
  ordenNumMax = 0;
  modalLabel = '';
  newModalLabel = '';
  etiquetas = [];
  api: string = GLOBAL.url + '/reportes/Reservaciones/SelHctes';

  constructor(private _menuServ: ManttoMenuService,
    private _router: Router,
    private _crudservice: CrudService,
    private _formBuild: FormBuilder,
    private _modal: NgbModal,
    private _info: InfociaGlobalService,
    private _http: HttpClient) {
    this.ngForm = this.createttGeneral();
    this._http.get<any>(this.api).subscribe(res => {
      const result = res.response.siRepreserv.dsRepreserv.ttSelectivo;
      if (result) {
        this.ngSelectivo = this.createSelectivo(result[0]);
      } else {
        this.swal('Error al obtener etiquetas!!', 'Reportelo a su personal de sistemas', 'info');
      }
    });
    this._crudservice.getData('/manttos/ingagrp').subscribe(res => {
      this.listAgrupado = res.siIngagrp.dsIngagrp.ttIngagrp;
      this.loading = false;
    });
    const fecha = this._info.getSessionInfocia().fday;
    this.ngForm.patchValue({
      fechaDesde: moment(fecha, GLOBAL.formatIso).toDate(),
      fechaHasta: moment(fecha, GLOBAL.formatIso).toDate(),
      fFec1: fecha,
      fFec2: fecha
    });
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  createttGeneral() {
    return this._formBuild.group({
      cChr1: [''],
      cChr2: [''],
      cChr3: [''],
      cChr4: [''],
      cChr5: [''],
      cChr6: [''],
      cChr7: [''],
      cChr8: [''],
      cChr9: [''],
      cChr10: [''],
      cChr11: [''],
      cChr12: [''],
      cChr13: [''],
      cChr14: [''],
      cChr15: [''],
      cChr16: [''],
      cChr17: [''],
      cChr18: [''],
      cChr19: [''],
      cChr20: [''],
      iInt1: [1],
      iInt2: [0],
      iInt3: [0],
      iInt4: [0],
      iInt5: [0],
      iInt6: [0],
      iInt7: [0],
      iInt8: [0],
      iInt9: [0],
      iInt10: [0],
      iInt11: [0],
      iInt12: [0],
      iInt13: [0],
      iInt14: [0],
      iInt15: [0],
      iInt16: [0],
      iInt17: [0],
      iInt18: [0],
      iInt19: [0],
      iInt20: [0],
      lLog1: [false],
      lLog2: [false],
      lLog3: [true],
      lLog4: [false],
      lLog5: [false],
      lLog6: [false],
      lLog7: [false],
      lLog8: [false],
      lLog9: [false],
      lLog10: [false],
      lLog11: [false],
      lLog12: [false],
      lLog13: [false],
      lLog14: [false],
      lLog15: [false],
      lLog16: [false],
      lLog17: [false],
      lLog18: [false],
      lLog19: [false],
      lLog20: [false],
      dDec1: [0.0],
      dDec2: [0.0],
      dDec3: [0.0],
      dDec4: [0.0],
      dDec5: [0.0],
      dDec6: [0.0],
      dDec7: [0.0],
      dDec8: [0.0],
      dDec9: [0.0],
      dDec10: [0.0],
      dDec11: [0.0],
      dDec12: [0.0],
      dDec13: [0.0],
      dDec14: [0.0],
      dDec15: [0.0],
      dDec16: [0.0],
      dDec17: [0.0],
      dDec18: [0.0],
      dDec19: [0.0],
      dDec20: [0.0],
      fFec1: [''],
      fFec2: [''],
      fFec3: [null],
      fFec4: [null],
      fFec5: [null],
      fFec6: [null],
      fFec7: [null],
      fFec8: [null],
      fFec9: [null],
      fFec10: [null],
      fFec11: [null],
      fFec12: [null],
      fFec13: [null],
      fFec14: [null],
      fFec15: [null],
      fFec16: [null],
      fFec17: [null],
      fFec18: [null],
      fFec19: [null],
      fFec20: [null],
      zDtz1: [null],
      zDtz2: [null],
      zDtz3: [null],
      zDtz4: [null],
      zDtz5: [null],
      zDtz6: [null],
      zDtz7: [null],
      zDtz8: [null],
      zDtz9: [null],
      zDtz10: [null],
      zDtz11: [null],
      zDtz12: [null],
      zDtz13: [null],
      zDtz14: [null],
      zDtz15: [null],
      zDtz16: [null],
      zDtz17: [null],
      zDtz18: [null],
      zDtz19: [null],
      zDtz20: [null],
      fechaDesde: [''],
      fechaHasta: [''],
    });
  }

  createSelectivo(selectivo) {
    return this._formBuild.group(selectivo);
  }

  fechaChange(num) {
    if (num === 1) {
      this.ngForm.patchValue({
        fFec1: moment(this.ngForm.value.fechaDesde).format(GLOBAL.formatIso)
      });
    } else {
      this.ngForm.patchValue({
        fFec2: moment(this.ngForm.value.fechaHasta).format(GLOBAL.formatIso)
      });
    }
  }

  changeOption(data) {
    this.ngForm.patchValue({ cChr1: '' });

    switch (data.target.id) {
      case 'ragencia':
        this.typeModal = 'age';
        break;
      case 'rsubagencia':
        this.typeModal = 'sag';
        break;
      case 'rempresa':
        this.typeModal = 'emp';
        break;
      case 'rtipocuarto':
        this.typeModal = 'tpc';
        break;
      default:
        this.typeModal = '';
        break;
    }
  }

  openModal(content, type: string, label?: string) {
    let modalSize: any = 'xl';

    switch (type) {
      case 'ctr':
        this.modalTitle = 'Tipo de Cuarto';
        this.modalType = 'crt';
        break;
      case 'age':
        this.modalTitle = 'Agencia';
        this.modalType = 'age';
        break;
      case 'sag':
        this.modalTitle = 'Subagencia';
        this.modalType = 'sag';
        break;
      case 'emp':
        this.modalTitle = 'Empresa';
        this.modalType = 'emp';
        break;
      case 'seg':
        this.modalTitle = 'Segmento';
        this.modalType = 'seg';
        break;
      case 'tm':
        this.modalTitle = 'Tipos de Movimientos';
        this.modalType = 'tm';
        break;
      case 'tpc':
        this.modalTitle = 'Tipo de Cuarto';
        this.modalType = 'tpc';
        break;

      case 'lbl':
        this.modalTitle = 'Cambiar Header';
        this.modalType = 'lbl';
        this.modalLabel = label;
        modalSize = 'sm';
        break;
    }
    this._modal.open(content, { size: modalSize });
  }

  getDataModal(data, type) {
    this._modal.dismissAll();

    switch (type) {
      case 'crt':
        this.listTipoCuarto.push(data);
        this.ngForm.patchValue({
          valueTCuarto: JSON.parse(JSON.stringify(this.listTipoCuarto))
        });
        break;
      case 'age':
        this.ngForm.patchValue({ cChr1: data.Agencia });
        break;
      case 'sag':
        this.ngForm.patchValue({ cChr1: data.Agencia });
        break;
      case 'emp':
        this.ngForm.patchValue({ cChr1: data.Empresa });
        break;
      case 'seg':
        this.ngForm.patchValue({ cChr1: data.Segmento });
        break;
      case 'tm':
        this.listTipoMov.push(data);
        this.ngForm.patchValue({
          valueTMov: JSON.parse(JSON.stringify(this.listTipoMov))
        });
        break;
      case 'tpc':
        this.ngForm.patchValue({ cChr1: data.tcuarto });
        break;
    }
  }

  checkChange(event) {
    const row = event.target.id.split('lSel');

    if (event.target.checked) {
      this.ordenNumMax += 1;
      const arr = '{"iInt' + row[1] + '":' + this.ordenNumMax + '}';
      this.ngSelectivo.patchValue(JSON.parse(arr));
    } else {
      if (this.ngSelectivo.get('iInt' + row[1]).value < this.ordenNumMax) {
        const numVal = this.ngSelectivo.get('iInt' + row[1]).value;

        let arr = '{"iInt' + row[1] + '":0}';
        this.ngSelectivo.patchValue(JSON.parse(arr));

        for (let cont = 1; cont <= 80; cont++) {
          if (this.ngSelectivo.get('iInt' + cont).value > numVal) {
            arr = '{"iInt' + cont + '":' + (this.ngSelectivo.get('iInt' + cont).value - 1) + '}';
            this.ngSelectivo.patchValue(JSON.parse(arr));
          }
        }
      } else {
        const arr = '{"iInt' + row[1] + '":0}';
        this.ngSelectivo.patchValue(JSON.parse(arr));
      }

      this.ordenNumMax -= 1;
    }
  }

  getNewHeader() {
    this._modal.dismissAll();

    const arr = '{"' + this.modalLabel + '":"' + this.newModalLabel + '"}';
    this.ngSelectivo.patchValue(JSON.parse(arr));
  }

  reiniciar() {
    this.ordenNumMax = 0;
    this.loading = false;
    this.dataGrid = [];
    Object.keys(this.ngSelectivo.controls).forEach(item => {
      if (item.substring(0, 4) === 'lSel') {
        this.ngSelectivo.get(item).setValue(false);
      }
      if (item.substr(0, 4) === 'iInt') {
        this.ngSelectivo.get(item).setValue(0);
      }
    });
  }

  buscar() {
    this.loading = true;
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    delete form.fechaDesde;
    delete form.fechaHasta;
    let arr = '';
    arr += '{"dsRepreserv":{"ttTgeneral":[' + JSON.stringify(this.ngForm.value) + '],';
    arr += '"ttSelectivo":[' + JSON.stringify(this.ngSelectivo.value) + ']';
    arr += '}}';
    
    this._http.post<any>(this.api, arr, GLOBAL.httpOptions).subscribe(res => {
      const result = res.response.siRepreserv.dsRepreserv;
      this.dataGrid = result.ttSeleccionado;
      if (result.ttEtiquetas !== undefined) {
        this.etiquetas = result.ttEtiquetas[0].Etiquetas;
        const base64 = res.response.siRepreserv.dsEstadAgen.RespuestaExcel[0];
        if (base64.ArchivoExportado) {
          const linkSource = 'data:application/vnd.ms-excel;base64,' + base64.ArchivoExportado;
          const download = document.createElement('a');
          const fileName = base64.Archivo;
          download.href = linkSource;
          download.download = fileName;
          download.click();
        }
      } else {
        this.dataGrid = [];
      }
      this.loading = false;
    });
  }
}
