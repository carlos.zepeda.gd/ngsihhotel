import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CompositeFilterDescriptor, filterBy } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { CrudService } from '../../../mantto-srv.services';
import { ReportesService } from '../../reportes.service'
import { TblGeneral } from '../../../../models/tgeneral';
import { GLOBAL } from '../../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-reporte-pick-up',
  templateUrl: './reporte-pick-up.component.html',
})
export class ReportePickUpComponent implements OnInit {
  heading = 'Reporte Pick-Up';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  ttGeneral = new TblGeneral();
  ngForm: FormGroup;
  noRecords = GLOBAL.noRecords;
  ttInfocia: any = [];
  dataGrid: any = [];
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  filter: CompositeFilterDescriptor;
  fecha: any;
  listMoneda = [];
  listTipoCuarto = [];
  dsIngAgrupados = [];
  ingAgrupadosSelected: any = [];
  api: Array<string> = ['/reportes/Reservaciones/PickUp', 'dsRepreserv', 'ttTgeneral'];
  TipoCuarto = [];
  loading = true;
  fileName: string = '';
  base: any = [];
  toast = GLOBAL.toast;
  swal = GLOBAL.swal;
  tblGeneral: TblGeneral;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: ReportesService,
    private _modal: NgbModal,
    private _http: HttpClient,
    private _crud: CrudService,
  ) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.ngForm = this.createForm();
    this.ngForm.patchValue({ cChr2: this.ttInfocia.Moneda });
    this._https.getData('/manttos/monedas').map(data => data).subscribe(data => {
      this.listMoneda = data.siMoneda.dsMonedas.ttMonedas;
    });
    this._crud.getData('/ayuda/tcuartos').map(data => data).subscribe(data => {
      this.listTipoCuarto = data.siAyuda.dsAyuda.ttTcuartos;
      return this.listTipoCuarto = filterBy(this.listTipoCuarto, {
        logic: 'and', filters: [
          { field: 'inactivo', operator: 'eq', value: false },
          { field: 'tuso', operator: 'eq', value: true }
        ],
      });
    });
    this._crud.getData('/manttos/ingagrp').subscribe(res => {
      this.dsIngAgrupados = res.siIngagrp.dsIngagrp.ttIngagrp;
    });
    this.fecha = moment(this.ttInfocia.fday).toDate();
    this.loading = false;

  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  createForm() {
    return this._formBuild.group(this.ttGeneral);
  }

  openModal(modal) {
    this._modal.open(modal, { size: 'lg', centered: true });
  }

  getDataModal(data) {
    this.ngForm.patchValue({ cChr1: data.Agencia });
  }

  public buscarInfo() {
    this.loading = true;
    let fechaStr = '';
    fechaStr = '' + (this.fecha.getMonth() + 1) + '/' + this.fecha.getFullYear();
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    if (this.fecha.getMonth() + 1 >= 10){
      temp.iInt1 = fechaStr.substr(3, 4);
      temp.iInt2 = fechaStr.substr(0, 2);
    }else {
      temp.iInt1 = fechaStr.substr(2, 4);
      temp.iInt2 = fechaStr.substr(0, 1);
    }


    const json = '{"' + this.api[1] + '":{"' + this.api[2] + '": [' + JSON.stringify(temp) + '],' +
      '"ttIagrp": ' + JSON.stringify(this.ingAgrupadosSelected) + ',' +
      '"tmptcto": ' + JSON.stringify(this.TipoCuarto) + '}}';

    this._http.post<any>(GLOBAL.url + this.api[0], JSON.parse(json)).subscribe(res => {
      const data = res.response.siRepreserv;
      const resultExcel = data.dsEstadAgen.RespuestaExcel;
      if (!data.dsRepreserv) {
        this.dataGrid = [];
      } else {
        if (!data.dsRepreserv.ttTgeneral) {
          this.dataGrid = data.dsRepreserv.ttDet;
          this.fileName = resultExcel[0].Archivo;
          this.base = resultExcel;
        } else {
          const errorMesj = data.dsMensajes.ttMensError[0].cMensTxt;
          this.swal(GLOBAL.textError, errorMesj, 'error');
        }
      }
      this.loading = false;
    });
  }


  public exportExcel() {
    this.loading = true;
    if (this.base) {
      this._http.get<any>(GLOBAL.url + '/reportes/excel/' + this.fileName)
      .subscribe(res => {
        const result = res.response.siEnvioExcel.dsEnvioExcel.RespuestaExcel[0];
        const fileExcel = result.ArchivoExportado;
        const linkSource = 'data:application/vnd.ms-excel;base64,' + fileExcel;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = result.Archivo;
        download.click();
        this.loading = false;
      });
    }
  }

  tipoIngresoChange() {
    if (Number(this.ngForm.value.iInt4) === 1) {
      this.TipoCuarto = [];
    } else if (Number(this.ngForm.value.iInt4) !== 1) {
      this.ingAgrupadosSelected = [];
    }
  }
}
