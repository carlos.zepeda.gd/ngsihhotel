import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CompositeFilterDescriptor, distinct, filterBy } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { CrudService } from '../../../manttos/mantto-srv.services';
import { TblGeneral } from '../../../../models/tgeneral';
import { GLOBAL } from '../../../../services/global';
import moment from 'moment';

interface Item {
  name: string,
  value: number
}

@Component({
  selector: 'app-produccion-agencias-paquetes',
  templateUrl: './produccion-agencias-paquetes.component.html',
})
export class ProduccionAgenciasPaquetesComponent implements OnInit {
  heading = 'Producción de Agencias por Paquetes';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  ttGeneral = new TblGeneral();
  noRecords = GLOBAL.noRecords;
  loading: boolean = true;
  ngForm: FormGroup;
  dataGrid: any = [];
  filter: CompositeFilterDescriptor;
  dataFilter: any[];
  toast = GLOBAL.toast;
  kendoFormat = localStorage.getItem('formatKendo');
  pageSize = 10;
  skip = 0;
  listMoneda = [];
  listAgrupado = [];
  fechafin = false;
  listTipoCuarto = [];
  listTipoMov = [];
  modalTitle = '';
  modalType = '';
  dataRes: any = [];
  typeModal = 'age';
  listReporte: Array<Item> = [{ name: 'Global', value: 1 }, { name: 'Detallado', value: 2 }];
  selectedReporte: Item = this.listReporte[0];

  listOrdenado: Array<Item> = [
    { name: 'Clasificación', value: 1 },
    { name: 'Subtotal', value: 2 },
    { name: 'Segmento', value: 3 },
    { name: 'País Agencia', value: 4 },
    { name: 'Tipo de Huésped', value: 5 },
    { name: 'Origen Huésped', value: 6 }
  ];
  selectedOrdenado: Item = this.listOrdenado[0];
  ttInfocia: any;

  constructor(private _menuServ: ManttoMenuService,
    private _router: Router,
    private _crud: CrudService,
    private _formBuild: FormBuilder,
    private _modal: NgbModal,
    private _info: InfociaGlobalService,
    private _http: HttpClient) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.getDataSource();
    this.ngForm = this.createFormGroup();
    this.ngForm.get('cChr2').setValidators([Validators.required]);
    this.ngForm.patchValue({ cChr2: this.ttInfocia.Moneda });
    this.validationsFormGroup();
    this.loading = false;
    this.dataFilter = filterBy([this.dataGrid], this.filter);
  }

  ngOnInit() {
    this.tipo_excel(1);
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
      }
    });
  }

  validationsFormGroup() {
    this.ngForm.patchValue({
      cChr2: this.ttInfocia.Moneda,
      iInt1: 1, iInt2: 1, iInt3: 1, iInt4: 1, iInt5: 1,
      lLog1: true, lLog2: true, lLog3: true, lLog9: true, lLog19: true,
      lLog20: true, lLog21: true, lLog22: true,
      fFec1: moment(this.ttInfocia.fday, GLOBAL.formatIso).toDate(),
      fFec2: moment(this.ttInfocia.fday, GLOBAL.formatIso).toDate(),
    });
  }

  createFormGroup() {
    return this._formBuild.group(this.ttGeneral);
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  openModal(content, type: string) {
    switch (type) {
      case 'age':
        this.modalTitle = 'Agencia';
        this.modalType = 'age';
        break;
      case 'sag':
        this.modalTitle = 'Subagencia';
        this.modalType = 'sag';
        break;
      case 'emp':
        this.modalTitle = 'Empresa';
        this.modalType = 'emp';
        break;
      case 'seg':
        this.modalTitle = 'Segmento';
        this.modalType = 'seg';
        break;
    }

    this._modal.open(content, { ariaLabelledBy: 'modal-distri', size: 'lg', centered: true });
  }

  getDataModal(data, type) {
    this._modal.dismissAll();
    switch (type) {
      case 'age':
        this.ngForm.patchValue({ cChr1: data.Agencia });
        break;
      case 'sag':
        this.ngForm.patchValue({ cChr1: data.Agencia });
        break;
      case 'emp':
        this.ngForm.patchValue({ cChr1: data.Empresa });
        break;
      case 'seg':
        this.ngForm.patchValue({ cChr1: data.Segmento });
        break;
    }
  }

  buscar() {
    this.loading = true;
    const ingAgrp: any = [];
    const tipoCuarto: any = [];
    const tipoMov: any = [];
    let form = JSON.parse(JSON.stringify(this.ngForm.value));

    // tslint:disable-next-line:triple-equals
    if (form.iInt1 != 3) {
      form.fFec2 = null;
      form.fFec1 = moment(form.fFec1).format(GLOBAL.formatIso);
    } else {
      form.fFec1 = moment(form.fFec1).format(GLOBAL.formatIso);
      form.fFec2 = moment(form.fFec2).format(GLOBAL.formatIso);
    }
    if (form.lLog15 === false){
      form.iInt4 = 0;
    }
    // tslint:disable-next-line:triple-equals
    if (form.iInt3 == '2' && typeof (form.cChr13) === 'object') {
      form.cChr13.map((value: any) => ingAgrp.push({ iagrp: value }));
    }

    if (typeof (form.cChr14) === 'object') {
      form.cChr14.map((value: any) => tipoCuarto.push({ ttcto: value }));
    }

    if (typeof (form.cChr15) === 'object') {
      form.cChr15.map((item: any) => tipoMov.push({ ttm: item.TM, tpos: item.cpos }));
    }

    form.iInt8 = 0;
    form.iInt3 = this.selectedOrdenado.value;
    form.cChr13 = '';
    form.cChr14 = '';
    form.cChr15 = '';
    let arrStr = '{"dsEstadisticos":{"bttTgen":[' + JSON.stringify(form) + ']';

    if (tipoMov.length !== 0) {
      arrStr += ',"tmptm":' + JSON.stringify(tipoMov);
    }

    if (ingAgrp.length !== 0 && (form.iInt3 === 2 || form.iInt3 === '2')) {
      arrStr += ',"ttIagrp":' + JSON.stringify(ingAgrp);
    }

    if (tipoCuarto.length !== 0) {
      arrStr += ',"tmptcto":' + JSON.stringify(tipoCuarto);
    }

    arrStr += '}}';

    form = JSON.parse(arrStr);

    this._http.put<any>(GLOBAL.url + '/reportes/Reservaciones/estadisticos/Agepaq', form).subscribe(res => {
      this.loading = false;
      if (res.response.pc_arch) {
        this.toast({ title: GLOBAL.textError, text: res.response.pc_arch, type: 'error' });
      } else {
        this.dataRes = res.response.siEstadistico.dsEstadisticos.RespuestaExcel;
      }
    });
    this.loading = false;
  }

  changeOption(data) {
    switch (data.target.id) {
      case 'agencia':
        this.typeModal = 'age';
        break;
      case 'subagencia':
        this.typeModal = 'sag';
        break;
      case 'empresa':
        this.typeModal = 'age';
        break;
      case 'segmento':
        this.typeModal = 'sag';
        break;
      default:
        this.typeModal = '';
        break;
    }
  }

  reporteChange(data) {
    // tslint:disable-next-line:triple-equals
    if (data.value == 1) {
      this.ngForm.patchValue({
        lLog19: false,
      });
    // tslint:disable-next-line:triple-equals
    } else if (data.value == 2) {
      this.ngForm.patchValue({
        lLog19: true,
      });
    }
  }


  tipo_excel(tipo) {
    if (!this.ngForm.value.lLog8 && !this.ngForm.value.lLog15 && tipo === 1) {
      this.ngForm.patchValue({ iInt8: 1 });
    }

    if (this.ngForm.value.iInt1 === '2' && tipo === 2) {
      this.ngForm.patchValue({ iInt8: 2 });
    }

    if (this.ngForm.value.lLog15 && tipo === 3) {
      this.ngForm.patchValue({ iInt8: 3 });
    }

    if (this.ngForm.value.lLog15 && tipo === 4) {
      this.ngForm.patchValue({ iInt8: 4 });
    }

    if (this.ngForm.value.lLog15 && this.ngForm.value.lLog20 && tipo === 5) {
      this.ngForm.patchValue({ iInt8: 4 });
    }

    if (this.ngForm.value.lLog8 && tipo === 6) {
      this.ngForm.patchValue({ iInt8: 5 });
    }
  }

  getDataSource() {
    this._crud.getData('/manttos/monedas').subscribe(res => this.listMoneda = res.siMoneda.dsMonedas.ttMonedas);
    this._crud.getData('/manttos/ingagrp').subscribe(res => this.listAgrupado = res.siIngagrp.dsIngagrp.ttIngagrp);
    this._crud.getData('/ayuda/tcuartos').subscribe(data => {
      const res = data.siAyuda.dsAyuda.ttTcuartos;
      return this.listTipoCuarto = filterBy(res, {
        logic: 'and', filters: [
          { field: 'inactivo', operator: 'eq', value: false },
          { field: 'tuso', operator: 'eq', value: true }
        ],
      });
    });
    this._crud.getData('/manttos/codtipos').subscribe(res => this.listTipoMov = res.siCodtipo.dsCodtipos.ttCodtipos);
    this.loading = false;
  }

  excel(indice) {
    if (this.dataRes) {
      const fileExcel = this.dataRes[indice];
      this._http.get<any>(GLOBAL.url + '/reportes/Reservaciones/estadisticos/' + fileExcel.Archivo).subscribe(res => {
        const result = res.response.siEstadistico.dsEstadisticos.RespuestaExcel;
        const linkSource = 'data:application/vnd.ms-excel;base64,' + result[0].ArchivoExportado;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = result[0].Archivo;
        download.click();
        this.loading = false;
      });
    }
  }
}
