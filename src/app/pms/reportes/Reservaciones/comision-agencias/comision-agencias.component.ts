import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { ReportesService } from '../../reportes.service';
import { TblGeneral } from '../../../../models/tgeneral';
import { GLOBAL } from '../../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-comision-agencias',
  templateUrl: './comision-agencias.component.html',
})
export class ComisionAgenciasComponent implements OnInit {
  heading = 'Comisión de Agencias';
  subheading: string;
  icon: string;
  api: Array<string> = ['/reportes/Reservaciones/AgeComision', 'dsRepreserv', 'ttTgeneral'];
  filter: CompositeFilterDescriptor;
  tblGeneral: TblGeneral;
  ngForm: FormGroup;
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  noRecords = GLOBAL.noRecords;
  toast = GLOBAL.toast;
  swal = GLOBAL.swal;
  dataGrid: any = [];
  ttInfocia: any = [];
  ttUsuario: any = [];
  dataGlobal: any = [];
  pageSize = 10;
  skip = 0;
  loading = false;
  base64 = '';
  base = '';
  modalTitle = '';
  modalType = '';

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: ReportesService,
    private _modal: NgbModal,
    private _http: HttpClient,
  ) {
    this.ttInfocia = _info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.crudTable();
    this.ngForm = this.createForm();
    this.ngForm.get('cChr1').setValidators([Validators.required]);
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  crudTable() {
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).subtract(1, 'days').toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).toDate();
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }

  openModal(modal, type: string) {
    switch (type) {
      case 'agencia':
        this.modalTitle = 'Consulta de Agencias';
        this.modalType = 'agencia';
        break;
    }

    this._modal.open(modal, { size: 'lg' });
  }

  getDataModal(data, type) {
    switch (type) {
      case 'agencia':
        this.ngForm.patchValue({ cChr1: data.Agencia });
        break;
    }
  }

  public getInfo() {
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    if (this.ngForm.value.iInt1 == 2) {
      temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
      temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);
    }
    this._https.postInfo(temp, this.api[0], this.api[1], this.api[2]).subscribe(data => {
      const result = data.siRepreserv;
      if (result.dsMensajes.ttMensError) {
        this.toast({ type: 'error', title: result.dsMensajes.ttMensError[0].cMensTxt });
      } else {
        const resultExcel = result.dsEstadAgen.RespuestaExcel;
        if (!result.dsEstadAgen.DetalleComisiones) {
          this.dataGrid = [];
          this.dataGlobal = [];
          this.base64 = null;
          this.base = null;
        } else {
          this.dataGrid = result.dsEstadAgen.DetalleComisiones;
          this.dataGlobal = result.dsEstadAgen.TotalAgencia;
          this.base64 = resultExcel[0].Archivo;
          this.base = resultExcel;
        }
      }

      this.loading = false;
    });
  }

  public exportExcel() {
    if (this.base) {
      this.loading = true;
      this._http.get<any>(GLOBAL.url + '/reportes/excel/' + this.base64)
        .subscribe(res => {
          const result = res.response.siEnvioExcel.dsEnvioExcel.RespuestaExcel[0];
          const fileExcel = result.ArchivoExportado;
          const linkSource = 'data:application/vnd.ms-excel;base64,' + fileExcel;
          const download = document.createElement('a');
          const fileName = result.Archivo;
          download.href = linkSource;
          download.download = fileName;
          download.click();
          this.loading = false;
        });
    }
  }

  exportPdf() {
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(this.formatMoment);
    form.fFec2 = moment(form.fFec2).format(this.formatMoment);
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: '001-COMISION-AGEN',
      dataGrid: this.dataGrid,
      global: this.dataGlobal,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      hora: moment().format('HH:mm:ss'),
      form: form,
    }
    this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(response => {
      const linkSource = 'data:application/pdf;base64,' + response.base64;
      const download = document.createElement('a');
      download.href = linkSource;
      download.download = this.heading + '.pdf';
      download.click();
    },
      error => {
        this.swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
      });
  }


}
