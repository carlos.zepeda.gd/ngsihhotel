// Modulos Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Modulos
import { ListahuespedesModule } from './recepcion/lista-huespedes/listahuespedes.module';
import { LlegadasModule } from './recepcion/llegadas-huespedes/llegadas.module';
import { ReservacionesModule } from './reservaciones/reservaciones.module';
import { AmallavesModule } from './ama-llaves/amallaves.module';
import { SharedAppsModule } from '../shared/shared-apps.module';
import { AuditoriaModule } from './auditoria/auditoria.module';
import { ConsultasModule } from './consultas/consultas.module';
import { RecepcionModule } from './recepcion/recepcion.module';
import { ReportesModule } from './reportes/reportes.module';
import { ManttosModule } from './manttos/manttos.module';
import { SystemModule } from '../shared/system.module';
import { KendouiModule } from '../shared/kendo.module';
import { CajaModule } from './caja/caja.module';
import { ParametrosModule } from './parametros/parametros.module';
// Rutas
import { PMS_ROUTING } from './pms.routes';
// Servicios
import { CompaniasService } from './parametros/companias/companias.service';
import { SistemaService } from './parametros/sistema/sistema.service';
import { InfociaGlobalService } from '../services/infocia.service';
import { RecepcionService } from './recepcion/recepcion.service';
import { SharedService } from '../services/shared.service';
import { MenuPmsService } from '../services/menu.service';
import { CrudService } from './mantto-srv.services';
import { PdfService } from './mantto-pdf.service';
import { TelefonoModule } from './telefonos/telefonos.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    PMS_ROUTING,
    SystemModule,
    KendouiModule,
    SharedAppsModule,
    ConsultasModule,
    AmallavesModule,
    ReservacionesModule,
    RecepcionModule,
    LlegadasModule,
    ListahuespedesModule,
    CajaModule,
    AuditoriaModule,
    ManttosModule,
    ReportesModule,
    ParametrosModule,
    TelefonoModule
  ],
  declarations: [],
  providers: [
    InfociaGlobalService,
    MenuPmsService,
    CrudService,
    PdfService,
    SharedService,
    RecepcionService,
    CompaniasService,
    SistemaService
  ],
})
export class PmsModule {
}
