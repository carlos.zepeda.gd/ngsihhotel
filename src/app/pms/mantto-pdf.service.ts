import { Injectable } from '@angular/core';
import { InfociaGlobalService } from './../services/infocia.service';
import { GLOBAL } from './../services/global';
import { filterBy } from '@progress/kendo-data-query';
declare var jsPDF: any;

@Injectable()
export class PdfService {

    private ttInfocia: any;
    public url: string;

    constructor(private _info: InfociaGlobalService) {
        this.ttInfocia = this._info.getSessionInfocia();
        this.url = GLOBAL.url;
    }


    printPDF(titulo: string, columns: any[], data: any, position: string = 'p') {
        const encabezado = this.ttInfocia.cnombre;
        const rows = data;
        const doc = new jsPDF(position);
        doc.setFontSize(8);
        doc.text(titulo + ' ' + encabezado, 14, 8);
        doc.setFontType('normal');
        doc.setFontSize(8);
        doc.setTextColor(100);
        doc.autoTable(columns, rows, {
            theme: 'plain',
            styles: {
                cellPadding: 0, // a number, array or object (see margin below)
                fontSize: 7,
                font: 'helvetica' // helvetica, times, courier
            },
        });
        doc.output('save', titulo + '.pdf');
    }

    printHTMLPDF(cont) {
        const encabezado = this.ttInfocia.cnombre;
        const doc = new jsPDF('p', 'pt');
        // const source = $('#dataForm')[0];
        // const specialElementHandlers = {
        //     '#form': function(element, renderer){ // Elemento a Omitir
        //         return true;
        //     },
        // };
        doc.setFontSize(8);
        doc.text(encabezado, 40, 30);
        doc.setFontType('normal');
        doc.setFontSize(8);
        doc.setTextColor(100);
        doc.fromHTML(cont.innerHTML, 40, 40, {
            // 'width': 600,
            // 'elementHandlers': specialElementHandlers
        });
        // doc.output('save', titulo + '.pdf');
        doc.autoPrint();
        // window.open(doc.output('bloburl'), '_blank');
        window.open(doc.output('bloburl'), '', 'width=800,height=800');
    }

    pdfLost(art, ar, fec, emp, not) {
        const encabezado = this.ttInfocia.cnombre;
        const doc = new jsPDF('p', 'pt');
        doc.setFontSize(8);
        doc.text(encabezado, 40, 30);
        doc.setFontType('italic');
        doc.setFontSize(8);
        doc.rect(40, 40, 500, 110);
        doc.text(45, 52, 'Artículo:');
        doc.text(100, 52, art);
        doc.text(45, 64, 'Área:');
        doc.text(100, 64, ar);
        doc.text(45, 76, 'Fecha:');
        doc.text(100, 76, fec);
        doc.text(45, 88, 'Empleado:');
        doc.text(100, 88, emp);
        doc.text(45, 100, 'Notas:');
        doc.text(100, 100, not);
        doc.text(45, 130, 'Cuarto/Room:_________ Nombre/Name:____________________________________ Firma/Signature:_______________________');
        doc.autoPrint();
        window.open(doc.output('bloburl'), '', 'width=800,height=800');
    }

    printAsignaCamaPdf(titulo: string, columns: any[], data: any, camaristas: any) {
        const encabezado = this.ttInfocia.cnombre;
        const doc = new jsPDF('landscape', 'pt');
        let tot_cama: number = 0;
        let page: number = 0;
        let position: number = 0;
        let totCred: number = 0;
        tot_cama = camaristas.length;

        camaristas.forEach(obj => {
            let first = doc.autoTable.previous;
            page++;
            if (page === 1 && position === 0) {
                totCred = camaristas[position].ccred;
                const result = filterBy(data, {
                    logic: 'and',
                    filters: [{ field: 'camarista', operator: 'eq', value: camaristas[position].camarista }]
                })
                doc.setFontSize(8);
                doc.text(encabezado, 40, 30);
                doc.text(625, 30, 'Hoja ' + page + '/' + tot_cama);
                doc.setFontType('normal');
                doc.setFontSize(8);
                doc.setTextColor(100);
                doc.text(40, 50, 'C/O: ' + camaristas[position].camarista);
                doc.text(90, 50, 'Nombre: ' + camaristas[position].cnombre);
                doc.autoTable(columns, result, {
                    startY: 60,
                    theme: 'striped',
                    styles: {
                        cellPadding: 5,
                        fontSize: 8,
                        font: 'helvetica' // helvetica, times, courier
                    },
                });
                first = doc.autoTable.previous;
                doc.text(40, first.finalY + 20, 'Cuartos: ' + result.length);
                doc.text(90, first.finalY + 20, ' Créditos: ' + totCred);
            } else {
                position++;
                totCred = camaristas[position].ccred;
                if (totCred > 0) {
                    const result = filterBy(data, {
                        logic: 'and',
                        filters: [{ field: 'camarista', operator: 'eq', value: camaristas[position].camarista }]
                    })
                    doc.addPage('l', 'a6');
                    doc.setFontSize(8);
                    doc.text(encabezado, 40, 30);
                    doc.text(625, 30, 'Hoja ' + page + '/' + tot_cama);
                    doc.setFontType('normal');
                    doc.setFontSize(8);
                    doc.setTextColor(100);
                    doc.text(40, 50, 'C/O: ' + camaristas[position].camarista);
                    doc.text(90, 50, 'Nombre: ' + camaristas[position].cnombre);
                    doc.autoTable(columns, result, {
                        startY: 60,
                        theme: 'striped',
                        styles: {
                            cellPadding: 5,
                            fontSize: 8,
                            font: 'helvetica' // helvetica, times, courier
                        },
                    });
                    first = doc.autoTable.previous;
                    doc.text(40, first.finalY + 20, 'Cuartos: ' + result.length);
                    doc.text(90, first.finalY + 20, ' Créditos: ' + totCred);
                }
            }
        })
        doc.output('save', titulo + '.pdf');
    }

    printTarifasDetalle(titulo: string, columns: any[], data: any, detail: any, pos: string = 'p') {

        const encabezado = this.ttInfocia.encab;
        const doc = new jsPDF(pos, 'pt');
        let position: number = 0;
        const dataTitles = [
            {
                'proc': 'Proc',
                'Tarifa': 'Tarifa',
                'ttipo': 'T.T.',
                'tdesc': 'Descripción',
                'tmon': 'Mon.',
                'tprom': 'Prom.',
                'tdesde': 'Desde',
                'thasta': 'Hasta',
                'tgratis': 'Gratis',
                'tnoches': 'Por Cada',
                'tminimo': 'Mínimo',
                'tcont': 'Tar.Cont'
            }
        ]
        const tarTittle = [
            { title: '', dataKey: 'proc' },
            { title: '', dataKey: 'Tarifa' },
            { title: '', dataKey: 'ttipo' },
            { title: '', dataKey: 'tdesc' },
            { title: '', dataKey: 'tmon' },
            { title: '', dataKey: 'tprom' },
            { title: '', dataKey: 'tdesde' },
            { title: '', dataKey: 'thasta' },
            { title: '', dataKey: 'tgratis' },
            { title: '', dataKey: 'tnoches' },
            { title: '', dataKey: 'tminimo' },
            { title: '', dataKey: 'tcont' }
        ]

        doc.setFontSize(8);
        doc.text(encabezado, 40, 30);
        doc.setFontType('normal');
        doc.setFontSize(8);
        doc.setTextColor(100);
        doc.autoTable(tarTittle, dataTitles, {
            startY: 40, theme: 'plain', styles: {
                cellPadding: 5, // a number, array or object (see margin below)
                fontSize: 9,
                fontStyle: 'bold',
                font: 'helvetica' // helvetica, times, courier
            },
            bodyStyles: {
                fillColor: [51, 102, 153],
                textColor: 255
            },
            headerStyles: {
                fontSize: 0
            },
            columnStyles: {
                proc: { columnWidth: 40 },
                Tarifa: { columnWidth: 65 },
                ttipo: { columnWidth: 30 },
                tdesc: { columnWidth: 150 },
                tmon: { columnWidth: 45 },
                tprom: { columnWidth: 45 },
                tdesde: { columnWidth: 60 },
                thasta: { columnWidth: 60 },
                tgratis: { columnWidth: 60 },
                tnoches: { columnWidth: 60 },
                tminimo: { columnWidth: 60 },
                tcont: { columnWidth: 60 }
            },
        });
        let first = doc.autoTable.previous;
        data.forEach(obj => {
            doc.autoTable(tarTittle, [data[position]], {
                startY: first.finalY - 12,
                theme: 'plain',
                styles: {
                    cellPadding: 5,
                    fontSize: 8,
                    font: 'helvetica' // helvetica, times, courier
                },
                headerStyles: {
                    fontSize: 0
                },
                bodyStyles: {
                    fillColor: [230, 236, 255],
                },
                columnStyles: {
                    proc: { columnWidth: 40 },
                    Tarifa: { columnWidth: 65 },
                    ttipo: { columnWidth: 30 },
                    tdesc: { columnWidth: 150 },
                    tmon: { columnWidth: 45 },
                    tprom: { columnWidth: 45 },
                    tdesde: { columnWidth: 60 },
                    thasta: { columnWidth: 60 },
                    tgratis: { columnWidth: 60 },
                    tnoches: { columnWidth: 60 },
                    tminimo: { columnWidth: 60 },
                    tcont: { columnWidth: 60 }
                },
            });
            first = doc.autoTable.previous;
            const ttTar = [
                { title: obj.proc, dataKey: 'x' },
                { title: obj.Tarifa, dataKey: 'tcuarto' },
                { title: obj.ttipo, dataKey: 'tfecini' },
                { title: obj.tdesc, dataKey: 'tfecter' },
                { title: obj.tmon, dataKey: 'tta1' },
                { title: obj.tprom ? 'SI' : 'NO', dataKey: 'tta2', },
                { title: obj.tdesde ? obj.tdesde : '-', dataKey: 'tta3' },
                { title: obj.thasta ? obj.thasta : '-', dataKey: 'tta11', },
                { title: obj.tgratis ? obj.tgratis : '-', dataKey: 'ttj11' },
                { title: obj.tnoches ? obj.tnoches : '-', dataKey: 'ttm11', },
                { title: obj.tminimo ? obj.tminimo : '-', dataKey: 'detalle' },
                { title: obj.tcont ? 'SI' : 'NO', dataKey: 'y', }
            ]
            const result = filterBy(detail, {
                logic: 'and',
                filters: [{ field: 'tarifa', operator: 'eq', value: data[position].Tarifa }]
            });
            doc.autoTable(ttTar, result, {
                startY: first.finalY - 12,
                theme: 'plain',
                styles: {
                    cellPadding: 5, // a number, array or object (see margin below)
                    fontSize: 8,
                    font: 'helvetica' // helvetica, times, courier
                },
                headerStyles: {
                    fontSize: 0
                },
                columnStyles: {
                    x: { columnWidth: 40 },
                    tcuarto: { columnWidth: 65 },
                    tfecini: { columnWidth: 80 },
                    tfecter: { columnWidth: 80 },
                    tta1: { columnWidth: 60 },
                    tta2: { columnWidth: 60 },
                    tta3: { columnWidth: 60 },
                    tta11: { columnWidth: 60 },
                    ttj11: { columnWidth: 60 },
                    ttm11: { columnWidth: 70 },
                    detalle: { columnWidth: 60 },
                    y: { columnWidth: 40 },
                },
            });
            first = doc.autoTable.previous;
            position++;
        });
        doc.output('save', titulo + '.pdf');
    }

}
