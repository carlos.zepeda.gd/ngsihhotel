import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { CrudService } from '../../manttos/mantto-srv.services';
import { TblGeneral } from '../../../models/tgeneral';
import { GLOBAL } from '../../../services/global';

@Component({
  selector: 'app-consulta-bandas',
  templateUrl: './consulta-bandas.component.html',
  styleUrls: ['./consulta-bandas.component.css']
})
export class ConsultaBandasComponent implements OnInit {
  heading = 'Consulta de Brazaletes';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  ttGeneral = new TblGeneral();
  loading = false;
  loadingG = false;
  pageSize = 10;
  skip = 0;
  noRecords = GLOBAL.noRecords;
  base64 = '';
  base64G = '';
  ngForm: FormGroup;
  ngFormG: FormGroup;
  dataGrid: any = [];
  dataGridG: any = [];
  dataGridGlobal: any = [];
toast = GLOBAL.toast;
  modalTitle = '';
  modalType = '';
  public filter: CompositeFilterDescriptor;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _crud: CrudService,
    private _formBuild: FormBuilder,
    private _modal: NgbModal) {
    this.ngForm = this.createForm();
    this.ngFormG = this.createForm();
    this.ngForm.get('cChr1').setValidators(Validators.required);
    this.ngFormG.get('cChr1').setValidators(Validators.required);
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  createForm() {
    return this._formBuild.group(this.ttGeneral);
  }

  openModal(content, type) {
    switch (type) {
      case 'crt':
        this.modalType = type;
        this.modalTitle = 'Buscar Huesped';
        break;
      case 'tbn':
        this.modalType = type;
        this.modalTitle = 'Tipo de Bandas';
        break;
      case 'bgl':
        this.modalType = type;
        this.modalTitle = 'Buscar Global';
        break;
    }

    this._modal.open(content, { ariaLabelledBy: 'modal-distri', size: 'lg' });
  }

  getDataModal(data, type) {
    switch (type) {
      case 'crt':
        this.ngForm.patchValue({ cChr1: data.cuarto });
        break;
      case 'tbn':
        this.ngForm.patchValue({ cChr2: data.tbanda });
        break;
    }
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  buscar() {
    this.loading = true;
    this._crud.postInfo(this.ngForm.value, '/reportes/Reservaciones/ConsultaBrazalete', 'dsRepreserv', 'ttTgeneral').subscribe(res => {
      if (res.siRepreserv.dsRepreserv.Brazaletes !== undefined) {
        this.dataGrid = res.siRepreserv.dsRepreserv.Brazaletes;
        this.base64 = res.siRepreserv.dsEstadAgen.RespuestaExcel;
      } else {
        this.dataGrid = [];
        this.base64 = '';
      }
      this.loading = false;
    });
  }

  export(base64) {
    const linkSource = 'data:application/vnd.ms-excel;base64,' + base64[0].ArchivoExportado;
    const download = document.createElement('a');
    const fileName = base64[0].Archivo;
    download.href = linkSource;
    download.download = fileName;
    download.click();
  }

  openModalGlobal(type) {
    switch (type) {
      case 'tbnG':
        this.modalType = 'tbnG';
        break;
    }
  }

  getDataModalG(data, type) {
    switch (type) {
      case 'tbnG':
        this.ngFormG.patchValue({ cChr1: data.tbanda });
        this.modalType = 'bgl';
        break;
    }
  }

  buscarG() {
    this.loadingG = true;
    this._crud.postInfo(this.ngFormG.value, '/reportes/Reservaciones/GlobalBrazaletes', 'dsRepreserv', 'ttTgeneral').subscribe(res => {
      const result = res.siRepreserv.dsRepreserv;
      if (result.ttTbrazalete !== undefined) {
        this.dataGridG = result.ttTbrazalete;
        this.base64G = res.siRepreserv.dsEstadAgen.RespuestaExcel;
        this.dataGridGlobal = result.ttGlob;
      } else {
        this.dataGridG = [];
        this.base64G = '';
        this.dataGridGlobal = [];
      }
      this.loadingG = false;
    });
  }

}
