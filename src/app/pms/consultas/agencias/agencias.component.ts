import { Component, OnInit } from '@angular/core';
import { AgenciasService } from './agencias.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CompositeFilterDescriptor, filterBy } from '@progress/kendo-data-query';
import { CheckinService } from '../../../pms/recepcion/check-in/services/checkin.service';
import { ManttoMenuService } from '../../../pms/manttos/mantto-menu/mantto-menu.service';
import { CrudService } from '../../../pms/mantto-srv.services';
import { GLOBAL } from '../../../services/global';
import { Observable } from 'rxjs';
import moment from 'moment';
const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-agencias',
  templateUrl: './agencias.component.html',
  styleUrls: ['./agencias.component.css']
})
export class AgenciasComponent implements OnInit {
  heading = 'Mantenimiento de Agencias';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  api: Array<string> = ['/manttos/agencias', 'siAgencia', 'dsAgencias', 'ttAgencias'];
  api2: Array<string> = ['/manttos/agenbotones', 'siAgencia', 'dsAgencias', 'ttEtarifas'];
  noRecords = GLOBAL.noRecords;
  textSuccess = GLOBAL.textSuccess;
  textError = GLOBAL.textError;
  formatKendo = localStorage.getItem('formatKendo');
  dataGrid = [];
  gridDataServicios = [];
  gridDataServiciosDetalles = [];
  comisiones = [];
  agencias: any;
  servicios: any;
  ngForm: FormGroup;
  pageSize = 10;
  skip = 0;
  dsTT: any = [
    { tipo: 'E', desc: 'E.P. (E)' },
    { tipo: 'A', desc: 'ALL INCLUSIVE (A)' },
    { tipo: 'P', desc: 'PAQUETE (P)' },
    { tipo: 'C', desc: 'CORTESIA (C)' }
  ]
  dsClasif = [];
  dsTipoHuesped = [];
  dsCondCredito = [];
  dsSegmentos = [];
  dsVendedores = [];
  dsFormasPago = [];
  dsSucursales = [];
  dsOrigenes = [];
  dsTipoCuartos = [];
  loading: boolean;
  flagNuevo: boolean;
  toast = GLOBAL.toast;
  uso: string;
  methodButtons: string = 'put';
  dsTarifas: any = [];
  ngFormDetCom: FormGroup;
  ngFormTarifas: FormGroup;
  ngFormAllot: FormGroup;
  ngFormCentral: FormGroup;
  ngFormObserv: FormGroup;
  ngFormMistery: FormGroup;
  ngFormGarantias: FormGroup;
  filter: CompositeFilterDescriptor;
  dataComplete: any[] = [];
  checked: boolean;

  constructor(private _agenciaServ: AgenciasService,
    private _helps: CheckinService,
    private _crud: CrudService,
    private _formBuild: FormBuilder,
    private _modalService: NgbModal,
    private _menuServ: ManttoMenuService,
    private _active: ActivatedRoute,
    private _router: Router) {

    this.ngForm = this.createFormGroup();
    this.marcarRequeridos();
    this.ngFormDetCom = this.createFgDetCom();
    this.ngFormTarifas = this.createFgTarifas();
    this.ngFormAllot = this.createFgAllot();
    this.ngFormCentral = this.createFgCentral();
    this.ngFormObserv = this.createFgObserv();
    this.ngFormMistery = this.createFgMistery();
    this.ngFormGarantias = this.createFgGarantias();
    this._helps.getSegmentos().subscribe(data => this.dsSegmentos = data);
    this._helps.getOrigen().subscribe(data => this.dsOrigenes = data);
    this._helps.getFormaPago().subscribe(data => this.dsFormasPago = data);
    this._helps.getVendedores().subscribe(data => this.dsVendedores = data);
    this._helps.getTipoHuesped().subscribe(data => this.dsTipoHuesped = data);
    this._helps.getTarifas().subscribe(data => this.dsTarifas = data);
    this._helps.getTipoCuartos().subscribe(data => {
      this.dsTipoCuartos = data;
      this.dsTipoCuartos.forEach(item => item.tdesc = item.tdesc + ' - ' + item.tcuarto);
    });

    this._crud.getData('/manttos/agclasif').subscribe(data => {
      const res = data.siAgclasif.dsAgclasif.ttAgclasif;
      if (res) {
        this.dsClasif = filterBy(res, {
          logic: 'and', filters: [
            { field: 'Inactivo', operator: 'eq', value: false },
          ]
        });
      }
    });
    this._crud.getData('/manttos/agtipos').subscribe(data => {
      const res = data.siAgtipo.dsAgtipos.ttAgtipos;
      if (res) {
        this.dsCondCredito = filterBy(res, {
          logic: 'and', filters: [
            { field: 'Inactivo', operator: 'eq', value: false },
          ]
        });
      }
    });
    this._crud.getData('/manttos/sucursales').subscribe(data => {
      const res = data.siSucursal.dsSucursales.ttSucursales;
      if (res) {
        this.dsSucursales = filterBy(res, {
          logic: 'and', filters: [
            { field: 'Inactivo', operator: 'eq', value: false },
          ]
        });
      }
    });
    this._active.data.subscribe(data => {
      if (data.uso) {
        this.uso = data.uso;
      } else {
        Object.keys(this.ngForm.controls).forEach(key => this.ngForm.get(key).disable());
      }
    });
    this.getDataSource();
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  createFgDetCom() {
    return this._formBuild.group({
      Agencia: [''],
      proc: [''],
      tarifa: ['', Validators.required],
      atipo: [''],
      acomision: [[]],
      amonto: [0],
      cRowID: [''],
      lError: [false],
      cErrDes: ['']
    });
  }

  createFgTarifas() {
    return this._formBuild.group({
      prov: [''],
      Agencia: [''],
      atarifa: ['', Validators.required],
      atipo: [''],
      cRowID: [''],
      lError: [false],
      cErrDes: ['']
    });
  }

  createFgAllot() {
    return this._formBuild.group({
      Agencia: [''],
      msecuencia: [0],
      tcuarto: ['', Validators.required],
      mfecini: [null, Validators.required],
      mfecter: [null, Validators.required],
      mnctos: [0],
      mcutoff: [0],
      mhotel: [''],
      magencia: [''],
      mnotas: [''],
      Usuario: [''],
      cRowID: [''],
      lError: [false],
      cErrDes: ['']

    });
  }

  createFgCentral() {
    return this._formBuild.group({
      Agencia: [''],
      Estado: [''],
      aciudad: [''],
      acp: [''],
      adesc: ['', Validators.required],
      adireccion: [['', '']],
      atelefono: [['', '']],
      aemail: [''],
      afax: [''],
      cRowID: [''],
      lError: [false],
      cErrDes: ['']

    });
  }

  createFgObserv() {
    return this._formBuild.group({
      Agencia: [''],
      afecha: [null, Validators.required],
      aobserv: [['', '', '']],
      cRowID: [''],
      lError: [false],
      cErrDes: ['']
    });
  }

  createFgMistery() {
    return this._formBuild.group({
      Agencia: [''],
      afecsol: ['', Validators.required],
      abase: [''],
      aestancia: [''],
      agateway: [''],
      aperiodo: [''],
      apersona: [''],
      aofertas: [['', '', '']],
      aobserv: [['', '', '']],
      cRowID: [''],
      lError: [false],
      cErrDes: ['']
    });
  }

  createFgGarantias() {
    return this._formBuild.group({
      Agencia: [''],
      msecuencia: [0],
      mfecini: ['', Validators.required],
      mfecter: ['', Validators.required],
      Usuario: [''],
      tcuarto: ['', Validators.required],
      mnctos: [0],
      mcutoff: [0],
      mnotas: [''],
      cRowID: [''],
      lError: [false],
      cErrDes: ['']
    });
  }

  createFormGroup() {
    return this._formBuild.group({
      Agencia: ['', [Validators.required, Validators.maxLength(10)]],
      anombre: ['', [Validators.required, Validators.maxLength(50)]],
      adireccion1: [''],
      adireccion2: [''],
      aciudad: [''],
      estado: [''],
      acp: [''],
      atelefono1: [''],
      atelefono2: [''],
      afax: [''],
      aemail: [''],
      rfc: [''],
      aclasif: ['', [Validators.required]],
      proc: [''],
      atop: [false],
      atipo: ['', [Validators.required]],
      acontrol: [false],
      treser: [''],
      segmento: [''],
      anotas: [''],
      aiata: ['', [Validators.maxLength(10)]],
      afp: [''],
      origen: [''],
      cobranza: [''],
      sucursal: ['', [Validators.required]],
      afact: [''],
      arfact: [false],
      // forma fact no encontrada
      Vendedor: [''],
      Maestra: [''],
      Usuario: [''],
      acontacto1: [''],
      acontacto2: [''],
      acontacto3: [''],
      acontacto4: [''],
      acontacto5: [''],
      acontacto6: [''],
      apuesto1: [''],
      apuesto2: [''],
      apuesto3: [''],
      apuesto4: [''],
      apuesto5: [''],
      apuesto6: [''],
      aemc1: [''],
      aemc2: [''],
      aemc3: [''],
      aemc4: [''],
      aemc5: [''],
      aemc6: [''],
      iModID: [0],
      cColor: [''],
      aenv: [''],
      Inactivo: [false],
      cImagen: [''],
      cRowID: [''],
      lError: false,
      cErrDes: ''
    });
  }

  getDataSource(e?) {
    this.flagNuevo = false;
    this._agenciaServ.get_agencias().subscribe(data => {
      this.agencias = data.response.siAyuda.dsAyuda.ttAgencias;
      this.ngForm.patchValue(this.agencias[0]);
      this.dataGrid = this.agencias;
      this.dataComplete = this.agencias;
      this.filter = {
        logic: 'and',
        filters: [
          { field: 'Inactivo', operator: 'eq', value: false },
        ]
      };
    });

  }

  nuevo(modal) {
    this.ngForm = this.createFormGroup();
    this.marcarRequeridos();
    this.flagNuevo = true;
    this.openModal(modal);
  }

  nuevoButtons(e) {
    this.methodButtons = 'post';
    switch (e) {
      case 'C':
        this.ngFormDetCom = this.createFgDetCom();
        this.ngFormDetCom.patchValue({ Agencia: this.ngForm.value.Agencia, proc: 'T', acomision: [false, false, false, false] });
        break;
      case 'T':
        this.ngFormTarifas = this.createFgTarifas();
        this.ngFormTarifas.patchValue({ Agencia: this.ngForm.value.Agencia, prov: 'A' });
        break;
      case 'A':
        this.ngFormAllot = this.createFgAllot();
        this.ngFormAllot.patchValue({ Agencia: this.ngForm.value.Agencia, sec: 0 });
        break;
      case 'O':
        this.ngFormObserv = this.createFgObserv();
        this.ngFormObserv.patchValue({ Agencia: this.ngForm.value.Agencia });
        break;
      case 'E':
        this.ngFormCentral = this.createFgCentral();
        this.ngFormCentral.patchValue({ Agencia: this.ngForm.value.Agencia });
        break;
      case 'G':
        this.ngFormGarantias = this.createFgGarantias();
        this.ngFormGarantias.patchValue({ Agencia: this.ngForm.value.Agencia });
        break;
      case 'M':
        this.ngFormMistery = this.createFgMistery();
        this.ngFormMistery.patchValue({ Agencia: this.ngForm.value.Agencia });
        break;
    }

  }

  parseData(dataItem, param) {
    switch (param) {
      case 'C':
        for (const i in dataItem.acomision) {
          if (dataItem.acomision[i]) {
            dataItem.acomision[i] = 'S';
          } else {
            dataItem.acomision[i] = 'N';
          }
        }
        dataItem.acomision = dataItem.acomision.join('');
        break;
      case 'G':
        dataItem.mfecini = moment(dataItem.mfecini).format(GLOBAL.formatIso);
        dataItem.mfecter = moment(dataItem.mfecter).format(GLOBAL.formatIso);
        break;
      case 'M':
        dataItem.afecsol = moment(dataItem.afecsol).format(GLOBAL.formatIso);
        break;
      case 'O':
        dataItem.afecha = moment(dataItem.afecha).format(GLOBAL.formatIso);
        break;
      case 'A':
        dataItem.mfecini = moment(dataItem.mfecini).format(GLOBAL.formatIso);
        dataItem.mfecter = moment(dataItem.mfecter).format(GLOBAL.formatIso);
        break;
    }
    return dataItem;
  }

  serviceButtons(data, param, eliminar?) {
    const url = this.api2[0] + '/' + param;
    let dataItem = JSON.parse(JSON.stringify(data));
    dataItem = this.parseData(dataItem, param);
    if (eliminar) {
      this.methodButtons = 'delete';
    }
    let response: Observable<any>;

    switch (this.methodButtons) {
      case 'post':
        response = this._crud.postInfo(dataItem, url, this.api2[2], this.api2[3]);
        break;
      case 'put':
        response = this._crud.putInfo(dataItem, url, this.api2[2], this.api2[3]);
        break;
      case 'delete':
        response = this._crud.deleteData(dataItem, url, this.api2[3]);
        break;
    }
    response.subscribe(res => {
      const err = res[this.api[1]].dsMensajes.ttMensError;
      if (err !== undefined) {
        this.toast({
          type: 'error', title: this.textError, text: err[0].cMensTxt
        });
      } else {
        this.toast({ type: 'success', title: this.textSuccess });
        this.methodButtons = 'put';
        this.gridDataServicios = [];
        this.openService(param);
      }
      this.loading = false;
    });
  }

  guardar(form) {
    this.loading = true;
    if (!this.flagNuevo) {
      this._crud.putInfo(form, this.api[0], this.api[2], this.api[3]).subscribe(res => this.resultService(res));
    } else {
      this._crud.postInfo(form, this.api[0], this.api[2], this.api[3]).subscribe(res => this.resultService(res));
    }
  }

  editar(dataItem, modal) {
    this.openModal(modal);
    this.ngForm.patchValue(dataItem);
    this.flagNuevo = false;
  }

  eliminar(dataItem) {
    this._crud.deleteData(dataItem, this.api[0], this.api[3]).subscribe(res => this.resultService(res, true));
  }

  resultService(res, eliminar?) {
    const response = res[this.api[1]][this.api[2]][this.api[3]];
    if (response && response[0]['cErrDes']) {
      this.toast({
        type: 'error', title: this.textError,
        text: response[0]['cErrDes']
      });
    } else {
      if (eliminar) {
        this.ngForm.reset();
      }
      this._modalService.dismissAll();
      this.getDataSource(this.ngForm.value.Agencia);
      this.toast({ type: 'success', title: this.textSuccess });
    }
    this.loading = false;
  }

  selectedAgencia(data) {
    this.ngForm.patchValue(data.selectedRows[0].dataItem);
  }

  selectedComision(data) {
    this.methodButtons = 'put';
    this.fillComision(data.selectedRows[0].dataItem);
  }

  selectedTarifa(data) {
    this.fillTarifa(data.selectedRows[0].dataItem);
  }

  selectedAllotment(data) {
    this.fillAllotment(data.selectedRows[0].dataItem);
  }

  selectedCentrales(data) {
    this.fillCentral(data.selectedRows[0].dataItem);
  }

  selectedMistery(data) {
    this.fillMistery(data.selectedRows[0].dataItem);
  }

  selectedGarantia(data) {
    this.fillGarantia(data.selectedRows[0].dataItem);
  }

  selectedObservaciones(data) {
    this.ngFormObserv.patchValue(data.selectedRows[0].dataItem);
  }

  openApp(modal, opcion) {
    this.methodButtons = 'put';
    this.openService(opcion);
    this.openModal(modal);
    this.nuevoButtons(opcion);
  }
  openModal(modal) {
    this._modalService.open(modal, { size: 'lg' });
  }

  openService(opcion) {
    this._agenciaServ.get_servicio(this.ngForm.get('Agencia').value, opcion).subscribe(res => {
      const result = res.response[this.api[1]][this.api[2]];
      if (opcion === 'C') {
        this.api2[3] = 'ttAgcomisiones';
        this.servicios = result.ttAgcomisiones;
        if (typeof this.servicios !== 'undefined') {
          this.servicios.forEach(serv => {
            serv.acomision = serv.acomision.split('');
            for (let item of serv.acomision) {
              if (item === 'S') {
                item = true;
              }
              if (item === 'N') {
                item = false;
              }
            }
          });

          this.fillComision(this.servicios[0]);
        }
      }
      if (opcion === 'T') {
        this.api2[3] = 'ttAgtarifas';
        this.servicios = result.ttAgtarifas;
        if (typeof this.servicios !== 'undefined') {
          this.fillTarifa(this.servicios[0]);

          let strTarifa = '';
          let auxTarifa = [];
          if (this.servicios[0].atarifa.indexOf('/') !== -1) {
            auxTarifa = this.servicios[0].atarifa.split('/');

            for (let i = 0; i < auxTarifa.length; i++) {
              strTarifa += auxTarifa[i];
              if (i < auxTarifa.length - 1) {
                strTarifa += '©';
              }
            }

          } else {
            strTarifa = this.servicios[0].atarifa;
          }
          this._agenciaServ.get_tarifas_detalles(strTarifa).subscribe(data => {
            this.gridDataServiciosDetalles = data.response.siAyuda.dsAyuda.ttTarifas;
          });

        }
      }
      if (opcion === 'A') {
        this.api2[3] = 'ttAllotma';
        this.servicios = result.ttAllotma;
        if (typeof this.servicios !== 'undefined') {
          this.servicios.forEach(item => {
            if (item) {
              item.mfecini = moment(item.mfecini).toDate();
              item.mfecter = moment(item.mfecter).toDate();
            }
          });
          this.fillAllotment(this.servicios[0]);
        }
      }
      if (opcion === 'E') {
        this.api2[3] = 'ttAgcentral';
        this.servicios = result.ttAgcentral;
        if (typeof this.servicios !== 'undefined') {
          this.fillCentral(this.servicios[0]);
        }
      }
      if (opcion === 'M') {
        this.api2[3] = 'ttAgmistery';
        this.servicios = result.ttAgmistery;

        if (typeof this.servicios !== 'undefined') {
          this.servicios.forEach(item => {
            item.afecsol = moment(item.afecsol).toDate();
          });
          this.fillMistery(this.servicios[0]);
        }
      }
      if (opcion === 'O') {
        this.api2[3] = 'ttAgobserv';
        this.servicios = result.ttAgobserv;
        if (typeof this.servicios !== 'undefined') {
          this.servicios.forEach(item => {
            item.afecha = moment(item.afecha).toDate();
          });
          this.ngFormObserv.patchValue(this.servicios[0]);
        }
      }
      if (opcion === 'G') {
        this.api2[3] = 'ttAggarantia';
        this.servicios = result.ttAggarantia;

        if (typeof this.servicios !== 'undefined') {
          this.servicios.forEach(item => {
            if (item) {
              item.mfecini = moment(item.mfecini).toDate()
              item.mfecter = moment(item.mfecter).toDate();
            }
          });
          this.fillGarantia(this.servicios[0]);
        }
      }
      if (typeof this.servicios !== 'undefined') {
        this.gridDataServicios = this.servicios;
      } else {
        this.gridDataServicios = [];
      }
    });
  }

  resultRfc(e) {
    this.ngForm.patchValue({ rfc: e.rfc });
  }

  resultEstado(e) {
    if (this.ngFormCentral.value.Agencia) {
      this.ngFormCentral.patchValue({ Estado: e.Estado });
    } else {
      this.ngForm.patchValue({ estado: e.Estado });
    }
  }

  resultMaestra(e) {
    this.ngForm.patchValue({ Maestra: e.hfolio });
  }

  fillComision(data) {
    this.ngFormDetCom.patchValue(data);
  }

  fillTarifa(ttAgtarifas: any) {
    this.ngFormTarifas.patchValue(ttAgtarifas);
  }

  fillAllotment(ttAllotma: any) {
    this.ngFormAllot.patchValue(ttAllotma);
  }

  fillGarantia(ttAggarantia: any) {
    this.ngFormGarantias.patchValue(ttAggarantia);
  }

  fillMistery(ttAgmistery: any) {
    this.ngFormMistery.patchValue(ttAgmistery);
  }

  fillCentral(ttAgcentral: any) {
    this.ngFormCentral.patchValue(ttAgcentral);
  }

  tSelectChanged(data) {
    for (let cont = 0; cont < this.dsTT.length; cont++) {
      if (this.dsTT[cont].tipo === data.ttipo) {
        this.ngFormTarifas.patchValue({
          atipo: this.dsTT[cont].tipo
        });
        cont = this.dsTT.length;
      } else {
        this.ngFormTarifas.patchValue({
          atipo: ''
        });
      }
    }
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || { logic: 'and', filters: [] };
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  marcarRequeridos() {
    Object.keys(this.ngForm.controls).forEach(key => {
      this.ngForm.get(key).markAsDirty();
      this.ngForm.get(key).markAsTouched();
    });
  }
}
