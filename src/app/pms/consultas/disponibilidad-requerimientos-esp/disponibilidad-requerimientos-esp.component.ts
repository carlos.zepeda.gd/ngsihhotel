import {Component, OnInit} from '@angular/core';
import {ManttoMenuService} from '../../manttos/mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import {CrudService} from '../../manttos/mantto-srv.services';
import {FormBuilder} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {InfociaGlobalService} from '../../../services/infocia.service';
import {SharedService} from '../../../services/shared.service';
import {HttpClient} from '@angular/common/http';
import {GLOBAL} from '../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-disponibilidad-requerimientos-esp',
  templateUrl: './disponibilidad-requerimientos-esp.component.html',
  styleUrls: ['./disponibilidad-requerimientos-esp.component.css']
})
export class DisponibilidadRequerimientosEspComponent implements OnInit {
  heading = 'Disponibilidad de Requerimientos Especiales';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  noRecords = GLOBAL.noRecords;
  loading = false;

  kendoFormat = localStorage.getItem('formatKendo');

  fecha: Date = null;

  cols = [];
  dataGrid = [];

  constructor(private _menuServ: ManttoMenuService,
              private _router: Router,
              private _crudservice: CrudService,
              private formBuild: FormBuilder,
              private modal: NgbModal,
              private _info: InfociaGlobalService,
              private _excel: SharedService,
              private http: HttpClient) {
    this.fecha = moment(this._info.getSessionInfocia().fday, GLOBAL.formatIso).toDate();

    this.consulta();
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  changeFecha(num) {
    if (num === 1) {
      this.fecha = moment(this.fecha).add(-5, 'd').toDate();
    } else {
      this.fecha = moment(this.fecha).add(5, 'd').toDate();
    }

    this.consulta();
  }

  consulta() {
    this.loading = true;

    this.http.get<any>(GLOBAL.url + '/consultas/varias/dispreq®' + moment(this.fecha).format(GLOBAL.formatIso)).subscribe(res => {
      this.dataGrid = res.response.siConsulta.dsConsulta.ttDispReq;
      this.cols = res.response.siConsulta.dsConsulta.Etiqueta[0].Etiqueta;

      this.loading = false;
    });
  }

}
