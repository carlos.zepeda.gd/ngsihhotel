import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BuscarService } from '../../recepcion/check-in/services/childs/buscar.service';
import { CuartosBloqueadosServicesService } from './cuartos-bloqueados-services.service';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-cuartos-bloqueados',
  templateUrl: './cuartos-bloqueados.component.html',
  styleUrls: ['./cuartos-bloqueados.component.css']
})
export class CuartosBloqueadosComponent implements OnInit {
  heading = 'Cuartos Bloqueados';
  subheading: string;
  icon: string;
  loading: boolean;
  formatDay = localStorage.getItem('formatKendo');
  fechaSys = JSON.parse(localStorage.getItem('globalInfocia')).fday;
  value: Date = null;
  buscarCuarto = [this.fechaSys, 'U', '', 'U'];
  valueCB: any;
  name_folio = true;
  fechaPasada: boolean;
  colorsBg = [
    '000000',
    '000080',
    '008000',
    '008080',
    '800000',
    '800080',
    'abab29',
    'c0c0c0ab',
    'c0c0c0ab',
    '0000ff',
    '00ff00ba',
    '00ffff',
    'ff0000db',
    'ff00ff9e',
    'f0df0a',
    'ffffff'
  ];
  cuarto: string = '';
  noFolio: any;
  loadingColor: any = GLOBAL.loadingColor;
  
  constructor(private _menuServ: ManttoMenuService,
    private _router: Router,
    private _infoCiaServ: InfociaGlobalService,
    private _cuartosBS: CuartosBloqueadosServicesService,
    private _modalService: NgbModal,
    private _bucarServ: BuscarService) {
    this.loading = true;
    this.value = moment(this.fechaSys, GLOBAL.formatIso).toDate();
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });

    this._cuartosBS.getCuartos(this.buscarCuarto).subscribe(res => {
      this.valueCB = res.response.siConsulta.dsConsulta;
      this.colsapn();

      this.loading = false;
    });
  }

  surfCuarto(direction: string, button: boolean) {
    const data = [];
    const fecha = moment(this.value).format(localStorage.getItem('formatMoment'));

    if (!button) {
      data.push(this._infoCiaServ.serverDate(this.value));
      data.push('U');
      if (this.cuarto) {
        data.push('' + this.cuarto);
      } else {
        data.push('');
      }
      data.push('');
      this.buscarCuarto = [this._infoCiaServ.formatDate(this.value)];
    } else {
      if (direction === 'U') {
        const fechaU = moment(this.value).add(15, 'd').toDate();
        this.value = fechaU;
        data.push(moment(this.value).format(GLOBAL.formatIso));
        data.push('U');
      } else {
        const fechaU = moment(this.value).add(-15, 'd').toDate();
        this.value = fechaU;
        data.push(moment(this.value).format(GLOBAL.formatIso));
        data.push('U');
      }

      if (this.cuarto) {
        data.push('' + this.cuarto);
      } else {
        data.push('');
      }

      data.push('');

      if (moment(this.fechaSys).toDate().getTime() > this.value.getTime()) {
        this.fechaPasada = true;
      } else {
        this.fechaPasada = false;
      }
    }
    this._cuartosBS.getCuartos(data).subscribe(res => {
      this.valueCB = res.response.siConsulta.dsConsulta;
    });
  }

  preNext(surf: string) {
    const tamRegistroCuartos = this.valueCB.ttTbloqueo.length;
    const cuartoInicial = this.valueCB.ttTbloqueo[0].Cuarto;
    const cuartoFinal = this.valueCB.ttTbloqueo[tamRegistroCuartos - 1].Cuarto;

    const data = [];
    const fecha = moment(this.value).format(localStorage.getItem('formatMoment'));

    data.push(this._infoCiaServ.serverDate(this.value));
    data.push('U');

    if (surf === 'N') {
      data.push('' + cuartoFinal);
      data.push('U');
    } else {
      data.push('' + cuartoInicial);
      data.push('D');
    }

    this._cuartosBS.getCuartos(data).subscribe(res => {
      this.valueCB = res.response.siConsulta.dsConsulta;
    });
  }

  /*Modal Functions*/

  openModalHuesped(content, folio) {
    this._bucarServ.getHuespedFolio(folio).subscribe(res => {
      this.noFolio = res[0];
      this._modalService.open(content, {
        size: 'xl', centered: true
      });
    });
  }

  openModalReservacion(content, folio) {
    this._bucarServ.getReservacion(folio, 'folio').subscribe(res => {
      this.noFolio = res[0];
      this._modalService.open(content, {
        size: 'xl', centered: true
      });
    });
  }

  colsapn() {
  }

}
