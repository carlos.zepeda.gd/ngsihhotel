import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { ConsultaAllotmentService } from '../consulta-allotment/consulta-allotment-services.service'
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../services/global';
import { HttpClient } from '@angular/common/http';
import { TblGeneral } from '../../../models/tgeneral';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import moment from 'moment';

@Component({
  selector: 'app-consulta-allotment',
  templateUrl: './consulta-allotment.component.html',
})
export class ConsultaAllotmentComponent implements OnInit {
  heading = 'Allotment';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  dataGlobal: any = [];
  dataMensual: any = [];
  dataTotal: any = [];
  filter: CompositeFilterDescriptor;
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  ttUsuario: any = [];
  minDateDesde: Date;
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  loading: boolean;
  modalTitle = '';
  modalType = '';
  base64 = '';
  base = '';
  closeResult: string;
  selectedItem: any = [];
  api: Array<string> = ['/reportes/Reservaciones/ConsAllot', 'dsRepreserv', 'ttTgeneral'];
  toast = GLOBAL.toast;
  swal = GLOBAL.swal;
  tblGeneral: TblGeneral;
  fecha: any;
  tipoGrid = true;
  tipoCto: any;
  dias: any;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: ConsultaAllotmentService,
    private _http: HttpClient,
    private _modal: NgbModal,
  ) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.crudTable();
    this.ngForm = this.createForm();
    this.minDateDesde = new Date(moment(this.ttInfocia.fday).toDate());
    this.fecha = moment(this.ttInfocia.fday).toDate();
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  crudTable() {
    this.tblGeneral = new TblGeneral();
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }


  openModal(content, type: string) {
    switch (type) {
      case 'Agencia':
        this.modalTitle = 'Selección de Agencia';
        this.modalType = 'Agencia';
        break;
    }

    this._modal.open(content, { size: 'lg' });
  }

  getDataModal(data, type: string) {
    switch (type) {
      case 'Agencia':
        this.ngForm.patchValue({ cChr1: data.Agencia });
        break;
    }
  }

  public getInfo() {
    this.loading = true;
    let fechaStr = '';
    const numMes = <number>this.fecha.getMonth() + 1;
    fechaStr = '' + numMes + '/' + this.fecha.getFullYear();
    if (fechaStr.substr(0, 2) === '10' || fechaStr.substr(0, 2) === '11' || fechaStr.substr(0, 2) === '12') {
      this.ngForm.patchValue({
        iInt2: fechaStr.substr(fechaStr.length - 4, fechaStr.length),
        iInt1: fechaStr.substr(0, 2),
      });
    } else {
      this.ngForm.patchValue({
        iInt2: fechaStr.substr(fechaStr.length - 4, fechaStr.length),
        iInt1: fechaStr.substr(0, 1),
      });
    }
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    this._https.putInfo(temp, this.api[0], this.api[1], this.api[2], GLOBAL.httpOptions)
      .subscribe(data => {

        const result = data.siRepreserv.dsRepreserv;
        const resultExcel = data.siRepreserv.dsEstadAgen.RespuestaExcel;
        if (!result.AllotmentDetalle) {
          this.dataGrid = [];
          this.dataGlobal = [];
          this.dataMensual = [];
          this.dataTotal = [];
          this.tipoCto = [];
          this.base64 = '';
          this.base = '';
        } else {
          this.dataGrid = result.AllotmentDetalle;
          this.dataGlobal = result.DispAllotment;
          this.dataMensual = result.AllotmentMensual;
          this.dataTotal = result.TotalGlobal[0];
          this.tipoCto = this.dataGrid[0].AllDetCto;
          this.base64 = resultExcel[0].Archivo;
          this.base = resultExcel;

        }
        this.loading = false;
      });
  }

  public exportExcel() {
    if (this.base) {
      this.loading = true;
      this._http.get<any>(GLOBAL.url + '/reportes/excel/' + this.base64)
        .subscribe(res => {
          const result = res.response.siEnvioExcel.dsEnvioExcel.RespuestaExcel[0];
          const fileExcel = result.ArchivoExportado;
          const linkSource = 'data:application/vnd.ms-excel;base64,' + fileExcel;
          const download = document.createElement('a');
          download.href = linkSource;
          download.download = result.Archivo;
          download.click();
          this.loading = false;
        });
    }
  }

  exportPdf() {
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(this.formatMoment);
    form.fFec2 = moment(form.fFec2).format(this.formatMoment);
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: '000-CONSULT-ALLOT',
      dataGrid: this.dataGrid,
      global: this.dataGlobal,
      total: this.dataTotal,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      hora: moment().format('HH:mm:ss'),
      form: form,
    }
    this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = this.heading + '.pdf';
        download.click();
      },
      error => {
        this.swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
      });
  }
}
