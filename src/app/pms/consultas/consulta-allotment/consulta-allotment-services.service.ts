import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { GLOBAL } from '../../../services/global';

@Injectable({
  providedIn: 'root'
})
export class ConsultaAllotmentService {

  public url: string;

  constructor(private http: HttpClient) {
    this.url = GLOBAL.url;
  }


  getData(url: string) {
    return this.http.get<any>(this.url + url)
      .map(res => res.response);
  }

  putInfo(data, urlSec: string, ds: string, tt: string, { headers: HttpHeaders }) {
    const json = JSON.stringify(data);
    const params = '{"' + ds + '":{"' + tt + '":[' + json + ']}}';

    return this.http.put<any>(this.url + urlSec, params, GLOBAL.httpOptions)
      .map(res => res.response);
  }

  postInfo(data, urlSec: string, ds: string, tt: string) {
    const json = JSON.stringify(data);
    const params = '{"' + ds + '":{"' + tt + '":[' + json + ']}}';

    return this.http.post<any>(this.url + urlSec, params, GLOBAL.httpOptions)
      .map(res => res.response);
  }

  delete(data, module) {
    /*const httpOptionsDelete = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      body: data
    };
    return this.http.delete<any>(this.url + '/manttos/telefonos/' + module, httpOptionsDelete);*/
  }


}
