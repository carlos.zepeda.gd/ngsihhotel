import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { GLOBAL } from 'app/services/global';
import 'rxjs/add/operator/map';

const headers = new Headers({ 'Content-Type': 'application/json' });
const esp = GLOBAL.char174;

@Injectable()
export class HistoricoClientesServices {

    public url: string;

    constructor(
        private http: Http
    ) {
        this.url = GLOBAL.url;
    }

    getEstadoCivil() {
        return this.http.get(this.url + '/manttos/eciviles')
            .map((res: Response) => res.json().response.siEcivil.dsEciviles.ttEciviles);
    }

    getClasificacion() {
        return this.http.get(this.url + '/manttos/clasif')
            .map((res: Response) => res.json().response.siClasif.dsClasif.ttClasif);
    }

    getOcupacion() {
        return this.http.get(this.url + '/manttos/ocupaciones')
            .map((res: Response) => res.json().response.siOcupacion.dsOcupaciones.ttOcupaciones);
    }

    getTitulo() {
        return this.http.get(this.url + '/manttos/titulos')
            .map((res: Response) => res.json().response.siTitulo.dsTitulos.ttTitulos);
    }

    getCliente(prop, cliente) {
        return this.http.get(this.url + '/manttos/histctes/' + prop + esp + cliente)
            .map((res: Response) => res.json().response.siHistcte.dsHistctes);
    }

    getClientes(value: any, field: string, value2?: string) {
        if (value === undefined || value === '') {
            value = '';
        }
        if (value2 === undefined || value2 === '') {
            value2 = '';
        }
        let urlAyuda = '';
        switch (field) {
            case 'nombre':
                urlAyuda = `N${esp + value + esp + value2}`;
                break;
            case 'contrato':
                urlAyuda = `O${esp + value}`;
                break;
            case 'cliente':
                urlAyuda = `C${esp + '' + esp + value}`;
                break;
            case 'email':
                urlAyuda = `E${esp + value}`;
                break;
            default:
                urlAyuda = `*${esp + value + esp + value2}`;
        }

        return this.http.get(this.url + '/ayuda/clientes' + esp + 'H' + esp + urlAyuda)
            .map((res: Response) => res.json().response.siAyuda.dsAyuda.ttHistctes);
    }

    getClientesRsrv(value: any, field: string, value2?: string) {
        if (value === undefined || value === '') {
            value = '*';
        }
        if (value2 === undefined || value2 === '') {
            value2 = '*';
        }
        let urlAyuda = '';
        switch (field) {
            case 'nombre':
                urlAyuda = `N${esp + value + esp + value2}`;
                break;
            case 'contrato':
                urlAyuda = `O${esp + value}`;
                break;
            case 'cliente':
                urlAyuda = `C${esp + '' + esp + value}`;
                break;
            case 'email':
                urlAyuda = `E${esp + value}`;
                break;
        }
        return this.http.get(this.url + '/ayuda/clientes' + esp + 'R' + esp + urlAyuda)
            .map((res: Response) => res.json().response.siAyuda.dsAyuda.ttHistctes);
    }

    post(data) {
        const json = JSON.stringify(data);
        const params = '{"dsHistctes":{"ttHistctes":[' + json + ']}}';

        return this.http.post(this.url + '/manttos/histctes', params, { headers: headers })
            .map(res => {
                const result = res.json().response.siHistcte;
                if (result.dsMensajes.ttMensError) {
                    return result.dsMensajes.ttMensError[0];
                }
                return result.dsHistctes.ttHistctes;

            });
    }

    update(data) {
        const json = JSON.stringify(data);
        const params = '{"dsHistctes":{"ttHistctes":[' + json + ']}}';

        return this.http.put(this.url + '/manttos/histctes', params, { headers: headers })
            .map(res => {
                const val = res.json().response.siHistcte;
                if (val.dsMensajes.ttMensError) {
                    return val.dsMensajes.ttMensError[0];
                } else {
                    return val.dsHistctes.ttHistctes;

                }
            });
    }

    delete(data) {
        const json = JSON.stringify(data);
        const params = '{"dsHistctes":{"ttHistctes":[' + json + ']}}';

        return this.http.delete(this.url + '/manttos/histctes',
            new RequestOptions({ headers: headers, body: params }))
            .map(res => {
                const result = res.json().response.siHistcte;
                if (result.dsMensajes.ttMensError) {
                    return result.dsMensajes.ttMensError;
                }else {
                    return result.dsHistctes.ttHistctes;

                }
            });
    }
}






