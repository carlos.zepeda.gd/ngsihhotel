import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { CheckinService } from '../../recepcion/check-in/services/checkin.service';
import { HistoricoClientesServices } from './historico-clientes.service';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';

@Component({
    selector: 'app-historico-clientes',
    templateUrl: './historico-clientes.component.html',
    providers: [
        HistoricoClientesServices
    ]
})
export class HistoricoClientesComponent implements OnInit, OnChanges, OnDestroy {
    heading = 'Historico de Clientes';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';
    @Input() data: any;
    @Output() result = new EventEmitter;
    public myFormGroup: FormGroup;
    public ttInfocia: any = [];
    public formatKendo: string = localStorage.getItem('formatKendo');
    public dsOcupacion = [];
    public dsIdioma = [];
    public dsEstadoCivil = [];
    public dsTitulo = [];
    public dsClasif = [];
    public dsVendedores = [];
    public dsTipoNotas: any = [];
    public dsNotas: any = [];
    public dsHistdet: any = [];
    public dsResumenCte: any = [];
    public dsEnvCorreo = [{ value: 'C', desc: 'CASA' }, { value: 'O', desc: 'OFICINA' }];
    public editar: boolean;
    public nuevo: boolean = true;
    public direccion1: string;
    public direccion2: string;
    uso: any;
    public swal = GLOBAL.swal;

    constructor(private _fb: FormBuilder,
        private _info: InfociaGlobalService,
        private _ayuda: CheckinService,
        private _serv: HistoricoClientesServices,
        private _menuServ: ManttoMenuService,
        private _router: Router,
        private _modalService: NgbModal,
        private _active: ActivatedRoute) {
        this._active.data.subscribe(data => {
            this.uso = data.uso;
        });
        this.ttInfocia = this._info.getSessionInfocia();
        this._ayuda.getIdiomas().subscribe(data => this.dsIdioma = data);
        this._ayuda.getVendedores().subscribe(data => this.dsVendedores = data);
        this._serv.getClasificacion().subscribe(data => this.dsClasif = data);
        this._serv.getTitulo().subscribe(data => this.dsTitulo = data);
        this._serv.getEstadoCivil().subscribe(data => this.dsEstadoCivil = data);
        this._serv.getOcupacion().subscribe(data => this.dsOcupacion = data);
        this.buildFormGroup();
    }

    ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
          const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
          if (resp) {
            this.icon = resp.icon;
            this.subheading = resp.subheading;
          }
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['data'] && changes['data'].currentValue && changes['data'].firstChange) {
            if (this.data.reactual === 'VI') {
                this.direccion1 = this.data.rdireccion1;
                this.direccion2 = this.data.rdireccion2;
                this.setReservacion(this.data);
            } else {
                this.direccion1 = this.data.hdireccion1;
                this.direccion2 = this.data.hdireccion2;
                this.data.hdfp = '';
                this.data.hfp = '';
                this.myFormGroup.patchValue(this.data);
                this.myFormGroup.patchValue({ clasif: 'H' });
            }
            if (this.data.ncte) {
                this.editar = true;
                this.nuevo = false;
                this.clienteChange(this.data.ncte);
            } else {
                this.editar = false;
                this.nuevo = true;
                this.guardarRegistro();
            }
        }
    }
    ngOnDestroy() {
        this.result.emit(this.myFormGroup.value);
    }
    setReservacion(data) {
        this.myFormGroup.patchValue({
            ncte: data.ncte, // string
            hapellido: data.rapellido, // date
            hnombre: data.rnombre, // string
            htitulo: data.titulo,
            hdireccion: [data.rdireccion1, data.rdireccion2],
            hciudad: data.rciudad,
            estado: data.estado,
            hcp: data.rcp, // string
            htelefono: data.rtelefono,
            hidioma: data.ridioma,
            rfc: data.rfc,
            hmail: data.remail,
            hfp: data.rfp,
        });
    }
    private buildFormGroup() {
        this.myFormGroup = this._fb.group({
            propiedad: [''], // number
            ncte: [0], // string
            hapellido: ['', Validators.required], // date
            hnombre: ['', Validators.required], // string
            htitulo: [''],
            hdireccion: [[]],
            hciudad: [''],
            estado: [''],
            hcp: [''], // string
            htelefono: [''],
            hidioma: ['', Validators.required],
            ECivil: [''],
            clasif: ['', Validators.required],
            hfecalta: [{ value: moment(this.ttInfocia.fday).toDate(), disabled: true }],
            hfecnac: [null],
            hfecaniv: [null],
            rfc: [''],
            hpuesto: [''],
            ocupacion: [''],
            hemail: [''],
            hecorreo: [''],
            hreqesp: [''],
            Vendedor: [{ value: null, disabled: true }],
            hfp: [''],
            hdfp: [''],
            htllegadas: [0],
            htnoches: [0],
            htnoshow: [0],
            hthab: [0],
            htayb: [0],
            htotros: [0],
            hultfec: [{ value: null, disabled: true }],
            henv: [''],
            contrato: [''],
            Inactivo: [false],
            iModId: [0],
            cRowID: [''], cErrDesc: [''], lError: [false]
        });
        this.onChangesForm();
    }

    // Usuario cancela movimientos??? cClave
    onChangesForm() {
        this.myFormGroup.valueChanges.subscribe(change => {

        });
    }

    resultRfc(e) {
        this.myFormGroup.patchValue({ rfc: e.rfc })
    }

    clienteChange(value) {
        this._serv.getCliente('', value).subscribe(data => {
            if (data) {
                this.resultNotas(data);
            } else {
                this.buildFormGroup();
                this.swal('No. Cliente no Existe', '', 'warning');
            }
        });
    }

    resultNotas(e) {
        this.resultClientes(e.ttHistctes[0]);
        if (e.ttNotas !== undefined) {
            this.dsTipoNotas = e.ttTnotas;
            this.dsNotas = e.ttNotas;
        }
        if (e.ttHistdet !== undefined) {
            this.dsHistdet = e.ttHistdet;
        } else {
            this.dsHistdet = []
        }
        if (e.ttThis !== undefined) {
            this.dsResumenCte = e.ttThis;
        } else {
            this.dsResumenCte = []
        }
    }

    resultClientes(e) {
        if (e.hultfec) {
            e.hultfec = moment(e.hultfec).toDate();
        }
        if (e.hfecalta) {
            e.hfecalta = moment(e.hfecalta).toDate();
        }
        if (e.hfecaniv) {
            e.hfecaniv = moment(e.hfecaniv).toDate();
        }
        if (e.hfecnac) {
            e.hfecnac = moment(e.hfecnac).toDate();
        }
        this.direccion1 = e.hdireccion[0];
        this.direccion2 = e.hdireccion[1];
        e.htayb = e.htayb.toFixed(2);
        e.hthab = e.hthab.toFixed(2);
        e.htotros = e.htotros.toFixed(2);
        this.myFormGroup.patchValue(e);
        this.editar = true;
        this.nuevo = false;
    }

    nuevoRegistro() {
        this.buildFormGroup();
        this.nuevo = true;
    }

    resultEstado(e) {
        this.myFormGroup.patchValue({ estado: e.Estado })
    }

    guardarRegistro() {
        const form = [...[this.myFormGroup.value]][0];
        if (form.hfecalta) {
            form.hfecalta = moment(form.hfecalta).format(GLOBAL.formatIso);
        } else {
            form.hfecalta = this.ttInfocia.fday;
        }
        if (form.hultfec) {
            form.hultfec = moment(form.hultfec).format(GLOBAL.formatIso);
        }
        if (form.hfecaniv) {
            form.hfecaniv = moment(form.hfecaniv).format(GLOBAL.formatIso);
        }
        if (form.hfecnac) {
            form.hfecnac = moment(form.hfecnac).format(GLOBAL.formatIso);
        }
        form.hdireccion = [this.direccion1, this.direccion2];
        if (this.nuevo) {
            this._serv.post(form).subscribe(data => {
                if (data) {
                    if (data.cMensTxt) {
                        return this.swal(data.cMensTit, data.cMensTxt, 'error');
                    }
                    this.swal('Registro Guardado!', '', 'success');
                    this.resultClientes(data[0]);
                }
            });
        }
        if (this.editar) {
            this._serv.update(form).subscribe(data => {
                if (data) {
                    if (data.cMensTxt) {
                        return this.swal(data.cMensTit, data.cMensTxt, 'error');
                    }
                    this.swal('Registro Guardado!', '', 'success');

                    this.resultClientes(data[0]);
                }
            });
        }
    }

    eliminarRegistro() {
        this._serv.delete(this.myFormGroup.value).subscribe(data => {
            if (data && data[0].cMensTxt) {
               return this.swal(data[0].cMensTit, data[0].cMensTxt, 'error');
            }
            this.buildFormGroup();
            this.swal('Registro eliminado!', '', 'success');
        });
    }

    reiniciarForm() {

    }

    // imprimirRegistro() {

    // }

    openModal(content) {
        this._modalService.open(content, { size: 'lg' });
    }
}
