import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'app-detalle-cliente',
  templateUrl: './detalle-cliente.component.html',
  styleUrls: ['./detalle-cliente.component.css']
})
export class DetalleClienteComponent implements OnInit, OnChanges {
  @Input() data: any = [];
  @Input() resumen: any = [];
  public dataGrid: any = [];
  
  constructor() { }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['data'] !== undefined && changes['data'].currentValue) {
    }
  }
}
