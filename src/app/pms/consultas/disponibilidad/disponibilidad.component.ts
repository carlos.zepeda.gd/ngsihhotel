import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { CrudService } from '../../mantto-srv.services';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-disponibilidad',
  templateUrl: './disponibilidad.component.html',
  styleUrls: ['./disponibilidad.component.css']
})
export class DisponibilidadComponent implements OnInit {
  heading = 'Disponibilidad';
  subheading: string;
  icon: string;
  chartValues = { cuarto: '', total: '', keys: [] };
  private urlSec: string = '/consultas/disponibilidad';
  private siBase: string = 'siDispon';
  private ds: string = 'dsDispon';
  public gridFields = [];
  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public sort: SortDescriptor[] = [{ field: '', dir: 'asc' }]; // Order DataGrid
  public formatKendo = localStorage.getItem('formatKendo');;
  public ttInfocia: any = [];
  public fechaHoy: any;
  public ttDispon: any = [];
  public diaLet: any;
  public totCtos: any;
  public totfinv: any;
  public ttfinv: any;
  public ttcasa: any;
  public treserv: any;
  public tfserv: any;
  public tporc: any;
  public tsalesp: any;
  public tpact: any;
  public ttotalc: any;
  public tucfinv: any;
  public tctoesp: any;
  public tctovta: any;
  public tcdisp: any;
  public casaesp: any;
  public casaact: any;
  public llegesp: any;
  public llegact: any;
  public wiesp: any;
  public wiact: any;
  public salpend: any;
  public salact: any;
  public saliact: any;
  public adesp: any;
  public adact: any;
  public junesp: any;
  public junact: any;
  public menesp: any;
  public menact: any;
  public tdisp: any;
  public trsvgar: any;
  public trsvnog: any;
  public trgrupo: any;
  public ttcort: any;
  public ttusoc: any;
  public ttfserv: any;
  public dsIngagrp: any = [];
  public iagrpEstatus: any = null;
  public newDAte: any;
  public fechaBusqueda: any;
  public ingagprBusqueda: any;
  public ttTmpctos: any = [];
  public dataOcup: any = [];
  public dataCventa: any = [];
  public distFormGroup: FormGroup;
  public fechaDisp: any;
  public ingagprDisp: any;
  public noDias: any = 15;
  public dataDias: any = [];
  public addDist: boolean = false;
  public gridGraf: any = [];
  public grafFormGroup: FormGroup;
  public ttTdisgra = [];

  public totales: any = {
    iagrp: '',
    inactivo: false,
    maxadjun: 0,
    maxmenor: 0,
    minadjun: 0,
    tagrptarifa: '',
    tallototment: '',
    tcasa: 0,
    tcfinv: 0,
    tcolor: 0,
    tcons: 0,
    tcortesia: 0,
    tcreales: 0,
    tctosob: 0,
    tcuarto: 'Total:',
    tdescripcion: '',
    tdfinv: false,
    tdispon: 0,
    tfinv: 0,
    tforden: 0,
    tfserv: 0,
    tgrupo: 0,
    tlesp: 0,
    tmpc: 0,
    trrsrvnogar: 0,
    trsrvgar: 0,
    tsalidas: 0,
    tsec: 0,
    tsobreventa: false,
    ttotalctos: 0,
    ttshare: 0,
    tucfinv: 0,
    tusocasa: 0,
    tvirtual: '',
    tvtas: 0,
    tdisp: 0
  };
  dataGridDisp: any = [];
  dispLabels: any = [];
  loading = true;
  loadingColor: any = GLOBAL.loadingColor;

  constructor(private _infoCia: InfociaGlobalService,
    private _crudservice: CrudService,
    private _modalService: NgbModal,
    private _menuServ: ManttoMenuService,
    private _router: Router) {
  }

  ngOnInit() {
    this.initialData();
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  public getDateDisp() {
    const form = JSON.parse(JSON.stringify(this.distFormGroup.value));
    this.fechaDisp = '';
    this.ingagprDisp = '';
    this.fechaDisp = this._infoCia.formatDate(form.fecha);
    this.ingagprDisp = form.iagrp;
    this.noDias = form.noDias;
  }

  changeGraficaDisp(form, minus?) {
    switch (form) {
      case 'g':
        let gdias = this.grafFormGroup.value.noDias;
        if (minus) {
          gdias = -this.grafFormGroup.value.noDias;
        }
        this.buscaDisp(this.grafFormGroup.value.fecha, gdias, form);
        break;
      case 'd':
        let ddias = this.distFormGroup.value.noDias;
        if (minus) {
          ddias = -this.distFormGroup.value.noDias;
        }
        this.buscaDisp(this.distFormGroup.value.fecha, ddias, form);
        break;
    }
  }

  public buscarDisp() {
    this.getDateDisp();
    const date = this._infoCia.toDate(this.fechaDisp);
    this.distribucion(true, date);
  }

  openModalDistri(modal) {
    this._modalService.open(modal, {
      ariaLabelledBy: 'modal-distri',
      size: 'xl', centered: true
    });
    this.distribucion();
  }

  openChartModal(content, event: any) {
    this.chartValues = { cuarto: '', total: '', keys: [] };

    this.chartValues.cuarto = event.tcuarto;
    this.chartValues.total = event.tdispon;

    this.chartValues.keys.push(event.tcasa);
    this.chartValues.keys.push(event.trsrvgar);
    this.chartValues.keys.push(event.trrsrvnogar);
    this.chartValues.keys.push(event.tgrupo);
    this.chartValues.keys.push(event.tcortesia);
    this.chartValues.keys.push(event.tusocasa);
    this.chartValues.keys.push(event.ttshare);
    this.chartValues.keys.push(event.tfserv);
    this.chartValues.keys.push(event.tvtas);

    this._modalService.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'lg', centered: true
    });
  }

  openModalGraf(content) {
    this._modalService.open(content, {
      ariaLabelledBy: 'modal-graf',
      size: 'xl', centered: true
    });
    this.graficas();
  }

  public distribucion(search: boolean = false, date: Date = null) {
    this.distForm();
    if (!search) {
      this.getDate();
      this.fechaDisp = moment(this.fechaBusqueda).format(GLOBAL.formatIso);
      date = moment(this.fechaBusqueda).toDate();
      // this.noDias = 15;
    } else {
      this.fechaDisp = moment(date).format(GLOBAL.formatIso);
    }

    let servicio: any;
    this.distFormGroup.patchValue({ fecha: date, noDias: this.noDias });
    // const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    // const fechaEsp = date.toLocaleDateString('es-ES', options);
    servicio = this.queryDisp(this.fechaDisp, this.ingagprBusqueda, 'C', this.noDias, 'S', 'ttTiagrp');
    servicio.map(data => data = data.siDispon.dsDispon).subscribe(data => {
      this.dataGridDisp = data.ttTmpctos;
      this.dispLabels = data.ttTdiscto[0];

      let rowOcup: any;
      let daynum: any;
      let num: any;
      let porc: any;
      let aux: any;
      rowOcup = data.ttTdiscto[0];
      this.dataDias = [];
      this.dataOcup = [];
      this.dataCventa = [];
      const noDias = this.noDias;
      let fechaEsp = '';
      for (let n = 1; n <= noDias; n++) {
        daynum = 'daynum' + n;
        /*num = 'numcto' + n;
        const options = {weekday: 'short', month: '2-digit', day: '2-digit', year: 'numeric'};
        const dateTitle = this.infoCia.toDate(rowOcup[daynum]);
        let fechaEsp = dateTitle.toLocaleDateString('es-ES', options);

        aux = fechaEsp.split(', ');
        fechaEsp = aux[0] + aux[1];*/

        fechaEsp = moment(rowOcup[daynum]).locale('es').format('dd') + '. ' + moment(rowOcup[daynum]).format(localStorage.getItem('formatMoment'));

        this.dataDias.push({ title: fechaEsp, dataKey: num });
      }
      for (let n = 1; n <= noDias; n++) {
        num = 'numcto' + n;
        this.dataCventa.push({ [num]: rowOcup[num] });
      }
      for (let p = 1; p <= noDias; p++) {
        porc = 'porccto' + p;
        this.dataOcup.push({ [porc]: rowOcup[porc] });
      }
      this.ttTmpctos = data.ttTmpctos;
    });
  }

  public distForm() {
    this.distFormGroup = new FormGroup({
      fecha: new FormControl('', Validators.required),
      iagrp: new FormControl(''),
      noDias: new FormControl(this.noDias)
    });
  }

  public grafForm() {
    this.grafFormGroup = new FormGroup({
      fecha: new FormControl('', Validators.required),
      iagrp: new FormControl(''),
      noDias: new FormControl(this.noDias)
    });
  }

  public graficas(search: boolean = false, date: Date = null) {
    this.grafForm();
    if (!search) {
      this.getDate();
      this.fechaDisp = moment(this.fechaBusqueda).format(GLOBAL.formatIso);
      date = moment(this.fechaBusqueda).toDate();
      // this.noDias = 15;
    } else {
      this.fechaDisp = moment(date).format(GLOBAL.formatIso);
    }
    let servicio: any;
    this.grafFormGroup.patchValue({ fecha: date, noDias: this.noDias });
    servicio = this.queryDisp(this.fechaDisp, this.ingagprBusqueda, 'G', this.noDias, 'S', '');
    servicio.map(data => data = data.siDispon.dsDispon).subscribe(data => {
      this.ttTdisgra = data.ttTdisgra;
      for (let i = 0; i < this.ttTdisgra.length; i++) {
        this.ttTdisgra[i].tfecha = moment(this.ttTdisgra[i].tfecha).format(localStorage.getItem('formatMoment'));
      }
    });
  }

  public queryDisp(fecha, ingagrup, opcion, dias = 8, sumres, tabla) {
    let servicio: any;
    if (ingagrup) {
      const json = { Iagrp: ingagrup };
      servicio = this._crudservice.putInfo(json,
        this.urlSec + '/' + opcion + '®' + fecha + '®' + dias + '®' + sumres + '®', this.ds, tabla);
    } else {
      servicio = this._crudservice.getData(this.urlSec + '/' + opcion + '®' + fecha + '®' + dias + '®' + sumres + '®')
    }
    return servicio;
  }

  public formGrup() {
    this.myFormGroup = new FormGroup({
      fecha: new FormControl(this.fechaHoy, Validators.required),
      iagrp: new FormControl(''),
      Inactivo: new FormControl(false),
      cRowID: new FormControl(''),
      lError: new FormControl(false),
      cErrDes: new FormControl(''),
    });
  }

  public getDate() {
    this.fechaBusqueda = '';
    this.ingagprBusqueda = '';
    const form = this.myFormGroup.value;
    this.fechaBusqueda = this._infoCia.formatDate(form.fecha);
    this.ingagprBusqueda = form.iagrp;
  }

  public buscar() {
    this.getDate();
    this.getDataSource(this.fechaBusqueda, this.ingagprBusqueda);
  }

  public addDays(date: Date, days: number) {
    date.setDate(date.getDate() + days);
    return this._infoCia.formatDate(date);
  }

  public buscaDisp(date: Date, days: number, form: any = '') {
    date = moment(date).add(days, 'd').toDate();

    if (form === '') {
      this.myFormGroup.patchValue({ fecha: date });
    }

    switch (form) {
      case 'd':
        this.getDateDisp();
        this.distribucion(true, date);
        break;
      case 'g':
        this.getDateDisp();
        this.graficas(true, date);
        break;
      default:
        this.getDate();
        this.getDataSource(this.fechaBusqueda, this.ingagprBusqueda);
        break;
    }
  }

  private getDataSource(fecha, iagrp: any = '') {
    fecha = moment(fecha).format(GLOBAL.formatIso);
    let servicio: any;
    if (iagrp) {
      const json = { Iagrp: iagrp };
      servicio = this._crudservice.putInfo(json, this.urlSec + '/D®' + fecha, this.ds, 'ttTiagrp')
    } else {
      servicio = this._crudservice.getData(this.urlSec + '/D®' + fecha)
    }
    servicio.map(data => data = data[this.siBase][this.ds]).subscribe(data => {
      this.ttDispon = data.ttDispon[0];
      this.diaLet = this.ttDispon.diaLet;
      this.totCtos = this.ttDispon.totctos;
      this.totfinv = this.ttDispon.totfinv;
      this.ttfinv = this.ttDispon.ttfinv;
      this.ttcasa = this.ttDispon.ttcasa;
      this.treserv = this.ttDispon.treserv;
      this.tfserv = this.ttDispon.tfserv;
      this.tporc = this.ttDispon.tporc;
      this.tsalesp = this.ttDispon.tsalesp;
      this.tpact = this.ttDispon.tpact;

      this.tdisp = this.ttDispon.tdisp;

      this.ttotalc = this.ttDispon.ttotalc;
      this.tucfinv = this.ttDispon.tucfinv;
      this.tctoesp = this.ttDispon.tctoesp;
      this.tctovta = this.ttDispon.tctovta;
      this.tcdisp = this.ttDispon.tcdisp;

      this.casaesp = this.ttDispon.casaesp;
      this.casaact = this.ttDispon.casaact;
      this.llegesp = this.ttDispon.llegesp;
      this.llegact = this.ttDispon.llegact;
      this.wiact = 0; // Walk in: 0
      this.wiesp = this.ttDispon.wiesp;
      this.salpend = this.ttDispon.salpend;
      this.salact = this.ttDispon.salact;
      this.saliact = this.ttDispon.saliact;
      this.adesp = this.ttDispon.adesp;
      this.adact = this.ttDispon.adact;
      this.junesp = this.ttDispon.junesp;
      this.junact = this.ttDispon.junact;
      this.menesp = this.ttDispon.menesp;
      this.menact = this.ttDispon.menact;

      this.tdisp = this.ttDispon.tdisp;
      this.trsvgar = this.ttDispon.trsvgar;
      this.trsvnog = this.ttDispon.trsvnog;
      this.trgrupo = this.ttDispon.trgrupo;
      this.ttcort = this.ttDispon.ttcort;
      this.ttusoc = this.ttDispon.ttusoc;
      this.ttfserv = this.ttDispon.ttfserv;

      this.dataGrid = data.ttTmpdisp;
      this.dataGrid = {
        data: orderBy(this.dataGrid, this.sort),
        total: this.dataGrid.length
      };

      this.totales.maxadjun = 0;
      this.totales.maxmenor = 0;
      this.totales.minadjun = 0;
      this.totales.tcasa = 0;
      this.totales.tcfinv = 0;
      this.totales.tcolor = 0;
      this.totales.tcons = 0;
      this.totales.tcortesia = 0;
      this.totales.tcreales = 0;
      this.totales.tctosob = 0;
      this.totales.tdispon = 0;
      this.totales.tfinv = 0;
      this.totales.tforden = 0;
      this.totales.tfserv = 0;
      this.totales.tgrupo = 0;
      this.totales.tlesp = 0;
      this.totales.tmpc = 0;
      this.totales.trrsrvnogar = 0;
      this.totales.trsrvgar = 0;
      this.totales.tsalidas = 0;
      this.totales.tsec = 0;
      this.totales.ttotalctos = 0;
      this.totales.ttshare = 0;
      this.totales.tucfinv = 0;
      this.totales.tusocasa = 0;
      this.totales.tvtas = 0;
      this.totales.tdisp = 0;

      for (let i = 0; i < this.dataGrid.total; i++) {
        this.totales.maxadjun += this.dataGrid.data[i].maxadjun;
        this.totales.maxmenor += this.dataGrid.data[i].maxmenor;
        this.totales.minadjun += this.dataGrid.data[i].minadjun;
        this.totales.tcasa += this.dataGrid.data[i].tcasa;
        this.totales.tcfinv += this.dataGrid.data[i].tcfinv;
        this.totales.tcolor += this.dataGrid.data[i].tcolor;
        this.totales.tcons += this.dataGrid.data[i].tcons;
        this.totales.tcortesia += this.dataGrid.data[i].tcortesia;
        this.totales.tcreales += this.dataGrid.data[i].tcreales;
        this.totales.tctosob += this.dataGrid.data[i].tctosob;
        this.totales.tdispon += this.dataGrid.data[i].tdispon;
        this.totales.tfinv += this.dataGrid.data[i].tfinv;
        this.totales.tforden += this.dataGrid.data[i].tforden;
        this.totales.tfserv += this.dataGrid.data[i].tfserv;
        this.totales.tgrupo += this.dataGrid.data[i].tgrupo;
        this.totales.tlesp += this.dataGrid.data[i].tlesp;
        this.totales.tmpc += this.dataGrid.data[i].tmpc;
        this.totales.trrsrvnogar += this.dataGrid.data[i].trrsrvnogar;
        this.totales.trsrvgar += this.dataGrid.data[i].trsrvgar;
        this.totales.tsalidas += this.dataGrid.data[i].tsalidas;
        this.totales.tsec += this.dataGrid.data[i].tsec;
        this.totales.ttotalctos += this.dataGrid.data[i].ttotalctos;
        this.totales.ttshare += this.dataGrid.data[i].ttshare;
        this.totales.tucfinv += this.dataGrid.data[i].tucfinv;
        this.totales.tusocasa += this.dataGrid.data[i].tusocasa;
        this.totales.tvtas += this.dataGrid.data[i].tvtas;
        this.totales.tdisp += this.dataGrid.data[i].tdispon;
      }

      this.dataGrid.data[this.dataGrid.total] = this.totales;

      this.loading = false;
    });
  }

  public getColor(porcentaje: any) {
    let color: any;
    switch (true) {
      case porcentaje <= 89.99:
        color = 'green';
        break
      case (porcentaje >= 90) && (porcentaje <= 99.99):
        color = 'yellow';
        break
      case porcentaje === 100:
        color = 'red';
        break
      default:
        color = 'transparent';
        break
    }
    return color;
  }
  initialData() {
    this.ttInfocia = this._infoCia.getSessionInfocia();
    this.fechaHoy = moment(this.ttInfocia.fday).toDate();

    this.gridFields = [
      { title: 'Tipo Cto.', dataKey: 'tcuarto', width: '80', filtro: true },
      { title: 'T.Ctos', dataKey: 'ttotalctos', width: '60', filtro: false },
      { title: 'F/I', dataKey: 'tfinv', width: '50', filtro: false },
      { title: 'F/O', dataKey: 'tforden', width: '50', filtro: false },
      { title: 'Total.', dataKey: 'tdispon', width: '50', filtro: false },
      { title: 'Casa.', dataKey: 'tcasa', width: '50', filtro: false },
      { title: 'R/G', dataKey: 'trsrvgar', width: '50', filtro: false },
      { title: 'RNG', dataKey: 'trrsrvnogar', width: '50', filtro: false },
      { title: 'Gpo.', dataKey: 'tgrupo', width: '50', filtro: false },
      { title: 'Cort.', dataKey: 'tcortesia', width: '50', filtro: false },
      { title: 'U.C.', dataKey: 'tusocasa', width: '50', filtro: false },
      { title: 'T.S.', dataKey: 'ttshare', width: '50', filtro: false },
      { title: 'F/S.', dataKey: 'tfserv', width: '50', filtro: false },
      { title: 'C.Vta.', dataKey: 'tvtas', width: '50', filtro: false },
      { title: 'Casa FI', dataKey: 'tucfinv', width: '60', filtro: false },
      { title: 'L.E.', dataKey: 'tlesp', width: '50', filtro: false },
    ];
    this.gridGraf = [
      // { title: 'Fecha', dataKey: 'tfecha', width: '', filtro: false },
      { title: 'Día', dataKey: 'tdia', width: '70', filtro: false },
      { title: '% Ocupación Esperada', dataKey: 'tporc', width: '130', filtro: false },
      { title: 'Restricción', dataKey: 'tcodrest', width: '70', filtro: false },
      { title: 'Venta', dataKey: 'tdispventa', width: '70', filtro: false },
      { title: 'F.C.', dataKey: 'tforecast', width: '70', filtro: false },
      { title: 'N.G.', dataKey: 'tnogar', width: '70', filtro: false },
      { title: 'GRP', dataKey: 'tgrupo', width: '70', filtro: false },
      { title: 'F.S.', dataKey: 'tfserv', width: '70', filtro: false },
      { title: 'L.E.', dataKey: 'tlesp', width: '70', filtro: false },
    ]
    this._crudservice.getData('/manttos/ingagrp')
      .map(data => data = data.siIngagrp.dsIngagrp.ttIngagrp).subscribe(data => {
        this.dsIngagrp = data;
      });
    this.formGrup();
    this.distForm();
    this.getDataSource(this.fechaHoy);
  }
}
