import { Component, Input, OnChanges, OnInit, SimpleChanges, TemplateRef, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TblReservacion } from '../../../models/tbl-reservacion';
import { GLOBAL } from '../../../services/global';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TblGeneral } from 'app/models/tgeneral';
import { InfociaGlobalService } from 'app/services/infocia.service';
import moment from 'moment';

const httpOptions = GLOBAL.httpOptions;

@Component({
  selector: 'app-cambios-reservaciones',
  templateUrl: './cambios-reservaciones.component.html',
  styleUrls: ['./cambios-reservaciones.component.css']
})
export class CambiosReservacionesComponent implements OnInit, OnChanges {
  @ViewChild('modalHelp') modalHelp: TemplateRef<any>;
  @Input() folio: TblReservacion;
  api: string = GLOBAL.url + '/consultas/varias/Cambios' + GLOBAL.char174 + 'R';
  ttGeneral: TblGeneral;
  ngForm: FormGroup;
  dataGrid: any = [];
  dsBotones: any = [];
  dataBotones = [
    { name: 'Cuartos', module: 'R', code: 'Cuartos' },
    { name: 'Depositos', module: 'R', code: 'Depositos' },
    { name: 'Tarifas', module: 'R', code: 'Tarifas' },
    { name: 'Requerimientos', module: 'R', code: 'ReqEsp' },
    { name: 'Cuentas', module: 'R', code: 'Cuentas' },
    { name: 'Cargos', module: 'R', code: 'Cargos' },
  ];
  noRecords = GLOBAL.noRecords;
  formatKendo: string = localStorage.getItem('formatKendo');
  formatMoment: string = localStorage.getItem('formatMoment');
  dsTipos = [
    { desc: 'N', tipo: 'Nueva (N)' },
    { desc: 'M', tipo: 'Modificación (M)' },
    { desc: 'B', tipo: 'Borrar (B)' },
    { desc: 'C', tipo: 'Cxld (C)' },
    { desc: 'L', tipo: 'Bloqueo (B)' },
    { desc: 'S', tipo: 'No Show (S)' },
    { desc: 'R', tipo: 'Re-activada (R)' },
    { desc: 'CC', tipo: 'Visualizo Tarjeta (CC)' }
  ];
  dataItem: any;
  dsCuentas = ['A', 'B', 'C', 'D', 'E', 'F'];
  loadHelps: string;
  dataTarifas: any = [];
  dataReqEsp: any = [];
  dataDepositos: any = [];
  dataCuartos: any = [];
  dataCuentas: any = [];
  dataCargos: any = [];
  ttInfocia: any;
  maxAdultos: any;
  arrTarifas = [
    'Sencillo',
    'Doble',
    'Triple',
    'Cuádruple',
    'Quíntuple',
    'Séxtuple',
    'Séptuple',
    'Óctuple',
    'Nónuplo',
    'Décuplo',
  ];
  arrTariCols: any = [];
  loading: boolean = false;
  dataItemCtos: any;
  dataItemDeptos: any;
  ngFormSelection: any[] = [];
  ngFormSelectionCtos: any = [];
  ngFormSelectionDeptos: any = [];

  constructor(private _http: HttpClient,
    public _info: InfociaGlobalService,
    private _formBuild: FormBuilder,
    private _modal: NgbModal,) {
    this.ngForm = this.createForm();
    this.ttInfocia = this._info.getSessionInfocia();
    this.maxAdultos = this.ttInfocia.cact;
    for (let cont = 0; cont < this.ttInfocia.cact; cont++) {
      this.arrTariCols.push(this.arrTarifas[cont]);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['folio'] && changes['folio']['currentValue']) {
      this.ngForm.patchValue({
        cChr1: 'Reservacion',
        iInt1: this.folio.rnreser,
        iInt2: 1,
        fFec1: null,
        fFec2: null
      });
      this.buscarCambios();
    }
  }

  ngOnInit() {
  }

  buscarCambios() {
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    const json = '{"dsCambios":{"ttTgeneral":[' + JSON.stringify(form) + ']}}';

    this.ngFormSelection = [];
    this._http.put<any>(this.api, json, httpOptions).subscribe(data => {
      const result = data.response.siCambio.dsCambios;
      this.dataGrid = result.ttCreserv;
      if (this.dataGrid.length) {
        this.dataGrid.forEach(item => {
          if (item.FechaConfirmacion) {
            item.FechaConfirmacion = moment(item.FechaConfirmacion).format(this.formatMoment)
          }
        });
        this.ngFormSelection.push(this.dataGrid[0].HoraMov);
        this.dataItem = this.dataGrid[0];
      }
      this.dsBotones = result.Botones[0];
    });
  }

  changeFolio(folio) {

  }

  createForm() {
    return this._formBuild.group(new TblGeneral());
  }

  selectedRow({ selectedRows }) {
    this.dataItem = selectedRows[0].dataItem;
  }

  btnSelected(boton) {
    const form = JSON.parse(JSON.stringify(this.ngForm.value));

    if (boton === 'ReservacionEx') {
      form.cChr1 = boton;
    } else {
      this.loading = true;
      this._modal.open(this.modalHelp, { size: 'xl', centered: true });
      this.loadHelps = boton.name;
      form.cChr1 = boton.code;
    }

    const json = '{"dsCambios":{"ttTgeneral":[' + JSON.stringify(form) + ']}}';
    this.ngFormSelection = [];
    this._http.put<any>(this.api, json, httpOptions).subscribe(data => {
      const result = data.response.siCambio.dsCambios;
      switch (boton.name) {
        case 'Cuartos':
          this.dataCuartos = result.ttCresctos;
          this.ngFormSelectionCtos.push(this.dataDepositos[0]);
          this.dataCuartos.forEach(item => {
            if (item.FechaEntrada) {
              item.FechaEntrada = moment(item.FechaEntrada).format(this.formatMoment);
            }
            if (item.FechaSalida) {
              item.FechaSalida = moment(item.FechaSalida).format(this.formatMoment);
            }
          });
          break;
        case 'Depositos':
          this.dataDepositos = result.ttCdepositos;
          this.ngFormSelectionDeptos.push(this.dataDepositos[0]);
          break;
        case 'Tarifas':
          this.dataTarifas = result.ttCrestar;
          break;
        case 'Requerimientos':
          this.dataReqEsp = result.ttCresreq;
          break;
        case 'Cuentas':
          this.dataCuentas = result.ttCresfol;
          break;
        case 'Cargos':
          this.dataCargos = result.ttCrescar;
          break;
        default:
          const file = result.RespuestaExcel;
          if (file && file[0].ArchivoExportado) {
            const linkSource = 'data:application/vnd.ms-excel;base64,' + file[0].ArchivoExportado;
            const download = document.createElement('a');
            download.href = linkSource;
            download.download = file[0].Archivo;
            download.click();
          }
          break;

      }
      this.loading = false;
    });

  }
  selectionDeptos({ selectedRows }) {
    this.dataItemDeptos = selectedRows[0].dataItem;
  }
  selectioneCtos({ selectedRows }) {
    this.dataItemCtos = selectedRows[0].dataItem;
  }
  exportarExcel() {
  }
}
