// Imports Angular
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
// Modules
import {ReservacionModule} from '../reservaciones/reservacion/reservacion.module';
import {ArchitectModule} from '../../_architect/architect.module';
import {HuespedModule} from '../recepcion/huesped/huesped.module';
import {SharedAppsModule} from '../../shared/shared-apps.module';
import {KendouiModule} from '../../shared/kendo.module';
// Services
import {CuartosBloqueadosServicesService} from './cuartos-bloqueados/cuartos-bloqueados-services.service';
import {HistoricoClientesServices} from './historico-clientes/historico-clientes.service';
import {RackVisualServicesService} from './rack-visual/rack-visual-services.service';
import {AgenciasService} from './agencias/agencias.service';
// Componentes
import {CuartosBloqueadosComponent} from './cuartos-bloqueados/cuartos-bloqueados.component';
import {DisponibilidadComponent} from './disponibilidad/disponibilidad.component';
import {RackVisualComponent} from './rack-visual/rack-visual.component';
import {AgenciasComponent} from './agencias/agencias.component';
import { ConsultaBandasComponent } from './consulta-bandas/consulta-bandas.component';
import { ConsultaAllotmentComponent } from './consulta-allotment/consulta-allotment.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    KendouiModule,
    SharedAppsModule,
    RouterModule,
    ArchitectModule,
    HuespedModule,
    ReservacionModule
  ],
  declarations: [
    DisponibilidadComponent,
    CuartosBloqueadosComponent,
    RackVisualComponent,
    AgenciasComponent,
    ConsultaBandasComponent,
    ConsultaAllotmentComponent,
  ],
  providers: [
    HistoricoClientesServices,
    CuartosBloqueadosServicesService,
    RackVisualServicesService,
    AgenciasService
  ],
  exports: [
    DisponibilidadComponent,
  ]
})
export class ConsultasModule {
}
