import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { TblGeneral } from '../../../../models/tgeneral';
import { GLOBAL } from '../../../../services/global';
import { InterfaService } from '../interface.service'
import moment from 'moment';

@Component({
  selector: 'app-programa-lealtad',
  templateUrl: './programa-lealtad.component.html',
})
export class ProgramaLealtadComponent implements OnInit {

  heading = 'Programa de Lealtad';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  dataGlobal: any = [];
  filter: CompositeFilterDescriptor;
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  ttUsuario: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  loading: boolean;
  base64 = '';
  base = '';
  flagEdit: boolean;
  selectedItem: any = [];
  api: Array<string> = ['/parametros/parametros/Lealtad', 'dsRepparametros', 'ttTgeneral'];
  toast = GLOBAL.toast;
  swal = GLOBAL.swal;
  tblGeneral: TblGeneral;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: InterfaService,
    private _http: HttpClient,
    ) {
      this.ttInfocia = this._info.getSessionInfocia();
      this.ttUsuario = this._info.getSessionUser();
      this.crudTable();
      this.ngForm = this.createForm();
    }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  crudTable(){
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.iInt1 = 1;
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).subtract(1, 'days').toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).toDate();
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }

  public getInfo(){
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
    temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);
    this._https.putInfo(temp, this.api[0], this.api[1], this.api[2], GLOBAL.httpOptions)
    .subscribe(data => {
      const result = data.siRepparametro.dsRepparametros;
      const resultExcel = data.siRepparametro.dsRepparametros.RespuestaExcel;
      if (!result)  {
        this.dataGrid = [];
        this.dataGlobal = [];
      }else{
        if (!result.ttTgeneral){
        this.dataGrid = result.ProgramaLealtad;
        this.dataGlobal = result.TotalPlealtad;
        this.base64 = resultExcel[0].Archivo;
        this.base = resultExcel;
        }else{
          const errorMesj = data.siRepparametro.dsMensajes.ttMensError[0].cMensTxt;
            this.swal('ERROR!', errorMesj, 'error');
        }
      }
        this.loading = false;
      });
  }

  public exportExcel() {
    this.loading = true;
    if (this.base) {
      this._http.get<any>(GLOBAL.url + '/reportes/excel/' + this.base64)
      .subscribe(res => {
        const result = res.response.siEnvioExcel.dsEnvioExcel.RespuestaExcel[0];
        const fileExcel = result.ArchivoExportado;
        const linkSource = 'data:application/vnd.ms-excel;base64,' + fileExcel;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = result.Archivo;
        download.click();
        this.loading = false;
      });
    }
  }

  exportPdf() {
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(this.formatMoment);
    form.fFec2 = moment(form.fFec2).format(this.formatMoment);
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: '009-PROGRAM-LEAL',
      dataGrid: this.dataGrid,
      dataGlobal: this.dataGlobal,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      hora: moment().format('HH:mm:ss'),
      form: form,
    }
    this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = this.heading + '.pdf';
        download.click();
      },
      error => {
        this.swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
      }
    );
  }

}
