import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { InterfaService } from '../interface.service'
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { ManttoMenuService } from 'app/pms/manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../../services/global';
import { CrudService } from 'app/pms/mantto-srv.services';
import { TblGeneral } from 'app/models/tgeneral';

@Component({
  selector: 'app-interfaces-check-in-out',
  templateUrl: './interfaces-check-in-out.component.html',
})
export class InterfacesCheckInOutComponent implements OnInit {

  heading = 'Interfaces';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  ngForm: FormGroup;
  modalTitle = '';
  modalType = '';
  closeResult: string;
  nombre = '';
  extension = '';
  toast = GLOBAL.toast;
  tblGeneral: TblGeneral;


  constructor(
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private formBuild: FormBuilder,
    private _https: InterfaService,
    private modal: NgbModal,
    private _crud: CrudService,
    ) { this.crudTable();
      this.ngForm = this.createForm();

    }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  crudTable(){
    this.tblGeneral = new TblGeneral();
  }

  createForm() {
    return this.formBuild.group(this.tblGeneral);
  }


  openModal(content, type: string) {
    switch (type) {
      case 'allCrts':
        this.modalTitle = 'Cuartos';
        this.modalType = 'allCrts';
        break;
    }

    this.modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getDataModal(data, type: string){
    switch (type){
      case 'allCrts':
        this.ngForm.patchValue({
          cChr3: data.cuarto,
        });
        this._crud.getData('/ayuda/telefonos/cuarto®' + this.ngForm.value.cChr3).subscribe(data => {
          const result = data.siSistint.dsSistint.Cuartos[0];
          if (!result)  {
            console.log('Algo está mal');
          }else{
            this.nombre = result.Nombre;
            this.extension = result.Extension;
          }
          });
      break;
    }
  }

  crudService (tipo: any){
    if (!this.ngForm.value.lLog1){
      this.ngForm.patchValue({
        cChr3: '',
      });
    }
    this.ngForm.patchValue({
      cChr1: tipo,
    });
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    this._https.putInfo(temp, '/parametros/sistint/InterfaceCiCo', 'dsSistint', 'ttTgeneral', GLOBAL.httpOptions)
      .subscribe(data => {
        if (!data.siReptelefonos.dsMensajes.ttMensError){
          if (this.ngForm.value.cChr1 === 'I'){
            this.toast({
              type: 'success',
              title: 'Check-In Realizado!!',
            });
          }else if (this.ngForm.value.cChr1 === 'O' || this.ngForm.value.cChr1 === 'V'){
            this.toast({
              type: 'success',
              title: 'Check-Out Realizado!!',
            });
          }

        }else {
          // const errorMesj = data.siReptelefonos.dsMensajes.ttMensError[0].cMensTxt;
            swal('ERROR!', 'Interface no existe!!', 'error');
        }
      });
  }

}
