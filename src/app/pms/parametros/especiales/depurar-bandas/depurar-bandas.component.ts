import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { ManttoMenuService } from 'app/pms/manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../../services/global';
import { CrudService } from 'app/pms/mantto-srv.services';
import { InfociaGlobalService } from 'app/services/infocia.service';
import moment from 'moment';
import { HttpClient } from '@angular/common/http';
import { InterfaService } from '../../Interfaces/interface.service';
import { TblGeneral } from 'app/models/tgeneral';

@Component({
  selector: 'app-depurar-bandas',
  templateUrl: './depurar-bandas.component.html',
})
export class DepurarBandasComponent implements OnInit {

  heading = 'Depuración de Bandas';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  ngForm: FormGroup;
  modalTitle = '';
  modalType = '';
  noRecords = GLOBAL.noRecords;
  closeResult: string;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  listBrazalete = '';
  api: Array<string> = ['/parametros/parametros/DepBrazaletes', 'dsRepparametros', 'ttTgeneral'];
  loading = false;
  toast = GLOBAL.toast;
  tblGeneral: TblGeneral;


  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private formBuild: FormBuilder,
    private _https: InterfaService,
    private modal: NgbModal,
    private http: HttpClient,
    private _crud: CrudService,
    ) { this.crudTable();
      this.ngForm = this.createForm();
      this.ttInfocia = _info.getSessionInfocia();
    }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  crudTable(){
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.cChr1 = 'N';
    this.tblGeneral.iInt1 = [0, [Validators.required]];
    this.tblGeneral.iInt2 = [0, [Validators.required]];
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).subtract(1, 'days').toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).toDate();
  }

  createForm() {
    return this.formBuild.group(this.tblGeneral);
  }

  openModal(content, type: string) {
    switch (type) {
      case 'banda':
        this.modalTitle = 'Tipos de Banda';
        this.modalType = 'banda';
        break;
    }

    this.modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getDataModal(data, type: string){
    switch (type){
      case 'banda':
        this.ngForm.patchValue({
          cChr2: data.tbanda,
        });
        break;
    }
  }

  public procesar(){
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    this._https.putInfo(temp, this.api[0], this.api[1], this.api[2], GLOBAL.httpOptions).subscribe(data => {
      const result = data.siRepparametro.dsMensajes.ttMensError;
      if (result)  {
        swal('ERROR!', result[0].cMensTxt, 'error');
      }else{
        this.toast({
          type: 'success',
          title: 'Proceso Realizado!!',
        });
      }
        this.loading = false;
      });
  }


}
