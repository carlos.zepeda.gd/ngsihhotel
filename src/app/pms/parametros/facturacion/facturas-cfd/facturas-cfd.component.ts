import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor, filterBy } from '@progress/kendo-data-query';
import { GLOBAL } from 'app/services/global';
import { InfociaGlobalService } from 'app/services/infocia.service';
import { ManttoMenuService } from 'app/pms/manttos/mantto-menu/mantto-menu.service';
import { Router } from '@angular/router';
import { FacturasCFDService } from './facturas-cfd.services'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import moment from 'moment';

@Component({
  selector: 'app-facturas-cfd',
  templateUrl: './facturas-cfd.component.html',
  styleUrls: ['./facturas-cfd.component.css']
})
export class FacturasCFDComponent implements OnInit {
  heading = 'Facturación CFD 3.3';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  dsRfc: any = [];
  filter: CompositeFilterDescriptor;
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = '';
  Month = '';
  subMonth = '';
  api: Array<string> = ['/reportes/general/ifactCFD', 'dsReporte', 'ttTgeneral'];
  loading: boolean;
  modalTitle = '';
  modalType = '';
  closeResult: string;
  toast = GLOBAL.toast;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _facturas: FacturasCFDService,
    private _modal: NgbModal) {
    this.ngForm = this.createForm();
    this.formatKendo = localStorage.getItem('formatKendo');
    this.ttInfocia = this._info.getSessionInfocia();
    this.dataFilter = filterBy([this.dataGrid], this.filter);
  }

  ngOnInit() {
    this.createForm();
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  createForm() {
    return this._formBuild.group({
      cChr1: ['3.3'],
      cChr2: [''],
      cChr3: [''],
      cChr4: [''],
      cChr5: [''],
      cChr6: [''],
      cChr7: [''],
      cChr8: [''],
      cChr9: [''],
      cChr10: [''],
      cChr11: [''],
      iInt1: [1],
      iInt2: [0],
      iInt3: [0],
      iInt4: [0],
      iInt5: [0],
      iInt6: [0],
      iInt7: [0],
      iInt8: [0],
      iInt9: [0],
      iInt10: [0],
      iInt11: [0],
      lLog1: [false],
      lLog2: [false],
      lLog3: [false],
      lLog4: [false],
      lLog5: [false],
      lLog6: [false],
      lLog7: [false],
      lLog8: [false],
      lLog9: [false],
      lLog10: [false],
      lLog11: [false],
      dDec1: [0.0],
      dDec2: [0.0],
      dDec3: [0.0],
      dDec4: [0.0],
      dDec5: [0.0],
      dDec6: [0.0],
      dDec7: [0.0],
      dDec8: [0.0],
      dDec9: [0.0],
      dDec10: [0.0],
      dDec11: [0.0],
      fFec1: [moment(this.ttInfocia.fday).toDate(), Validators.required],
      fFec2: [moment(this.ttInfocia.fday).toDate(), Validators.required],
      fFec3: [null],
      fFec4: [null],
      fFec5: [null],
      fFec6: [null],
      fFec7: [null],
      fFec8: [null],
      fFec9: [null],
      fFec10: [null],
      fFec11: [null],
      zDtz1: [null],
      zDtz2: [null],
      zDtz3: [null],
      zDtz4: [null],
      zDtz5: [null],
      zDtz6: [null],
      zDtz7: [null],
      zDtz8: [null],
      zDtz9: [null],
      zDtz10: [null],
      zDtz11: [null]
    });
  }

  getMonth() {
    this.Month = this.ngForm.value.fFec1
    this.subMonth = String(this.Month).substring(4, 7);
  }

  updateMonth() {
    this.getMonth();
  }


  buscarInfo() {
    this.loading = true;

    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso); // todo YYYY-MM-DD
    temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso); // todo YYYY-MM-DD

    this._facturas.putInfo(temp, '/reportes/general/ifactCFD', 'dsReporte', 'ttTgeneral', GLOBAL.httpOptions)
      .subscribe(res => {

        if (!res.siReporte.dsReporte.ttFact) {
          this.dataGrid = [];
          this.toast({ type: 'info', title: 'No se encontraron registros.' });
        } else {
          this.dataGrid = res.siReporte.dsReporte.ttFact;
        }
        this.loading = false;
      });

  }

  openModal(content, type: string) {
    switch (type) {
      case 'rfc':
        this.modalTitle = 'RFC de Compañías';
        this.modalType = 'rfc';
        break;
    }
    this._modal.open(content, { size: 'lg' });
  }

  getDataModal(data, type: string) {
    switch (type) {
      case 'rfc':
        this.ngForm.patchValue({ cChr4: data.rfc });
        break;
    }
  }
}
