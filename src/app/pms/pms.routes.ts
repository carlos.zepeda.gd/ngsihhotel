import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../components/home/home.component';
//  Architect **start
import { VerifyInfociaGuardService } from '../services/verify-infocia-guard.service';
import { BaseLayoutComponent } from '../_architect/Layout/base-layout/base-layout.component';
// Consultas
import { DisponibilidadComponent } from './consultas/disponibilidad/disponibilidad.component';
import { CuartosBloqueadosComponent } from './consultas/cuartos-bloqueados/cuartos-bloqueados.component';
import { RackVisualComponent } from './consultas/rack-visual/rack-visual.component';
import { ConsultaBandasComponent } from './consultas/consulta-bandas/consulta-bandas.component';
import { ConsultaAllotmentComponent } from './consultas/consulta-allotment/consulta-allotment.component';
import { PerfilUserComponent } from 'app/components/perfil-user/perfil-user.component';
import { AuthGuard } from 'app/services/auth-guard.service';
import { RoutesGuard } from 'app/services/routes-guard.service';

const PMS_ROUTES: Routes = [
  {
    path: '',
    component: BaseLayoutComponent,
    children: [
      { path: 'home', component: HomeComponent, data: { extraParameter: '' } },
      { path: 'perfil', component: PerfilUserComponent, data: { extraParameter: '' } },
      { path: 'reservaciones', loadChildren: './reservaciones/reservaciones.module#ReservacionesModule' },
      { path: 'recepcion', loadChildren: './recepcion/recepcion.module#RecepcionModule' },
      { path: 'caja', loadChildren: './caja/caja.module#CajaModule' },
      { path: 'auditoria', loadChildren: './auditoria/auditoria.module#AuditoriaModule' },
      { path: 'llaves', loadChildren: './ama-llaves/amallaves.module#AmallavesModule' },
      { path: 'reportes', loadChildren: './reportes/reportes.module#ReportesModule' },
      { path: 'mantto', loadChildren: './manttos/manttos.module#ManttosModule' },
      { path: 'parametros', loadChildren: './parametros/parametros.module#ParametrosModule' },
      // Consultas
      { path: 'disponibilidad', component: DisponibilidadComponent, data: { extraParameter: '' } },
      { path: 'cuartos-bloqueados', component: CuartosBloqueadosComponent, data: { extraParameter: '' } },
      { path: 'rack-visual', component: RackVisualComponent, data: { extraParameter: '', consulta: true } },
      { path: 'consulta-bandas', component: ConsultaBandasComponent, data: { extraParameter: '', uso: 'H' } },
      { path: 'consulta-allotment', component: ConsultaAllotmentComponent, data: { extraParameter: '' } },
    ],
    canActivateChild: [
      VerifyInfociaGuardService
    ],
    canActivate: [
      AuthGuard,
      RoutesGuard
    ]
  }
];

export const PMS_ROUTING = RouterModule.forChild(PMS_ROUTES);
