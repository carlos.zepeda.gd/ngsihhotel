import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { ReservacionesService } from '../reservaciones.service';
import { TblGeneral } from '../../../models/tgeneral';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
})
export class ForecastComponent implements OnInit {
  heading = 'Forecast';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  ngForm: FormGroup;
  modalTitle = '';
  modalType = '';
  noRecords = GLOBAL.noRecords;
  ttInfocia: any = [];
  minDateDesde: Date;
  dataGrid: any = [];
  dataColumns: any = [];
  dataFechas: any = [];
  pageSize = 10;
  skip = 0;
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  filter: CompositeFilterDescriptor;
  api: Array<string> = ['/manttos/forecast/', 'dsAllotdisp', 'ttTgeneral'];
  loading = false;
  toast = GLOBAL.toast;
  tblGeneral: TblGeneral;
  dsTiposCtos: any = [];
  fechaTemp: any;
  contadorSearch = 0;
  agencia: any;
  tCuarto: any;
  fecha: any;
  forecast: any;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: ReservacionesService,
    private _modal: NgbModal
  ) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.crudTable();
    this.ngForm = this.createForm();
    this.ngForm.get('cChr1').setValidators([Validators.required])
    this.fechaTemp = moment(this.ttInfocia.fday).toDate();
    this.minDateDesde = new Date(moment(this.ngForm.value.fFec1).toDate());
    this._https.getData('/ayuda/tcuartos').subscribe(data => {
      this.dsTiposCtos = data.siAyuda.dsAyuda.ttTcuartos;
      this.dsTiposCtos.forEach(item => item.tdesc = item.tdesc + ' - ' + item.tcuarto);
    });
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  crudTable() {
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).toDate();
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }

  openModal(content, type: string) {
    switch (type) {
      case 'agencia':
        this.modalTitle = 'Consulta de Agencias';
        this.modalType = 'agencia';
        break;
      case 'forecast':
        this.modalTitle = 'Forecast';
        this.modalType = 'forecast';
        break;
    }
    this._modal.open(content, { size: 'lg' });
  }

  getDataModal(data) {
    this.ngForm.patchValue({ cChr1: data.Agencia });
  }

  public getInfo(research: string) {
    this.loading = true;
    switch (research) {
      case 'menos':
        if (this.contadorSearch > 0) {
          this.fechaTemp = moment(this.fechaTemp).subtract(10, 'days');
          this.contadorSearch--;
        }
        break;
      case 'mas':
        this.fechaTemp = moment(this.fechaTemp).add(10, 'days');
        this.contadorSearch++;
        break;
      case 'igual':
        this.contadorSearch = 0;
        this.fechaTemp = moment(this.ngForm.value.fFec1);
        break;
    }

    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.fFec1 = moment(this.fechaTemp).format(GLOBAL.formatIso);
    temp.fFec2 = moment(this.fechaTemp).add(9, 'days').format(GLOBAL.formatIso);

    this._https.putInfo(temp, this.api[0] + this.ngForm.value.cChr1, this.api[1], this.api[2], GLOBAL.httpOptions)
      .subscribe(data => {
        const result = data.siAllotdisp.dsAllotdisp;
        if (!result.ttAllotAgencia) {
          this.dataGrid = [];
          this.dataFechas = [];
          this.dataColumns = [];
        } else {
          this.dataGrid = result.ttAllotAgencia;
          this.dataFechas = result.ttAllotFecha;
          this.dataColumns = this.dataGrid[0].ttAllotdisp;
        }
        this.loading = false;
      });
  }


  public cellClickHandler({ rowIndex, columnIndex, dataItem }) {
    this.agencia = dataItem.Agencia;
    this.tCuarto = dataItem.TipoCuarto;
    this.fecha = this.dataFechas[columnIndex - 2].Fecha;
    this.forecast = dataItem.ttAllotdisp[columnIndex - 2].Forecast;

  }

  public saveInfo() {
    const temp = {
      Agencia: this.agencia,
      TipoCuarto: this.tCuarto,
      Fecha: moment(this.fecha).format(GLOBAL.formatIso),
      Forecast: this.forecast,
      cRorID: '',
      lError: false,
      cErrDesc: ''
    }

    this._https.postInfo(temp, this.api[0] + this.ngForm.value.cChr1, this.api[1], 'ttAllotdisp')
      .subscribe(data => {
        const resultError = data.siAllotdisp.dsMensajes;
        if (resultError.ttMensError) {
          this.toast({ title: 'Error', type: 'warning', text: 'Algo está mal!!' });
        } else {
          this.toast({ type: 'success', text: 'Forecas modificado!!' });
          this.getInfo('igual');
        }
      });
  }
}
