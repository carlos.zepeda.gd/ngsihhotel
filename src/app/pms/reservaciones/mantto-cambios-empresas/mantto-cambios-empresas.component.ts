import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { ReservacionesService } from '../reservaciones.service';
import { TblGeneral } from '../../../models/tgeneral';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-mantto-cambios-empresas',
  templateUrl: './mantto-cambios-empresas.component.html',
})
export class ManttoCambiosEmpresasComponent implements OnInit {

  heading = 'Cambios a Empresas';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  ngForm: FormGroup;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  maxDateHasta: Date;
  maxDateDesde: Date;
  minDateHasta: Date;
  filter: CompositeFilterDescriptor;
  api: Array<string> = ['/reportes/Reservaciones/CambiosAEA', 'dsRepreserv', 'ttTgeneral'];
  loading = false;
  listTipos: [];
  selectedItem: any = [];
  tblGeneral: TblGeneral;
  nameExcel: any;


  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: ReservacionesService,
    private _http: HttpClient,
    private _modal: NgbModal,
  ) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.crudTable();
    this.ngForm = this.createForm();
    this.ngForm.get('cChr2').setValidators([Validators.required]);
    this.maxDateHasta = new Date(moment(this.ttInfocia.fday).toDate());
    this.minDateHasta = new Date(moment(this.ttInfocia.fday).toDate());
    this.maxDateDesde = new Date(moment(this.ttInfocia.fday).subtract(1, 'day').toDate());
    this._https.getData('/manttos/empresas').map(data => data).subscribe(data => {
      if (data) {
        this.listTipos = data.siEmpresa.dsEmpresas.ttEmpresas;
      }
    });
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  crudTable() {
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.cChr1 = 'E';
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).subtract(1, 'days').toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).toDate();
    this.tblGeneral.lLog1 = true;
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }

  public buscarInfo() {
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
    temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);
    this._https.postInfo(temp, this.api[0], this.api[1], this.api[2]).subscribe(data => {
      const result = data.siRepreserv.dsRepreserv;
      if (!result.CambiosAgencias) {
        this.dataGrid = [];
      } else {
        this.dataGrid = result.CambiosEmpresas;
        this.nameExcel = data.siRepreserv.dsEstadAgen.RespuestaExcel;
      }
      this.loading = false;
    });
  }

  public exportExcel() {
    if (this.nameExcel) {
      this._http.get<any>(GLOBAL.url + '/reportes/excel/' + this.nameExcel[0].Archivo).subscribe(res => {
        const result = res.response.siEnvioExcel.dsEnvioExcel.RespuestaExcel;
        if (result && result[0]) {
          const linkSource = 'data:application/vnd.ms-excel;base64,' + result[0].ArchivoExportado;
          const download = document.createElement('a');
          download.href = linkSource;
          download.download = result[0].Archivo;
          download.click();
        }
        this.loading = false;
      });
    }
  }

  fechaDesdeChange() {
    this.minDateHasta = new Date(moment(this.ngForm.value.fFec1).add(1, 'day').toDate());
  }

  fechaHastaChange() {
    this.maxDateDesde = new Date(moment(this.ngForm.value.fFec2).subtract(1, 'day').toDate());
  }

  openModal(content, dataItem) {
    this.selectedItem = dataItem;
    this._modal.open(content, { size: 'lg' });
  }

}
