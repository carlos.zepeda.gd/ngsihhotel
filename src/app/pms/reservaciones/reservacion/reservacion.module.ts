import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Modules
import { KendouiModule } from '../../../shared/kendo.module';
import { SharedAppsModule } from '../../../shared/shared-apps.module';
// SERVICES
import { ReservacionesService } from '../reservaciones.service';
// COMPONENTES
import { ReservacionComponent } from '../reservacion/reservacion.component';
import { FormReservacionComponent } from '../reservacion/components/form-reservacion/form-reservacion.component';
import { ActionReservacionComponent } from '../reservacion/components/action-reservacion/action-reservacion.component';
import { DepositosComponent } from '../reservacion/subcomponents/depositos/depositos.component';
import { ReservacionService } from './services/reservacion.service';
import { CuentasReservService } from './services/cuentas.service';
import { DepositosService } from './services/depositos.service';
import { SolicitudService } from './services/solicitud.service';
import { HistoricoClientesServices } from 'app/pms/consultas/historico-clientes/historico-clientes.service';
import { RoomingListComponent } from './subcomponents/rooming-list/rooming-list.component';
import { SolicitudDepositoComponent } from '../solicitud-deposito/solicitud-deposito.component';
import { ArchitectModule } from 'app/_architect/architect.module';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { TarifasRsrvComponent } from './subcomponents/tarifas-rsrv/tarifas-rsrv.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    KendouiModule,
    SharedAppsModule,
    ArchitectModule,
  ],
  declarations: [
    ReservacionComponent,
    FormReservacionComponent,
    ActionReservacionComponent,
    DepositosComponent,
    RoomingListComponent,
    SolicitudDepositoComponent,
    TarifasRsrvComponent
  ],
  exports: [
    ReservacionComponent,
    FormReservacionComponent,
    ActionReservacionComponent,
    DepositosComponent,
    RoomingListComponent,
    SolicitudDepositoComponent,
    TarifasRsrvComponent
  ],
  providers: [
    ReservacionesService,
    ReservacionService,
    CuentasReservService,
    DepositosService,
    SolicitudService,
    HistoricoClientesServices,
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { showError: true }
    }
  ]
})
export class ReservacionModule { }
