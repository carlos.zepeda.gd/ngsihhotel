export class Solicitud {
    constructor(
        public iNreser: number,
        public iSecuencia: number,
        public fFecha: string,
        public dMonto: number,
        public cMon: string,
        public cNotas: string,
        public cOpe: string,
        public cCancel: string,
        public cRowID: string,
        public lError: boolean = false,
        public cErrDes: string = ''
    ) {}
}
