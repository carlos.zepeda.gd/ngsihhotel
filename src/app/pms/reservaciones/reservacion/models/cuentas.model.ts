export class Cuenta {
    constructor(
        public cErrDes = '',
        public cRowID: string,
        public hclase: string,
        public hdesc: string,
        public hfolio: number,
        public htfola: number,
        public htfolb: number,
        public htfolc: number,
        public htfolio: string,
        public lError = '?'
    ) {}
}
