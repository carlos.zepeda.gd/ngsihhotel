import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-reservacion',
  templateUrl: './reservacion.component.html',
  styleUrls: ['./reservacion.component.css']
})
export class ReservacionComponent implements OnInit, OnChanges {
  @Input() folio: any;
  @Input() outUso: string;
  @Input() bloqueo: any;
  @Input() consulta: boolean;
  @Output() finally = new EventEmitter;
  constructor(
  ) { }

  ngOnInit() {
  }
  finallyEvent(event) {
    this.finally.emit(event);
  }
  ngOnChanges(change: SimpleChanges) {
    if (change.folio && change.folio.currentValue) {

    }
  }
}
