import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { GLOBAL } from '../../../../services/global';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

const headers = new Headers({'Content-Type': 'application/json'});

@Injectable()
export class CuentasReservService extends BehaviorSubject<any[]> {
    public url: string;
    constructor(private http: Http) {
        super([]);
        this.url = GLOBAL.url;
    }
    getCargos(folio: number) {
        return this.http.get( this.url + '/rsrv/reserv/' + folio + '/f')
                        .map( (res: Response) => { 
                          const r = res.json().response.siReservacion;
                          if (r.dsMensajes.ttMensError) {
                            return r.dsMensajes.ttMensError[0];
                          }else {
                            return r.dsReservaciones.ttHuesfol;
                          }
                        });
    }

    putCuentas(cta) {
        const json = JSON.stringify(cta);
        const params = '{"ttHuesfol":[' + json + ']}';

        return this.http.put( this.url + '/rec/huesfol/M', params, { headers: headers })
                        .map( (res: Response) => {
                            const success = res.json().response.siHuesfol.dsHuesfol.ttHuesfol;
                            const error = res.json().response.siHuesfol.dsMensajes.ttMensError;
                            if (error) {
                              return error;
                            }else {
                              return success;
                            }
                        });
    }
}
