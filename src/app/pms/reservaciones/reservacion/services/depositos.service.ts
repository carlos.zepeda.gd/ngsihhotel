import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { GLOBAL } from '../../../../services/global';

@Injectable()
export class DepositosService {

  private url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }
  postSolicitud(data, tipo) {
    const json = JSON.stringify(data);
    const params = '{"ttDepsol":[' + json + ']}';
    const headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(this.url + '/rsrv/solicitud/' + tipo, params, { headers: headers })
      .map((res: Response) => {
        const r = res.json().response.siReservacion;
        if (r.dsMensajes.ttMensError) {
          return r.dsMensajes.ttMensError[0];
        } else {
          return r.dsReservaciones.ttDepsol;
        }
      });
  }
  postDepositos(data, tipo) {
    const json = JSON.stringify(data);
    const params = '{"ttDepositos":[' + json + ']}';
    const headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(this.url + '/rsrv/depositos/' + tipo, params, { headers: headers })
      .map((res: Response) => {
        const r = res.json().response.siReservacion;
        if (r.dsMensajes.ttMensError) {
          return r.dsMensajes.ttMensError[0];
        } else {
          return r.dsReservaciones.ttDepositos;
        }
      });
  }
  public getDepositos(folio: number) {
    return this.http.get(this.url + '/rsrv/reserv/' + folio + '/d')
      .map((res: Response) => {
        const r = res.json().response.siReservacion;
        if (r.dsMensajes.ttMensError) {
          return r.dsMensajes.ttMensError[0];
        } else {
          return r.dsReservaciones;
        }
      });
  }
  public getAllDepositos(folio: number) {
    return this.http.get(this.url + '/rsrv/reserv/' + folio + '/d')
      .map((res: Response) => {
        const r = res.json().response.siReservacion;
        if (r.dsMensajes.ttMensError) {
          return r.dsMensajes.ttMensError[0];
        } else {
          return r.dsReservaciones;
        }
      });
  }
  public getAllSolicitud(folio: number) {
    return this.http.get(this.url + '/rsrv/reserv/' + folio + '/d')
      .map((res: Response) => {
        const r = res.json().response.siReservacion;
        if (r.dsMensajes.ttMensError) {
          return r.dsMensajes.ttMensError[0];
        } else {
          return r.dsReservaciones;
        }
      });
  }
  public getSolicitud(folio: number) {
    return this.http.get(this.url + '/rsrv/reserv/' + folio + '/s')
      .map((res: Response) => {
        const r = res.json().response.siReservacion;
        if (r.dsMensajes.ttMensError) {
          return r.dsMensajes.ttMensError[0];
        } else {
          return r.dsReservaciones;
        }
      });
  }
}
