import { Injectable } from '@angular/core';
import { GLOBAL } from '../../../../services/global';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CuartosService {

  private url: string;
  private notify = new Subject<any>();
  notifyObservable$ = this.notify.asObservable();

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }
  public notifyOther(data: any) {

    if (data) { this.notify.next(data); }
  }
  public getSolicitudDep(folio: number) {
    return this.http.get( this.url + '/rsrv/reserv/' + folio + '/s' )
                    .map( (res: Response) => res.json().response.siReservacion.dsReservaciones.ttDepsol);
  }
  public getTarifas(folio: number, sec: number) {
    return this.http.get( this.url + '/rsrv/reserv/' + folio + '/t' + GLOBAL.char174 + sec )
                    .map( (res: Response) => res.json().response.siReservacion.dsReservaciones.ttHuestar);
  }
}
