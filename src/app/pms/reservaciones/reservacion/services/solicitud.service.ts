import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { GLOBAL } from '../../../../services/global';

@Injectable()
export class SolicitudService {

  private url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  public getSolicitudDep(folio: number) {
    return this.http.get( this.url + '/rsrv/reserv/' + folio + '/s' )
                    .map( (res: Response) => res.json().response.siReservacion.dsReservaciones.ttDepsol);
  }
  public getMonedas(folio: number) {
    return this.http.get( this.url + '/rsrv/reserv/' + folio + '/s' )
                    .map( (res: Response) => res.json().response.siReservacion.dsReservaciones.ttMonedas);
  }
}
