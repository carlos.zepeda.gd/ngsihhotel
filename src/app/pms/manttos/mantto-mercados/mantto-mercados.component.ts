import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import {InfociaGlobalService} from '../../../services/infocia.service';
import swal from 'sweetalert2';
import {CrudService} from '../mantto-srv.services';
import {PdfService} from '../mantto-pdf.service';
import {ManttoMenuService} from '../mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import { GLOBAL } from '../../../services/global';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

const flatten = GLOBAL.flatten;

@Component({
    selector: 'app-mantto-mercados',
    templateUrl: './mantto-mercados.component.html',
    styleUrls: ['./../manttos-desing.css']
})
export class ManttoMercadosComponent implements OnInit {
    heading = 'Mantenimientos de Mercados';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';

    public myFormGroup: FormGroup;
    flagEdit: boolean;
    public flagShowForm: boolean;
    public dsManuales = [];
    private ttInfocia: any;
    public checked = false;
    public dataComplete: any = [];
    public dataGrid: any = [];
    public filter: CompositeFilterDescriptor;
    public groups: GroupDescriptor[] = [];
    public sort: SortDescriptor[] = [{field: 'idesc', dir: 'asc'}];
    private urlSec: string = '/manttos/mercados';
    private ds: string = 'dsMercados';
    private tt: string = 'ttMercados';
    dataIdiomas: any[] = [];
    loading = false;
    closeResult: string;
    toast = GLOBAL.toast;

    constructor(private fb: FormBuilder,
                private _info: InfociaGlobalService,
                private _crudservice: CrudService,
                private _menuServ: ManttoMenuService,
                private _router: Router,
                private modal: NgbModal) {
        this.ttInfocia = this._info.getSessionInfocia();
        this.dsManuales = [
            {id: 'reservaciones', value: false, desc: 'Reservaciones'},
            {id: 'recepcion', value: false, desc: 'Recepción'},
        ];
        this.createForm();
    }

    ngOnInit() {
        this.getMercados();

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.loadProducts();
    }

    private loadProducts(): void {
        this.dataGrid = process(this.dataComplete, {group: this.groups});
    }

    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.dataGrid.data = filterBy(this.dataComplete, filter);
    }

    public switchChange(checked: boolean): void {
        const root = this.filter || {logic: 'and', filters: []};
        const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
        if (!filter) {
            root.filters.push({
                field: 'Inactivo',
                operator: 'eq',
                value: checked
            });
        } else {
            filter.value = checked;
        }
        this.checked = checked;
        this.filterChange(root);
    }

    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.dataStructure();
    }

    private dataStructure(): void {
        this.dataGrid = {
            data: orderBy(this.dataGrid.data, this.sort),
            total: this.dataGrid.data.length
        };
    }

    createForm() {
        this.myFormGroup = this.fb.group({
            Mercado: ['', [Validators.required, Validators.maxLength(6)]],
            mdesc: ['', [Validators.required, Validators.maxLength(40)]],
            Secuencia: ['', [Validators.required, Validators.max(999), Validators.min(0)]],
            Inactivo: [false],
            cRowID: [''],
            lError: [false],
            cErrDesc: [''],
        });
    }

    rebuildForm() {
        this.myFormGroup.reset({
            Mercado: '', mdesc: '', Secuencia: '', Inactivo: false, lError: false, cRowID: ''
        });
    }

    guardarForm() {
        (this.flagEdit) ? this.saveRow('put') : this.saveRow('post');
    }

    cerrarForm() {
        this.flagShowForm = false;
        this.rebuildForm();
    }

    printPDF() {
      this.toast({
        type: 'info',
        title: 'NO DISPONIBLE!',
        text: 'En mantenimiento'
      });
        // const columns = [
        //     {title: 'Mercado', dataKey: 'Mercado'},
        //     {title: 'Descripción', dataKey: 'mdesc'},
        //     {title: 'Secuencia   ', dataKey: 'Secuencia'},
        //     {title: 'Inactivo', dataKey: 'Inactivo'}
        // ];
        // this._pdfService.printPDF('Mercados', columns, this.dataGrid.data);
    }

    openModal(content){
      this.modal.open(content, {
        ariaLabelledBy: 'modal-distri',
        size: 'sm',
        centered: true
      }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }

    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return `with: ${reason}`;
      }
    }

    newRow(content) {
        this.flagShowForm = true;
        this.flagEdit = false;
        this.rebuildForm();
        this.openModal(content);
    }

    getMercados() {
      this.loading = true;
        this._crudservice.getData(this.urlSec)
            .map(data => data = data.siMercado.dsMercados.ttMercados)
            .subscribe(data => {
              if (!data){
                this.dataGrid = [];
                this.dataComplete = [];
              }else {
                this.dataGrid = data;
                this.dataComplete = data;
                this.dataGrid = {
                    data: orderBy(this.dataGrid, this.sort),
                    total: this.dataGrid.length
                };
              }
            });
        this.loading = false;
    }

    saveRow(request) {
      this.loading = true;
        const form = this.myFormGroup.value;
        let method = null;
        if (request === 'post'){
          method = this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt);
        }else{
          method = this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt);
        }
        method.subscribe(data => {
              const result = data.siMercado.dsMercados.ttMercados;
                if (result[0].cErrDesc) {
                    this.toast({
                      type: 'error',
                      title: GLOBAL.textError,
                      text: result[0].cErrDesc
                    });
                    this.loading = false;
                } else {
                    this.toast({
                        type: 'success',
                        title: GLOBAL.textSuccess
                    })
                }
                this.flagShowForm = false;
                this.getMercados();
                this.rebuildForm();
            })
    }

    public editRow({dataItem}, content) {
        this.myFormGroup.setValue(dataItem);
        this.flagEdit = true;
        this.flagShowForm = true;
        this.openModal(content);
    }

    public deleteRow({dataItem}) {
      this.loading = true;
        this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
          const result = data.siMercado.dsMercados.ttMercados;
          if (result[0].cErrDesc){
            this.toast({
              type: 'error',
              title: GLOBAL.textError,
              text: result[0].cErrDesc
            });
            this.loading = false;
          }else{
            this.toast({
              type: 'success',
              title: GLOBAL.textDeleteSuccess
            });
            this.flagShowForm = false;
            this.getMercados();
            this.rebuildForm();
          }
        });
    }

}
