import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SortDescriptor, filterBy, CompositeFilterDescriptor, GroupDescriptor } from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoCuartoService } from './mantto-cuartos.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { ManttoMenuService } from '../mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../services/global';
declare var jsPDF: any;
const flatten = GLOBAL.flatten;

@Component({
    selector: 'app-mantto-cuartos',
    templateUrl: './mantto-cuartos.component.html',
    styleUrls: ['./mantto-cuartos.component.css']
})
export class ManttoCuartosComponent implements OnInit {
    heading = 'Mantenimiento Estado de Cuartos';
    subheading: string;
    icon: string;
    public toast = GLOBAL.toast;
    public myFormGroup: FormGroup;
    public dataGrid: any = [];
    public dataComplete: any = [];
    public ttInfocia: any = [];
    public dsTipos: any = [];
    public selectedItemTipo: any;
    public flagEdit: boolean;
    public checked: boolean;
    public groups: GroupDescriptor[] = [];
    public filter: CompositeFilterDescriptor;
    public sort: SortDescriptor[] = [{ field: 'cedo', dir: 'asc' }];
    public cutil = [];
    public cguardar = [];
    public dsInterfase = Array(8).fill(0).map((x, i) => {
        if (i) {
            return i;
        }
    });
    loading: boolean;

    constructor(private _fb: FormBuilder,
        private _mantto: ManttoCuartoService,
        private _info: InfociaGlobalService,
        private _menuServ: ManttoMenuService,
        private _modal: NgbModal,
        private _router: Router) {
        this.ttInfocia = this._info.getSessionInfocia();
        this.dsTipos = [
            { ctipo: 'O', descripcion: 'Ocupado' },
            { ctipo: 'V', descripcion: 'Vacante' },
            { ctipo: 'X', descripcion: 'Fuera de Servicio' }
        ];
        this.cutil = [
            { id: 'cutil1', value: false, desc: 'Recepción' },
            { id: 'cutil2', value: false, desc: 'Llaves' },
            { id: 'cutil3', value: false, desc: 'Sistema' },
            { id: 'cutil4', value: false, desc: 'Botones' }
        ];
        this.cguardar = [
            { id: 'cguardar1', value: false, desc: 'Recepción' },
            { id: 'cguardar2', value: false, desc: 'Llaves' },
            { id: 'cguardar3', value: false, desc: 'Sistema' },
            { id: 'cguardar4', value: false, desc: 'Revisión' },
            { id: 'cguardar5', value: false, desc: 'Botones' }
        ];
        this.buildFormGroup();
        this.getDataSource();
    }

    ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
          const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
          if (resp) {
            this.icon = resp.icon;
            this.subheading = resp.subheading;
          }
        });
    }

    private buildFormGroup() {
        this.myFormGroup = this._fb.group({
            iSec: [0, [Validators.required, Validators.max(999)]],
            cedo: ['', [Validators.required, Validators.maxLength(1)]],
            cdesc: ['', [Validators.required, Validators.maxLength(30)]],
            ctipo: ['', [Validators.required]],
            Interfase: [''],
            cColor: [''],
            cImagen: [''],
            Inactivo: [false],
            cguardar: [''],
            cutil: [''],
            cErrDes: [''],
            lError: [false],
            cRowID: ['']
        });
    }

    private getDataSource() {
        this.loading = true;
        this._mantto.getEstadoCuartos().subscribe(data => {
            this.dataGrid = data;
            this.dataComplete = data;
            this.loading = false;
        });
    }

    public newRow(modal) {
        this.buildFormGroup();
        this.openModal(modal);
        this.flagEdit = false;
    }

    public editRow(dataItem, modal) {
        this.openModal(modal);
        this.myFormGroup.setValue(dataItem);
        const arrCutil = dataItem.cutil.split('');
        const arrCguardar = dataItem.cguardar.split('');
        let index = 0;
        let index2 = 0;
        for (const util of arrCutil) {
            if (util === 'S') {
                this.cutil[index].value = true;
            } else {
                this.cutil[index].value = false;
            }
            index++;
        }
        for (const guardar of arrCguardar) {
            if (guardar === 'S') {
                this.cguardar[index2].value = true;
            } else {
                this.cguardar[index2].value = false;
            }
            index2++;
        }
        this.flagEdit = true;
    }

    public guardarForm() {
        this.flagEdit ? this.updateForm() : this.saveRow();
    }

    private updateForm() {
        this.loading = true;
        const form = JSON.parse(JSON.stringify(this.myFormGroup.value));
        const cutil = [];
        const cguardar = [];

        this.cutil.forEach(util => {
            if (util.value) {
                cutil.push('S');
            } else {
                cutil.push('N');
            }
        });
        this.cguardar.forEach(guardar => {
            if (guardar.value) {
                cguardar.push('S');
            } else {
                cguardar.push('N');
            }
        });
        form.cutil = cutil.join('');
        form.cguardar = cguardar.join('');
        this._mantto.putEstadoCuaros(form).subscribe(data => {
            if (data && data[0].cErrDes) {
                this.toast({ type: 'error', title: GLOBAL.textError, text: data[0].cErrDes});
            }else {
                this._modal.dismissAll();
                this.getDataSource();
                this.toast({
                  type: 'success',
                  title: GLOBAL.textSuccess
                });
            }
        });
    }

    private saveRow() {
        this.loading = true;
        const form = JSON.parse(JSON.stringify(this.myFormGroup.value));
        const cutil = [];
        const cguardar = [];
        this.cutil.forEach(util => {
            if (util.value) {
                cutil.push('S');
            } else {
                cutil.push('N');
            }
        });
        this.cguardar.forEach(guardar => {
            if (guardar.value) {
                cguardar.push('S');
            } else {
                cguardar.push('N');
            }
        });
        form.cutil = cutil.join('');
        form.cguardar = cguardar.join('');
        this._mantto.postEstadoCuaros(form).subscribe(data => {
            if (data && data[0].cMensTxt) {
                this.toast({ type: 'error', title: data[0].cMensTxt });
            } else {
                this.toast({ type: 'success', title: GLOBAL.textSuccess });
                this.getDataSource();
                this._modal.dismissAll();
            }
        })
    }

    openModal(content) {
        this._modal.open(content, { size: 'md', centered: true });
    }

    public deleteRow(dataItem) {
        this._mantto.deleteEstadoCuaros(dataItem).subscribe(data => {
            this.getDataSource();
            this.toast({
              type: 'success',
              title: GLOBAL.textDeleteSuccess
            });
        });
    }

    public printPDF() {
      this.toast({
        type: 'info',
        title: 'NO DISPONIBLE!',
        text: 'En mantenimiento'
      });
    }

    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.dataGrid = filterBy(this.dataComplete, filter);
    }

    public switchChange(checked: boolean): void {
        const root = this.filter || { logic: 'and', filters: [] };
        const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
        if (!filter) {
            root.filters.push({
                field: 'Inactivo',
                operator: 'eq',
                value: checked
            });
        } else {
            filter.value = checked;
        }
        this.checked = checked;
        this.filterChange(root);
    }
}
