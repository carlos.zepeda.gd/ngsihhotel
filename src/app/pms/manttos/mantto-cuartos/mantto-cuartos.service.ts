import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { GLOBAL } from '../../../services/global';
const headers = new Headers({'Content-Type': 'application/json'});

@Injectable()
export class ManttoCuartoService {

  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }
  postEstadoCuaros(data) {
    const json = JSON.stringify(data);
    const params = '{"dsCtoedos":{"ttCtoedos":[' + json + ']}}';

    return this.http.post( this.url + '/manttos/ctoedos',  params, { headers: headers })
                    .map( res => {
                      const succes = res.json().response.siCtoedos.dsCtoedos.ttCtoedos;
                      const error  = res.json().response.siCtoedos.dsMensajes.ttMensError;
                      if (error) {
                          return error;
                      }else {
                          return succes;
                      }
                      
                    });
  }
  putEstadoCuaros(data) {
    const json = JSON.stringify(data);
    const params = '{"dsCtoedos":{"ttCtoedos":[' + json + ']}}';

    return this.http.put( this.url + '/manttos/ctoedos',  params, { headers: headers })
                    .map( res => res.json().response.siCtoedos.dsCtoedos.ttCtoedos );
  }
  deleteEstadoCuaros(data) {
    const json = JSON.stringify(data);
    const params = '{"dsCtoedos":{"ttCtoedos":[' + json + ']}}';

    return this.http.delete( this.url + '/manttos/ctoedos', 
                    new RequestOptions({ headers: headers, body: params }))
                    .map( res => res.json().response.siCtoedos.dsCtoedos.ttCtoedos );
  }
  getEstadoCuartos() {
    return this.http.get( this.url + '/manttos/ctoedos')
                    .map( res => res.json().response.siCtoedos.dsCtoedos.ttCtoedos );
  }
}
