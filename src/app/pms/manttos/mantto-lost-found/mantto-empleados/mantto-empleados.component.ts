import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import {CrudService} from '../../mantto-srv.services';
import {ManttoMenuService} from '../../mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import {PageChangeEvent} from '@progress/kendo-angular-grid';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { GLOBAL } from '../../../../services/global';

const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-empleados',
  templateUrl: './mantto-empleados.component.html',
  styleUrls: ['./../../manttos-desing.css']
})
export class ManttoEmpleadosComponent implements OnInit {
  heading = 'Mantenimiento Empleados';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public flagShowForm: boolean;
  flagEdit: boolean;
  public dataComplete: any = [];
  public checked: boolean;
  public groups: GroupDescriptor[] = [];
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{field: 'punto', dir: 'asc'}];

  private urlSec: string = '/manttos/empleados';
  private ds: string = 'dsEmpleados'; // siEmpleado.dsEmpleados.ttEmpleados
  private tt: string = 'ttEmpleados';
  public gridFields = [];
  public pdfGridFields = [];
  public pdfGrid = [];
  seccion: string = 'Empleados';
  errorMesj: string = null;
  formStatus: string = null;
  public dsDeptos: any = [];
  public dsDeptosComplete: any = [];
  public selectedItem: string = '';

  loading = false;
  pageSize = 10;
  skip = 0;
  closeResult: string;
  toast = GLOBAL.toast;

  constructor(private _crudservice: CrudService,
              private _menuServ: ManttoMenuService,
              private _router: Router,
              private formBuild: FormBuilder,
              private modal: NgbModal) {

    this.myFormGroup = this.buildFormGroup();

    this.gridFields = [
      {title: 'Empleado', dataKey: 'Empleado', width: '150', filtro: false},
      {title: 'Nombre', dataKey: 'enombre', width: '200', filtro: true},
      {title: 'No. Depto', dataKey: 'depto', width: '150', filtro: false},
      {title: 'Depto', dataKey: 'cDepto', width: '150', filtro: false},
    ];
    this.pdfGridFields = [
      {title: 'Inactivo', dataKey: 'Inactivo', width: '', filtro: false},
    ];

    this._crudservice.getData('/manttos/deptos').subscribe(data => {
        this.dsDeptos = data.siDepto.dsDeptos.ttDeptos;
        this.dsDeptosComplete = data;
      });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
  }

  ngOnInit() {
    this.getDataSource();

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  private buildFormGroup() {
    return this.formBuild.group({
      Empleado: ['', [Validators.required, Validators.max(999999)]],
      enombre: ['', [Validators.required, Validators.maxLength(40)]],
      depto: [''],
      cDepto: ['', [Validators.required]],
      Inactivo: [false],
      cErrDesc: [''],
      lError: [false],
      cRowID: [''],
    });
  }

  private getDataSource() {
    this.loading = true;
    this._crudservice.getData(this.urlSec).subscribe(data => {
      const result = data.siEmpleado.dsEmpleados.ttEmpleados
      if (result){
        this.dataGrid = result;
        this.dataComplete = result;
      }else {
        this.dataGrid = [];
        this.dataComplete = [];
      }
        this.loading = false;
      });
  }

  public aBuscar(info) {
    this.myFormGroup.patchValue({
      cDepto: info.ddesc,
      depto: info.depto
    });
  }

  public reset(){
    this.myFormGroup.reset({
      Empleado: '',
      enombre: '',
      depto: '',
      cDepto: '',
      Inactivo: false,
      cErrDesc: '',
      lError: false,
      cRowID: '',
    });
    this.selectedItem = '';
  }

  openModal(content){
    this.modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'sm',
      centered: true
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  public newRow(content) {
    this.reset();
    this.flagShowForm = true;
    this.flagEdit = false;
    this.formStatus = 'Nuevo';

    this.openModal(content);
  }

  public guardarForm() {
    (this.flagEdit) ? this.updateForm() : this.saveRow();
  }

  selectDropdownListDepto(clave: string) {
    let i: any;
    for (i in this.dsDeptos) {
      if (this.dsDeptos[i].ddesc === clave) {
        this.selectedItem = this.dsDeptos[i];
      }
    }
  }

  public editRow({dataItem}, content) {
    this.myFormGroup.setValue(dataItem);
    this.selectDropdownListDepto(dataItem.cDepto);

    this.flagEdit = true;
    this.flagShowForm = true;
    this.formStatus = 'Editar';

    this.openModal(content);
  }

  private saveRow() {
    const form = this.myFormGroup.value;
    if (form.cDepto.ddesc) {
      form.cDepto = form.cDepto.ddesc
    }
    // if (form.eEstado.Estado) { form.eEstado = form.eEstado.Estado }
    this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt)
      .subscribe(data => {
        this.errorMesj = (data.siEmpleado.dsMensajes.ttMensError == null) ?
          null : data.siEmpleado.dsMensajes.ttMensError[0].cMensTxt;
        if (this.errorMesj) {
          this.toast({
            type: 'error',
            title: GLOBAL.textError,
            text: this.errorMesj
          });
        } else {
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          })
        }
        this.flagShowForm = false;
        this.getDataSource();
      })
  }

  private updateForm() {
    const form = this.myFormGroup.value;
    if (form.cDepto.ddesc) {
      form.cDepto = form.cDepto.ddesc
    }
    this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
      this.errorMesj = (data.siEmpleado.dsMensajes.ttMensError == null) ?
        null : data.siEmpleado.dsMensajes.ttMensError[0].cMensTxt;
      if (this.errorMesj) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: this.errorMesj
        });
      } else {
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        })
      }
      this.getDataSource();
    });
  }

  public deleteRow({dataItem}) {
    this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
      this.errorMesj = (data.siEmpleado.dsMensajes.ttMensError == null) ?
        null : data.siEmpleado.ttMensError[0].cMensTxt;
      if (this.errorMesj) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: this.errorMesj
        })
      } else {
        this.toast({
          type: 'success',
          title: GLOBAL.textDeleteSuccess
        })
        this.getDataSource();
      }
    });
  }

  public closeForm() {
    this.myFormGroup.reset();
    this.flagShowForm = false;
    this.flagEdit = false;
  }

  public close(component) {
    this[component + 'Opened'] = false;
    this.closeForm();
  }

  // Generear PDF
  public printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    });
    // const columns = this.pdfGrid.concat(this.gridFields, this.pdfGridFields);
    // this._pdfService.printPDF(this.seccion, columns, this.dataGrid.data);
  }

  // Funciones para la Grid de Kendo
  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.dataGrid = process(this.dataComplete, {group: this.groups});
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || {logic: 'and', filters: []};
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

}
