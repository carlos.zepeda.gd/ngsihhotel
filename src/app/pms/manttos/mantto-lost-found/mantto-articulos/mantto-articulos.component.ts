import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import swal from 'sweetalert2';
import {CrudService} from '../../mantto-srv.services';
import {PdfService} from '../../mantto-pdf.service';
import {ManttoMenuService} from '../../mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import {PageChangeEvent} from '@progress/kendo-angular-grid';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { GLOBAL } from '../../../../services/global';

const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-articulos',
  templateUrl: './mantto-articulos.component.html',
  styleUrls: ['./../../manttos-desing.css']
})
export class ManttoArticulosComponent implements OnInit {
  heading = 'Mantenimientos Estado de Cuartos';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public flagShowForm: boolean;
  flagEdit: boolean;
  public dataComplete: any = [];
  public checked: boolean;
  public groups: GroupDescriptor[] = [];
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{field: 'punto', dir: 'asc'}];

  private urlSec: string = '/manttos/larticulos';
  private ds: string = 'dsLarticulos'; // siLarticulo.dsLarticulos.ttLarticulos
  private tt: string = 'ttLarticulos';
  public gridFields = [];
  public pdfGridFields = [];
  public pdfGrid = [];
  seccion: string = 'Artículos';
  errorMesj: string = null;
  formStatus: string = null;
  public dsEstados: any = [];
  public dsEstadosComplete: any = [];
  toast = GLOBAL.toast;

  loading = false;
  pageSize = 10;
  skip = 0;
  closeResult: string;

  constructor(private _crudservice: CrudService,
              private _pdfService: PdfService,
              private _menuServ: ManttoMenuService,
              private _router: Router,
              private modal: NgbModal) {
    this.gridFields = [
      {title: 'Artículo', dataKey: 'Articulo', width: '150', filtro: false},
      {title: 'Descripción', dataKey: 'adesc', width: '', filtro: true},
      {title: 'Días', dataKey: 'adias', width: '', filtro: false},
    ];
    this.pdfGridFields = [
      {title: 'Inactivo', dataKey: 'Inactivo', width: '', filtro: false},
    ];
    this.buildFormGroup();
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
  }

  ngOnInit() {
    this.getDataSource();

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  private buildFormGroup() {
    this.myFormGroup = new FormGroup({
      Articulo: new FormControl('', Validators.required, Validators.maxLength[6]),
      adesc: new FormControl('', Validators.required, Validators.maxLength[30]),
      adias: new FormControl(0, Validators.min(0), Validators.max[999]),
      Inactivo: new FormControl(false),
      cErrDesc: new FormControl(''),
      lError: new FormControl(false),
      cRowID: new FormControl(''),
    });
  }

  private getDataSource() {
    this.loading = true;
    this._crudservice.getData(this.urlSec).map(data =>
      data = data.siLarticulo.dsLarticulos.ttLarticulos)
      .subscribe(data => {
        this.dataGrid = data;
        this.dataComplete = data;
        /*this.dataGrid = {
          data: orderBy(this.dataGrid, this.sort),
          total: this.dataGrid.length
        };*/
        this.loading = false;
      });
  }

  public openModal(content){
    this.modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'md',
      centered: true
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  public newRow(content) {
    this.buildFormGroup();
    this.flagShowForm = true;
    this.flagEdit = false;
    this.openModal(content);

  }

  public guardarForm() {
    (this.flagEdit) ? this.saveRow('put') : this.saveRow('post');
  }

  public editRow({dataItem}, content) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
    this.openModal(content);
  }

  private saveRow(request) {
    const form = this.myFormGroup.value;
    let method = null;
    if (request === 'post'){
      method = this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt);
    }else {
      method = this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt);
    }
    method.subscribe(data => {
        this.errorMesj = (data.siLarticulo.dsMensajes.ttMensError == null) ?
          null : data.siLarticulo.dsMensajes.ttMensError[0].cMensTxt;
        if (this.errorMesj) {
          this.toast({
            type: 'error',
            title: GLOBAL.textError,
            text: this.errorMesj});
        } else {
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          })
        }
        this.flagShowForm = false;
        this.getDataSource();
        this.closeForm();
      })
  }

  public deleteRow({dataItem}) {
    this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
      this.errorMesj = (data.siLarticulo.dsMensajes.ttMensError == null) ?
        null : data.siLarticulo.ttMensError[0].cMensTxt;
      if (this.errorMesj) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: this.errorMesj
        })
      } else {
        this.toast({
          type: 'success',
          title: GLOBAL.textDeleteSuccess
        });
        this.getDataSource();
      }
      this.closeForm();
    });
  }

  public closeForm() {
    this.myFormGroup.reset();
    this.flagShowForm = false;
    this.flagEdit = false;
  }

  public close(component) {
    this[component + 'Opened'] = false;
    this.closeForm();
  }

  // Generear PDF
  public printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    });
  }

  // Funciones para la Grid de Kendo
  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.dataGrid = process(this.dataComplete, {group: this.groups});
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || {logic: 'and', filters: []};
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

}
