import { Injectable } from '@angular/core';

@Injectable()
export class DateFormatService {

  constructor() {
  }

  public formatDate(date, split: string = '-') { // Recibe fecha en formato JavaScript : Date y devuelve formato sql
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) { month = '0' + month; }
    if (day.length < 2) { day = '0' + day; }
    return [year, month, day].join(split);
  }
  public toDate(dateStr) { // Recibe formato SQL y Devuelve fecha en formato JavaScript :Date
    const [year, month, day] = dateStr.split('-');
    return new Date(year, month - 1, day);
  }

}
