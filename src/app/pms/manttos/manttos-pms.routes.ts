import {RouterModule, Routes} from '@angular/router';
import {ManttoMenuComponent} from './mantto-menu/mantto-menu.component';
import {ManttoCuartosComponent} from './mantto-cuartos/mantto-cuartos.component';

// Mantto General
import {ManttoMonedasComponent} from './mantto-general/mantto-monedas/mantto-monedas.component';
import {ManttoTipoBandasComponent} from './mantto-general/mantto-tipo-bandas/mantto-tipo-bandas.component';
import {ManttoTiposReservacionComponent} from './mantto-general/mantto-tipos-reservacion/mantto-tipos-reservacion.component';
import {ManttoOrigenesReservacionComponent} from './mantto-general/mantto-origenes-reservacion/mantto-origenes-reservacion.component';
import {ManttoSegmentosMercadoComponent} from './mantto-general/mantto-segmentos-mercado/mantto-segmentos-mercado.component'
import {ManttoSubtotsSegmerComponent} from './mantto-general/mantto-subtots-segmer/mantto-subtots-segmer.component';
import {ManttoBancosComponent} from './mantto-general/mantto-bancos/mantto-bancos.component';
import {ManttoClasAgenComponent} from './mantto-general/mantto-clasificacion-agencias/mantto-clasificacion-agencias.component';
import {ManttoSucursalesComponent} from './mantto-general/mantto-sucursales/mantto-sucursales.component';
import {ManttoTiposNotasComponent} from './mantto-general/mantto-tipos-notas/mantto-tipos-notas.component';
import {ManttoMotivosRazonesComponent} from './mantto-general/mantto-motivos-razones/mantto-motivos-razones.component';
// Mantto Tarifas
import {ManttoTarifasComponent} from './mantto-tarifas/mantto-tarifas/mantto-tarifas.component';
import {ManttoCategoriasTarifaComponent} from './mantto-tarifas/mantto-categorias-tarifa/mantto-categorias-tarifa.component';
import {ManttoSuplementosComponent} from './mantto-tarifas/mantto-suplementos/mantto-suplementos.component';
import {ManttoDesgloseTarifasComponent} from './mantto-tarifas/mantto-desglose-tarifas/mantto-desglose-tarifas.component';
import {ManttoSemaforosComponent} from './mantto-tarifas/mantto-semaforos/mantto-semaforos.component';
import {ManttoSemaforosFechaComponent} from './mantto-tarifas/mantto-semaforos-fecha/mantto-semaforos-fecha.component'
// Mantto Requerimientos Especiales
import {
  ManttoRequerimientosEspecialesComponent
} from './mantto-requerimientos-especiales/mantto-requerimientos-especiales/mantto-requerimientos-especiales.component';
import {
  ManttoDepartamentosComponent
} from './mantto-requerimientos-especiales/mantto-departamentos/mantto-departamentos.component';
import {
  ManttoGruposRequerimientoComponent
} from './mantto-requerimientos-especiales/mantto-grupos-requerimiento/mantto-grupos-requerimiento.component';
import {
  ManttoSatisfaccionesComponent
} from './mantto-requerimientos-especiales/mantto-satisfacciones/mantto-satisfacciones.component';
import {
  ManttoTrabajosComponent
} from './mantto-requerimientos-especiales/mantto-trabajos/mantto-trabajos.component';
// Mantto Ingresos Agrupados
import {ManttoIngAgrupadosComponent} from './manttos-presupuestos/mantto-ing-agrupados/mantto-ing-agrupados.component';
// Mantto Lost and Found
import {ManttoArticulosComponent} from './mantto-lost-found/mantto-articulos/mantto-articulos.component';
import {ManttoAreasComponent} from './mantto-lost-found/mantto-areas/mantto-areas.component';
import {ManttoEmpleadosComponent} from './mantto-lost-found/mantto-empleados/mantto-empleados.component';
// Mantto Paises
import {ManttoPaisesEstadosComponent} from './mantto-paises-estados/mantto-paises-estados.component';
import {ManttoIdiomaComponent} from './mantto-idioma/mantto-idioma.component';
import {ManttoRegionesComponent} from './mantto-regiones/mantto-regiones.component';
import {ManttoPaisesComponent} from './mantto-paises/mantto-paises.component';
import {ManttoMercadosComponent} from './mantto-mercados/mantto-mercados.component';
import {ManttoRolesComponent} from './mantto-roles/mantto-roles.component';
import {ManttoUsuariosComponent} from './mantto-usuarios/mantto-usuarios.component';
import {BaseLayoutComponent} from '../../_architect/Layout/base-layout/base-layout.component';
import {VerifyInfociaGuardService} from '../../services/verify-infocia-guard.service';
import {ListaHuespedesComponent} from '../recepcion/lista-huespedes/lista-huespedes.component';
import {RfcCompaniasComponent} from './rfc-companias/rfc-companias.component';
import {EstadosCivilesComponent} from './Mantto Historicos/estados-civiles/estados-civiles.component';
import {ConsecutivosComponent} from './Mantto Facturacion/consecutivos/consecutivos.component';
import {VipComponent} from './mantto-general/vip/vip.component';
import {ClasesCodigosComponent} from './Mantto Codigos/clases-codigos/clases-codigos.component';
import {SubtotalCodigosComponent} from './Mantto Codigos/subtotal-codigos/subtotal-codigos.component';
import {ImpuestosComponent} from './Mantto Facturacion/impuestos/impuestos.component';
import {ClasifClientesComponent} from './Mantto Historicos/clasif-clientes/clasif-clientes.component';
import {UbicacionesComponent} from './mantto-cuartos/ubicaciones/ubicaciones.component';
import {TiposCuartosComponent} from './mantto-cuartos/tipos-cuartos/tipos-cuartos.component';
import {UnidadesMedidaComponent} from './Mantto Facturacion/unidades-medida/unidades-medida.component';
import {VendedoresComponent} from './mantto-general/vendedores/vendedores.component';
import {ManttoSeccionesComponent} from './mantto-cuartos/mantto-secciones/mantto-secciones.component';
import {CondicionesCreditoComponent} from './mantto-general/condiciones-credito/condiciones-credito.component';
import {CaractCuartoComponent} from './mantto-cuartos/caract-cuarto/caract-cuarto.component';
import {CuartosComponent} from './mantto-cuartos/cuartos/cuartos.component';
import {TiposCambioComponent} from './tipos-cambio/tipos-cambio.component';
import {ManttoEstadisticasComponent} from './Mantto Estadisticas/mantto-estadisticas/mantto-estadisticas.component';
import {EstadoHuespedesComponent} from './Informacion Adicional/estado-huespedes/estado-huespedes.component';
import {DocumentosViajeComponent} from './Informacion Adicional/documentos-viaje/documentos-viaje.component';
import {PuntosEntradaPaisComponent} from './Informacion Adicional/puntos-entrada-pais/puntos-entrada-pais.component';
import {CodigosComponent} from './Mantto Codigos/codigos/codigos.component';
import {DesgloseCodigosComponent} from './Mantto Codigos/desglose-codigos/desglose-codigos.component';
import {ManttoTiposMovtosComponent} from './Mantto Codigos/mantto-tipos-movtos/mantto-tipos-movtos.component';
import {ManttoOcupacionesComponent} from './Mantto Historicos/mantto-ocupaciones/mantto-ocupaciones.component';
import {ManttoTitulosComponent} from './Mantto Historicos/mantto-titulos/mantto-titulos.component';
import {UnidadesMedidaCFDComponent} from './Facturacion/unidades-medida-cfd/unidades-medida-cfd.component';
import {ProductosServiciosCFDComponent} from './Facturacion/productos-servicios-cfd/productos-servicios-cfd.component';
import {FormasPagoCFDComponent} from './Facturacion/formas-pago-cfd/formas-pago-cfd.component';
import {MetodosPagoCFDComponent} from './Facturacion/metodos-pago-cfd/metodos-pago-cfd.component';
import {UsosCFDComponent} from './Facturacion/usos-cfd/usos-cfd.component';
import {RegimenesFiscalesCFDComponent} from './Facturacion/regimenes-fiscales-cfd/regimenes-fiscales-cfd.component';
import {TiposRelacionCFDComponent} from './Facturacion/tipos-relacion-cfd/tipos-relacion-cfd.component';
import {TiposComprobanteCFDComponent} from './Facturacion/tipos-comprobante-cfd/tipos-comprobante-cfd.component';
import {ManttoAjustesEstadisticasComponent} from './mantto-requerimientos-especiales/mantto-ajustes-estadisticas/mantto-ajustes-estadisticas.component';
import { ManttoCambiosTarifasComponent } from './mantto-tarifas/mantto-cambios-tarifas/mantto-cambios-tarifas.component';
import { RoutesGuard } from 'app/services/routes-guard.service';

const MANTTOS_PMS_ROUTES: Routes = [
  {
    path: '',
    component: BaseLayoutComponent,
    children: [
      {path: 'tipos-cambio', component: TiposCambioComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'rfc-companias', component: RfcCompaniasComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      // Estadisticos
      {path: 'mantto-estadisticas', component: ManttoEstadisticasComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      // Usuarios
      {path: 'mantto-menu', component: ManttoMenuComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-roles', component: ManttoRolesComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-usuarios', component: ManttoUsuariosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      // Mantto Codigos
      {path: 'codigos', component: CodigosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'clases-codigos', component: ClasesCodigosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'subtotal-codigos', component: SubtotalCodigosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'tipos-movto', component: ManttoTiposMovtosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'desglose-codigos', component: DesgloseCodigosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      // Información Adicional
      {path: 'estado-huespedes', component: EstadoHuespedesComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'puntos-entrada-pais', component: PuntosEntradaPaisComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'documentos-viaje', component: DocumentosViajeComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      // Mantto Cuartos
      {path: 'mantto-cuartos', component: CuartosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'tipos-cuartos', component: TiposCuartosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'caract-cuarto', component: CaractCuartoComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-estado-cuartos', component: ManttoCuartosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'ubicaciones', component: UbicacionesComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'secciones', component: ManttoSeccionesComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      // Facturación
      {path: 'consecutivos', component: ConsecutivosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'impuestos', component: ImpuestosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'unidades-medida', component: UnidadesMedidaComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      // Mantto General
      {path: 'mantto-monedas', component: ManttoMonedasComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-tipo-bandas', component: ManttoTipoBandasComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-tipos-reservacion', component: ManttoTiposReservacionComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'condiciones-credito', component: CondicionesCreditoComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-origenes-reservacion', component: ManttoOrigenesReservacionComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-segmentos-mercado', component: ManttoSegmentosMercadoComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-subtots-segmer', component: ManttoSubtotsSegmerComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'vendedores', component: VendedoresComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'vip', component: VipComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-bancos', component: ManttoBancosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-clasificacion-agencias', component: ManttoClasAgenComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-sucursales', component: ManttoSucursalesComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-tipos-notas', component: ManttoTiposNotasComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-motivos-razones', component: ManttoMotivosRazonesComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      // Mantto Tarifas
      {path: 'mantto-tarifas', component: ManttoTarifasComponent, canActivate: [RoutesGuard], data: {extraParameter: '', proc: 'T'}},
      {path: 'tarifas-mayoristas', component: ManttoTarifasComponent, canActivate: [RoutesGuard], data: {extraParameter: '', proc: 'M'}},
      {path: 'mantto-categorias-tarifa', component: ManttoCategoriasTarifaComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-suplementos', component: ManttoSuplementosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-desglose-tarifas', component: ManttoDesgloseTarifasComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-semaforos', component: ManttoSemaforosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-semaforos-fecha', component: ManttoSemaforosFechaComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-cambios-tarifas', component: ManttoCambiosTarifasComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      // Mantto Requerimientos Especiales
      {path: 'mantto-requerimientos-especiales', component: ManttoRequerimientosEspecialesComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-departamentos', component: ManttoDepartamentosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-grupos-requerimiento', component: ManttoGruposRequerimientoComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-satisfacciones', component: ManttoSatisfaccionesComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-trabajos', component: ManttoTrabajosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      // Mantto Lost and Found
      {path: 'mantto-articulos', component: ManttoArticulosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-areas', component: ManttoAreasComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-empleados', component: ManttoEmpleadosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      // Mantto Ingresos Agrupados
      {path: 'mantto-ing-agrupados', component: ManttoIngAgrupadosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      // Mantto Paises
      {path: 'mantto-paises-estados', component: ManttoPaisesEstadosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-paises', component: ManttoPaisesComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-regiones', component: ManttoRegionesComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-idioma', component: ManttoIdiomaComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-mercados', component: ManttoMercadosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      // Mantto Historico de Clientes
      {path: 'estados-civiles', component: EstadosCivilesComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-clasifs', component: ClasifClientesComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-ocupaciones', component: ManttoOcupacionesComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-titulos', component: ManttoTitulosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'unidades-medida-cfd', component: UnidadesMedidaCFDComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'productos-servicios-cfd', component: ProductosServiciosCFDComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'formas-pago-cfd', component: FormasPagoCFDComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'metodos-pago-cfd', component: MetodosPagoCFDComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'usos-de-cfd', component: UsosCFDComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'regimenes-fiscales-cfd', component: RegimenesFiscalesCFDComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'tipos-relacion-cfd', component: TiposRelacionCFDComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'tipos-comprobante-cfd', component: TiposComprobanteCFDComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'mantto-ajustes-estadisticas', component: ManttoAjustesEstadisticasComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
    ],
    canActivateChild: [
      VerifyInfociaGuardService
    ]
  }
];

export const MANTTOS_PMS_ROUTING = RouterModule.forChild(MANTTOS_PMS_ROUTES);

