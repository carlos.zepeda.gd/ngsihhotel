import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { InfociaGlobalService } from '../../services/infocia.service';
import 'rxjs/add/operator/map';
import { GLOBAL } from '../../services/global';
const headers = new Headers({'Content-Type': 'application/json'});

declare var $: any;
declare var jsPDF: any;


@Injectable()
export class PdfService {

    private ttInfocia: any;
    public url: string;
    
    constructor(
      private http: Http,
      private _info: InfociaGlobalService
      ) {
        this.ttInfocia = this._info.getSessionInfocia();
        this.url = GLOBAL.url;
  }

  printPDF(titulo: string, columns: any[], data: any) {
      const encabezado = this.ttInfocia.encab;
      const rows = data;
        const doc = new jsPDF('p', 'pt');
        doc.setFontSize(8);
        doc.text(encabezado, 40, 30);
        doc.setFontType('normal');
        doc.setFontSize(8);
        doc.setTextColor(100);
        doc.autoTable(columns, rows, {
            theme: 'striped',
            styles: {
                cellPadding: 5, // a number, array or object (see margin below)
                fontSize: 8,
                font: 'helvetica' // helvetica, times, courier
            },
        });
        doc.output('save', titulo + '.pdf');
  }

}
