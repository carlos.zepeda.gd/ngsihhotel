export class Menu {
    constructor(
        public cSistema: string,
        public cMenu: string,
        public cSubMenu: string,
        public cOpcion: string,
        public cPrograma: string,
        public iSec: number,
        public cRowID: string,
        public cIcon = '',
        public lexiste = false,
        public lError = false,
        public cErrDes = ''
    ) {}
}
