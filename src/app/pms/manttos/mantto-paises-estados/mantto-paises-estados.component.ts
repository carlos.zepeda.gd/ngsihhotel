import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import {InfociaGlobalService} from '../../../services/infocia.service';
import swal from 'sweetalert2';
import {CrudService} from '../mantto-srv.services';
import {PdfService} from '../mantto-pdf.service';
import {ManttoMenuService} from '../mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import { GLOBAL } from '../../../services/global';
const flatten = GLOBAL.flatten;

@Component({
    selector: 'app-mantto-paises-estados',
    templateUrl: './mantto-paises-estados.component.html',
    styleUrls: ['./../manttos-desing.css']
})
export class ManttoPaisesEstadosComponent implements OnInit {
    heading = 'Mantenimientos de Estados';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';

    public myFormGroup: FormGroup;
    private flagEdit: boolean;
    public flagShowForm: boolean;
    private ttInfocia: any;
    public checked = false;
    public dataComplete: any = [];
    public dataGrid: any = [];
    public filter: CompositeFilterDescriptor;
    public groups: GroupDescriptor[] = [];
    public sort: SortDescriptor[] = [{field: 'idesc', dir: 'asc'}];
    private urlSec: string = '/manttos/estados';
    private ds: string = 'dsEstados';
    private tt: string = 'ttEstados';

    dataRegiones: any[] = [];
    dataPaises: any[] = [];
    public emmanual = [];

    constructor(private fb: FormBuilder,
                private _info: InfociaGlobalService,
                private _crudservice: CrudService,
                private _pdfService: PdfService,
                private _menuServ: ManttoMenuService,
                private _router: Router) {
        this.ttInfocia = this._info.getSessionInfocia();
        this.emmanual = [
            {id: 'reservaciones', value: false, desc: 'Reservaciones'},
            {id: 'recepcion', value: false, desc: 'Recepción'},
        ];
        this.createForm();
        this._crudservice.getData('/manttos/regiones').subscribe(data => {
                this.dataRegiones = data.siRegion.dsRegiones.ttRegiones;
            }
        );
        this._crudservice.getData('/manttos/paises').subscribe(data => {
                this.dataPaises = data.siPais.dsPaises.ttPaises;
            }
        );
    }

    ngOnInit() {
        this.getPais();

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    getPais() {
        this._crudservice.getData(this.urlSec)
            .map(data => data = data.siEstado.dsEstados.ttEstados)
            .subscribe(data => {
                this.dataGrid = data;
                this.dataComplete = data;
                this.dataGrid = {
                    data: orderBy(this.dataGrid, this.sort),
                    total: this.dataGrid.length
                };
            });
    }

    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.loadProducts();
    }

    private loadProducts(): void {
        this.dataGrid = process(this.dataComplete, {group: this.groups});
    }

    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.dataGrid.data = filterBy(this.dataComplete, filter);
    }

    public switchChange(checked: boolean): void {
        const root = this.filter || {logic: 'and', filters: []};
        const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
        if (!filter) {
            root.filters.push({
                field: 'Inactivo',
                operator: 'eq',
                value: checked
            });
        } else {
            filter.value = checked;
        }
        this.checked = checked;
        this.filterChange(root);
    }

    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.dataStructure();
    }

    private dataStructure(): void {
        this.dataGrid = {
            data: orderBy(this.dataGrid.data, this.sort),
            total: this.dataGrid.data.length
        };
    }

    createForm() {
        this.myFormGroup = this.fb.group({
            Estado: ['', Validators.required],
            enombre: ['', Validators.required],
            region: [''],
            Pais: ['', Validators.required],
            emmanual: [''],
            cErrDesc: [''],
            cRowID: [''],
            Inactivo: [false],
            lError: [false]
        });
    }

    public editRow({sender, rowIndex, dataItem}) {
        this.myFormGroup.setValue(dataItem);
        const arrManuales = dataItem.emmanual.split('');
        let index = 0;
        for (const util of arrManuales) {
            this.emmanual[index].value = (util === 'S') ? true : false;
            index++;
        }
        this.flagEdit = true;
        this.flagShowForm = true;
    }

    public deleteRow({dataItem}) {
        this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
            this.flagShowForm = false;
            this.getPais();
            this.rebuildForm();
        })
    }

    cerrarForm() {
        this.flagShowForm = false;
        this.rebuildForm();
    }

    editarForm() {
        const form = this.myFormGroup.value;
        const cutil = [];
        this.emmanual.forEach(emmanual => {
            (emmanual.value) ? cutil.push('S') : cutil.push('N');
        });
        form.emmanual = cutil.join('');
        this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
            this.flagShowForm = false;
            this.getPais();
            this.rebuildForm();
        })

    }

    printPDF() {
        const columns = [
            {title: 'Estado', dataKey: 'Estado'},
            {title: 'Nombre', dataKey: 'enombre'},
            {title: 'Region', dataKey: 'region'},
            {title: 'Pais', dataKey: 'Pais'}
        ];
        this._pdfService.printPDF('Estados', columns, this.dataGrid.data);
    }

    guardarForm() {
        (this.flagEdit) ? this.editarForm() : this.saveRow();
    }

    newRow() {
        this.flagShowForm = true;
        this.flagEdit = false;
    }

    saveRow() {
        const form = this.myFormGroup.value;
        const cutil = [];
        this.emmanual.forEach(emmanual => {
            (emmanual.value) ? cutil.push('S') : cutil.push('N');
        });
        form.emmanual = cutil.join('');
        this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt)
            .map(data => data = data.siEstado.dsEstados.ttEstados)
            .subscribe(data => {
                if (data[0].cErrDesc) {
                    swal('ERROR!', data[0].cErrDesc, 'error');
                } else {
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Registro guardado!!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                this.flagShowForm = false;
                this.getPais();
                this.rebuildForm();
            })
    }

    rebuildForm() {
        this.myFormGroup.reset({
            Estado: '',
            enombre: '',
            region: '',
            Pais: '',
            emmanual: '',
            Inactivo: false,
            cRowID: '',
            lError: false,
            cErrDesc: ''
        });
    }
}
