import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import {InfociaGlobalService} from '../../../../services/infocia.service';
import {CrudService} from '../../mantto-srv.services';
import {ManttoMenuService} from '../../mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import { GLOBAL } from '../../../../services/global';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-tipos-reservacion',
  templateUrl: './mantto-tipos-reservacion.component.html',
  styleUrls: ['./../../manttos-desing.css']
})
export class ManttoTiposReservacionComponent implements OnInit {
  heading = 'Mantenimientos Tipos de Reservación';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  public myFormGroup: FormGroup;
  public flagEdit: boolean;
  public flagShowForm: boolean;
  private ttInfocia: any;
  public checked = false;
  public dataComplete: any = [];
  public dataGrid: any = [];
  public filter: CompositeFilterDescriptor;
  public groups: GroupDescriptor[] = [];
  public sort: SortDescriptor[] = [{field: 'idesc', dir: 'asc'}];
  private urlSec: string = '/manttos/treser';
  private ds: string = 'dstreser';
  private tt: string = 'ttTreser';
  disponibilidad: any = [];
  public tmmanual = [];
  public gridFields = [];
  seccion: string = 'Tipos de Reservación';
  toast = GLOBAL.toast;
  modalTitle = '';
  closeResult: string;
  noRecords = GLOBAL.noRecords;
  loading = true;

  constructor(private fb: FormBuilder,
              private _info: InfociaGlobalService,
              private _crudservice: CrudService,
              private _modal: NgbModal,
              private _menuServ: ManttoMenuService,
              private _router: Router) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.gridFields = [
      {field: 'treser', title: 'T.R.', width: '90', filtro: ''},
      {field: 'tdesc', title: 'Descripción', width: '320', filtro: 'false'},
      {field: 'tdisp', title: 'Disponibilidad', width: '', filtro: ''},
      {field: 'tmmanual', title: 'Uso', width: '', filtro: ''}
    ];
    this.tmmanual = [
      {id: 'reservaciones', value: false, desc: 'Reservaciones'},
      {id: 'recepcion', value: false, desc: 'Recepción'}
    ];
    this.disponibilidad = [
      {value: 'G', desc: 'Gar'},
      {value: 'N', desc: 'NG'},
      {value: 'R', desc: 'Gpo'},
      {value: 'L', desc: 'LE'},
      {value: 'C', desc: 'Cort'},
      {value: 'U', desc: 'UC'},
      {value: 'T', desc: 'TC'},
      {value: 'W', desc: 'Walk-In'},
    ];
    this.createForm();
  }

  ngOnInit() {
    this.getInfoList();

    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  getInfoList() {
    this.loading = true;
    this._crudservice.getData(this.urlSec).subscribe(data => {
      const result = data.siTreser.dstreser.ttTreser
      if (result){
        this.dataGrid = result;
        this.dataComplete = result;
      }else {
        this.dataGrid = [];
        this.dataComplete = [];
      }
      });
      this.loading = false;
  }

  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.loadProducts();
  }

  private loadProducts(): void {
    this.dataGrid = process(this.dataComplete, {group: this.groups});
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || {logic: 'and', filters: []};
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataStructure();
  }

  private dataStructure(): void {
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

  createForm() {
    this.myFormGroup = this.fb.group({
      treser: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(2)]],
      tdesc: ['', [Validators.required, Validators.maxLength(30)]],
      tdisp: ['', [Validators.required, Validators.maxLength(1)]],
      tmmanual: [''],
      Inactivo: [false],
      cRowID: [''],
      lError: [false],
      cErrDesc: [''],
    });
  }

  public editRow({sender, rowIndex, dataItem}) {
    this.myFormGroup.setValue(dataItem);
    const arrManuales = dataItem.tmmanual.split('');
    let index = 0;
    for (const util of arrManuales) {
      this.tmmanual[index].value = (util === 'S') ? true : false;
      index++;
    }
    this.flagEdit = true;
    this.flagShowForm = true;
  }

  public deleteRow({dataItem}) {
    this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
      const result = data.siTreser;
      if (!result.dsMensajes.ttMensError){
        this.toast({
          type: 'success',
          title: GLOBAL.textDeleteSuccess
        });
        this.getInfoList();
      }else{
        this.toast({
          type: 'error',
          title: GLOBAL.textError
        });
      }
      this.flagShowForm = false;
      this.rebuildForm();

    })
  }

  cerrarForm() {
    this.flagShowForm = false;
    this.rebuildForm();
  }

  editarForm() {
    const form = this.myFormGroup.value;
    const cutil = [];
    this.tmmanual.forEach(tmmanual => {
      (tmmanual.value) ? cutil.push('S') : cutil.push('N');
    });
    form.tmmanual = cutil.join('');
    this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
      const result = data.siTreser.dstreser.ttTreser;
      if (result[0].cErrDesc){
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: result[0].cErrDesc
        });
      }else {
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        });
      this.getInfoList();
      }
      this.flagShowForm = false;
      this.rebuildForm();
    })
  }

  printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    });
  }

  guardarForm() {
    (this.flagEdit) ? this.editarForm() : this.saveRow();
  }

  newRow() {
    this.flagShowForm = true;
    this.flagEdit = false;
  }

  saveRow() {
    const form = this.myFormGroup.value;
    const cutil = [];
    this.tmmanual.forEach(tmmanual => {
      (tmmanual.value) ? cutil.push('S') : cutil.push('N');
    });
    form.tmmanual = cutil.join('');
    this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
      const result = data.siTreser.dstreser.ttTreser;
      if (result[0].cErrDesc){
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: result[0].cErrDesc
        });
      }else {
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        });
      this.getInfoList();
      }
      this.flagShowForm = false;
      this.rebuildForm();
      })
  }

  rebuildForm() {
    this.myFormGroup.reset({
      treser: '',
      tdesc: '',
      tdisp: '',
      tmmanual: '',
      Inactivo: false,
      cRowID: '',
      lError: false,
      cErrDesc: ''
    });
    const cutil = [];
    this.tmmanual.forEach(tmmanual => {
      (tmmanual.value) = false;
    });
  }

  openModal(content, name: string) {
    this.modalTitle = name;

    this._modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'sm',
      centered: true
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
