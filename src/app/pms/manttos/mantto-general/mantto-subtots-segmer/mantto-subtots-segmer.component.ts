import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import {InfociaGlobalService} from '../../../../services/infocia.service';
import {CrudService} from '../../mantto-srv.services';
import {ManttoMenuService} from '../../mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import { GLOBAL } from '../../../../services/global';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-subtots-segmer',
  templateUrl: './mantto-subtots-segmer.component.html'
})
export class ManttoSubtotsSegmerComponent implements OnInit {
  heading = 'Mantenimiento Subtotal de Segmentos';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  public myFormGroup: FormGroup;
  flagEdit: boolean;
  public flagShowForm: boolean;
  private ttInfocia: any;
  public checked = false;
  public dataComplete: any = [];
  public dataGrid: any = [];
  public filter: CompositeFilterDescriptor;
  public groups: GroupDescriptor[] = [];
  public sort: SortDescriptor[] = [{field: 'idesc', dir: 'asc'}];
  private urlSec: string = '/manttos/segsub';
  private ds: string = 'dsSegsub';
  private tt: string = 'ttSegsub';
  public gridFields = [];
  seccion: string = 'Subtotal de Segmentos';
  modalTitle = '';
  closeResult: string;
  noRecords = GLOBAL.noRecords;
  toast = GLOBAL.toast;
  loading = false;

  constructor(private fb: FormBuilder,
              private _info: InfociaGlobalService,
              private _crudservice: CrudService,
              private _modal: NgbModal,
              private _menuServ: ManttoMenuService,
              private _router: Router) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.gridFields = [
      {field: 'ssub', title: '#Subt', width: '90', filtro: ''},
      {field: 'sdesc', title: 'Descripción', width: '320', filtro: 'false'}
    ];
    this.createForm();
  }

  ngOnInit() {
    this.getInfoList();

    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  getInfoList() {
    this._crudservice.getData(this.urlSec)
      .map(data => data = data.siSegsub.dsSegsub.ttSegsub)
      .subscribe(data => {
        this.dataGrid = data;
        this.dataComplete = data;
        this.dataGrid = {
          data: orderBy(this.dataGrid, this.sort),
          total: this.dataGrid.length
        };
      });
  }

  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.loadProducts();
  }

  private loadProducts(): void {
    this.dataGrid = process(this.dataComplete, {group: this.groups});
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || {logic: 'and', filters: []};
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataStructure();
  }

  private dataStructure(): void {
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

  createForm() {
    this.myFormGroup = this.fb.group({
      ssub: ['', [Validators.required, Validators.maxLength(3)]],
      sdesc: ['', [Validators.required, Validators.maxLength(30)]],
      Inactivo: [false],
      cRowID: [''],
      lError: [false],
      cErrDesc: [''],
    });
  }

  public editRow({sender, rowIndex, dataItem}) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
  }

  public deleteRow({dataItem}) {
    this.loading = true;
    this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
      if (!data.siSegsub.dsMensajes.ttMensError){
      this.toast({
        type: 'success',
        title: GLOBAL.textDeleteSuccess
      });
      this.getInfoList();
      }else {
        this.toast({
          type: 'error',
          title: GLOBAL.textError
        });
      }
      this.flagShowForm = false;
      this.rebuildForm();
  });
  this.loading = false;
  }

  cerrarForm() {
    this.flagShowForm = false;
    this.rebuildForm();
  }

  editarForm() {
    this.loading = true;
    const form = this.myFormGroup.value;
    this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
      if (data.siSegsub.dsSegsub.ttSegsub[0].cErrDesc){
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: data.siSegsub.dsSegsub.ttSegsub[0].cErrDesc
        });
      }else {
      this.toast({
        type: 'success',
        title: GLOBAL.textSuccess
      });
      this.getInfoList();
      }
      this.flagShowForm = false;
      this.rebuildForm();
  });
    this.loading = false;
  }

  printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    })
  }

  guardarForm() {
    (this.flagEdit) ? this.editarForm() : this.saveRow();
  }

  newRow() {
    this.flagShowForm = true;
    this.flagEdit = false;
  }

  saveRow() {
    this.loading = true;
    const form = this.myFormGroup.value;
    this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
        if (data.siSegsub.dsSegsub.ttSegsub[0].cErrDesc){
          this.toast({
            type: 'error',
            title: GLOBAL.textError,
            text: data.siSegsub.dsSegsub.ttSegsub[0].cErrDesc
          });
        }else {
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        });
        this.getInfoList();
        }
        this.flagShowForm = false;
        this.rebuildForm();
    });
      this.loading = false;
  }

  rebuildForm() {
    this.myFormGroup.reset({
      ssub: '',
      sdesc: '',
      Inactivo: false,
      cRowID: '',
      lError: false,
      cErrDesc: ''
    });
  }

  openModal(content, name: string) {
    this.modalTitle = name;

    this._modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'sm',
      centered: true
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
