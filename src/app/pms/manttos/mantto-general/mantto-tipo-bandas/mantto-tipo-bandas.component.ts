import {Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import {InfociaGlobalService} from '../../../../services/infocia.service';
import 'rxjs/add/operator/map';
import {CrudService} from '../../mantto-srv.services';
import {ManttoMenuService} from '../../mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import {GLOBAL} from '../../../../services/global';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EMPTY } from 'rxjs';

const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-tipo-bandas',
  templateUrl: './mantto-tipo-bandas.component.html',
  styleUrls: ['./../../manttos-desing.css']
})
export class ManttoTipoBandasComponent implements OnInit {
  heading = 'Mantenimiento Tipo de Bandas';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  public myFormGroup: FormGroup;
  public ngFormRangos: FormGroup;
  public flagEdit: boolean;
  public flagShowForm: boolean;
  private ttInfocia: any;
  public checked = false;
  public dataComplete: any = [];
  public dataGrid: any = [];
  public filter: CompositeFilterDescriptor;
  public groups: GroupDescriptor[] = [];
  public sort: SortDescriptor[] = [{field: 'idesc', dir: 'asc'}];
  private urlSec: string = '/manttos/tbandas';
  private ds: string = 'dsTbandas';
  private tt: string = 'ttTbandas';
  seccion: string = 'Tipo de Bandas';
  dataGridR = [];
  banda = '';
  modalTitle = '';
  closeResult: string;
  noRecords = GLOBAL.noRecords;
  toast = GLOBAL.toast;
  loading = false;
  flagRango = false;

  rES = true; // true es para nuevo, false es cuando se editara el registro

  constructor(private fb: FormBuilder,
              private _info: InfociaGlobalService,
              private _crudservice: CrudService,
              private _menuServ: ManttoMenuService,
              private _modal: NgbModal,
              private _router: Router) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.createForm();
    this.creaeFormRangos();
  }

  ngOnInit() {
    this.getInfoList();

    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  getInfoList() {
    this._crudservice.getData(this.urlSec).subscribe(data => {
        if (data.siTbanda.dsMensajes.ttMensError) {
          this.dataGrid = [];
          this.dataComplete = [];
        } else {
          this.dataGrid = data.siTbanda.dsTbandas.ttTbandas;
          this.dataComplete = data.siTbanda.dsTbandas.ttTbandas;
        }
        this.loading = false;
      });
  }

  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.loadProducts();
  }

  private loadProducts(): void {
    this.dataGrid = process(this.dataComplete, {group: this.groups});
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || {logic: 'and', filters: []};
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataStructure();
  }

  private dataStructure(): void {
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

  createForm() {
    this.myFormGroup = this.fb.group({
      cFormat: ['', [Validators.maxLength(15)]],
      tbanda: ['', [Validators.required, Validators.maxLength(2)]],
      tdesc: ['', [Validators.required, Validators.maxLength(30)]],
      tvig: [false],
      tub: [''],
      Inactivo: [false],
      cRowID: [''],
      lError: [false],
      cErrDes: ['']
    });
  }

  creaeFormRangos() {
    this.ngFormRangos = this.fb.group({
      cErrDes: [''],
      cRowID: [''],
      cUserID: [''],
      iDesde: [null, [Validators.required, Validators.min(1), Validators.max(999999999)]],
      iHasta: [null, [Validators.required, Validators.min(1), Validators.max(999999999)]],
      lError: [false],
      tbanda: [''],
      tdesc: ['']
    });
  }

  public editRow({dataItem}) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
  }

  public deleteRow({dataItem}) {
    this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
      if (!data.siTbanda.dsMensajes.ttMensError){
        this.toast({
          type: 'success',
          title: GLOBAL.textDeleteSuccess
        });
        this.getInfoList();
        this.dataGridR = [];
        this.ngFormRangos.reset();
        this.flagRango = false;
      }else {
        this.toast({
          type: 'error',
          title: GLOBAL.textError
        });
      }
      this.flagShowForm = false;
      this.rebuildForm();
    });
  }

  cerrarForm() {
    this.flagShowForm = false;
    this.rebuildForm();
  }

  editarForm() {
    const form = this.myFormGroup.value;
    this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
      const result = data.siTbanda.dsTbandas;
        if (result.ttTbandas[0].cErrDes) {
          this.toast({
            type: 'error',
            title: GLOBAL.textError,
            text: result.ttTbandas[0].cErrDes
          });
        } else {
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          });
        this.getInfoList();
        }
      this.flagShowForm = false;
      this.rebuildForm();
    });
  }

  printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    });
  }

  guardarForm() {
    (this.flagEdit) ? this.editarForm() : this.saveRow();
  }

  newRow() {
    this.flagShowForm = true;
    this.flagEdit = false;
  }

  saveRow() {
    const form = this.myFormGroup.value;
    this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
      const result = data.siTbanda.dsTbandas;
        if (result.ttTbandas[0].cErrDes) {
          this.toast({
            type: 'error',
            title: GLOBAL.textError,
            text: result.ttTbandas[0].cErrDes
          });
        } else {
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          });
        this.getInfoList();
        }
        this.flagShowForm = false;
        this.rebuildForm();
      });
  }

  rebuildForm() {
    this.myFormGroup.reset({
      cFormat: '',
      tbanda: '',
      tdesc: '',
      tvig: false,
      tub: '',
      Inactivo: false,
      cRowID: '',
      lError: false,
      cErrDes: ''
    });
  }

  selectedRow({selectedRows}) {
    this.banda = selectedRows[0].dataItem.tbanda;
    this.loading = true;
    this.flagRango = true;
    this.refillRangos(this.banda);
  }

  gridRslection({selectedRows}) {
    this.rES = false;
    this.ngFormRangos.patchValue(selectedRows[0].dataItem);
  }

  rNuevo() {
    this.rES = true;

    this.ngFormRangos.patchValue({
      iDesde: '',
      iHasta: '',
      cUserID: '',
      cErrDes: '',
      cRowID: ''
    });
  }

  rGuardar() {
    this.loading = true;

    if (this.rES) {
      this._crudservice.postInfo(this.ngFormRangos.value, '/manttos/tbandaR', 'dsTbandasR', 'ttTbandaR')
      .subscribe(res => {
        if (res.siTbandasR.dsMensajes.ttMensError){
          this.toast({
            type: 'error',
            title: GLOBAL.textError,
            text: res.siTbandasR.dsTbandasR.ttTbandaR[0].cErrDes
          });
        }else{
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          });
        this.refillRangos(this.ngFormRangos.value.tbanda);
        }
      });
    } else {
      // Editar
      this._crudservice.putInfo(this.ngFormRangos.value, '/manttos/tbandaR', 'dsTbandasR', 'ttTbandaR')
      .subscribe(res => {
        if (res.siTbandasR.dsMensajes.ttMensError){
          this.toast({
            type: 'error',
            title: GLOBAL.textError,
            text: res.siTbandasR.dsTbandasR.ttTbandaR[0].cErrDes
          });
        }else{
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          });
          this.refillRangos(this.ngFormRangos.value.tbanda);
        }
      });
    }
    this.loading = false;
  }

  refillRangos(banda) {
    this.loading = true;
    // tslint:disable-next-line:triple-equals
    if (!this.banda){
      this.dataGridR = [];
      this.loading = false;
    }else {
    this.ngFormRangos.patchValue({
      tbanda: banda
    });
    this._crudservice.getData('/manttos/tbandaR/' + banda).subscribe(res => {
      if (typeof res.siTbandasR.dsTbandasR.ttTbandaR !== 'undefined') {
        this.dataGridR = res.siTbandasR.dsTbandasR.ttTbandaR;
        this.ngFormRangos.patchValue(this.dataGridR[0]);
        this.loading = false;
      } else {
        this.dataGridR = [];
        this.loading = false;
      }

    });
    }
  }

  rBorrar() {
    this.loading = true;

    this._crudservice.deleteData(this.ngFormRangos.value, '/manttos/tbandaR', 'ttTbandaR').subscribe(res => {
      if (!res.siTbandasR.dsMensajes.ttMensError){
        this.toast({
          type: 'success',
          title: GLOBAL.textDeleteSuccess
        });
        this.refillRangos(this.banda);
      }else {
        this.toast({
          type: 'error',
          title: GLOBAL.textError
        });
      }
    });
      this.rNuevo();
  }

  openModal(content, name: string) {
    this.modalTitle = name;

    this._modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'sm',
      centered: true
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
