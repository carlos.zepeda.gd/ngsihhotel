import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process } from '@progress/kendo-data-query';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { CrudService } from '../../mantto-srv.services';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { Router } from '@angular/router';
import { GLOBAL } from '../../../../services/global';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-segmentos-mercado',
  templateUrl: './mantto-segmentos-mercado.component.html',
  styleUrls: ['./../../manttos-desing.css']
})
export class ManttoSegmentosMercadoComponent implements OnInit {
  heading = 'Mantenimientos Segmentos de Mercado';
  subheading: string;
  icon: string;
  public myFormGroup: FormGroup;
  flagEdit: boolean;
  private ttInfocia: any;
  public sort: SortDescriptor[] = [{ field: 'Segmento', dir: 'asc' }];
  public checked = false;
  public dataComplete: any = [];
  public dataGrid: any = [];
  public filter: CompositeFilterDescriptor;
  private urlSec: string = '/manttos/segmentos';
  private ds: string = 'dsSegmentos';
  private tt: string = 'ttSegmentos';
  public gridFields = [];
  tipo: any = [];
  dataSubSeg: any[] = [];
  modalTitle = '';
  noRecords = GLOBAL.noRecords;
  toast = GLOBAL.toast;
  loading = false;
  loadingColor: any = GLOBAL.loadingColor;

  constructor(private fb: FormBuilder,
    private _info: InfociaGlobalService,
    private _crudservice: CrudService,
    private _modal: NgbModal,
    private _menuServ: ManttoMenuService,
    private _router: Router) { }

  ngOnInit() {
    this.initialData();

    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  getInfoList() {
    this._crudservice.getData(this.urlSec)
      .map(data => data = data.siSegmento.dsSegmentos.ttSegmentos)
      .subscribe(data => {
        this.dataGrid = data;
        this.dataComplete = data;
        this.dataGrid = {
          data: orderBy(this.dataGrid, this.sort),
          total: this.dataGrid.length
        };
      });
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || { logic: 'and', filters: [] };
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  createForm() {
    this.myFormGroup = this.fb.group({
      Segmento: ['', [Validators.required, Validators.maxLength(6)]],
      sdesc: ['', [Validators.required, Validators.maxLength(40)]],
      stipo: ['', [Validators.required]],
      ssub: ['', [Validators.required]],
      Inactivo: [false],
      cRowID: [''],
      lError: [false],
      cErrDesc: [''],
    });
  }

  public editRow({ dataItem }) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
  }

  public deleteRow({ dataItem }) {
    this.loading = true;
    this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
      if (!data.siSegmento.dsMensajes.ttMensError) {
        this.toast({ type: 'success', title: GLOBAL.textDeleteSuccess });
        this.getInfoList();
        this.createForm();
      } else {
        this.toast({ type: 'error', title: GLOBAL.textError });
      }
    })
    this.loading = false;
  }

  editarForm() {
    this.loading = true;
    const form = this.myFormGroup.value;
    this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
      const result = data.siSegmento.dsSegmentos.ttSegmentos;
      if (!result[0].cErrDesc) {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.getInfoList();
        this.createForm();
      } else {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: result[0].cErrDesc
        });
      }
    });
    this.loading = false;
  }

  printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    });
  }

  guardarForm() {
    (this.flagEdit) ? this.editarForm() : this.saveRow();
  }

  newRow() {
    this.flagEdit = false;
    this.createForm();
  }

  saveRow() {
    this.loading = true;
    const form = this.myFormGroup.value;
    this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
      const result = data.siSegmento.dsSegmentos.ttSegmentos;
      if (!result[0].cErrDesc) {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.getInfoList();
        this.createForm();
      } else {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: result[0].cErrDesc
        });
      }
      this.loading = false;
    });
  }

  openModal(content, name: string) {
    this.modalTitle = name;
    this._modal.open(content, { size: 'md', centered: true });
  }

  initialData() {
    this.ttInfocia = this._info.getSessionInfocia();
    this.gridFields = [
      { field: 'Segmento', title: 'Segmento', width: '130', filtro: 'false' },
      { field: 'sdesc', title: 'Descripción', width: '320', filtro: 'false' },
      { field: 'stipo', title: 'Tipo', width: '120', filtro: '' },
      { field: 'ssub', title: '#Sub', width: '120', filtro: '' }
    ];
    this.tipo = [
      { value: 'C', desc: 'Cortesía' },
      { value: 'U', desc: 'Uso Casa' },
      { value: 'T', desc: 'T.C.' },
      { value: 'N', desc: 'Normal' },
    ];
    this._crudservice.getData('/manttos/segsub').subscribe(data => {
      this.dataSubSeg = data.siSegsub.dsSegsub.ttSegsub;
    });
    this.createForm();
    this.getInfoList();
  }

}
