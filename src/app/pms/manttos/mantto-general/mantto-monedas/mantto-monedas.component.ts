import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SortDescriptor, filterBy, CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { CrudService } from '../../mantto-srv.services';
import { GLOBAL } from '../../../../services/global';

const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-monedas',
  templateUrl: './mantto-monedas.component.html',
  styleUrls: ['./../../manttos-desing.css']
})
export class ManttoMonedasComponent implements OnInit {
  heading = 'Mantenimiento de Monedas';
  subheading: string;
  icon: string;
  public myFormGroup: FormGroup;
  public flagEdit: boolean;
  public ttInfocia: any;
  public checked = false;
  public dataComplete: any = [];
  public dataGrid: any = [];
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'idesc', dir: 'asc' }];
  private urlSec: string = '/manttos/monedas';
  private ds: string = 'dsMonedas';
  private tt: string = 'ttMonedas';
  public gridFields = [];
  modalTitle = '';
  noRecords = GLOBAL.noRecords;
  toast = GLOBAL.toast;
  loading = false;
  loadingColor: any = GLOBAL.loadingColor;

  constructor(private _fb: FormBuilder,
    private _info: InfociaGlobalService,
    private _crudservice: CrudService,
    private _menuServ: ManttoMenuService,
    private _modal: NgbModal,
    private _router: Router) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.gridFields = [
      { field: 'Moneda', title: 'Moneda', width: '90', filtro: '' },
      { field: 'mdesc', title: 'Descripción', width: '320', filtro: 'false' },
      { field: 'Simbolo', title: 'Símbolo', width: '', filtro: '' },
      { field: 'Abreviado', title: 'Abreviado', width: '', filtro: '' }
    ];
    this.createForm();
  }

  ngOnInit() {
    this.getInfoList();
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  getInfoList() {
    this.loading = true;
    this._crudservice.getData(this.urlSec)
      .map(data => data = data.siMoneda.dsMonedas.ttMonedas)
      .subscribe(data => {
        this.dataGrid = data;
        this.dataComplete = data;
        this.loading = false;
      });
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || { logic: 'and', filters: [] };
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  createForm() {
    this.myFormGroup = this._fb.group({
      Moneda: ['', [Validators.required, Validators.maxLength(3)]],
      mdesc: ['', [Validators.required, Validators.maxLength(30)]],
      mval1: [false],
      mval2: [false],
      Simbolo: ['', [Validators.maxLength(1)]],
      Abreviado: ['', [Validators.maxLength(10)]],
      dmon1: ['', [Validators.maxLength(15)]],
      dmon2: ['', [Validators.maxLength(15)]],
      Sec: [0, [Validators.max(9999), Validators.min(0), Validators.maxLength(4)]],
      Inactivo: [false],
      cRowID: [''],
      lError: [false],
      cErrDesc: [''],
      ISOmon: ['', [Validators.maxLength(3)]]
    });
  }

  newRow() {
    this.flagEdit = false;
    this.createForm();
  }

  editRow(dataItem, modal) {
    this.myFormGroup.patchValue(dataItem);
    this.flagEdit = true;
    this.openModal(modal, 'Modificar Moneda');
  }

  guardarForm() {
    this.flagEdit ? this.crudService('edit') : this.crudService('new');
  }

  crudService(method, dataItem?) {
    let response: any;

    switch (method) {
      case 'new':
        response = this._crudservice.postInfo(this.myFormGroup.value, this.urlSec, this.ds, this.tt);
        break;
      case 'edit':
        response = this._crudservice.putInfo(this.myFormGroup.value, this.urlSec, this.ds, this.tt);
        break;
      case 'delete':
        response = this._crudservice.deleteData(dataItem, this.urlSec, this.tt);
        break;
    }
    response.subscribe(data => this.resultService(data));
  }

  resultService(data) {
    const result = data.siMoneda.dsMonedas.ttMonedas;

    if (result && result[0].cErrDesc) {
      this.toast({
        type: 'error',
        title: GLOBAL.textError,
        text: result[0].cErrDesc
      });
    } else {
      this.toast({ type: 'success', title: GLOBAL.textSuccess });
      this.getInfoList();
    }
    this.createForm();
  }

  printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    });
  }

  openModal(content, name: string) {
    this.modalTitle = name;
    this._modal.open(content, { size: 'md', centered: true });
  }
}
