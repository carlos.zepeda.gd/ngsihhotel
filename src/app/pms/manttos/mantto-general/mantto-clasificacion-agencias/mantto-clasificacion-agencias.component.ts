import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { CrudService } from '../../mantto-srv.services';
import { GLOBAL } from '../../../../services/global';

const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-clasificacion-agencias',
  templateUrl: './mantto-clasificacion-agencias.component.html',
  styleUrls: ['./../../manttos-desing.css']
})
export class ManttoClasAgenComponent implements OnInit {
  heading = 'Mantenimiento Clasificación de Agencias';
  subheading: string;
  icon: string;
  public myFormGroup: FormGroup;
  flagEdit: boolean;
  private ttInfocia: any;
  public checked = false;
  public dataComplete: any = [];
  public dataGrid: any = [];
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'idesc', dir: 'asc' }];
  private urlSec: string = '/manttos/agclasif';
  private ds: string = 'dsAgclasif';
  private tt: string = 'ttAgclasif';
  public gridFields = [];
  procTarifa: any = [];
  public ammanual = [];
  modalTitle = '';
  noRecords = GLOBAL.noRecords;
  toast = GLOBAL.toast;
  loading = false;
  loadingColor: any = GLOBAL.loadingColor;

  constructor(private fb: FormBuilder,
    private _info: InfociaGlobalService,
    private _crudservice: CrudService,
    private _modal: NgbModal,
    private _menuServ: ManttoMenuService,
    private _router: Router) { }

  ngOnInit() {
    this.ttInfocia = this._info.getSessionInfocia();
    this.gridFields = [
      { field: 'Clasif', title: 'Clasif', width: '90', filtro: 'false' },
      { field: 'adesc', title: 'Descripción', width: '320', filtro: 'false' },
      { field: 'Proc', title: 'Uso', width: '90', filtro: 'false' },
      { field: 'ammanual', title: 'Proc', width: '90', filtro: '' },
      { field: 'Cuenta', title: 'Cuenta', width: '130', filtro: '' }
    ];
    this.procTarifa = [
      { value: 'M', desc: '(M)ayorista' },
      { value: 'T', desc: '(T)odas' },
    ];
    this.ammanual = [
      { id: 'agencia', value: false, desc: 'Agencia' },
      { id: 'empresa', value: false, desc: 'Empresa' },
      { id: 'grupo', value: false, desc: 'Grupo' }
    ];
    this.createForm();
    this.getInfoList();
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  getInfoList() {
    this._crudservice.getData(this.urlSec).subscribe(data => {
      const result = data.siAgclasif.dsAgclasif.ttAgclasif
      this.dataGrid = result;
      this.dataComplete = result;
    });
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || { logic: 'and', filters: [] };
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  createForm() {
    this.myFormGroup = this.fb.group({
      Clasif: ['', [Validators.required, Validators.maxLength(6)]],
      adesc: ['', [Validators.required, Validators.maxLength(40)]],
      Proc: ['', [Validators.required]],
      ammanual: [''],
      Cuenta: ['', Validators.max(99999999999999)],
      Inactivo: [false],
      cRowID: [''],
      lError: [false],
      cErrDesc: ['']
    });
  }

  public editRow({ dataItem }) {
    this.myFormGroup.setValue(dataItem);
    const arrManuales = dataItem.ammanual.split('');
    let index = 0;
    for (const util of arrManuales) {
      this.ammanual[index].value = (util === 'S') ? true : false;
      index++;
    }
    this.flagEdit = true;
  }

  public deleteRow({ dataItem }) {
    this.loading = true;
    this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
      if (!data.siAgclasif.dsMensajes.ttMensError) {
        this.toast({ type: 'success', title: GLOBAL.textDeleteSuccess });
        this.getInfoList();
      } else {
        this.toast({ type: 'error', title: GLOBAL.textError });
      }
      this.rebuildForm();
      this.loading = false;
    });
  }

  editarForm() {
    this.loading = true;
    const form = this.myFormGroup.value;
    const cutil = [];
    this.ammanual.forEach(ammanual => {
      (ammanual.value) ? cutil.push('S') : cutil.push('N');
    });
    form.ammanual = cutil.join('');
    this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
      const result = data.siAgclasif.dsAgclasif.ttAgclasif;
      if (result[0].cErrDesc) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: result[0].cErrDesc
        });
      } else {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.getInfoList();
      }
      this.rebuildForm();
    });
    this.loading = false;
  }

  printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    });
  }

  guardarForm() {
    (this.flagEdit) ? this.editarForm() : this.saveRow();
  }

  newRow() {
    this.flagEdit = false;
    this.createForm();
  }

  saveRow() {
    this.loading = true;
    const form = this.myFormGroup.value;
    const cutil = [];
    this.ammanual.forEach(ammanual => {
      (ammanual.value) ? cutil.push('S') : cutil.push('N');
    });
    form.ammanual = cutil.join('');
    this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
      const result = data.siAgclasif.dsAgclasif.ttAgclasif;
      if (result[0].cErrDesc) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: result[0].cErrDesc
        });
      } else {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.getInfoList();
      }
      this.rebuildForm();
    });
    this.loading = false;
  }

  rebuildForm() {
    this.myFormGroup.reset({
      Clasif: '',
      adesc: '',
      Proc: '',
      ammanual: '',
      Cuenta: '',
      Inactivo: false,
      cRowID: '',
      lError: false,
      cErrDesc: ''
    });
    const cutil = [];
    this.ammanual.forEach(ammanual => {
      (ammanual.value) = false;
    });
  }

  openModal(content, name: string) {
    this.modalTitle = name;
    this._modal.open(content, { size: 'md', centered: true });
  }
}
