import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process } from '@progress/kendo-data-query';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { CrudService } from '../../mantto-srv.services';
import { GLOBAL } from '../../../../services/global';

const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-bancos',
  templateUrl: './mantto-bancos.component.html',
  styleUrls: ['./../../manttos-desing.css']
})
export class ManttoBancosComponent implements OnInit {
  heading = 'Mantenimiento Bancos';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  public myFormGroup: FormGroup;
  flagEdit: boolean;
  public flagShowForm: boolean;
  public ttInfocia: any;
  public checked = false;
  public dataComplete: any = [];
  public dataGrid: any = [];
  public filter: CompositeFilterDescriptor;
  public groups: GroupDescriptor[] = [];
  public sort: SortDescriptor[] = [{ field: 'idesc', dir: 'asc' }];
  private urlSec: string = '/manttos/bancos';
  private ds: string = 'dsBancos';
  private tt: string = 'ttBancos';
  public gridFields = [];
  seccion: string = 'Bancos';
  dataMonedas: any[] = [];
  modalTitle = '';
  closeResult: string;
  noRecords = GLOBAL.noRecords;
  toast = GLOBAL.toast;
  loading = false;

  constructor(private fb: FormBuilder,
    private _info: InfociaGlobalService,
    private _crudservice: CrudService,
    private _modal: NgbModal,
    private _menuServ: ManttoMenuService,
    private _router: Router) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.gridFields = [
      { field: 'Banco', title: 'Banco', width: '90', filtro: '' },
      { field: 'bdesc', title: 'Descripción', width: '300', filtro: 'false' },
      { field: 'bbanco', title: 'Cuenta Banco', width: '150', filtro: '' },
      { field: 'Cuenta', title: 'Cuenta', width: '150', filtro: '' }
    ];
    this._crudservice.getData('/manttos/monedas').subscribe(data => {
      this.dataMonedas = data.siMoneda.dsMonedas.ttMonedas;
    }
    );
    this.createForm();
  }

  ngOnInit() {
    this.getInfoList();

    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  getInfoList() {
    this._crudservice.getData(this.urlSec)
      .map(data => data = data.siBanco.dsBancos.ttBancos)
      .subscribe(data => {
        this.dataGrid = data;
        this.dataComplete = data;
        this.dataGrid = {
          data: orderBy(this.dataGrid, this.sort),
          total: this.dataGrid.length
        };
      });
  }

  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.loadProducts();
  }

  private loadProducts(): void {
    this.dataGrid = process(this.dataComplete, { group: this.groups });
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || { logic: 'and', filters: [] };
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataStructure();
  }

  private dataStructure(): void {
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

  createForm() {
    this.myFormGroup = this.fb.group({
      Banco: ['', [Validators.required, Validators.maxLength(3)]],
      bdesc: ['', [Validators.required, Validators.maxLength(40)]],
      bbanco: ['', [Validators.maxLength(40)]],
      Moneda: ['', [Validators.required]],
      Cuenta: ['', [Validators.maxLength(14)]],
      bcxc: [false],
      Inactivo: [false],
      caux: ['', [Validators.maxLength(20)]],
      Cuenta2: ['', [Validators.maxLength(14)]],
      caux2: ['', [Validators.maxLength(20)]],
      cRowID: [''],
      lError: [false],
      cErrDesc: [''],
    });
  }

  public editRow({ sender, rowIndex, dataItem }) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
  }

  public deleteRow({ dataItem }) {
    this.loading = true;
    this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
      if (!data.siBanco.dsMensajes.ttMensError){
      this.toast({
        type: 'success',
        title: GLOBAL.textDeleteSuccess
      });
      this.getInfoList();
      }else{
        this.toast({
          type: 'error',
          title: GLOBAL.textError
        });
      }
      this.flagShowForm = false;
      this.rebuildForm();
      this.loading = false;
    });
  }

  cerrarForm() {
    this.flagShowForm = false;
    this.rebuildForm();
  }

  editarForm() {
    this.loading = true;
    const form = this.myFormGroup.value;
    this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
      if (data.siBanco.dsBancos.ttBancos[0].cErrDesc){
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: data.siBanco.dsBancos.ttBancos[0].cErrDesc
        });
      }else {
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        });
      this.getInfoList();
      }
      this.flagShowForm = false;
      this.rebuildForm();
    });
      this.loading = false;
  }

  printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    });
  }

  guardarForm() {
    (this.flagEdit) ? this.editarForm() : this.saveRow();
  }

  newRow() {
    this.flagShowForm = true;
    this.flagEdit = false;
  }

  saveRow() {
    this.loading = true;
    const form = this.myFormGroup.value;
    this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
        if (data.siBanco.dsBancos.ttBancos[0].cErrDesc){
          this.toast({
            type: 'error',
            title: GLOBAL.textError,
            text: data.siBanco.dsBancos.ttBancos[0].cErrDesc
          });
        }else {
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          });
        this.getInfoList();
        }
        this.flagShowForm = false;
        this.rebuildForm();
      });
        this.loading = false;
  }

  rebuildForm() {
    this.myFormGroup.reset({
      Banco: '',
      bdesc: '',
      bbanco: '',
      Moneda: '',
      Cuenta: '',
      bcxc: false,
      Inactivo: false,
      caux: '',
      Cuenta2: '',
      caux2: '',
      cRowID: '',
      lError: false,
      cErrDesc: ''
    });
  }

  openModal(content, name: string) {
    this.modalTitle = name;
    this._modal.open(content, { size: 'md', centered: true });
  }
}
