import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { filterBy, CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { CrudService } from '../../mantto-srv.services';
import { GLOBAL } from '../../../../services/global';
const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-motivos-razones',
  templateUrl: './mantto-motivos-razones.component.html',
  styleUrls: ['./../../manttos-desing.css']
})
export class ManttoMotivosRazonesComponent implements OnInit {
  heading = 'Mantenimiento Motivos y/o Razones';
  subheading: string;
  icon: string;
  public myFormGroup: FormGroup;
  flagEdit: boolean;
  public ttInfocia: any;
  public checked = false;
  public dataComplete: any = [];
  public dataGrid: any = [];
  public filter: CompositeFilterDescriptor;
  public urlSec: string = '/manttos/motivos';
  public ds: string = 'dsMotivos';
  public tt: string = 'ttMotivos';
  public gridFields = [];
  public tipoMotRaz = [];
  modalTitle = '';
  noRecords = GLOBAL.noRecords;
  toast = GLOBAL.toast;
  loading = false;
  loadingColor: any = GLOBAL.loadingColor;
  
  constructor(private _fb: FormBuilder,
    private _info: InfociaGlobalService,
    private _crudservice: CrudService,
    private _modal: NgbModal,
    private _menuServ: ManttoMenuService,
    private _router: Router) { }

  ngOnInit() {
    this.ttInfocia = this._info.getSessionInfocia();
    this.gridFields = [
      { field: 'tmotivo', title: 'Tipo', width: '90', filtro: '' },
      { field: 'Motivo', title: 'Motivo', width: '120', filtro: '' },
      { field: 'Descripcion', title: 'Descripción', width: '320', filtro: '' }
    ];
    this.tipoMotRaz = [
      { value: 'U', desc: 'Upgrade' },
      { value: 'RC', desc: 'Cancelar Reservacion' },
    ];
    this.createForm();
    this.getInfoList();
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  getInfoList() {
    this.loading = true;
    this._crudservice.getData(this.urlSec).subscribe(data => {
      const result = data.siMotivo.dsMotivos.ttMotivos;
      this.dataGrid = result;
      this.dataComplete = result;
      this.loading = false;
    });
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || { logic: 'and', filters: [] };
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  createForm() {
    this.myFormGroup = this._fb.group({
      tmotivo: ['', [Validators.required]],
      Motivo: ['', [Validators.required, Validators.maxLength(10)]],
      Descripcion: ['', Validators.maxLength(40)],
      ptarifa: [false],
      Inactivo: [false],
      cRowID: [''],
      lError: [false],
      cErrDesc: ['']
    });
  }

  public editRow({ dataItem }) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
  }

  public deleteRow({ dataItem }) {
    this.loading = true;
    this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
      const result = data.siMotivo.dsMensajes.ttMensError;
      if (result) {
        this.toast({ type: 'error', title: GLOBAL.textError });
      } else {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.getInfoList();
      }
      this.createForm();
    });
    this.loading = false;
  }

  editarForm() {
    this.loading = true;
    const form = this.myFormGroup.value;
    this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
      const result = data.siMotivo.dsMotivos.ttMotivos;
      if (result[0].cErrDesc) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: result[0].cErrDesc
        });
      } else {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.getInfoList();
      }
      this.createForm();
      this.loading = false;
    });
  }

  printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    });
  }

  guardarForm() {
    this.flagEdit ? this.editarForm() : this.saveRow();
  }

  newRow() {
    this.flagEdit = false;
    this.createForm();
  }

  saveRow() {
    this.loading = true;
    const form = this.myFormGroup.value;
    this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
      const result = data.siMotivo.dsMotivos.ttMotivos;
      if (result[0].cErrDesc) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: result[0].cErrDesc
        });
      } else {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.getInfoList();
      }
      this.createForm();
    });
    this.loading = false;
  }

  openModal(content, name: string) {
    this.modalTitle = name;
    this._modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'md',
      centered: true
    });
  }
}
