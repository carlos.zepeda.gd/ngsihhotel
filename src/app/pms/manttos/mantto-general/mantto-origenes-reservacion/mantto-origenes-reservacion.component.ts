import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SortDescriptor, filterBy, CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { CrudService } from '../../mantto-srv.services';
import { GLOBAL } from '../../../../services/global';
const flatten = GLOBAL.flatten;

@Component({
    selector: 'app-mantto-origenes-reservacion',
    templateUrl: './mantto-origenes-reservacion.component.html',
    styleUrls: ['./../../manttos-desing.css']
})
export class ManttoOrigenesReservacionComponent implements OnInit {
    heading = 'Mantenimientos Orígenes de Reservación';
    subheading: string;
    icon: string;
    public myFormGroup: FormGroup;
    flagEdit: boolean;
    private ttInfocia: any;
    public checked = false;
    public dataComplete: any = [];
    public dataGrid: any = [];
    public filter: CompositeFilterDescriptor;
    public sort: SortDescriptor[] = [{ field: 'idesc', dir: 'asc' }];
    private urlSec: string = '/manttos/origenes';
    private ds: string = 'dsOrigenes';
    private tt: string = 'ttOrigenes';
    public gridFields = [];
    seccion: string = 'Origenes de Reservacion';
    modalTitle = '';
    noRecords = GLOBAL.noRecords;
    toast = GLOBAL.toast;
    loading = false;
    loadingColor: any = GLOBAL.loadingColor;

    constructor(private fb: FormBuilder,
        private _info: InfociaGlobalService,
        private _crudservice: CrudService,
        private _menuServ: ManttoMenuService,
        private _modal: NgbModal,
        private _router: Router) { }

    ngOnInit() {
        this.ttInfocia = this._info.getSessionInfocia();
        this.gridFields = [
            { field: 'Origen', title: 'Origen', width: '90', filtro: '' },
            { field: 'odesc', title: 'Descripción', width: '320', filtro: 'false' }
        ];
        this.createForm();
        this.getInfoList();
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    getInfoList() {
        this.loading = true;
        this._crudservice.getData(this.urlSec)
            .map(data => data = data.siOrigen.dsOrigenes.ttOrigenes)
            .subscribe(data => {
                this.dataGrid = data;
                this.dataComplete = data;
            });
        this.loading = false;
    }

    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.dataGrid = filterBy(this.dataComplete, filter);
    }

    public switchChange(checked: boolean): void {
        const root = this.filter || { logic: 'and', filters: [] };
        const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
        if (!filter) {
            root.filters.push({
                field: 'Inactivo',
                operator: 'eq',
                value: checked
            });
        } else {
            filter.value = checked;
        }
        this.checked = checked;
        this.filterChange(root);
    }

    createForm() {
        this.myFormGroup = this.fb.group({
            Origen: ['', [Validators.required, Validators.maxLength(3)]],
            odesc: ['', [Validators.required, Validators.maxLength(30)]],
            Inactivo: [false],
            cRowID: [''],
            lError: [false],
            cErrDesc: [''],
        });
    }

    public editRow({ dataItem }) {
        this.myFormGroup.setValue(dataItem);
        this.flagEdit = true;
    }

    public deleteRow({ dataItem }) {
        this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
            this.getInfoList();
            this.createForm();
            this.toast({ type: 'success', title: GLOBAL.textSuccess });
        })
    }

    editarForm() {
        const form = this.myFormGroup.value;
        this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
            this.getInfoList();
            this.createForm();
            this.toast({ type: 'success', title: GLOBAL.textSuccess });

        })
    }

    printPDF() {
        this.toast({
            type: 'info',
            title: 'NO DISPONIBLE!',
            text: 'En mantenimiento'
        });
    }

    guardarForm() {
        this.flagEdit ? this.editarForm() : this.saveRow();
    }

    newRow() {
        this.flagEdit = false;
        this.createForm();
    }

    saveRow() {
        const form = this.myFormGroup.value;
        this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt)
            .map(data => data = data.siOrigen.dsOrigenes.ttOrigenes)
            .subscribe(data => {
                if (data[0].cErrDesc) {
                    this.toast('ERROR!', data[0].cErrDesc, 'error');
                } else {
                    this.toast({ type: 'success', title: GLOBAL.textSuccess });
                }
                this.getInfoList();
                this.createForm();
            })
    }

    openModal(content, name: string) {
        this.modalTitle = name;
        this._modal.open(content, {
            ariaLabelledBy: 'modal-distri',
            size: 'md',
            centered: true
        });
    }

}
