import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ManttoRolesService } from './mantto-roles.service';
import { ManttoSharedService } from '../mantto-shared.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { FilterService } from '@progress/kendo-angular-grid';
import {
    SortDescriptor, orderBy, filterBy, process,
    CompositeFilterDescriptor, GroupDescriptor, FilterDescriptor
} from '@progress/kendo-data-query';
import { ManttoMenuService } from '../mantto-menu/mantto-menu.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GLOBAL } from '../../../services/global';
declare var jsPDF: any;
const flatten = GLOBAL.flatten;

@Component({
    selector: 'app-mantto-roles',
    templateUrl: './mantto-roles.component.html',
    styleUrls: ['./mantto-roles.component.css']
})
export class ManttoRolesComponent implements OnInit {
    heading = 'Roles de Usuario';
    subheading: string;
    icon: string;
    public ttInfocia: any;
    public dataGrid: any = [];
    public groups: GroupDescriptor[] = [];
    public filter: CompositeFilterDescriptor;
    private flagEdit: boolean;
    public checked: boolean;
    public sort: SortDescriptor[] = [{ field: 'cRolcod', dir: 'asc' }];
    public dsGridComplete: any = [];
    public ddlSistemas: any = [];
    public myFormGroup: FormGroup;
    flagShowGrid: boolean;
    dataItemSelected: any;
    toast = GLOBAL.toast;

    constructor(private _fb: FormBuilder,
        private _mantto: ManttoRolesService,
        private _share: ManttoSharedService,
        private _info: InfociaGlobalService,
        private _menuServ: ManttoMenuService,
        private _modalService: NgbModal,
        private _router: Router) {
        this.ttInfocia = this._info.getSessionInfocia();
        this.helpsDataSource();
        this.createFormGroup();
    }

    ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    helpsDataSource() {
        this.getRoles();
        this._share.getSistemas().subscribe(data => this.ddlSistemas = data);
    };

    getRoles() {
        this._mantto.getRolMaster().subscribe(data => {
            this.dataGrid = data;
            this.dsGridComplete = data;
            this.dataGrid = {
                data: orderBy(this.dataGrid, this.sort),
                total: this.dataGrid.length
            };
        });
    };

    refreshGrid($event) {
        this.flagShowGrid = false;
        this.getRoles();
    }

    newRecord(modal) {
        this.openModal(modal, 'sm');
        this.flagEdit = false;
        this.createFormGroup();
    }

    openModal(content, size) {
        this._modalService.open(content, { size: size, centered: true });
    }

    expandGrid({ dataItem }) {
        this.dataItemSelected = dataItem;
    }

    public editRow({ dataItem }, modal) {
        this.createFormGroup();
        this.flagEdit = true;
        this.openModal(modal, 'sm');
        this.myFormGroup.patchValue(dataItem);
    }

    guardarForm() {
        const form = this.myFormGroup.value;
        let respServ: any;

        if (this.flagEdit) {
            respServ = this._mantto.putRolMaster(form);
        } else {
            respServ = this._mantto.postRolMaster(form);
        }
        respServ.subscribe(data => {
            if (data[0].cMensTxt !== undefined) {
                this.toast({ title: data[0].cMensTxt, type: 'error' });
            } else {
                this._modalService.dismissAll();
                this.getRoles();
                this.createFormGroup();
            }
        });
    }

    public deleteRow({ dataItem }) {
        this._mantto.deleteRolMaster(dataItem).subscribe(data => {
            if (data[0].cMensTxt !== undefined) {
                this.toast({ title: data[0].cMensTxt, type: 'error' });
            }else {
                this.getRoles();
                this.createFormGroup();
            }
        });
    }

    createFormGroup() {
        this.myFormGroup = this._fb.group({
            cRolcod: ['', Validators.required],
            cRolDesc: ['', Validators.required],
            Inactivo: [false],
            cSistema: [''],
            cRowID: [''],
            cErrDesc: [''],
            lError: [false],
        });
    }

    printPDF() {
        const encabezado = this.ttInfocia.encab;
        const columns = [
            { title: 'Usuario', dataKey: 'cUserId' },
            { title: 'Nombre', dataKey: 'cUserNomb' },
            { title: 'Apellido', dataKey: 'cUserApel' },
            { title: 'Roles', dataKey: 'cRoles' },
            { title: 'cSistema', dataKey: 'Sistemas' }
        ];
        const rows = this.dataGrid.data;
        const doc = new jsPDF('p', 'pt');
        doc.setFontSize(8);
        doc.text(encabezado, 40, 30);
        doc.setFontType('normal');
        doc.setFontSize(8);
        doc.setTextColor(100);
        doc.autoTable(columns, rows, {
            theme: 'striped',
            styles: {
                cellPadding: 5, // a number, array or object (see margin below)
                fontSize: 8,
                font: 'helvetica' // helvetica, times, courier
            },
        });
        doc.output('save', 'Usuarios.pdf');
    }

    // Funciones externas para la Grid de Kendo for Angular //

    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.loadRows();
    }

    private loadRows(): void {
        this.dataGrid = process(this.dsGridComplete, { group: this.groups });
    }

    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.dataGrid.data = filterBy(this.dsGridComplete, filter);
    }

    public switchChange(checked: boolean): void {
        const root = this.filter || { logic: 'and', filters: [] };
        const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
        if (!filter) {
            root.filters.push({
                field: 'Inactivo',
                operator: 'eq',
                value: checked
            });
        } else {
            filter.value = checked;
        }
        this.checked = checked;
        this.filterChange(root);
    }

    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.dataStructure();
    }

    private dataStructure(): void {
        this.dataGrid = {
            data: orderBy(this.dataGrid.data, this.sort),
            total: this.dataGrid.data.length
        };
    };

    public categoryChange(values: any[], filterService: FilterService): void {
        filterService.filter({
            filters: values.map(value => ({
                field: 'cSistema',
                operator: 'eq',
                value
            })),
            logic: 'or'
        });
    };

    public categoryFilters(filter: CompositeFilterDescriptor): FilterDescriptor[] {
        return flatten(filter).map(({ value }) => value);
    }
}
