import { Component, Input, OnChanges, SimpleChanges, OnInit, EventEmitter, Output } from '@angular/core';
import { ManttoRolesMenuService } from './mantto-roles-menu.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-mantto-roles-menu',
  templateUrl: './mantto-roles-menu.component.html',
  styleUrls: ['./mantto-roles-menu.component.css']
})
export class ManttoRolesMenuComponent implements OnInit, OnChanges {
  @Input() data: any = [];
  @Output() refresh = new EventEmitter
  public dsOpcionesMenu: any = [];
  dataSubmenu: any;
  dataMenu: any;

  constructor(private _mantto: ManttoRolesMenuService) { }

  ngOnInit() {
  }
  getMenu(data) {
    this._mantto.getOpcionesMenu(data).subscribe(menu => {
      this.dsOpcionesMenu = menu;
    })
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.data !== undefined) {
      this.getMenu(this.data);
    }
  }
  expandSubmenu({dataItem}) {
    this.dataSubmenu = dataItem;
  }
  expandMenu({dataItem}) {
    this.dataMenu = dataItem;
  }
  agregarMenu({dataItem}) {
    this.refresh.emit(true);
    this._mantto.postMenu(dataItem, this.data.cRolcod).subscribe(menu => {
      // if (menu[0].cErrDesc) {
      //   swal('Error!', menu[0].cErrDesc, 'error');
      // }
    });
  }
  agregarSubMenu({dataItem}) {
    this.refresh.emit(true);
    // this.dataMenu.SubMenu = [{}];
    // delete this.dataMenu.SubMenu[0];
    // this.dataMenu.SubMenu.push(dataItem);
    // this._mantto.postMenu(this.dataMenu, this.data.cRolcod).subscribe(menu => {
    //   // if (menu[0].cErrDesc) {
    //   //   swal('Error!', menu[0].cErrDesc, 'error');
    //   // }
    // });
    // const data = {
    //   cRolcod: this.data.cRolcod,
    //   cSistema: this.data.cSistema,
    //   cMenu: this.dataMenu.cMenu,
    //   cSubMenu: dataItem.cSubMenu,
    //   cOpcion: '',
    //   cRowID: '',
    //   lError: false,
    //   cErrDesc: ''
    // };
    if (dataItem.Opcion) {
      for (const item of dataItem.Opcion) {
        const json = {
          cRolcod: this.data.cRolcod,
          cSistema: this.data.cSistema,
          cMenu: this.dataMenu.cMenu,
          cSubMenu: dataItem.cSubMenu,
          cOpcion: item.cOpcion,
          cRowID: '',
          lError: false,
          cErrDesc: ''
        };
        this._mantto.postRolMenu(json).subscribe(menu => {
          if (menu[0].cErrDesc) {
            swal('Error!', menu[0].cErrDesc, 'error');
          }
        });
      }
    }else {
      const json = {
        cRolcod: this.data.cRolcod,
        cSistema: this.data.cSistema,
        cMenu: this.dataMenu.cMenu,
        cSubMenu: dataItem.cSubMenu,
        cOpcion: '',
        cRowID: '',
        lError: false,
        cErrDesc: ''
      };
      this._mantto.postRolMenu(json).subscribe(menu => {
        if (menu[0].cErrDesc) {
          swal('Error!', menu[0].cErrDesc, 'error');
        }
      });
    }

  }
  agregarOpcion({dataItem}) {

    const data = {
      cRolcod: this.data.cRolcod,
      cSistema: this.data.cSistema,
      cMenu: this.dataMenu.cMenu,
      cSubMenu: this.dataSubmenu.cSubMenu,
      cOpcion: dataItem.cOpcion,
      cRowID: '',
      lError: false,
      cErrDesc: ''
    };
    this._mantto.postRolMenu(data).subscribe(menu => {
      if (menu[0].cErrDesc) {
        swal('Error!', menu[0].cErrDesc, 'error');
      }
      this.refresh.emit(true);
    });
  }
}
