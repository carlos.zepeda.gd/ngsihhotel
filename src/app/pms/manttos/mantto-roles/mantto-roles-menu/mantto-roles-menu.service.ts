import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { GLOBAL } from '../../../../services/global';
const headers = new Headers({'Content-Type': 'application/json'});
const esp = GLOBAL.char174;

@Injectable()
export class ManttoRolesMenuService {
  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }
  getOpcionesMenu(value) {
    return this.http.get( this.url + '/admin/ayuda/RolMenu/noselec/' + value.cRolcod + esp + value.cSistema)
                    .map( res => res.json().response.siMenuGral.dsMenuGral.ttMenuGral );
  }
  getOpcionesMenu2(sistema) {
    return this.http.get( this.url + '/admin/ayuda/menu/' + sistema + esp + esp)
                    .map( res => res.json().response.siMenuGral.dsMenuGral.ttMenuGral );
  }
  postMenu(data, codigo) {
    const json = JSON.stringify(data);
    const params = '{"dsMenuGral":{"ttMenuGral":[' + json + ']}}';

    return this.http.post( this.url + '/admin/manttos/RolMenu/' + codigo,  params, { headers: headers })
                    .map( res => {
                      const succes = res.json().response.siMenuGral.dsMenuGral.ttMenuGral;
                      const error  = res.json().response.siMenuGral.dsMensajes.ttMensError;
                      if (error) {
                          return error;
                      }else {
                          return succes;
                      }
                    });
  }
  postRolMenu(data) {
    const json = JSON.stringify(data);
    const params = '{"dsRolMenu":{"ttRolMenu":[' + json + ']}}';

    return this.http.post( this.url + '/admin/manttos/RolMenu',  params, { headers: headers })
                    .map( res => {
                      const succes = res.json().response.siRolMenu.dsRolMenu.ttRolMenu;
                      const error  = res.json().response.siRolMenu.dsMensajes.ttMensError;
                      if (error) {
                          return error;
                      }else {
                          return succes;
                      }
                    });
  }
  deleteRolMenu(data) {
    const json = JSON.stringify(data);
    const params = '{"dsRolMenu":{"ttRolMenu":[' + json + ']}}';

    return this.http.delete( this.url + '/admin/manttos/RolMenu', 
                    new RequestOptions({ headers: headers, body: params }))
                    .map( res => res.json().response.siRolMenu.dsRolMenu.ttRolMenu );
  }

}
