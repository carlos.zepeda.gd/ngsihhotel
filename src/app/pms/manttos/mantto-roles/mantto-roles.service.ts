import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { GLOBAL } from '../../../services/global';
const headers = new Headers({ 'Content-Type': 'application/json' });
const esp = GLOBAL.char174;

@Injectable()
export class ManttoRolesService {

  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }
  getRolMaster() {
    return this.http.get(this.url + '/admin/ayuda/RolMstr/' + esp)
      .map(res => res.json().response.siRolMstr.dsRolMstr.ttRolMstr);
  }
  postRolMaster(data) {
    const json = JSON.stringify(data);
    const params = '{"dsRolMstr":{"ttRolMstr":[' + json + ']}}';

    return this.http.post(this.url + '/admin/manttos/RolMstr', params, { headers: headers })
      .map(res => {
        const succes = res.json().response.siRolMstr.dsRolMstr.ttRolMstr;
        const error = res.json().response.siRolMstr.dsMensajes.ttMensError;
        if (error) {
          return error;
        } else {
          return succes;
        }
      });
  }
  putRolMaster(data) {
    const json = JSON.stringify(data);
    const params = '{"dsRolMstr":{"ttRolMstr":[' + json + ']}}';

    return this.http.put(this.url + '/admin/manttos/RolMstr', params, { headers: headers })
      .map(res => {
        const succes = res.json().response.siRolMstr.dsRolMstr.ttRolMstr;
        const error = res.json().response.siRolMstr.dsMensajes.ttMensError;
        if (error) {
          return error;
        } else {
          return succes;
        }
      });
  }
  deleteRolMaster(data) {
    const json = JSON.stringify(data);
    const params = '{"dsRolMstr":{"ttRolMstr":[' + json + ']}}';

    return this.http.delete(this.url + '/admin/manttos/RolMstr',
      new RequestOptions({ headers: headers, body: params }))
      .map(res => res.json().response.siRolMstr.dsRolMstr.ttRolMstr);
  }
  deleteRolMenu(data) {
    const json = JSON.stringify(data);
    const params = '{"dsRolMenu":{"ttRolMenu":[' + json + ']}}';

    return this.http.delete(this.url + '/admin/manttos/RolMenu',
      new RequestOptions({ headers: headers, body: params }))
      .map(res => res.json().response.siRolMenu.dsRolMenu.ttRolMenu);
  }
}
