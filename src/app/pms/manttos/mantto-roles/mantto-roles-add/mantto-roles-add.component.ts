import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { ManttoRolesAddService } from './mantto-roles-add.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-mantto-roles-add',
  templateUrl: './mantto-roles-add.component.html',
  styleUrls: ['./mantto-roles-add.component.css']
})
export class ManttoRolesAddComponent implements OnInit, OnChanges {
  @Input() data: any = [];
  @Output() show = new EventEmitter;
  @Output() refresh = new EventEmitter;
  public dataGrid: any = [];
  public dsOpcionesMenu: any = [];
  public myFormGroup: FormGroup;
  public widthTab: number = 550;
  public flagShowGrid: boolean;

  constructor(
    private fb: FormBuilder, 
    private _mantto: ManttoRolesAddService) { }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.data !== undefined) {
      this.getRolMenu(this.data);
    }
  }
  getRolMenu(dataItem) {
    this._mantto.getRolMenu(dataItem.cRolcod).subscribe(data => this.dataGrid = data);
  }
  addRow() {
    this.flagShowGrid = true;
    this.show.emit(true);
  }
  deleteRowSubMenu({dataItem}) {
    this._mantto.deleteRolMenu(dataItem).subscribe(data => this.refresh.emit(true));
  }
  createFormGroup() {
    this.myFormGroup = this.fb.group({
      cRolcod:     [''],
      cSistema:    [''],
      cMenu:       [''],
      cSubMenu:    [''],
      cOpcion:     [''],
      cRowID:      [''],
      cErrDesc:    [''],
      lError:      [false],
    });
  };
}
