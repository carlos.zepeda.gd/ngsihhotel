import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { GLOBAL } from '../../../../services/global';
const headers = new Headers({'Content-Type': 'application/json'});
const esp = GLOBAL.char174;

@Injectable()
export class ManttoRolesAddService {
  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }
  getRolMenu(codigo) {
    return this.http.get( this.url + '/admin/ayuda/RolMenu/' + codigo)
                    .map( res => res.json().response.siRolMenu.dsRolMenu.ttRolMenu );
  }
  getOpcionesMenu(sistema) {
    return this.http.get( this.url + '/admin/ayuda/menu/' + sistema + esp + esp)
                    .map( res => res.json().response.siMenuGral.dsMenuGral.ttMenuGral );
  }
  postRolMenu(data) {
    const json = JSON.stringify(data);
    const params = '{"dsRolMenu":{"ttRolMenu":[' + json + ']}}';

    return this.http.post( this.url + '/admin/manttos/RolMenu',  params, { headers: headers })
                    .map( res => {
                      const succes = res.json().response.siRolMenu.dsRolMenu.ttRolMenu;
                      const error  = res.json().response.siRolMenu.dsMensajes.ttMensError;
                      if (error) {
                          return error;
                      }else {
                          return succes;
                      }
                    });
  }
  deleteRolMenu(data) {
    const json = JSON.stringify(data);
    const params = '{"dsRolMenu":{"ttRolMenu":[' + json + ']}}';

    return this.http.delete( this.url + '/admin/manttos/RolMenu', 
                    new RequestOptions({ headers: headers, body: params }))
                    .map( res => res.json().response.siRolMenu.dsRolMenu.ttRolMenu );
  }

}
