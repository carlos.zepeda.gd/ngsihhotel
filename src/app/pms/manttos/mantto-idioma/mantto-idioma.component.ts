import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import {InfociaGlobalService} from '../../../services/infocia.service';
import {CrudService} from '../mantto-srv.services';
import {ManttoMenuService} from '../mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import { GLOBAL } from '../../../services/global';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

const flatten = GLOBAL.flatten;

@Component({
    selector: 'app-mantto-idioma',
    templateUrl: './mantto-idioma.component.html',
    styleUrls: ['./../manttos-desing.css']
})
export class ManttoIdiomaComponent implements OnInit {
    heading = 'Mantenimiento de Idioma';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';

    myFormGroup: FormGroup;
    flagEdit: boolean;
    public flagShowForm: boolean;
    public dsManuales = [];
    private ttInfocia: any;
    public checked = false;
    public dataComplete: any = [];
    public dataGrid: any = [];
    public filter: CompositeFilterDescriptor;
    public groups: GroupDescriptor[] = [];
    public sort: SortDescriptor[] = [{field: 'idesc', dir: 'asc'}];
    private urlSec: string = '/manttos/idiomas';
    private ds: string = 'dsIdiomas';
    private tt: string = 'ttIdiomas';
    dataIdiomas: any[] = [];
    loading = false;
    closeResult: string;
    toast = GLOBAL.toast;

    constructor(private _info: InfociaGlobalService,
                private _crudservice: CrudService,
                private _menuServ: ManttoMenuService,
                private _router: Router,
                private modal: NgbModal) {
        this.ttInfocia = this._info.getSessionInfocia();
        this.dsManuales = [
            {id: 'reservaciones', value: false, desc: 'Reservaciones'},
            {id: 'recepcion', value: false, desc: 'Recepción'},
        ];
        this.createForm();
    }

    ngOnInit() {
        this.getList();

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.loadProducts();
    }

    private loadProducts(): void {
        this.dataGrid = process(this.dataComplete, {group: this.groups});
    }

    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.dataGrid.data = filterBy(this.dataComplete, filter);
    }

    public switchChange(checked: boolean): void {
        const root = this.filter || {logic: 'and', filters: []};
        const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
        if (!filter) {
            root.filters.push({
                field: 'Inactivo',
                operator: 'eq',
                value: checked
            });
        } else {
            filter.value = checked;
        }
        this.checked = checked;
        this.filterChange(root);
    }

    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.dataStructure();
    }

    private dataStructure(): void {
        this.dataGrid = {
            data: orderBy(this.dataGrid.data, this.sort),
            total: this.dataGrid.data.length
        };
    }

    createForm() {
        this.myFormGroup = new FormGroup({
            'Idioma': new FormControl('', [Validators.required, Validators.maxLength(1)]),
            'idesc': new FormControl('', [Validators.required, Validators.maxLength(30)]),
            'iconm': new FormControl('', [Validators.required, Validators.maxLength(5)]),
            'nletra': new FormControl('', [Validators.maxLength(1)]),
            'nid': new FormControl(''),
            'Inactivo': new FormControl(false),
            'cRowID': new FormControl(''),
            'lError': new FormControl(false),
            'cErrDesc': new FormControl(''),
        })
    }

    rebuildForm() {
        this.myFormGroup.reset({
            Idioma: '', idesc: '', iconm: '', Inactivo: false, nletra: '', lError: false, cRowID: '', nid: ''
        });
    }

    guardarForm() {
        (this.flagEdit) ? this.saveRow('put') : this.saveRow('post');
    }

    cerrarForm() {
        this.flagShowForm = false;
        this.rebuildForm();
    }

    printPDF() {
      this.toast({
        type: 'info',
        title: 'NO DISPONIBLE!',
        text: 'En mantenimiento'
      });
        // const columns = [
        //     {title: 'Idioma', dataKey: 'Idioma'},
        //     {title: 'Descripción', dataKey: 'idesc'},
        //     {title: 'Conmutador', dataKey: 'iconm'},
        //     {title: 'N-letra', dataKey: 'nletra'},
        //     {title: 'Inactivo', dataKey: 'Inactivo'}
        // ];
        // this._pdfService.printPDF('Idiomas', columns, this.dataGrid.data);
    }

    openModal(content){
      this.modal.open(content, {
        ariaLabelledBy: 'modal-distri',
        size: 'sm',
        centered: true
      }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }

    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return `with: ${reason}`;
      }
    }

    newRow(content) {
        this.flagShowForm = true;
        this.flagEdit = false;
        this.rebuildForm();
        this.openModal(content);
    }

    getList() {
      this.loading = true;
        this._crudservice.getData(this.urlSec)
            .map(data => data = data.siIdioma.dsIdiomas.ttIdiomas)
            .subscribe(data => {
              if (!data){
                this.dataGrid = [];
                this.dataComplete = [];
              }else {
                this.dataGrid = data;
                this.dataComplete = data;
                this.dataGrid = {
                    data: orderBy(this.dataGrid, this.sort),
                    total: this.dataGrid.length
                };
              }
            });
        this.loading = false;
    }

    saveRow(request) {
      this.loading = true;
        const form = this.myFormGroup.value;
        let method = null;
        if (request === 'post'){
          method = this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt);
        }else {
          method = this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt);
        }
        method.subscribe(data => {
              const result = data.siIdioma.dsIdiomas.ttIdiomas;
                if (result[0].cErrDesc) {
                    this.toast({
                      type: 'error',
                      title: GLOBAL.textError,
                      text: result[0].cErrDesc
                    });
                      this.loading = false;
                } else {
                    this.toast({
                        type: 'success',
                        title: GLOBAL.textSuccess
                    })
                }
                this.flagShowForm = false;
                this.getList();
                this.rebuildForm();
            })
    }

    public editRow({dataItem}, content) {
        this.myFormGroup.setValue(dataItem);
        this.flagEdit = true;
        this.flagShowForm = true;
        this.openModal(content);
    }

    public deleteRow({dataItem}) {
      this.loading = true;
        this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
          const result = data.siIdioma.dsMensajes;
          if (result){
            this.toast({
              type: 'error',
              title: GLOBAL.textError
            });
            this.loading = false;
          }else{
            this.toast({
              type: 'success',
              title: GLOBAL.textDeleteSuccess
            });
            this.flagShowForm = false;
            this.getList();
            this.rebuildForm();
          }
        })
    }

}
