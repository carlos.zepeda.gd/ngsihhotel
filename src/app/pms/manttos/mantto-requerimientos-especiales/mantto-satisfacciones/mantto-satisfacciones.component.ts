import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import {CrudService} from '../../mantto-srv.services';
import {ManttoMenuService} from '../../mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { GLOBAL } from '../../../../services/global';

const flatten = GLOBAL.flatten;

@Component({
    selector: 'app-mantto-satisfacciones',
    templateUrl: './mantto-satisfacciones.component.html',
    styleUrls: ['./../../manttos-desing.css']
})
export class ManttoSatisfaccionesComponent implements OnInit {
    heading = 'Mantenimientos Satisfacciones';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';

    loading = false;

    private urlSec: string = '/manttos/satisfacciones';
    private siBase: string = 'siSatisfaccion';
    private ds: string = 'dsSatisfacciones';
    private tt: string = 'ttSatisfacciones';
    public gridFields = [];
    public pdfGridFields = [];
    public pdfGrid = [];
    seccion: string = 'Satisfacciones';
    formStatus: string = null;
    errorMesj: string = null;
    toast = GLOBAL.toast;

    public myFormGroup: FormGroup;
    public dataGrid: any = [];
    public flagShowForm: boolean;
    flagEdit: boolean;
    public dataComplete: any = [];
    public checked: boolean;
    public groups: GroupDescriptor[] = [];
    public filter: CompositeFilterDescriptor;
    public sort: SortDescriptor[] = [{field: 'Satisfaccion', dir: 'asc'}];

    constructor(private _crudservice: CrudService,
                private _menuServ: ManttoMenuService,
                private _router: Router,
                private modalService: NgbModal) {
        this.gridFields = [
            {title: 'Satisfacción', dataKey: 'Satisfaccion', width: '150', filtro: false},
            {title: 'Descripción', dataKey: 'sdesc', width: '250', filtro: true},
        ];
        this.pdfGridFields = [
            {title: 'Inactivo', dataKey: 'Inactivo', width: '', filtro: false},
        ];
    }

    ngOnInit() {
        this.formGrup();
        this.getDataSource();

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    private formGrup() {
        this.myFormGroup = new FormGroup({
            Satisfaccion: new FormControl('', Validators.required, Validators.maxLength[1]),
            sdesc: new FormControl('', Validators.required, Validators.maxLength[30]),
            Inactivo: new FormControl(false),
            cRowID: new FormControl(''),
            lError: new FormControl(false),
            cErrDesc: new FormControl(''),
        });
    }

    private getDataSource() {
        this.loading = true;
        this._crudservice.getData(this.urlSec).map(data =>
            data = data[this.siBase][this.ds][this.tt])
            .subscribe(data => {
                this.dataGrid = data;
                this.dataComplete = data;
                this.dataGrid = {
                    data: orderBy(this.dataGrid, this.sort),
                    total: this.dataGrid.length
                };
                this.loading = false;
            });
    }

    public newRow(modal) {
        this.formGrup();
        this.flagShowForm = true;
        this.flagEdit = false;
        this.formStatus = 'Nuevo';

        this.openModalData(modal);
    }

    public guardarForm() {
        (this.flagEdit) ? this.saveRow('put') : this.saveRow('post');
    }

    public editRow({dataItem}, modal) {
        this.myFormGroup.setValue(dataItem);
        this.flagEdit = true;
        this.flagShowForm = true;
        this.formStatus = 'Editar';

        this.openModalData(modal);
    }

    private saveRow(request) {
        const form = this.myFormGroup.value;
        let method = null;
        if (request === 'post'){
          method = this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt);
        }else {
          method = this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt);
        }
        method.subscribe(data => {
                this.errorMesj = (data[this.siBase].dsMensajes.ttMensError == null) ?
                    null : data[this.siBase].dsMensajes.ttMensError[0].cMensTxt;
                    if (this.errorMesj) {
                        this.toast({
                            type: 'error',
                            title: GLOBAL.textError,
                            text: this.errorMesj
                        });
                    } else {
                        this.toast({
                          type: 'success',
                          title: GLOBAL.textSuccess
                        });
                        this.getDataSource();
                        this.flagShowForm = false;
                    }
            })
    }

    public deleteRow({dataItem}) {
        this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
            this.errorMesj = (data[this.siBase].dsMensajes.ttMensError == null) ?
                null : data[this.siBase].dsMensajes.ttMensError[0].cMensTxt;
            if (this.errorMesj) {
                this.toast({
                    type: 'error',
                    title: GLOBAL.textError,
                    text: this.errorMesj
                });
            } else {
                this.toast({
                  type: 'success',
                  title: GLOBAL.textDeleteSuccess
                });
                this.getDataSource();
            }
            this.closeForm();
        });
    }

    public closeForm() {
        this.myFormGroup.reset();
        this.flagShowForm = false;
        this.flagEdit = false;
    }

    public close(component) {
        this[component + 'Opened'] = false;
        this.closeForm();
    }

    // Generear PDF
    public printPDF() {
      this.toast({
        type: 'info',
        title: 'NO DISPONIBLE!',
        text: 'En mantenimiento'
      })
        // const columns = this.pdfGrid.concat(this.gridFields, this.pdfGridFields);
        // this._pdfService.printPDF(this.seccion, columns, this.dataGrid.data);
    }

    // Funciones para la Grid de Kendo
    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.dataGrid = process(this.dataComplete, {group: this.groups});
    }

    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.dataGrid.data = filterBy(this.dataComplete, filter);
    }

    public switchChange(checked: boolean): void {
        const root = this.filter || {logic: 'and', filters: []};
        const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
        if (!filter) {
            root.filters.push({
                field: 'Inactivo',
                operator: 'eq',
                value: checked
            });
        } else {
            filter.value = checked;
        }
        this.checked = checked;
        this.filterChange(root);
    }

    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.dataGrid = {
            data: orderBy(this.dataGrid.data, this.sort),
            total: this.dataGrid.data.length
        };
    }

    openModalData(content) {
        this.modalService.open(content, {
            size: 'sm',
            centered: true
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
}
