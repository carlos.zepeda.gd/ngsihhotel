import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import swal from 'sweetalert2';
import {CrudService} from '../../mantto-srv.services';
import {PdfService} from '../../mantto-pdf.service';
import {ManttoMenuService} from '../../mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { GLOBAL } from '../../../../services/global';

const flatten = GLOBAL.flatten;

@Component({
    selector: 'app-mantto-departamentos',
    templateUrl: './mantto-departamentos.component.html',
    styleUrls: ['../../manttos-desing.css'],
})
export class ManttoDepartamentosComponent implements OnInit {
    heading = 'Mantenimientos de Mercados';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';

    public myFormGroup: FormGroup;
    public dataGrid: any = [];
    public flagShowForm: boolean;
    private flagEdit: boolean;
    public dataComplete: any = [];
    public checked: boolean;
    public groups: GroupDescriptor[] = [];
    public filter: CompositeFilterDescriptor;
    public sort: SortDescriptor[] = [{field: 'punto', dir: 'asc'}];

    private urlSec: string = '/manttos/deptos';
    private ds: string = 'dsDeptos'; // siDepto.dsDeptos.ttDeptos
    private tt: string = 'ttDeptos';
    public gridFields = [];
    seccion: string = 'Departamentos';
    errorMesj: string = null;
    formStatus: string = null;

    loading = false;

    constructor(private _crudservice: CrudService,
                private _pdfService: PdfService,
                private _menuServ: ManttoMenuService,
                private _router: Router,
                private modalService: NgbModal) {
        this.gridFields = [
            {field: 'depto', title: 'Depto', width: '150', filtro: false},
            {field: 'ddesc', title: 'Descripción', width: '', filtro: true},
        ];
        this.buildFormGroup();
    }

    ngOnInit() {
        this.getDataSource();

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    private buildFormGroup() {
        this.myFormGroup = new FormGroup({
            depto: new FormControl('', Validators.required),
            ddesc: new FormControl('', Validators.required),
            Inactivo: new FormControl(false),
            cErrDesc: new FormControl(''),
            lError: new FormControl(false),
            cRowID: new FormControl(''),
        });
    }

    private getDataSource() {
        this.loading = true;
        this._crudservice.getData(this.urlSec)
            .map(data => data = data.siDepto.dsDeptos.ttDeptos)
            .subscribe(data => {
                this.dataGrid = data;
                this.dataComplete = data;
                this.dataGrid = {
                    data: orderBy(this.dataGrid, this.sort),
                    total: this.dataGrid.length
                };
                this.loading = false;
            });
    }

    public newRow(modal) {
        this.buildFormGroup();
        this.flagShowForm = true;
        this.flagEdit = false;
        this.formStatus = 'Nuevo';

        this.openModalData(modal);
    }

    public guardarForm() {
        (this.flagEdit) ? this.updateForm() : this.saveRow();
    }

    public editRow({dataItem}, modal) {
        this.myFormGroup.setValue(dataItem);
        this.flagEdit = true;
        this.flagShowForm = true;
        this.formStatus = 'Editar';

        this.openModalData(modal);
    }

    private saveRow() {
        const form = this.myFormGroup.value;
        this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt)
            .subscribe(data => {
                this.errorMesj = (data.siDepto.dsMensajes.ttMensError == null) ?
                    null : data.siDepto.dsMensajes.ttMensError[0].cMensTxt;
                if (this.errorMesj) {
                    swal('ERROR!', this.errorMesj, 'error');
                } else {
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Registro guardado!!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                this.flagShowForm = false;
                this.getDataSource();
                this.closeForm();
            })
    }

    private updateForm() {
        const form = this.myFormGroup.value;
        this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
            this.getDataSource();
            this.closeForm();
        });
    }

    public deleteRow({dataItem}) {
        this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
            this.errorMesj = (data.siDepto.dsMensajes.ttMensError == null) ?
                null : data.siDepto.ttMensError[0].cMensTxt;
            if (this.errorMesj) {
                swal({
                    position: 'center',
                    type: 'error',
                    title: 'Error!!',
                    text: this.errorMesj,
                    showConfirmButton: false,
                    timer: 1500
                })
            } else {
                this.getDataSource();
            }
            this.closeForm();
        });
    }

    public closeForm() {
        this.myFormGroup.reset();
        this.flagShowForm = false;
        this.flagEdit = false;
    }

    public close(component) {
        this[component + 'Opened'] = false;
        this.closeForm();
    }

    // Generear PDF
    public printPDF() {
        const columns = [
            {title: 'Depto', dataKey: 'depto'},
            {title: 'Descripción', dataKey: 'ddesc'},
            {title: 'Inactivo', dataKey: 'Inactivo'},
        ];
        this._pdfService.printPDF(this.seccion, columns, this.dataGrid.data);
    }

    // Funciones para la Grid de Kendo
    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.dataGrid = process(this.dataComplete, {group: this.groups});
    }

    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.dataGrid.data = filterBy(this.dataComplete, filter);
    }

    public switchChange(checked: boolean): void {
        const root = this.filter || {logic: 'and', filters: []};
        const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
        if (!filter) {
            root.filters.push({
                field: 'Inactivo',
                operator: 'eq',
                value: checked
            });
        } else {
            filter.value = checked;
        }
        this.checked = checked;
        this.filterChange(root);
    }

    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.dataGrid = {
            data: orderBy(this.dataGrid.data, this.sort),
            total: this.dataGrid.data.length
        };
    }

    openModalData(content) {
        this.modalService.open(content, {
            size: 'lg'
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
}
