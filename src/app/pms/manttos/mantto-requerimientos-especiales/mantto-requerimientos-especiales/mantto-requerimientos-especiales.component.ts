import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { SortDescriptor, CompositeFilterDescriptor, GroupDescriptor } from '@progress/kendo-data-query';
import { CrudService } from '../../mantto-srv.services';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../../services/global';
import { SharedService } from 'app/services/shared.service';

@Component({
  selector: 'app-mantto-requerimientos-especiales',
  templateUrl: './mantto-requerimientos-especiales.component.html',
  styleUrls: ['./../../manttos-desing.css']
})
export class ManttoRequerimientosEspecialesComponent implements OnInit {
  heading = 'Mantenimiento de Requerimientos';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  private urlSec: string = '/manttos/reqesp';
  private siBase: string = 'siReqesp';
  private ds: string = 'dsReqesp';
  private tt: string = 'ttReqesp';
  public gridFields = [];
  public pdfGridFields = [];
  public pdfGrid = [];
  seccion: string = 'Requerimientos Especiales';
  formStatus: string = null;
  errorMesj: string = null;
  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public flagShowForm: boolean;
  public flagEdit: boolean;
  public dataComplete: any = [];
  public checked: boolean;
  public groups: GroupDescriptor[] = [];
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'rreqesp', dir: 'asc' }];
  loading = false;
  arrNotas = [];
  arrInicio = [];
  arrTermino = [];
  arrOpcion = [];
  arrTrabajo = [];
  arrDeptos = [];
  arrGrupos = [];
  selectedItem = [];
  deptoSelectedItem = [];
  deptoseSelectedItem = [];
  deptosaSelectedItem = [];
  reqgrpSelectedItem = [];
  dsCodigos: any = [];
  norecords = GLOBAL.noRecords;
  pageSize = 10;
  skip = 0;
  toast = GLOBAL.toast;
  constructor(private _crudservice: CrudService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _modalService: NgbModal,
    private _fb: FormBuilder,
    private _share: SharedService) {
    this.gridFields = [
      { title: 'Código', dataKey: 'rreqesp', width: '170', filtro: true },
      { title: 'Descripción', dataKey: 'rdesc', width: '250', filtro: true },
      { title: 'No. Depto', dataKey: 'depto', width: '150', filtro: false },
      { title: 'No. Depto Seg.', dataKey: 'deptose', width: '150', filtro: false },
      { title: 'No. Depto Sat.', dataKey: 'deptosa', width: '150', filtro: false },
    ];
    this.pdfGridFields = [
      { title: 'Inactivo', dataKey: 'Inactivo', width: '', filtro: false },
    ];
    this.getDataSource();
    this.myFormGroup = this.formGrup();
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  private formGrup() {
    return this._fb.group({
      Alerta: [0],
      EMail: [''],
      Inactivo: [false],
      Trabajo: [''],
      cCodigo: [''],
      cDepto: [''],
      cDeptosa: [''],
      cDeptose: [''],
      cErrDesc: [''],
      cReqgrp: [''],
      cRowID: [''],
      cTrabajo: [''],
      codigo: [''],
      depto: ['', Validators.required],
      deptosa: ['', Validators.required],
      deptose: ['', Validators.required],
      lError: [false],
      rcosto: [false],
      rcto: [false],
      rdesc: ['', [Validators.required, Validators.maxLength(30)]],
      rdisp: [false],
      rdur: [0],
      reqgrp: ['', Validators.required],
      rini: [''],
      rmmanual: [''],
      rnotas: [''],
      rreqesp: ['', [Validators.required, Validators.maxLength(6)]],
      rter: [''],
      rtotal: [0],
      opt1: [null],
      opt2: [null],
      opt3: [null],
      ini1: [null],
      ini2: [null],
      term1: [null],
      term2: [null],
      notas1: [null],
      notas2: [null],
      notas3: [null]
    });
  }

  private getDataSource() {
    this.loading = true;
    this._share.getCodigos('d', '3', 'y').subscribe(data => {
      if (data) {
        data.forEach(item => item.cdesc = item.cdesc + ' - ' + item.Codigo);
        this.dsCodigos = data;
      }
    });
    this._crudservice.getData(this.urlSec)
      .map(data => data = data[this.siBase][this.ds][this.tt])
      .subscribe(data => {
        this.dataGrid = data;
        this.dataComplete = data;

        this._crudservice.getData('/manttos/trabajos')
          .map(res => res = res.siTrabajo.dsTrabajos.ttTrabajos)
          .subscribe(res => {
            this.arrTrabajo = res;

            this._crudservice.getData('/manttos/deptos')
              .map(result => result = result.siDepto.dsDeptos.ttDeptos)
              .subscribe(result => {
                this.arrDeptos = result;

                this._crudservice.getData('/manttos/reqgrupo')
                  .map(response => response = response.siReqgrp.dsReqgrp.ttReqgrp)
                  .subscribe(response => {
                    this.arrGrupos = response;
                    this.loading = false;
                  });
              });
          });
      });
  }

  public newRow(modal) {
    this.myFormGroup = this.formGrup();
    this.flagShowForm = true;
    this.flagEdit = false;
    this.formStatus = 'Nuevo';
    this.deptoSelectedItem = [];
    this.deptoseSelectedItem = [];
    this.deptosaSelectedItem = [];
    this.reqgrpSelectedItem = [];
    this.selectedItem = [];
    this.openModalData(modal);
  }

  public guardarForm() {
    this.flagEdit ? this.updateForm() : this.saveRow();
  }

  public editRow({ dataItem }, modal) {
    this.arrOpcion = dataItem.rmmanual.split('');
    this.arrNotas = dataItem.rnotas.split('');
    this.arrInicio = dataItem.rini.split('');
    this.arrTermino = dataItem.rter.split('');

    this.myFormGroup.patchValue({
      opt1: (this.arrOpcion[0] === 'S' ? true : false),
      opt2: (this.arrOpcion[1] === 'S' ? true : false),
      opt3: (this.arrOpcion[2] === 'S' ? true : false),
      ini1: (this.arrNotas[0] === 'S' ? true : false),
      ini2: (this.arrNotas[1] === 'S' ? true : false),
      notas1: (this.arrNotas[0] === 'S' ? true : false),
      notas2: (this.arrNotas[1] === 'S' ? true : false),
      notas3: (this.arrNotas[2] === 'S' ? true : false),
      term1: (this.arrTermino[0] === 'S' ? true : false),
      term2: (this.arrTermino[1] === 'S' ? true : false)
    });

    this.myFormGroup.patchValue(dataItem);

    for (let i = 0; i < this.arrTrabajo.length; i++) {
      if (this.arrTrabajo[i].trabajo === dataItem.Trabajo) {
        this.selectedItem = this.arrTrabajo[i];
      }
    }

    for (let i = 0; i < this.arrDeptos.length; i++) {
      if (this.arrDeptos[i].depto === dataItem.depto) {
        this.deptoSelectedItem = this.arrDeptos[i];
      }
      if (this.arrDeptos[i].depto === dataItem.deptose) {
        this.deptoseSelectedItem = this.arrDeptos[i];
      }
      if (this.arrDeptos[i].depto === dataItem.deptosa) {
        this.deptosaSelectedItem = this.arrDeptos[i];
      }
    }

    for (let i = 0; i < this.arrGrupos.length; i++) {
      if (this.arrGrupos[i].Reqgrp === dataItem.reqgrp) {
        this.reqgrpSelectedItem = this.arrGrupos[i];
      }
    }

    this.flagEdit = true;
    this.flagShowForm = true;
    this.formStatus = 'Editar';
    this.openModalData(modal);
  }

  openModalData(modal) {
    this._modalService.open(modal, { size: 'lg', centered: true });
  }


  private saveRow() {
    this.myFormGroup.patchValue({
      rmmanual: (this.myFormGroup.value.opt1 ? 'S' : 'N') + (this.myFormGroup.value.opt2 ? 'S' : 'N') +
      (this.myFormGroup.value.opt3 ? 'S' : 'N'),
      rnotas: (this.myFormGroup.value.notas1 ? 'S' : 'N') + (this.myFormGroup.value.notas2 ? 'S' : 'N') +
      (this.myFormGroup.value.notas3 ? 'S' : 'N'),
      rini: (this.myFormGroup.value.ini1 ? 'S' : 'N') + (this.myFormGroup.value.ini2 ? 'S' : 'N'),
      rter: (this.myFormGroup.value.term1 ? 'S' : 'N') + (this.myFormGroup.value.term2 ? 'S' : 'N'),
      Trabajo: (typeof this.myFormGroup.value.Trabajo !== 'object' || this.myFormGroup.value.Trabajo !== null) ?
      this.myFormGroup.value.Trabajo.trabajo : '',
      depto: this.myFormGroup.value.depto.length !== 0 ? this.myFormGroup.value.depto.depto : '',
      deptose: this.myFormGroup.value.deptose.length !== 0 ? this.myFormGroup.value.deptose.depto : '',
      deptosa: this.myFormGroup.value.deptosa.length !== 0 ? this.myFormGroup.value.deptosa.depto : '',
      reqgrp: this.myFormGroup.value.reqgrp.length !== 0 ? this.myFormGroup.value.reqgrp.Reqgrp : ''
    });

    this._crudservice.postInfo(this.myFormGroup.value, this.urlSec, this.ds, this.tt).subscribe(data => {
      this.errorMesj = (data[this.siBase].dsMensajes.ttMensError == null) ?
        null : data[this.siBase].dsMensajes.ttMensError[0].cMensTxt;
      if (this.errorMesj) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: this.errorMesj
          });
      } else {
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        });
      }
      this.flagShowForm = false;
      this.getDataSource();
    });
  }

  private updateForm() {
    this.myFormGroup.patchValue({
      rmmanual: (this.myFormGroup.value.opt1 ? 'S' : 'N') + (this.myFormGroup.value.opt2 ? 'S' : 'N') +
      (this.myFormGroup.value.opt3 ? 'S' : 'N'),
      rnotas: (this.myFormGroup.value.notas1 ? 'S' : 'N') + (this.myFormGroup.value.notas2 ? 'S' : 'N') +
      (this.myFormGroup.value.notas3 ? 'S' : 'N'),
      rini: (this.myFormGroup.value.ini1 ? 'S' : 'N') + (this.myFormGroup.value.ini2 ? 'S' : 'N'),
      rter: (this.myFormGroup.value.term1 ? 'S' : 'N') + (this.myFormGroup.value.term2 ? 'S' : 'N'),
      Trabajo: (typeof this.myFormGroup.value.Trabajo !== 'object' || this.myFormGroup.value.Trabajo !== null) ?
      this.myFormGroup.value.Trabajo.trabajo : '', /*ternario*/
      depto: this.myFormGroup.value.depto.length !== 0 ? this.myFormGroup.value.depto.depto : '',
      deptose: this.myFormGroup.value.deptose.length !== 0 ? this.myFormGroup.value.deptose.depto : '',
      deptosa: this.myFormGroup.value.deptosa.length !== 0 ? this.myFormGroup.value.deptosa.depto : '',
      reqgrp: this.myFormGroup.value.reqgrp.length !== 0 ? this.myFormGroup.value.reqgrp.Reqgrp : ''
    });

    this._crudservice.putInfo(this.myFormGroup.value, this.urlSec, this.ds, this.tt).subscribe(data => {
      this.errorMesj = (data[this.siBase].dsMensajes.ttMensError == null) ?
        null : data[this.siBase].dsMensajes.ttMensError[0].cMensTxt;
        if (this.errorMesj) {
          this.toast({
            type: 'error',
            title: GLOBAL.textError,
            text: this.errorMesj
            });
        } else {
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          });
        }
        this.flagShowForm = false;
        this.getDataSource();
    });
  }

  public deleteRow({ dataItem }) {
    this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
      this.errorMesj = (data[this.siBase].dsMensajes.ttMensError == null) ?
        null : data[this.siBase].dsMensajes.ttMensError[0].cMensTxt;
        if (this.errorMesj) {
          this.toast({
            type: 'error',
            title: GLOBAL.textError,
            text: this.errorMesj
            });
        } else {
          this.toast({
            type: 'success',
            title: GLOBAL.textDeleteSuccess
          });
        }
        this.flagShowForm = false;
        this.getDataSource();
    });
  }

  public printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    })
    // const columns = this.pdfGrid.concat(this.gridFields, this.pdfGridFields);
    // this._pdfService.printPDF(this.seccion, columns, this.dataGrid);
  }

  public cerrarForm(){

  }
}
