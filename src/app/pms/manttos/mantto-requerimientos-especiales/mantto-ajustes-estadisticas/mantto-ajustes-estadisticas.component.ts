import {Component, OnInit} from '@angular/core';
import {GLOBAL} from '../../../../services/global';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CompositeFilterDescriptor, distinct, filterBy} from '@progress/kendo-data-query';
import {InfociaGlobalService} from '../../../../services/infocia.service';
import {ManttoMenuService} from '../../mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import {CrudService} from '../../mantto-srv.services';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PageChangeEvent} from '@progress/kendo-angular-grid';
import moment from 'moment';

@Component({
  selector: 'app-mantto-ajustes-estadisticas',
  templateUrl: './mantto-ajustes-estadisticas.component.html',
  styleUrls: ['./mantto-ajustes-estadisticas.component.css']
})
export class ManttoAjustesEstadisticasComponent implements OnInit {
  heading = 'Mantenimiento Ajustes Estadísticas';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  loading = false;
  pageSize = 10;
  skip = 0;
  closeResult: string;
  noRecords = GLOBAL.noRecords;
  ttInfocia: any = [];

  ngForm: FormGroup;
  dataGrid: any = [];
  dataMovimientos: any = [];
  MovimientosPos: any = [];
  TiposMovimientosNom: any = [];
  banderaEditar = false;
  selectedItem = null;

  toast = GLOBAL.toast;

  modalTitle = '';
  modalType = '';

  apiMov: Array<string> = ['/manttos/codtipos', 'dsCodtipos', 'ttCodtipos'];

  public filter: CompositeFilterDescriptor;
  public dataComplete: any = [];

  ajusteDrop: Array<{ text: string, value: string }> = [
    {text: 'Agencias', value: 'PA'},
    {text: 'Empresas', value: 'PE'},
    {text: 'Segmentos', value: 'SM'},
    {text: 'Origen Gegráfico', value: 'OG'},
    {text: 'Origen Reservación', value: 'OR'},
  ];
  ajusteDropSelec;

  Codigo = '';

  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');

  constructor(private _info: InfociaGlobalService,
              private _menuServ: ManttoMenuService,
              private _router: Router,
              private _crudservice: CrudService,
              private formBuild: FormBuilder,
              private modal: NgbModal) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.ngForm = this.createForm();

    this.getTipoMov();
    this.buscar();
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  createForm() {
    return this.formBuild.group({
      Ajuste: [''],
      Fecha: [moment(this.ttInfocia.fday).toDate()],
      Codigo: [''],
      Tipo: ['', [Validators.required]],
      nctos: [0],
      Adultos: [0],
      Juniors: [0],
      Menores: [0],
      MenorCC: [0],
      aIng: [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]],
      aImp: [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]],
      cRowID: [''],
      lError: [false],
      cErrDesc: ['']
    });
  }

  pageChange({skip, take}: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  openModal(content, type) {
    switch (type) {
      case 'PA':
        this.modalType = type;
        this.modalTitle = 'Agencias';
        break;
      case 'PE':
        this.modalType = type;
        this.modalTitle = 'Empresas';
        break;
      case 'SM':
        this.modalType = type;
        this.modalTitle = 'Segmentos';
        break;
      case 'OG':
        this.modalType = type;
        this.modalTitle = 'Origen Geográfico';
        break;
      case 'OR':
        this.modalType = type;
        this.modalTitle = 'Origen Reservación';
        break;
      case 'crt':
        this.modalType = type;
        this.modalTitle = 'Tipo de Cuarto';
        break;
    }

    this.modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'md',
      centered: true
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  getDataModal(data, type) {
    switch (type) {
      case 'PA':
        this.Codigo = data.Agencia;
        break;
      case 'PE':
        this.Codigo = data.Empresa;
        break;
      case 'SM':
        this.Codigo = data.Segmento;
        break;
      case 'OG':
        this.Codigo = data.Estado;
        break;
      case 'OR':
        this.Codigo = data.Origen;
        break;
      case 'crt':
        this.ngForm.patchValue({Tipo: data.tcuarto});
        break;
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getDataCuarto(cuarto) {
    this._crudservice.getData('/llaves/entradareq/' + cuarto).subscribe(res => {
      if (res.siHuesreq.dsHuesreq.ttHuesreq !== undefined) {
        this.dataGrid = res.siHuesreq.dsHuesreq.ttHuesreq;
      } else {
        this.dataGrid = [];
      }
    });
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  guardar() {
    if (!this.ajusteDropSelec){
      this.toast({
        type: 'info',
        title: 'Selecciona un Ajuste'
      })
    }else if (!this.Codigo){
      this.toast({
        type: 'info',
        title: 'Selecciona un Código'
      })
    }else {
      this.ngForm.value.Fecha = moment(this.ngForm.value.Fecha).format(GLOBAL.formatIso);
      this.ngForm.value.Ajuste = this.ajusteDropSelec.value;
      this.ngForm.value.Codigo = this.Codigo;

      this.loading = true;
      let method = null;
      if (this.banderaEditar === true){
      method = this._crudservice.putInfo(this.ngForm.value, '/manttos/estajustes', 'dsEstajustes', 'ttEstajustes');
      }else {
      method = this._crudservice.postInfo(this.ngForm.value, '/manttos/estajustes', 'dsEstajustes', 'ttEstajustes');
      }
      method.subscribe(res => {
        if (!res.siEstajuste.dsEstajustes.ttEstajustes[0].cErrDesc) {
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          });
          this.getDataCuarto(this.ngForm.value.cuarto);
          this.buscar();
          this.nuevo();
        } else {
          this.toast({
            type: 'error',
            title: res.siEstajuste.dsEstajustes.ttEstajustes[0].cErrDesc
          });
        }

        this.loading = false;
      });
    }

  }

  nuevo() {
    this.Codigo = '';
    this.banderaEditar = false;
    this.ngForm.reset({
      Ajuste: '',
      Fecha: moment(this.ttInfocia.fday).toDate(),
      Codigo: '',
      Tipo: '',
      nctos: 0,
      Adultos: 0,
      Juniors: 0,
      Menores: 0,
      MenorCC: 0,
      aIng: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      aImp: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      cRowID: '',
      lError: false,
      cErrDesc: ''
    });
    this.ajusteDropSelec = null;
  }

  buscar() {
    this.loading = true;
      this._crudservice.getData('/manttos/estajustes')
        .subscribe(res => {
          if (res.siEstajuste.dsEstajustes.ttEstajustes) {
            this.dataGrid = res.siEstajuste.dsEstajustes.ttEstajustes;
          } else {
            this.dataGrid = [];
          }
          this.loading = false;
        });
      this.loading = false;
  }

  getTipoMov(){
    this._crudservice.getData(this.apiMov[0]).subscribe(res => {
      this.dataMovimientos = res.siCodtipo.dsCodtipos.ttCodtipos;
      var aux;
      for(var i = 0; i < this.dataMovimientos.length; i++){
        for(var j = i + 1; j < this.dataMovimientos.length; j++){
          if(this.dataMovimientos[i].cpos > this.dataMovimientos[j].cpos){
            aux = this.dataMovimientos[i];
            this.dataMovimientos[i] = this.dataMovimientos[j];
            this.dataMovimientos[j] = aux;
          }else if(this.dataMovimientos[i].cpos == this.dataMovimientos[j].cpos){
            this.dataMovimientos[i] = this.dataMovimientos[j];
            this.dataMovimientos.splice(j, 1);
          }
        }
        this.MovimientosPos = this.dataMovimientos;
        this.TiposMovimientosNom[i] = this.MovimientosPos[i].cdesc;
      }
    });

  }

  public selectRow({ selectedRows }) {
    this.banderaEditar = true;
    this.selectedItem = selectedRows[0].dataItem;
    this.selectedItem.Fecha = moment(this.selectedItem.Fecha).toDate();
    this.ngForm.patchValue(this.selectedItem);
    this.Codigo = this.ngForm.value.Codigo;
    this.changeAjuste();
  }

  public changeAjuste(){
    let cont = 0;
    switch (this.ngForm.value.Ajuste){
      case 'PA':
        cont = 0;
        this.ajusteDropSelec = this.ajusteDrop[cont];
        break;
      case 'PE':
        cont = 1;
        this.ajusteDropSelec = this.ajusteDrop[cont];
        break;
      case 'SM':
        cont = 2;
        this.ajusteDropSelec = this.ajusteDrop[cont];
        break;
      case 'OG':
        cont = 3;
        this.ajusteDropSelec = this.ajusteDrop[cont];
        break;
      case 'OR':
        cont = 4;
        this.ajusteDropSelec = this.ajusteDrop[cont];
        break;
    }
  }


  eliminar(e) {
    this._crudservice.deleteData(e, '/manttos/estajustes', 'ttEstajustes').subscribe(res => {
      if (res.siEstajuste.dsMensajes.ttMensError){
        this.toast({
          type: 'error',
          title: GLOBAL.textError
        });
      }else {
        this.toast({
          type: 'success',
          title: GLOBAL.textDeleteSuccess
        });
      this.buscar();
      this.nuevo();
      }
    });
  }

}
