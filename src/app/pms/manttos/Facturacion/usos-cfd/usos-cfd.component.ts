import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { CrudService } from '../../../mantto-srv.services';
import { GLOBAL } from '../../../../services/global';

@Component({
  selector: 'app-usos-cfd',
  templateUrl: './usos-cfd.component.html',
})
export class UsosCFDComponent implements OnInit {
  heading = 'Usos de CFD';
  subheading: string;
  icon: string;
  loading = true;
  pageSize = 10;
  skip = 0;
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  toast = GLOBAL.toast;
  filter: CompositeFilterDescriptor;
  flagNuevo = true;
  loadingColor: any = GLOBAL.loadingColor;

  constructor(public _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _crud: CrudService,
    private _router: Router,
    private _formBuild: FormBuilder) { }

  ngOnInit() {
    this.createForm();
    this.consultarGet();
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  createForm() {
    this.ngForm = this._formBuild.group({
      Inactivo: [false],
      cClave: ['', [Validators.required, Validators.maxLength(8)]],
      cDesc: ['', [Validators.required, Validators.maxLength(200)]],
      cErrDesc: [''],
      cRowID: [''],
      cVal: [''],
      lError: [false]
    });
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  getRow({ selectedRows }) {
    this.flagNuevo = false;
    this.ngForm.patchValue(selectedRows[0].dataItem);
  }

  nuevo() {
    this.flagNuevo = true;
    this.createForm();
  }

  guardar() {
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    let method = null;
    if (this.flagNuevo) {
      method = this._crud.postInfo(temp, '/manttos/cfd/Usos', 'dsCfdayuda', 'ttCfdUsos');
    } else {
      method = this._crud.putInfo(temp, '/manttos/cfd/Usos', 'dsCfdayuda', 'ttCfdUsos');
    }
    method.subscribe(res => {
      if (!res.siCfdayuda.dsCfdayuda.ttCfdUsos[0].cErrDesc) {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.consultarGet();
      } else {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: res.siCfdayuda.dsCfdayuda.ttCfdUsos[0].cErrDesc
        });
      }
      this.loading = false;
    });
  }

  consultarGet() {
    this.loading = true;
    this._crud.getData('/manttos/cfd/Usos').subscribe(res => {
      const result = res.siCfdayuda.dsCfdayuda.ttCfdUsos;
      if (result) {
        this.dataGrid = result;
      } else {
        this.dataGrid = [];
      }
      this.loading = false;
      this.nuevo();
    });
  }

  eliminar(data) {
    this.loading = true;
    this._crud.deleteData(data, '/manttos/cfd/Usos', 'ttCfdUsos').subscribe(res => {
      if (!res.siCfdayuda.dsCfdayuda.ttCfdUsos[0].cErrDesc) {
        this.toast({ type: 'success', title: GLOBAL.textDeleteSuccess });
        this.consultarGet();
      } else {
        this.toast({ type: 'error', title: GLOBAL.textError });
      }
      this.loading = false;
    });
  }
}
