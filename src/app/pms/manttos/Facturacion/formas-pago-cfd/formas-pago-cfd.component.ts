import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { CrudService } from '../../../mantto-srv.services';
import { GLOBAL } from '../../../../services/global';

@Component({
  selector: 'app-formas-pago-cfd',
  templateUrl: './formas-pago-cfd.component.html',
})
export class FormasPagoCFDComponent implements OnInit {
  heading = 'Formas de Pago CFD';
  subheading: string;
  icon: string;
  loading = true;
  loadingColor: any = GLOBAL.loadingColor;
  pageSize = 10;
  skip = 0;
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  toast = GLOBAL.toast;
  filter: CompositeFilterDescriptor;
  flagNuevo = true;
  metodoDrop = [];
  
  constructor(public _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _crud: CrudService,
    private _router: Router,
    private _formBuild: FormBuilder) {
  }

  ngOnInit() {
    this.createForm();
    this.consultarGet();
    this.consultarMetodos();
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  createForm() {
    this.ngForm = this._formBuild.group({
      Inactivo: [false],
      cClave: ['', [Validators.required, Validators.maxLength(8)]],
      cDesc: ['', [Validators.required, Validators.maxLength(50)]],
      cErrDesc: [''],
      cRowID: [''],
      cVal: [''],
      lError: [false],
      cfdMetodo: ['']
    });
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  getRow({ selectedRows }) {
    this.flagNuevo = false;
    this.ngForm.patchValue(selectedRows[0].dataItem);
  }

  nuevo() {
    this.flagNuevo = true;
    this.createForm();
  }

  guardar() {
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    let method = null;
    if (this.flagNuevo) {
      method = this._crud.postInfo(temp, '/manttos/cfd/FP', 'dsCfdayuda', 'ttCfdFP');
    } else {
      method = this._crud.putInfo(temp, '/manttos/cfd/FP', 'dsCfdayuda', 'ttCfdFP');
    }
    method.subscribe(res => {
      const result = res.siCfdayuda.dsCfdayuda.ttCfdFP;
      if (result && result[0].cErrDesc) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: result[0].cErrDesc
        });
      } else {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.consultarGet();
      }
      this.loading = false;
    });

  }

  consultarGet() {
    this.loading = true;
    this._crud.getData('/manttos/cfd/FP').subscribe(res => {
      const result = res.siCfdayuda.dsCfdayuda.ttCfdFP;
      if (result !== undefined) {
        this.dataGrid = result;
      } else {
        this.dataGrid = [];
      }
      this.loading = false;
      this.nuevo();
    });
  }

  consultarMetodos() {
    this.loading = true;
    this._crud.getData('/manttos/cfd/metodos').subscribe(res => {
      const result = res.siCfdayuda.dsCfdayuda.ttCfdMetodos;
      if (result) {
        this.metodoDrop = result;
      } else {
        this.metodoDrop = [];
      }
      this.loading = false;
    });
  }

  eliminar(data) {
    this.loading = true;
    this._crud.deleteData(data, '/manttos/cfd/FP', 'ttCfdFP').subscribe(res => {
      const result = res.siCfdayuda.dsCfdayuda.ttCfdFP;
      if (result && result[0].cErrDesc) {
        this.toast({ type: 'error', title: GLOBAL.textError });
      } else {
        this.toast({ type: 'success', title: GLOBAL.textDeleteSuccess });
        this.consultarGet();
      }
      this.loading = false;
    });
  }
}
