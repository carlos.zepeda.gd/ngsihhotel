import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor, distinct, filterBy } from '@progress/kendo-data-query';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { CrudService } from '../../../mantto-srv.services';
import { GLOBAL } from '../../../../services/global';

@Component({
  selector: 'app-metodos-pago-cfd',
  templateUrl: './metodos-pago-cfd.component.html',
})
export class MetodosPagoCFDComponent implements OnInit {
  heading = 'Métodos de Pago CFD';
  subheading: string;
  icon: string;
  loading = true;
  pageSize = 10;
  skip = 0;
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  toast = GLOBAL.toast;
  public filter: CompositeFilterDescriptor;
  public dataComplete: any = [];
  flagNuevo = true;
  checBox = [false, false];

  constructor(public _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _crud: CrudService,
    private _router: Router,
    private _formBuild: FormBuilder) {
  }

  ngOnInit() {
    this.createForm();
    this.consultarGet();
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  createForm() {
    this.ngForm = this._formBuild.group({
      Inactivo: [false],
      cClave: ['', [Validators.required, Validators.maxLength(8)]],
      cDesc: ['', [Validators.required, Validators.maxLength(200)]],
      cErrDesc: [''],
      cRowID: [''],
      cVal: [''],
      lError: [false],
      cTipoComp: ['', [Validators.maxLength(10)]]
    });
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  getRow({ selectedRows }) {
    this.flagNuevo = false;
    this.ngForm.patchValue(selectedRows[0].dataItem);
    const checBox = this.ngForm.value.cVal.split(',');

    for (let cont = 0; cont < checBox.length; cont++) {
      if (checBox[cont] === 'yes') {
        this.checBox[cont] = true;
      } else {
        this.checBox[cont] = false;
      }
    }
  }

  nuevo() {
    this.flagNuevo = true;
    this.checBox = [false, false];
  }

  guardar() {
    this.loading = true;

    let auxCheStr = '';
    for (let cont = 0; cont < this.checBox.length; cont++) {
      if (this.checBox[cont]) {
        auxCheStr += 'yes';
      } else {
        auxCheStr += 'no';
      }

      if ((cont + 1) < this.checBox.length) {
        auxCheStr += ',';
      }
    }
    this.ngForm.patchValue({ cVal: auxCheStr });

    // arrStr += '{"dsCfdayuda":{"ttCfdMetodos":[' + JSON.stringify(this.ngForm.value) + ']}}';
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    let method = null;
    if (this.flagNuevo) {
      method = this._crud.postInfo(temp, '/manttos/cfd/Metodos', 'dsCfdayuda', 'ttCfdMetodos');
    } else {
      method = this._crud.putInfo(temp, '/manttos/cfd/Metodos', 'dsCfdayuda', 'ttCfdMetodos');
    }
    method.subscribe(res => {
      const result = res.siCfdayuda.dsCfdayuda.ttCfdMetodos;
      if (!result[0].cErrDesc) {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.consultarGet();
      } else {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: result[0].cErrDesc
        });
        this.loading = false;
      }
    });
  }

  consultarGet() {
    this.loading = true;
    this._crud.getData('/manttos/cfd/Metodos').subscribe(res => {
      const result = res.siCfdayuda.dsCfdayuda.ttCfdMetodos;
      if (result !== undefined) {
        this.dataGrid = result;
      } else {
        this.dataGrid = [];
      }
      this.loading = false;
      this.nuevo();
    });
  }

  eliminar(data) {
    this.loading = true;
    this._crud.deleteData(data, '/manttos/cfd/Metodos', 'ttCfdMetodos').subscribe(res => {
      const result = res.siCfdayuda.dsCfdayuda.ttCfdMetodos;
      if (!result[0].cErrDesc) {
        this.toast({ type: 'success', title: GLOBAL.textDeleteSuccess });
        this.consultarGet();
      } else {
        this.toast({ type: 'error', title: GLOBAL.textError });
      }
      this.loading = false;
    });
  }

}
