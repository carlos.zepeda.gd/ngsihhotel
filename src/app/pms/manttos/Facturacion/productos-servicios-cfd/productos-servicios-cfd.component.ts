import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { CrudService } from '../../../mantto-srv.services';
import { GLOBAL } from '../../../../services/global';

@Component({
  selector: 'app-productos-servicios-cfd',
  templateUrl: './productos-servicios-cfd.component.html',
})
export class ProductosServiciosCFDComponent implements OnInit {
  heading = 'Productos y Servicios CFD';
  subheading: string;
  icon: string;
  loading = true;
  pageSize = 10;
  skip = 0;
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  toast = GLOBAL.toast;
  filter: CompositeFilterDescriptor;
  flagNuevo = true;
  loadingColor: any = GLOBAL.loadingColor;

  constructor(public _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _crud: CrudService,
    private _router: Router,
    private _formBuild: FormBuilder) { }

  ngOnInit() {
    this.createForm();
    this.consultarGet();
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  createForm() {
    this.ngForm = this._formBuild.group({
      Inactivo: [false],
      cClave: [null, [Validators.required, Validators.maxLength(8)]],
      cDesc: [null, [Validators.required, Validators.maxLength(50)]],
      cErrDesc: [''],
      cRowID: [''],
      cVal: [''],
      lError: [false]
    });
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  selectedRow({ selectedRows }) {
    this.flagNuevo = false;
    this.ngForm.patchValue(selectedRows[0].dataItem);
  }

  nuevo() {
    this.flagNuevo = true;
    this.createForm();
  }

  guardar() {
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    let method = null;
    if (this.flagNuevo) {
      method = this._crud.postInfo(temp, '/manttos/cfd/PS', 'dsCfdayuda', 'ttCfdPS');
    } else {
      method = this._crud.putInfo(temp, '/manttos/cfd/PS', 'dsCfdayuda', 'ttCfdPS');
    }
    method.subscribe(res => {
      const result = res.siCfdayuda.dsCfdayuda.ttCfdPS;
      if (!result[0].cErrDesc) {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.consultarGet();
        this.nuevo();
      } else {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: result[0].cErrDesc
        });
        this.loading = false;
      }
    });

  }

  consultarGet() {
    this.loading = true;
    this._crud.getData('/manttos/cfd/PS').subscribe(res => {
      const result = res.siCfdayuda.dsCfdayuda.ttCfdPS;
      if (result) {
        this.dataGrid = result;
      } else {
        this.dataGrid = [];
      }
      this.loading = false;
    });
  }

  eliminar(data) {
    this.loading = true;
    this._crud.deleteData(data, '/manttos/cfd/PS', 'ttCfdPS').subscribe(res => {
      if (!res.siCfdayuda.dsCfdayuda.ttCfdPS[0].cErrDesc) {
        this.toast({ type: 'success', title: GLOBAL.textDeleteSuccess });
        this.consultarGet();
        this.nuevo();
      } else {
        this.toast({ type: 'error', title: GLOBAL.textError });
      }
      this.loading = false;
    });
  }

}
