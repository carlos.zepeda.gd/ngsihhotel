import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ManttoUsuariosService } from './mantto-usuarios.service';
import { SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process } from '@progress/kendo-data-query';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { dsRestricciones } from './data';
import { ManttoMenuService } from '../mantto-menu/mantto-menu.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GLOBAL } from 'app/services/global';
declare var jsPDF: any;
const flatten = GLOBAL.flatten

@Component({
    selector: 'app-mantto-usuarios',
    templateUrl: './mantto-usuarios.component.html',
    styleUrls: ['./mantto-usuarios.component.css']
})
export class ManttoUsuariosComponent implements OnInit {
    heading = 'Mantenimiento de Usuarios';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';
    loading = false;
    public dataGrid: any = [];
    public groups: GroupDescriptor[] = [];
    public filter: CompositeFilterDescriptor;
    public checked: boolean;
    private flagEdit: boolean;
    public ttInfocia: any;
    public formUserPms: FormGroup;
    public formUserConf: FormGroup;
    public cSistemas: any = [];
    public cRoles: any = [];
    private dsGridComplete: any = [];
    public dropdownRoles: any = [];
    public dropdownSistemas: any = [];
    public dsGrupos: any = [];
    public dsDeptos: any = [];
    public dsMeseros: any = [];
    public dsIngAgrup: any = [];
    public dsTipos: any = [];
    public sort: SortDescriptor[] = [{ field: 'cUserId', dir: 'asc' }];
    public dsRestricciones: { nombre: string; field: string, iSec: number; opciones: { id: string; value: boolean; desc: string; }[]; }[];
    public changePassword: boolean;
    toast = GLOBAL.toast;

    constructor(private _fb: FormBuilder,
        private _mantto: ManttoUsuariosService,
        private _info: InfociaGlobalService,
        private _menuServ: ManttoMenuService,
        private _router: Router,
        private _modalService: NgbModal) {
        this.ttInfocia = this._info.getSessionInfocia();
        this._mantto.getSistemas().subscribe(data => this.dropdownSistemas = data);
        this._mantto.getRoles().subscribe(data => this.dropdownRoles = data);
        this._mantto.getGrupos().subscribe(data => this.dsGrupos = data);
        this._mantto.getDeptos().subscribe(data => this.dsDeptos = data);
        this._mantto.getMeseros().subscribe(data => this.dsMeseros = data);
        this._mantto.getIngAgrupado().subscribe(data => this.dsIngAgrup = data);
        this.dsTipos = [{ tipo: 'LOCAL', tipoId: true }, { tipo: 'REMOTO', tipoId: false }];
        this.dsRestricciones = dsRestricciones;
        this.getUsuarios();
        this.createFormConf();
        this.createFormPms();
    }

    ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
            }
        });
    }

    // METODOS PRINCIPALES
    getUsuarios() {
        this.loading = true;
        this._mantto.getUsuarios().subscribe(data => {
            this.dataGrid = data;
            this.dsGridComplete = data;
            this.dataGrid = {
                data: orderBy(this.dataGrid, this.sort),
                total: this.dataGrid.length
            };
            this.loading = false;
        });
    }

    newRecord(modal) {
        this.createFormConf();
        this.createFormPms();
        this.openModalData(modal);
        this.flagEdit = false;
        this.marcarRequeridos();
    }

    public editRow({ dataItem }, modal) {
        // if (dataItem.cRoles) {
        //     dataItem.cRoles = dataItem.cRoles.toUpperCase();
        // }
        this.cRoles = dataItem.cRoles.split(',');
        this.cSistemas = dataItem.cSistemas.split(',');
        dataItem.cUserPwd = '';
        dataItem.dPwdUltimo = '';
        this.formUserConf.patchValue(dataItem);

        this._mantto.getUsuarioPms(dataItem.cUserId).subscribe(data => {
            this.dsRestricciones.forEach(item => {
                if (item.field === 'cVal1') {
                    let index = 0;
                    const array = data[0].cVal1.split('');
                    for (const val of item.opciones) {
                        if (array[index] === 'N') {
                            val.value = false;
                        } else {
                            val.value = true;
                        }
                        index++;
                    }
                }
                if (item.field === 'cVal2') {
                    let index = 0;
                    const array = data[0].cVal2.split('');
                    for (const val of item.opciones) {
                        if (array[index] === 'N') {
                            val.value = false;
                        } else {
                            val.value = true;
                        }
                        index++;
                    }
                }
                if (item.field === 'cVal3') {
                    let index = 0;
                    const array = data[0].cVal3.split('');
                    for (const val of item.opciones) {
                        if (array[index] === 'N') {
                            val.value = false;
                        } else {
                            val.value = true;
                        }
                        index++;
                    }
                }
                if (item.field === 'cVal4') {
                    let index = 0;
                    const array = data[0].cVal4.split('');
                    for (const val of item.opciones) {
                        if (array[index] === 'N') {
                            val.value = false;
                        } else {
                            val.value = true;
                        }
                        index++;
                    }
                }
                if (item.field === 'cVal5') {
                    let index = 0;
                    const array = data[0].cVal5.split('');
                    for (const val of item.opciones) {
                        if (array[index] === 'N') {
                            val.value = false;
                        } else {
                            val.value = true;
                        }
                        index++;
                    }
                }
                if (item.field === 'cVal6') {
                    let index = 0;
                    const array = data[0].cVal6.split('');
                    for (const val of item.opciones) {
                        if (array[index] === 'N') {
                            val.value = false;
                        } else {
                            val.value = true;
                        }
                        index++;
                    }
                }
                if (item.field === 'cVal7') {
                    let index = 0;
                    const array = data[0].cVal7.split('');
                    for (const val of item.opciones) {
                        if (array[index] === 'N') {
                            val.value = false;
                        } else {
                            val.value = true;
                        }
                        index++;
                    }
                }
                if (item.field === 'cVal8') {
                    let index = 0;
                    const array = data[0].cVal8.split('');
                    for (const val of item.opciones) {
                        if (array[index] === 'N') {
                            val.value = false;
                        } else {
                            val.value = true;
                        }
                        index++;
                    }
                }
                if (item.field === 'cVal9') {
                    let index = 0;
                    const array = data[0].cVal9.split('');
                    for (const val of item.opciones) {
                        if (array[index] === 'N') {
                            val.value = false;
                        } else {
                            val.value = true;
                        }
                        index++;
                    }
                }
                if (item.field === 'cReq') {
                    let index = 0;
                    const array = data[0].cReq.split('');
                    for (const val of item.opciones) {
                        if (array[index] === 'N') {
                            val.value = false;
                        } else {
                            val.value = true;
                        }
                        index++;
                    }
                }
                if (item.field === 'cRest') {
                    let index = 0;
                    const array = data[0].cRest.split('');
                    for (const val of item.opciones) {
                        if (array[index] === 'N') {
                            val.value = false;
                        } else {
                            val.value = true;
                        }
                        index++;
                    }
                }
            });
            data[0].cUserPwd = '';
            // data[0].cVal14 = data[0].cUserPwd;
            this.formUserPms.patchValue(data[0]);
        })
        this.flagEdit = true;
        this.openModalData(modal);
        this.marcarRequeridos();
    }

    public deleteRow({ dataItem }) {
        this._mantto.deleteUserConf(dataItem).subscribe(data => {
            if (data[0].cMensTxt) {
                this.toast(data[0].cMensTit, data[0].cMensTxt, 'error');
            } else {
                this.getUsuarios();
            }
        })
    }

    updateUserConf() {
        this.formUserConf.patchValue({
            cSistemas: this.cSistemas.join(','),
            cRoles: this.cRoles.join(','),
            dPwdUltimo: ''
        });
        this._mantto.putUserConf(this.formUserConf.value).subscribe(data => {
            if (data[0].cMensTxt) {
                this.toast(data[0].cMensTit, data[0].cMensTxt, 'error');
            } else {
                this.updateUserPms();
            }
        });
    }
    guardarForm() {
        if (this.flagEdit) {
            this.updateUserConf();
        } else {
            this.saveUserConf();
        }
    }
    updateUserPms() {
        this.parseFormUserPms();
        this.formUserPms.patchValue({
            cVal14: '',
            cUserId: this.formUserConf.value.cUserId,
            cUserPwd: this.formUserConf.value.cUserPwd
        });
        this._mantto.putUserPms(this.formUserPms.value).subscribe(data => {
            this._modalService.dismissAll();
            this.getUsuarios();
            this.createFormConf();
            this.createFormPms();
            this._info.getUserCia().subscribe(user => {
                localStorage.clear();
                localStorage.setItem('globalInfocia', JSON.stringify(user.ttInfocia[0]));
                localStorage.setItem('globalUser', JSON.stringify(user.ttUsuarios[0]));
            });
        });
    }
    saveUserConf() {
        this.formUserConf.patchValue({
            cRoles: this.cRoles.join(','),
            cSistemas: this.cSistemas.join(','),
            dPwdUltimo: ''
        });
        this._mantto.postUserConf(this.formUserConf.value).subscribe(data => {
            if (data[0].cMensTxt) {
                this.toast(data[0].cMensTit, data[0].cMensTxt, 'error');
            } else {
                this.updateUserPms();
            }
        });
    }
    parseFormUserPms() {
        const formUserPms = this.formUserPms;
        this.dsRestricciones.forEach(item => {
            if (item.field === 'cVal1') {
                const array = [];
                for (const val of item.opciones) {
                    val.value ? array.push('S') : array.push('N');
                }
                formUserPms.patchValue({ cVal1: array.join('') });
            }
            if (item.field === 'cVal2') {
                const array = [];
                for (const val of item.opciones) {
                    val.value ? array.push('S') : array.push('N');
                }
                formUserPms.patchValue({ cVal2: array.join('') });
            }
            if (item.field === 'cVal3') {
                const array = [];
                for (const val of item.opciones) {
                    val.value ? array.push('S') : array.push('N');
                }
                formUserPms.patchValue({ cVal3: array.join('') });
            }
            if (item.field === 'cVal4') {
                const array = [];
                for (const val of item.opciones) {
                    val.value ? array.push('S') : array.push('N');
                }
                formUserPms.patchValue({ cVal4: array.join('') });
            }
            if (item.field === 'cVal5') {
                const array = [];
                for (const val of item.opciones) {
                    val.value ? array.push('S') : array.push('N');
                }
                formUserPms.patchValue({ cVal5: array.join('') });
            }
            if (item.field === 'cVal6') {
                const array = [];
                for (const val of item.opciones) {
                    val.value ? array.push('S') : array.push('N');
                }
                formUserPms.patchValue({ cVal6: array.join('') });
            }
            if (item.field === 'cVal7') {
                const array = [];
                for (const val of item.opciones) {
                    val.value ? array.push('S') : array.push('N');
                }
                formUserPms.patchValue({ cVal7: array.join('') });
            }
            if (item.field === 'cVal8') {
                const array = [];
                for (const val of item.opciones) {
                    val.value ? array.push('S') : array.push('N');
                }
                formUserPms.patchValue({ cVal8: array.join('') });
            }
            if (item.field === 'cVal9') {
                const array = [];
                for (const val of item.opciones) {
                    val.value ? array.push('S') : array.push('N');
                }
                formUserPms.patchValue({ cVal9: array.join('') });
            }
            if (item.field === 'cReq') {
                const array = [];
                for (const val of item.opciones) {
                    val.value ? array.push('S') : array.push('N');
                }
                formUserPms.patchValue({ cReq: array.join('') });
            }
            if (item.field === 'cRest') {
                const array = [];
                for (const val of item.opciones) {
                    val.value ? array.push('S') : array.push('N');
                }
                formUserPms.patchValue({ cRest: array.join('') });
            }
        });
    }

    // METODOS FORM GROUP FOR ANGULAR
    createFormConf() {
        this.formUserConf = this._fb.group({
            cUserId: ['', Validators.required],
            Inactivo: [false],
            lUserBlock: [false],
            cUserNomb: ['', Validators.required],
            cUserApel: ['', Validators.required],
            cUserEmail: [''],
            dtUserAlta: [''],
            dtUserActiv: [''],
            dtUserInactiv: [''],
            cSistemas: [[], Validators.required],
            cRoles: [['PSCUser'], Validators.required],
            cUserDomain: [''],
            cUserFirma: [''],
            cUserFoto: [''],
            iUserNum: [''],
            cErrDesc: [''],
            lError: [false],
            cRowID: [''],
            cUserPwd: [''],
            dPwdUltimo: ['']
        }, {
            validator: this.compararPassword('cUserPwd', 'dPwdUltimo')
        });
    }

    compararPassword(pass: string, passConfirm: string) {
        if (!passConfirm) {
            return;
        }
        return (group: FormGroup) => {
            const passwordInput = group.controls[pass];
            const passwordConfirmInput = group.controls[passConfirm];
            if (passwordInput.value) {
                passwordConfirmInput.setErrors(null);
            }
            if (passwordInput.value !== passwordConfirmInput.value) {
                return passwordConfirmInput.setErrors({ notEquivalent: true });
            } else {
                return passwordConfirmInput.setErrors(null);
            }
        }
    }

    createFormPms() {
        this.formUserPms = this._fb.group({
            cUserId: [''],
            cUserPwd: [''],
            Grupo: [0, Validators.required],
            Depto: ['', Validators.required],
            lTipo: [false],
            iMax: [0],
            lDel: [false],
            lExport: [false],
            cReq: [''],
            cRest: [''],
            Iagrp: [''],
            Clasif: [''],
            Mesero: [''],
            cVal1: [''],
            cVal2: [''],
            cVal3: [''],
            cVal4: [''],
            cVal5: [''],
            cVal6: [''],
            cVal7: [''],
            cVal8: [''],
            cVal9: [''],
            cVal10: [''],
            cVal11: [''],
            cVal12: [''],
            cVal13: [''],
            cVal14: [''],
            cVal15: [''],
            cVal31: [''],
            cVal32: [''],
            cErrDesc: [''],
            lError: [false],
            cRowID: ['']
        });
    }

    // METODOS EXPORTABLES DE INFORMACIÓN
    printPDF() {
        const encabezado = this.ttInfocia.encab;
        const columns = [
            { title: 'Usuario', dataKey: 'cUserId' },
            { title: 'Nombre', dataKey: 'cUserNomb' },
            { title: 'Apellido', dataKey: 'cUserApel' },
            { title: 'Roles', dataKey: 'cRoles' },
            { title: 'cSistemas', dataKey: 'Sistemas' }
        ];
        const rows = this.dataGrid.data;
        const doc = new jsPDF('p', 'pt');
        doc.setFontSize(8);
        doc.text(encabezado, 40, 30);
        doc.setFontType('normal');
        doc.setFontSize(8);
        doc.setTextColor(100);
        doc.autoTable(columns, rows, {
            theme: 'striped',
            styles: {
                cellPadding: 5, // a number, array or object (see margin below)
                fontSize: 8,
                font: 'helvetica' // helvetica, times, courier
            },
        });
        doc.output('save', 'Usuarios.pdf');
    }

    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.loadRows();
    }

    private loadRows(): void {
        this.dataGrid = process(this.dsGridComplete, { group: this.groups });
    }

    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.dataGrid.data = filterBy(this.dsGridComplete, filter);
    }

    public switchChange(checked: boolean): void {
        const root = this.filter || { logic: 'and', filters: [] };
        const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
        if (!filter) {
            root.filters.push({
                field: 'Inactivo',
                operator: 'eq',
                value: checked
            });
        } else {
            filter.value = checked;
        }
        this.checked = checked;
        this.filterChange(root);
    }

    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.dataStructure();
    }

    private dataStructure(): void {
        this.dataGrid = {
            data: orderBy(this.dataGrid.data, this.sort),
            total: this.dataGrid.data.length
        };
    }

    openModalData(modal) {
        this._modalService.open(modal, { size: 'lg', centered: true });
    }

    marcarRequeridos() {
        Object.keys(this.formUserConf.controls).forEach(key => {
            this.formUserConf.get(key).markAsDirty();
            this.formUserConf.get(key).markAsTouched();
        });
        Object.keys(this.formUserPms.controls).forEach(key => {
            this.formUserPms.get(key).markAsDirty();
            this.formUserPms.get(key).markAsTouched();
        });
    }
}
