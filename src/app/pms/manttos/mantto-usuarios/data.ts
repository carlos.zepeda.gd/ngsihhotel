export const dsRestricciones = [
    {
        nombre: 'Caja Movimientos',
        field: 'cVal1',
        iSec: 1,
        opciones: [
            { id: 'cm1', value: false, desc: 'Cancelar' },
            { id: 'cm2', value: false, desc: 'Transferir' },
            { id: 'cm3', value: false, desc: 'Ajustar' },
            { id: 'cm4', value: false, desc: 'Cargos en Rojo' },
            { id: 'cm5', value: false, desc: 'Separar' },
            { id: 'cm6', value: false, desc: 'FP en Negro' },
            { id: 'cm7', value: false, desc: 'Separar Creditos' }
        ]
    },
    {
        nombre: 'Req. Especiales',
        field: 'cReq',
        iSec: 2,
        opciones: [
            { id: 're1', value: false, desc: 'Mensaje' },
            { id: 're2', value: false, desc: 'Enterado' },
            { id: 're3', value: false, desc: 'Inicio' },
            { id: 're4', value: false, desc: 'Terminado' },
            { id: 're5', value: false, desc: 'Cancelado' },
            { id: 're6', value: false, desc: 'Satisfacción' },
            { id: 're7', value: false, desc: 'Borrar' }
        ]
    },
    {
        nombre: 'Reservaciones',
        field: 'cRest',
        iSec: 3,
        opciones: [
            { id: 'rv1', value: false, desc: 'Clave Rest.' },
            { id: 'rv2', value: false, desc: 'Depositos Pend.' }
        ]
    },
    {
        nombre: 'Tarifas',
        field: 'cVal7',
        iSec: 4,
        opciones: [
            { id: 'ta1', value: false, desc: 'Manules' },
            { id: 'ta2', value: false, desc: 'Suplementos' },
            { id: 'ta3', value: false, desc: 'Tarifas y Fechas' },
            { id: 'ta4', value: false, desc: 'Tipo Cuarto' },
            { id: 'ta5', value: false, desc: 'Personas' },
            { id: 'ta6', value: false, desc: 'Tarifas Rest.' }
        ]
    },
    {
        nombre: 'Consultas Reserv.',
        field: 'cVal8',
        iSec: 5,
        opciones: [
            { id: 'cr1', value: false, desc: 'Mensajes' },
            { id: 'cr2', value: false, desc: 'Notas' },
            { id: 'cr3', value: false, desc: 'Req. Especiales' },
            { id: 'cr4', value: false, desc: '' },
            { id: 'cr5', value: false, desc: 'Cuentas' },
            { id: 'cr6', value: false, desc: 'Depositos' },
            { id: 'cr7', value: false, desc: 'Solicitud' }
        ]
      }, 
      {
        nombre: 'Consultas Huesped',
        field: 'cVal5',
        iSec: 6,
        opciones: [
            { id: 'ch1', value: false, desc: 'Mensajes' },
            { id: 'ch2', value: false, desc: 'Notas' },
            { id: 'ch3', value: false, desc: 'Req. Especiales' },
            { id: 'ch4', value: false, desc: 'Cargos' },
            { id: 'ch5', value: false, desc: 'Cuentas' },
            { id: 'ch6', value: false, desc: 'Adicionales' },
            { id: 'ch7', value: false, desc: 'Bandas' } 
        ]
      },
      {
        nombre: 'P.V. Cheques',
        field: 'cVal4',
        iSec: 7,
        opciones: [
            { id: 'pvc1', value: false, desc: 'Cxld Normal' },
            { id: 'pvc2', value: false, desc: 'Cxld Pagado' },
            { id: 'pvc3', value: false, desc: 'Cancelar Comanda' },
            { id: 'pvc4', value: false, desc: 'Pagar Cuenta' },
            { id: 'pvc5', value: false, desc: 'Ver Cuentas' },
            { id: 'pvc6', value: false, desc: 'Cambiar FP' },
            { id: 'h', value: false, desc: 'Reabrir' }
        ]
      },
      {
        nombre: 'P.V. Reservar',
        field: 'cVal3',
        iSec: 8,
        opciones: [
            { id: 'pvr1', value: false, desc: 'Huesped' },
            { id: 'pvr2', value: false, desc: 'Reservacón' },
            { id: 'pvr3', value: false, desc: 'Maestra' },
            { id: 'pvr4', value: false, desc: 'Borrar' }
        ]
      },
      {
        nombre: 'SPA',
        field: 'cVal2',
        iSec: 9,
        opciones: [
            { id: 'spa1', value: false, desc: 'Cancelar' },
            { id: 'spa2', value: false, desc: 'Reactivar' }
        ]
      },
      {
        nombre: 'Teléfonos',
        field: 'cVal6',
        iSec: 10,
        opciones: [
            { id: 'te1', value: false, desc: 'Despertador' }
        ]
      },
      {
        nombre: 'Rest. Seguridad',
        field: 'cVal9',
        iSec: 10,
        opciones: [
            { id: 'rs1', value: false, desc: 'Vista' }
        ]
      }
];
