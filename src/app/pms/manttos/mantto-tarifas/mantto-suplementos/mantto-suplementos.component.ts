import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { filterBy, CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { CrudService } from '../../mantto-srv.services';
import { PdfService } from '../../mantto-pdf.service';
import { GLOBAL } from '../../../../services/global';
import moment from 'moment';
import { InfociaGlobalService } from 'app/services/infocia.service';

@Component({
  selector: 'app-mantto-suplementos',
  templateUrl: './mantto-suplementos.component.html',
  styleUrls: ['./../../manttos-desing.css']
})
export class ManttoSuplementosComponent implements OnInit {
  heading = 'Mantenimiento de Suplementos';
  subheading: string;
  icon: string;
  private api: Array<string> = ['/manttos/suplementos', 'siSuplemento', 'dsSuplementos', 'ttSuplementos'];
  public gridFields = [];
  public pdfGridFields = [];
  public pdfGrid = [];
  errorMesj: string = null;
  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  flagEdit: boolean;
  public filter: CompositeFilterDescriptor;
  public calculos: any = [];
  public aplicaMonto: any = [];
  public ttMonedas: any = [];
  public aplTarifa: any;
  public ttCodigos: any;
  public tVenta: any;
  public dsTcuartos: any = [];
  public dias: any = [];
  public aplicaTarifa: boolean = true;
  public formatKendo = localStorage.getItem('formatKendo');
  public formatMoment = localStorage.getItem('formatMoment');
  toast = GLOBAL.toast;
  ttInfocia: any;
  pageSize = 10;
  skip = 0;
  loading = false;

  constructor(private _crudservice: CrudService,
    private _pdfService: PdfService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _info: InfociaGlobalService,
    private _modalService: NgbModal) {
    this.ttInfocia = _info.getSessionInfocia();
    this.dataSource();
    this.getDataSource();
    this.formGroup();
  }


  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  public aplTarValue(event) {
    this.aplicaTarifa = (event === 'A') ? true : false;
  }

  public reqCod(event) {
    if (event) {
      this.myFormGroup.controls['Codigo'].setValue('');
      this.myFormGroup.controls['Codigo'].disable();
    } else {
      this.myFormGroup.controls['Codigo'].enable();
    }
  }

  public formGroup() {
    this.myFormGroup = new FormGroup({
      Suplemento: new FormControl('', Validators.required, Validators.maxLength[8]),
      sdesc: new FormControl('', Validators.required, Validators.maxLength[40]),
      scalculo: new FormControl('', Validators.required),
      sam: new FormControl('D', Validators.required),
      smonto: new FormControl(0),
      sporc: new FormControl(false),
      moneda: new FormControl(this.ttInfocia.Moneda, Validators.required),
      sfecini: new FormControl('', Validators.required),
      sfecter: new FormControl(''),
      sat: new FormControl('A', Validators.required),
      Codigo: new FormControl(''),
      sventa: new FormControl('T', Validators.required),
      sope: new FormControl(''),
      Semana: new FormControl(''),
      Dias: new FormGroup({
        Domingo: new FormControl(true),
        Lunes: new FormControl(true),
        Martes: new FormControl(true),
        Miercoles: new FormControl(true),
        Jueves: new FormControl(true),
        Viernes: new FormControl(true),
        Sabado: new FormControl(true)
      }),
      tcuartos: new FormControl(''),
      fic: new FormControl(''),
      ftc: new FormControl(''),
      Monto1: new FormControl(0, Validators.max[999999]),
      Monto2: new FormControl(0, Validators.max[999999]),
      Monto3: new FormControl(0, Validators.max[999999]),
      Monto4: new FormControl(0, Validators.max[999999]),
      Monto5: new FormControl(0),
      Monto6: new FormControl(0),
      Monto7: new FormControl(0),
      Monto8: new FormControl(0),
      Monto9: new FormControl(0),
      Monto10: new FormControl(0),
      pe1: new FormControl(0),
      pe2: new FormControl(0),
      pe3: new FormControl(0),
      pe4: new FormControl(0),
      pe5: new FormControl(0),
      pe6: new FormControl(0),
      pe7: new FormControl(0),
      pe8: new FormControl(0),
      pe9: new FormControl(0),
      pe10: new FormControl(0),
      Inactivo: new FormControl(false),
      cRowID: new FormControl(),
      lError: new FormControl(false),
      cErrDes: new FormControl(''),
    });
  }

  public getDataSource() {
    this.loading = true;
    this._crudservice.getData(this.api[0])
      .map(data => data = data[this.api[1]][this.api[2]][this.api[3]]).subscribe(data => {
        this.dataGrid = data;
      });
      this.loading = false;
  }

  public newRow(modal) {
    this.formGroup();
    this.aplTarValue('A');
    this.flagEdit = false;
    this.openModal(modal);
  }

  public guardarForm() {
    (this.flagEdit) ? this.aForm('edit') : this.aForm('new');
    this._modalService.dismissAll();
  }

  public itemsCheck(arrayItems, arrayToCompare, arrayToAssign) {
    const arraySplit = arrayItems.split('');
    for (let i = 0; i < arraySplit.length; i++) {
      arrayToCompare[i].value = (arraySplit[i] === 'S') ? true : false;
      const k = arrayToCompare[i].id
      const v = (arraySplit[i] === 'S') ? true : false;
      Object.assign(arrayToAssign, { [k]: v });
    }
  }

  public editRow({ dataItem }, modal) {
    const form = JSON.parse(JSON.stringify(dataItem));
    // this.calc(form.scalculo);
    this.aplicaTarifa = (dataItem.sat === 'A') ? true : false;
    form.sfecini = (form.sfecini) ? moment(dataItem.sfecini).toDate() : null;
    form.sfecter = dataItem.sfecter ? moment(dataItem.sfecter).toDate() : null;
    form.fic = dataItem.fic ? moment(dataItem.fic).toDate() : null;
    form.ftc = dataItem.ftc ? moment(dataItem.ftc).toDate() : null;
    this.itemsCheck(dataItem.Semana, this.dias, {});
    form.tcuartos = (form.tcuartos) ? form.tcuartos.split(',') : '';
    this.myFormGroup.patchValue(form);
    this.flagEdit = true;
    this.openModal(modal);
  }
  openModal(modal) {
    this._modalService.open(modal, { size: 'lg', centered: true });
  }
  public valueCheck(dataObject) {
    const valChk = [];
    // tslint:disable-next-line:forin
    for (const val in dataObject) {
      (dataObject[val]) ? valChk.push('S') : valChk.push('N');
    }
    return valChk.join('');
  }

  private aForm(event, dataDelete?) {
    this.loading = true;
    let service: any;
    const form = JSON.parse(JSON.stringify(this.myFormGroup.value));
    form.Semana = this.valueCheck(form.Dias);
    delete form.Dias;
    form.sfecini = (form.sfecini) ? moment(form.sfecini).format(GLOBAL.formatIso) : null;
    form.sfecter = (form.sfecter) ? moment(form.sfecter).format(GLOBAL.formatIso) : null;
    form.fic = (form.fic) ? moment(form.fic).format(GLOBAL.formatIso) : null;
    form.ftc = (form.ftc) ? moment(form.ftc).format(GLOBAL.formatIso) : null;
    form.tcuartos = (form.tcuartos) ? form.tcuartos.join(',') : '';

    switch (event) {
      case 'new':
        service = this._crudservice.postInfo(form, this.api[0], this.api[2], this.api[3])
        break;
      case 'edit':
        service = this._crudservice.putInfo(form, this.api[0], this.api[2], this.api[3])
        break;
      case 'delete':
        service = this._crudservice.deleteData(dataDelete, this.api[0], this.api[3]);
        break;
    }
    service.subscribe(data => {
      this.errorMesj = (data[this.api[1]].dsMensajes.ttMensError == null) ?
        null : data[this.api[1]].dsMensajes.ttMensError[0].cMensTxt;
      if (this.errorMesj) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: this.errorMesj
        });
        this.loading = false;
      } else {
        if (event === 'delete'){
          this.toast({
            type: 'success',
            title: GLOBAL.textDeleteSuccess
          });
        }else {
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          });
        }
        this.getDataSource();
      }
    });
  }

  public deleteRow({ dataItem }) {
    this.aForm('delete', dataItem);
  }

  dataSource() {
    this.gridFields = [
      { title: 'Suplemento', dataKey: 'Suplemento', width: '175', filtro: true },
      { title: 'Descripción', dataKey: 'sdesc', width: '240', filtro: true },
      // { title: 'Cálculo', dataKey: 'scalculo', width: '100', filtro: false },
      // { title: 'A.M.', dataKey: 'sam', width: '100', filtro: false },
      // { title: 'Porc.', dataKey: 'sporc', width: '100', filtro: false },
      // { title: 'Mon.', dataKey: 'moneda', width: '100', filtro: false },
      // { title: 'A.T.', dataKey: 'sat', width: '100', filtro: false },
      { title: 'Código', dataKey: 'Codigo', width: '110', filtro: false },
    ];
    this.pdfGridFields = [
      { title: 'Inac', dataKey: 'Inactivo', width: '150', filtro: false },
    ];
    this.calculos = [
      { title: 'Fijo', key: 'F' },
      { title: 'Personas', key: 'P' },
      { title: 'Adultos', key: 'A' },
      { title: 'Juniors', key: 'J' },
      { title: 'Menores', key: 'M' }
    ];
    this.aplicaMonto = [
      { title: 'Diario', key: 'D' },
      { title: 'Entrada', key: 'E' }
    ];
    this._crudservice.getData('/manttos/monedas')
      .map(data => data = data.siMoneda.dsMonedas.ttMonedas).subscribe(data => {
        this.ttMonedas = data;
      });
    this.aplTarifa = [
      { title: 'Aplica en Tarifa', key: 'A' },
      { title: 'Incluido en Tarifa', key: 'I' }
    ];
    this._crudservice.getData('/ayuda/codigos®d®®y®')
      .map(data => data = data.siAyuda.dsAyuda.ttCodigos).subscribe(data => {
        this.ttCodigos = data;
      });
    this.tVenta = [
      { title: 'Tarifa', key: 'T' },
      { title: 'Opcional', key: 'O' }
    ];
    this.dias = [
      { id: 'Domingo', dia: 'Domingo', value: true },
      { id: 'Lunes', dia: 'Lunes', value: true },
      { id: 'Martes', dia: 'Martes', value: true },
      { id: 'Miercoles', dia: 'Miércoles', value: true },
      { id: 'Jueves', dia: 'Jueves', value: true },
      { id: 'Viernes', dia: 'Viernes', value: true },
      { id: 'Sabado', dia: 'Sábado', value: true }
    ];
    this._crudservice.getData('/manttos/tcuartos')
      .map(data => data = data.siTcuarto.dsTcuartos.ttTcuartos)
      .subscribe(data => {
        this.dsTcuartos = data;
        this.dsTcuartos = filterBy(this.dsTcuartos, {
          logic: 'and',
          filters: [{ field: 'inactivo', operator: 'eq', value: false }]
        })
      });
  }
  public printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    });
  }
}
