import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CrudService } from 'app/pms/mantto-srv.services';
import { InfociaGlobalService } from 'app/services/infocia.service';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { GLOBAL } from 'app/services/global';
import moment from 'moment';
import { TblGeneral } from 'app/models/tgeneral';

@Component({
  selector: 'app-mantto-cambios-tarifas',
  templateUrl: './mantto-cambios-tarifas.component.html',
  styleUrls: ['./mantto-cambios-tarifas.component.css']
})
export class ManttoCambiosTarifasComponent implements OnInit {
  heading = 'Cambios de Tarifas';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  api: Array<string> = ['/manttos/tarifas/Cambios', 'dsCambiosTarifas', 'ttTgeneral'];
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  dataGridDetalle: any = [];
  filter: CompositeFilterDescriptor;
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  loading: boolean;
  toast = GLOBAL.toast;
  tblGeneral: TblGeneral;

  constructor(private _crud: CrudService,
    private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _modalService: NgbModal) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.ngForm = this.createForm();
    this.ngForm.patchValue({ iInt1: 1 });
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  crudTable(){
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.cChr1 = 'T';
    this.tblGeneral.iInt1 = 1;
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).subtract(2, 'days').toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).subtract(1, 'days').toDate();
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }

  openModal(modal) {
    this._modalService.open(modal, { size: 'lg' });
  }

  getDataModal(data) {
    this.ngForm.patchValue({ cChr2: data.Tarifa, cChr3: data.ttipo });
  }

  buscarInfo() {
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    if (temp.lLog1 === true) {
      temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
      temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);
      temp.iInt1 = Number(temp.iInt1);
      this._crud.putInfo(temp, this.api[0], this.api[1], this.api[2]).subscribe(res => {
        const result = res.siCambiosTarifa.dsCambiosTarifas.bttResp;
        if (!result) {
          this.toast({ type: 'info', title: 'No se encontraron registros.' });
        } else {
          const download = document.createElement('a');
          download.href = 'data:application/vnd.ms-excel;base64,' + result[0].lcRes;
          download.download = result[0].cArc;
          download.click();
        }
        this.loading = false;

      });

    } else if (temp.lLog1 === false) {
      temp.fFec1 = null;
      temp.fFec2 = null;
      if (temp.cChr2 !== '') {
        this._crud.putInfo(temp, this.api[0], this.api[1], this.api[2]).subscribe(res => {
          const result = res.siCambiosTarifa.dsCambiosTarifas;
          if (!result.Tarifas) {
            this.dataGrid = [];
            this.dataGridDetalle = [];
            this.toast({ type: 'info', title: 'No se encontraron registros.' });
          } else {
            this.dataGrid = result.Tarifas;
            this.dataGridDetalle = result.DetalleTarifa;
          }
          this.loading = false;

        });
      } else {
        this.toast({ type: 'info', title: 'Debe especificar una Tarifa.' });
      }
    }

  }
}
