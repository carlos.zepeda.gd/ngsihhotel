import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { SortDescriptor, filterBy } from '@progress/kendo-data-query';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CrudService } from '../../mantto-srv.services';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { GLOBAL } from 'app/services/global';
import swal from 'sweetalert2';
import moment from 'moment';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-mantto-semaforos-fecha',
  templateUrl: './mantto-semaforos-fecha.component.html',
  styleUrls: ['./../../manttos-desing.css'],
  styles: [`
    .btn-day {
      font-weight: bold;
      background: #6969697a;
      width: 13vh;
      font-size: 10px;
    }
  `]
})
export class ManttoSemaforosFechaComponent implements OnInit {
  heading = 'Semáforos Por Fecha';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  private urlSec: string = '/manttos/semfecha/';
  private siBase: string = 'siSemfecha'; // response.siSemfecha.dsSemfecha.ttTdet
  private ds: string = 'dsSemfecha';
  private tt: string = 'ttTdet';
  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public sort: SortDescriptor[] = [{ field: 'tper', dir: 'asc' }];
  public formItems: any = [];
  public dsIngagrp: any = [];
  public dayColor: any;
  public semaforos: any = [];
  public formBuscar: FormGroup;
  public edit: boolean = false;
  public fechaSem: any = '';
  formatMoment = localStorage.getItem('formatMoment');
  formatKendo = localStorage.getItem('formatKendo');
  ttInfocia: any;
  arrayIndex = Array.from({ length: 31 }, (v, i) => i + 1);
  toast = GLOBAL.toast;
  loading = false;

  constructor(private _infocia: InfociaGlobalService,
    private _crudservice: CrudService,
    private _formBuilder: FormBuilder,
    private _menuServ: ManttoMenuService,
    private _modalService: NgbModal,
    private _router: Router) {
    this.ttInfocia = this._infocia.getSessionInfocia();
    this.getSemaforos();
    this.formSearch();
    this.formBuscar.patchValue({
      tperDesde: moment(this.ttInfocia.fday).startOf('month').toDate(),
      tperHasta: moment(this.ttInfocia.fday).endOf('year').toDate()
    });
    this._crudservice.getData('/manttos/ingagrp').map(data =>
      data = data.siIngagrp.dsIngagrp.ttIngagrp).subscribe(data => this.dsIngagrp = data);
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  public search() {
    this.loading = true;
    const form = JSON.parse(JSON.stringify(this.formBuscar.value));
    form.tperDesde = moment(form.tperDesde).format(GLOBAL.formatIso);
    form.tperHasta = moment(form.tperHasta).format(GLOBAL.formatIso);
    this.getDataSource(form.tperDesde, form.tperHasta, form.iagrup);
    this.loading = false;
  }

  public getSemaforos() {
    this.semaforos = [];
    this._crudservice.getData('/manttos/semaforos')
      .map(data => data = data.siSemaforo.dsSemaforos.ttSemaforos)
      .subscribe(data => this.semaforos = data);
  }

  public formSearch() {
    this.formBuscar = new FormGroup({
      tperDesde: new FormControl('', Validators.required),
      tperHasta: new FormControl('', Validators.required),
      iagrup: new FormControl(''),
      semafr: new FormControl('')
    });
  }
  returnColor(value) {
    let color = '';
    if (this.semaforos){
      this.semaforos.forEach(item => {
      if (item.Semaforo === value) {
        color = item.cColor;
        }
      });
    }
    return color;
  }

  public splitAnio(fecha) {
    const year = fecha.slice(0, -2);
    return year;
  }

  public splitFecha(fecha) {
    const year = fecha.slice(0, -2);
    const month = fecha.slice(4);
    return year + '-' + month;
  }

  public getDataSource(fecha1, fecha2, regAgrup: string = '') {
    this.loading = true;
    const format = GLOBAL.formatIso;
    const url = this.urlSec + moment(fecha1).format(format) + '®' + moment(fecha2).format(format) + '®®' + regAgrup + '®B';
    this._crudservice.getData(url)
      .map(data => data = data[this.siBase][this.ds][this.tt])
      .subscribe(data => {this.dataGrid = data});
      this.loading = false;
  }

  public open(fecha, dia, modal) {
    let diaStr = dia.toString();
    if (diaStr.length <= 1) {
      diaStr = '0' + diaStr;
    }
    this.fechaSem = this.splitFecha(fecha) + '-' + diaStr;
    if (this.fechaSem >= this.ttInfocia.fday) {
      this._modalService.open(modal, { size: 'lg', centered: true });
    } else {
      swal('Fecha Pasada!!', 'Selecciona una fecha mayor a la del sistema...', 'info');
    }
  }

  public createFormGroup(dataItem: any): FormGroup {
    return this._formBuilder.group({
      'Semaforo': dataItem.Semaforo,
      'iagrp': dataItem.iagrp,
      'Fecha': dataItem.Fecha,
      'cSemaforo': dataItem.cColor,
      'cIagrp': dataItem.iagrp,
      'fFinal': dataItem.dFechaate,
      'cRowID': '',
      'lError': false,
      'cErrDesc': '',
    });
  }

  public assign({ dataItem }) {
    this.formItems = [];
    Object.assign(dataItem, { Fecha: this.fechaSem });
    Object.assign(dataItem, { iagrp: this.formBuscar.value.iagrup });
    this.formItems = this.createFormGroup(dataItem);
    this.aForm();
    this._modalService.dismissAll();
  }

  public remove({dataItem}) {
    this.formItems = [];
    Object.assign(dataItem, { iagrp: '' });
    Object.assign(dataItem, { Semaforo: '' });
    Object.assign(dataItem, { cSemaforo: null });
    this.formItems = this.createFormGroup(dataItem);
    this.aForm();
    this._modalService.dismissAll();
  }

  public selectRow({ selectedRows }) {
    // this.flagEdit = false;
    // this.selectedItem = selectedRows[0].dataItem;
    // this.ngForm.patchValue(
    //   this.selectedItem
    // );
  }

  private aForm() {
    this.loading = true;
    this._crudservice.putInfo(this.formItems.value, this.urlSec, this.ds, 'ttSemfecha').subscribe(data => {
      const errorMesj = (data[this.siBase].dsMensajes.ttMensError == null) ?
        null : data[this.siBase].dsMensajes.ttMensError[0].cMensTxt;
      if (errorMesj) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: errorMesj
        });
        this.loading = false;
      } else {
        this.search();
        this.toast({
          type: 'success',
          title: GLOBAL.textSuccess
        });
      }
    });
  }

  public exportExcel() {
    const tbl = document.getElementById('tblExcel');
    const wb = XLSX.utils.table_to_book(tbl);
    XLSX.writeFile(wb, 'Reporte Semáforos Por Fecha.xlsx');
  }

}
