import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process } from '@progress/kendo-data-query';
import { CrudService } from '../../mantto-srv.services';
import { PdfService } from '../../mantto-pdf.service';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { GLOBAL } from '../../../../services/global';

const flatten = GLOBAL.flatten;

@Component({
    selector: 'app-mantto-categorias-tarifa',
    templateUrl: './mantto-categorias-tarifa.component.html',
    styleUrls: ['./../../manttos-desing.css']
})
export class ManttoCategoriasTarifaComponent implements OnInit {
    heading = 'Mantenimientos Categorías de Tarifas';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';
    private api: Array<string> = ['/manttos/tarcateg', 'siTarcateg', 'dsTarcateg', 'ttTarcateg'];
    public gridFields = [];
    public pdfGridFields = [];
    public pdfGrid = [];
    errorMesj: string = null;
    public myFormGroup: FormGroup;
    public dataGrid: any = [];
    flagEdit: boolean;
    public dataComplete: any = [];
    public checked: boolean;
    public groups: GroupDescriptor[] = [];
    public filter: CompositeFilterDescriptor;
    public sort: SortDescriptor[] = [{ field: 'tcat', dir: 'asc' }];
    toast = GLOBAL.toast;
    loading = false;

    constructor(private _crudservice: CrudService,
        private _pdfService: PdfService,
        private _modalService: NgbModal,
        private _menuServ: ManttoMenuService,
        private _router: Router) {
        this.gridFields = [
            { title: 'Categoría', dataKey: 'tcat', width: '150', filtro: true },
            { title: 'Descripción', dataKey: 'Descripcion', width: '', filtro: true },
        ];
        this.pdfGridFields = [
            { title: 'Inac', dataKey: 'Inactivo', width: '', filtro: false },
        ];
    }

    ngOnInit() {
        this.formGrup();
        this.getDataSource();

    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
    }

    public formGrup() {
        this.myFormGroup = new FormGroup({
            tcat: new FormControl('', Validators.required, Validators.maxLength[30]),
            Descripcion: new FormControl('', Validators.required, Validators.maxLength[40]),
            Inactivo: new FormControl(false),
            cRowID: new FormControl(''),
            lError: new FormControl(false),
            cErrDesc: new FormControl(''),
        });
    }

    public getDataSource() {
        this.loading = true;
        this._crudservice.getData(this.api[0])
            .map(data => data = data[this.api[1]][this.api[2]][this.api[3]]).subscribe(data => {
                this.dataGrid = data;
                this.dataComplete = data;
                this.dataGrid = {
                    data: orderBy(this.dataGrid, this.sort),
                    total: this.dataGrid.length
                };
            });
          this.loading = false;
    }

    public newRow(modal) {
        this.formGrup();
        this.flagEdit = false;
        this.openModal(modal);
    }
    openModal(modal) {
        this._modalService.open(modal, { size: 'sm', centered: true });
    }
    public guardarForm() {
        (this.flagEdit) ? this.aForm('edit') : this.aForm('new');
        this._modalService.dismissAll();
    }

    public itemsCheck(arrayItems, arrayToCompare, arrayToAssign) {
        const arraySplit = arrayItems.split('');
        for (let i = 0; i < arraySplit.length; i++) {
            arrayToCompare[i].value = (arraySplit[i] === 'S') ? true : false;
            const k = arrayToCompare[i].id
            const v = (arraySplit[i] === 'S') ? true : false;
            Object.assign(arrayToAssign, { [k]: v });
        }
    }

    public editRow({ dataItem }, modal) {
        this.myFormGroup.patchValue(dataItem);
        this.flagEdit = true;
        this.openModal(modal);
    }

    private aForm(event, dataDelete: any = {}) {
        let service: any;
        const form = this.myFormGroup.value;
        this.loading = true;
        switch (event) {
            case 'new':
                service = this._crudservice.postInfo(form, this.api[0], this.api[2], this.api[3])
                break
            case 'edit':
                service = this._crudservice.putInfo(form, this.api[0], this.api[2], this.api[3])
                break
            case 'delete':
                service = this._crudservice.deleteData(dataDelete, this.api[0], this.api[3]);
                break
            default:
                break
        }
        service.subscribe(data => {
          this.errorMesj = (data[this.api[1]].dsMensajes.ttMensError == null) ?
                null : data[this.api[1]].dsMensajes.ttMensError[0].cMensTxt;
          if (this.errorMesj) {
            this.toast({
              type: 'error',
              title: GLOBAL.textError,
              text: this.errorMesj
            });
            this.loading = false;
          } else {
            if (event !== 'delete'){
              this.toast({ title: GLOBAL.textSuccess, type: 'success' });
            }else {
              this.toast({ title: GLOBAL.textDeleteSuccess, type: 'success' });
            }
            this.flagEdit = true;
            this.getDataSource();
          }
        });
    }

    public deleteRow({ dataItem }) {
        this.aForm('delete', dataItem);
    }

    public printPDF() {
      this.toast({
        type: 'info',
        title: 'NO DISPONIBLE!',
        text: 'En mantenimiento'
      });
    }

    // Funciones para la Grid de Kendo
    // Procesa la grid a mostrar cuando se agrupa por un campo. Grid debe tener Groupable = true
    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.dataGrid = process(this.dataComplete, { group: this.groups });
    }

    // Procesa la grid cuando se filtra en un campo. Grid debe tener Filterable = true
    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.dataGrid.data = filterBy(this.dataComplete, filter);
    }

    // Procesa la grid cuando se marca checked en un campo.
    public switchChange(checked: boolean): void {
        const root = this.filter || { logic: 'and', filters: [] };
        const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
        if (!filter) {
            root.filters.push({
                field: 'Inactivo',
                operator: 'eq',
                value: checked
            });
        } else {
            filter.value = checked;
        }
        this.checked = checked;
        this.filterChange(root);
    }

    // Procesa la grid en asc o desc en el campo seleccionado
    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.dataGrid = {
            data: orderBy(this.dataGrid.data, this.sort),
            total: this.dataGrid.data.length
        };
    }

}
