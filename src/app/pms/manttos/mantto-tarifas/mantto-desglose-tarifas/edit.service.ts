import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { zip } from 'rxjs/observable/zip';

const CREATE_ACTION = 'create';
const UPDATE_ACTION = 'update';
const REMOVE_ACTION = 'destroy';

import { CrudService } from '../../mantto-srv.services';

const itemIndex = (item: any, data: any[]): number => {
    for (let idx = 0; idx < data.length; idx++) {
        if (data[idx].dcodigo === item.dcodigo) {
            return idx;
        }
    }
    return -1;
};

const cloneData = (data: any[]) => data.map(item => Object.assign({}, item));

@Injectable()
export class EditService extends BehaviorSubject<any[]> {
    private data: any[] = [];
    private originalData: any[] = [];
    private createdItems: any[] = [];
    private updatedItems: any[] = [];
    private deletedItems: any[] = [];

    private urlSec: string = '/manttos/detcod/';
    private siBase: string = 'siDetcod';
    private ds: string = 'dsDetcod';
    private tt: string = 'ttDetcod';
    errorMesj: string = null;
    private itemsDesglose: any = [];
    public result: any;

    constructor(private http: Http, private _crudservice: CrudService) {
        super([]);
    }

    public read() {
        if (this.data.length) {
            return super.next(this.data);
        }
    }

    public create(item: any): void {
        this.createdItems.push(item);
        this.data.unshift(item);
        super.next(this.data);
    }

    public update(item: any): void {
        if (!this.isNew(item)) {
            const index = itemIndex(item, this.updatedItems);
            if (index !== -1) {
                this.updatedItems.splice(index, 1, item);
            } else {
                this.updatedItems.push(item);
            }
        } else {
            const index = this.createdItems.indexOf(item);
            this.createdItems.splice(index, 1, item);
        }
    }

    public remove(item: any): void {
        let index = itemIndex(item, this.data);
        this.data.splice(index, 1);
        index = itemIndex(item, this.createdItems);
        if (index >= 0) {
            this.createdItems.splice(index, 1);
        } else {
            this.deletedItems.push(item);
        }
        index = itemIndex(item, this.updatedItems);
        if (index >= 0) {
            this.updatedItems.splice(index, 1);
        }
        super.next(this.data);
    }

    public isNew(item: any): boolean {
        return !item.dcodigo;
    }

    public hasChanges(): boolean {
        return Boolean(this.deletedItems.length || this.updatedItems.length || this.createdItems.length);
    }

    public saveChanges(): void {
        if (!this.hasChanges()) {
            return;
        }
        const completed = [];
        if (this.deletedItems.length) {
            this.deletedItems.forEach(element => {
                this.aForm(REMOVE_ACTION, element);
            });
        }
        if (this.updatedItems.length) {
            this.updatedItems.forEach(element => {
                this.aForm(UPDATE_ACTION, element);
            });
        }
        if (this.createdItems.length) {
            this.createdItems.forEach(element => {
                this.itemsBase();
                this.assignValues(this.itemsDesglose, element);
                this.aForm(CREATE_ACTION, this.itemsDesglose);
            });
        }
        this.reset();
        zip(...completed).subscribe(() => this.read());
    }

    public cancelChanges(): void {
        this.reset();
        this.data = this.originalData;
        this.originalData = cloneData(this.originalData);
        super.next(this.data);
    }

    public assignValues(target: any, source: any): void {
        Object.assign(target, source);
    }

    private reset() {
        this.data = [];
        this.deletedItems = [];
        this.updatedItems = [];
        this.createdItems = [];
        this.itemsDesglose = [];
    }

    private aForm(action: string = '', form?: any) {
        this.result = '';
        let service: any;
        switch (action) {
            case 'create':
                service = this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt)
                break;
            case 'update':
                service = this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt)
                break;
            case 'destroy':
                service = this._crudservice.deleteData(form, this.urlSec, this.tt);
                break;
        }
        service.subscribe(data => {
            this.errorMesj = (data[this.siBase].dsMensajes.ttMensError == null) ?
                null : data[this.siBase].dsMensajes.ttMensError[0].cMensTxt;
            return this.result = (this.errorMesj) ? this.errorMesj : 'Acción ejecutada correctamente! :)';
        });
    }

    private itemsBase() {
        this.itemsDesglose = {
            detalle: '',
            dcodigo: '',
            cCodigo: '',
            dta1: 0,
            dta2: 0,
            dta3: 0,
            dta4: 0,
            dta5: 0,
            dta6: 0,
            dta7: 0,
            dta8: 0,
            dta9: 0,
            dta10: 0,
            dta11: 0,
            dpa1: false,
            dpa2: false,
            dpa3: false,
            dpa4: false,
            dpa5: false,
            dpa6: false,
            dpa7: false,
            dpa8: false,
            dpa9: false,
            dpa10: false,
            dpa11: false,
            dtj1: 0,
            dtj2: 0,
            dtj3: 0,
            dtj4: 0,
            dtj5: 0,
            dtj6: 0,
            dtj7: 0,
            dtj8: 0,
            dtj9: 0,
            dtj10: 0,
            dtj11: 0,
            dpj1: false,
            dpj2: false,
            dpj3: false,
            dpj4: false,
            dpj5: false,
            dpj6: false,
            dpj7: false,
            dpj8: false,
            dpj9: false,
            dpj10: false,
            dpj11: false,
            dtm1: 0,
            dtm2: 0,
            dtm3: 0,
            dtm4: 0,
            dtm5: 0,
            dtm6: 0,
            dtm7: 0,
            dtm8: 0,
            dtm9: 0,
            dtm10: 0,
            dtm11: 0,
            dpm1: false,
            dpm2: false,
            dpm3: false,
            dpm4: false,
            dpm5: false,
            dpm6: false,
            dpm7: false,
            dpm8: false,
            dpm9: false,
            dpm10: false,
            dpm11: false,
            dmonto1: 0,
            dmonto2: 0,
            dmonto3: 0,
            dmonto4: 0,
            dmonto5: 0,
            dmonto6: 0,
            dporc1: false,
            dporc2: false,
            dporc3: false,
            dporc4: false,
            dporc5: false,
            dporc6: false,
            cRowID: '',
            lError: false,
            cErrDesc: '',
        }
        return this.itemsDesglose;
    }
}
