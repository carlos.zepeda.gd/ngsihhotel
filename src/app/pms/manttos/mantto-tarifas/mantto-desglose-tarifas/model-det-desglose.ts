export class DetDesglose {
    // public detalle = '';
    // public dcodigo = '';
    // public dta1: number;
    // public dpa1 = false;
    // public dta2: number;
    // public dpa2 = false;
    // public dta3: number;
    // public dpa3 = false;
    // public dta11: number;
    // public dpa11 = false;
    // public dtj11: number;
    // public dpj11 = false;
    // public dtm11: number;
    // public dpm11 = false;
    public detalle = '';
    public dcodigo = '';
    public cCodigo = '';
    public dta1: number = 0;
    public dta2: number = 0;
    public dta3: number = 0;
    public dta4: number = 0;
    public dta5: number = 0;
    public dta6: number = 0;
    public dta7: number = 0;
    public dta8: number = 0;
    public dta9: number = 0;
    public dta10: number = 0;
    public dta11: number = 0;
    public dpa1 = false;
    public dpa2 = false;
    public dpa3 = false;
    public dpa4 = false;
    public dpa5 = false;
    public dpa6 = false;
    public dpa7 = false;
    public dpa8 = false;
    public dpa9 = false;
    public dpa10 = false;
    public dpa11 = false;
    public dtj1: number = 0;
    public dtj2: number = 0;
    public dtj3: number = 0;
    public dtj4: number = 0;
    public dtj5: number = 0;
    public dtj6: number = 0;
    public dtj7: number = 0;
    public dtj8: number = 0;
    public dtj9: number = 0;
    public dtj10: number = 0;
    public dtj11: number = 0;
    public dpj1 = false;
    public dpj2 = false;
    public dpj3 = false;
    public dpj4 = false;
    public dpj5 = false;
    public dpj6 = false;
    public dpj7 = false;
    public dpj8 = false;
    public dpj9 = false;
    public dpj10 = false;
    public dpj11 = false;
    public dtm1: number = 0;
    public dtm2: number = 0;
    public dtm3: number = 0;
    public dtm4: number = 0;
    public dtm5: number = 0;
    public dtm6: number = 0;
    public dtm7: number = 0;
    public dtm8: number = 0;
    public dtm9: number = 0;
    public dtm10: number = 0;
    public dtm11: number = 0;
    public dpm1 = false;
    public dpm2 = false;
    public dpm3 = false;
    public dpm4 = false;
    public dpm5 = false;
    public dpm6 = false;
    public dpm7 = false;
    public dpm8 = false;
    public dpm9 = false;
    public dpm10 = false;
    public dpm11 = false;
    public dmonto1: number = 0;
    public dmonto2: number = 0;
    public dmonto3: number = 0;
    public dmonto4: number = 0;
    public dmonto5: number = 0;
    public dmonto6: number = 0;
    public dporc1 = false;
    public dporc2 = false;
    public dporc3 = false;
    public dporc4 = false;
    public dporc5 = false;
    public dporc6 = false;
    public cRowID = '';
    public lError = false;
    public cErrDesc = '';
}
