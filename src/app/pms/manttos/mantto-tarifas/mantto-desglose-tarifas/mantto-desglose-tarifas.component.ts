import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CrudService } from '../../mantto-srv.services';
import { PdfService } from '../../mantto-pdf.service';
import { EditService } from './edit.service';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import swal from 'sweetalert2';
import { InfociaGlobalService } from 'app/services/infocia.service';
import { GLOBAL } from 'app/services/global';

@Component({
  selector: 'app-mantto-desglose-tarifas',
  templateUrl: './mantto-desglose-tarifas.component.html',
  styleUrls: ['./../../manttos-desing.css']
})
export class ManttoDesgloseTarifasComponent implements OnInit {
  heading = 'Mantenimiento Desglose de Tarifas';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  private api: Array<string> = ['/manttos/detalles', 'siDetalle', 'dsDetalles', 'ttDetalles'];
  private api2: Array<string> = ['/manttos/detcod/', 'siDetcod', 'dsDetcod', 'ttDetcod'];
  public gridFields = [];
  public pdfGridFields = [];
  public pdfGrid = [];
  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public flagEdit: boolean;
  public filter: CompositeFilterDescriptor;
  public idDesglose: any;
  public tblDetalleDesg: any = [];
  public gridDesglose: any = [];
  public gridDesglosePdf: any = [];
  errorMesj: string = null;
  tblCodDesglose: any;
  loading: boolean;
  pageSize = 10;
  skip = 0;
  ngFormDetalle: FormGroup;
  flagEditDetalle: boolean;
  arrTarifas = [
    'Sencillo',
    'Doble',
    'Triple',
    'Cuádruple',
    'Quíntuple',
    'Séxtuple',
    'Séptuple',
    'Óctuple',
    'Nónuplo',
    'Décuplo',
  ];
  arrTariCols = [];
  ttInfocia: any;
  toast = GLOBAL.toast;

  constructor(private _crudservice: CrudService,
    private _info: InfociaGlobalService,
    private _pdfService: PdfService,
    private _formBuilder: FormBuilder,
    public _editService: EditService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _modalService: NgbModal) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.gridFields = [
      { title: 'Desglose', dataKey: 'Detalle', width: '170', filtro: true },
      { title: 'Descripción', dataKey: 'ddesc', width: '350', filtro: true },
    ];
    this.pdfGridFields = [
      { title: 'Inac', dataKey: 'Inactivo', width: '170', filtro: false },
    ];
    this.gridDesglosePdf = [
      { title: 'Desglose', dataKey: 'detalle' },
      { title: 'Código', dataKey: 'dcodigo' }
    ]
    this._crudservice.getData('/ayuda/codigos®D®3®')
      .map(data => data = data.siAyuda.dsAyuda.ttCodigos)
      .subscribe(data => this.tblCodDesglose = data);
    this.formGrup();
    this.createFormGroup();
    this.getDataSource();
    for (let cont = 0; cont < this.ttInfocia.cact; cont++) {
      this.arrTariCols.push(this.arrTarifas[cont]);
    }
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  editarDetalle(dataItem) {
    this.ngFormDetalle.patchValue(dataItem);
    this.flagEditDetalle = true;
  }

  guardarDetalle(): void {
    const dataItem = this.ngFormDetalle.value;
    dataItem.detalle = this.idDesglose;
    (this.flagEditDetalle) ? this.crudServiceDetalle(dataItem, 'edit') : this.crudServiceDetalle(dataItem, 'new');
  }

  validarDesglose(form) {
    if (!form.duso) {
      swal({
        type: 'info', title: 'Desglose no cuadra!!!', text: 'No estara en uso.'
      });
    }
  }

  nuevoDetalle() {
    this.createFormGroup();
    this.flagEditDetalle = false;
  }

  createFormGroup() {
    this.ngFormDetalle = this._formBuilder.group({
      detalle: [''],
      dcodigo: [''],
      cRowID: [''],
      lError: [false],
      cErrDesc: [''],
      dta1: [0, [Validators.max(999)]], dta2: [0, [Validators.max(999)]], dta3: [0], dta4: [0], dta5: [0],
      dta6: [0], dta7: [0], dta8: [0], dta9: [0], dta10: [0], dta11: [0, [Validators.max(999)]],
      // tslint:disable-next-line: max-line-length
      dpa1: [false], dpa2: [false], dpa3: [false], dpa4: [false], dpa5: [false], dpa6: [false], dpa7: [false], dpa8: [false], dpa9: [false], dpa10: [false], dpa11: [false],
      dtj1: [0], dtj2: [0], dtj3: [0], dtj4: [0], dtj5: [0], dtj6: [0], dtj7: [0], dtj8: [0], dtj9: [0], dtj10: [0],
      dtj11: [0, [Validators.max(999)]],
      // tslint:disable-next-line: max-line-length
      dpj1: [false], dpj2: [false], dpj3: [false], dpj4: [false], dpj5: [false], dpj6: [false], dpj7: [false], dpj8: [false], dpj9: [false], dpj10: [false], dpj11: [false],
      dtm1: [0], dtm2: [0], dtm3: [0], dtm4: [0], dtm5: [0], dtm6: [0], dtm7: [0], dtm8: [0], dtm9: [0], dtm10: [0],
      dtm11: [0, [Validators.max(999)]],
      // tslint:disable-next-line: max-line-length
      dpm1: [false], dpm2: [false], dpm3: [false], dpm4: [false], dpm5: [false], dpm6: [false], dpm7: [false], dpm8: [false], dpm9: [false], dpm10: [false], dpm11: [false],
      dmonto1: [0], dmonto2: [0], dmonto3: [0], dmonto4: [0], dmonto5: [0], dmonto6: [0],
      dporc1: [false], dporc2: [false], dporc3: [false], dporc4: [false], dporc5: [false], dporc6: [false]
    });
  }

  open(dataItem, modal, size) {
    this.nuevoDetalle();
    this.idDesglose = dataItem.Detalle;
    this.loadDetDesg(dataItem.Detalle);
    this.myFormGroup.patchValue(dataItem);
    this.openModal(modal, size);
    // this.flagEditDetalle = true;
  }

  loadDetDesg(desglose) {
    this.loading = true;
    this._crudservice.getData('/manttos/detcod/' + desglose)
      .map(data => data = data.siDetcod.dsDetcod.ttDetcod).subscribe(data => {
        if (data) {
          this.tblDetalleDesg = data;
          this.ngFormDetalle.patchValue(this.tblDetalleDesg[0]);
          this.flagEditDetalle = true;
        } else {
          this.tblDetalleDesg = [];
          this.flagEditDetalle = false;
          this.nuevoDetalle();
        }
      });
    this.loading = false;
  }

  printPDFdetalle(idDesglose) { // gridDesglosePdf
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    });
  }

  formGrup() {
    this.myFormGroup = new FormGroup({
      Detalle: new FormControl('', Validators.required, Validators.maxLength[6]),
      ddesc: new FormControl('', Validators.required, Validators.maxLength[30]),
      duso: new FormControl(false),
      Inactivo: new FormControl(false),
      cRowID: new FormControl(''),
      lError: new FormControl(false),
      cErrDesc: new FormControl('')
    });
  }

  getDataSource() {
    this.loading = true;
    this._crudservice.getData(this.api[0])
      .map(data => data = data[this.api[1]][this.api[2]][this.api[3]]).subscribe(data => {
        this.dataGrid = data;
        this.dataGrid.forEach(item => {
          if (item.Detalle === this.idDesglose) {
            this.myFormGroup.patchValue(item);
          }
        });
      });
    this.loading = false;
  }

  newRow(modal, size) {
    this.formGrup();
    this.flagEdit = false;
    this.openModal(modal, size);
  }

  guardarForm() {
    (this.flagEdit) ? this.crudService(this.myFormGroup.value, 'edit') : this.crudService(this.myFormGroup.value, 'new');
  }

  changeDesglose({ selectedRows }) {
    if (selectedRows && selectedRows[0]) {
      this.editarDetalle(selectedRows[0].dataItem);
    }
  }

  editRow({ dataItem }, modal) {
    this.myFormGroup.patchValue(dataItem);
    this.flagEdit = true;
    this.openModal(modal, 'sm');
  }

  openModal(modal, size) {
    this._modalService.open(modal, { size: size, centered: true });
  }

  crudService(dataItem, method) {
    let service: any;
    this.loading = true;
    switch (method) {
      case 'new':
        service = this._crudservice.postInfo(dataItem, this.api[0], this.api[2], this.api[3])
        break;
      case 'edit':
        service = this._crudservice.putInfo(dataItem, this.api[0], this.api[2], this.api[3])
        break;
      case 'delete':
        service = this._crudservice.deleteData(dataItem, this.api[0], this.api[3]);
        break;
    }
    service.subscribe(data => {
      this.errorMesj = (data[this.api[1]].dsMensajes.ttMensError == null) ?
        null : data[this.api[1]].dsMensajes.ttMensError[0].cMensTxt;
      if (this.errorMesj) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: this.errorMesj
        });
        this.loading = false;
      } else {
        if (method === 'delete'){
          this.toast({
            type: 'success',
            title: GLOBAL.textDeleteSuccess
          });
        }else {
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          });
        }
        this.getDataSource();
      }
      this._modalService.dismissAll();
    });
  }

  crudServiceDetalle(dataItem, method) {
    let service: any;
    this.loading = true;
    switch (method) {
      case 'new':
        service = this._crudservice.postInfo(dataItem, this.api2[0], this.api2[2], this.api2[3])
        break;
      case 'edit':
        service = this._crudservice.putInfo(dataItem, this.api2[0], this.api2[2], this.api2[3])
        break;
      case 'delete':
        service = this._crudservice.deleteData(dataItem, this.api2[0], this.api2[3]);
        break;
    }
    service.subscribe(data => {
      this.errorMesj = (data[this.api2[1]].dsMensajes.ttMensError == null) ?
        null : data[this.api2[1]].dsMensajes.ttMensError[0].cMensTxt;
      if (this.errorMesj) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: this.errorMesj
        });
        this.loading = false;
      } else {
        if (method === 'delete'){
          this.toast({
            type: 'success',
            title: GLOBAL.textDeleteSuccess
          });
        }else {
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          });
        }
        this.loadDetDesg(this.idDesglose);
        // this.getDataSource();
      }
    });
  }

  deleteRow({ dataItem }) {
    this.crudService(dataItem, 'delete');
  }

  printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    });
  }

}

