import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { SortDescriptor, filterBy, CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';
import { ComboBoxComponent } from '@progress/kendo-angular-dropdowns';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { DetalleTarifaService } from './detalleTarifa.service';
import { GLOBAL } from '../../../../services/global';
import { DeTarifa } from './model-deTarifa';
import { Subscription } from 'rxjs';
import { CrudService } from '../../mantto-srv.services';
import { PdfService } from '../../../mantto-pdf.service';
import moment from 'moment';
const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-tarifas',
  templateUrl: './mantto-tarifas.component.html',
  styleUrls: ['./../../manttos-desing.css',]
})
export class ManttoTarifasComponent implements OnInit {
  heading = 'Mantenimiento de Tarifas';
  subheading: string;
  icon: string;
  noRecords = GLOBAL.noRecords;
  loading = false;
  loadingTarifas = false;
  @ViewChild('modalForm') modalForm: TemplateRef<any>;
  @ViewChild('modalDetalle') modalDetalle: TemplateRef<any>;
  @ViewChild('modalSuplemento') modalSuplemento: TemplateRef<any>;
  @ViewChild('modalAgencias') modalAgencias: TemplateRef<any>;
  @ViewChild('tcuarto') tcuarto: ComboBoxComponent;
  // Variables Generales
  private urlSec: string = '/manttos/tarifas';
  private siBase: string = 'siTarifa';
  private ds: string = 'dsTarifas';
  private tt: string = 'ttTarifas';
  public pdfGridFields = [];
  public pdfGrid = [];
  public seccion: string = 'Tarifas';
  public formStatus: string = null;
  public errorMesj: string = null;
  public myFormGroup: FormGroup;
  // Variables DataGrid
  public dataGrid: any = [];
  public dataTarifaAgencia: any = [];
  public flagEdit: boolean;
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'Tarifa', dir: 'asc' }]; // Order DataGrid
  // Variables Tarifas
  public ttMonedas: any = [];
  public ttTarcateg: any = [];
  public ttSegmentos: any = [];
  public tts: any = [];
  public procList: any = [];
  public dsCalculo: any = [];
  public ttSemaforos: any = [];
  public listTtipo: any = [];
  public formItems: any = [];
  public dDate: Date;
  public promoActiva: boolean = false;
  public PDFTarifas: any;
  public ttTarifas: any = [];
  public ttTardet: any = [];
  // Variables Detalle Tarifa
  public proc: any;
  public tarifa: any;
  public cTarifa: any;
  public ttipo: any;
  public tblDeTarifa: any = [];
  public gridDeTarifa: any = [];
  public gridDeTarifaPdf: any = [];
  public editDeTarifa: boolean;
  public tblCodTarifa: any = [];
  public dsDesglose: any = [];
  public formGroup: FormGroup;
  private editedRowIndex: number;
  public dtItems: any = [];
  subscription: Subscription;
  public selectedValue: any = '';
  ttInfocia: any;
  public fechaActual: any;
  public fechaActualJS: any;
  flagTarifaEmpresa: boolean;
  flagTarifaAgencia: boolean;
  tarifaSelected: any;
  dsAgencias: any = [];
  formAgencia: any = { Agencia: '', prov: '', atarifa: '', atipo: '', cRowID: '', lError: false, cErrDes: '' };
  dsEmpresas: any = [];
  ngFormDetaTari: FormGroup;
  flagEditTarifa = false;
  desgloseDetaTari = true;
  toast = GLOBAL.toast;
  keepdTarifa: any;
  kendoFormat = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  dsTarifasProm = [];
  arrTarifas = GLOBAL.personas;
  arrTariCols = [];
  dsTipoCuartos = [];
  dsDesgloses: any;
  listTtipo2: any;
  fechaMin: Date;
  checked: boolean;
  dataComplete: unknown[];

  constructor(private _crudservice: CrudService,
    private _pdfService: PdfService,
    private _formBuilder: FormBuilder,
    public _editService: DetalleTarifaService,
    private _menuServ: ManttoMenuService,
    private _modalService: NgbModal,
    private _router: Router,
    private _info: InfociaGlobalService,
    private _active: ActivatedRoute) {

    this._active.data.subscribe(data => {
      this.proc = data.proc;
      if (this.proc === 'M') {
        this.heading = this.heading + ' Mayoristas';
      }
    });
    this.formGrup();
    this.getDataSource();
    this.ttInfocia = this._info.getSessionInfocia();
    this.fechaActual = moment(this.ttInfocia.fday).toDate();
    for (let cont = 0; cont < this.ttInfocia.cact; cont++) {
      this.arrTariCols.push(this.arrTarifas[cont]);
    }
    this.dataSource();
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  openModal(modal, size?) {
    if (!size) {
      size = 'lg';
    }
    this._modalService.open(modal, { size: size, centered: true });
  }

  public tdesglose() {
    this._crudservice.getData('/manttos/detalles')
      .map(data => data = data.siDetalle.dsDetalles.ttDetalles).subscribe(data => {
        this.dsDesglose = data;
        if (this.dsDesglose) {
          return this.dsDesglose = filterBy(this.dsDesglose, {
            logic: 'and', filters: [{ field: 'Inactivo', operator: 'eq', value: false }]
          });
        }
      });
  }

  public refreshTblDeTarifas(event) {
    this.loadDeTarifa(event);
  }

  public addHandler({ sender }) {
    this.fechaActualJS = moment(this.fechaActual).toDate();
    this.editDeTarifa = false;
    this.closeEditor(sender);
    this.formGroup = this.createFormGroup(new DeTarifa());
    sender.addRow(this.formGroup);
  }

  public editHandler({ sender, rowIndex, dataItem }) {
    this.editDeTarifa = true;
    this.closeEditor(sender);
    Object.assign(this.dtItems, dataItem);
    const tfecini = moment(dataItem.tfecini);
    Object.assign(this.dtItems, { 'tfecini': tfecini });
    const tfecter = moment(dataItem.tfecter);
    Object.assign(this.dtItems, { 'tfecter': tfecter });
    this.formGroup = this.createFormGroup(this.dtItems);
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
    this.editDeTarifa = false;
  }

  public saveHandler({ sender, rowIndex, formGroup, isNew }): void {
    const product = formGroup.value;
    this._editService.save(product, isNew);
    sender.closeRow(rowIndex);
    this.responseObservable();
  }

  public removeHandler({ dataItem }): void {
    this._editService.remove(dataItem);
    this.responseObservable();
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  public buscaDuplicadoDeTarifa({ sender, rowIndex, formGroup, isNew }): void {
    this.editDeTarifa = true;
    let tblFilter: any;
    tblFilter = filterBy(this.tblDeTarifa, { field: 'tcuarto', operator: 'eq', value: formGroup.value.tcuarto });
    const fini = moment(formGroup.value.tfecini).format(GLOBAL.formatIso);
    tblFilter = filterBy(tblFilter, { field: 'tfecini', operator: 'eq', value: fini });
    const ffin = moment(formGroup.value.tfecter).format(GLOBAL.formatIso);
    tblFilter = filterBy(tblFilter, { field: 'tfecter', operator: 'eq', value: ffin });
    if (isNew && tblFilter.length >= 1) {
      this.toast({
        type: 'error',
        title: GLOBAL.textError,
        html: 'Ya existe un registro con esta información!',
      })
    } else {
      this.saveHandler({ sender, rowIndex, formGroup, isNew });
    }
  }

  public createFormGroup(dataItem: any): FormGroup {
    return this._formBuilder.group({
      'proc': this.proc,
      'tarifa': this.tarifa,
      'cTarifa': this.cTarifa,
      'ttipo': this.ttipo,
      'tcuarto': new FormControl(dataItem.tcuarto, Validators.required),
      'tfecini': new FormControl(dataItem.tfecini, Validators.required),
      'tfecter': new FormControl(dataItem.tfecter, Validators.required),
      'tta1': new FormControl(dataItem.tta1, Validators.compose([Validators.required])),
      'tta2': new FormControl(dataItem.tta2, Validators.compose([Validators.required])),
      'tta3': dataItem.tta3,
      'tta11': dataItem.tta11,
      'ttj11': dataItem.ttj11,
      'ttm11': dataItem.ttm11,
      'detalle': new FormControl(dataItem.detalle, Validators.required),
      'cRowID': dataItem.cRowID
    });
  }

  public responseObservable() {
    this.subscription = this._editService.notifyObservable$.subscribe((response) => {
      if (response.result === 'success') {
        this.tblDeTarifa = response.dataItems;
      } else {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          html: response.message[0].cMensTxt + ' <br> ' + response.dataItems[0].cErrDesc,
        })
      }
    });
  }

  getTarifasAgencia(tarifa, tipo) {
    const url = tipo + GLOBAL.char174 + GLOBAL.char174 + tarifa + GLOBAL.char174
    this._crudservice.getData('/manttos/tarage/' + url).subscribe(data => {
      const result = data.siAgtarifa.dsAgtarifas.ttAgtarifas;
      if (!result) {
        this.dataTarifaAgencia = [];
      } else {
        this.dataTarifaAgencia = result;
        // if (result && result[0]) {
        //   this.formAgencia = result[0];
        // }
      }
    });
  }

  public open(event, win) {
    switch (win) {
      case 'D':
        this.openModal(this.modalDetalle);
        this.loadDeTarifa(event);
        // setTimeout(() => this.tcuarto.focus(), 1000);
        break;
      case 'S':
        this.tarifaSelected = event;
        this.openModal(this.modalSuplemento, 'xl');
        break;
      case 'A':
        this.tarifaSelected = event;
        this.openModal(this.modalAgencias);
        this.flagTarifaAgencia = true;
        this.flagTarifaEmpresa = false;
        this.getTarifasAgencia(event.Tarifa, 'A');
        this._crudservice.getData('/ayuda/agencias®N®' + this.myFormGroup.value.proc).subscribe(data => {
          this.dsAgencias = data.siAyuda.dsAyuda.ttAgencias;
          this.dsAgencias.forEach(item => item.anombre = item.anombre + ' - ' + item.Agencia);
          this.formAgencia.Agencia = '';
        });
        break;
      case 'E':
        this.tarifaSelected = event;
        this.openModal(this.modalAgencias);
        this.flagTarifaEmpresa = true;
        this.flagTarifaAgencia = false;
        this.getTarifasAgencia(event.Tarifa, 'E');
        this._crudservice.getData('/ayuda/empresas').subscribe(data => {
          this.dsEmpresas = data.siAyuda.dsAyuda.ttEmpresas;
          if (this.dsEmpresas) {
            this.dsEmpresas.forEach(item => item.enombre = item.enombre + ' - ' + item.Empresa);
          }
          this.formAgencia.Agencia = '';
        });
        break;

    }

  }

  public loadDeTarifa(dTarifa) {
    this.keepdTarifa = dTarifa;
    this.proc = dTarifa.proc;
    this.tarifa = dTarifa.Tarifa;
    this.cTarifa = dTarifa.tdesc;
    this.ttipo = dTarifa.ttipo;
    this.loadingTarifas = true;
    this._crudservice.getData('/manttos/tardet/' + this.proc + '®' + this.tarifa + '®' + this.ttipo)
      .map(data => data = data.siTardet.dsTardet.ttTardet)
      .subscribe(data => {
        this.tblDeTarifa = data ? data : [];
        this.loadingTarifas = false;
      });
    this.tdesglose();
    this.nuevoDetaTari();
  }

  public printPDFdetalle(idTarifa) {
    const columns = this.pdfGrid.concat(this.gridDeTarifa, this.gridDeTarifaPdf);
    this._pdfService.printPDF('Detalle Desglose Tarifa ' + idTarifa, columns, this.tblDeTarifa, 'l');
  }

  public chckPromo(event) {
    if (event) {
      this.promoActiva = true;
      this.myFormGroup.controls['tdesde'].setValidators([Validators.required]);
      this.myFormGroup.controls['thasta'].setValidators([Validators.required]);
    } else {
      this.promoActiva = false;
      this.myFormGroup.controls['tdesde'].setValue('');
      this.myFormGroup.controls['thasta'].setValue('');
      this.myFormGroup.controls['tdesde'].clearValidators();
      this.myFormGroup.controls['thasta'].clearValidators();
    }
    this.myFormGroup.controls['tdesde'].updateValueAndValidity();
    this.myFormGroup.controls['thasta'].updateValueAndValidity();
  }

  public valueDateInputs(dateData) {
    if (dateData) {
      return this.dDate = moment(dateData).toDate();
    }
  }

  public ListaTipoTarifa(event) {
    const tt = event;
    const data = JSON.parse(JSON.stringify(this.dataGrid));
    this.listTtipo = filterBy(data, {
      logic: 'and', filters: [
        { field: 'ttipo', operator: 'eq', value: tt },
        { field: 'Inactivo', operator: 'eq', value: false }
      ]
    });
    this.listTtipo.forEach(item => item.tdesc = item.tdesc + ' - ' + item.Tarifa);
    this.listTtipo2 = JSON.parse(JSON.stringify(this.listTtipo));
    return this.listTtipo;
  }

  public calculoTarifa(event) {
    this.myFormGroup.patchValue({ mred: 0, tbase: '' });
  }

  public useSemaforo(event: boolean = false) {
    if (event === true) {
      this.myFormGroup.controls['Semaforo'].enable();
      this.myFormGroup.controls['starifa'].enable();
    } else {
      this.myFormGroup.controls['Semaforo'].disable();
      this.myFormGroup.controls['starifa'].disable();
    }
    this.myFormGroup.patchValue({ Semaforo: '', starifa: '' });
  }

  public formGrup() {
    this.myFormGroup = new FormGroup({
      proc: new FormControl(this.proc, Validators.required),
      Tarifa: new FormControl('', Validators.required, Validators.maxLength[20]),
      ttipo: new FormControl('', Validators.required),
      tdesc: new FormControl('', Validators.required, Validators.maxLength[40]),
      tmon: new FormControl('', Validators.required),
      ttfl: new FormControl(false),
      tprom: new FormControl(false),
      tdesde: new FormControl(''),
      thasta: new FormControl(''),
      tgratis: new FormControl(0),
      tnoches: new FormControl(0),
      tminimo: new FormControl(0),
      tpt: new FormControl(false),
      ttarifa: new FormControl(''),
      tcont: new FormControl(false),
      tope: new FormControl(''),
      Semana: new FormControl(''),
      CET: new FormControl(''),
      Restringida: new FormControl(false),
      tsec: new FormControl(''),
      crv: new FormControl(false),
      tantes: new FormControl(''),
      fiantes: new FormControl(''),
      ftantes: new FormControl(''),
      tcat: new FormControl('', Validators.required),
      Segmento: new FormControl(''),
      Notas: new FormControl(''),
      Internet: new FormControl(false),
      Calculo: new FormControl(''),
      tbase: new FormControl(''),
      mred: new FormControl(0),
      usem: new FormControl(false),
      Semaforo: new FormControl(''),
      starifa: new FormControl([]),
      cCe1: new FormControl(''),
      cCe2: new FormControl(''),
      cCe3: new FormControl(''),
      cCe4: new FormControl(''),
      cCe5: new FormControl(''),
      cCe6: new FormControl(''),
      cCe7: new FormControl(''),
      cCe8: new FormControl(''),
      cCe9: new FormControl(''),
      cCe10: new FormControl(''),
      tselec: new FormControl(''),
      Inactivo: new FormControl(false),
      cRowID: new FormControl(''),
      lError: new FormControl(false),
      cErrDes: new FormControl(''),
    });

    this.onChangesForm();
  }

  onChangesForm() {
    this.myFormGroup.valueChanges.subscribe(change => {
      if (change['usem'] && change['tprom']) {
        this.toast({ text: 'Uso semaforo no puede ser tambien promoción', type: 'info' });
      }
      if (change['ttarifa'] && change['ttfl']) {
        this.toast({ text: 'Tarifa Cont. no procede con T.F.L', type: 'info' });
      }
    });
  }

  public getDataSource(tarifa?) {
    this.loading = true;
    this._crudservice.getData(this.urlSec + '/' + this.proc + GLOBAL.char174)
      .map(data => data = data[this.siBase][this.ds][this.tt])
      .subscribe(data => this.resultData(data, tarifa));
  }

  resultData(data, tarifa?) {
    this.dataGrid = data;
    this.dataComplete = data;
    this.dataGrid.forEach(item => {
      if (item.starifa) {
        item.starifa = item.starifa.split(',')
      }
      if (item.Semaforo) {
        item.Semaforo = item.Semaforo.split(',')
      }
    });
    this.filter = {
      logic: 'and',
      filters: [
        { field: 'Inactivo', operator: 'eq', value: false },
      ]
    };
    this.dsTarifasProm = filterBy(this.dataGrid, {
      logic: 'and', filters: [
        { field: 'Inactivo', operator: 'eq', value: false },
      ],
    });
    this.loading = false;
    if (tarifa) {
      this.dataGrid.forEach(item => {
        if (item.Tarifa === tarifa) {
          this.myFormGroup.patchValue(item);
        }
      });
    }
  }
  handleFilter(tarifa) {
    this.listTtipo = this.listTtipo2.filter((s) => {
      const res = s.Tarifa.toLowerCase().indexOf(tarifa.toLowerCase()) !== -1;
      if (!res) { return s.tdesc.toLowerCase().indexOf(tarifa.toLowerCase()) !== -1; }
      return res;
    });
  }

  public newRow() {
    this.formGrup();
    this.useSemaforo(false);
    this.chckPromo(false);
    this.flagEdit = false;
    this.formStatus = 'Nuevo';
    this.tarifa = '';
    this.openModal(this.modalForm)
  }

  public guardarForm() {
    (this.flagEdit) ? this.aForm('edit') : this.aForm('new');
  }

  public editRow(dataItem) {
    this.formGrup();
    this.tarifa = dataItem.Tarifa;
    this.chckPromo(dataItem.tprom);
    Object.assign(this.formItems, dataItem);
    this.ListaTipoTarifa(dataItem.ttipo);
    if (dataItem.tprom === true) {
      const tdesde = dataItem.tdesde ? this.valueDateInputs(dataItem.tdesde) : dataItem.tdesde;
      Object.assign(this.formItems, { 'tdesde': tdesde });
      const thasta = dataItem.thasta ? this.valueDateInputs(dataItem.thasta) : dataItem.thasta;
      Object.assign(this.formItems, { 'thasta': thasta });
      if (dataItem.fiantes) {
        const fiantes = dataItem.fiantes ? this.valueDateInputs(dataItem.fiantes) : dataItem.fiantes;
        Object.assign(this.formItems, { 'fiantes': fiantes });
      }
      if (dataItem.ftantes) {
        const ftantes = dataItem.ftantes ? this.valueDateInputs(dataItem.ftantes) : dataItem.ftantes;
        Object.assign(this.formItems, { 'ftantes': ftantes });
      }
    }
    this.myFormGroup.patchValue(this.formItems);
    this.flagEdit = true;
    this.openModal(this.modalForm);
    this.formStatus = 'Editar';
  }

  private aForm(event, dataDelete: any = {}) {
    let service: any = '';
    const form = JSON.parse(JSON.stringify(this.myFormGroup.value));
    if (Array.isArray(form.starifa)) {
      form.starifa = form.starifa.join(',');
    }
    if (Array.isArray(form.Semaforo)) {
      form.Semaforo = form.Semaforo.join(',');
    }
    switch (event) {
      case 'new':
        service = this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt);
        break;
      case 'edit':
        service = this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt);
        break;
      case 'delete':
        service = this._crudservice.deleteData(dataDelete, this.urlSec, this.tt);
        break;
    }
    service.subscribe(data => {
      const response = data.siTarifa.dsTarifas.ttTarifas;
      if (response && response[0].cErrDes) {
        this.toast({ type: 'error', title: GLOBAL.textError, text: response[0].cErrDes });
      } else {
        if (event !== 'delete') {
          this.toast({ title: GLOBAL.textSuccess, type: 'success' });
        } else {
          this.toast({ title: GLOBAL.textDeleteSuccess, type: 'success' });
        }
        this.flagEdit = true;
        this.tarifa = this.myFormGroup.value.Tarifa;
        this.resultData(response, this.tarifa);
        this._modalService.dismissAll();
      }
    });
  }

  public deleteRow(dataItem) {
    this.aForm('delete', dataItem);
  }

  eliminarTarifaAgencia(dataItem) {
    this._crudservice.deleteData(dataItem, '/manttos/tarage', 'ttAgtarifas')
      .subscribe(res => this.getTarifasAgencia(dataItem.atarifa, dataItem.prov));
  }

  nuevaTarifaAgencia() {
    this.formAgencia.Agencia = '';
  }

  guardarTarifaAgencia() {
    if (this.flagTarifaEmpresa) {
      this.formAgencia.prov = 'E';
    } else {
      this.formAgencia.prov = 'A';
    }
    this.formAgencia.atarifa = this.tarifaSelected.Tarifa;
    this.formAgencia.atipo = this.tarifaSelected.ttipo;
    this._crudservice.postInfo(this.formAgencia, '/manttos/tarage', 'dsAgtarifas', 'ttAgtarifas').subscribe(data => {
      const result = data.siAgtarifa.dsAgtarifas.ttAgtarifas;
      if (result === undefined) {
        this.getTarifasAgencia(this.formAgencia.atarifa, this.formAgencia.prov);
      } else {
        if (result && result[0].cErrDes) {
          this.toast({ title: GLOBAL.textError, text: result[0].cErrDes, type: 'error' });
        }
      }
    });
  }

  public infoDetalleTarifa() {
    this._crudservice.getData('/ayuda/tarifas®®®®®')
      .map(data => data = data.siAyuda.dsAyuda).subscribe(data => {
        this.ttTarifas = data.ttTarifas;
        this.ttTardet = data.ttTardet;
      });
  }

  // Generear PDF
  public printPDF() {
    this._pdfService.printTarifasDetalle(this.seccion, this.PDFTarifas, this.ttTarifas, this.ttTardet, 'l');
  }

  detalleSelect({ selectedRows }) {
    if (selectedRows && selectedRows[0].dataItem) {
      const item = selectedRows[0].dataItem;
      if (item.ttipo === 'E' || item.ttipo === 'C') {
        this.desgloseDetaTari = true;
        this.ngFormDetaTari.get('detalle').clearValidators();
      } else if (item.ttipo === 'P' || item.ttipo === 'A') {
        this.desgloseDetaTari = false;
        this.ngFormDetaTari.get('detalle').setValidators([Validators.required]);
      }

      item.tfecini = moment(item.tfecini).toDate();
      item.tfecter = moment(item.tfecter).toDate();
      this.ngFormDetaTari.patchValue(item);
      this.flagEditTarifa = true;
      const fecini = this.ngFormDetaTari.get('tfecini');
      if (fecini.value >= this.fechaActual) {
        this.fechaMin = this.fechaActual;
      } else {
        this.fechaMin = null;
      }
    }
  }

  nuevoDetaTari() {
    this.createFgDetTar();
    this.fechaMin = this.fechaActual;
    // this.ngFormDetaTari.get('tfecini').setValidators([Validators.min(this.fechaActual)]);
    this.flagEditTarifa = false;
    const item = this.ngFormDetaTari.value;
    if (item.ttipo === 'E' || item.ttipo === 'C') {
      this.desgloseDetaTari = true;
      this.ngFormDetaTari.get('detalle').clearValidators();
    } else if (item.ttipo === 'P' || item.ttipo === 'A') {
      this.desgloseDetaTari = false;
      this.ngFormDetaTari.get('detalle').setValidators([Validators.required]);
    }
  }

  createFgDetTar() {
    this.ngFormDetaTari = this._formBuilder.group({
      Envia: [''],
      cCe1: [''],
      cCe2: [''],
      cCe3: [''],
      cCe4: [''],
      cCe5: [''],
      cCe6: [''],
      cCe7: [''],
      cCe8: [''],
      cCe9: [''],
      cCe10: [''],
      cErrDesc: [''],
      cRowID: [''],
      cTarifa: [''],
      detalle: [''],
      lError: [false],
      proc: [this.myFormGroup.value.proc],
      tarifa: [this.myFormGroup.value.Tarifa],
      tcuarto: ['', Validators.required],
      tfecini: [this.fechaActual],
      tfecter: [this.fechaActual],
      tope: [''],
      tta1: [0, [Validators.required]],
      tta2: [0, [Validators.required]],
      tta3: [0, [Validators.required]],
      tta4: [0],
      tta5: [0],
      tta6: [0],
      tta7: [0],
      tta8: [0],
      tta9: [0],
      tta10: [0],
      tta11: [0],
      ttipo: [this.myFormGroup.value.ttipo],
      ttj1: [0],
      ttj2: [0],
      ttj3: [0],
      ttj4: [0],
      ttj5: [0],
      ttj6: [0],
      ttj7: [0],
      ttj8: [0],
      ttj9: [0],
      ttj10: [0],
      ttj11: [0],
      ttm1: [0],
      ttm2: [0],
      ttm3: [0],
      ttm4: [0],
      ttm5: [0],
      ttm6: [0],
      ttm7: [0],
      ttm8: [0],
      ttm9: [0],
      ttm10: [0],
      ttm11: [0]
    });
  }

  eliminarDetalle(dataItem) {
    this.loadingTarifas = true;
    this._crudservice.deleteData(dataItem, '/manttos/tardet', 'ttTardet').subscribe(data => {
      this.loadingTarifas = false;
      if (data.siTardet.dsMensajes.ttMensError !== undefined) {
        return this.toast({ type: 'error', title: 'No se pudo eliminar el registro.' });
      }
      this.loadDeTarifa(this.myFormGroup.value);
    });
  }

  selectedAgencia({ selectedRows }) {
    if (selectedRows && selectedRows[0]) {
      this.formAgencia = selectedRows[0].dataItem;
    }
  }

  guardarDetaTari() {
    let responseService;
    const form = JSON.parse(JSON.stringify(this.ngFormDetaTari.value));
    form.tfecini = moment(form.tfecini).format(GLOBAL.formatIso);
    form.tfecter = moment(form.tfecter).format(GLOBAL.formatIso);
    if (moment(this.ttInfocia.fday).isAfter(form.tfecter)) {
      this.toast({ type: 'error', title: 'No es posible editar fechas pasadas.' });
    } else {
      this.loadingTarifas = false;
      if (this.flagEditTarifa) {
        responseService = this._crudservice.putInfo(form, '/manttos/tardet', 'dsTardet', 'ttTardet');
      } else {
        responseService = this._crudservice.postInfo(form, '/manttos/tardet', 'dsTardet', 'ttTardet');
      }
      responseService.subscribe(data => {
        const result = data.siTardet.dsTardet.ttTardet;
        this.loadingTarifas = false;
        if (result && result[0].cErrDesc) {
          this.toast({ title: result[0].cErrDesc, type: 'error' });
        } else {
          this.loadDeTarifa(this.myFormGroup.value);
        }
      });
    }
  }

  dataSource() {
    this.pdfGridFields = [
      { title: 'Cod.', dataKey: 'codigo', width: '', filtro: false },
      { title: 'Disp.', dataKey: 'rdisp', width: '', filtro: false },
      { title: 'Total', dataKey: 'rtotal', width: '', filtro: false },
      { title: 'Trab.', dataKey: 'Trabajo', width: '', filtro: false },
      { title: 'Dur.', dataKey: 'rdur', width: '', filtro: false },
      { title: 'Manual.', dataKey: 'rmmanual', width: '', filtro: false },
      { title: 'Inicio', dataKey: 'rini', width: '', filtro: false },
      { title: 'Term.', dataKey: 'rter', width: '', filtro: false },
      { title: 'Inac', dataKey: 'Inactivo', width: '', filtro: false },
    ];

    this.PDFTarifas = [
      { title: 'Proc', dataKey: 'proc' },
      { title: 'Tarifa', dataKey: 'Tarifa' },
      { title: 'T.T.', dataKey: 'ttipo' },
      { title: 'Descripción', dataKey: 'tdesc' },
      { title: 'Mon.', dataKey: 'tmon' },
      { title: 'Prom.', dataKey: 'tprom' },
      { title: 'Desde', dataKey: 'tdesde' },
      { title: 'Hasta', dataKey: 'thasta' },
      { title: 'Gratis', dataKey: 'tgratis' },
      { title: 'Por Cada', dataKey: 'tnoches' },
      { title: 'Mínimo', dataKey: 'tminimo' },
      { title: 'Tar.Cont', dataKey: 'tcont' },
    ];

    this.gridDeTarifa = [
      { title: 'Proc.', dataKey: 'proc', width: '', filtro: false },
      { title: 'Tarifa.', dataKey: 'tarifa', width: '', filtro: false },
      // { title: 'TArifa.', dataKey: 'cTarifa', width: '', filtro: false },
      { title: 'T.T.', dataKey: 'ttipo', width: '', filtro: false },
      { title: 'Tipo Cuarto', dataKey: 'tcuarto', width: '', filtro: false },
      { title: 'Fecha Inicio', dataKey: 'tfecini', width: '', filtro: false, editor: 'date', format: '{0:d}' },
      { title: 'Fecha Terminación', dataKey: 'tfecter', width: '', filtro: false, editor: 'date', format: '{0:d}' },
      { title: 'Sencillo', dataKey: 'tta1', width: '', filtro: false, editor: 'numeric' },
      { title: 'Doble', dataKey: 'tta2', width: '', filtro: false, editor: 'numeric' },
      { title: 'Triple', dataKey: 'tta3', width: '', filtro: false, editor: 'numeric' },
      { title: 'Adicional', dataKey: 'tta11', width: '', filtro: false, editor: 'numeric' },
      { title: 'Junior', dataKey: 'ttj11', width: '', filtro: false, editor: 'numeric' },
      { title: 'Menor', dataKey: 'ttm11', width: '', filtro: false, editor: 'numeric' },
      { title: 'Desg', dataKey: 'detalle', width: '', filtro: false },
    ];

    this._crudservice.getData('/manttos/monedas')
      .map(data => data = data.siMoneda.dsMonedas.ttMonedas)
      .subscribe(data => this.ttMonedas = data);
    this._crudservice.getData('/manttos/detalles')
      .map(data => data = data.siDetalle.dsDetalles.ttDetalles)
      .subscribe(data => this.dsDesgloses = data);
    this._crudservice.getData('/manttos/tarcateg')
      .map(data => data = data.siTarcateg.dsTarcateg.ttTarcateg)
      .subscribe(data => this.ttTarcateg = data);
    this._crudservice.getData('/manttos/segmentos')
      .map(data => data = data.siSegmento.dsSegmentos.ttSegmentos)
      .subscribe(data => this.ttSegmentos = data);
    this._crudservice.getData('/manttos/semaforos')
      .map(data => data = data.siSemaforo.dsSemaforos.ttSemaforos)
      .subscribe(data => this.ttSemaforos = data);
    this._crudservice.getData('/ayuda/tcuartos').subscribe(data => {
      this.dsTipoCuartos = data.siAyuda.dsAyuda.ttTcuartos;
      const addOption = JSON.parse(JSON.stringify(this.dsTipoCuartos[0]));
      addOption.tcuarto = '*';
      addOption.tdesc = 'Todos (*)';
      this.dsTipoCuartos.push(addOption);
      return this.dsTipoCuartos = filterBy(this.dsTipoCuartos, {
        logic: 'and', filters: [
          { field: 'inactivo', operator: 'eq', value: false },
          { field: 'tuso', operator: 'eq', value: true },
          { field: 'tagrp', operator: 'isempty', value: true }
        ],
      })
    });
    this.tts = [
      { id: 'E', title: 'E.P.', value: false },
      { id: 'A', title: 'ALL INCLUSIVE', value: false },
      { id: 'P', title: 'PAQUETE', value: false },
      { id: 'C', title: 'TARIFA EN CORTESIA', value: false },
    ];
    this.procList = [
      { id: 'M', title: 'MAYORISTA', value: false },
      { id: 'T', title: 'TARIFA PÚBLICO GRAL', value: false }
    ];
    this.dsCalculo = [
      { id: 'P', title: 'PORCENTAJE', value: false },
      { id: 'M', title: 'MONTO', value: false }
    ];
  }


  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || { logic: 'and', filters: [] };
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  distinctPrimitive(fieldName: string) {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }
}
