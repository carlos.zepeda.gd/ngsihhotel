import { Injectable } from '@angular/core';
import { CrudService } from '../../mantto-srv.services';
import { DateFormatService } from '../../mantto-dateformat.service';

@Injectable({
  providedIn: 'root'
})
export class SuplementosService {

  private urlSec: string = '/manttos/tardet/';
  private siBase: string = 'siTardet'; // siTardet.dsTardet.ttTardet
  private ds: string = 'dsTardet';
  private tt: string = 'ttTardet';
  public errorMesj: string = null;
  private itemsDeTarifa: any = [];
  public result: any = [];

  constructor(
    private _crudservice: CrudService,
    private dateFormat: DateFormatService,
  ) { }

  public remove(dataItem: any): void {
    this.aForm('destroy', dataItem);
  }

  public save(dataItem: any, isNew: boolean): void {
    if (isNew) {
      this.itemsBase();
      this.assignValues(this.itemsDeTarifa, dataItem);
      const tfecini = this.dateFormat.formatDate(dataItem.tfecini);
      Object.assign(this.itemsDeTarifa, { 'tfecini': tfecini });
      const tfecter = this.dateFormat.formatDate(dataItem.tfecter);
      Object.assign(this.itemsDeTarifa, { 'tfecter': tfecter });
      this.aForm('create', this.itemsDeTarifa);
    } else {
      this.aForm('update', dataItem);
    }
  }

  public assignValues(target: any, source: any): void {
    Object.assign(target, source);
  }

  public aForm(action: string = '', form?: any) {
    this.result = '';
    let service: any;
    switch (action) {
      case 'create':
        service = this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt)
        break
      case 'update':
        service = this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt)
        break
      case 'destroy':
        service = this._crudservice.deleteData(form, this.urlSec, this.tt);
        break
      default:
        break
    }
    service.subscribe(data => {
      this.errorMesj = (data[this.siBase].dsMensajes.ttMensError == null) ?
        null : data[this.siBase].dsMensajes.ttMensError[0].cMensTxt;
      this.result = (this.errorMesj) ? this.errorMesj : 'Acción ejecutada correctamente! :)';

    });
  }

  private itemsBase() {
    this.itemsDeTarifa = {
      proc: '',
      tarifa: '',
      cTarifa: '',
      ttipo: '',
      tcuarto: '',
      tfecini: '',
      tfecter: '',
      tta1: 0,
      tta2: 0,
      tta3: 0,
      tta4: 0,
      tta5: 0,
      tta6: 0,
      tta7: 0,
      tta8: 0,
      tta9: 0,
      tta10: 0,
      tta11: 0,
      ttj1: 0,
      ttj2: 0,
      ttj3: 0,
      ttj4: 0,
      ttj5: 0,
      ttj6: 0,
      ttj7: 0,
      ttj8: 0,
      ttj9: 0,
      ttj10: 0,
      ttj11: 0,
      ttm1: 0,
      ttm2: 0,
      ttm3: 0,
      ttm4: 0,
      ttm5: 0,
      ttm6: 0,
      ttm7: 0,
      ttm8: 0,
      ttm9: 0,
      ttm10: 0,
      ttm11: 0,
      detalle: 0,
      tope: '',
      Envia: '',
      cCe1: '',
      cCe2: '',
      cCe3: '',
      cCe4: '',
      cCe5: '',
      cCe6: '',
      cCe7: '',
      cCe8: '',
      cCe9: '',
      cCe10: '',
      cRowID: '',
      lError: false,
      cErrDesc: '',
    }
    return this.itemsDeTarifa;
  }
}
