import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { filterBy } from '@progress/kendo-data-query';
import { PdfService } from '../../../mantto-pdf.service';
import { CrudService } from '../../mantto-srv.services';
import { GLOBAL } from '../../../../services/global';
const _ = GLOBAL.char174;
@Component({
  selector: 'app-help-suplementos',
  templateUrl: './help-suplementos.component.html',
  styleUrls: ['./help-suplementos.component.css']
})
export class HelpSuplementosComponent implements OnInit, OnChanges, OnDestroy {
  @Input() tarifa: any = [];
  @Input() consulta: boolean = false;
  @Input() mantto: string = 'S';
  @Input() uso: string;
  ngForm: FormGroup;
  api: Array<string> = ['/manttos/tarsup/', 'siTarsup', 'dsTarsup', 'ttTarsup'];
  apiH: Array<string> = ['/rec/huestar/', 'siHuestar', 'dsHuestar', 'TarSuplementos'];
  apiM: Array<string> = ['/manttos/suplementos', 'siSuplemento', 'dsSuplementos', 'ttSuplementos'];
  public infoRow: any;
  toast = GLOBAL.toast;
  public kendoFormat = localStorage.getItem('formatKendo');
  public aplicaTarifa: boolean = true;
  public dsSuplementos: any = [];
  public tblSupl: any = [];
  public ttSuplementos: any = [];
  public suplTarifa: any = [];
  public newSuplTar: any = [];
  public ttCodigos: any = [];
  public calculos: any = [
    { title: 'Fijo', key: 'F' },
    { title: 'Personas', key: 'P' },
    { title: 'Adultos', key: 'A' },
    { title: 'Juniors', key: 'J' },
    { title: 'Menores', key: 'M' }
  ];
  aplTarifa: any = [
    { title: 'Aplica en Tarifa', key: 'A' },
    { title: 'Incluido en Tarifa', key: 'I' }
  ];
  tVenta: any = [
    { title: 'Tarifa', key: 'T' },
    { title: 'Opcional', key: 'O' }
  ];
  dias: any = [
    { id: 'Domingo', dia: 'Domingo', value: true },
    { id: 'Lunes', dia: 'Lunes', value: true },
    { id: 'Martes', dia: 'Martes', value: true },
    { id: 'Miercoles', dia: 'Miércoles', value: true },
    { id: 'Jueves', dia: 'Jueves', value: true },
    { id: 'Viernes', dia: 'Viernes', value: true },
    { id: 'Sabado', dia: 'Sábado', value: true }
  ];
  aplicaMonto: any = [
    { title: 'Diario', key: 'D' },
    { title: 'Entrada', key: 'E' }
  ];
  gridSuplmentos: any = [
    // { title: 'Proc.', dataKey: 'Proc', width: '', filtro: false },
    // { title: 'Tarifa.', dataKey: 'tarifa', width: '', filtro: false },
    // { title: 'T.T.', dataKey: 'ttipo', width: '', filtro: false },
    { title: 'Supl.', dataKey: 'Suplemento', width: '120', class: '', filtro: false },
    { title: 'Descripción', dataKey: 'sdesc', width: '200', class: '', filtro: false },
    { title: 'Ad.', dataKey: 'Monto1', width: '', class: 'text-right', filtro: false },
    { title: 'Jrs.', dataKey: 'Monto2', width: '', class: 'text-right', filtro: false },
    { title: 'Men.', dataKey: 'Monto3', width: '', class: 'text-right', filtro: false },
    { title: 'P.A.', dataKey: 'Monto4', width: '', class: 'text-right', filtro: false },
    { title: 'Desde', dataKey: 'pe1', width: '', class: 'text-right', filtro: false },
    { title: 'Mon.', dataKey: 'moneda', width: '', class: '', filtro: false },
    { title: 'Fec. Ini.', dataKey: 'sfecini', width: '110', class: '', filtro: false, hidden: true },
    { title: 'Fec. Term.', dataKey: 'sfecter', width: '110', class: '', filtro: false, hidden: true },
  ];
  errorMesj: any;
  refresh: any = new EventEmitter;
  userAuthValid: boolean;
  messageUserAuth: string;
  getAuth: boolean;
  loading: boolean;
  userAuth: any;

  constructor(private _crud: CrudService,
    private _pdfService: PdfService) {

    this._crud.getData('/ayuda/codigos®d®®y®')
      .map(data => data = data.siAyuda.dsAyuda.ttCodigos)
      .subscribe(data => this.ttCodigos = data);
  }

  ngOnDestroy(): void {
    this.refresh.emit(this.tarifa.folio);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['tarifa'] && changes['tarifa']['currentValue']) {
      this.fGroup();
    }

    if (changes['uso'] && changes['uso']['currentValue']) {
      this.ddlSuplementos();
      this.crudService2('search', []);
    } else {
      this.loadSuplemento();
    }
  }

  ngOnInit() {
  }

  public fGroup() {
    this.ngForm = new FormGroup({
      Proc: new FormControl(this.tarifa.proc),
      tarifa: new FormControl(this.tarifa.Tarifa),
      cTarifa: new FormControl(''),
      ttipo: new FormControl(this.tarifa.ttipo),
      Suplemento: new FormControl('', Validators.required),
      cSuplemento: new FormControl(''),
      cRowID: new FormControl(''),
      lError: new FormControl(false),
      cErrDesc: new FormControl(''),
    });
  }

  public rowInfo({ selectedRows }) {
    this.infoRow = selectedRows[0].dataItem;
    const dSema: any = {};
    this.itemsCheck(this.infoRow.Semana, this.dias, dSema);
    this.aplTarValue(this.infoRow.Codigo);
  }

  public itemsCheck(arrayItems, arrayToCompare, arrayToAssign) {
    const arraySplit = arrayItems.split('');
    for (let i = 0; i < arraySplit.length; i++) {
      arrayToCompare[i].value = (arraySplit[i] === 'S') ? true : false;
      const k = arrayToCompare[i].id
      const v = (arraySplit[i] === 'S') ? true : false;
      Object.assign(arrayToAssign, { [k]: v });
    }
  }

  public aplTarValue(event) {
    this.aplicaTarifa = (event) ? false : true;
  }

  ddlSuplementos() {
    this.newSuplTar = [];
    this._crud.getData('/manttos/tarsup/' + this.tarifa.proc + '®' + this.tarifa.Tarifa + '®' + this.tarifa.ttipo + '®O')
      .subscribe(res => {
        this.dsSuplementos = res['siTarsup']['dsTarsup']['ttTarsup'];
      });
  }

  public loadSuplemento() {
    this._crud.getData(this.api[0] + this.tarifa.proc + '®' + this.tarifa.Tarifa + '®' + this.tarifa.ttipo).subscribe(data => {
      const res = data[this.api[1]][this.api[2]][this.api[3]];
      this.tblSupl = res ? res : [];
      this.suplementoTarifa();
    });
  }

  public suplementoTarifa() {
    this.newSuplTar = [];
    this.ttSuplementos = [];
    this._crud.getData(this.apiM[0]).map(data => data = data[this.apiM[1]][this.apiM[2]][this.apiM[3]])
      .subscribe(data => {
        if (this.tblSupl) {
          this.tblSupl.forEach(supl => {
            this.suplTarifa = filterBy(data, { field: 'Suplemento', operator: 'eq', value: supl.Suplemento });
            this.suplTarifa = this.suplTarifa[0];
            this.ttSuplementos = Object.assign(this.suplTarifa, supl);
            this.newSuplTar.push(this.ttSuplementos);
          });
        }

        // Obtener suplementos de manttos
        if (data && !this.uso) {
          return this.dsSuplementos = filterBy(data, {
            logic: 'and', filters: [{ field: 'Inactivo', operator: 'eq', value: false }]
          });
        }

      });
  }

  setUrl(method) {
    return `${method + _ + this.uso + _ + this.tarifa.folio + _ +
      this.tarifa.sec + _ + this.tarifa.proc + _ + this.tarifa.Tarifa + _ +
      this.tarifa.ttipo + _ + this.tarifa.moneda + _ + 'O'
      }`;
  }
  crudService2(method, dataItem) {
    // this.loading = true;
    let subscribe;
    if (this.userAuth && this.userAuth.user) {
      dataItem.Operador = this.userAuth.user;
    }

    if (method !== 'search' && this.mantto === 'N') {
      this.messageUserAuth = 'Modificar Suplementos'
      this.getAuth = true;
      this.userAuthValid = false;
    } else {
      switch (method) {
        case 'new':
          subscribe = this._crud.postInfo(dataItem, this.apiH[0] + this.setUrl('N'), this.apiH[2], this.apiH[3]);
          break;
        case 'delete':
          subscribe = this._crud.postInfo(dataItem, this.apiH[0] + this.setUrl('B'), this.apiH[2], this.apiH[3]);
          break;
        case 'search':
          subscribe = this._crud.getData(this.apiH[0] + 'tarsup/' + this.setUrl('C'));
          break;
      }

      subscribe.subscribe(resp => {
        const error = resp[this.apiH[1]]['dsMensajes']['ttMensError'];
        this.errorMesj = (error == null) ? null : error[0]['cMensTxt'];
        if (this.errorMesj) {
          this.toast({ type: 'error', title: GLOBAL.textError, text: this.errorMesj });
        } else {
          this.ngForm.controls['Suplemento'].setValue('');
          if (method === 'search') {
            this.newSuplTar = resp[this.apiH[1]][this.apiH[2]][this.apiH[3]];
          } else {
            this.crudService2('search', []);
          }
        }
        this.loading = false;
      });
    }
  }

  crudService(method, dataItem) {
    if (this.uso) {
      this.crudService2(method, dataItem);
    } else {
      let response: any;

      switch (method) {
        case 'new':
          response = this._crud.postInfo(dataItem, this.api[0], this.api[2], this.api[3]);
          break;
        case 'delete':
          response = this._crud.deleteData(dataItem, this.api[0], this.api[3]);
          break;
      }
      response.subscribe(resp => {
        const error = resp[this.api[1]]['dsMensajes']['ttMensError'];
        this.errorMesj = (error == null) ? null : error[0]['cMensTxt'];
        if (this.errorMesj) {
          this.toast({ type: 'error', title: GLOBAL.textError, text: this.errorMesj });
        } else {
          this.ngForm.controls['Suplemento'].setValue('');
          this.loadSuplemento();
        }
      });
    }
  }

  printPDFsupl(idTarifa) {
    this._pdfService.printPDF('Suplementos de Tarifa ' + idTarifa, this.gridSuplmentos, this.dsSuplementos, 'l');
  }

  userValue(user) {
    this.userAuth = user;
    if (user.permisos[1] === 'S') {
      this.mantto = 'S';
      this.userAuthValid = true;
      this.getAuth = false;
    }
  }

}
