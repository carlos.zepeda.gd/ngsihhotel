import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CrudService } from '../../mantto-srv.services';
import { ManttoMenuService } from '../../mantto-menu/mantto-menu.service';
import { GLOBAL } from 'app/services/global';


@Component({
  selector: 'app-mantto-semaforos',
  templateUrl: './mantto-semaforos.component.html',
  styleUrls: ['./../../manttos-desing.css'],
  styles: ['.template { display: inline-block; width: 2rem; height: 1rem; } '],
})
export class ManttoSemaforosComponent implements OnInit {
  heading = 'Mantenimientos de Semáforos';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  private api: Array<string> = ['/manttos/semaforos', 'siSemaforo', 'dsSemaforos', 'ttSemaforos'];
  public gridFields = [];
  public pdfGridFields = [];
  public pdfGrid = [];
  errorMesj: string = null;
  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  flagEdit: boolean;
  public filter: CompositeFilterDescriptor;
  public semaColor: any = [];
  public colour = {idColor: 0, color: ''};
  toast = GLOBAL.toast;
  loading = false;

  constructor(private _crudservice: CrudService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _modalService: NgbModal) {

    this.gridFields = [
      { title: 'Semáforo', dataKey: 'Semaforo', width: '180', filtro: true },
      { title: 'Descripción', dataKey: 'Descripcion', width: '250', filtro: true },
      { title: 'Color', dataKey: 'scolor', width: '150', filtro: true },
    ];
    this.pdfGridFields = [
      { title: 'Inactivo', dataKey: 'Inactivo', width: '', filtro: false },
    ];
    this.semaColor = [
      { idColor: 2, color: '#008000' }, { idColor: 3, color: '#008080' },
      { idColor: 6, color: '#808000' }, { idColor: 7, color: '#808080' },
      { idColor: 8, color: '#c0c0c0' }, { idColor: 10, color: '#34fc34' },
      { idColor: 11, color: '#00ffff' }, { idColor: 12, color: '#f00' },
      { idColor: 13, color: '#f0f' }, { idColor: 14, color: '#ff0' }
    ];
    this.formGrup();
    this.getDataSource();
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  public valueColor(event) {
    this.colour.color = (event === undefined) ? 'transparent' : event.color;
    this.colour.idColor = (event === undefined) ? 0 : event.idColor;
    this.myFormGroup.patchValue({
      scolor: this.colour.idColor
    });
  }

  public formGrup() {
    this.myFormGroup = new FormGroup({
      Semaforo: new FormControl('', Validators.required, Validators.maxLength[10]),
      Descripcion: new FormControl('', Validators.required, Validators.maxLength[40]),
      sdefault: new FormControl(false),
      scolor: new FormControl(''),
      cColor: new FormControl('#ffffff'),
      cImagen: new FormControl(''),
      Inactivo: new FormControl(false),
      cRowID: new FormControl(''),
      lError: new FormControl(false),
      cErrDesc: new FormControl(''),
    });
  }

  public getDataSource() {
    this.loading = true;
    this._crudservice.getData(this.api[0])
      .map(data => data = data[this.api[1]][this.api[2]][this.api[3]]).subscribe(data => {
        this.dataGrid = data;
      this.loading = false;
      });
  }

  public newRow(modal) {
    this.formGrup();
    this.flagEdit = false;
    this.openModal(modal);
  }

  openModal(modal) {
    this._modalService.open(modal, { size: 'sm', centered: true });
  }

  public guardarForm() {
    (this.flagEdit) ? this.aForm('edit') : this.aForm('new');
    this._modalService.dismissAll();
  }

  public editRow({ dataItem }, modal) {
    this.semaColor.forEach(element => {
      if (element.idColor === dataItem.scolor) {
        this.colour.color = element.color;
      }
    });
    this.myFormGroup.patchValue(dataItem);
    this.flagEdit = true;
    this.openModal(modal);

  }
  private aForm(event, dataDelete: any = {}) {
    this.loading = true;
    let service: any;
    const form = this.myFormGroup.value;
    switch (event) {
      case 'new':
        service = this._crudservice.postInfo(form, this.api[0], this.api[2], this.api[3])
        break
      case 'edit':
        service = this._crudservice.putInfo(form, this.api[0], this.api[2], this.api[3])
        break
      case 'delete':
        service = this._crudservice.deleteData(dataDelete, this.api[0], this.api[3]);
        break
    }
    service.subscribe(data => {
      this.errorMesj = (data[this.api[1]].dsMensajes.ttMensError == null) ?
        null : data[this.api[1]].dsMensajes.ttMensError[0].cMensTxt;
      if (this.errorMesj) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError,
          text: this.errorMesj
        });
        this.loading = false;
      } else {
        if (event === 'delete'){
          this.toast({
            type: 'success',
            title: GLOBAL.textDeleteSuccess
          });
        }else {
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          });
        }
        this.getDataSource();
        this.formGrup();
      }
    });
  }

  public deleteRow({ dataItem }) {
    this.aForm('delete', dataItem);
  }

  // Generear PDF
  public printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    });
  }
}
