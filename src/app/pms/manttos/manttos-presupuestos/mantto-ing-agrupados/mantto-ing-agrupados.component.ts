import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import swal from 'sweetalert2';
import {CrudService} from '../../mantto-srv.services';
import {PdfService} from '../../mantto-pdf.service';
import {ManttoMenuService} from '../../mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';
import { GLOBAL } from '../../../../services/global';

const flatten = GLOBAL.flatten;

@Component({
    selector: 'app-mantto-ing-agrupados',
    templateUrl: './mantto-ing-agrupados.component.html',
    styleUrls: ['../../manttos-desing.css'],
})
export class ManttoIngAgrupadosComponent implements OnInit {
    heading = 'Mantenimientos de Mercados';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';

    public myFormGroup: FormGroup;
    public dataGrid: any = [];
    public flagShowForm: boolean;
    private flagEdit: boolean;
    public dataComplete: any = [];
    public checked: boolean;
    public groups: GroupDescriptor[] = [];
    public filter: CompositeFilterDescriptor;
    public sort: SortDescriptor[] = [{field: 'punto', dir: 'asc'}];

    private urlSec: string = '/manttos/ingagrp';
    private ds: string = 'dsIngagrp'; // siIngagrp.dsIngagrp.ttIngagrp
    private tt: string = 'ttIngagrp';
    public gridFields = [];
    seccion: string = 'Ingresos Agrupados';
    errorMesj: string = null;
    formStatus: string = null;
    public dsEstados: any = [];
    public dsEstadosComplete: any = [];

    constructor(private _crudservice: CrudService,
                private _pdfService: PdfService,
                private _menuServ: ManttoMenuService,
                private _router: Router) {
        this.gridFields = [
            {field: 'iagrp', title: 'Ing. Agrp', width: '150', filtro: false},
            {field: 'cDesc', title: 'Descripción', width: '', filtro: true},
            {field: 'AC', title: 'AC', width: '', filtro: false},
        ];
        this.buildFormGroup();
        this._crudservice.getData('/manttos/estados')
            .map(data => data = data.siEstado.dsEstados.ttEstados).subscribe(data => {
            this.dsEstados = data;
            this.dsEstadosComplete = data;
        });
    }

    ngOnInit() {
        this.getDataSource();

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    private buildFormGroup() {
        this.myFormGroup = new FormGroup({
            iagrp: new FormControl('', Validators.required),
            cDesc: new FormControl('', Validators.required),
            AC: new FormControl(1),
            cRSocial: new FormControl(''),
            cDireccion1: new FormControl(''),
            cDireccion2: new FormControl(''),
            Exterior: new FormControl(''),
            Interior: new FormControl(''),
            cCiudad: new FormControl(''),
            Estado: new FormControl(''),
            cCP: new FormControl(''),
            RFC: new FormControl(''),
            eDireccion1: new FormControl(''),
            eDireccion2: new FormControl(''),
            eExterior: new FormControl(''),
            eInterior: new FormControl(''),
            eCiudad: new FormControl(''),
            eEstado: new FormControl(''),
            eCP: new FormControl(''),
            eRFT: new FormControl(''),
            cErrDesc: new FormControl(''),
            lError: new FormControl(false),
            cRowID: new FormControl(''),
        });
    }

    private getDataSource() {
        this._crudservice.getData(this.urlSec).map(data => data = data.siIngagrp.dsIngagrp.ttIngagrp)
            .subscribe(data => {
                this.dataGrid = data;
                this.dataComplete = data;
                this.dataGrid = {
                    data: orderBy(this.dataGrid, this.sort),
                    total: this.dataGrid.length
                };
            });
    }

    public newRow() {
        this.buildFormGroup();
        // this.listaEdos();
        this.flagShowForm = true;
        this.flagEdit = false;
        this.formStatus = 'Nuevo';
    }

    public guardarForm() {
        (this.flagEdit) ? this.updateForm() : this.saveRow();
    }

    public editRow({dataItem}) {
        this.myFormGroup.setValue(dataItem);
        this.flagEdit = true;
        this.flagShowForm = true;
        this.formStatus = 'Editar';
    }

    private saveRow() {
        const form = this.myFormGroup.value;
        if (form.Estado.Estado) {
            form.Estado = form.Estado.Estado
        }
        if (form.eEstado.Estado) {
            form.eEstado = form.eEstado.Estado
        }
        this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt)
            .subscribe(data => {
                this.errorMesj = (data.siIngagrp.dsMensajes.ttMensError == null) ?
                    null : data.siIngagrp.dsMensajes.ttMensError[0].cMensTxt;
                if (this.errorMesj) {
                    swal('ERROR!', this.errorMesj, 'error');
                } else {
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Registro guardado!!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                this.flagShowForm = false;
                this.getDataSource();
                this.closeForm();
            })
    }

    private updateForm() {
        const form = this.myFormGroup.value;
        if (form.Estado.Estado) {
            form.Estado = form.Estado.Estado
        }
        if (form.eEstado.Estado) {
            form.eEstado = form.eEstado.Estado
        }
        this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
            this.getDataSource();
            this.closeForm();
        });
    }

    public deleteRow({dataItem}) {
        this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
            this.errorMesj = (data.siIngagrp.dsMensajes.ttMensError == null) ?
                null : data.siIngagrp.ttMensError[0].cMensTxt;
            if (this.errorMesj) {
                swal({
                    position: 'center',
                    type: 'error',
                    title: 'Error!!',
                    text: this.errorMesj,
                    showConfirmButton: false,
                    timer: 1500
                })
            } else {
                this.getDataSource();
            }
            this.closeForm();
        });
    }

    public closeForm() {
        this.myFormGroup.reset();
        this.flagShowForm = false;
        this.flagEdit = false;
    }

    public close(component) {
        this[component + 'Opened'] = false;
        this.closeForm();
    }

    // Generear PDF
    public printPDF() {
        const columns = [
            {title: 'Ing. Agrp', dataKey: 'iagrp'},
            {title: 'Descripción', dataKey: 'cDesc'},
            {title: 'AC', dataKey: 'AC'},
        ];
        this._pdfService.printPDF(this.seccion, columns, this.dataGrid.data);
    }

    // Funciones para la Grid de Kendo
    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.dataGrid = process(this.dataComplete, {group: this.groups});
    }

    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.dataGrid.data = filterBy(this.dataComplete, filter);
    }

    public switchChange(checked: boolean): void {
        const root = this.filter || {logic: 'and', filters: []};
        const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
        if (!filter) {
            root.filters.push({
                field: 'Inactivo',
                operator: 'eq',
                value: checked
            });
        } else {
            filter.value = checked;
        }
        this.checked = checked;
        this.filterChange(root);
    }

    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.dataGrid = {
            data: orderBy(this.dataGrid.data, this.sort),
            total: this.dataGrid.data.length
        };
    }
}
