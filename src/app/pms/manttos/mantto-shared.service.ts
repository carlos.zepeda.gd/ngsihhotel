import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {GLOBAL} from '../../services/global';
import {HttpClient} from '@angular/common/http';

const headers = new Headers({'Content-Type': 'application/json'});

interface ResInterface {
    'response': any;
}

@Injectable()
export class ManttoSharedService {

    public url: string;

    constructor(private http: Http,
                private httpC: HttpClient) {
        this.url = GLOBAL.url;
    }

    getUsuarios() {
        return this.http.get(this.url + '/admin/manttos/userConf/')
            .map(res => res.json().response.siUserConf.dsUserConf.ttUserConf);
    }

    getUsuarioPms(user) {
        return this.http.get(this.url + '/manttos/usuarios/' + user)
            .map(res => res.json().response.siUsuario.dsUsuarios.ttUsuarios);
    }

    getRoles() {
        return this.http.get(this.url + '/admin/ayuda/RolMstr/' + GLOBAL.char174)
            .map(res => res.json().response.siRolMstr.dsRolMstr.ttRolMstr);
    }

    getSistemas() {
        return this.http.get(this.url + '/admin/ayuda/menu/' + GLOBAL.char174 + GLOBAL.char174)
            .map(res => res.json().response.siMenuGral.dsMenuGral.ttMenuGral);
    }

    getGrupos() {
        return this.http.get(this.url + '/ayuda/usrgrp')
            .map(res => res.json().response.siAyuda.dsAyuda.ttUsrgrp);
    }

    getDeptos() {
        return this.http.get(this.url + '/ayuda/deptos')
            .map(res => res.json().response.siAyuda.dsAyuda.ttDeptos);
    }

    getMeseros() {
        return this.http.get(this.url + '/ayuda/meseros')
            .map(res => res.json().response.siAyuda.dsAyuda.ttPmeseros);
    }

    getIngAgrupado() {
        return this.http.get(this.url + '/ayuda/ingagrp')
            .map(res => res.json().response.siAyuda.dsAyuda.ttIngagrp);
    }

    getTipos() {

    }

    postUserConf(data) {
        const json = JSON.stringify(data);
        const params = '{"dsUserConf":{"ttUserConf":[' + json + ']}}';

        return this.http.post(this.url + '/admin/manttos/userConf', params, {headers: headers})
            .map(res => {
                const succes = res.json().response.siUserConf.dsUserConf.ttUserConf;
                const error = res.json().response.siUserConf.dsMensajes.ttMensError;
                if (error) {
                    return error;
                } else {
                    return succes;
                }

            });
    }

    postUserPms(data) {
        const json = JSON.stringify(data);
        const params = '{"dsUsuarios":{"ttUsuarios":[' + json + ']}}';

        return this.http.post(this.url + '/manttos/usuarios', params, {headers: headers})
            .map(res => {
                const succes = res.json().response.siUsuarios.dsUsuarios.ttUsuarios;
                const error = res.json().response.siUsuarios.dsMensajes.ttMensError;
                if (error) {
                    return error;
                } else {
                    return succes;
                }

            });
    }

    putUserConf(data) {
        const json = JSON.stringify(data);
        const params = '{"dsUserConf":{"ttUserConf":[' + json + ']}}';

        return this.http.put(this.url + '/admin/manttos/userConf', params, {headers: headers})
            .map(res => res.json().response.siUserConf.dsUserConf.ttUserConf);
    }

    putUserPms(data) {
        const json = JSON.stringify(data);
        const params = '{"dsUsuarios":{"ttUsuarios":[' + json + ']}}';

        return this.http.put(this.url + '/manttos/usuarios', params, {headers: headers})
            .map(res => res.json().response.siUsuarios.dsUsuarios.ttUsuarios);
    }

    deleteUserConf(data) {
        const json = JSON.stringify(data);
        const params = '{"dsUserConf":{"ttUserConf":[' + json + ']}}';

        return this.http.delete(this.url + '/admin/manttos/userConf',
            new RequestOptions({headers: headers, body: params}))
            .map(res => res.json().response.siUserConf.dsUserConf.ttUserConf);
    }

    getAyudaCuartoCaract() {
        /* https://192.168.1.2:8811/front/rest/ayuda/ctocar */
        return this.httpC.get<ResInterface>(this.url + '/ayuda/ctocar');
    }

}
