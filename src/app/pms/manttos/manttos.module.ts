// Imports Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Modules
import { KendouiModule } from '../../shared/kendo.module';
import { ColorPickerModule } from 'ngx-color-picker';
// Services
import { InfociaGlobalService } from '../../services/infocia.service';
import { ManttoUsuariosService } from './mantto-usuarios/mantto-usuarios.service';
import { ManttoMenuService } from './mantto-menu/mantto-menu.service';
import { ManttoCuartoService } from './mantto-cuartos/mantto-cuartos.service';
import { ManttoRolesService } from './mantto-roles/mantto-roles.service';
import { ManttoRolesAddService } from './mantto-roles/mantto-roles-add/mantto-roles-add.service';
import { ManttoRolesMenuService } from './mantto-roles/mantto-roles-menu/mantto-roles-menu.service';
import { ManttoSharedService } from './mantto-shared.service';
import { CrudService } from './../manttos/mantto-srv.services';
import { PdfService } from './../manttos/mantto-pdf.service';
// Componentes
import { ManttoMenuComponent } from './mantto-menu/mantto-menu.component';
import { ManttoCuartosComponent } from './mantto-cuartos/mantto-cuartos.component';
import { ManttoUsuariosComponent } from './mantto-usuarios/mantto-usuarios.component';
// Mantto General
import { ManttoMonedasComponent } from './mantto-general/mantto-monedas/mantto-monedas.component';
import { ManttoTipoBandasComponent } from './mantto-general/mantto-tipo-bandas/mantto-tipo-bandas.component';
import { ManttoTiposReservacionComponent } from './mantto-general/mantto-tipos-reservacion/mantto-tipos-reservacion.component';
import { ManttoOrigenesReservacionComponent } from './mantto-general/mantto-origenes-reservacion/mantto-origenes-reservacion.component';
import { ManttoSegmentosMercadoComponent } from './mantto-general/mantto-segmentos-mercado/mantto-segmentos-mercado.component'
import { ManttoSubtotsSegmerComponent } from './mantto-general/mantto-subtots-segmer/mantto-subtots-segmer.component';
import { ManttoBancosComponent } from './mantto-general/mantto-bancos/mantto-bancos.component';
import { ManttoClasAgenComponent } from './mantto-general/mantto-clasificacion-agencias/mantto-clasificacion-agencias.component';
import { ManttoSucursalesComponent } from './mantto-general/mantto-sucursales/mantto-sucursales.component';
import { ManttoTiposNotasComponent } from './mantto-general/mantto-tipos-notas/mantto-tipos-notas.component';
import { ManttoMotivosRazonesComponent } from './mantto-general/mantto-motivos-razones/mantto-motivos-razones.component';
// Mantto Tarifas
import { ManttoTarifasComponent } from './mantto-tarifas/mantto-tarifas/mantto-tarifas.component';
import { ManttoSuplementosComponent } from './mantto-tarifas/mantto-suplementos/mantto-suplementos.component';
import { ManttoDesgloseTarifasComponent } from './mantto-tarifas/mantto-desglose-tarifas/mantto-desglose-tarifas.component';
import { ManttoCategoriasTarifaComponent } from './mantto-tarifas/mantto-categorias-tarifa/mantto-categorias-tarifa.component';
import { ManttoSemaforosComponent } from './mantto-tarifas/mantto-semaforos/mantto-semaforos.component';
import { ManttoSemaforosFechaComponent } from './mantto-tarifas/mantto-semaforos-fecha/mantto-semaforos-fecha.component';
// Mantto Requerimientos Especiales
import {
  ManttoRequerimientosEspecialesComponent
} from './mantto-requerimientos-especiales/mantto-requerimientos-especiales/mantto-requerimientos-especiales.component';
import {
  ManttoDepartamentosComponent
} from './mantto-requerimientos-especiales/mantto-departamentos/mantto-departamentos.component';
import {
  ManttoGruposRequerimientoComponent
} from './mantto-requerimientos-especiales/mantto-grupos-requerimiento/mantto-grupos-requerimiento.component';
import {
  ManttoSatisfaccionesComponent
} from './mantto-requerimientos-especiales/mantto-satisfacciones/mantto-satisfacciones.component';
import {
  ManttoTrabajosComponent
} from './mantto-requerimientos-especiales/mantto-trabajos/mantto-trabajos.component';
// Mantto Ingresos Agrupados
import { ManttoIngAgrupadosComponent } from './manttos-presupuestos/mantto-ing-agrupados/mantto-ing-agrupados.component';
// Mantto Lost and Found
import { ManttoArticulosComponent } from './mantto-lost-found/mantto-articulos/mantto-articulos.component';
import { ManttoAreasComponent } from './mantto-lost-found/mantto-areas/mantto-areas.component';
import { ManttoEmpleadosComponent } from './mantto-lost-found/mantto-empleados/mantto-empleados.component';
// Mantto Paises
import { ManttoPaisesEstadosComponent } from './mantto-paises-estados/mantto-paises-estados.component';
import { ManttoPaisesComponent } from './mantto-paises/mantto-paises.component';
import { ManttoRegionesComponent } from './mantto-regiones/mantto-regiones.component';
import { ManttoIdiomaComponent } from './mantto-idioma/mantto-idioma.component';
import { ManttoMercadosComponent } from './mantto-mercados/mantto-mercados.component';
import { ManttoRolesComponent } from './mantto-roles/mantto-roles.component';
import { ManttoRolesAddComponent } from './mantto-roles/mantto-roles-add/mantto-roles-add.component';
import { ManttoRolesMenuComponent } from './mantto-roles/mantto-roles-menu/mantto-roles-menu.component';
// Rutas
import { MANTTOS_PMS_ROUTING } from './manttos-pms.routes';
import { EditService } from './mantto-tarifas/mantto-desglose-tarifas/edit.service';
import { DateFormatService } from './mantto-dateformat.service';
import { DetalleTarifaService } from './mantto-tarifas/mantto-tarifas/detalleTarifa.service';
import { ArchitectModule } from '../../_architect/architect.module';
import { RfcCompaniasComponent } from './rfc-companias/rfc-companias.component';
import { EstadosCivilesComponent } from './Mantto Historicos/estados-civiles/estados-civiles.component';
import { ConsecutivosComponent } from './Mantto Facturacion/consecutivos/consecutivos.component';
import { VipComponent } from './mantto-general/vip/vip.component';
import { ClasesCodigosComponent } from './Mantto Codigos/clases-codigos/clases-codigos.component';
import { SubtotalCodigosComponent } from './Mantto Codigos/subtotal-codigos/subtotal-codigos.component';
import { ImpuestosComponent } from './Mantto Facturacion/impuestos/impuestos.component';
import { ClasifClientesComponent } from './Mantto Historicos/clasif-clientes/clasif-clientes.component';
import { UbicacionesComponent } from './mantto-cuartos/ubicaciones/ubicaciones.component';
import { TiposCuartosComponent } from './mantto-cuartos/tipos-cuartos/tipos-cuartos.component';
import { UnidadesMedidaComponent } from './Mantto Facturacion/unidades-medida/unidades-medida.component';
import { VendedoresComponent } from './mantto-general/vendedores/vendedores.component';
import { ManttoSeccionesComponent } from './mantto-cuartos/mantto-secciones/mantto-secciones.component';
import { CondicionesCreditoComponent } from './mantto-general/condiciones-credito/condiciones-credito.component';
import { CaractCuartoComponent } from './mantto-cuartos/caract-cuarto/caract-cuarto.component';
import { CuartosComponent } from './mantto-cuartos/cuartos/cuartos.component';
import { TiposCambioComponent } from './tipos-cambio/tipos-cambio.component';
import { ManttoEstadisticasComponent } from './Mantto Estadisticas/mantto-estadisticas/mantto-estadisticas.component';
import { DocumentosViajeComponent } from './Informacion Adicional/documentos-viaje/documentos-viaje.component';
import { EstadoHuespedesComponent } from './Informacion Adicional/estado-huespedes/estado-huespedes.component';
import { PuntosEntradaPaisComponent } from './Informacion Adicional/puntos-entrada-pais/puntos-entrada-pais.component';
import { SharedAppsModule } from 'app/shared/shared-apps.module';
import { CodigosComponent } from './Mantto Codigos/codigos/codigos.component';
import { CodigosService } from './Mantto Codigos/codigos/codigos.service';
import { ManttoTiposMovtosComponent } from './Mantto Codigos/mantto-tipos-movtos/mantto-tipos-movtos.component';
import { ManttoOcupacionesComponent } from './Mantto Historicos/mantto-ocupaciones/mantto-ocupaciones.component';
import { ManttoTitulosComponent } from './Mantto Historicos/mantto-titulos/mantto-titulos.component';
import { DesgloseCodigosComponent } from './Mantto Codigos/desglose-codigos/desglose-codigos.component';
import { ExcelModule, GridModule } from '@progress/kendo-angular-grid';
import { UnidadesMedidaCFDComponent } from './Facturacion/unidades-medida-cfd/unidades-medida-cfd.component';
import { ProductosServiciosCFDComponent } from './Facturacion/productos-servicios-cfd/productos-servicios-cfd.component';
import { FormasPagoCFDComponent } from './Facturacion/formas-pago-cfd/formas-pago-cfd.component';
import { MetodosPagoCFDComponent } from './Facturacion/metodos-pago-cfd/metodos-pago-cfd.component';
import { UsosCFDComponent } from './Facturacion/usos-cfd/usos-cfd.component';
import { RegimenesFiscalesCFDComponent } from './Facturacion/regimenes-fiscales-cfd/regimenes-fiscales-cfd.component';
import { TiposRelacionCFDComponent } from './Facturacion/tipos-relacion-cfd/tipos-relacion-cfd.component';
import { TiposComprobanteCFDComponent } from './Facturacion/tipos-comprobante-cfd/tipos-comprobante-cfd.component';
import { ManttoAjustesEstadisticasComponent } from './mantto-requerimientos-especiales/mantto-ajustes-estadisticas/mantto-ajustes-estadisticas.component';
import { ManttoCambiosTarifasComponent } from './mantto-tarifas/mantto-cambios-tarifas/mantto-cambios-tarifas.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedAppsModule,
    KendouiModule,
    ColorPickerModule,
    MANTTOS_PMS_ROUTING,
    ArchitectModule,
    ExcelModule,
    GridModule
  ],
  declarations: [
    ManttoMenuComponent,
    ManttoCuartosComponent,
    ManttoUsuariosComponent,
    CodigosComponent,
    // Mantto General
    ManttoMonedasComponent,
    ManttoTipoBandasComponent,
    ManttoTiposReservacionComponent,
    ManttoSegmentosMercadoComponent,
    ManttoSubtotsSegmerComponent,
    ManttoBancosComponent,
    ManttoClasAgenComponent,
    ManttoSucursalesComponent,
    ManttoTiposNotasComponent,
    ManttoMotivosRazonesComponent,
    // Mantto Tarifas
    ManttoTarifasComponent,
    ManttoCategoriasTarifaComponent,
    ManttoSuplementosComponent,
    ManttoDesgloseTarifasComponent,
    ManttoSemaforosComponent,
    ManttoSemaforosFechaComponent,
    // Mantto Requerimientos Especiales
    ManttoRequerimientosEspecialesComponent,
    ManttoDepartamentosComponent,
    ManttoGruposRequerimientoComponent,
    ManttoSatisfaccionesComponent,
    ManttoTrabajosComponent,
    // Mantto Ingresos Agrupados
    ManttoIngAgrupadosComponent,
    // Mantto Lost and Found
    ManttoArticulosComponent,
    ManttoAreasComponent,
    ManttoEmpleadosComponent,
    // Mantto Paises
    ManttoPaisesEstadosComponent,
    ManttoPaisesComponent,
    ManttoRegionesComponent,
    ManttoIdiomaComponent,
    ManttoMercadosComponent,
    ManttoRolesComponent,
    ManttoRolesAddComponent,
    ManttoRolesMenuComponent,
    ManttoOrigenesReservacionComponent,
    RfcCompaniasComponent,
    EstadosCivilesComponent,
    ConsecutivosComponent,
    VipComponent,
    ClasesCodigosComponent,
    SubtotalCodigosComponent,
    ImpuestosComponent,
    ClasifClientesComponent,
    UbicacionesComponent,
    TiposCuartosComponent,
    UnidadesMedidaComponent,
    VendedoresComponent,
    ManttoSeccionesComponent,
    CondicionesCreditoComponent,
    CaractCuartoComponent,
    CuartosComponent,
    TiposCambioComponent,
    ManttoEstadisticasComponent,
    DocumentosViajeComponent,
    EstadoHuespedesComponent,
    PuntosEntradaPaisComponent,
    DesgloseCodigosComponent,
    ManttoTiposMovtosComponent,
    ManttoOcupacionesComponent,
    ManttoTitulosComponent,
    UnidadesMedidaCFDComponent,
    ProductosServiciosCFDComponent,
    FormasPagoCFDComponent,
    MetodosPagoCFDComponent,
    UsosCFDComponent,
    RegimenesFiscalesCFDComponent,
    TiposRelacionCFDComponent,
    TiposComprobanteCFDComponent,
    ManttoAjustesEstadisticasComponent,
    ManttoCambiosTarifasComponent,
  ],
  providers: [
    ManttoMenuService,
    InfociaGlobalService,
    ManttoCuartoService,
    ManttoUsuariosService,
    ManttoRolesService,
    ManttoSharedService,
    ManttoRolesAddService,
    ManttoRolesMenuService,
    CrudService,
    PdfService,
    EditService,
    DateFormatService,
    DetalleTarifaService,
    CodigosService
  ]
})
export class ManttosModule {
}

