import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process } from '@progress/kendo-data-query';
import { InfociaGlobalService } from '../../../services/infocia.service';
import 'rxjs/add/operator/map';
import { CrudService } from '../mantto-srv.services';
import { ManttoMenuService } from '../mantto-menu/mantto-menu.service';
import { Router } from '@angular/router';
import { GLOBAL } from '../../../services/global';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-mantto-paises',
  templateUrl: './mantto-paises.component.html',
  styleUrls: ['./../manttos-desing.css']
})
export class ManttoPaisesComponent implements OnInit {
  heading = 'Mantenimiento de Países';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  public myFormGroup: FormGroup;
  flagEdit: boolean;
  public flagShowForm: boolean;
  public dsManuales = [];
  private ttInfocia: any;
  public checked = false;
  public dataComplete: any = [];
  public dataGrid: any = [];
  public filter: CompositeFilterDescriptor;
  public groups: GroupDescriptor[] = [];
  public sort: SortDescriptor[] = [{ field: 'idesc', dir: 'asc' }];
  private urlSec: string = '/manttos/paises';
  private ds: string = 'dsPaises';
  private tt: string = 'ttPaises';
  loading = false;
  closeResult: string;
  toast = GLOBAL.toast;

  dataIdiomas: any[] = [];
  dataMercados: any[] = [];

  constructor(
    private _info: InfociaGlobalService,
    private _crudservice: CrudService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private modal: NgbModal) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.dsManuales = [
      { id: 'reservaciones', value: false, desc: 'Reservaciones' },
      { id: 'recepcion', value: false, desc: 'Recepción' },
    ];
    this.createForm();
    this._crudservice.getData('/manttos/idiomas').subscribe(data => {
      this.dataIdiomas = data.siIdioma.dsIdiomas.ttIdiomas;
    }
    );
    this._crudservice.getData('/manttos/mercados').subscribe(data => {
      this.dataMercados = data.siMercado.dsMercados.ttMercados;
    }
    );
  }

  ngOnInit() {
    this.getPais();

    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  getPais() {
    this.loading = true;
    this._crudservice.getData(this.urlSec)
      .map(data => data = data.siPais.dsPaises.ttPaises)
      .subscribe(data => {
        if (!data) {
          this.dataGrid = [];
          this.dataComplete = [];
        } else {
          this.dataGrid = data;
          this.dataComplete = data;
          this.dataGrid = {
            data: orderBy(this.dataGrid, this.sort),
            total: this.dataGrid.length
          };
        }
      });
    this.loading = false;
  }

  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.loadProducts();
  }

  private loadProducts(): void {
    this.dataGrid = process(this.dataComplete, { group: this.groups });
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }

  public switchChange(checked: boolean): void {
    const root = this.filter || { logic: 'and', filters: [] };
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataStructure();
  }

  private dataStructure(): void {
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

  createForm() {
    this.myFormGroup = new FormGroup({
      'Pais': new FormControl('', [Validators.required, Validators.maxLength(3)]),
      'pnombre': new FormControl('', [Validators.required, Validators.maxLength(30)]),
      'pidioma': new FormControl('', [Validators.required, Validators.maxLength(4)]),
      'psecuencia': new FormControl('', [Validators.required, Validators.maxLength(3)]),
      'Mercado': new FormControl('', [Validators.required]),
      'Inactivo': new FormControl(false),
      'cRowID': new FormControl(''),
      'ISOpais': new FormControl('', [Validators.maxLength(3)]),
      'ISOpais2': new FormControl('', [Validators.maxLength(2)]),
      'lError': new FormControl(false),
      'cErrDesc': new FormControl(''),
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  openModal(content) {
    this.modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'md',
      centered: true
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  public editRow({ dataItem }, content) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
    this.openModal(content);
  }

  public deleteRow({ dataItem }) {
    this.loading = true;
    this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
      const result = data.siPais.dsPaises.ttPaises;
      if (result[0].cErrDesc) {
        this.toast({
          type: 'error',
          title: GLOBAL.textError
        });
        this.loading = false;
      } else {
        this.toast({
          type: 'success',
          title: GLOBAL.textDeleteSuccess
        });
        this.flagShowForm = false;
        this.getPais();
        this.rebuildForm();
      }
    });
  }

  cerrarForm() {
    this.flagShowForm = false;
    this.rebuildForm();
  }

  printPDF() {
    this.toast({
      type: 'info',
      title: 'NO DISPONIBLE!',
      text: 'En mantenimiento'
    });
    // const columns = [
    //     {title: 'País', dataKey: 'Pais'},
    //     {title: 'Nombre', dataKey: 'pnombre'},
    //     {title: 'Idioma', dataKey: 'pidioma'},
    //     {title: 'Sec.', dataKey: 'psecuencia'},
    //     {title: 'Mercado', dataKey: 'Mercado'},
    //     {title: 'Inactivo', dataKey: 'Inactivo'}
    // ];
    // this._pdfService.printPDF('Paises', columns, this.dataGrid.data);
  }

  guardarForm() {
    (this.flagEdit) ? this.saveRow('put') : this.saveRow('post');
  }

  newRow(content) {
    this.flagShowForm = true;
    this.flagEdit = false;
    this.openModal(content);
  }

  saveRow(request) {
    const form = this.myFormGroup.value;
    this.loading = true;
    let method = null;
    if (request === 'post') {
      method = this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt);
    } else {
      method = this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt);
    }
    method.map(data => data = data.siPais.dsPaises.ttPaises)
      .subscribe(data => {
        if (data[0].cErrDesc) {
          this.toast({
            type: 'error',
            title: GLOBAL.textError,
            text: data[0].cErrDesc
          });
          this.loading = false;
        } else {
          this.toast({
            type: 'success',
            title: GLOBAL.textSuccess
          });
        }
        this.flagShowForm = false;
        this.getPais();
        this.rebuildForm();
      });
  }

  rebuildForm() {
    this.myFormGroup.reset({
      Idioma: '', idesc: '', iconm: '', Inactivo: false, nletra: '', lError: false, cRowID: ''
    });
  }
}
