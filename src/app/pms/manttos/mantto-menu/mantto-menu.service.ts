import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { GLOBAL } from '../../../services/global';
import { Menu } from '../model/mantto-menu.model';
import { BehaviorSubject } from 'rxjs';

const headers = new Headers({ 'Content-Type': 'application/json' });

@Injectable()
export class ManttoMenuService {
  private observableMenu = new BehaviorSubject<any>([]);
  cuerrentObservableMenu = this.observableMenu.asObservable();

  private url: string;
  private data;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  getSystems() {
    return this.http.get(this.url + '/admin/ayuda/sistema')
      .map(res => res.json().response.siSistMstr.dsSistMstr.ttSistMstr);
  }

  setMenuObs(data?: any) {
    this.observableMenu.next(data);
  }

  public getMenu(sistema) {
    const menu = this.http.get(this.url + '/admin/ayuda/menu/' + sistema + GLOBAL.char174 + GLOBAL.char174)
      .map(res => res.json().response.siMenuGral.dsMenuGral.ttMenuGral);
    return menu;
  }

  public getSubMenu() {
    return this.http.get(this.url + '/admin/ayuda/menu/PMS' + GLOBAL.char174 + 'Consultas' + GLOBAL.char174)
      .map(res => res.json().response.siMenuGral.dsMenuGral.ttMenuGral);
  }

  public postMenu(menu: Menu) {
    const json = JSON.stringify(menu);
    const params = '{"dsMenuGral":{"ttMenuGral":[' + json + ']}}';
    return this.http.post(this.url + '/manttos/menu', params, { headers: headers })
      .map((res: Response) => {
        const succes = res.json().response.siMenuGral.dsMenuGral.ttMenuGral;
        const error = res.json().response.siMenuGral.dsMensajes.ttMensError;
        if (error) {
          return error;
        } else {
          return succes;
        }
      });
  }

  public putMenu(menu: Menu) {
    const json = JSON.stringify(menu);
    const params = '{"dsMenuGral":{"ttMenuGral":[' + json + ']}}';

    return this.http.put(this.url + '/manttos/menu', params, { headers: headers })
      .map(res => {
        const succes = res.json().response.siMenuGral.dsMenuGral.ttMenuGral;
        const error = res.json().response.siMenuGral.dsMensajes.ttMensError;
        if (error) {
          return error;
        } else {
          return succes;
        }
      });
  }

  deteleMenu(menu: Menu) {
    const json = JSON.stringify(menu);
    const params = '{"dsMenuGral":{"ttMenuGral":[' + json + ']}}';
    const options = new RequestOptions({ headers: headers });
    return this.http.delete(this.url + '/manttos/menu', { body: params, headers })
      .map((res: Response) => {
        const succes = res.json().response.siMenuGral.dsMenuGral.ttMenuGral;
        const error = res.json().response.siMenuGral.dsMensajes.ttMensError;
        if (error) {
          return error;
        } else {
          return succes;
        }
      });
  }
  serviceMenu(data, routeAux, title) {
    title.icon = 'icon-gradient bg-happy-itmeo';
    if (data.length !== 0) {
      data.forEach(cMenu => {
        if (cMenu.SubMenu !== '') {
          cMenu.SubMenu.forEach(SubMenu => {
            if (SubMenu.Opcion && SubMenu.Opcion !== '') {
              SubMenu.Opcion.forEach(Opcion => {
                if (Opcion.cPrograma === routeAux[routeAux.length - 1]) {
                  title.icon += ' ' + cMenu.cIcon;
                  if (cMenu.cSistema === 'PMS') {
                    title.subheading = 'Property Management System.';
                  } else {
                    title.subheading = 'Point of Sales.';
                  }
                }
              });
            } else if (SubMenu.cPrograma === routeAux[routeAux.length - 1]) {
              title.icon += ' ' + cMenu.cIcon;
              if (cMenu.cSistema === 'PMS') {
                title.subheading = 'Property Management System.';
              } else {
                title.subheading = 'Point of Sales System.';
              }
            }
          });
        }
      });
      return title;
    }
  }
}
