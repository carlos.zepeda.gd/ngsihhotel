import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ManttoMenuService} from './mantto-menu.service';
import {Menu} from '../model/mantto-menu.model';
import {SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process} from '@progress/kendo-data-query';
import {icons} from './icons';
import swal from 'sweetalert2';
import {Router} from '@angular/router';
import { GLOBAL } from '../../../services/global';

const flatten = GLOBAL.flatten;

@Component({
    selector: 'app-mantto-menu',
    templateUrl: './mantto-menu.component.html',
    styleUrls: ['./mantto-menu.component.css']
})
export class ManttoMenuComponent implements OnInit {
    heading = 'Mantenimiento a Menú de Usuario';
    subheading = 'Property Management System.';
    icon = '';

    public icons: any[] = icons;
    public dataIcons: Array<{ text: string, value: string }>;
    public cIcon: { text: string, value: string };

    public update: boolean;
    private selectedRow: Menu;
    public menu: Menu;
    public dataGrid: any = [];
    public systems: Array<{ cSistema: string }> = [];
    public cSistema: string;
    public flagForm: boolean;
    private menuExpand: any;
    private subMenuExpand: any;
    public flagMenu: boolean;
    public flagSubmenu: boolean;
    public flagOpcion: boolean;
    public sort: SortDescriptor[] = [{field: 'iSec', dir: 'asc'}];
    public order: any = 'iSec';
    public dataComplete: any = [];
    public checked: boolean;
    public groups: GroupDescriptor[] = [];
    public filter: CompositeFilterDescriptor;

    constructor(private _menuServ: ManttoMenuService,
                private _router: Router) {
        this.dataIcons = this.icons.slice();
    }

    public ngOnInit() {
        const routeAux = this._router.url.split('/');
        this.startSystem(routeAux[1]);

        this._menuServ.getSystems().subscribe(data => this.systems = data);
        this.refreshIcon();
    }

    setcIcon(menu: string) {
        this.dataGrid.data.forEach(data => {
            if (menu === data.cMenu) {
                this.icons.forEach(dataIcon => {
                    if (dataIcon.value === data.cIcon) {
                        this.cIcon = dataIcon;
                    }
                });
            }
        });
    }

    getMenu() {
        this._menuServ.getMenu(this.cSistema).subscribe(data => {
            this.dataGrid = data;
            this.dataGrid = {
                data: orderBy(this.dataGrid, this.sort),
                total: this.dataGrid.length
            };
        });
    }

    startSystem(sys: string) {
        this.cSistema = sys;
        this.getMenu();
    }

    changeSystem($event) {
        this.cSistema = $event.cSistema;
        this.getMenu();
    }

    public newRecord() {
        this.cIcon = {text: '', value: ''};
        this.menu = new Menu('PMS', '', '', '', '', null, '');
        this.flagMenu = true;
        this.flagForm = true;
    }

    editRow({sender, rowIndex, dataItem}) {
        this.flagForm = true;
        this.flagMenu = true;
        this.update = true;
        this.selectedRow = dataItem;
        this.menu = new Menu(this.cSistema,
            this.selectedRow.cMenu,
            this.selectedRow.cSubMenu,
            this.selectedRow.cOpcion,
            this.selectedRow.cPrograma,
            this.selectedRow.iSec,
            this.selectedRow.cRowID);

    }

    deleteRow({dataItem}) {
        this._menuServ.deteleMenu(dataItem).subscribe(
            data => {
                if (data[0].cMensTxt) {
                    swal('', data[0].cMensTxt, 'error');
                } else {
                    this.getMenu();
                    this.flagForm = false;
                }
            }
        );
    }

    updateRecord(menu) {
        this._menuServ.putMenu(menu).subscribe(
            data => {
                if (data[0].cMensTxt) {
                    swal('', data[0].cMensTxt, 'error');
                } else {
                    this.getMenu();
                    this.resetForm();

                    this._menuServ.getMenu(this.cSistema).subscribe(dataV => {
                        this._menuServ.setMenuObs(dataV);
                        dataV.forEach(dataValue => {
                            if (dataValue.cMenu === menu.cMenu) {
                                this.refreshIcon();
                            }
                        });
                    });
                }
            }
        );
    }

    deleteRowSubMenu({dataItem}) {
        this.selectedRow = dataItem;
        this.menu = new Menu(this.menuExpand.cSistema, this.menuExpand.cMenu, this.selectedRow.cSubMenu, this.selectedRow.cOpcion,
            this.selectedRow.cPrograma, this.selectedRow.iSec, this.selectedRow.cRowID);
        this._menuServ.deteleMenu(this.menu).subscribe(
            data => {
                if (data[0].cMensTxt) {
                    swal('', data[0].cMensTxt, 'error');
                } else {
                    this.getMenu();
                    this.flagForm = false;
                }
            }
        );
    }

    deleteRowOpcion({dataItem}) {
        this.selectedRow = dataItem;
        this.menu = new Menu(this.menuExpand.cSistema, this.menuExpand.cMenu, this.subMenuExpand.cSubMenu, this.selectedRow.cOpcion,
            this.selectedRow.cPrograma, this.selectedRow.iSec, this.selectedRow.cRowID);
        this._menuServ.deteleMenu(this.menu).subscribe(
            data => {
                if (data[0].cMensTxt) {
                    swal('', data[0].cMensTxt, 'error');
                } else {
                    this.getMenu();
                    this.flagForm = false;
                }
            }
        );
    }

    public expandMenu({dataItem}) {
        this.menuExpand = dataItem;
    }

    public expandSubmenu({dataItem}) {
        this.subMenuExpand = dataItem;
    }

    public valueSubMenu({sender, rowIndex, dataItem}) {
        this.selectedRow = dataItem;
        this.menu = new Menu(this.menuExpand.cSistema, this.menuExpand.cMenu, this.selectedRow.cSubMenu, this.selectedRow.cOpcion,
            this.selectedRow.cPrograma, this.selectedRow.iSec, this.selectedRow.cRowID);
        this.update = true;
        this.flagForm = true;
    }

    public valueOpciones({sender, rowIndex, dataItem}) {
        this.flagOpcion = true;
        this.selectedRow = dataItem;
        this.menu = new Menu(this.menuExpand.cSistema, this.menuExpand.cMenu, this.subMenuExpand.cSubMenu, this.selectedRow.cOpcion,
            this.selectedRow.cPrograma, this.selectedRow.iSec, this.selectedRow.cRowID);
        this.update = true;
        this.flagForm = true;
    }

    public newRecordSubMenu(dataItem) {
        this.menu = dataItem;
        this.flagForm = true;
        this.flagOpcion = false;
    }

    newRecordOpcion(dataItem) {
        this.flagOpcion = true;
        this.flagForm = true;
        this.menu = new Menu(this.menuExpand.cSistema, this.menuExpand.cMenu, this.subMenuExpand.cSubMenu, '', '', 0, '');
    }

    saveRecord(form: NgForm) {
        const val = form.value;
        let cMenu = val.cMenu;
        if (!cMenu) {
            cMenu = '';
        }
        let iSec = val.iSec;
        if (!iSec) {
            iSec = '';
        }
        let cIcon = '';
        if (val.cIcon) {
            cIcon = val.cIcon.value;
            if (!cIcon) {
                cIcon = '';
            }
        }
        let cSubMenu = val.cSubMenu;
        if (!cSubMenu) {
            cSubMenu = '';
        }
        let cOpcion = val.cOpcion;
        if (!cOpcion) {
            cOpcion = '';
        }
        let cPrograma = val.cPrograma;
        if (!cPrograma) {
            cPrograma = '';
        }
        const cRowID = '';
        this.menu = new Menu(this.cSistema, cMenu, cSubMenu, cOpcion, cPrograma, iSec, cRowID, cIcon);

        if (this.update) {
            this.menu = new Menu(this.cSistema, cMenu, cSubMenu, cOpcion, cPrograma, iSec, this.selectedRow.cRowID, cIcon);
            this.updateRecord(this.menu);

            return;
        }
        this._menuServ.postMenu(this.menu).subscribe(data => {
                if (data[0].cMensTxt) {
                    swal('', data[0].cMensTxt, 'error');
                } else {
                    this.getMenu();
                    this.resetForm();
                }

                this._menuServ.getMenu(this.cSistema).subscribe(dataV => {
                    this._menuServ.setMenuObs(dataV);
                });
            }
        );
    }

    private resetForm() {
        this.flagForm = false;
        this.flagMenu = false;
        this.update = false;
    }

    // Funciones para la Grid de Kendo
    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.dataGrid = process(this.dataComplete, {group: this.groups});
    }

    public filterChange(filter: CompositeFilterDescriptor): void {
        this.filter = filter;
        this.dataGrid.data = filterBy(this.dataComplete, filter);
    }

    public switchChange(checked: boolean): void {
        const root = this.filter || {logic: 'and', filters: []};
        const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
        if (!filter) {
            root.filters.push({
                field: 'Inactivo',
                operator: 'eq',
                value: checked
            });
        } else {
            filter.value = checked;
        }
        this.checked = checked;
        this.filterChange(root);
    }

    public sortChange(sort: SortDescriptor[]): void {
        this.sort = sort;
        this.dataGrid = {
            data: orderBy(this.dataGrid.data, this.sort),
            total: this.dataGrid.data.length
        };
    }

    handleFilter(value) {
        this.dataIcons = this.icons.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
    }

    refreshIcon() {
        this.icon = 'icon-gradient bg-tempting-azure';
        const routeAux = this._router.url.split('/');
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            if (data.length !== 0) {
                data.forEach(cMenu => {
                    if (cMenu.SubMenu !== '' && cMenu.SubMenu !== '' && cMenu.SubMenu) {
                        cMenu.SubMenu.forEach(SubMenu => {
                            if (SubMenu.Opcion && SubMenu.Opcion !== '') {
                                SubMenu.Opcion.forEach(Opcion => {
                                    if (Opcion.cPrograma === routeAux[routeAux.length - 1]) {
                                        this.icon += ' ' + cMenu.cIcon;
                                        if (cMenu.cSistema === 'pms') {
                                            this.subheading = 'Property Management System.';
                                        } else {
                                            this.subheading = 'Point of Sales.';
                                        }
                                    }
                                });
                            } else if (SubMenu.cPrograma === routeAux[routeAux.length - 1]) {
                                this.icon += ' ' + cMenu.cIcon;
                                if (cMenu.cSistema === 'pms') {
                                    this.subheading = 'Property Management System.';
                                } else {
                                    this.subheading = 'Point of Sales System.';
                                }
                            }
                        });
                    }
                });
            }
        });
    }
}
