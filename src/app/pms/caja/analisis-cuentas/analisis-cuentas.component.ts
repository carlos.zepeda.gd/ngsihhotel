import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FilterService } from '@progress/kendo-angular-grid';
import { HttpClient } from '@angular/common/http';
import { CompositeFilterDescriptor, FilterDescriptor } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { CrudService } from '../../mantto-srv.services';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';

const flatten = GLOBAL.flatten;

@Component({
  selector: 'app-analisis-cuentas',
  templateUrl: './analisis-cuentas.component.html',
  styleUrls: ['./analisis-cuentas.component.css']
})

export class AnalisisCuentasComponent implements OnInit {
  heading = 'Análisis de Cuentas';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  api: Array<string> = ['/reportes/general/AnalisisCuenta', 'dsReporte', 'ttTgeneral'];
  kendoFormat = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  noRecords = GLOBAL.noRecords;
  pageSize = 10;
  skip = 0;
  loading: boolean;
  dataGrid: any = [];
  ngForm: FormGroup;
  ttInfocia: any = [];
  ttUsuario: any = [];
  categoryFilter: any[] = [];
  swal = GLOBAL.swal;
  dsTipos = [
    { tipo: 'F', desc: 'FACTURA' },
    { tipo: 'C', desc: 'CANCELA FACTURA' },
    { tipo: 'S', desc: 'CHECK OUT' },
    { tipo: 'R', desc: 'REGRESO' }
  ]

  constructor(private _menuServ: ManttoMenuService,
    private _info: InfociaGlobalService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _http: HttpClient,
    private _crudservice: CrudService) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.ngForm = this.createFormGroup();
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  getDataSource(form) {
    this.dataGrid = [];
    this.loading = true;
    form.fFec1 = moment(form.fFec1).format(GLOBAL.formatIso);
    form.fFec2 = moment(form.fFec2).format(GLOBAL.formatIso);
    this._crudservice.putInfo(form, this.api[0], this.api[1], this.api[2]).subscribe(res => {
      const result = res.siReporte.dsReporte.ttCcuenta;
      if (result) {
        this.dataGrid = result;
      }
      this.loading = false;
    });
  }

  public categoryChange(values: any[], filterService: FilterService): void {
    filterService.filter({
      filters: values.map(value => ({ field: 'Tipo', operator: 'eq', value })),
      logic: 'or'
    });
  }

  public categoryFilters(filter: CompositeFilterDescriptor): FilterDescriptor[] {
    this.categoryFilter.splice(0, this.categoryFilter.length, ...flatten(filter).map(({ value }) => value));
    return this.categoryFilter;
  }

  createFormGroup() {
    return this._formBuild.group({
      cChr1: [''],
      cChr2: [''],
      cChr3: [''],
      cChr4: [''],
      cChr5: [''],
      cChr6: [''],
      cChr7: [''],
      cChr8: [''],
      cChr9: [''],
      cChr10: [''],
      cChr11: [''],
      iInt1: [1],
      iInt2: [3],
      iInt3: [1],
      iInt4: [0],
      iInt5: [0],
      iInt6: [0],
      iInt7: [0],
      iInt8: [0],
      iInt9: [0],
      iInt10: [0],
      iInt11: [0],
      lLog1: [false],
      lLog2: [false],
      lLog3: [false],
      lLog4: [false],
      lLog5: [false],
      lLog6: [false],
      lLog7: [false],
      lLog8: [false],
      lLog9: [false],
      lLog10: [false],
      lLog11: [false],
      dDec1: [0.0],
      dDec2: [0.0],
      dDec3: [0.0],
      dDec4: [0.0],
      dDec5: [0.0],
      dDec6: [0.0],
      dDec7: [0.0],
      dDec8: [0.0],
      dDec9: [0.0],
      dDec10: [0.0],
      dDec11: [0.0],
      fFec1: [moment(this.ttInfocia.fday).toDate()],
      fFec2: [moment(this.ttInfocia.fday).toDate()],
      fFec3: [null],
      fFec4: [null],
      fFec5: [null],
      fFec6: [null],
      fFec7: [null],
      fFec8: [null],
      fFec9: [null],
      fFec10: [null],
      fFec11: [null],
      zDtz1: [null],
      zDtz2: [null],
      zDtz3: [null],
      zDtz4: [null],
      zDtz5: [null],
      zDtz6: [null],
      zDtz7: [null],
      zDtz8: [null],
      zDtz9: [null],
      zDtz10: [null],
      zDtz11: [null]
    });
  }

  exportPdf() {
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(this.formatMoment);
    form.fFec2 = moment(form.fFec2).format(this.formatMoment);
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: '002-ANALISIS-CUENTAS',
      dataGrid: this.dataGrid,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      hora: moment().format('HH:mm:ss'),
      form: form,
    }
    this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = this.heading + '.pdf';
        download.click();
      },
      error => {
        this.swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
      }
    );
  }
}
