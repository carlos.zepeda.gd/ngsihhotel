import { RouterModule, Routes } from '@angular/router';
import { BaseLayoutComponent } from '../../_architect/Layout/base-layout/base-layout.component';
import { VerifyInfociaGuardService } from '../../services/verify-infocia-guard.service';
import { HistoricoClientesComponent } from '../consultas/historico-clientes/historico-clientes.component';
import { AnalisisCuentasComponent } from './analisis-cuentas/analisis-cuentas.component';
import { EntradaMovimientosComponent } from './entrada-movimientos/entrada-movimientos.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { CajaRegsalidaComponent } from './caja-regsalida/caja-regsalida.component';
import { CodigosCajaComponent } from './codigos-caja/codigos-caja.component';
import { AuditoriaDivisasComponent } from './auditoria-divisas/auditoria-divisas.component';
import { DivisasComponent } from './divisas/divisas.component';
import { RoutesGuard } from '../../services/routes-guard.service';

const CAJA_ROUTES: Routes = [
    {
        path: '',
        component: BaseLayoutComponent,
        children: [
            { path: 'historico-clientes', component: HistoricoClientesComponent, canActivate: [RoutesGuard], data: { uso: 'menu', extraParameter: '' } },
            { path: 'analisis-cuentas', component: AnalisisCuentasComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'movimientos', component: EntradaMovimientosComponent, canActivate: [RoutesGuard], data: { menu: 'caja', uso: 'H', extraParameter: '' } },
            { path: 'check-out', component: CheckOutComponent, canActivate: [RoutesGuard], data: { extraParameter: '', consulta: false, uso: 'H' } },
            { path: 'consulta-cuentas', component: CheckOutComponent, canActivate: [RoutesGuard], data: { extraParameter: '', consulta: true, uso: 'H' } },
            { path: 'regreso-salida', component: CajaRegsalidaComponent, canActivate: [RoutesGuard], data: { extraParameter: '', uso: 'H' } },
            { path: 'parametro-codigos', component: CodigosCajaComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'auditoria-divisas', component: AuditoriaDivisasComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'divisas', component: DivisasComponent, canActivate: [RoutesGuard], data: { extraParameter: '', uso: 'H' } },
        ],
        canActivateChild: [
            VerifyInfociaGuardService
        ]
    }
];
export const CAJA_ROUTING = RouterModule.forChild(CAJA_ROUTES);

