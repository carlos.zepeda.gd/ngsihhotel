import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ValidationsService, BuscarService } from '../../recepcion/check-in/services/service-checkin.index';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { SharedService } from '../../../services/shared.service';
import { ComboBoxComponent } from '@progress/kendo-angular-dropdowns';
import { CrudService } from '../../mantto-srv.services';
import { GLOBAL } from '../../../services/global';
import swal from 'sweetalert2';
import moment from 'moment';
declare var $: any;
const textError = GLOBAL.textError;

@Component({
    selector: 'app-entrada-movimientos',
    templateUrl: './entrada-movimientos.component.html',
    styleUrls: ['./entrada-movimientos.component.css'],
    providers: [NgbModalConfig, NgbModal]
})

export class EntradaMovimientosComponent implements OnInit, OnChanges, OnDestroy {
    heading = 'Movimientos a Huéspedes';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';
    formatKendo = localStorage.getItem('formatKendo');
    formatMoment = localStorage.getItem('formatMoment');
    @ViewChild('recibo') content: ElementRef;
    @ViewChild('idCuarto') idCuarto: ElementRef;
    @ViewChild('idCodigo') idCodigo: ComboBoxComponent;
    @ViewChild('modalAuth') modalAuth: ElementRef;
    @ViewChild('modalHuesped') modalHuesped: ElementRef;
    @ViewChild('modalPack') modalPack: ElementRef;
    @Input() folio: any = [];
    @Input() entrada: boolean;
    @Input() cuenta: any = [];
    @Input() movimientos: any = [];
    @Input() pagar: boolean;
    @Output() result = new EventEmitter;
    @Input() ajuste: string;
    @Input() movtoAjuste: any = [];
    public myFormGroup: FormGroup;
    public ttInfocia: any = [];
    public ttUsuario: any = [];
    public dsMonedas: any = [];
    public dsCodigos: any = [];
    public dsVouchers: Array<string> = [];
    public dsTipoCambio: any = [];
    public dsParamCode: any = [];
    public manttoCaja: any[];
    public folioSelected: any = [];
    public codigoSelected: any;
    public movimientoSelected: any = [];
    public dataCodigos: any = [];
    public dataFolios: any = [];
    private api: any = ['/caja/movimientos', 'siHuesmov', 'dsHuesmov', 'ttHuesmov'];
    private urlTarifa: string = '/rec/huesped/';
    private urlAjuste: string = '/caja/checkout/';
    private urlParam: string = '/ayuda/parametros®codigos-'
    public flagCargoInv: boolean;
    public flagCancelarMovto: boolean;
    public flagAjuste: boolean;
    public flagIsPack: boolean;
    public flagSplit: boolean;
    private flagAutorizado: boolean;
    public total: number = 0;
    public montoAjuste: number = 0;
    public totalAjuste: number = 0;
    private tarifaDia: any;
    private minMontoAjuste: number;
    public nombreHuesped: string;
    public totalPorciones = Array(4).fill(0).map((x, i) => i);
    public porciones: number = 2;
    public validatorZero: boolean;
    public desglosePorciones: boolean;
    public checkout: boolean;
    public tipoSplit: string = 'Porcentaje';
    public arrayPorciones: any = [];
    public codigoTrans: string;
    public totalFormatText: string;
    public formatNumber: string;
    public swall = GLOBAL.swal;

    constructor(private _crudservice: CrudService,
        private _active: ActivatedRoute,
        private _config: NgbModalConfig,
        private _modalService: NgbModal,
        private _fb: FormBuilder,
        private _info: InfociaGlobalService,
        private _shared: SharedService,
        private _valid: ValidationsService,
        private _buscar: BuscarService,
        private _menuServ: ManttoMenuService,
        private _router: Router) {
        this.getCodigos(1);
        this._shared.getMonedas().subscribe(data => {
            this.dsMonedas = data;
            data.forEach(moneda => moneda.mdesc = moneda.mdesc + ' (' + moneda.Moneda + ')');
        });
        this._config.backdrop = 'static';
        this._config.keyboard = false;
        this._active.data.subscribe(data => data.menu === 'caja' ? this.buildFormGroup() : this.checkout = true);
        this.ttInfocia = this._info.getSessionInfocia();
        this.ttInfocia.fday = moment(this.ttInfocia.fday, GLOBAL.formatIso).format(this.formatMoment);
        this.ttUsuario = this._info.getSessionUser();
        this.manttoCaja = this._info.manttoCaja(this.ttUsuario);
    }

    ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    ngOnDestroy(): void {
        this.codigoTrans = '';
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['folio'] && changes['folio'].currentValue) {
            this.nombreHuesped = this.folio.hapellido + ' ' + this.folio.hnombre;
            this.buildFormGroup();
            if (changes['entrada'] && changes['entrada'].currentValue) {
                this.buildFormGroup();
                this.total = 0;
            }
            if (changes['pagar'] && changes['pagar'].currentValue) {
                if (this.cuenta.dbalance) {
                    $('.k-numeric-wrap>.k-input').css('color', 'red');
                }
                if (this.cuenta.dbalance < 0) {
                    this.cuenta.dbalance = this.cuenta.dbalance * -1;
                }
                this.myFormGroup.patchValue({
                    hfecha: moment(this.movimientos.tfecha).toDate(), hsubtotal: this.cuenta.dbalance,
                    cuenta: this.cuenta.cCuenta, codigo: this.folio.hfp, hmon: this.ttInfocia.Moneda
                });
                this.getTipoCambio(this.ttInfocia.Moneda);
                // this.pagar = false;
            }
            setTimeout(() => this.idCodigo.focus(), 300);

            if (changes['ajuste'] && changes['ajuste'].currentValue === 'J') {
                this.flagAjuste = true;
            }
            if (this.ajuste === 'J') {
                if (changes.movtoAjuste !== undefined) {
                    this.myFormGroup.patchValue({
                        htc: this.movtoAjuste.ttcamb, codigo: this.movtoAjuste.tcod,
                        hvoucher: this.movtoAjuste.tvouc, hmon: this.movtoAjuste.Moneda,
                        hsubtotal: this.movtoAjuste.tcargo
                    });
                    this.montoAjuste = this.movtoAjuste.tcargo;
                }
            }
            if (this.ajuste === 'S') {
                this.getCodigos('');
                this.splitMovto();
                this.myFormGroup.patchValue({
                    htc: this.movtoAjuste.ttcamb, codigo: this.movtoAjuste.tcod,
                    hvoucher: this.movtoAjuste.tvouc, hmon: this.movtoAjuste.Moneda
                });
                this.totalAjuste = this.movtoAjuste.tabono + this.movtoAjuste.tcargo;
                this.flagSplit = true;
            }
        }
    }

    getCodigos(option) {
        this._shared.getCodigos('', option, 'y').subscribe(data => {
            this.dsCodigos = data;
            data.forEach(codigo => codigo.cdesc = codigo.cdesc + ' (' + codigo.Codigo + ')');
            this.dataCodigos = data;
            if (this.folio) {
                this.consultarCodigo(this.folio.hfp);
            }
        });
    }
    private buildFormGroup() {
        let cuenta = '';
        if (Object.keys(this.cuenta).length) {
            cuenta = this.cuenta.cCuenta;
        }
        this.myFormGroup = this._fb.group({
            hfolio: [this.folio.hfolio, Validators.required], // number
            htfolio: [cuenta], // string
            hfecha: [this.ttInfocia.fday], // date
            hhora: [''], // string
            codigo: ['', Validators.required],
            hvoucher: ['', [Validators.required, Validators.maxLength(7)]],
            hsubtotal: [0, [Validators.min(this.minMontoAjuste), Validators.required]],
            hprop: [0],
            hmon: [this.ttInfocia.Moneda, Validators.required], // string
            htc: [this.dsTipoCambio.dTc, Validators.required],
            htransfer: [0],
            hajuste: [0],
            detalle: [''],
            paquete: [0],
            hnotas: ['', Validators.maxLength(100)],
            hope: [''],
            htm: [''],
            ntran: [0],
            Con: [0],
            iagrp: [''],
            dfp: [''],
            hadultos: [0], hjunior: [0], hmenores: [0], hmenpag: [0],
            SubOrigen: [0],
            PropOrigen: [0],
            TransNo: [0],
            lCargoInv: [false],
            cTarifa: [''],
            cClave: [''],
            dSaldo: [0],
            cFcon: [''],
            iInte1: [1], iInte2: [0], iInte3: [0], iInte4: [0], iInte5: [0],
            cChar1: ['C'], cChar2: [''], cChar3: [''], cChar4: [''], cChar5: [''],
            cRowID: [''], cErrDesc: [''], lError: [false]
        });
        this.onChangesForm();
    }

    // Usuario cancela movimientos??? cClave
    onChangesForm() {
        this.myFormGroup.valueChanges.subscribe(change => {
            if (this.myFormGroup.value.hsubtotal) {
                this.total = Number(change.hsubtotal) + Number(change.hprop);
            }
        });
    }

    splitMovto() {
        this.flagAjuste = false;
        this.flagSplit = true;
        this.desglosarPorciones(2);
    }

    desglosarPorciones(porciones) {
        this.porciones = porciones;
        this.arrayPorciones = [];
        let total = 0;
        let acumulador = 0;
        let totalProp = 0;
        this.validatorZero = false;
        this.desglosePorciones = true;
        if (this.tipoSplit === 'Monto') {
            this.movtoAjuste.tcargo ? total = this.movtoAjuste.tcargo : total = this.movtoAjuste.tabono;
            this.movtoAjuste.tcargo ? totalProp = this.movtoAjuste.tcargo : totalProp = this.movtoAjuste.tabono;
            for (let i = 0; i < porciones - 1; i++) {
                if (total / porciones === 0) {
                    this.validatorZero = true;
                }
                this.arrayPorciones.push((total / porciones).toFixed(2));
            }
            this.arrayPorciones.forEach(item => acumulador += parseFloat(item));
            this.arrayPorciones.push(total - acumulador);
        } else {
            total = 100;
            for (let i = 0; i < porciones - 1; i++) {
                if (Math.floor(total / porciones) === 0) {
                    this.validatorZero = true;
                }
                this.arrayPorciones.push(Math.floor(total / porciones));
                acumulador += Math.floor(total / porciones);
            }
            this.arrayPorciones.push(Math.ceil(total - acumulador));
        }
    }

    generarSplit() {
        // Solo actualiza el valor de split 1
        const arreglo = [];
        let tipo = 'p';
        if (this.tipoSplit === 'Monto') {
            tipo = 'm';
        }
        for (let i = 0; i < this.arrayPorciones.length; i++) {
            if (this.movtoAjuste.tcargo) {
                this.myFormGroup.patchValue({
                    hsubtotal: this.arrayPorciones[i], hprop: 0, cChar1: tipo
                });
                arreglo.push(this.myFormGroup.value);
            } else {
                this.myFormGroup.patchValue({
                    hsubtotal: this.arrayPorciones[i], hprop: 0, cChar1: tipo
                });
                arreglo.push(this.myFormGroup.value);
            }
        }
        this.guardarAjuste(arreglo);
    }

    continuarAjuste() {
        this.minMontoAjuste = 0;
        if (this.movtoAjuste.ttHuespaq) {
            this.movtoAjuste.ttHuespaq.forEach(item => {
                if (item.hsel) {
                    this.minMontoAjuste += Math.abs(item.hmonto) * -1;
                }
            });
        }
        if (this.minMontoAjuste > this.myFormGroup.value.hsubtotal) {
            this.swall({ type: 'warning', title: 'Revise montos!!!' });
            this.flagIsPack = false;
        } else {
            this.flagIsPack = false;
        }
    }

    closePaquetes(paquete, close) {
        let count = 0;
        paquete.forEach(item => {
            if (item.hsel) { count++; }
        });
        count ? close() : swal('No hay códigos seleccionados', '', 'info');
    }

    cambioSubtotal() {
        // Si es de paquete debe mostrar un dialogo con los codigos que se ajustaran
        if (this.movtoAjuste.ttHuespaq) {
            this.flagIsPack = true;
            this.openModal(this.modalPack, 'md');
        }
        this.montoAjuste = this.myFormGroup.value.hsubtotal;
        this.totalAjuste = this.movtoAjuste.tcargo + this.myFormGroup.value.hsubtotal + this.myFormGroup.value.hprop;
        this.minMontoAjuste = Math.abs(this.movtoAjuste.tcargo) * -1;
        if (this.montoAjuste < this.minMontoAjuste) {
            this.montoAjuste = this.minMontoAjuste;
            this.myFormGroup.patchValue({ hsubtotal: this.montoAjuste });
            this.totalAjuste = this.movtoAjuste.tcargo + this.myFormGroup.value.hsubtotal + this.myFormGroup.value.hprop;
        }
    }

    onChangePorciones(value, index) {
        this.arrayPorciones[index] = value;
    }

    guardarForm() {
        this.myFormGroup.patchValue({ lCargoInv: this.flagCargoInv });
        const data = [...[this.myFormGroup.value]][0];
        this.flagAjuste || this.flagSplit ? this.guardarAjuste(data) : this.guardarMovto(data)
    }

    openModal(modal, size) {
        this._modalService.open(modal, { size: size, centered: true });
    }

    guardarMovto(data) {
        data.hfecha = moment(data.hfecha, this.formatMoment).format(GLOBAL.formatIso);
        this._crudservice.putInfo(data, this.api[0], this.api[2], this.api[3]).subscribe(res => {
            const result = res[this.api[1]][this.api[2]][this.api[3]];
            if (result) {
                if (result[0].cErrDes) {
                    swal(textError, result[0].cErrDes, 'error');
                } else {
                    if (this.codigoSelected.coimpuesto) {
                        this._info.getConvertirMontoTexto(this.total, this.myFormGroup.value.hmon, '').subscribe(item => {
                            if (item) {
                                this.totalFormatText = item.pcCod;
                                this.impRecibo(data);
                            }
                        });
                    } else {

                        this.codigoTrans = result[0].ntran;
                        this.swall({ type: 'success', title: 'Transacción Realizada!!!', text: '# Transacción: ' + this.codigoTrans })
                            .then(() => {
                                this.result.emit('close');
                                this.buildFormGroup();
                                this.myFormGroup.patchValue({ codigo: this.codigoSelected.Codigo });
                                this.idCuarto.nativeElement.select();
                            });
                    }
                }
            } else {
                swal(textError, 'Intentelo nuevamente', 'error');
                this.result.emit('close');
                this.buildFormGroup();
                this.flagCancelarMovto = false;
            }
        });
    }

    guardarAjuste(data) {
        const temp1 = JSON.parse(JSON.stringify(data));
        const temp2 = JSON.parse(JSON.stringify(this.movtoAjuste));
        if (temp1.length) {
            temp1.forEach(item => {
                item.hfecha = moment(item.hfecha, this.formatMoment).format(GLOBAL.formatIso);
            });
        } else {
            temp1.hfecha = moment(temp1.hfecha, this.formatMoment).format(GLOBAL.formatIso);
        }
        temp2.tfecha = moment(temp2.tfecha, this.formatMoment).format(GLOBAL.formatIso);
        let json = {};
        let leter = 'S';
        if (this.flagSplit) {
            if (this.codigoSelected.cdc === 'D' && this.manttoCaja[4] || this.flagAutorizado) {
                json = '{"dsCheckout":{"ttHuesmov": ' + JSON.stringify(temp1) + ', "ttTmpco": [' + JSON.stringify(temp2) + ']}}';
                leter = 'S';
            } else {
                this.openModal(this.modalAuth, 'md');
                return;
            }
            if (this.codigoSelected.cdc === 'C' && this.manttoCaja[4] || this.flagAutorizado) {
                json = '{"dsCheckout":{"ttHuesmov": ' + JSON.stringify(temp1) + ', "ttTmpco": [' + JSON.stringify(temp2) + ']}}';
                leter = 'S';
            } else {
                this.openModal(this.modalAuth, 'md');
                return;
            }
        } else {
            if (this.manttoCaja[2] || this.flagAutorizado) {
                json = '{"dsCheckout":{"ttHuesmov": [' + JSON.stringify(temp1) + '], "ttTmpco": [' + JSON.stringify(temp2) + ']}}';
                leter = 'J';
            } else {
                this.openModal(this.modalAuth, 'md');
                return;
            }
        }
        this._crudservice.putInfo2(json, this.urlAjuste + leter).subscribe(result => {
            const res = result.siCheckout.dsMensajes.ttMensError;
            res ? swal(res[0].cMensTit, res[0].cMensTxt, 'error') : this.result.emit('close');
            this.flagAutorizado = false;
        });
    }
    consultarCodigo(codigo): Promise<boolean> {
        return new Promise((resolve) => {
            this.dsCodigos.forEach(cod => {
                if (cod.Codigo === codigo) {
                    this.codigoSelected = cod;
                }
            });
            resolve(true);
        });
    }
    // Cargo en rojo letra D // Forma de pago en negro con C
    async onChangeCodigo(codigo) {
        this.myFormGroup.patchValue({ hvoucher: '', dfp: '' });
        await this.consultarCodigo(codigo);
        if (this.codigoSelected) {
            if (!this.pagar) {
                this._crudservice.getData(this.urlParam + codigo).subscribe(data => {
                    if (Object.keys(data.siAyuda.dsAyuda).length) {
                        this.dsParamCode = data.siAyuda.dsAyuda.ttParametros[0];
                        this.myFormGroup.patchValue({
                            hsubtotal: this.dsParamCode.pchr1, hmon: this.dsParamCode.pchr2
                        });
                        this.total = this.dsParamCode.pchr1;
                    } else {
                        this.myFormGroup.patchValue({ hsubtotal: 0, hmon: this.ttInfocia.Moneda, htc: 1 });
                        this.total = 0;
                    }
                });
            }
            if (this.codigoSelected.ctr === 'H') {
                setTimeout(() => this.rentaAutomatica(), 500);
            }
            if (this.codigoSelected.ctr === 'D') {
                this.getTarifaDia();
                setTimeout(() => {
                    if (!this.tarifaDia.detalle) {
                        swal('Código no valido!!!', '', 'warning');
                        this.myFormGroup.value.codigo = [];
                    }
                }, 500);
            }
            this.getTipoCambio(this.myFormGroup.value.hmon);
            this.movimientos.forEach(item => {
                if (item.tcod === codigo) {
                    this.dsVouchers.push(item.tvouc);
                }
            });
        }
    }

    changeVoucher() {
        if (this.movimientos && this.movimientos.length) {
            this.movimientos.forEach(mov => {
                if (this.codigoSelected.Codigo === mov.tcod && mov.tvouc === this.myFormGroup.value.hvoucher) {
                    if (mov.tfecha === this.ttInfocia.fday) {
                        this.flagCancelarMovto = true;
                        setTimeout(() => this.cancelarMovimiento(mov), 500);
                    }
                }
            });
        }
    }

    rentaAutomatica() {
        swal({
            title: '¿Renta Automatica?', text: '', type: 'warning',
            showCancelButton: true, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33',
            confirmButtonText: 'Si', cancelButtonText: 'No',
            confirmButtonClass: 'btn btn-success py-3 px-4', cancelButtonClass: 'btn btn-light ml-1 py-3 px-4', buttonsStyling: false,
        }).then((result) => {
            if (result.value) {
                this.getTarifaDia();
            }
        });
    }

    noCancelarMovto() {
        this.flagCancelarMovto = false;
        this.myFormGroup.patchValue({
            hvoucher: '', hfecha: '', hsubtotal: 0,
            hnotas: '', codigo: this.codigoSelected.Codigo
        });
    }

    userAuth(user) {
        this._valid.getAuthorization(user.user, user.pass, 1, 0, 0).subscribe(async data => {
            const cval = data.pcCod.split('');
            if (cval[0] === 'S' && cval[1] === 'S') {
                if (this.flagCancelarMovto) {
                    this.autorizaCancelarMovto(cval, user);
                }
                if (this.flagAjuste) {
                    this.autorizaAjusteMovto(user, cval);
                }
                if (this.flagSplit) {
                    this.autorizaSplitMovto(user, cval);
                }
            } else {
                if (cval[0] !== 'S') {
                    swal('Usuario no existe!!!', '', 'warning');
                }
                if (cval[1] !== 'S') {
                    swal('Clave incorrecta!!!', '', 'warning');
                }
            }
        });
    }

    autorizaAjusteMovto(user, cval) {
        if (cval[4] === 'S') {
            this.flagAutorizado = true;
            this.myFormGroup.patchValue({ cClave: user.user });
            this.guardarForm();
        } else {
            swal('No Autorizado!!!', '', 'warning');
        }
    }

    autorizaSplitMovto(user, cval) {
        // PUEDE AUTORIZAR DEBITOS
        this._shared.getCodigos('', 1, 'y', this.myFormGroup.value.codigo).subscribe(data => {
            if (data) {
                this.codigoSelected = data[0];
                if (this.codigoSelected.cdc === 'D') {
                    if (cval[6] === 'S') {
                        this.flagAutorizado = true;
                        this.myFormGroup.patchValue({ cClave: user.user });
                        this.generarSplit();
                    } else {
                        swal('No Autorizado!!!', '', 'warning');
                    }
                }
                // PUEDE AUTORIZAR CREDITOS?
                if (this.codigoSelected.cdc === 'C') {
                    if (cval[8] === 'S') {
                        this.flagAutorizado = true;
                        this.myFormGroup.patchValue({ cClave: user.user });
                        this.generarSplit();
                    } else {
                        swal('No Autorizado!!!', '', 'warning');
                    }
                }
            }
        });
    }

    async autorizaCancelarMovto(cval, user?) {
        if (cval[2] === 'S') {
            const { value: text } = await swal({
                input: 'textarea', inputPlaceholder: 'Razón de la Cancelación:', showCancelButton: true
            })
            if (text) {
                this.myFormGroup.patchValue({ cClave: user.user, hnotas: text });
                this.guardarForm();
            } else {
                this.noCancelarMovto();
            }
        } else {
            swal('No Autorizado!!!', '', 'warning');
        }
    }

    cancelarMovimiento(mov) {
        this.movimientoSelected = mov;
        swal({
            title: '¿Deseas cancelar el movimiento?',
            text: '', type: 'warning',
            showCancelButton: true, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33',
            confirmButtonText: 'Si', cancelButtonText: 'No',
            confirmButtonClass: 'btn btn-success py-3 px-4', cancelButtonClass: 'btn btn-light ml-1 py-3 px-4', buttonsStyling: false,
        }).then((result) => {
            if (result.value) {
                let subtotal = 0;
                mov.tcargo ? subtotal = mov.tcargo : subtotal = mov.tabono;
                this.myFormGroup.patchValue({
                    hvoucher: mov.tvouc, hfecha: mov.tfecha, hsubtotal: subtotal,
                    hnotas: mov.tnotas, cuenta: mov.ttfolio
                });
                // Puede cancelar movimientos?
                if (!this.manttoCaja[0]) {
                    this.openModal(this.modalAuth, 'md');
                }
            } else {
                this.noCancelarMovto();
            }
        });
    }

    getTarifaDia() {
        const url = this.urlTarifa + this.folio.hfolio + '/T';
        this._crudservice.getData(url).subscribe(data => {
            const tarifas = data.sihuesped.dsHuespedes.ttHuestar;
            if (tarifas) {
                tarifas.forEach(tarifa => {
                    tarifa.hfecha = moment(tarifa.hfecha, GLOBAL.formatIso).format(this.formatMoment);
                    if (tarifa.hfecha === this.ttInfocia.fday) {
                        this.tarifaDia = tarifa;
                        this.myFormGroup.patchValue({
                            hsubtotal: this.tarifaDia.hta1
                        });
                    }
                });
            }
        });
    }

    valueChangeCuarto(value) {
        this._buscar.getHuesped(value, 'cuarto').subscribe(cuarto => {
            if (cuarto) {
                this.resultData(cuarto);
            } else {
                this._buscar.getHuesped(value, 'folio').subscribe(folio => {
                    folio ? this.resultData(folio) : swal('Cuarto Invalido!!', '', 'info').then(() => this.noCancelarMovto());
                });
            }
        });
    }

    resultData(data) {
        if (data.length === 1) {
            this.resultHuesped(data[0]);
        } else {
            this.openModal(this.modalHuesped, 'xl');
            this.dataFolios = data;
        }
    }

    resultHuesped(folio) {
        this.folio = folio;
        this.buildFormGroup();
        this.myFormGroup.patchValue({ hfolio: folio.hfolio });
        this.nombreHuesped = folio.hapellido + ' ' + folio.hnombre;
        setTimeout(() => this.idCodigo.focus(), 300);

    }
    getTipoCambio(value) {
        this._info.getTipoCambioDia(value).subscribe(data => {
            if (data) {
                this.dsTipoCambio = data[0];
                this.myFormGroup.patchValue({ htc: this.dsTipoCambio.dTc });
            }
        });
    }

    handleFilter(value) {
        if (value.length < 3) {
            this.dsCodigos = this.dataCodigos.filter((s) => s.cdesc.toLowerCase().indexOf(value.toLowerCase()) !== -1);
        } else {
            this.dsCodigos = this.dataCodigos.filter((s) => s.Codigo.toLowerCase().indexOf(value.toLowerCase()) !== -1);
            if (!this.dsCodigos.length) {
                this.dsCodigos = this.dataCodigos.filter((s) => s.cdesc.toLowerCase().indexOf(value.toLowerCase()) !== -1);
            }
        }
    }

    public impRecibo(e) {
        const mywindow = window.open('', 'spos_print', 'height=500,width=700');
        mywindow.document.write('<html><head><title>Configuración de Impresión</title>');
        mywindow.document.write('</head><body>');
        mywindow.document.write($('#recibo').html());
        mywindow.document.write('</body></html>');
        mywindow.document.close();
        mywindow.print();
        mywindow.onafterprint = () => {
            this.result.emit('close');
            mywindow.close();
        }
    }
}
