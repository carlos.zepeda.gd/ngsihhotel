// Imports Angular
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// Modules
import {KendouiModule} from '../../shared/kendo.module';
import {SharedAppsModule} from '../../shared/shared-apps.module';
import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';
// Consultas
import {ArchitectModule} from '../../_architect/architect.module';
import {RouterModule} from '@angular/router';
import { CheckOutModule } from './check-out/checkout.module';
import { AnalisisCuentasComponent } from './analisis-cuentas/analisis-cuentas.component';
import { CajaRegsalidaComponent } from './caja-regsalida/caja-regsalida.component';
import { CodigosCajaComponent } from './codigos-caja/codigos-caja.component';
import { CAJA_ROUTING } from './caja.routes';
import { CajaService } from './caja.service';
import { AuditoriaDivisasComponent } from './auditoria-divisas/auditoria-divisas.component';
import { DivisasComponent } from './divisas/divisas.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CAJA_ROUTING,
    KendouiModule,
    SharedAppsModule,
    RouterModule,
    ArchitectModule,
    CheckOutModule,
    GridModule,
    ExcelModule,
  ],
  declarations: [
    AnalisisCuentasComponent,
    CajaRegsalidaComponent,
    CodigosCajaComponent,
    AuditoriaDivisasComponent,
    DivisasComponent,
  ],
  providers: [
    CajaService,
  ],
  exports: [
  ]
})
export class CajaModule {
}
