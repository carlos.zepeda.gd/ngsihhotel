import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild, TemplateRef } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { SelectableSettings, RowArgs } from '@progress/kendo-angular-grid';
import { ValidationsService } from '../../recepcion/check-in/services/service-checkin.index';
import { BuscarService } from '../../recepcion/check-in/services/childs/buscar.service';
import { CheckinService } from '../../recepcion/check-in/services/checkin.service';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { SharedService } from '../../../services/shared.service';
import { CrudService } from '../../mantto-srv.services';
import { GLOBAL } from '../../../services/global';
import swal from 'sweetalert2';
import moment from 'moment';
const esp = GLOBAL.char174;

@Component({
    selector: 'app-check-out',
    templateUrl: './check-out.component.html',
    styleUrls: ['./check-out.component.css'],
    providers: [NgbModalConfig, NgbModal, DecimalPipe]
})
export class CheckOutComponent implements OnInit, OnChanges {
    @Input() inFolio: any;
    @ViewChild('modalDetMov') modalDetMov: TemplateRef<any>;
    @ViewChild('modalHelp') modalHelp: TemplateRef<any>;
    heading = 'Check-Out';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';
    formatMoment = localStorage.getItem('formatMoment');
    public faBell = '';
    public norecords = GLOBAL.noRecords;
    private urlSec: string = '/ayuda/calmov';
    private api = ['/caja/checkout', 'dsCheckout', 'ttTmpco'];
    public dataGrid: any = [];
    public folioSelected: any;
    public ttInfocia: any = [];
    public ttUsuario: any = [];
    public movtoSelect: any = [];
    public dataMovimientos: any = [];
    public dataBalances: any = [];
    public dataTtmon: any = [];
    public manttoCaja: any[];
    public selectableSettings: SelectableSettings;
    public mySelection: string[] = [];
    public fday: string;
    public cuentaSelected: any = { cCuenta: 'A', lfactura: false };
    public vistaSelected: string = 'D';
    public saldoTotal: any = 0;
    public desglosarPaquetes: boolean;
    public descSecundaria: boolean = true;
    public flagSaldos: boolean;
    public flagBuscarHuesped: boolean = true;
    public flagEntradaMovimiento: boolean;
    public flagSeleccionar: boolean;
    public flagPagarCuenta: boolean;
    public flagUserAuth: boolean;
    public flagAutorizado: boolean;
    public consulta: boolean;
    public lmensajes: boolean;
    public lreqesp: boolean;
    public lnotas: boolean;
    public lcargos: boolean;
    public folioTdc: string;
    public action: string;
    public folioTransfer: any;
    public loadApp: string;
    toast = GLOBAL.toast;
    public loading: boolean;

    public mySelectionKey(context: RowArgs): void {
        return context.dataItem;
    }

    constructor(private _crud: CrudService,
        private _buscar: BuscarService,
        private _info: InfociaGlobalService,
        private _active: ActivatedRoute,
        private _router: Router,
        private _shared: SharedService,
        private _nav: CheckinService,
        private _valid: ValidationsService,
        private _modalService: NgbModal,
        private _config: NgbModalConfig,
        private _menuServ: ManttoMenuService,
        private _httpClient: HttpClient,
        private _decimal: DecimalPipe) {
        this._config.backdrop = 'static';
        this._config.keyboard = false;
        this._active.data.subscribe(data => {
            this.consulta = data.consulta;
            if (!this.consulta) {
                this.selectableSettings = { checkboxOnly: false, mode: 'multiple' };
            } else {
                this.heading = 'Consulta de Cuentas';
            }
        });
        this.ttInfocia = this._info.getSessionInfocia();
        this.ttUsuario = this._info.getSessionUser();
        this.fday = moment(this.ttInfocia.fday).format(this.formatMoment);
        this.manttoCaja = this._info.manttoCaja(this._info.getSessionUser());
    }

    ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.inFolio && changes.inFolio.currentValue) {
            this.consulta = true;
            this.resultHuesped(this.inFolio);
        }
    }

    seleccionarCuenta(e) {
        if (this.flagSeleccionar) {
            this.moverCuenta(e);
        } else {
            if (e.cCuenta !== this.cuentaSelected.cCuenta) {
                this.cuentaSelected = e;
                this.getSaldos();
            }
        }
        this.mySelection = [];
    }

    seleccionarVista(e) {
        if (e !== this.vistaSelected) {
            this.vistaSelected = e;
            this.getSaldos();
        }
    }

    resultFactura(e) {
        if (e === 'refresh') {
            this.consultarFolio();
            this.cuentaSelected = this.dataBalances[0];
        }
        if (e === 'facturado') {
            this.consultarFolio();
            this.cuentaSelected = this.dataBalances[0];
            this.cuentaSelected.lfactura = true;
        }
    }

    async darCheckOut() {
        const i = [];
        this.dataBalances.forEach(item => {
            if (item.dbalance) {
                i.push(item);
                return;
            }
        });
        if (Object.keys(i).length) {
            this.toast({ type: 'info', title: 'Cuenta ' + i[0].cCuenta + ' con Saldo!!!' });
        } else {
            if (!this.ttInfocia.cof) {
                const hash = {};
                const result = this.dataMovimientos.filter(item => {
                    const exists = !hash[item.ttfolio] || false;
                    hash[item.ttfolio] = true;
                    return exists;
                });
                result.forEach(item => {
                    switch (item.ttfolio) {
                        case 'A':
                            if (!this.folioSelected.lCuenta1) {
                                return this.toast({ type: 'info', title: 'Cuenta ' + item.ttfolio + ' falta imprimir factura!!!' });
                            }
                            break;
                        case 'B':
                            if (!this.folioSelected.lCuenta2) {
                                return this.toast({ type: 'info', title: 'Cuenta ' + item.ttfolio + ' falta imprimir factura!!!' });
                            }
                            break;
                        case 'C':
                            if (!this.folioSelected.lCuenta3) {
                                return this.toast({ type: 'info', title: 'Cuenta ' + item.ttfolio + ' falta imprimir factura!!!' });
                            }
                            break;
                        case 'D':
                            if (!this.folioSelected.lCuenta4) {
                                return this.toast({ type: 'info', title: 'Cuenta ' + item.ttfolio + ' falta imprimir factura!!!' });
                            }
                            break;
                        case 'E':
                            if (!this.folioSelected.lCuenta5) {
                                return this.toast({ type: 'info', title: 'Cuenta ' + item.ttfolio + ' falta imprimir factura!!!' });
                            }
                            break;
                        case 'F':
                            if (!this.folioSelected.lCuenta6) {
                                return this.toast({ type: 'info', title: 'Cuenta ' + item.ttfolio + ' falta imprimir factura!!!' });
                            }
                    }
                })
            }
            if (this.folioSelected.hfsal !== this.fday) {
                swal({
                    input: 'textarea', inputPlaceholder: 'Razón de la salida inesperada:',
                    showCancelButton: true, showLoaderOnConfirm: true,
                    preConfirm: (nota) => {
                        if (!nota) {
                            swal.showValidationMessage(`Ingrese una razón!`)
                        }
                    }
                }).then(async (result) => {
                    if (result.value) {
                        this.folioSelected.etice1 = result.value;
                        this.putCheckOut();
                    }
                });
            }
            if (this.folioSelected.hfsal === this.fday) {
                this.putCheckOut();
            }
        }
    }

    putCheckOut() {
        const folio = JSON.parse(this.parseFolio(this.folioSelected));
        folio.heactual = 'CO';
        this._crud.putInfo(folio, this.api[0], this.api[1], 'ttHuespedes').subscribe(data => {
            const res = data.siCheckout.dsMensajes.ttMensError;
            if (res) {
                return swal(res[0].cMensTit, res[0].cMensTxt, 'error');
            }
            swal({
                position: 'center', type: 'success', title: 'Transacción Realizada!!!',
                showConfirmButton: false, timer: 1500
            }).then(() => {
                this.resetForm();
                this.btnBuscarFolio();
            });
        });
    }

    moverCuenta(e) {
        const temp = JSON.parse(JSON.stringify(this.movtoSelect));
        let i = 0;
        if (temp.length) {
            temp.forEach(item => {
                if (item.ttm === 'T') {
                    i++;
                    this.mySelection.splice(item);
                    return this.toast({ type: 'warning', title: 'Movimiento Invalido!!!' });
                }
                if (item.ttm === 'C') {
                    i++;
                    this.mySelection.splice(item);
                    return this.toast({ type: 'warning', title: 'Movimiento de Cargos Adicionales!!!' });
                }
                if (item.ttm === 'R') {
                    i++;
                    this.mySelection.splice(item);
                    return this.toast({ type: 'warning', title: 'Movimiento de Requerimientos Especiales!!!' });
                }
                item.ttf = e.cCuenta;
                item.lsel = true;
                item.tfecha = moment(item.tfecha, this.formatMoment).format(GLOBAL.formatIso);
            });
            if (i) { return; }

            this.loading = true;
            this._crud.putInfoArray(temp, this.api[0] + '/mc', this.api[1], this.api[2]).subscribe(async data => {
                this.loading = false;
                const result = data.siCheckout.dsCheckout.ttHuespedes;
                if (data) {
                    const err = data.siCheckout.dsMensajes.ttMensError;
                    if (err) {
                        return swal(err[0].cMensTit, err[0].cMensTxt, 'error');
                    }
                    if (result && result[0].cEactual === 'CO') {
                        swal('Huesped Check-Out!!', '', 'warning');
                        this.btnBuscarFolio();
                    }
                    await this.consultarFolio();
                    this.cuentaSelected = this.dataBalances[0];
                }
            });
        } else {
            this.mySelection = [];
        }
    }

    async resultHuesped(huesped) {
        if (!this.consulta && huesped.heactual === 'CO') {
            return swal('Huesped Check-Out!!!', '', 'warning');
        }
        this.loading = true;
        if (!huesped.cuarto) {
            huesped.cuarto = huesped.hfolio
        }
        this.folioSelected = huesped;
        await this.getSaldos();
        this.actionsNotification(this.folioSelected.hfolio);
        if (this.folioSelected.hdfp) {
            this.folioTdc = this.folioSelected.hdfp.substring(13);
        }
        this.flagBuscarHuesped = false;
        this.flagSaldos = true;
        setTimeout(() => this.cuentaSelected = this.dataBalances[0], 1000);
    }

    getSaldos(): Promise<boolean> {
        this.loading = true;
        return new Promise((resolve) => {
            this.mySelection = [];
            this.dataGrid = [];
            this.dataTtmon = [];
            this.dataMovimientos = [];
            let total = 0;
            let desglosar = 'no';
            if (this.desglosarPaquetes) {
                desglosar = 'yes';
            }
            const url = this.urlSec + esp + this.folioSelected.hfolio + esp + this.vistaSelected + ',' + desglosar + ',c' + esp + 'i';
            this._crud.getData(url).subscribe(result => {
                const data = result.siAyuda.dsAyuda;
                if (data.ttTmon) {
                    data.ttTotmon.forEach(item => total += item.dbalance);
                    this.dataBalances = data.ttTotmon;
                    if (data.ttTmpco) {
                        data.ttTmpco.forEach(item => {
                            item.tfecha = moment(item.tfecha, GLOBAL.formatIso).format(this.formatMoment);
                            if (item.ttfolio === this.cuentaSelected.cCuenta) {
                                this.dataGrid.push(item);
                            }
                        });
                        this.dataMovimientos = data.ttTmpco;
                        data.ttTmon.forEach(item => {
                            if (item.ttfolio === this.cuentaSelected.cCuenta) {
                                this.dataTtmon.push(item);
                            }
                        });
                    }
                }
                this.saldoTotal = total.toFixed(2);
                this.loading = false;
                resolve(true);
            });
        });

    }

    pagarCuenta() {
        this.entradaMovimiento(this.modalDetMov);
        this.flagPagarCuenta = true;
        this.action = 'P';
    }

    entradaMovimiento(modal) {
        this.openModal(modal, 'xl');
        this.flagPagarCuenta = false;
        this.flagEntradaMovimiento = true;
        this.action = 'M';
    }

    parseFolio(folio) {
        const tempFol = JSON.parse(JSON.stringify(folio));
        // tempFol.hfecha = moment(tempFol.hfecha, this.formatMoment).format(GLOBAL.formatIso);
        return JSON.stringify(tempFol);
    }

    abrirCuenta() {
        const grid = [...this.dataGrid];
        grid.forEach(item => item.tfecha = moment(item.tfecha, this.formatMoment).format(GLOBAL.formatIso));
        swal({
            title: 'Factura Impresa de la Cuenta ' + this.cuentaSelected.cCuenta + '!!!', text: '¿Deseas Abrir Cuenta y Cancelar Factura?',
            type: 'info', showCancelButton: true, confirmButtonColor: '#3085d6', cancelButtonColor: '#d33', confirmButtonText: 'Si',
            cancelButtonText: 'No', confirmButtonClass: 'btn btn-success py-3 px-4', cancelButtonClass: 'btn btn-light ml-1 py-3 px-4'
        }).then((result) => {
            if (result.value) {
                const json = '{"dsCheckout":{"ttHuespedes": [' + this.parseFolio(this.folioSelected) + '],' +
                    '"ttTmpco": ' + JSON.stringify(grid) + '}}';
                this._crud.putInfo2(json, '/caja/cuenta/' + this.cuentaSelected.cCuenta).subscribe(async data => {
                    if (data) {
                        const res = data.siCheckout.dsMensajes.ttMensError;
                        if (res) {
                            return swal(res[0].cMensTit, res[0].cMensTxt, 'error');
                        }
                        await this.consultarFolio();
                        this.cuentaSelected = this.dataBalances[0];
                        this.cuentaSelected.lfactura = false;
                    }
                });
            }
        });
    }

    transferirMovto(folio) {
        this.folioTransfer = folio;
        const temp = JSON.parse(JSON.stringify(this.movtoSelect));
        if (this.manttoCaja[1] || this.flagAutorizado) {
            temp.forEach(item => {
                item.lsel = true;
                item.tfecha = moment(item.tfecha, this.formatMoment).format(GLOBAL.formatIso);
            });
            this._crud.putInfoArray(temp, this.api[0] + '/' + 't' + esp + folio.hfolio, this.api[1], this.api[2]).subscribe(async data => {
                const res = data.siCheckout.dsMensajes.ttMensError;
                if (res) {
                    return swal(res[0].cMensTit, res[0].cMensTxt, 'error');
                }
                this.movtoSelect = [];
                this.flagAutorizado = false;
                await this.consultarFolio();
                this.cuentaSelected = this.dataBalances[0];

            });
        } else {
            this.flagUserAuth = true;
        }
    }

    transferMovto(modal) {
        this.flagEntradaMovimiento = false;
        if (this.movtoSelect) {
            this.validarMovto(this.movtoSelect, 'transferir').then(res => {
                if (res) {
                    this.mySelection = [];
                } else {
                    this.openModal(modal, 'lg');
                    this.action = 'T';
                }
            });
        }
    }

    ajusteMovto(modal) {
        if (this.movtoSelect) {
            this.validarMovto(this.movtoSelect, 'ajuste').then(res => {
                if (res) {
                    this.mySelection = [];
                } else {
                    this.openModal(modal, 'lg');
                    this.action = 'J';
                    this.flagEntradaMovimiento = true;
                }
            });
        }
    }

    splitMovto(modal) {
        if (this.movtoSelect) {
            this.validarMovto(this.movtoSelect, 'split').then(res => {
                if (res) {
                    this.mySelection = [];
                } else {
                    this.openModal(modal, 'xl');
                    this.action = 'S';
                    this.flagEntradaMovimiento = true;
                }
            });
        }
    }

    validarMovto(dataItem, tipo): Promise<number> {
        return new Promise((resolve) => {
            dataItem.forEach(item => {
                if (item.tfecha === this.fday || item.ttm === 'T' || tipo === 'ajuste' || this.desglosarPaquetes) {
                    if (item.tfecha === this.fday) {
                        this.toast({ type: 'info', title: 'Movimiento del Día, Cancelar!!!' });
                        resolve(1);
                    } else if (item.ttm === 'T') {
                        this.toast({ type: 'info', title: 'Movimiento Desglosado!!!' });
                        resolve(1);
                    } else if (this.desglosarPaquetes) {
                        this.toast({ type: 'info', title: 'Desglose de paquetes!!!' });
                        resolve(1);
                    } else if (tipo === 'ajuste') {
                        this._shared.getCodigos('', '', 'y', item.tcod).subscribe(data => {
                            if (data && data[0].cdc === 'C') {
                                this.toast({ type: 'info', title: 'Creditos no se Ajustan!!!' });
                                resolve(1);
                            } else {
                                resolve(0);
                            }
                        });
                    }
                } else {
                    resolve(0);
                }
            });
        });
    }

    autorizarTransferMovto(user, cval) {
        if (cval[3] === 'S') {
            this.flagUserAuth = false;
            this.movtoSelect.forEach(item => item.cChar3 = user.user);
            this.flagAutorizado = true;
            this.transferirMovto(this.folioTransfer);
        } else {
            this.toast({ type: 'info', title: 'No Autorizado!!!' });
            this.flagAutorizado = false;
        }
    }

    userAuth(user) {
        this._valid.getAuthorization(user.user, user.pass, 1, 0, 0).subscribe(async data => {
            const cval = data.pcCod.split('');
            if (cval[0] === 'S' && cval[1] === 'S') {
                if (this.action === 'T') {
                    this.autorizarTransferMovto(user, cval);
                }
            } else {
                if (cval[0] !== 'S') {
                    this.toast({ type: 'info', title: 'Usuario no existe!!!' });
                }
                if (cval[1] !== 'S') {
                    this.toast({ type: 'info', title: 'Clave incorrecta!!!' });
                }
            }
        });
    }

    async resultMovto(event, close) {
        if (event === 'close') {
            close();
        }
        await this.consultarFolio();
        this.cuentaSelected = this.dataBalances[0];
        if (this.cuentaSelected.dbalance !== 0) {
            this.cuentaSelected.lfactura = false;
        }
    }

    consultarFolio(): Promise<boolean> {
        return new Promise((resolve) => {
            this.loading = true;
            this._buscar.getHuesped(this.folioSelected.hfolio, 'folio').subscribe(async data => {
                if (data) {
                    this.folioSelected = data[0];
                    this.flagSeleccionar = false;
                    await this.getSaldos();
                    resolve(true);
                }
            });

        });
    }

    crearPreimpreso() {
        this.loading = true;
        const folio = [...[this.folioSelected]][0];
        const infocia = [...[this.ttInfocia]][0];
        const movtos = JSON.parse(JSON.stringify(this.dataGrid));
        let totalExentos = 0;
        infocia.fday = moment(infocia.fday).format(this.formatMoment);
        movtos.forEach(item => {
            totalExentos += item.texentos;
            item.tcargo = this._decimal.transform(item.tcargo, '1.2-2');
            item.tabono = this._decimal.transform(item.tabono, '1.2-2');
            item.tbalan = this._decimal.transform(item.tbalan, '1.2-2');
            item.texentos = this._decimal.transform(item.texentos, '1.2-2');
        });
        const json = {
            Propiedad: this.ttInfocia.propiedad,
            iSec: '003-PREIMPRESO',
            movtos: movtos,
            folio: folio,
            infocia: infocia,
            usuario: this.ttUsuario,
            balance: this._decimal.transform(this.cuentaSelected.dbalance, '1.2-2'),
            totalExentos: this._decimal.transform(totalExentos, '1.2-2'),
            hora: moment().format('HH:mm:ss'),
        }
        this._httpClient.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
            response => {
                const linkSource = 'data:application/pdf;base64,' + response.base64;
                const download = document.createElement('a');
                download.href = linkSource;
                download.download = this.folioSelected.hfolio + '.pdf';
                download.click();
                this.loading = false;
            },
            error => {
                swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
                this.loading = false;
            }
        );
    }

    openHelp(param, size) {
        if (!size) { size = 'lg'; }
        this.reiniciarBanderas();
        this.loadApp = param;
        this.openModal(this.modalHelp, size);
        if (this.flagSeleccionar) {
            this.seleccionMultiple();
        }
    }

    openModal(modal, size) {
        this._modalService.open(modal, { size: size, centered: true });
    }

    btnBuscarFolio() {
        this.reiniciarBanderas();
        this.flagBuscarHuesped = true;
        this.flagSaldos = false;
    }

    actionsNotification(folio) {
        if (!folio) {
            this.lmensajes = false;
            this.lnotas = false;
            this.lreqesp = false;
            this.lcargos = false;
        } else {
            this._nav.actionsNotification(folio).subscribe(data => {
                if (data) {
                    this.lmensajes = data[0].lmensajes;
                    this.lnotas = data[0].lnotas;
                    this.lreqesp = data[0].lreqesp;
                    this.lcargos = data[0].lcargos;

                }
            });
        }
    }
    seleccionMultiple() {
        this.flagSeleccionar = !this.flagSeleccionar;
        this.mySelection = [];
    }

    onSelectedKeysChange(e) {
        this.movtoSelect = e;
    }

    resetForm() {
        this.flagBuscarHuesped = true;
        this.flagSaldos = false;
        this.folioSelected = null;
        this.dataGrid = [];
        this.dataBalances = [];
        this.folioTdc = '';
    }

    reiniciarBanderas() {
        this.flagSaldos = true;
        this.flagBuscarHuesped = false;
        this.loadApp = '';
        this.flagEntradaMovimiento = false;
        if (this.folioSelected) {
            this.actionsNotification(this.folioSelected.hfolio);

        }
    }
}
