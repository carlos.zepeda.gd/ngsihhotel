import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DecimalPipe } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ChequeFacturaService } from '../../../../pos/puntos-venta/cheque/services/cheque.factura.service';
import { HelpsCheckInService } from '../../../recepcion/check-in/services/helps/helps-checkin.service';
import { RecepcionService } from '../../../recepcion/recepcion.service';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { Tgeneral } from '../../../../models/tbl-general';
import { GLOBAL } from '../../../../services/global';
import moment from 'moment';
const esp = GLOBAL.char174;

@Component({
    selector: 'app-factura-checkout',
    templateUrl: './factura-checkout.component.html',
    styleUrls: ['./factura-checkout.component.css'],
    providers: [ChequeFacturaService, DecimalPipe]
})
export class FacturaCheckoutComponent implements OnInit, OnChanges {
    @Input() facturar: boolean;
    @Input() folio: any = [];
    @Input() movtos: any = [];
    @Output() finished = new EventEmitter;
    @ViewChild('rfc') _rfc: ElementRef;
    facturaForm: FormGroup;
    dataCliente: any;
    dsConsecutivos: any;
    formGeneral: any;
    dsTiposCfd: any = [];
    formEnviar: Tgeneral;
    maskRfc = '?LLL-000000-???';
    maskPhone = '(000) 000-0000???';
    rfcSelected: any;
    dsParametroRfc: any = [];
    etiquetaRfc: string = 'R.F.C.';
    formatMoment: string = localStorage.getItem('formatMoment');
    ttInfocia: any = [];
    loading: boolean;
    toast = GLOBAL.toast;
    swal = GLOBAL.swal;
    msjFacturaService: string = 'Espere un momento, transacción en proceso. ';

    constructor(
        private _fb: FormBuilder,
        private _info: InfociaGlobalService,
        private _fact: ChequeFacturaService,
        private _rec: RecepcionService,
        private _modalService: NgbModal,
        private _helps: HelpsCheckInService,
        private _httpClient: HttpClient,
        private _decimal: DecimalPipe
    ) {
        this.ttInfocia = this._info.getSessionInfocia();
        this.createForm();
        this.createTableGeneral();
        this._fact.getIntFactHoy().subscribe(data => {
            const cPco = data[0].cPco[8].split(',');
            cPco.forEach(item => {
                const clave = item.substring(0, 3);
                const desc = item.substring(4)
                this.dsTiposCfd.push({ 'cClave': clave, 'cDesc': desc + ' - ' + clave });
            });
            this.formGeneral.usoCfd = this.dsTiposCfd[0];
            this._fact.getConsecutivos('c').subscribe(consec => this.dsConsecutivos = consec);
        });
        this._rec.getParametros('rfc').subscribe(data => {
            if (data) {
                this.dsParametroRfc = data[0];
                this.etiquetaRfc = data[0].pchr1;
                this.maskRfc = data[0].pchr2.replace(/X/g, 'a');
            }
        });
    }

    ngOnInit() {
    }

    ngOnChanges(change: SimpleChanges) {
        if (change['folio'] && change['folio'].currentValue) {
            if (this.folio.rfc) {
                this.facturaForm.patchValue({ rfc: this.folio.rfc });
                this._rfc.nativeElement.focus();
            }
        }
    }

    enviarEmail() {
        if (this.rfcSelected) {
            if (!this.formGeneral.enviar) {
                this.formGeneral.emailEnv = this.rfcSelected.remail;
            } else {
                this.formGeneral.emailEnv = '';
            }
        }
    }

    rfcValue(e) {
        this.rfcSelected = e;
        this.facturaForm.setValue(e);
        if (this.rfcSelected.remail) {
            this.formGeneral.emailEnv = this.rfcSelected.remail;
        }
    }

    estadoValue(e) {
        this.facturaForm.patchValue({ estado: e.Estado });
    }

    openModal(modal, size) {
        this._modalService.open(modal, { size: size, centered: true });
    }

    consecutivoSelected(e) {
        if (e) {
            this.formGeneral.factura = e.iNum;
        }
    }

    changeRfc(field) {
        let api;
        if (field === 'rfc') {
            api = this.facturaForm.value.rfc + '|';
        }
        if (field === 'sucursal') {
            api = this.facturaForm.value.rfc + '|' + this.facturaForm.value.Sucursal;
        }
        this._helps.getRfc('R', api).subscribe(data => {
            if (data) {
                this.rfcValue(data[0]);
            }
        });
    }

    changeForm(field, change) {
        this._fact.validarInputs(field, change).subscribe(data => {
            if (field === 'estados') {
                if (data.pcCod) {  // No es un valor valido
                    this.facturaForm.controls['estado'].setErrors({ 'invalid': true });
                }
            }
        });
    }

    createTableGeneral() {
        this.formGeneral = {
            consecutivo: '',
            usoCfd: '',
            factura: '',
            emailEnv: '',
            consumo: '',
            enviar: false
        };
    }

    createForm() {
        this.facturaForm = this._fb.group({
            rfc: ['', [Validators.minLength(12), Validators.maxLength(13)]],
            Sucursal: ['', Validators.maxLength(30)],
            rrsocial: ['', [Validators.required, Validators.maxLength(150)]],
            rdireccion1: ['', Validators.maxLength(50)],
            rdireccion2: ['', Validators.maxLength(50)],
            exterior: ['', Validators.maxLength(10)],
            interior: ['', Validators.maxLength(10)],
            rciudad: ['', [Validators.required, Validators.maxLength(50)]],
            estado: ['', Validators.maxLength(4)],
            rcp: ['', Validators.maxLength(15)],
            rtelefono1: ['', Validators.maxLength(20)],
            rtelefono2: ['', Validators.maxLength(20)],
            rfax: ['', Validators.maxLength(10)],
            rcontacto: ['', Validators.maxLength(40)],
            rpuesto: ['', Validators.maxLength(30)],
            remail: ['', Validators.maxLength(250)],
            rnotas: ['', Validators.maxLength(30)],
            Inactivo: [false],
            renv: [''],
            Usuario: [''],
            cRowID: [''],
            lError: [false],
            cErrDes: ['']
        });
    }

    onSubmit() {
        this.dataCliente = this.facturaForm.value;
        if (this.facturaForm.value.rfc && this.rfcSelected) {
            this._fact.putCliente(this.dataCliente).subscribe(data => {
                if (data) {
                    this.resultFactura(data);
                }
            });
        }
        if (!this.facturaForm.value.rfc && !this.rfcSelected) {
            this.facturarFolio();
        }
        if (this.facturaForm.value.rfc && !this.rfcSelected) {
            this._fact.postCliente(this.dataCliente).subscribe(data => {
                if (data) {
                    this.resultFactura(data);
                }
            });
        }
    }

    resultFactura(data) {
        this.dataCliente = data[0];
        if (this.dataCliente.cErrDes) {
            this.swal('Error al grabar R.F.C.!', this.dataCliente.cErrDes, 'error');
            this.finished.emit('close');
        } else {
            this.facturarFolio();
        }
    }

    facturarFolio() {
        const val = this.formGeneral;
        const movtos = [...this.movtos];

        let rfcSucursal = '';
        if (this.facturaForm.value.rfc) {
            rfcSucursal = this.facturaForm.value.rfc;
        }
        if (this.rfcSelected && this.rfcSelected.Sucursal) {
            rfcSucursal = rfcSucursal + '|' + this.rfcSelected.Sucursal;
        }
        movtos.forEach(item => {
            item.tfecha = moment(item.tfecha, this.formatMoment).format(GLOBAL.formatIso);
            delete item['ttHuespaq'];

        });
        this.formEnviar = new Tgeneral(
            val.consecutivo.cCon + esp + this.folio.hfolio + esp + rfcSucursal + esp + val.usoCfd.cClave,
            movtos[0].ttfolio, '', '', '', '', this.facturaForm.value.rnotas
        );
        this.formEnviar.lLog1 = true;
        this.loading = true;
        this._fact.putFactura2(this.folio, movtos, this.formEnviar).subscribe(async factura => {
            if (factura.dsMensajes.ttMensError) {
                this.msjFacturaService = '';
                this.loading = false;
                this.swal(factura.dsMensajes.ttMensError[0].cMensTit, factura.dsMensajes.ttMensError[0].cMensTxt, 'warning');
            } else {
                if (factura.dsFacturaH.BttRfccias) {
                    this.msjFacturaService = 'Factura Timbrada!! Generando PDF...';
                    const sihFact = factura.dsFacturaH;
                    const fileName = sihFact.BttTgeneral[0];
                    const response = await this.crearFactura(sihFact, fileName);
                    if (response) {
                        this.loading = false;
                        this.finished.emit('facturado');
                    }
                }
            }
        });
    }

    crearFactura(val, file): Promise<boolean> {

        return new Promise((resolve) => {
            let enviarEmail = true;
            if (!this.formGeneral.emailEnv) {
                enviarEmail = false;
            }
            val.BttMov.forEach(item => {
                item.ffecha = moment(item.ffecha).format(this.formatMoment)
                item.dCargo = this._decimal.transform(item.dCargo, '1.2-2');
                item.dMonto = this._decimal.transform(item.dMonto, '1.2-2');
                if (item.ttTimptos !== undefined) {
                    item.ttTimptos.forEach(item2 => {
                        item2.dFactor = this._decimal.transform(item2.dFactor, '1.2-2');
                        item2.dBase = this._decimal.transform(item2.dBase, '1.2-2');
                        item2.dMonto = this._decimal.transform(item2.dMonto, '1.2-2');
                    });
                }

            });
            val.BttHuespedes.forEach(item => {
                item.hfent = moment(item.hfent).format(this.formatMoment);
                item.hfsal = moment(item.hfsal).format(this.formatMoment);
                item.dTotal = this._decimal.transform(item.dTotal, '1.2-2');
                if (item.BttTimptos !== undefined) {
                    item.BttTimptos.forEach(item2 => {
                        item2.dFactor = this._decimal.transform(item2.dFactor, '1.2-2');
                        item2.dBase = this._decimal.transform(item2.dBase, '1.2-2');
                        item2.dMonto = this._decimal.transform(item2.dMonto, '1.2-2');
                    });
                }
            });
            // carlos.zepeda.ti@gmail.com;carlos.zepeda_green@hotmail.com;elamm.2040@gmail.com
            const emails = this.formGeneral.emailEnv.split(';');
            const cc = [];
            const para = [emails[0]];
            if (emails.length) {
                delete emails[0];
                if (emails.length) {
                    emails.forEach(item => cc.push(item));
                }
            }
            const json = {
                propiedad: this.ttInfocia.propiedad,
                config: val.BttTgeneral[0].cChr5,
                nombrepdf: file.cChr11,
                xml: '<xml></xml>',
                BttConsec: val.BttConsec[0],
                BttHuespedes: val.BttHuespedes[0],
                BttMov: val.BttMov,
                BttRfccias: val.BttRfccias[0],
                BttTgeneral: val.BttTgeneral[0],
                BttTmpco: val.BttTmpco,
                ttCias: val.ttCias[0],
                email: {
                    enviar: enviarEmail,
                    de: '',
                    deNombre: '',
                    para: para,
                    cc: cc,
                    asunto: 'Factura: ' + file.cChr11,
                    msj: ''
                }
            }
            this._httpClient.post<any>(GLOBAL.urlPhp + '/api/pdf', json).subscribe(
                response => {
                    if (response.errors !== undefined) {
                        if (response.errors.type === 'email') {
                            this.toast({ title: 'Error en el envio de email.', type: 'warning' });
                        }
                    }
                    const linkSource = 'data:application/pdf;base64,' + response.pdf;
                    const download = document.createElement('a');
                    download.href = linkSource;
                    download.download = file.cChr11 + '.pdf';
                    download.click();
                    resolve(true);
                },
                error => {
                    this.msjFacturaService = GLOBAL.errorPdf[0];
                    this.swal(GLOBAL.errorPdf[0],
                        'Por favor entrar a "Caja / Analisis de Cuentas" para reimprimir su factura.', 'info')
                        .then(() => this.finished.emit('facturado'));
                }

            );
        });
    }

    example() {
        const json = {
            propiedad: 'sih',
            config: '100FACT',
            nombrepdf: 'Facturaprueba',
            BttConsec: [],
            xml: '<xml></xml>',
            email: {
                enviar: false, de: '', deNombre: '',
                para: '', asunto: '', msj: ''
            }
        }
        this._fact.postGenerarFactura3(JSON.stringify(json)).subscribe(data => {
            const cadena = JSON.parse(data._body);
            const linkSource = 'data:application/pdf;base64,' + cadena.pdf;
            const download = document.createElement('a');
            const fileName = 'factura-checkOut.pdf';
            download.href = linkSource;
            download.download = fileName;
            download.click()
        });
    }
}
