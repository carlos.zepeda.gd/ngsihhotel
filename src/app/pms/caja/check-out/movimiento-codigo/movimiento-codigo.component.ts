import { Component, Input, SimpleChanges, OnChanges } from '@angular/core';
import { CrudService } from 'app/pms/mantto-srv.services';
import { GLOBAL } from 'app/services/global';

@Component({
    selector: 'app-movimiento-codigo',
    templateUrl: './movimiento-codigo.component.html',
    styleUrls: ['./movimiento-codigo.component.css']
})
export class MovimientoCodigoComponent implements OnChanges {
    @Input() movimiento: any = [];
    public dataGrid: any = [];
    formatDate = localStorage.getItem('formatKendo');
    noRecords = GLOBAL.noRecords;
    constructor(private _crud: CrudService) {
    }
    ngOnChanges(changes: SimpleChanges): void {
        if (changes.movimiento && changes.movimiento.currentValue) {
            this._crud.getData('/ayuda/hisaud' + GLOBAL.char174 + this.movimiento[0].trecid).subscribe(data => {
                if (data.siAyuda.dsAyuda.ttTmpmovto) {
                    this.dataGrid = data.siAyuda.dsAyuda.ttTmpmovto;
                }
            });
        }
    }
}
