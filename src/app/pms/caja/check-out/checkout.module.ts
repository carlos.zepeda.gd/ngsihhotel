// Imports Angular
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
// Componentes
import { EntradaMovimientosComponent } from '../entrada-movimientos/entrada-movimientos.component';
import { DetalleMovimientoComponent } from './detalle-movimiento/detalle-movimiento.component';
import { MovimientoCodigoComponent } from './movimiento-codigo/movimiento-codigo.component';
import { FacturaCheckoutComponent } from './factura-checkout/factura-checkout.component';
import { CheckOutComponent } from './check-out.component';
// Modules
import { ArchitectModule } from '../../../_architect/architect.module';
import { SharedAppsModule } from '../../../shared/shared-apps.module';
import { KendouiModule } from '../../../shared/kendo.module';
// Servicios externos
import { BuscarService } from '../../recepcion/check-in/services/childs/buscar.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { SharedService } from '../../../services/shared.service';
import { CrudService } from '../../mantto-srv.services';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        KendouiModule,
        RouterModule,
        SharedAppsModule,
        ArchitectModule
    ],
    declarations: [
        EntradaMovimientosComponent,
        CheckOutComponent,
        MovimientoCodigoComponent,
        DetalleMovimientoComponent,
        FacturaCheckoutComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [
        InfociaGlobalService,
        CrudService,
        SharedService,
        BuscarService
    ],
    exports: [
        EntradaMovimientosComponent,
        CheckOutComponent,
        MovimientoCodigoComponent,
        DetalleMovimientoComponent,
        FacturaCheckoutComponent
    ]
})
export class CheckOutModule {
}
