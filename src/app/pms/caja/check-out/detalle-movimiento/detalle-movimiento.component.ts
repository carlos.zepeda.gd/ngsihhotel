import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import { BuscarService } from '../../../recepcion/check-in/services/childs/buscar.service';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { SharedService } from '../../../../services/shared.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GLOBAL } from '../../../../services/global';
const swal = GLOBAL.swal;

@Component({
    selector: 'app-detalle-movimiento',
    templateUrl: './detalle-movimiento.component.html',
    styleUrls: ['./detalle-movimiento.component.css']
})
export class DetalleMovimientoComponent implements OnInit, OnChanges {

    @Input() movimiento: any = [];
    @Input() folio: any = [];
    @Input() cuenta: any = [];
    @Input() action: string;
    @Output() transferir = new EventEmitter;
    @Output() split = new EventEmitter;
    public dsMonedas: any = [];
    public folioSelected: any = [];
    public ttInfocia: any = [];
    public dataGrid: any = [];
    public dsAjuste: any = { subtotal: 0, servicio: 0, ajuste: 0, total: 0 };
    public nombreHuesped: string;
    public flagTransferir: boolean;
    public flagSplit: boolean;
    public flagBuscarHuesped: boolean;
    public inputFolio: number;
    public inputNombre: string;
    public totalPorciones = Array(4).fill(0).map((x, i) => i);
    public porciones: number = 2;
    public validatorZero: boolean;
    public arrayPorciones: any[];
    public desglosePorciones: boolean;
    public tipoSplit: string = 'Porcentaje';

    constructor(private _buscar: BuscarService,
        private _info: InfociaGlobalService,
        private modalService: NgbModal,
        private _shared: SharedService) {
    }

    ngOnInit() {
        this.ttInfocia = this._info.getSessionInfocia();
        this._shared.getMonedas().subscribe(data => {
            this.dsMonedas = data;
            data.forEach(moneda => moneda.mdesc = moneda.mdesc + ' (' + moneda.Moneda + ')');
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.action !== undefined) {
            if (changes.action.currentValue === 'T') {
                this.transferirMovto();
            }
        }
        if (changes.folio && changes.folio.currentValue) {
            this.nombreHuesped = this.folio.hapellido + ' ' + this.folio.hnombre;
        }
        if (changes.movimiento && changes.movimiento.currentValue) {
            this.dsAjuste.subtotal = this.movimiento.tcargo;
            this.dsAjuste.ajuste = this.dsAjuste.subtotal;
        }
    }
    openModal(modal) {
        this.modalService.open(modal, { size: 'xl' });
    }
    
    desglosarPorciones(porciones) {
        this.porciones = porciones;
        this.arrayPorciones = [];
        let total = 0;
        let acumulador = 0;
        this.validatorZero = false;
        this.desglosePorciones = true;
        if (this.tipoSplit === 'Monto') {
            this.movimiento[0].tcargo ? total = this.movimiento[0].tcargo : total = this.movimiento[0].tabono;
        } else {
            total = 100;
        }
        for (let i = 0; i < porciones - 1; i++) {
            if (Math.floor(total / porciones) === 0) {
                this.validatorZero = true;
            }
            this.arrayPorciones.push(Math.floor(total / porciones));
            acumulador += Math.floor(total / porciones);
        }
        this.arrayPorciones.push(Math.ceil(total - acumulador));

    }

    splitMovto() {
        this.flagTransferir = false;
        this.flagSplit = true;
        this.desglosarPorciones(2);
    }

    resultHuesped(e) {
        if (this.folio.hfolio === e.hfolio) {
            return swal('Folio incorrecto!!!', '', 'warning');
        }
        if (e.heactual === 'CO') {
            return swal('Folio Check-Out!!!', '', 'warning');
        }
        this.folioSelected = e;
        this.inputFolio = this.folioSelected.hfolio;
        this.inputNombre = this.folioSelected.hapellido + ' ' + this.folioSelected.hnombre;
        this.flagBuscarHuesped = false;
    }

    buscarFolio() {
        this.nombreHuesped = '';
        if (this.inputFolio.toString().length <= 0) {
            return;
        }
        this._buscar.getHuespedFolio(this.inputFolio.toString()).subscribe(data => {
            if (!data) {
                swal('Cuarto Invalido!!', '', 'error');
            } else {
                this.resultHuesped(data[0]);
            }
        });
    }

    cambioSubtotal() {
        if (this.movimiento.tcod === 'BP') {

        }
        this.dsAjuste.ajuste = this.dsAjuste.subtotal;
        this.dsAjuste.total = this.dsAjuste.ajuste + this.movimiento.tcargo;
    }

    guardarForm() {
        if (this.flagTransferir) {
            if (Object.keys(this.folioSelected).length) {
                this.transferir.emit(this.folioSelected);
                this.movimiento = [];
            } else {
                swal('Folio!!!', 'Ingrese folio a transferir!!!', 'warning');
            }
        }
        if (this.flagSplit) {
            const arreglo = [];
            for (let i = 0; i < this.arrayPorciones.length; i++) {
                if (this.movimiento[0].tcargo) {
                    this.movimiento[0].tcargo = this.arrayPorciones[i];
                    arreglo.push(this.movimiento[0]);
                } else {
                    this.movimiento[0].tabono = this.arrayPorciones[i];
                    arreglo.push(this.movimiento[0]);
                }
            }
        }
    }

    transferirMovto() {
        this.flagTransferir = true;
        this.flagSplit = false;
    }

}
