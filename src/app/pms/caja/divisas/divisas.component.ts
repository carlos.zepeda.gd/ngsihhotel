import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../services/global';
import { CajaService } from '../caja.service';

@Component({
  selector: 'app-divisas',
  templateUrl: './divisas.component.html',
})
export class DivisasComponent implements OnInit {
  heading = 'Divisas';
  subheading: string;
  icon: string;
  api: any = ['/manttos/divisas/Divisas', 'dsDivisas', 'ttTgeneral'];
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  filter: CompositeFilterDescriptor;
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  loading: boolean;
  modalTitle = '';
  modalType = '';
  listMoneda: [];
  flagEdit: boolean;
  selectedItem: any = [];
  toast = GLOBAL.toast;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: CajaService,
    private _modal: NgbModal,
  ) { }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
    this.ttInfocia = this._info.getSessionInfocia();
    this.ngForm = this.createForm();
    this.newRow();
    this._https.getData('/manttos/monedas').map(data => data).subscribe(data => {
      if (data) {
        this.listMoneda = data.siMoneda.dsMonedas.ttMonedas;
      }
    });
  }

  createForm() {
    return this._formBuild.group({
      Cuarto: [''],
      Folio: [''],
      Control: [''],
      Desc: [''],
      Nombre: [''],
      Pasaporte: [''],
      Consec: [''],
      Pais: [''],
      DeMoneda: [''],
      HastaMoneda: [1],
      Monto: [''],
    });
  }

  openModal(modal) {
    this._modal.open(modal, { size: 'lg', centered: true });
  }

  resultHuesped(data) {
    this.ngForm.patchValue({
      Cuarto: data.cuarto,
      Folio: data.hfolio,
      Control: data.hfolio,
      Nombre: data.hapellido + ', ' + data.hnombre,
    });
  }

  public newRow() {
    this.ngForm = this.createForm();
  }

  postInfo() {
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    this._https.postInfo(temp, this.api[0], this.api[1], this.api[2]).map(data => data).subscribe(data => {
      if (data) {
        this.dataGrid = data[this.api[1]][this.api[2]][this.api[3]];
      }
      this.loading = false;
    });
  }

}
