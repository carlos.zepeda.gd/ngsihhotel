import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { CrudService } from '../../mantto-srv.services';
import { GLOBAL } from '../../../services/global';
const swal = GLOBAL.swal;

@Component({
  selector: 'app-codigos-caja',
  templateUrl: './codigos-caja.component.html',
  styleUrls: ['./codigos-caja.component.css']
})
export class CodigosCajaComponent implements OnInit {
  heading = 'Parámetro (Códigos)';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  api: Array<string> = ['/parametros/parametros/', 'siParametro', 'dsParametros', 'ttParametros'];
  toast = GLOBAL.toast;
  noRecords = GLOBAL.noRecords;
  dataGrid = [];
  ngForm: FormGroup;
  loading: boolean = true;
  listMoneda = [];

  constructor(private _menuServ: ManttoMenuService,
    private _router: Router,
    private _crudservice: CrudService,
    private _formBuild: FormBuilder) {
    this._crudservice.getData('/manttos/monedas').subscribe(data => {
      const moneda = data.siMoneda.dsMonedas.ttMonedas;
      if (moneda) {
        this.listMoneda = moneda;
      }
    });
    this.getDataSource();
    this.nuevo();
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  getDataSource() {
    this._crudservice.getData(this.api[0] + 'COD').subscribe(res => {
      const result = res[this.api[1]][this.api[2]][this.api[3]];
      if (result) {
        this.dataGrid = result;
      }
      this.loading = false;
    });
  }

  selectedRow({ selectedRows }) {
    if (selectedRows && selectedRows[0].dataItem) {
      this.ngForm.patchValue(selectedRows[0].dataItem);
      this.ngForm.patchValue({ edit: true });
    }
  }

  nuevo() {
    this.ngForm = this.createFormGroup();
  }

  guardar() {
    this.loading = true;
    this.ngForm.value.edit ? this.crudService('edit') : this.crudService('new');
  }

  private crudService(method, dataDelete?) {
    this.loading = true;
    let responseService: any;
    switch (method) {
      case 'new':
        responseService = this._crudservice.postInfo(this.ngForm.value, this.api[0], this.api[2], this.api[3]);
        break;
      case 'edit':
        responseService = this._crudservice.putInfo(this.ngForm.value, this.api[0], this.api[2], this.api[3]);
        break;
      case 'delete':
        responseService = this._crudservice.deleteData(dataDelete, this.api[0], this.api[3]);
        break;
    }

    responseService.subscribe(data => {
      const res = data[this.api[1]][this.api[2]][this.api[3]];
      if (res && res[0].cErrDesc) {
        swal('ERROR!', res[0].cErrDesc, 'error');
      } else {
        this.toast({ type: 'success', title: 'Registro guardado!!' });
        this.getDataSource();
        this.nuevo();
      }
      this.loading = false;
    });
  }

  createFormGroup() {
    return this._formBuild.group({
      Borrar: [true],
      cErrDes: [''],
      cRowID: [''],
      descripcion: ['', Validators.required],
      lError: [false],
      parametro: ['', Validators.required],
      pchr1: [''],
      pchr2: ['', Validators.required],
      pchr3: ['S'],
      pchr4: ['S'],
      pchr5: ['S'],
      pchr6: ['S'],
      pchr7: ['S'],
      pchr8: ['S'],
      pchr9: ['S'],
      pchr10: ['S'],
      pchr11: ['S'],
      pchr12: ['S'],
      pchr13: ['S'],
      pchr14: [''],
      pchr15: [''],
      pchr16: [''],
      pchr17: [''],
      pchr18: [''],
      pchr19: [''],
      pchr20: [''],
      pdate1: [null],
      pdate2: [null],
      pdate3: [null],
      pdate4: [null],
      pdate5: [null],
      pdate6: [null],
      pdate7: [null],
      pdate8: [null],
      pdate9: [null],
      pdate10: [null],
      pdate11: [null],
      pdate12: [null],
      pdate13: [null],
      pdate14: [null],
      pdate15: [null],
      pdate16: [null],
      pdate17: [null],
      pdate18: [null],
      pdate19: [null],
      pdate20: [null],
      pdec1: [null],
      pdec2: [null],
      pdec3: [null],
      pdec4: [null],
      pdec5: [null],
      pdec6: [null],
      pdec7: [null],
      pdec8: [null],
      pdec9: [null],
      pdec10: [null],
      pdec11: [null],
      pdec12: [null],
      pdec13: [null],
      pdec14: [null],
      pdec15: [null],
      pdec16: [null],
      pdec17: [null],
      pdec18: [null],
      pdec19: [null],
      pdec20: [null],
      pint1: [null],
      pint2: [null],
      pint3: [null],
      pint4: [null],
      pint5: [null],
      pint6: [null],
      pint7: [null],
      pint8: [null],
      pint9: [null],
      pint10: [null],
      pint11: [null],
      pint12: [null],
      pint13: [null],
      pint14: [null],
      pint15: [null],
      pint16: [null],
      pint17: [null],
      pint18: [null],
      pint19: [null],
      pint20: [null],
      edit: [false]
    });
  }
}
