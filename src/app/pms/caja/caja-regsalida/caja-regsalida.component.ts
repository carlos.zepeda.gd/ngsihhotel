import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BuscarService } from '../../recepcion/check-in/services/childs/buscar.service';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { CrudService } from '../../mantto-srv.services';
import { GLOBAL } from 'app/services/global';
const swal = GLOBAL.swal;

@Component({
    selector: 'app-caja-regsalida',
    templateUrl: './caja-regsalida.component.html',
    styleUrls: ['./caja-regsalida.component.css']
})
export class CajaRegsalidaComponent implements OnInit {
    heading = 'Regreso de Salida';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';
    @ViewChild('idCuarto') idCuarto: ElementRef;
    @ViewChild('modalHuesped') modalHuesped: ElementRef;

    public dataFolios: any = [];
    public folio: any = [];
    public ttInfocia: any = [];
    public nombreHuesped: string;
    public folioTdc: Boolean

    constructor(private _buscar: BuscarService,
        private _info: InfociaGlobalService,
        private _crud: CrudService,
        private _menuServ: ManttoMenuService,
        private _modalService: NgbModal,
        private _router: Router) {
        this.ttInfocia = this._info.getSessionInfocia();
    }

    ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    valueChange(value) {
        this.folio = [];
        this.nombreHuesped = '';
        this.dataFolios = [];
        if (value.length < 1) {
            return;
        }
        this._buscar.getHuesped(value, 'cuarto').subscribe(cuarto => {
            if (!cuarto) {
                this._buscar.getHuesped(value, 'folio').subscribe(folio => {
                    if (!folio) {
                        swal({
                            type: 'info',
                            title: 'Ho hay huespedes registrados en cuarto'
                        });

                    } else {
                        this.resultData(folio);
                    }
                });
            } else {
                this.resultData(cuarto);
            }
        });
    }

    resultData(data) {
        const temp = [];
        if (data) {
            data.forEach(item => {
                if (item.heactual === 'CO') {
                    temp.push(item);
                }
            });
        }
        if (temp.length === 1) {
            this.resultHuesped(data[0]);
        } else {
            this.dataFolios = temp;
            this.openModal();
        }
    }

    openModal() {
        this._modalService.open(this.modalHuesped, { size: 'xl', centered: true });
    }

    public resultHuesped(e) {
        if (e.heactual !== 'CO') {
            swal({
                type: 'info', title: 'No es Check-Out!!!',
            });
        } else {
            this.folio = e;
            this.nombreHuesped = this.folio.hapellido + ' ' + this.folio.hnombre;
        }
    }

    public guardarForm() {
        this._crud.putInfo(this.folio, '/caja/regSalida', 'dsCheckout', 'ttHuespedes').subscribe(data => {
            const result = data.siCheckout.dsMensajes;
            if (Object.keys(result).length) {
                if (result.ttMensError[0]) {
                    swal(result.ttMensError[0].cMensTit, result.ttMensError[0].cMensTxt, 'error');
                }
            } else {
                swal({
                    type: 'success', title: 'Transacción Realizada!!!',
                });
                this.folio = [];
                this.nombreHuesped = '';
                this.idCuarto.nativeElement.focus();
            }
        });
    }
}
