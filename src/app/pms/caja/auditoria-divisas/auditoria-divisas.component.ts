import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { CompositeFilterDescriptor, distinct, filterBy, process } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { TblGeneral } from '../../../models/tgeneral';
import { GLOBAL } from '../../../services/global';
import { CajaService} from '../caja.service'
import moment from 'moment';

@Component({
  selector: 'app-auditoria-divisas',
  templateUrl: './auditoria-divisas.component.html',
})
export class AuditoriaDivisasComponent implements OnInit {

  heading = 'Auditoría de Divisas';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  ngForm: FormGroup;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];
  dataGlobal: any = [];
  dataTotal: any = [];
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  ttUsuario: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  filter: CompositeFilterDescriptor;
  api: Array<string> = ['/reportes/caja/AudDivisas', 'dsReprecepcion', 'ttTgeneral'];
  loading = false;
  tblGeneral: TblGeneral;
  swal = GLOBAL.swal;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: CajaService,
    private _http: HttpClient,
    ) {}

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.crudTable();
    this.ngForm = this.createForm();
    this.allData = this.allData.bind(this);
  }

  crudTable(){
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.iInt1 = 1;
    this.tblGeneral.iInt3 = 1;
    this.tblGeneral.iInt4 = 2;
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).subtract(1, 'days').toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).toDate();
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }

  public buscarInfo(){
    this.loading = true;
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
    temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);
    this._https.putInfo(temp, this.api[0], this.api[1], this.api[2], GLOBAL.httpOptions).subscribe(data => {
      const result = data.siReprecepcion.dsReprecepcion;
      const resultExcel = data.siReprecepcion.dsReprecepcion.RespuestaExcel;
      if (!result.Divisas)  {
        this.dataGrid = [];
        this.dataGlobal = [];
        this.dataTotal = [];
      }else{

        this.dataGrid = result.Divisas;
        this.dataGlobal = result.Movimientos;
        this.dataTotal = result.ttTglobal[0];
      }
        this.loading = false;
      });
  }

  filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataFilter = filterBy(this.dataGrid, filter);
  }

  distinctPrimitive(fieldName: string) {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  public allData(): ExcelExportData {
    if (this.filter){
      const result: ExcelExportData =  {
        data: process(this.dataFilter, {sort: [{field: 'Fecha', dir: 'asc'}], filter: this.filter}).data
      };
        return result;
    }else{
      const result: ExcelExportData =  {
        data: process(this.dataGrid, {sort: [{field: 'Fecha', dir: 'asc'}], filter: this.filter}).data
      };
        return result;
    }
  }

  exportPdf() {
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(this.formatMoment);
    form.fFec2 = moment(form.fFec2).format(this.formatMoment);
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: '002-AUDIT-DIVISAS',
      dataGrid: this.dataGrid,
      dataGlobal: this.dataGlobal,
      dataTotal: this.dataTotal,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      hora: moment().format('HH:mm:ss'),
      form: form,
    }
    this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = this.heading + '.pdf';
        download.click();
      },
      error => {
        this.swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
      });
  }
}
