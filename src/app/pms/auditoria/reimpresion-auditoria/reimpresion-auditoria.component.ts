import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { Router } from '@angular/router';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { TblGeneral } from '../../../models/tgeneral';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-reimpresion-auditoria',
  templateUrl: './reimpresion-auditoria.component.html',
  styleUrls: ['./reimpresion-auditoria.component.css']
})
export class ReimpresionAuditoriaComponent implements OnInit {
  heading = 'Re-impresion de Reportes';
  subheading: string;
  icon: string;
  swal = GLOBAL.swal;
  loading: boolean;
  tblGeneral: TblGeneral;
  ttInfocia: any;
  formatKendo = localStorage.getItem('formatKendo');
  fday: Date;
  
  constructor(private _httpClient: HttpClient,
    private _infocia: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router) {
    this.ttInfocia = this._infocia.getSessionInfocia();
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.cChr1 = 'AN';
    this.fday = moment(this.ttInfocia.fday).add(-1, 'days').toDate();
    this.tblGeneral.fFec1 = this.fday;
    this.tblGeneral.iInt1 = 1;
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  async cargarArchivo() {
    this.loading = true;
    const tbl = JSON.parse(JSON.stringify(this.tblGeneral));
    tbl.fFec1 = moment(tbl.fFec1).format(GLOBAL.formatIso);
    const url = GLOBAL.url + '/auditoria/reprocesar/' + this.ttInfocia.propiedad + '®' + tbl.fFec1 + '®' + tbl.iInt1
    this._httpClient.get<any>(url, { responseType: 'json', reportProgress: true, observe: 'events' })
      .pipe(catchError(error => {
        this.swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
        this.loading = false;
        return throwError(error);
      }))
      .subscribe(async res => {
        if (res.type === HttpEventType.DownloadProgress) {
        }
        if (res.type === HttpEventType.Response) {
          const result = res.body.dsPA.ttRepProc;
          if (result) {
            await this.exportPdf(result[0]);
          } else {
            this.swal('Reporte no existe!', '', 'info');
          }
          this.loading = false;

        }
      });
  }
  exportPdf(reporte): Promise<boolean> {
    return new Promise((resolve1) => {
      this._httpClient.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', reporte)
        .pipe(catchError(error => {
          this.swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
          this.loading = false;
          resolve1(true);
          return throwError(error);
        })).subscribe(response => {
          resolve1(true);
          const linkSource = 'data:application/pdf;base64,' + response.base64;
          const download = document.createElement('a');
          download.href = linkSource;
          download.download = reporte.Propiedad + reporte.iSec + '.pdf';
          download.click();
        });
    });
  }
}
