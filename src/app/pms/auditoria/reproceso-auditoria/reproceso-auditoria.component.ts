import { HttpClient, HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { TblGeneral } from '../../../models/tgeneral';
import { GLOBAL } from '../../../services/global';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import moment from 'moment';
import swal from 'sweetalert2';

@Component({
  selector: 'app-reproceso-auditoria',
  templateUrl: './reproceso-auditoria.component.html',
  styleUrls: ['./reproceso-auditoria.component.css']
})
export class ReprocesoAuditoriaComponent implements OnInit {
  heading = 'Reproceso Cierre Diario';
  subheading: string;
  icon: string;
  tblGeneral: TblGeneral;
  ttInfocia: any;
  loading: boolean;
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  fday: Date;

  constructor(private _httpClient: HttpClient,
    private _infocia: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router) { }

  ngOnInit() {
    this.ttInfocia = this._infocia.getSessionInfocia();
    this.tblGeneral = new TblGeneral();
    this.fday = moment(this.ttInfocia.fday).add(-1, 'days').toDate();
    this.tblGeneral.fFec1 = this.fday;

    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  async iniciarAuditoria() {
    const fechaProc = moment(this.tblGeneral.fFec1).format(GLOBAL.formatIso);
    let lista = 'y';
    if (!this.tblGeneral.lLog1) {
      lista = 'no';
    }
    swal({
      title: '¿Esta seguro de continuar?', type: 'question',
      showCancelButton: true, confirmButtonText: 'Si', cancelButtonText: 'No',
    }).then((response) => {
      if (response.value) {
        this.loading = true;
        const api = GLOBAL.url + '/reportes/Auditoria/adicionales/Reproceso®' + fechaProc + GLOBAL.char174 + lista;
        this._httpClient.get<any>(api, { responseType: 'json', reportProgress: true, observe: 'events' })
          .subscribe(async res => {
            if (res.type === HttpEventType.DownloadProgress) {
            }
            if (res.type === HttpEventType.Response) {
              const result = res.body.response.siProcAud;
              if (result.dsMensajes.ttMensError !== undefined) {
                this.loading = false;
                return swal(result.dsMensajes.ttMensError[0].cMensTit, result.dsMensajes.ttMensError[0].cMensTxt);
              }
              this._infocia.notifyOther({ option: 'change-header', value: true });
              const data = result.dsProcAud.ttRepProc;
              if (data) {
                for (const item of data) {
                  await this.exportPdf(item);
                }
              }
              this.loading = false;
            }
          });
      }
    });
  }

  stopAuditoria() {
    this._httpClient.get<any>(GLOBAL.url + '/auditoria/nocturna/yes').subscribe(res => {
      const result = res.response.siTmpro.dsTmpro.ttTmppro;
      if (result !== undefined) {
        swal(result[0].tdesc, '', 'info');
      }
    });
  }
  exportPdf(reporte): Promise<boolean> {
    return new Promise((resolve1) => {
      this._httpClient.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', reporte)
        .pipe(catchError(error => {
          swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
          this.loading = false;
          resolve1(true);
          return throwError(error);
        })).subscribe(response => {
          resolve1(true);
          const linkSource = 'data:application/pdf;base64,' + response.base64;
          const download = document.createElement('a');
          download.href = linkSource;
          download.download = reporte.Propiedad + reporte.iSec + '.pdf';
          download.click();
        });
    });
  }
}
