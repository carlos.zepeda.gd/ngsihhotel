import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { GLOBAL } from '../../../services/global';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { CrudService } from '../../manttos/mantto-srv.services';
import moment from 'moment';
const swal = GLOBAL.swal;

@Component({
  selector: 'app-datos-estadisticos',
  templateUrl: './datos-estadisticos.component.html',
  styleUrls: ['./datos-estadisticos.component.css']
})
export class DatosEstadisticosComponent implements OnInit {
  heading = 'Datos Estadísticos';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  loading: boolean;
  ngForm: FormGroup;
  toast = GLOBAL.toast;
  formantKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  listMoneda = [];
  ttInfocia: any = [];
  fsys: Date;

  constructor(private _menuServ: ManttoMenuService,
    private _router: Router,
    private _crudservice: CrudService,
    private _formBuild: FormBuilder,
    private _info: InfociaGlobalService,
    private _http: HttpClient) {
    this.loading = true;
    this.ngForm = this.createFormGroup();
    this.ttInfocia = this._info.getSessionInfocia();
    this.fsys = moment(this.ttInfocia.fday).add(-1, 'd').toDate()

    this.ngForm.patchValue({
      cChr1: this.ttInfocia.Moneda,
      fFec1: this.fsys
    });

    this._crudservice.getData('/manttos/monedas').subscribe(res => {
      this.listMoneda = res.siMoneda.dsMonedas.ttMonedas;
      this.loading = false;
    });
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  createFormGroup() {
    return this._formBuild.group({
      cChr1: ['M'],
      cChr2: [''],
      cChr3: [''],
      cChr4: [''],
      cChr5: [''],
      cChr6: [''],
      cChr7: [''],
      cChr8: [''],
      cChr9: [''],
      cChr10: [''],
      cChr11: [''],
      iInt1: [2],
      iInt2: [5],
      iInt3: [1],
      iInt4: [0],
      iInt5: [0],
      iInt6: [0],
      iInt7: [0],
      iInt8: [0],
      iInt9: [0],
      iInt10: [0],
      iInt11: [0],
      lLog1: [false],
      lLog2: [false],
      lLog3: [false],
      lLog4: [false],
      lLog5: [false],
      lLog6: [false],
      lLog7: [false],
      lLog8: [false],
      lLog9: [false],
      lLog10: [false],
      lLog11: [false],
      dDec1: [0.0],
      dDec2: [0.0],
      dDec3: [0.0],
      dDec4: [0.0],
      dDec5: [0.0],
      dDec6: [0.0],
      dDec7: [0.0],
      dDec8: [0.0],
      dDec9: [0.0],
      dDec10: [0.0],
      dDec11: [0.0],
      fFec1: [null],
      fFec2: [null],
      fFec3: [null],
      fFec4: [null],
      fFec5: [null],
      fFec6: [null],
      fFec7: [null],
      fFec8: [null],
      fFec9: [null],
      fFec10: [null],
      fFec11: [null],
      zDtz1: [null],
      zDtz2: [null],
      zDtz3: [null],
      zDtz4: [null],
      zDtz5: [null],
      zDtz6: [null],
      zDtz7: [null],
      zDtz8: [null],
      zDtz9: [null],
      zDtz10: [null],
      zDtz11: [null],
    });
  }

  buscar() {
    this.loading = true;
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(GLOBAL.formatIso);
    const arrStr = '{"dsRepauditoria":{"ttTgeneral":[' + JSON.stringify(form) + ']}}';

    this._http.post<any>(GLOBAL.url + '/reportes/Auditoria/adicionales/DatosEstad', JSON.parse(arrStr)).subscribe(res => {
      const result = res.response;
      if (result.pc_arch) {
        swal('Error al generar reporte!!', result.pc_arch, 'error');
      } else {
        const fileExcel = result.dsEstadAgen.dsEstadAgen.bttResp[0];
        const linkSource = 'data:application/vnd.ms-excel;base64,' + fileExcel.lcRes;
        const download = document.createElement('a');
        const fileName = fileExcel.cArc;
        download.href = linkSource;
        download.download = fileName;
        download.click();
      }
      this.loading = false;
    });
  }
}
