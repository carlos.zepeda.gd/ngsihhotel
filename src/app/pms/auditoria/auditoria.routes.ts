import { RouterModule, Routes } from '@angular/router';
import { AuditoriaNocturnaComponent } from './auditoria-nocturna/auditoria-nocturna.component';
import { ReprocesoAuditoriaComponent } from './reproceso-auditoria/reproceso-auditoria.component';
import { ReimpresionAuditoriaComponent } from './reimpresion-auditoria/reimpresion-auditoria.component';
import { BaseLayoutComponent } from '../../_architect/Layout/base-layout/base-layout.component';
import { VerifyInfociaGuardService } from '../../services/verify-infocia-guard.service';
import { EntradasManualesComponent } from './entradas-manuales/entradas-manuales.component';
import { DatosEstadisticosComponent } from './datos-estadisticos/datos-estadisticos.component';
import { RoutesGuard } from '../../services/routes-guard.service';

const AUDITORIA_ROUTES: Routes = [
  {
    path: '',
    component: BaseLayoutComponent,
    children: [
      { path: 'auditoria-nocturna', component: AuditoriaNocturnaComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
      { path: 'reimpresion-auditoria', component: ReimpresionAuditoriaComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
      { path: 'reproceso-auditoria', component: ReprocesoAuditoriaComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
      { path: 'entradas-manuales', component: EntradasManualesComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
      { path: 'datos-estadisticos', component: DatosEstadisticosComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },

    ],
    canActivateChild: [
      VerifyInfociaGuardService
    ]
  }
];

export const AUDITORIA_ROUTING = RouterModule.forChild(AUDITORIA_ROUTES);
