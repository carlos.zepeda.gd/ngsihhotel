import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { GLOBAL } from '../../../services/global';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
// import * as dataResult from './auditoria.json';
import moment from 'moment';
import swal from 'sweetalert2';

@Component({
    selector: 'app-auditoria-nocturna',
    templateUrl: './auditoria-nocturna.component.html',
    styleUrls: ['./auditoria-nocturna.component.css'],
    providers: []
})
export class AuditoriaNocturnaComponent implements OnInit {
    heading = 'Auditoría Nocturna';
    subheading: string;
    icon: string;
    loading: boolean;
    dsEtiquetas: any;
    flagCancelar: boolean;
    ttInfocia: any;
    formatMoment: string = localStorage.getItem('formatMoment');
    fechaProc: string;
    toast = GLOBAL.toast;

    constructor(private _httpClient: HttpClient,
        private _menuServ: ManttoMenuService,
        private _info: InfociaGlobalService,
        private _router: Router) {
        this.ttInfocia = this._info.getSessionInfocia();
    }

    ngOnInit() {
        this.etiquetasAuditoria();
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    async iniciarAuditoria() {
        this.fechaProc = moment(this.ttInfocia.fday).format(this.formatMoment);
        swal({
            title: '¿Esta seguro de continuar con la auditoria?',
            text: 'Fecha a procesar ' + this.fechaProc, type: 'question',
            showCancelButton: true, confirmButtonText: 'Si', cancelButtonText: 'No',
        }).then((response) => {
            if (response.value) {
                this.loading = true;
                this._httpClient.get<any>(GLOBAL.url + '/auditoria/nocturna',
                    { responseType: 'json', reportProgress: true, observe: 'events' })
                    .subscribe(async res => {
                        if (res.type === HttpEventType.DownloadProgress) {
                            if (!this.flagCancelar) {
                                // await this.estatusAuditoria();
                            }
                        }
                        if (res.type === HttpEventType.Response) {
                            const result = res.body.response.siProcAud;
                            if (result.dsMensajes.ttMensError !== undefined) {
                                this.loading = false;
                                this.flagCancelar = false;
                                this.fechaProc = '';
                                return swal(result.dsMensajes.ttMensError[0].cMensTit, result.dsMensajes.ttMensError[0].cMensTxt);
                            }
                            this._info.notifyOther({ option: 'change-header', value: true });
                            const data = result.dsProcAud.ttRepProc;
                            if (data) {
                                for (const item of data) {
                                    await this.exportPdf(item);
                                }
                            }
                            this.fechaProc = '';
                            this.loading = false;
                        }
                    });
            } else {
                this.fechaProc = '';
            }
        });

        // const result = JSON.parse(JSON.stringify(dataResult));
        // const reportes = result.default.siProcAud.dsProcAud.ttRepProc;
        // for (const item of reportes) {
        //     await this.exportPdf(item);
        // }
    }
    exportPdf(reporte): Promise<boolean> {
        return new Promise((resolve1) => {
            this._httpClient.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', reporte)
                .pipe(catchError(error => {
                    swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
                    this.loading = false;
                    resolve1(true);
                    return throwError(error);
                })).subscribe(response => {
                    resolve1(true);
                    const linkSource = 'data:application/pdf;base64,' + response.base64;
                    const download = document.createElement('a');
                    download.href = linkSource;
                    download.download = reporte.Propiedad + reporte.iSec + '.pdf';
                    download.click();
                });
        });
    }

    stopAuditoria() {
        this.flagCancelar = true;
        this._httpClient.get<any>(GLOBAL.url + '/auditoria/nocturna/yes').subscribe(res => {
            const result = res.response.siTmpro.dsTmpro.ttTmppro;
            if (result !== undefined) {
                swal(result[0].tdesc, '', 'info');
            }
        });
    }
    estatusAuditoria(): Promise<boolean> {
        return new Promise(resolve2 => {
            this._httpClient.get<any>(GLOBAL.url + '/auditoria/nocturna/no').subscribe(res => {
                const result = res.response.siTmpro.dsTmpro.ttTmppro;
                if (result !== undefined) {
                    swal(result[0].tdesc, '', 'info');
                }
                resolve2(true);
            });
        });
    }
    etiquetasAuditoria() {
        this._httpClient.get<any>(GLOBAL.url + '/auditoria/nocturna/etiquetas',
            { responseType: 'json', reportProgress: true, observe: 'events' })
            .subscribe(async res => {
                if (res.type === HttpEventType.DownloadProgress) {
                }
                if (res.type === HttpEventType.Response) {
                    this.dsEtiquetas = res.body.response.siTmpro.dsTmpro.ttTmppro[0].cEtiq;
                }
            });
    }
}
