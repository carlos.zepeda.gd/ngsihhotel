import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {AUDITORIA_ROUTING} from './auditoria.routes';
import {AuditoriaService} from './auditoria.service';
import {KendouiModule} from 'app/shared/kendo.module';
import {SharedAppsModule} from 'app/shared/shared-apps.module';
import {ArchitectModule} from '../../_architect/architect.module';
import {AuditoriaNocturnaComponent} from './auditoria-nocturna/auditoria-nocturna.component';
import {EntradasManualesComponent} from './entradas-manuales/entradas-manuales.component';
import {DatosEstadisticosComponent} from './datos-estadisticos/datos-estadisticos.component';
import { ReprocesoAuditoriaComponent } from './reproceso-auditoria/reproceso-auditoria.component';
import { ReimpresionAuditoriaComponent } from './reimpresion-auditoria/reimpresion-auditoria.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    AUDITORIA_ROUTING,
    KendouiModule,
    SharedAppsModule,
    ArchitectModule
  ],
  declarations: [
    AuditoriaNocturnaComponent,
    EntradasManualesComponent,
    DatosEstadisticosComponent,
    ReprocesoAuditoriaComponent,
    ReimpresionAuditoriaComponent
  ],
  providers: [
    AuditoriaService
  ]
})
export class AuditoriaModule {
}
