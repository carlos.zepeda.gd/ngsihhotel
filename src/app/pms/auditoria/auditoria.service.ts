import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { GLOBAL } from 'app/services/global';
import 'rxjs/add/operator/map';
const headers = new Headers({ 'Content-Type': 'application/json' });
const esp = GLOBAL.char174;
const url = GLOBAL.url;
const urlPhp = GLOBAL.urlPhp;

@Injectable()

export class AuditoriaService {

  constructor(private http: Http) {
  }
  imprimirAuditoriaPdf(data) {
    return this.http.post(urlPhp + '/api/pdf/reportes', data, { headers: headers })
      .map((res: any) => res);
  }

  iniciarAuditoria() {
    return this.http.get(url + '/auditoria/nocturna')
      .map((res: Response) => {
        const result = res.json().response.siProcAud;
        if (result.dsMensajes.ttMensError !== undefined) {
          return result.dsMensajes.ttMensError[0];
        }
        return result.dsProcAud.ttRepProc;
      });
  }
  postCheckIn(huesped: any, typeCi: string) {
    let register = 'N';
    if (typeCi === 'cambio') { register = 'M'; typeCi = 'M'; }
    const json = JSON.stringify(huesped);
    const params = '{"dsHuesp":{"ttHuespedes":[' + json + ']}}';
    const service = `${register + esp + typeCi + esp + '' + esp + 'Prueba'}`

    return this.http.put(url + '/rec/huesped/' + service, params, { headers: headers })
      .map((res: Response) => {
        const success = res.json().response.siHuesp.dsHuesp.ttHuespedes;
        const error = res.json().response.siHuesp.dsMensajes.ttMensError;
        if (error) { return error; } else { return success; }
      });

  }
  public getEstatusHuesped(val) {
    return this.http.get(url + '/ayuda/status' + esp + val)
      .map((res: Response) => res.json().response.siAyuda.dsAyuda.ttHuespedes);
  }
  public getParametros(parametro) {
    return this.http.get(url + '/ayuda/parametros' + esp + parametro)
      .map(res => res.json().response.siAyuda.dsAyuda.ttParametros);
  }
  public llegadasHuespedes(data) {
    const json = JSON.stringify(data);
    const params = '{"dsReporte":{"ttTgeneral":[' + json + ']}}';

    return this.http.put(url + '/reportes/llegadas', params, { headers: headers })
      .map((res: Response) => {
        const r = res.json().response.siReporte
        if (r.dsMensajes.ttMensError !== undefined) {
          return r.dsMensajes.ttMensError[0];
        } else {
          return r.dsReporte;
        }
      });
  }
}
