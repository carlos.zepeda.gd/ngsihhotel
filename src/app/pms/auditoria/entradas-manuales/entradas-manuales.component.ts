import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { GLOBAL } from '../../../services/global';
import { CrudService } from '../../mantto-srv.services';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';

@Component({
  selector: 'app-entradas-manuales',
  templateUrl: './entradas-manuales.component.html',
  styleUrls: ['./entradas-manuales.component.css']
})
export class EntradasManualesComponent implements OnInit {
  heading = 'Entradas Manuales';
  subheading: string;
  icon: string;
  noRecords = GLOBAL.noRecords;
  textSuccess = GLOBAL.textSuccess;
  textError = GLOBAL.textError;
  api: Array<string> = ['/auditoria/entradasM', 'dsNmanual', 'ttNmanual'];
  api2: Array<string> = ['/auditoria/entradasD', 'dsNmandet', 'ttNmandet'];
  loading: boolean = true;
  dataGrid = [];
  ngForm: FormGroup;
  ngForm2: FormGroup;
  ngFormSelection = [];
  @ViewChild('first') _first: ElementRef;
  toast = GLOBAL.toast;
  flagNuevo: boolean;
  flagDetalle: boolean;
  dsCodigos: any = [];
  flagNuevoDetalle: boolean;
  entradaSelected: any;
  dataGridDet: any = [];
  ttUsuario: any;
  balance: number = 0.00;
  loadingColor: any = GLOBAL.loadingColor;

  constructor(private _menuServ: ManttoMenuService,
    private _info: InfociaGlobalService,
    private _router: Router,
    private _crudservice: CrudService,
    private _modalService: NgbModal,
    private _formBuild: FormBuilder) {
  }

  ngOnInit() {
    this.ttUsuario = this._info.getSessionUser();
    this.ngForm = this.createFormGroup();
    this.ngForm2 = this.createFormGroup2();
    this._crudservice.getData('/ayuda/codigos').subscribe(data => {
      this.dsCodigos = data.siAyuda.dsAyuda.ttCodigos;
    });
    this.getDataSource();

    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  getDataSource(e?) {
    this.flagNuevo = false;
    this._crudservice.getData(this.api[0]).subscribe(res => {
      const result = res.siNmanual.dsNmanual.ttNmanual;
      if (result) {
        this.dataGrid = result;
        if (e) {
          this.ngFormSelection.push(e);
          this.dataGrid.forEach(item => {
            if (item.nEent === e) {
              this.ngForm.patchValue(item);
            }
          });
        } else {
          this.ngFormSelection.push(this.dataGrid[0].nEent);
          this.ngForm.patchValue(this.dataGrid[0]);
        }
      } else {
        this.dataGrid = [];
      }
      this.loading = false;
    });
  }

  createFormGroup() {
    return this._formBuild.group({
      nEent: [0],
      nDesc: ['', [Validators.required]],
      nOpe: [''],
      cRowID: [''],
      lError: [false],
      cErrDes: [''],
    });
  }

  createFormGroup2() {
    return this._formBuild.group({
      nEent: [0],
      nSec: [0],
      Codigo: ['', [Validators.required]],
      nMonto: ['', [Validators.required]],
      nProp: [0],
      nTm: [''],
      nTipo: [''],
      balance: [0],
      cRowID: [''],
      lError: [false],
      cErrDes: [''],
    });
  }

  selectedRow({ selectedRows }) {
    if (selectedRows && selectedRows[0]) {
      this.flagNuevo = false;
      this.ngForm.patchValue(selectedRows[0].dataItem);
    }
  }

  selectedRowDet({ selectedRows }) {
    if (selectedRows && selectedRows[0]) {
      this.flagNuevo = false;
      this.ngForm2.patchValue(selectedRows[0].dataItem);
    }
  }

  nuevo() {
    this.ngForm.reset({ scercas: '', cErrDes: '', Inactivo: false, cRowID: '', lError: false });
    this._first.nativeElement.focus();
    this.flagNuevo = true;
    this.ngFormSelection = [];
  }

  guardar(form) {
    if (!form.cRowID) {
      this.flagNuevo = true;
    }
    form.nOpe = this.ttUsuario.cUserId;
    this.loading = true;
    if (!this.flagNuevo) {
      this._crudservice.putInfo(form, this.api[0], this.api[1], this.api[2]).subscribe(res => this.resultService(res));
    } else {
      this._crudservice.postInfo(form, this.api[0], this.api[1], this.api[2]).subscribe(res => this.resultService(res));
    }
  }

  eliminar(e) {
    this._crudservice.deleteData(e, this.api[0], this.api[2]).subscribe(res => this.resultService(res, true));
  }

  detalle(modal, dataItem) {
    this.entradaSelected = dataItem;
    this.getDataSourceDetalle();
    this._modalService.open(modal, { size: 'xl', centered: true });
  }

  getDataSourceDetalle(e?) {
    this.loading = true;
    this._crudservice.getData(this.api2[0] + '/' + this.entradaSelected.nEent).subscribe(data => {
      const result = data.siNmandet.dsNmandet.ttNmandet;
      if (result !== undefined) {
        this.dataGridDet = result;
        if (this.dataGridDet && this.dataGridDet[0].balance) {
          this.balance = this.dataGridDet[0].balance;
        } else {
          this.balance = 0;
        }
      } else {
        this.dataGridDet = [];
      }
      this.loading = false;
    });
  }

  nuevoDetalle() {
    this.flagNuevoDetalle = true;
    this.ngForm2.reset();
  }

  guardarDetalle(form) {
    if (!this.flagNuevoDetalle) {
      this._crudservice.putInfo(form, this.api2[0], this.api2[1], this.api2[2]).subscribe(res => this.resultServiceDetalle(res));
    } else {
      form.nEent = this.entradaSelected.nEent;
      form.nSec = this.dataGridDet.length + 1;
      this._crudservice.postInfo(form, this.api2[0], this.api2[1], this.api2[2]).subscribe(res => this.resultServiceDetalle(res));
    }
  }

  eliminarDetalle(e) {
    this._crudservice.deleteData(e, this.api2[0], this.api2[2]).subscribe(res => this.resultServiceDetalle(res, true));
  }

  resultServiceDetalle(res, eliminar?) {
    const result = res.siNmandet.dsNmandet.ttNmandet;
    if (result && result[0].cErrDesc) {
      this.toast({ type: 'error', title: this.textError, text: result[0].cErrDesc });
      this.loading = false;
    } else {
      if (!eliminar) {
        this.nuevoDetalle();
      }
      this.toast({ type: 'success', title: this.textSuccess });
      this.getDataSourceDetalle(this.ngForm.value.nEent);
    }
  }

  resultService(res, eliminar?) {
    const result = res.siNmanual.dsNmanual.ttNmanual;
    if (result === undefined) {
      this.getDataSource(this.ngForm.value.nEent);
      this.toast({ type: 'success', title: this.textSuccess });
    } else {
      if (result[0].cErrDesc !== undefined) {
        this.toast({ type: 'error', title: this.textError, text: result[0].cErrDesc });
        this.loading = false;
      }
    }
    if (eliminar) {
      this.ngForm.reset();
    }
  }
}
