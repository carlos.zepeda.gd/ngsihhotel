import { RouterModule, Routes } from '@angular/router';
import { BaseLayoutComponent } from '../../_architect/Layout/base-layout/base-layout.component';
import { VerifyInfociaGuardService } from '../../services/verify-infocia-guard.service';
import { TarificacionAreasComponent } from 'app/pms/telefonos/tarificacion/tarificacion-areas/tarificacion-areas.component';
import { TarificacionExtencionesComponent } from './tarificacion/tarificacion-extenciones/tarificacion-extenciones.component';
import { TarificacionClavesComponent } from './tarificacion/tarificacion-claves/tarificacion-claves.component';
import { TarificacionAccesosComponent } from './tarificacion/tarificacion-accesos/tarificacion-accesos.component';
import { TarificacionGruposComponent } from './tarificacion/tarificacion-grupos/tarificacion-grupos.component';
import { TarificacionTelUsualesComponent } from './tarificacion/tarificacion-tel-usuales/tarificacion-tel-usuales.component';
import { TarificacionDeptosComponent } from './tarificacion/tarificacion-deptos/tarificacion-deptos.component';
import { TarificacionClasifDirectorioComponent } from './tarificacion/tarificacion-clasif-directorio/tarificacion-clasif-directorio.component';
import { TarificacionTarifasLlamdasComponent } from './tarificacion/tarificacion-tarifas-llamdas/tarificacion-tarifas-llamdas.component';
import { TarificacionTarificadorManualComponent } from './tarificacion/tarificacion-tarificador-manual/tarificacion-tarificador-manual.component';
import { TarificacionCargosDiaComponent } from './tarificacion/tarificacion-cargos-dia/tarificacion-cargos-dia.component';
import { TarificacionCargosDirectosComponent } from './tarificacion/tarificacion-cargos-directos/tarificacion-cargos-directos.component';
import { ServicioDespertadorComponent } from './servicio-despertador/servicio-despertador.component';
import { CambiosDespertadorComponent } from './cambios-despertador/cambios-despertador.component';
import { EjecutaDespertadorComponent } from './ejecuta-despertador/ejecuta-despertador.component';
import { CosteoLlamadasComponent } from './reportes/costeo-llamadas/costeo-llamadas.component';
import { DiarioCargosDirComponent } from './reportes/diario-cargos-dir/diario-cargos-dir.component';
import { DiarioMensualLlamadasComponent } from './reportes/diario-mensual-llamadas/diario-mensual-llamadas.component';
import { DiarioMensualClavesComponent } from './reportes/diario-mensual-claves/diario-mensual-claves.component';
import { DiarioMensualCxldComponent } from './reportes/diario-mensual-cxld/diario-mensual-cxld.component';
import { HabilitarExtensionesComponent } from './habilitar-extensiones/habilitar-extensiones.component';
import { RoutesGuard } from '../../services/routes-guard.service';

const TELEFONOS_ROUTES: Routes = [
    {
        path: '',
        component: BaseLayoutComponent,
        children: [
            { path: 'tarificacion-areas', component: TarificacionAreasComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'tarificacion-extensiones', component: TarificacionExtencionesComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'tarificacion-claves', component: TarificacionClavesComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'tarificacion-accesos', component: TarificacionAccesosComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'tarificacion-grupos', component: TarificacionGruposComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'tarificacion-deptos', component: TarificacionDeptosComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'tarificacion-tel-usuales', component: TarificacionTelUsualesComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'tarificacion-clasif-dir', component: TarificacionClasifDirectorioComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'tarificacion-tarifa-llamada', component: TarificacionTarifasLlamdasComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'tarificacion-tarificador-man', component: TarificacionTarificadorManualComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'tarificacion-cargos', component: TarificacionCargosDiaComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'tarificacion-cargos-directos', component: TarificacionCargosDirectosComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'servicio-despertador', component: ServicioDespertadorComponent, canActivate: [RoutesGuard], data: { extraParameter: '', uso: 'H' } },
            { path: 'cambios-despertador', component: CambiosDespertadorComponent, canActivate: [RoutesGuard], data: { extraParameter: '', uso: 'H' } },
            { path: 'ejecutar-despertador', component: EjecutaDespertadorComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'costeo-llamadas', component: CosteoLlamadasComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'diario-cargos-dir', component: DiarioCargosDirComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'diario-llamadas-ext', component: DiarioMensualLlamadasComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'diario-llamadas-claves', component: DiarioMensualClavesComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'diario-llamadas-cxld', component: DiarioMensualCxldComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
            { path: 'habilitar-ext', component: HabilitarExtensionesComponent, canActivate: [RoutesGuard], data: { extraParameter: '' } },
        ],
        canActivateChild: [
            VerifyInfociaGuardService
        ]
    }
];
export const TELEFONOS_ROUTING = RouterModule.forChild(TELEFONOS_ROUTES);

