import { Component, Output, EventEmitter } from '@angular/core';
import { GLOBAL } from 'app/services/global';
import { CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';
import { CrudService } from 'app/pms/mantto-srv.services';
import { TarificacionService } from 'app/pms/telefonos/tarificacion.service'

@Component({
  selector: 'app-help-telefonos-codigo',
  templateUrl: './help-telefonos-codigo.component.html',
})
export class HelpTelefonosCodigoComponent {
  @Output() telefonosAreaCod: any = new EventEmitter;
  loading = false;
  pageSize = 10;
  skip = 0;
  closeResult: string;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];

  selectedRow: any;

  public filter: CompositeFilterDescriptor;
  public dataComplete: any = [];

  constructor(private _crud: CrudService,
    private _https: TarificacionService) {
    this.loading = true;
    this._https.getData('/ayuda/codigos®D®5®y®®N®').map(data => data).subscribe(data => {
      if (!data)  {
       console.log('Algo está mal');
      }else{
        this.dataGrid = data.siAyuda.dsAyuda.ttCodigos;
      }
        this.loading = false;
      });
  }

  public selectRow({ selectedRows }) {
    this.telefonosAreaCod.emit(selectedRows[0].dataItem);
  }

  public distinctPrimitive() {
    this.dataComplete = distinct(this.dataGrid, 'codigo');
  }


}
