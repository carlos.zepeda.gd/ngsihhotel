import { Component, Output, EventEmitter } from '@angular/core';
import { GLOBAL } from 'app/services/global';
import { CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';
import { CrudService } from 'app/pms/mantto-srv.services';

@Component({
  selector: 'app-help-cargos-directos',
  templateUrl: './help-cargos-directos.component.html',
})
export class HelpCargosDirectosComponent {
  @Output() telefonosCargosDir: any = new EventEmitter;

  loading = false;
  pageSize = 10;
  skip = 0;
  closeResult: string;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];

  selectedRow: any;

  public filter: CompositeFilterDescriptor;
  public dataComplete: any = [];

  constructor(private _crud: CrudService) {
    this.loading = true;
    this._crud.getData('/manttos/telefonos/directos').map(data => data).subscribe(data => {
      if (!data)  {
      }else{
        this.dataGrid = data.siXtelefono.dsXtelefonos.ttXcardir;
      }
        this.loading = false;
      });
  }

  public selectRow({ selectedRows }) {
    this.telefonosCargosDir.emit(selectedRows[0].dataItem);
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

}
