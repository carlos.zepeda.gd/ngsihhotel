import { Component, EventEmitter, Output } from '@angular/core';
import {GLOBAL} from '../../../../services/global';
import {CrudService} from '../../../manttos/mantto-srv.services';
import {CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';

@Component({
  selector: 'app-help-telefonos-extensiones',
  templateUrl: './help-telefonos-extensiones.component.html',
})
export class HelpTelefonosExtensionesComponent {
  @Output() telefonosExt: any = new EventEmitter;
  loading = false;
  pageSize = 10;
  skip = 0;
  closeResult: string;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];

  selectedRow: any;

  public filter: CompositeFilterDescriptor;
  public dataComplete: any = [];

  constructor(private _crud: CrudService) {
    this.loading = true;
    this._crud.getData('/manttos/telefonos/extensiones').map(data => data).subscribe(data => {
      if (!data)  {
      }else{
        this.dataGrid = data.siXtelefono.dsXtelefonos.ttXexten;
      }
        this.loading = false;
      });
  }

  public selectRow({ selectedRows }) {
    this.telefonosExt.emit(selectedRows[0].dataItem);
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

}
