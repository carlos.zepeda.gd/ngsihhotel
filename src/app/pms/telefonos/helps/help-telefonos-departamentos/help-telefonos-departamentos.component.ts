import { Component, Output, EventEmitter } from '@angular/core';
import { GLOBAL } from 'app/services/global';
import { CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';
import { CrudService } from 'app/pms/mantto-srv.services';

@Component({
  selector: 'app-help-telefonos-departamentos',
  templateUrl: './help-telefonos-departamentos.component.html',
})
export class HelpTelefonosDepartamentosComponent {
  @Output() telefonosDepto: any = new EventEmitter;
  loading = false;
  pageSize = 10;
  skip = 0;
  closeResult: string;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];

  selectedRow: any;

  public filter: CompositeFilterDescriptor;
  public dataComplete: any = [];

  constructor(private _crud: CrudService) {
    this.loading = true;
    this._crud.getData('/manttos/telefonos/deptos').map(data => data).subscribe(data => {
      if (!data)  {
      }else{
        this.dataGrid = data.siXtelefono.dsXtelefonos.ttXdeptos;
      }
        this.loading = false;
      });
  }

  public selectRow({ selectedRows }) {
    this.telefonosDepto.emit(selectedRows[0].dataItem);
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }


}
