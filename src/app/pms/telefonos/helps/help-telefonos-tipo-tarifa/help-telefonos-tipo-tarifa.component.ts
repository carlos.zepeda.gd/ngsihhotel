import { Component, Output, EventEmitter } from '@angular/core';
import { GLOBAL } from 'app/services/global';
import { CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';
import { CrudService } from 'app/pms/mantto-srv.services';

@Component({
  selector: 'app-help-telefonos-tipo-tarifa',
  templateUrl: './help-telefonos-tipo-tarifa.component.html',
})
export class HelpTelefonosTipoTarifaComponent {
  @Output() telefonosTTarifa: any = new EventEmitter;
  loading = false;
  pageSize = 10;
  skip = 0;
  closeResult: string;
  noRecords = GLOBAL.noRecords;
  dataGrid: any = [];

  selectedRow: any;

  public filter: CompositeFilterDescriptor;
  public dataComplete: any = [];

  constructor(private _crud: CrudService) {
    this.loading = true;
    this._crud.getData('/manttos/telefonos/tipos').map(data => data).subscribe(data => {
      if (!data)  {
       console.log('Algo está mal');
      }else{
        this.dataGrid = data.siXtelefono.dsXtelefonos.ttXtipos;
      }
        this.loading = false;
      });
  }

  public selectRow({ selectedRows }) {
    this.telefonosTTarifa.emit(selectedRows[0].dataItem);
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }


}
