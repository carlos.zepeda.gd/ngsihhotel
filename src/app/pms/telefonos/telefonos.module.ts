// Imports Angular
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// Modules
import {KendouiModule} from '../../shared/kendo.module';
import {SharedAppsModule} from '../../shared/shared-apps.module';
// Consultas
import {ArchitectModule} from '../../_architect/architect.module';
import {RouterModule} from '@angular/router';
import { TELEFONOS_ROUTING } from './telefonos.routes';
import { TarificacionAreasComponent } from 'app/pms/telefonos/tarificacion/tarificacion-areas/tarificacion-areas.component';
import { TarificacionService } from './tarificacion.service';
import { TarificacionExtencionesComponent } from './tarificacion/tarificacion-extenciones/tarificacion-extenciones.component';
import { HelpTelefonosExtensionesComponent } from './helps/help-telefonos-extensiones/help-telefonos-extensiones.component';
import { HelpTelefonosTarifasComponent } from './helps/help-telefonos-tarifas/help-telefonos-tarifas.component';
import { HelpTelefonosCodigoComponent } from './helps/help-telefonos-codigo/help-telefonos-codigo.component';
import { HelpTelefonosTipoLlamadaComponent } from './helps/help-telefonos-tipo-llamada/help-telefonos-tipo-llamada.component';
import { HelpTelefonosAccesosComponent } from './helps/help-telefonos-accesos/help-telefonos-accesos.component';
import { HelpTelefonosGruposComponent } from './helps/help-telefonos-grupos/help-telefonos-grupos.component';
import { HelpTelefonosDepartamentosComponent } from './helps/help-telefonos-departamentos/help-telefonos-departamentos.component';
import { HelpTelefonosTipoTarifaComponent } from './helps/help-telefonos-tipo-tarifa/help-telefonos-tipo-tarifa.component';
import { TarificacionClavesComponent } from './tarificacion/tarificacion-claves/tarificacion-claves.component';
import { TarificacionAccesosComponent } from './tarificacion/tarificacion-accesos/tarificacion-accesos.component';
import { TarificacionGruposComponent } from './tarificacion/tarificacion-grupos/tarificacion-grupos.component';
import { TarificacionDeptosComponent } from './tarificacion/tarificacion-deptos/tarificacion-deptos.component';
import { TarificacionTelUsualesComponent } from './tarificacion/tarificacion-tel-usuales/tarificacion-tel-usuales.component';
import { TarificacionClasifDirectorioComponent } from './tarificacion/tarificacion-clasif-directorio/tarificacion-clasif-directorio.component';
import { TarificacionTarifasLlamdasComponent } from './tarificacion/tarificacion-tarifas-llamdas/tarificacion-tarifas-llamdas.component';
import { TarificacionTarificadorManualComponent } from './tarificacion/tarificacion-tarificador-manual/tarificacion-tarificador-manual.component';
import { TarificacionCargosDiaComponent } from './tarificacion/tarificacion-cargos-dia/tarificacion-cargos-dia.component';
import { TarificacionCargosDirectosComponent } from './tarificacion/tarificacion-cargos-directos/tarificacion-cargos-directos.component';
import { ServicioDespertadorComponent } from './servicio-despertador/servicio-despertador.component';
import { CambiosDespertadorComponent } from './cambios-despertador/cambios-despertador.component';
import { EjecutaDespertadorComponent } from './ejecuta-despertador/ejecuta-despertador.component';
import { CosteoLlamadasComponent } from './reportes/costeo-llamadas/costeo-llamadas.component';
import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';
import { DiarioCargosDirComponent } from './reportes/diario-cargos-dir/diario-cargos-dir.component';
import { HelpCargosDirectosComponent } from './helps/help-cargos-directos/help-cargos-directos.component';
import { DiarioMensualLlamadasComponent } from './reportes/diario-mensual-llamadas/diario-mensual-llamadas.component';
import { DiarioMensualClavesComponent } from './reportes/diario-mensual-claves/diario-mensual-claves.component';
import { HelpTelefonosClavesComponent } from './helps/help-telefonos-claves/help-telefonos-claves.component';
import { DiarioMensualCxldComponent } from './reportes/diario-mensual-cxld/diario-mensual-cxld.component';
import { HabilitarExtensionesComponent } from './habilitar-extensiones/habilitar-extensiones.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TELEFONOS_ROUTING,
    KendouiModule,
    SharedAppsModule,
    RouterModule,
    ArchitectModule,
    GridModule,
    ExcelModule,
  ],
  declarations: [

  TarificacionAreasComponent,

  TarificacionExtencionesComponent,

  HelpTelefonosExtensionesComponent,

  HelpTelefonosTarifasComponent,

  HelpTelefonosCodigoComponent,

  HelpTelefonosTipoLlamadaComponent,

  HelpTelefonosAccesosComponent,

  HelpTelefonosGruposComponent,

  HelpTelefonosDepartamentosComponent,

  HelpTelefonosTipoTarifaComponent,

  TarificacionClavesComponent,

  TarificacionAccesosComponent,

  TarificacionGruposComponent,

  TarificacionDeptosComponent,

  TarificacionTelUsualesComponent,

  TarificacionClasifDirectorioComponent,

  TarificacionTarifasLlamdasComponent,

  TarificacionTarificadorManualComponent,

  TarificacionCargosDiaComponent,

  TarificacionCargosDirectosComponent,

  ServicioDespertadorComponent,

  CambiosDespertadorComponent,

  EjecutaDespertadorComponent,

  CosteoLlamadasComponent,

  DiarioCargosDirComponent,

  HelpCargosDirectosComponent,

  DiarioMensualLlamadasComponent,

  DiarioMensualClavesComponent,

  HelpTelefonosClavesComponent,

  DiarioMensualCxldComponent,

  HabilitarExtensionesComponent,


    ],
  providers: [
    TarificacionService,
  ],
  exports: [
    TarificacionAreasComponent,
    TarificacionExtencionesComponent,
    TarificacionClavesComponent,
    TarificacionAccesosComponent,
    TarificacionGruposComponent,
    TarificacionDeptosComponent,
    TarificacionTelUsualesComponent,
    TarificacionClasifDirectorioComponent,
    TarificacionTarifasLlamdasComponent,
    TarificacionTarificadorManualComponent,
    TarificacionCargosDiaComponent,
    TarificacionCargosDirectosComponent,
    ServicioDespertadorComponent,
    CambiosDespertadorComponent,
    EjecutaDespertadorComponent,
    CosteoLlamadasComponent,
    DiarioCargosDirComponent,
    DiarioMensualLlamadasComponent,
    DiarioMensualClavesComponent,
    DiarioMensualCxldComponent,
    HabilitarExtensionesComponent,

  ]
})
export class TelefonoModule {
}
