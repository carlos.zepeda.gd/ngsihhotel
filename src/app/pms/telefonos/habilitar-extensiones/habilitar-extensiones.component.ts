import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TarificacionService } from '../../telefonos/tarificacion.service'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../services/global';

@Component({
  selector: 'app-habilitar-extensiones',
  templateUrl: './habilitar-extensiones.component.html',
})
export class HabilitarExtensionesComponent implements OnInit {

  heading = 'Habilitar - Deshabilitar Extensiones';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  ngForm: FormGroup;
  modalTitle = '';
  modalType = '';
  closeResult: string;
  toast = GLOBAL.toast;

  constructor(
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: TarificacionService,
    private modal: NgbModal,
  ) {
    this.ngForm = this.createForm();

  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  createForm() {
    return this._formBuild.group({
      cChr1: '',
      cChr2: '',
      cChr3: 'E',
      cChr4: '',
      cChr5: '',
      cChr6: '',
      cChr7: '',
      cChr8: '',
      cChr9: '',
      cChr10: '',
      cChr11: '',
      iInt1: 0,
      iInt2: 0,
      iInt3: 0,
      iInt4: 0,
      iInt5: 0,
      iInt6: 0,
      iInt7: 0,
      iInt8: 0,
      iInt9: 0,
      iInt10: 0,
      iInt11: 0,
      lLog1: false,
      lLog2: false,
      lLog3: false,
      lLog4: false,
      lLog5: false,
      lLog6: false,
      lLog7: false,
      lLog8: false,
      lLog9: false,
      lLog10: false,
      lLog11: false,
      dDec1: 0.0,
      dDec2: 0.0,
      dDec3: 0.0,
      dDec4: 0.0,
      dDec5: 0.0,
      dDec6: 0.0,
      dDec7: 0.0,
      dDec8: 0.0,
      dDec9: 0.0,
      dDec10: 0.0,
      dDec11: 0.0,
      fFec1: null,
      fFec2: null,
      fFec3: null,
      fFec4: null,
      fFec5: null,
      fFec6: null,
      fFec7: null,
      fFec8: null,
      fFec9: null,
      fFec10: null,
      fFec11: null,
      zDtz1: null,
      zDtz2: null,
      zDtz3: null,
      zDtz4: null,
      zDtz5: null,
      zDtz6: null,
      zDtz7: null,
      zDtz8: null,
      zDtz9: null,
      zDtz10: null,
      zDtz11: null,



    });
  }

  openModal(content, type: string) {
    switch (type) {
      case 'allCrts':
        this.modalTitle = 'Cuartos';
        this.modalType = 'allCrts';
        break;
    }

    this.modal.open(content, { size: 'lg' });
  }

  getDataModal(data, type: string) {
    switch (type) {
      case 'allCrts':
        this.ngForm.patchValue({
          cChr2: data.cuarto,
        });
        break;
    }
  }

  checkIn(codigo: any) {
    if (!this.ngForm.value.lLog1) {
      this.ngForm.patchValue({
        cChr2: '',
      });
    }
    this.ngForm.patchValue({
      cChr1: codigo,
    });
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    this._https.postInfo(temp, '/reportes/telefonos/Extensiones', 'dsReptelefonos', 'ttTgeneral')
      .subscribe(data => {
        if (!data.siReptelefonos.dsMensajes.ttMensError) {
          if (this.ngForm.value.cChr1 === 'I') {
            this.toast({
              type: 'success',
              title: 'Check-In Realizado!!',
            });
          } else if (this.ngForm.value.cChr1 === 'A') {
            this.toast({
              type: 'success',
              title: 'Actualización Realizada!!',
            });
          } else if (this.ngForm.value.cChr1 === 'O') {
            this.toast({
              type: 'success',
              title: 'Check-Out Realizado!!',
            });
          } else if (this.ngForm.value.cChr1 === 'V') {
            this.toast({
              type: 'success',
              title: 'Check-Out Realizado!!',
            });
          }

        } else {
          const errorMesj = data.siReptelefonos.dsMensajes.ttMensError[0].cMensTxt;
          this.toast({ text: errorMesj.substr(0, 18) + ' - ' + errorMesj.substr(52, 20), type: 'error' });
        }
      });
  }

}
