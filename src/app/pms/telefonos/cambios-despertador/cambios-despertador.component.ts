import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { GLOBAL } from 'app/services/global';
import { InfociaGlobalService } from 'app/services/infocia.service';
import { Router } from '@angular/router';
import { TarificacionService } from 'app/pms/telefonos/tarificacion.service'
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from 'app/pms/manttos/mantto-menu/mantto-menu.service';

@Component({
  selector: 'app-cambios-despertador',
  templateUrl: './cambios-despertador.component.html',
})
export class CambiosDespertadorComponent implements OnInit {

  heading = 'Cambios a Despertador';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  noRecords = GLOBAL.noRecords;

  ngForm: FormGroup;
  dataGrid: any = [];
  filter: CompositeFilterDescriptor;
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  loading: boolean;
  modalTitle = '';
  modalType = '';
  closeResult: string;
  flagEdit: boolean;
  selectedItem: [];


  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private formBuild: FormBuilder,
    private _https: TarificacionService,
    private modal: NgbModal,
    ) { this.ngForm = this.createForm();
      this.ttInfocia = this._info.getSessionInfocia();
    }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  createForm() {
    return this.formBuild.group({
      NoFolio: ['', [Validators.required]],
      Nombre: [''],
      Razon: [''],
    });
  }

  openModal(content, type: string) {
    switch (type) {
      case 'fl':
        this.modalTitle = 'Huéspedes por Nombre';
        this.modalType = 'fl';
        break;
    }

    this.modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getDataModal(data, type: string){
    switch (type){
        case 'fl':
          this.ngForm.patchValue({
            NoFolio: data.hfolio,
            Nombre: data.hnombre + ' ' + data.hapellido,
          });

          break;
    }
  }



  public selectRow({ selectedRows }) {
    this.selectedItem = selectedRows[0].dataItem;
    this.flagEdit = true;
    this.ngForm.patchValue(this.selectedItem);
  }


  public getInfo(){
    this.loading = true;
    this._https.getData('/manttos/telefonos/CambiosDesp®' + this.ngForm.value.NoFolio).map(data => data)
    .subscribe(data => {
      const result = data.siXtelefono.dsXtelefonos.ttChuesDesp;
      if (!result)  {
        this.dataGrid = [];
      }else{
        this.dataGrid = result;
      }
        this.loading = false;
      });
  }


}
