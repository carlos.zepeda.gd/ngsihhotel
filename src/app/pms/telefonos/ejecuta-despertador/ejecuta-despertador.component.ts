import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { GLOBAL } from 'app/services/global';
import { InfociaGlobalService } from 'app/services/infocia.service';
import { Router } from '@angular/router';
import { TarificacionService } from 'app/pms/telefonos/tarificacion.service'
import swal from 'sweetalert2';
import { ManttoMenuService } from 'app/pms/manttos/mantto-menu/mantto-menu.service';

@Component({
  selector: 'app-ejecuta-despertador',
  templateUrl: './ejecuta-despertador.component.html',
})
export class EjecutaDespertadorComponent implements OnInit {

  heading = 'Despertador';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  noRecords = GLOBAL.noRecords;

  ngForm: FormGroup;
  dataGrid: any = [];
  filter: CompositeFilterDescriptor;
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  loading: boolean;
  selectedItem: [];
  Razon: '';
  flagEdit = true;
  toast = GLOBAL.toast;


  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private formBuild: FormBuilder,
    private _https: TarificacionService,
    ) { this.ngForm = this.createForm();
      this.ttInfocia = this._info.getSessionInfocia();
      this.getInfo();

    }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  createForm() {
    return this.formBuild.group({
      Folio: [''],
      FechaDespertador: [''],
      Hora: [''],
      cRowID: [''],
    });
  }


  public selectRow({ selectedRows }) {
    this.flagEdit = false;
    this.selectedItem = selectedRows[0].dataItem;
    this.ngForm.patchValue(
      this.selectedItem
    );
  }


  public getInfo(){
    this.loading = true;
    this._https.getData('/manttos/telefonos/EjecDesp®,S').map(data => data).subscribe(data => {
      const result = data.siXtelefono.dsXtelefonos.Despertador;
      if (!result)  {
        this.dataGrid = [];
      }else{
        if (result){
        this.dataGrid = result;
        }
      }
        this.loading = false;
      });
  }

  public ejecutarDesp(){
    this._https.putInfo(this.ngForm.value, '/manttos/telefonos/EjecDesp®E', 'dsXtelefonos', 'Despertador', GLOBAL.httpOptions)
    .subscribe(data => {
      const result = data.siXtelefono.dsXtelefonos.Despertador;
      if (result){
        if (result[0].cErrDesc === '' || result[0].cErrDesc == null){
          this.toast({
            position: 'center',
            type: 'success',
            title: 'Despertador Ejecutado!!',
          });
        }else {
          const errorMesj = result[0].cErrDesc;
            swal('ERROR!', errorMesj, 'error');
        }
      }else {
          this.toast({
            type: 'success',
            title: 'Despertador Ejecutado!!',
          });
      }
      this.dataGrid = [];
      this.getInfo();
      this.flagEdit = false;
    });

  }


  public cancelar(){
    if (!this.Razon){
      swal({
        position: 'center',
        type: 'error',
        title: 'Razón obligatoria!!!',
        showConfirmButton: false,
        timer: 2000
      });
    }else {
    this._https.putInfo(this.ngForm.value, '/manttos/telefonos/EjecDesp®C,' + this.Razon, 'dsXtelefonos', 'Despertador', GLOBAL.httpOptions)
      .subscribe(data => {
        const result = data.siXtelefono.dsXtelefonos.Despertador
      if (result){
        if (result[0].cErrDesc === '' || result[0].cErrDesc == null){
          this.toast({
            type: 'success',
            title: 'Despertador Cancelado!!',
          });

        }else {
          const errorMesj = result[0].cErrDesc;
            swal('ERROR!', errorMesj, 'error');
        }
      }else {

        this.toast({
          type: 'success',
          title: 'Despertador Cancelado!!',
        });

    }
        this.dataGrid = [];
        this.getInfo();
        this.flagEdit = false;
      });
      this.Razon = '';
    }

  }

}
