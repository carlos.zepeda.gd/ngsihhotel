import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { TarificacionService } from '../../../telefonos/tarificacion.service'
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../../services/global';

@Component({
  selector: 'app-tarificacion-extenciones',
  templateUrl: './tarificacion-extenciones.component.html'
})
export class TarificacionExtencionesComponent implements OnInit {
  heading = 'Extensiones';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  api: Array<string> = ['/manttos/telefonos/extensiones', 'siXtelefono', 'dsXtelefonos', 'ttXexten'];
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  filter: CompositeFilterDescriptor;
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  loading: boolean;
  modalTitle = '';
  modalType = '';
  closeResult: string;
  flagEdit: boolean;
  selectedItem: any = [];
  toast = GLOBAL.toast;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: TarificacionService,
    private _modal: NgbModal,
  ) {
    this.loading = true;
    this.ngForm = this.createForm();
    this.ttInfocia = this._info.getSessionInfocia();
    this.getInfo();
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  createForm() {
    return this._formBuild.group({
      CarMultUsos: [[]],
      Depto: [0, [Validators.required]],
      Descripcion: ['', [Validators.required, Validators.maxLength(30)]],
      Desviacion: [0],
      Grupo: [0, [Validators.required]],
      LogMultUsos: [[]],
      NoFolio: [0, [Validators.max(9999999)]],
      Tipo: [''],
      cRowID: [''],
      cuarto: [''],
      exten: [0, [Validators.max(99999), Validators.required]],
      lError: [false]
    });
  }

  openModal(content, type: string) {
    switch (type) {
      case 'telefonosDepto':
        this.modalTitle = 'Departamentos';
        this.modalType = 'telefonosDepto';
        break;
      case 'telefonosGrupos':
        this.modalTitle = 'Grupos';
        this.modalType = 'telefonosGrupos';
        break;
      case 'allCrts':
        this.modalTitle = 'Cuartos';
        this.modalType = 'allCrts';
        break;
      case 'telefonosTTarifa':
        this.modalTitle = 'Tipos de Tarificación';
        this.modalType = 'telefonosTTarifa';
        break;
      case 'telefonosExt':
        this.modalTitle = 'Selección de Extensiones';
        this.modalType = 'telefonosExt';
        break;
    }

    this._modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getDataModal(data, type: string) {
    switch (type) {
      case 'telefonosGrupos':
        this.ngForm.patchValue({ Grupo: data.Grupo });
        break;
      case 'telefonosDepto':
        this.ngForm.patchValue({ Depto: data.Depto });
        break;
      case 'allCrts':
        this.ngForm.patchValue({ cuarto: data.cuarto });
        break;
      case 'telefonosTTarifa':
        this.ngForm.patchValue({ Tipo: data.Tipo });
        break;
      case 'telefonosExt':
        this.ngForm.patchValue({ Desviacion: data.Desviacion });
        break;
    }
  }

  public selectRow({ selectedRows }) {
    this.flagEdit = true;
    this.selectedItem = selectedRows[0].dataItem;
    this.ngForm.patchValue(this.selectedItem);
  }

  public newRow() {
    this.flagEdit = false;
    this.ngForm = this.createForm();
  }

  private getInfo() {
    this._https.getData(this.api[0]).subscribe(data => {
      const res = data[this.api[1]][this.api[2]][this.api[3]];
      if (!res) {
        this.dataGrid = [];
      } else {
        this.dataGrid = res;
      }
      this.loading = false;
    });
  }

  public guardar() {
    this.flagEdit ? this.crudService('edit') : this.crudService('new');
  }

  private crudService(method) {
    this.loading = true;
    let result: any;
    switch (method) {
      case 'new':
        result = this._https.postInfo(this.ngForm.value, this.api[0], this.api[2], this.api[3]);
        break;
      case 'edit':
        result = this._https.putInfo(this.ngForm.value, this.api[0], this.api[2], this.api[3], GLOBAL.httpOptions);
        break;
    }
    result.subscribe(data => {
      const res = data[this.api[1]][this.api[2]][this.api[3]];
      if (res && res[0].cErrDesc) {
        this.toast(GLOBAL.textError, res[0].cErrDesc, 'error');
      } else {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.flagEdit = true;
        this.newRow();
        this.getInfo();
      }
      this.loading = false;
    });
  }


  eliminar() {
    this.loading = true;
    const data = '{"dsXtelefonos":{"ttXexten":[' + JSON.stringify(this.selectedItem) + ']}}';
    this._https.delete(data, 'extensiones').subscribe(res => {
      const result = res.response[this.api[1]][this.api[2]][this.api[3]];
      if (result && result[0].cErrDesc) {
        this.toast(GLOBAL.textError, result[0].cErrDesc, 'error');
      } else {
        this.toast({ type: 'success', title: GLOBAL.textDeleteSuccess });
        this.getInfo();
        this.newRow();
      }
      this.loading = false;
    })
  }


}
