import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { TarificacionService } from '../../../telefonos/tarificacion.service'
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { GLOBAL } from '../../../../services/global';

@Component({
  selector: 'app-tarificacion-tel-usuales',
  templateUrl: './tarificacion-tel-usuales.component.html'
})
export class TarificacionTelUsualesComponent implements OnInit {

  heading = 'Directorio Telefónico';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  api: Array<string> = ['/manttos/telefonos/telefonos', 'siXtelefono', 'dsXtelefonos', 'ttXtelef'];
  toast = GLOBAL.toast;
  formatKendo = localStorage.getItem('formatKendo');
  filter: CompositeFilterDescriptor;
  noRecords = GLOBAL.noRecords;
  skip = 0;
  pageSize = 10;
  ngForm: FormGroup;
  dataGrid: any = [];
  ttInfocia: any = [];
  selectedItem: [];
  listClasif: [];
  listTipos: [];
  loading: boolean;
  flagEdit: boolean;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: TarificacionService
  ) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.getInfo();
    this.getDataSource();
    this.newRow();
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  createForm() {
    return this._formBuild.group({
      telef: ['', [Validators.required, Validators.maxLength(18)]],
      Descripcion: ['', [Validators.maxLength(40)]],
      Clasif: ['', [Validators.required]],
      Tipo: ['', [Validators.required]],
      cRowID: [''],
      lError: [false],
      cErrDesc: ['']
    });
  }

  getDataModal(data) {
    this.ngForm.patchValue({ Tipo: data.Tipo });
  }

  public selectRow({ selectedRows }) {
    if (selectedRows && selectedRows[0].dataItem) {
      this.selectedItem = selectedRows[0].dataItem;
      this.ngForm.patchValue(this.selectedItem);
      this.flagEdit = true;
    }
  }

  public newRow() {
    this.flagEdit = false;
    this.ngForm = this.createForm();
  }

  private getInfo() {
    this.loading = true;
    this._https.getData(this.api[0]).subscribe(data => {
      const result = data[this.api[1]][this.api[2]][this.api[3]];
      !result ? this.dataGrid = [] : this.dataGrid = result;
      this.loading = false;
    });
  }

  private getDataSource() {
    this._https.getData('/manttos/telefonos/clasificacion').subscribe(data => {
      if (data) {
        this.listClasif = data[this.api[1]][this.api[2]].ttXclasif;
      }
    });
    this._https.getData('/manttos/telefonos/tipos').subscribe(data => {
      if (data) {
        this.listTipos = data[this.api[1]][this.api[2]].ttXtipos;
      }
    });
  }

  public guardar() {
    this.flagEdit ? this.crudService('edit') : this.crudService('new');
  }

  private crudService(method) {
    this.loading = true;
    let response: any;
    switch (method) {
      case 'new':
        response = this._https.postInfo(this.ngForm.value, this.api[0], this.api[2], this.api[3]);
        break;
      case 'edit':
        response = this._https.putInfo(this.ngForm.value, this.api[0], this.api[2], this.api[3], GLOBAL.httpOptions);
        break;
    }
    response.subscribe(data => {
      const result = data[this.api[1]][this.api[2]][this.api[3]];
      if (result && result[0].cErrDesc) {
        this.toast(GLOBAL.textError, result[0].cErrDesc, 'error');
      } else {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.getInfo()
        this.newRow();
      }
      this.loading = false;
    });
  }

  public eliminar() {
    this.loading = true;
    const data = '{"' + this.api[2] + '":{"' + this.api[3] + '":[' + JSON.stringify(this.selectedItem) + ']}}';
    this._https.delete(data, 'telefonos').subscribe(res => {
      const result = res.response[this.api[1]][this.api[2]][this.api[3]];
      if (result && result[0].cErrDesc) {
        this.toast(GLOBAL.textError, result[0].cErrDesc, 'error');
      } else {
        this.toast(GLOBAL.textDeleteSuccess, '', 'success');
        this.getInfo()
        this.newRow();
      }
      this.loading = false;
    })
  }
}
