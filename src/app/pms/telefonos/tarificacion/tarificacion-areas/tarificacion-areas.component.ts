
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { TarificacionService } from '../../../telefonos/tarificacion.service'
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../../services/global';

@Component({
  selector: 'app-tarificacion-areas',
  templateUrl: './tarificacion-areas.component.html'
})
export class TarificacionAreasComponent implements OnInit {
  heading = 'Áreas';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  api: Array<string> = ['/manttos/telefonos/areas', 'siXtelefono', 'dsXtelefonos', 'ttXareas'];
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  filter: CompositeFilterDescriptor;
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  loading: boolean;
  modalTitle = '';
  modalType = '';
  closeResult: string;
  AccesoSearch: [];
  selectedItem: [];
  flagEdit: boolean;
  toast = GLOBAL.toast;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: TarificacionService,
    private _modal: NgbModal,
  ) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.newRow();
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  createForm() {
    return this._formBuild.group({
      Acceso: ['', [Validators.maxLength(10), Validators.required]],
      Area: ['', [Validators.maxLength(5), Validators.required]],
      Descripcion: [''],
      TL: ['', [Validators.maxLength(1), Validators.required]],
      Tarifa: [, [Validators.maxLength(2)]],
      cErrDesc: [''],
      cRowID: [''],
      codigo: ['', [Validators.maxLength(3), Validators.required]],
      lError: [false],
      localiza: ['']
    });
  }

  openModal(content, type: string) {
    switch (type) {
      case 'telefonosTarifas':
        this.modalTitle = 'Tarifas';
        this.modalType = 'telefonosTarifas';
        break;
      case 'telefonosAreaCod':
        this.modalTitle = 'Código de Áreas';
        this.modalType = 'telefonosAreaCod';
        break;
      case 'telefonosTL':
        this.modalTitle = 'Tipo de Llamada';
        this.modalType = 'telefonosTL';
        break;
      case 'telefonosAccesos':
        this.modalTitle = 'Accesos';
        this.modalType = 'telefonosAccesos';
        break;
    }

    this._modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getDataModal(data, type: string) {
    switch (type) {
      case 'telefonosTarifas':
        this.ngForm.patchValue({ Tarifa: data.Tarifa });
        break;
      case 'telefonosAreaCod':
        this.ngForm.patchValue({ codigo: data.Codigo });
        break;
      case 'telefonosTL':
        this.ngForm.patchValue({ TL: data.TL });
        break;
      case 'telefonosAccesos':
        this.AccesoSearch = data.Acceso;
        this.ngForm.patchValue({ Acceso: data.Acceso });
        break;
    }
  }

  buscarAcceso(acceso) {
    this.loading = true;
    this._https.getData(this.api[0] + '®' + acceso).subscribe(data => {
      const res = data[this.api[1]][this.api[2]][this.api[3]];
      if (res) {
        this.dataGrid = res;
      } else {
        this.dataGrid = [];
      }
      this.loading = false;
    });
  }

  public selectRow({ selectedRows }) {
    if (selectedRows && selectedRows[0].dataItem) {
      this.selectedItem = selectedRows[0].dataItem;
      this.ngForm.patchValue(this.selectedItem);
      this.flagEdit = true;
    }
  }

  public newRow() {
    this.flagEdit = false;
    this.ngForm = this.createForm();
  }

  public guardar() {
    (this.flagEdit) ? this.crudService('edit') : this.crudService('new');
  }

  private crudService(method) {
    this.loading = true;
    let responseService: any;

    switch (method) {
      case 'new':
        responseService = this._https.postInfo(this.ngForm.value, this.api[0], this.api[2], this.api[3]);
        break;
      case 'edit':
        responseService = this._https.putInfo(this.ngForm.value, this.api[0], this.api[2], this.api[3], GLOBAL.httpOptions);
        break;
    }
    responseService.subscribe(data => {
      const res = data[this.api[1]][this.api[2]][this.api[3]];
      if (res && res[0].cErrDesc) {
        this.toast(GLOBAL.textError, res[0].cErrDesc, 'error');
      } else {
        if (res && res[0].cRowID) {
          this.toast({ type: 'success', title: GLOBAL.textSuccess });
          this.buscarAcceso(this.ngForm.value.Acceso);
          this.newRow();
        }
      }
      this.loading = false;
    });
  }

  public eliminar() {
    this.loading = true;
    const data = '{"dsXtelefonos":{"ttXareas":[' + JSON.stringify(this.selectedItem) + ']}}';
    this._https.delete(data, 'areas').subscribe(res => {
      const result = res.response[this.api[1]][this.api[2]][this.api[3]];
      if (result && result[0].cErrDesc) {
        this.toast(GLOBAL.textError, result[0].cErrDesc, 'error');
      } else {
        this.toast(GLOBAL.textDeleteSuccess, '', 'success');
        this.buscarAcceso(this.ngForm.value.Acceso);
        this.newRow();
      }
      this.loading = false;
    })
  }
}
