import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { TarificacionService } from '../../../telefonos/tarificacion.service'
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../../services/global';

@Component({
  selector: 'app-tarificacion-clasif-directorio',
  templateUrl: './tarificacion-clasif-directorio.component.html'
})
export class TarificacionClasifDirectorioComponent implements OnInit {

  heading = 'Clasificación de Directorio';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  api: Array<string> = ['/manttos/telefonos/clasificacion', 'siXtelefono', 'dsXtelefonos', 'ttXclasif'];
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  loading: boolean;
  flagEdit: boolean;
  selectedItem: [];
  toast = GLOBAL.toast;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: TarificacionService,
  ) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.getInfo();
    this.newRow();
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  createForm() {
    return this._formBuild.group({
      Descripcion: ['', [Validators.required, Validators.maxLength(40)]],
      Clasif: ['', [Validators.required, Validators.maxLength(6)]],
      cRowID: [''],
    });
  }

  public selectRow({ selectedRows }) {
    if (selectedRows && selectedRows[0].dataItem) {
      this.selectedItem = selectedRows[0].dataItem;
      this.ngForm.patchValue(this.selectedItem);
      this.flagEdit = true;
    }
  }

  public newRow() {
    this.flagEdit = false;
    this.ngForm = this.createForm();
  }

  private getInfo() {
    this.loading = true;
    this._https.getData(this.api[0]).subscribe(data => {
      const result = data[this.api[1]][this.api[2]][this.api[3]];
      !result ? this.dataGrid = [] : this.dataGrid = result;
      this.loading = false;
    });
  }

  public guardar() {
    this.flagEdit ? this.crudService('edit') : this.crudService('new');
  }

  private crudService(method) {
    this.loading = true;
    let response: any;
    switch (method) {
      case 'new':
        response = this._https.postInfo(this.ngForm.value, this.api[0], this.api[2], this.api[3]);
        break;
      case 'edit':
        response = this._https.putInfo(this.ngForm.value, this.api[0], this.api[2], this.api[3], GLOBAL.httpOptions);
        break;
    }
    response.subscribe(data => {
      const result = data[this.api[1]][this.api[2]][this.api[3]];
      if (result && result[0].cErrDesc) {
        this.toast(GLOBAL.textError, result[0].cErrDesc, 'error');
      } else {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.getInfo()
        this.newRow();
      }
      this.loading = false;
    });
  }

  public eliminar() {
    this.loading = true;
    const data = '{"' + this.api[2] + '":{"' + this.api[3] + '":[' + JSON.stringify(this.selectedItem) + ']}}';
    this._https.delete(data, 'clasificacion').subscribe(res => {
      const result = res.response[this.api[1]][this.api[2]][this.api[3]];
      if (result && result[0].cErrDesc) {
        this.toast(GLOBAL.textError, result[0].cErrDesc, 'error');
      } else {
        this.toast(GLOBAL.textDeleteSuccess, '', 'success');
        this.getInfo()
        this.newRow();
      }
      this.loading = false;
    })
  }

}
