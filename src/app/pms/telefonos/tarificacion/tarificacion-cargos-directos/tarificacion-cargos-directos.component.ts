import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { TarificacionService } from '../../../telefonos/tarificacion.service'
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../../services/global';

@Component({
  selector: 'app-tarificacion-cargos-directos',
  templateUrl: './tarificacion-cargos-directos.component.html'
})
export class TarificacionCargosDirectosComponent implements OnInit {

  heading = 'Cargos Directos';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  api: Array<string> = ['/manttos/telefonos/directos', 'siXtelefono', 'dsXtelefonos', 'ttXcardir'];
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  filter: CompositeFilterDescriptor;
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  loading: boolean;
  modalType = '';
  closeResult: string;
  flagEdit: boolean;
  selectedItem: [];
  listMoneda: [];
  listTipos: [];
  toast = GLOBAL.toast;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: TarificacionService,
    private _modal: NgbModal,
  ) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.newRow();
    this.getInfo();
    this.getDataSource();
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  createForm() {
    return this._formBuild.group({
      Telefono: ['', [Validators.required, Validators.maxLength(18)]],
      Descripcion: ['', [Validators.maxLength(30)]],
      Costo: ['', [Validators.required, Validators.maxLength(6)]],
      CostoxMinuto: [false],
      Moneda: ['', [Validators.required]],
      TarificaDesde: ['', [Validators.maxLength(2)]],
      TL: ['', [Validators.required]],
      codigo: ['', [Validators.required]],
      CargoSinImptos: [[0, 0, 0, 0, 0, 0]],
      TiempoMinimo: [[0, 0, 0, 0, 0, 0]],
      ExcedSinImptos: [[0, 0, 0, 0, 0, 0]],
      ExcedxMinuto: [[false, false, false, false, false, false]],
      cRowID: [''],
    });
  }

  openModal(modal) {
    this._modal.open(modal, { size: 'lg', centered: true });
  }

  getDataModal(data) {
        this.ngForm.patchValue({ codigo: data.Codigo });
  }

  public selectRow({ selectedRows }) {
    if (selectedRows && selectedRows[0].dataItem) {
      this.selectedItem = selectedRows[0].dataItem;
      this.ngForm.patchValue(this.selectedItem);
      this.flagEdit = true;
    }
  }

  public newRow() {
    this.flagEdit = false;
    this.ngForm = this.createForm();
  }

  private getInfo() {
    this.loading = true;
    this._https.getData(this.api[0]).subscribe(data => {
      const result = data[this.api[1]][this.api[2]][this.api[3]];
      !result ? this.dataGrid = [] : this.dataGrid = result;
      this.loading = false;
    });
  }

  public guardar() {
    this.flagEdit ? this.crudService('edit') : this.crudService('new');
  }

  private crudService(method) {
    this.loading = true;
    let response: any;
    switch (method) {
      case 'new':
        response = this._https.postInfo(this.ngForm.value, this.api[0], this.api[2], this.api[3]);
        break;
      case 'edit':
        response = this._https.putInfo(this.ngForm.value, this.api[0], this.api[2], this.api[3], GLOBAL.httpOptions);
        break;
    }
    response.subscribe(data => {
      const result = data[this.api[1]][this.api[2]][this.api[3]];
      if (result && result[0].cErrDesc) {
        this.toast(GLOBAL.textError, result[0].cErrDesc, 'error');
      } else {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.getInfo()
        if (method === 'new') {
          this.newRow();
        }
      }
      this.loading = false;
    });
  }

  public eliminar() {
    this.loading = true;
    const data = '{"' + this.api[2] + '":{"' + this.api[3] + '":[' + JSON.stringify(this.selectedItem) + ']}}';
    this._https.delete(data, 'directos').subscribe(res => {
      const result = res.response[this.api[1]][this.api[2]][this.api[3]];
      if (result && result[0].cErrDesc) {
        this.toast(GLOBAL.textError, result[0].cErrDesc, 'error');
      } else {
        this.toast(GLOBAL.textDeleteSuccess, '', 'success');
        this.getInfo()
        this.newRow();
      }
      this.loading = false;
    });
  }

  getDataSource() {
    this._https.getData('/manttos/monedas').map(data => data).subscribe(data => {
      if (data) {
        this.listMoneda = data.siMoneda.dsMonedas.ttMonedas;
      }
    });
    this._https.getData('/manttos/telefonos/TL').map(data => data).subscribe(data => {
      if (data) {
        this.listTipos = data.siXtelefono.dsXtelefonos.ttXtls;
      }
    });
  }
}
