import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { TarificacionService } from '../../../telefonos/tarificacion.service'
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../../services/global';

@Component({
  selector: 'app-tarificacion-accesos',
  templateUrl: './tarificacion-accesos.component.html'
})
export class TarificacionAccesosComponent implements OnInit {

  heading = 'Accesos';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  api: any = ['/manttos/telefonos/accesos', 'siXtelefono', 'dsXtelefonos', 'ttXaccesos'];
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  filter: CompositeFilterDescriptor;
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  loading: boolean;
  modalTitle = '';
  modalType = '';
  closeResult: string;
  flagEdit: boolean;
  selectedItem: any = [];
  toast = GLOBAL.toast;


  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: TarificacionService,
    private _modal: NgbModal,
  ) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.getInfo();
    this.newRow();
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  createForm() {
    return this._formBuild.group({
      Acceso: [null, [Validators.maxLength(10), Validators.required]],
      Descripcion: ['', [Validators.maxLength(30)]],
      codigo: ['', [Validators.required]],
      TL: ['', [Validators.required]],
      cRowID: [],
    });
  }

  openModal(content, type: string) {
    switch (type) {
      case 'telefonosAreaCod':
        this.modalTitle = 'Códigos de Áreas';
        this.modalType = 'telefonosAreaCod';
        break;

      case 'telefonosTL':
        this.modalTitle = 'Tipos de Llamada';
        this.modalType = 'telefonosTL';
        break;
    }

    this._modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getDataModal(data, type: string) {
    switch (type) {
      case 'telefonosAreaCod':
        this.ngForm.patchValue({ codigo: data.Codigo });
        break;
      case 'telefonosTL':
        this.ngForm.patchValue({ TL: data.TL });
        break;
    }
  }

  public selectRow({ selectedRows }) {
    this.selectedItem = selectedRows[0].dataItem;
    this.flagEdit = true;
    this.ngForm.patchValue(this.selectedItem);
  }

  public newRow() {
    this.flagEdit = false;
    this.ngForm = this.createForm();
  }

  private getInfo() {
    this.loading = true;
    this._https.getData('/manttos/telefonos/accesos®').map(data => data).subscribe(data => {
      if (!data) {
        console.log('Algo está mal');
        this.dataGrid = [];
      } else {
        this.dataGrid = data[this.api[1]][this.api[2]][this.api[3]];
      }
      this.loading = false;
    });
  }

  public guardar() {
    this.flagEdit ? this.crudService('edit') : this.crudService('new');
  }

  crudService(method) {
    this.loading = true;
    let result: any;

    switch (method) {
      case 'new':
        result = this._https.postInfo(this.ngForm.value, this.api[0], this.api[2], this.api[3]);
        break;
      case 'edit':
        result = this._https.putInfo(this.ngForm.value, this.api[0], this.api[2], this.api[3], GLOBAL.httpOptions);
        break;
    }
    result.subscribe(data => {
      const res = data[this.api[1]][this.api[2]][this.api[3]];
      if (res && res[0].cErrDesc) {
        this.toast(GLOBAL.textError, res[0].cErrDesc, 'error');
      } else {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.flagEdit = true;
        this.getInfo();
      }
      this.loading = false;
    });
  }

  eliminar() {
    this.loading = true;
    const data = '{"dsXtelefonos":{"ttXaccesos":[' + JSON.stringify(this.selectedItem) + ']}}';
    this._https.delete(data, 'accesos').subscribe(res => {
      const result = res.response[this.api[1]][this.api[2]][this.api[3]];
      if (result && result[0].cErrDesc) {
        this.toast(GLOBAL.textError, result[0].cErrDesc, 'error');
      } else {
        this.toast(GLOBAL.textDeleteSuccess, '', 'success');
        this.getInfo();
        this.newRow();
      }
      this.loading = false;
    });
  }
}
