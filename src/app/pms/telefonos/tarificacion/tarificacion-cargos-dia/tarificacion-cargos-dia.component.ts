import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { TarificacionService } from '../../../telefonos/tarificacion.service'
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-tarificacion-cargos-dia',
  templateUrl: './tarificacion-cargos-dia.component.html'
})
export class TarificacionCargosDiaComponent implements OnInit {

  heading = 'Cargos del Día';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  filter: CompositeFilterDescriptor;
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  loading: boolean;
  modalTitle = '';
  modalType = '';
  flagEdit: boolean;
  selectedItem: any = [];
  toast = GLOBAL.toast;
  hour: Date = new Date(2020, 12, 9, 0, 0, 0);

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: TarificacionService,
  ) {
    this.loading = true;
    this.ttInfocia = this._info.getSessionInfocia();
    this.ngForm = this.createForm();
    this.getInfo();
  }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  createForm() {
    return this._formBuild.group({
      cRowID: [''],
      NoCargo: [],
      exten: ['', [Validators.required]],
      Hora: ['', [Validators.required]],
      Minutos: ['', [Validators.required, Validators.max(999999)]],
      Telefono: [''],
      Llamada: [''],
      SM: [],
      Costo: [],
      Cargo: [],
      Notas: [''],
      TL: [''],
      Tipo: ['', [Validators.required]],
      Clave: [],
      Impuestos: [],
      Total: [],
      Origen: [''],
      Cxld: false,
      Fecha: moment(this.ttInfocia.fday).toDate(),
    });
  }


  public selectRow({ selectedRows }) {
    this.flagEdit = true;
    this.selectedItem = selectedRows[0].dataItem;
    this.ngForm.patchValue(this.selectedItem);
    const hr = this.ngForm.value.Hora.substring(0, 2);
    const mn = this.ngForm.value.Hora.substring(5, 3);
    this.hour = new Date(2020, 1, 1, hr, mn, 0);
  }

  public update() {
    const time = this.hour.toTimeString().substring(0, 5);
    this.ngForm.patchValue({
      Hora: time,
    });
    if (!this.ngForm.value.Cxld) {
      this.ngForm.patchValue({ Notas: '' });
    }
    this._https.putInfo(this.ngForm.value, '/manttos/telefonos/cargos', 'dsXtelefonos', 'ttXcargos', GLOBAL.httpOptions)
      .subscribe(data => {
        const res = data.siXtelefono.dsXtelefonos.ttXcargos;
        if (res && res[0].cErrDesc) {
          this.toast(GLOBAL.textDeleteSuccess, res[0].cErrDesc, 'error');
        } else {
          this.toast({ type: 'success', title: 'Registro guardado!!' });
          this.dataGrid = [];
          this.getInfo();
        }
      });
  }

  private getInfo() {
    this._https.getData('/manttos/telefonos/cargos').subscribe(data => {
      const res = data.siXtelefono.dsXtelefonos.ttXcargos;
      if (res) {
        this.dataGrid = data.siXtelefono.dsXtelefonos.ttXcargos;
      } else {
        this.dataGrid = [];
      }
      this.loading = false;
    });
  }


}
