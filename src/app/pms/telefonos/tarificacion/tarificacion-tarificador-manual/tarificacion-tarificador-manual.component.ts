import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { TarificacionService } from '../../../telefonos/tarificacion.service'
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { GLOBAL } from '../../../../services/global';
import swal from 'sweetalert2';
import moment from 'moment';

@Component({
  selector: 'app-tarificacion-tarificador-manual',
  templateUrl: './tarificacion-tarificador-manual.component.html'
})
export class TarificacionTarificadorManualComponent implements OnInit {

  heading = 'Tarificador Manual';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  filter: CompositeFilterDescriptor;
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  modalTitle = '';
  modalType = '';
  closeResult: string;
  flagEdit: boolean;
  selectedItem: [];
  maskHora = '00:00';
  flagCalculo: boolean;
  flagCargo: boolean;
  toast = GLOBAL.toast;
  hour: Date = new Date(2020, 12, 9, 0, 0, 0);

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: TarificacionService,
    private _modal: NgbModal,
  ) { }

  ngOnInit() {
    this.ttInfocia = this._info.getSessionInfocia();
    this.newRow();
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  createForm() {
    return this._formBuild.group({
      cRowID: [''],
      NoCargo: [],
      exten: ['', [Validators.required]],
      Hora: [0, [Validators.required]],
      Minutos: [0, [Validators.required, Validators.max(999999)]],
      Telefono: [''],
      Llamada: [''],
      Origen: [''],
      SM: [],
      Costo: [],
      Cargo: [],
      Cxld: false,
      Notas: [''],
      TL: [''],
      Tipo: ['', [Validators.required]],
      Clave: [],
      Impuestos: [],
      Total: [],
      Fecha: [moment(this.ttInfocia.fday).format(GLOBAL.formatIso)],

    });
  }

  openModal(content, type: string) {
    switch (type) {
      case 'telefonosExt':
        this.modalTitle = 'Selección de Extensiones';
        this.modalType = 'telefonosExt';
        break;
      case 'telefonosTTarifa':
        this.modalTitle = 'Tipos de Tarificación';
        this.modalType = 'telefonosTTarifa';
        break;
    }

    this._modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getDataModal(data, type: string) {
    switch (type) {
      case 'telefonosExt':
        this.ngForm.patchValue({ exten: data.exten });
        break;
      case 'telefonosTTarifa':
        this.ngForm.patchValue({ Tipo: data.Tipo });
        if (data.Tipo === 'A' || this.ngForm.value.Tipo) {
          this.flagCargo = true;
        } else {
          this.flagCargo = false;
        }
        break;
    }
  }

  public newRow() {
    this.flagEdit = false;
    this.ngForm = this.createForm();
    this.hour = new Date(2020, 1, 1, 0, 0, 0);
  }

  public changeHora(){
    const time = this.hour.toTimeString().substring(0, 5);
    this.ngForm.patchValue({
      Hora: time
    })
  }

  public saveRow() {
    const time = this.hour.toTimeString().substring(0, 5);
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.Hora = time;
    this._https.postInfo(temp, '/manttos/telefonos/cargos', 'dsXtelefonos', 'ttXcargos').subscribe(data => {
      const res = data.siXtelefono.dsXtelefonos.ttXcargos;
      if (res && res[0].cErrDesc) {
        this.toast(GLOBAL.textError, res[0].cErrDesc, 'error');
      } else {
        this.toast({ type: 'success', title: GLOBAL.textSuccess });
        this.newRow();
        this.flagCalculo = false;
      }
    });
  }

  public calcular() {
    this.ngForm.patchValue({
      Notas: '',
      Cargo: 0,
      Cxld: false,
      TL: 'M',
      Clave: 0,
    });

    this._https.putInfo(this.ngForm.value, '/ayuda/telefonos/CalcCargos', 'dsXtelefonos', 'ttXcargos', GLOBAL.httpOptions)
      .subscribe(data => {
        const res = data.siXtelefono.dsXtelefonos.ttXcargos;
        if (res && !res[0].cErrDesc) {
          this.ngForm.patchValue(res[0]);
        } else {
          this.toast(GLOBAL.textError, res[0].cErrDesc, 'error');
        }
      });
    this.flagCalculo = true;
  }
}
