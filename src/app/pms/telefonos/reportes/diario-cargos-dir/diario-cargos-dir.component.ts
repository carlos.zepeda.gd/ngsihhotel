import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { TarificacionService } from '../../../telefonos/tarificacion.service'
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { TblGeneral } from '../../../../models/tgeneral';
import { GLOBAL } from '../../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-diario-cargos-dir',
  templateUrl: './diario-cargos-dir.component.html',
})
export class DiarioCargosDirComponent implements OnInit {

  heading = 'Cargos Directos';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  dataGlobal: any = [];
  filter: CompositeFilterDescriptor;
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  ttUsuario: any = [];
  ttInfocia: any = [];
  formatMoment = localStorage.getItem('formatMoment');
  formatKendo = localStorage.getItem('formatKendo');
  maxDateDesde: Date;
  minDateHasta: Date;
  loading: boolean;
  modalTitle = '';
  modalType = '';
  flag = false;
  base64 = '';
  base = '';
  tblGeneral: TblGeneral;
  toast = GLOBAL.toast;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _http: HttpClient,
    private _https: TarificacionService,
    private _modal: NgbModal,
  ) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.crudTable();
    this.minDateHasta = new Date(moment(this.ttInfocia.fday).toDate());
    this.maxDateDesde = new Date(moment(this.ttInfocia.fday).subtract(1, 'day').toDate());
    this.ngForm = this.createForm();
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  crudTable() {
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).subtract(1, 'days').toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).toDate();
    this.tblGeneral.iInt1 = 1;
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }

  openModal(content, type: string) {
    switch (type) {
      case 'telefonosCargosDir':
        this.modalTitle = 'Selección de # de Teléfono';
        this.modalType = 'telefonosCargosDir';
        break;
    }

    this._modal.open(content, { size: 'lg' });
  }

  getDataModal(data, type: string) {
    switch (type) {
      case 'telefonosCargosDir':
        this.ngForm.patchValue({
          cChr1: data.Telefono,
          cChr2: data.Descripcion,
        });

        break;
    }
  }

  public getInfo() {
    this.loading = true;
    this.flag = true;

    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
    temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);

    this._https.putInfo(temp, '/reportes/telefonos/directos', 'dsReptelefonos', 'ttTgeneral', GLOBAL.httpOptions)
      .subscribe(data => {
        const result = data.siReptelefono.dsReptelefonos;
        const resultExcel = data.siReptelefono.dsEstadAgen.RespuestaExcel;
        if (!result.TotalGlobal) {
          this.dataGrid = [];
          this.dataGlobal = [];
        } else {
          // tslint:disable-next-line:triple-equals
          if (this.ngForm.value.iInt1 == 1) {
            this.dataGrid = result.TotalExtension;
            this.dataGlobal = result.Total;
            this.base64 = resultExcel[0].Archivo;
            this.base = resultExcel;
            this.dataGlobal.pop();
            this.dataGlobal.pop();

            // tslint:disable-next-line:triple-equals
          } else if (this.ngForm.value.iInt1 == 2) {
            this.dataGrid = result.TotalGlobal;
            this.dataGlobal = result.Total;
            this.base64 = resultExcel[0].Archivo;
            this.base = resultExcel;

          }
        }
        this.loading = false;
      });
  }

  public exportExcel() {
    if (this.base) {
      this._http.get<any>(GLOBAL.url + '/reportes/excel/' + this.base64)
        .subscribe(res => {
          const result = res.response.siEnvioExcel.dsEnvioExcel.RespuestaExcel[0]
          const fileExcel = result.ArchivoExportado;
          const linkSource = 'data:application/vnd.ms-excel;base64,' + fileExcel;
          const download = document.createElement('a');
          download.href = linkSource;
          download.download = result.Archivo;
          download.click();

          this.loading = false;
        });
    }
  }

  exportPdf() {
    if (this.ngForm.value.iInt1 === 1) {
      const form = JSON.parse(JSON.stringify(this.ngForm.value));
      form.fFec1 = moment(form.fFec1).format(this.formatMoment);
      form.fFec2 = moment(form.fFec2).format(this.formatMoment);
      const json = {
        Propiedad: this.ttInfocia.propiedad,
        iSec: '006-CARGOS-DIR-det',
        dataGrid: this.dataGrid,
        infocia: this.ttInfocia,
        usuario: this.ttUsuario,
        hora: moment().format('HH:mm:ss'),
        form: form,
        total: this.dataGlobal,
        global: this.dataGlobal[this.dataGlobal.length - 2],
      }
      this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
        response => {
          const linkSource = 'data:application/pdf;base64,' + response.base64;
          const download = document.createElement('a');
          download.href = linkSource;
          download.download = this.heading + '.pdf';
          download.click();
        },
        error => {
          this.toast(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
        }
      );
    } else {
      const form = JSON.parse(JSON.stringify(this.ngForm.value));
      form.fFec1 = moment(form.fFec1).format(this.formatMoment);
      form.fFec2 = moment(form.fFec2).format(this.formatMoment);
      const json = {
        Propiedad: this.ttInfocia.propiedad,
        iSec: '006-CARGOS-DIR-glob',
        dataGrid: this.dataGrid,
        infocia: this.ttInfocia,
        usuario: this.ttUsuario,
        hora: moment().format('HH:mm:ss'),
        form: form,
        total: this.dataGlobal,
        global: this.dataGlobal[0],
      }
      this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
        response => {
          const linkSource = 'data:application/pdf;base64,' + response.base64;
          const download = document.createElement('a');
          download.href = linkSource;
          download.download = this.heading + '.pdf';
          download.click();
        },
        error => {
          this.toast(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
        });
    }

  }

  public changeFlag() {
    this.dataGrid = '';
    this.dataGlobal = '';
    this.flag = false;
  }

  fechaChange(num) {
    if (num === 1) {
      this.minDateHasta = new Date(moment(this.ngForm.value.fFec1).add(1, 'day').toDate());
    } else {
      this.maxDateDesde = new Date(moment(this.ngForm.value.fFec2).subtract(1, 'day').toDate());
    }
  }
}
