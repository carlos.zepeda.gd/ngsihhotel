import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../../services/infocia.service';
import { TarificacionService } from '../../../telefonos/tarificacion.service'
import { CrudService } from '../../../mantto-srv.services';
import { TblGeneral } from '../../../../models/tgeneral';
import { GLOBAL } from '../../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-diario-mensual-claves',
  templateUrl: './diario-mensual-claves.component.html',
})
export class DiarioMensualClavesComponent implements OnInit {

  heading = 'Diario-Mensual de Llamadas por Claves';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  dataGlobal: any = [];
  dataTotal: any = [];
  filter: CompositeFilterDescriptor;
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  ttUsuario: any = [];
  loading: boolean;
  modalTitle = '';
  modalType = '';
  base64 = '';
  base = '';
  formatMoment = localStorage.getItem('formatMoment');
  formatKendo = localStorage.getItem('formatKendo');
  toast = GLOBAL.toast;
  Descripcion = '';
  listTipoLlamada = [];
  listTipoTarifa = [];
  dsListLlamadas = [];
  dsListTarifas = [];
  TipoLlam: [];
  TipoTarif: [];
  flagVisible = false;
  flag = false;
  tblGeneral: TblGeneral;
  maxDateDesde: Date;
  minDateHasta: Date;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _crud: CrudService,
    private _http: HttpClient,
    private _https: TarificacionService,
    private modal: NgbModal,
  ) {
    this.ttUsuario = this._info.getSessionUser();
    this.ttInfocia = this._info.getSessionInfocia();
    this.crudTable();
    this.ngForm = this.createForm();
    this._crud.getData('/manttos/telefonos/TL').map(data => data).subscribe(data => {
      if (!data) {
      } else {
        this.dsListLlamadas = data.siXtelefono.dsXtelefonos.ttXtls;
        this.dsListLlamadas.forEach(item => item.Descripcion = item.Descripcion);

      }
      this.loading = false;
    });
    this._crud.getData('/manttos/telefonos/tarifas').map(data => data).subscribe(data => {
      if (!data) {
      } else {
        this.dsListTarifas = data.siXtelefono.dsXtelefonos.ttXtarifas;
        this.dsListTarifas.forEach(item => item.Descripcion = item.Descripcion);

      }
      this.loading = false;
    });

  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  crudTable() {
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).subtract(1, 'days').toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).toDate();
    this.tblGeneral.iInt4 = 1;
    this.tblGeneral.cChr1 = 'C'
  }

  createForm() {
    return this._formBuild.group(this.tblGeneral);
  }

  openModal(content, type: string) {
    switch (type) {
      case 'telefonosClaves':
        this.modalTitle = 'Selección de Claves';
        this.modalType = 'telefonosClaves';
        break;
      case 'telefonosTL':
        this.modalTitle = 'Tipos de Llamadas';
        this.modalType = 'telefonosTL';
        break;
      case 'telefonosTarifas':
        this.modalTitle = 'Tipos de Tarifas';
        this.modalType = 'telefonosTarifas';
        break;
    }

    this.modal.open(content, { size: 'lg' });
  }


  getDataModal(data, type: string) {
    switch (type) {
      case 'telefonosClaves':
        this.ngForm.patchValue({
          iInt1: data.Clave,
          iInt2: 0,
          iInt3: 0,
        });
        this.Descripcion = data.Descripcion
        if (this.ngForm.value.iInt1 !== 0) {
          this.flagVisible = false;
          this.ngForm.patchValue({
            iInt4: 1,
          });
        } else {
          this.flagVisible = true;
        }
        break;
      case 'telefonosTL':
        this.listTipoLlamada.push(data.Descripcion);
        this.TipoLlam = JSON.parse(JSON.stringify(this.listTipoLlamada));
        break;
      case 'telefonosTarifas':
        this.listTipoTarifa.push(data.Descripcion);
        this.TipoTarif = JSON.parse(JSON.stringify(this.listTipoTarifa));
        break;
    }
  }

  public getInfo() {
    // tslint:disable-next-line:triple-equals
    if (this.ngForm.value.iInt1 == 0 && this.ngForm.value.iInt1 == 0 && this.ngForm.value.iInt1 == 0) {
      this.Descripcion = '';
    }
    // tslint:disable-next-line:triple-equals
    if (this.ngForm.value.iInt1 != 0 && this.ngForm.value.iInt4 == 2) {
      this.ngForm.patchValue({
        iInt4: 1,
      });
    }
    this.loading = true;
    this.flag = true;

    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
    temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);

    this._https.putInfo(temp, '/reportes/telefonos/diario', 'dsReptelefonos', 'ttTgeneral', GLOBAL.httpOptions)
      .subscribe(data => {
        const result = data.siReptelefono.dsReptelefonos;
        const resultExcel = data.siReptelefono.dsEstadAgen.RespuestaExcel
        if (!result.Total) {
          this.dataGrid = [];
          this.dataGlobal = [];
        } else {
          if (result.Extensiones) {
            this.dataGrid = result.Extensiones;
            this.dataTotal = result.Total;
            if (result.Secuencia) {
              this.dataGlobal = result.Secuencia;
            }
          }
          if (result.TotalGlobal) {
            this.dataGrid = result.TotalGlobal;
            this.dataTotal = result.Total;
            if (result.Secuencia) {
              this.dataGlobal = result.Secuencia;
            }
          }
          this.base64 = resultExcel[0].Archivo;
          this.base = resultExcel;
        }
        this.loading = false;
      });
  }

  public exportExcel() {
    if (this.base) {
      const fileName = this.base64;
      this._http.get<any>(GLOBAL.url + '/reportes/excel/' + fileName)
        .subscribe(res => {
          const result = res.response.siEnvioExcel.dsEnvioExcel.RespuestaExcel[0]
          const fileExcel = result.ArchivoExportado;
          const linkSource = 'data:application/vnd.ms-excel;base64,' + fileExcel;
          const download = document.createElement('a');
          // tslint:disable-next-line: no-shadowed-variable
          const fileName = result.Archivo;
          download.href = linkSource;
          download.download = fileName;
          download.click();

          this.loading = false;
        });
    }
  }


  tipoLlamadaChange(data) {
    this.listTipoLlamada = data;
  }

  tipoTarifaChange(data) {
    this.listTipoTarifa = data;
  }

  public changeFlag() {
    this.dataGrid = [];
    this.dataGlobal = [];
    this.flag = false;
  }

  exportPdf() {
    this.loading = true;
    // tslint:disable-next-line:triple-equals
    if (this.ngForm.value.iInt4 == 1) {
      const form = JSON.parse(JSON.stringify(this.ngForm.value));
      form.fFec1 = moment(form.fFec1).format(this.formatMoment);
      form.fFec2 = moment(form.fFec2).format(this.formatMoment);
      if (!this.dataGlobal) {
        const json = {
          Propiedad: this.ttInfocia.propiedad,
          iSec: '006-LLAM-CLA-det',
          dataGrid: this.dataGrid,
          infocia: this.ttInfocia,
          usuario: this.ttUsuario,
          hora: moment().format('HH:mm:ss'),
          form: form,
          global: [],
          total: this.dataTotal,
        }
        this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
          response => {
            const linkSource = 'data:application/pdf;base64,' + response.base64;
            const download = document.createElement('a');
            download.href = linkSource;
            download.download = this.heading + '.pdf';
            download.click();
          },
          error => {
            this.toast(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
          });
      } else {
        const json = {
          Propiedad: this.ttInfocia.propiedad,
          iSec: '006-LLAM-CLA-det',
          dataGrid: this.dataGrid,
          infocia: this.ttInfocia,
          usuario: this.ttUsuario,
          hora: moment().format('HH:mm:ss'),
          form: form,
          global: this.dataGlobal,
          total: this.dataTotal,
        }
        this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
          response => {
            const linkSource = 'data:application/pdf;base64,' + response.base64;
            const download = document.createElement('a');
            download.href = linkSource;
            download.download = this.heading + '.pdf';
            download.click();
          },
          error => {
            this.toast(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
          });
      }


    } else {
      const form = JSON.parse(JSON.stringify(this.ngForm.value));
      form.fFec1 = moment(form.fFec1).format(this.formatMoment);
      form.fFec2 = moment(form.fFec2).format(this.formatMoment);
      if (!this.dataGlobal) {
        const json = {
          Propiedad: this.ttInfocia.propiedad,
          iSec: '006-LLAM-CLA-glob',
          dataGrid: this.dataGrid,
          infocia: this.ttInfocia,
          usuario: this.ttUsuario,
          hora: moment().format('HH:mm:ss'),
          form: form,
          global: [],
          total: this.dataTotal,
        }
        this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
          response => {
            const linkSource = 'data:application/pdf;base64,' + response.base64;
            const download = document.createElement('a');
            download.href = linkSource;
            download.download = this.heading + '.pdf';
            download.click();
          },
          error => {
            this.toast(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
          });
      } else {
        const json = {
          Propiedad: this.ttInfocia.propiedad,
          iSec: '006-LLAM-CLA-glob',
          dataGrid: this.dataGrid,
          infocia: this.ttInfocia,
          usuario: this.ttUsuario,
          hora: moment().format('HH:mm:ss'),
          form: form,
          global: this.dataGlobal,
          total: this.dataTotal,
        }
        this._http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
          response => {
            const linkSource = 'data:application/pdf;base64,' + response.base64;
            const download = document.createElement('a');
            download.href = linkSource;
            download.download = this.heading + '.pdf';
            download.click();
          },
          error => {
            this.toast(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
          });
      }

    }
    this.loading = false;
  }

  fechaChange(num) {
    if (num === 1) {
      this.minDateHasta = new Date(moment(this.ngForm.value.fFec1).add(1, 'day').toDate());
    } else {
      this.maxDateDesde = new Date(moment(this.ngForm.value.fFec2).subtract(1, 'day').toDate());
    }
  }

}
