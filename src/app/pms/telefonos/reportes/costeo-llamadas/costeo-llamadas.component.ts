import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GLOBAL } from 'app/services/global';
import { InfociaGlobalService } from 'app/services/infocia.service';
import { Router } from '@angular/router';
import moment from 'moment';
import { TarificacionService } from 'app/pms/telefonos/tarificacion.service'
import { ManttoMenuService } from 'app/pms/manttos/mantto-menu/mantto-menu.service';
import { HttpClient } from '@angular/common/http';
import { TblGeneral } from 'app/models/tgeneral';

@Component({
  selector: 'app-costeo-llamadas',
  templateUrl: './costeo-llamadas.component.html',
})
export class CosteoLlamadasComponent implements OnInit {

  heading = 'Costeo de Llamadas';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  noRecords = GLOBAL.noRecords;

  ngForm: FormGroup;
  dataGrid: any = [];
  dataGlobal: any = [];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  ttUsuario: any = [];
  formatMoment = localStorage.getItem('formatMoment');
  formatKendo = localStorage.getItem('formatKendo');
  base64 = '';
  base = '';
  loading: boolean;
  tblGeneral: TblGeneral;
  maxDateDesde: Date;
  minDateHasta: Date;
  toast = GLOBAL.toast;

  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private formBuild: FormBuilder,
    private _https: TarificacionService,
    private http: HttpClient,
    ) {
      this.ttUsuario = this._info.getSessionUser();
      this.ttInfocia = this._info.getSessionInfocia();
      this.crudTable();
      this.minDateHasta = new Date(moment(this.ttInfocia.fday).toDate());
      this.maxDateDesde = new Date(moment(this.ttInfocia.fday).subtract(1, 'day').toDate());
      this.ngForm = this.createForm();
    }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  crudTable(){
    this.tblGeneral = new TblGeneral();
    this.tblGeneral.fFec1 = moment(this.ttInfocia.fday).subtract(1, 'days').toDate();
    this.tblGeneral.fFec2 = moment(this.ttInfocia.fday).toDate();
  }

  createForm() {
    return this.formBuild.group(this.tblGeneral);
  }

  public getInfo(){
    this.loading = true;

    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.fFec1 = moment(temp.fFec1).format(GLOBAL.formatIso);
    temp.fFec2 = moment(temp.fFec2).format(GLOBAL.formatIso);

    this._https.putInfo(temp, '/reportes/telefonos/costeo', 'dsReptelefonos', 'ttTgeneral', GLOBAL.httpOptions)
    .subscribe(data => {
      const result = data.siReptelefono.dsReptelefonos;
      const resultExcel = data.siReptelefono.dsEstadAgen.RespuestaExcel;
      if (!result.Costeo)  {
        this.dataGrid = [];
        this.dataGlobal = [];
      }else{

        this.dataGrid = result.Costeo;
        this.dataGlobal = result.Global;
        this.base64 = resultExcel[0].Archivo;
        this.base = resultExcel;

      }
        this.loading = false;
      });
  }

  public exportExcel() {
    this.loading = true;
    if (this.base) {
      const fileName = this.base64;
      this.http.get<any>(GLOBAL.url + '/reportes/excel/' + fileName)
      .subscribe(res => {
        const result = res.response.siEnvioExcel.dsEnvioExcel.RespuestaExcel[0];
        const fileExcel = result.ArchivoExportado;
        const linkSource = 'data:application/vnd.ms-excel;base64,' + fileExcel;
        const download = document.createElement('a');
        const fileName = result.Archivo;
        download.href = linkSource;
        download.download = fileName;
        download.click();

        this.loading = false;
      });
    }
  }

  exportPdf() {
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(this.formatMoment);
    form.fFec2 = moment(form.fFec2).format(this.formatMoment);
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: '006-COSTEO-DIA',
      dataGrid: this.dataGrid,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      hora: moment().format('HH:mm:ss'),
      form: form,
      global: this.dataGlobal,
    }
    this.http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = this.heading + '.pdf';
        download.click();
      },
      error => {
        this.toast(GLOBAL.errorPdf[0],
          GLOBAL.errorPdf[1], 'info');
      }
    );
  }

  fechaChange(num) {
    if (num === 1) {
      this.minDateHasta = new Date(moment(this.ngForm.value.fFec1).add(1, 'day').toDate());
    } else {
      this.maxDateDesde = new Date(moment(this.ngForm.value.fFec2).subtract(1, 'day').toDate());
    }
  }

}
