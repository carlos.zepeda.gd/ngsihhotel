import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { TarificacionService } from '../../telefonos/tarificacion.service'
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { BuscarService } from '../../recepcion/check-in/services/childs/buscar.service';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-servicio-despertador',
  templateUrl: './servicio-despertador.component.html',
})
export class ServicioDespertadorComponent implements OnInit {

  heading = 'Despertador';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  filter: CompositeFilterDescriptor;
  dataFilter: any[];
  pageSize = 10;
  skip = 0;
  ttInfocia: any = [];
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  loading: boolean;
  modalTitle = '';
  modalType = '';
  closeResult: string;
  flagEdit: boolean;
  selectedItem: any = [];
  toast = GLOBAL.toast;
  hour: Date = new Date(2020, 12, 9, 0, 0, 0);


  constructor(private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _buscarHues: BuscarService,
    private _router: Router,
    private _formBuild: FormBuilder,
    private _https: TarificacionService,
    private _modal: NgbModal
    ) {
      this.ttInfocia = this._info.getSessionInfocia();
      this.ngForm = this.createForm();
      this.getInfo();
    }

  ngOnInit() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
  }

  createForm() {
    return this._formBuild.group({
      Cuarto: ['', [ Validators.required]],
      NoFolio: ['', [Validators.required]],
      Hora: [0, [ Validators.required]],
      TipoMov: [''],
      FechaDespertador: moment(this.ttInfocia.fday).toDate(),
      Nombre: [''],
      Usuario: [''],
      cRowID: [],
    });
  }

  openModal(content, type: string) {
    switch (type) {
      case 'fl':
        this.modalTitle = 'Huéspedes por Nombre';
        this.modalType = 'fl';
        break;
    }

    this._modal.open(content, {
      ariaLabelledBy: 'modal-distri',
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getDataModal(data, type: string){
    switch (type){
        case 'fl':
          this.ngForm.patchValue({
            NoFolio: data.hfolio,
            Nombre: data.hnombre + ' ' + data.hapellido,
            Cuarto: data.cuarto,
          });

          break;
    }
  }


  public selectRow({ selectedRows }) {
    this.selectedItem = selectedRows[0].dataItem;
    this.flagEdit = true;
    this.selectedItem.FechaDespertador = moment(this.selectedItem.FechaDespertador).toDate();
    this.ngForm.patchValue(
      this.selectedItem
    );
    const hr = this.ngForm.value.Hora.substring(0, 2);
    const mn = this.ngForm.value.Hora.substring(4, 2);
    this.hour = new Date(2020, 1, 1, hr, mn, 0);

    this._buscarHues.getHuespedFolio(this.selectedItem.NoFolio).subscribe(data => {
      if (data) {
        if (data[0].cMensTxt) {
          return this.toast(data[0].cMensTit, data[0].cMensTxt, 'error');
        }

        this.ngForm.patchValue({
          Cuarto: data[0].cuarto,
          Nombre: data[0].hnombre + ' ' + data[0].hapellido,
        });
      } else {
      }
    });
  }

  public newRow(){
    this.flagEdit = false;
    this.ngForm.reset({
      Cuarto: '',
      NoFolio: '',
      Hora: 0,
      TipoMov: '',
      Nombre: '',
      Usuario: '',
    });
    this.ngForm.patchValue({
      FechaDespertador: moment(this.ttInfocia.fday).toDate(),
    });
    this.hour = new Date(2020, 1, 1, 0, 0, 0);
  }

  private getInfo(){

    this._https.getData('/manttos/telefonos/Despertador').map(data => data).subscribe(data => {
      if (!data)  {
        console.log('Algo está mal');
      }else{
        if (data.siXtelefono.dsXtelefonos.Despertador){
        this.dataGrid = data.siXtelefono.dsXtelefonos.Despertador;
        }
      }
        this.loading = false;
      });
  }

  public guardar(){
      this.saveRow();
  }

  private saveRow(){
    const time = this.hour.toTimeString().substring(0, 5);
    const temp = JSON.parse(JSON.stringify(this.ngForm.value));
    temp.Hora = time;
    temp.FechaDespertador = moment(temp.FechaDespertador).format(GLOBAL.formatIso);
    this._https.postInfo(temp, '/manttos/telefonos/Despertador', 'dsXtelefonos', 'ttHuesDesp')
      .subscribe(data => {
        if (data.siXtelefono.dsXtelefonos.Despertador[0].cErrDesc === '' || data.siXtelefono.dsXtelefonos.Despertador[0].cErrDesc === null){
          this.toast({
            type: 'success',
            title: 'Despertador Añadido!!'
          });
        }else {
          const errorMesj = data.siXtelefono.dsXtelefonos.Despertador[0].cErrDesc;
            this.toast(GLOBAL.textError, errorMesj, 'error');
        }
        this.newRow();
        this.getInfo();
      });

  }

  eliminar() {
    this.loading = true;
    const data = '{"dsXtelefonos":{"ttHuesDesp":[' + JSON.stringify(this.selectedItem) + ']}}';
    this._https.delete(data, 'Despertador').subscribe(res => {
      const result = res.response.siXtelefono.dsXtelefonos.ttHuesDesp;
      if (result && result[0].cErrDesc) {
        this.toast(GLOBAL.textError, result[0].cErrDesc, 'error');
      } else {
        this.toast({
          type: 'success',
          title: GLOBAL.textDeleteSuccess
        });
        this.dataGrid = [];
        this.newRow();
        this.getInfo();
      }
      this.loading = false;
    });
  }

}
