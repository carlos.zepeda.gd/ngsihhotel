import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { CrudService } from '../../mantto-srv.services';
import { PdfService } from '../../mantto-pdf.service';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';
const swal = GLOBAL.swal;

@Component({
    selector: 'app-lost-found',
    templateUrl: './lost-found.component.html',
    styleUrls: ['../../manttos-desing.css']
})
export class LostFoundComponent implements OnInit {
    heading = 'Lost and Found';
    subheading: string;
    icon: string;
    public myFormGroup: FormGroup;
    public dataGrid: any = [];
    private flagEdit: boolean;
    public filter: CompositeFilterDescriptor;
    private api = ['/llaves/lostfound', 'siLostfound', 'dsLostfound', 'ttLostfound'];
    public gridFields = [];
    public pdfGridFields = [];
    public pdfGrid = [];
    errorMesj: string = null;
    public articulos: any = [];
    public areas: any = [];
    public empleados = [];
    public estOper: Array<string> = [];
    ttInfocia: any = [];
    ttUsuario: any = [];
    fechaFin: any;
    estatusOper: string = 'Vigente'
    cxlc: any;
    formatKendo = localStorage.getItem('formatKendo');
    formatMoment = localStorage.getItem('formatMoment');
    @ViewChild('recibo') content: ElementRef;
    fday: Date;
    loadingColor: any = GLOBAL.loadingColor;
    loading: boolean = true;
    pageSize = GLOBAL.pageSize;
    skip = 0;

    constructor(private _crudservice: CrudService,
        private _info: InfociaGlobalService,
        private _pdfService: PdfService,
        private _menuServ: ManttoMenuService,
        private _modal: NgbModal,
        private _router: Router) {
    }

    ngOnInit() {
        this.initialData();
        this.setupHeading();
        this.buildFormGroup();
    }

    private buildFormGroup() {
        this.myFormGroup = new FormGroup({
            iNum: new FormControl(''),
            Articulo: new FormControl('', Validators.required),
            Area: new FormControl('', Validators.required),
            cNotas: new FormControl('', Validators.required),
            fAlta: new FormControl(moment(this.fday).toDate(), Validators.required),
            fVenc: new FormControl(null, Validators.required),
            Empleado: new FormControl(''),
            cOpe: new FormControl(''),
            cEntrega: new FormControl(''),
            cEntOpe: new FormControl(''),
            cArticulo: new FormControl(''),
            cArea: new FormControl(''),
            cEmpleado: new FormControl(''),
            cRowID: new FormControl(''),
            lError: new FormControl(false),
            cErrDesc: new FormControl(''),
            ea: new FormControl('', Validators.required),
        });
    }

    private getDataSource() {
        this._crudservice.getData(this.api[0])
            .map(data => data = data[this.api[1]][this.api[2]][this.api[3]])
            .subscribe(data => {
                if (data) {
                    this.dataGrid = data
                } else {
                    this.dataGrid = [];
                }
                this.loading = false;
            });
    }

    public newRow(modal) {
        this.openModal(modal);
        this.buildFormGroup();
        this.flagEdit = false;
        this.estOper = ['Vigente']
        this.estatusOper = 'Vigente'
    }

    public guardarForm() {
        this.flagEdit ? this.updateForm() : this.saveRow();
    }

    public editRow({ dataItem }, modal) {
        const item = JSON.parse(JSON.stringify(dataItem));
        item.fVenc = moment(item.fVenc).toDate();
        item.fAlta = moment(item.fAlta).toDate();
        this.myFormGroup.patchValue(item);
        this.flagEdit = true;
        this.estOper = ['Vigente', 'Entregado', 'Cancelado'];
        this.cxlc = item.cEntOpe.split('CXLD');
        this.estatusOper = (item.cEntOpe.length === 0) ?
            'Vigente' : (this.cxlc.length === 2) ? 'Cancelado' : 'Entregado';

        this.openModal(modal);
    }

    private saveRow() {
        this.loading = true;
        const form = JSON.parse(JSON.stringify(this.myFormGroup.value));
        form.cOpe = this.ttUsuario.cUserId
        this._crudservice.postInfo(form, this.api[0], this.api[2], this.api[3])
            .subscribe(data => {
                this.errorMesj = (data[this.api[1]].dsMensajes.ttMensError == null) ?
                    null : data[this.api[1]].dsMensajes.ttMensError[0].cMensTxt;
                if (this.errorMesj) {
                    swal({
                        type: 'error', title: GLOBAL.textError, text: this.errorMesj
                    });
                } else {
                    swal({ type: 'success', title: 'Registro guardado!!' });
                    this.closeForm();
                    this._modal.dismissAll();
                    this.getDataSource();
                }
                this.loading = false;
            })
    }

    private updateForm() {
        this.loading = true;
        const form = JSON.parse(JSON.stringify(this.myFormGroup.value));
        if (form.ea === 'Cancelado') {
            form.cEntOpe = ('CXLD ' + moment(this.fday).format(this.formatMoment) + ' POR ' + this.ttUsuario.cUserId);
        }
        if (form.ea === 'Entregado') {
            form.cEntOpe = (moment(this.fday).format(this.formatMoment) + ' POR ' + this.ttUsuario.cUserId);
        }
        form.fAlta = moment(form.fAlta).format(GLOBAL.formatIso);
        form.fVenc = moment(form.fVenc).format(GLOBAL.formatIso);
        form.cOpe = this.ttUsuario.cUserId
        this._crudservice.putInfo(form, this.api[0], this.api[2], this.api[3]).subscribe(data => {
            const result = data[this.api[1]][this.api[2]][this.api[3]];
            if (result && result[0].cErrDesc) {
                swal(GLOBAL.textError, result[0].cErrDesc, 'error');
            } else {
                this.closeForm();
                this._modal.dismissAll();
                this.getDataSource();
            }
            this.loading = false;
        });
    }

    public closeForm() {
        this.myFormGroup.reset();
        this.flagEdit = false;
    }

    public impRecibo(event) {
        const content = this.content.nativeElement;
        this._pdfService.printHTMLPDF(content);
        // this._pdfService.pdfLost(event.Articulo, event.cArea, this.formatDate(event.fAlta), event.cEmpleado, event.cNotas);
    }

    openModal(modal) {
        this._modal.open(modal, { size: 'lg', centered: true });
    }

    setupHeading() {
        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }

    initialData() {
        this.ttUsuario = this._info.getSessionUser();
        this.ttInfocia = this._info.getSessionInfocia();
        this.fday = moment(this.ttInfocia.fday).toDate();
        this.gridFields = [
            { title: 'Núm.', dataKey: 'iNum', width: '100', filtro: false },
            { title: 'Artículo', dataKey: 'Articulo', width: '250', filtro: true },
            { title: 'Área', dataKey: 'Area', width: '', filtro: true },
            { title: 'Fecha Alta', dataKey: 'fAlta', width: '', filtro: false },
            { title: 'Fecha Venc.', dataKey: 'fVenc', width: '', filtro: false },
            { title: 'Ope. Entrega', dataKey: 'cEntOpe', width: '', filtro: true },
        ];
        this._crudservice.getData('/manttos/larticulos')
            .subscribe(data => this.articulos = data.siLarticulo.dsLarticulos.ttLarticulos);
        this._crudservice.getData('/manttos/lareas')
            .map(data => data = data.siLarea.dsLareas.ttLareas).subscribe(data => {
                this.areas = data;
            });
        this._crudservice.getData('/manttos/empleados')
            .map(data => data = data.siEmpleado.dsEmpleados.ttEmpleados).subscribe(data => {
                this.empleados = data;
            });
        this.getDataSource();
    }

    public printPDF() {
        const columns = this.pdfGrid.concat(this.gridFields, this.pdfGridFields);
        this._pdfService.printPDF(this.heading, columns, this.dataGrid);
    }
}
