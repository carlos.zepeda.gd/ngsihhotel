import { Component, OnInit, OnDestroy } from '@angular/core';
import { RoomStatusService } from '../../services/estado-cuarto.service';
import { Room } from '../../models/estado-cuarto.model';
import { Subscription } from 'rxjs/Subscription';
<<<<<<< HEAD
import swal from 'sweetalert2'; 
=======
import { GLOBAL } from '../../../../../services/global';
>>>>>>> dev-alberto

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.css']
})
export class ActionsComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  cellaves: string;
  fso: string;
  alertDisc = false;
  updateCompleted = false;
  index = 0;
  rooms: Room;
  statusRooms: object;
  roomUpdate: Room;
  timeOut: any = null;
<<<<<<< HEAD

  constructor( public _RoomStatusService: RoomStatusService ) {
      this.roomUpdate = new Room('', '', '', '', '', '', '', '', '', '', '',
=======
  toast = GLOBAL.toast;
  constructor(public _RoomStatusService: RoomStatusService) {
    this.roomUpdate = new Room('', '', '', '', '', '', '', '', '', '', '',
>>>>>>> dev-alberto
      false, false, 0, 0, 0, false, '', false, '', '', '', '', '', '', '', '', false, '', '');
  }

  ngOnInit() {
    this.subscription = this._RoomStatusService.notifyObservable$.subscribe((res) => {
      if (res.option === 'cellaves') { this.cellaves = res.value; }
      if (res.option === 'fso') { this.fso = res.value; }
    });
    this._RoomStatusService.ctoedos().subscribe( data => this.statusRooms = data );
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  discTransfer(discrepancia: any) {
    this._RoomStatusService.notifyOther({ option: 'discrepancia', value: discrepancia });
  }
  updateRoom(idx: string) {
    this.index = this._RoomStatusService.getIndex();
    this.roomUpdate = this._RoomStatusService.rooms[this.index];
    this.rooms = this.roomUpdate;
    this.rooms.cellaves = idx;
<<<<<<< HEAD
    this._RoomStatusService.updateRoom(this.rooms)
                           .subscribe(data => {
                              this.rooms = data;
                              this.discTransfer(this.rooms[0]);
                              this.cellaves = this.rooms[0].cellaves;
                              this.alertDisc = this.rooms[0].cdisc;
                              this._RoomStatusService.arrayRooms().subscribe();
                              this.updateCompleted = true;
                              swal({
                                position: 'center',
                                type: 'success',
                                title: 'Cuarto Actualizado!!!',
                                showConfirmButton: false,
                                timer: 2000
                              }).then(() => {
                                if (this.alertDisc) {
                                  swal({
                                    position: 'center',
                                    type: 'warning',
                                    title: 'Discrepancia!!!',
                                    showConfirmButton: false,
                                    timer: 3000
                                  })
                                }
                              })
                              this.timeOut = setTimeout( () => {
                                this.updateCompleted = false;
                                this.alertDisc = false;
                                this.clearTimeout();
                              }, 2000);
                            });
=======
    this._RoomStatusService.updateRoom(this.rooms).subscribe(data => {
      this.rooms = data;
      this.discTransfer(this.rooms[0]);
      this.cellaves = this.rooms[0].cellaves;
      this.alertDisc = this.rooms[0].cdisc;
      this._RoomStatusService.arrayRooms().subscribe();
      this.updateCompleted = true;
      if (this.alertDisc) {
        this.toast({ type: 'warning', title: 'Discrepancia!!!' });
      }
      this.timeOut = setTimeout(() => {
        this.updateCompleted = false;
        this.alertDisc = false;
        this.clearTimeout();
      }, 2000);
    });
>>>>>>> dev-alberto
  }
  clearTimeout() {
      if (this.timeOut) {
        clearTimeout(this.timeOut);
        this.timeOut = null;
      }
  }
}
