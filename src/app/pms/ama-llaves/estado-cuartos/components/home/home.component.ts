import {Component, OnInit} from '@angular/core';
import {RoomStatusService} from '../../services/estado-cuarto.service';
import {Room} from '../../models/estado-cuarto.model';
import {ManttoMenuService} from '../../../../manttos/mantto-menu/mantto-menu.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class RoomComponent implements OnInit {
    heading = 'Estado de Cuartos';
    subheading = 'Property Management System.';
    icon = 'icon-gradient bg-tempting-azure';

    public loading: boolean = true;
    public index: number = 0;
    public rooms: Room;
    public lastRoom: Room;
    public arrayRoom: any = {};

    constructor(public _RoomStatusService: RoomStatusService,
                private _menuServ: ManttoMenuService,
                private _router: Router) {
        this.rooms = new Room('', '', '', '', '', '', '', '', '', '', '', false, false, 0, 0, 0, false,
            '', false, '', '', '', '', '', '', '', '', false, '', '');
    }

    ngOnInit() {
        this._RoomStatusService.arrayRooms().subscribe(data => {
            this.arrayRoom = data;
            this.loading = false;
        });

        const title = { icon: this.icon, subheading: this.subheading };
        this._menuServ.cuerrentObservableMenu.subscribe(data => {
            const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
            if (resp) {
                this.icon = resp.icon;
                this.subheading = resp.subheading;
            }
        });
    }
}
