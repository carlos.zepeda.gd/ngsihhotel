import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { RoomStatusService } from '../../services/estado-cuarto.service';
import { Room } from '../../models/estado-cuarto.model';
import { Subscription } from 'rxjs/Subscription';
import { GLOBAL } from '../../../../../services/global';

@Component({
  selector: 'app-echeader',
  templateUrl: './ec.header.component.html',
  styleUrls: ['./ec.header.component.css']
})
export class EcHeaderComponent implements OnInit, OnDestroy {

  public subscription: Subscription;
  public totalRoom: number;
  public discrepancia: boolean;
  public discrepanciaColor = '';
  sistema: string;
  noteFso: string;
  fso: string;
  cellaves: any;
  public rooms: Room;
  index = 0;
  clickNext = false;
  clickBack = false;
  roomSearch = true;
  public ipserver: string;
  constructor(
    public _RoomStatusService: RoomStatusService, public router: Router) {
    this.rooms = new Room('', '', '', '', '', '', '', '', '', '',
      '', false, false, 0, 0, 0, false, '', null,
      '', '', '', '', '', '', '', '', false, '', ''
    );
    this.ipserver = GLOBAL.ipserver;
  }
  ngOnInit() {
    this.index = this._RoomStatusService.getIndex();
    this.discrepanciaColor = this._RoomStatusService.getDiscrepanciaColor();
    if (this.discrepanciaColor === 'btn-danger') {
      this.fso = 'S';
      this._RoomStatusService.notifyOther({ option: 'fso', value: 'S' });
    }
    setTimeout(() => {
      this.totalRoom = this._RoomStatusService.totalRoom();
      this.totalRoom = this.totalRoom - 1;
    }, 1000);

    this.subscription = this._RoomStatusService.notifyObservable$.subscribe((res) => {
      if (res.option === 'discrepancia') {
        this.rooms = res.value;
        this.discrepancia = this.rooms.cdisc;
        this.discrepanciaColor = this.caseDisc(this.discrepancia);
        localStorage.setItem('discrepanciaColor', this.discrepanciaColor.toString());
      }
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  dataTransfer(noteFso: string, fso: string) {
    this._RoomStatusService.notifyOther({ option: 'noteFso', value: noteFso });
    this._RoomStatusService.notifyOther({ option: 'fso', value: fso });
  }
  nextRoom() {
    this.clickNext = true;
    this.index++;
    this.rooms = this._RoomStatusService.oneRoom(this.index);
    this.sistema = this.caseSist(this.rooms.cecomp);
    this.discrepanciaColor = this.caseDisc(this.rooms.cdisc);
    this.cellaves = this.rooms.cellaves;
    this.fso = this.rooms.fso;
    this.noteFso = this.rooms.frazon;
    setTimeout(() => this.clickNext = false, 50);
    this.localStorageRoom();
    this.dataTransfer(this.noteFso, this.fso);
  }
  backRoom() {
    this.clickBack = true;
    this.index--;
    this.rooms = this._RoomStatusService.oneRoom(this.index);
    this.sistema = this.caseSist(this.rooms.cecomp);
    this.discrepanciaColor = this.caseDisc(this.rooms.cdisc);
    this.cellaves = this.rooms.cellaves;
    this.fso = this.rooms.fso;
    this.noteFso = this.rooms.frazon;
    setTimeout(() => this.clickBack = false, 50);
    if (this.index === -1) { this.index = 0; }
    this.localStorageRoom();
    this.dataTransfer(this.noteFso, this.fso);
  }
  searchOne(buscar: string) {
    if (buscar.length <= 1) {
      return;
    }
    this._RoomStatusService.searchRoom(buscar); // Obtener coinsidencias
    this.rooms = this._RoomStatusService.thisRoom(); // Obtener registro uno

    if (!this.rooms) { // Si el cuarto no existe
      this.roomSearch = false;
      setTimeout(() => this.roomSearch = true, 3000);
    } else {
      this.sistema = this.caseSist(this.rooms.cecomp);
      this.discrepanciaColor = this.caseDisc(this.rooms.cdisc);
      this.cellaves = this.rooms.cellaves;
      this.fso = this.rooms.fso;
      this.noteFso = this.rooms.frazon;
      this.roomSearch = true;
      this.index = this._RoomStatusService.thisCont(); // Obtener la posición del registro en el arreglo
      this.localStorageRoom();
      this.dataTransfer(this.noteFso, this.fso);
    }
  }
  localStorageRoom() {
    localStorage.setItem('index', this.index.toString());
    localStorage.setItem('discrepanciaColor', this.discrepanciaColor.toString());
  }
  caseSist(sist: string) {
    switch (sist) {
      case 'N': this.sistema = 'btn-info';
        break;
      case 'T': this.sistema = 'btn-warning';
        break;
      case 'G': this.sistema = '';
        break;
      case '': this.sistema = '';
        break;
    }
    return this.sistema;
  }
  caseDisc(discrepancia: boolean) {
    switch (discrepancia) {
      case true: this.discrepanciaColor = 'btn-danger';
        break;
      case false: this.discrepanciaColor = '';
        break;
    }
    return this.discrepanciaColor;
  }
}
