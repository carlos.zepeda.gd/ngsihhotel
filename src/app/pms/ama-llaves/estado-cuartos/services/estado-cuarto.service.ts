import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { GLOBAL } from '../../../../services/global';
import { Room } from '../models/estado-cuarto.model';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class RoomStatusService {

  private notify = new Subject<any>();
  notifyObservable$ = this.notify.asObservable();
  public roomStatus: any;
  public totalRooms: number;
  public fso: string;
  public discrepancia: boolean;
  public cellaves: string;
  public saveRoom: Room;
  public index: any;
  public position: number = 0;
  public cont: number = 0;
  public url: string;
  public cutil: string;
  public arrayCtoEdos: Array<any> = [];
  public rooms: Array<Room> = [];
  public arreglo: Array<Room> = [];
  public oneRooms: any;
  public discrepanciaColor: string;
  constructor(private http: Http) { this.url = GLOBAL.url; }

  public notifyOther(data: any) {
    if (data) { this.notify.next(data); }
  }
  getCuartos() { // Listado de cuartos
    return this.http.get(this.url + '/manttos/llaves').map(res => res.json().response.siCuarto.dsCuartos);
  }
  arrayRooms() { // Listado de cuartos
    return this.http.get(this.url + '/manttos/llaves').map((res: Response) => {
      this.rooms = res.json().response.siCuarto.dsCuartos.ttCuartos;
      this.totalRooms = this.rooms.length;
      return this.rooms;
    })
  }
  roomsForStatus(status: string, tcuarto: string) {
    const link = `${GLOBAL.char174 + tcuarto + GLOBAL.char174 + status + GLOBAL.char174 + 'no'}`;
    return this.http.get(this.url + '/ayuda/cuartostipo' + link)
      .map((res: Response) => res.json().response.siAyuda.dsAyuda.ttCuartos);

  }
  totalRoom() { // total de cuartos en el arreglo
    return this.totalRooms;
  }

  ctoedos() { // mostrar botones con los estados de cuarto
    this.arrayCtoEdos = [];
    return this.http.get(this.url + '/manttos/llaves').map((res: Response) => {
      this.roomStatus = res.json().response.siCuarto.dsCuartos.ttCtoedos;

      for (const i of this.roomStatus) {
        if (i.cutil[1] === 'S') { this.arrayCtoEdos.push(i); }
      }
      this.arrayCtoEdos.sort(function (a, b) {
        if (a.iSec < b.iSec) {
          return -1;
        } else if (a.iSec > b.iSec) {
          return 1;
        } else {
          return 0;
        }
      });
      // this.arrayCtoEdos.push('{ cedo: "", cdesc: "Todos" }');
      return this.arrayCtoEdos.sort();
    })
  }

  oneRoom(id: number): Room { // Obtener cuarto de uno por uno y asignar los fuera de servicio
    this.fso = this.rooms[id].fso;
    this.oneRooms = this.rooms[id];
    return this.oneRooms;
  }
  searchRoom(roomNumber: string): Room { // Buscar cuarto con componente de buscar
    this.arreglo = [];
    this.cont = 0;
    for (const item of this.rooms) {
      const cuartos = item.cuarto;
      if (cuartos.indexOf(roomNumber) !== -1) {
        this.arreglo.push(item);
        this.position = this.cont;
      }
      this.cont++;
    }
    return this.arreglo[0];
  }
  thisRoom(): Room { // Obtener la data del cuarto buscado
    return this.arreglo[0];
  }
  thisCont() { // Obtener la posición del arreglo cuando buscar un cuarto
    return this.position;
  }
  getIndex() { // Obtener del Local Storage la ultima posicion del arreglo
    const index = localStorage.getItem('index');
    if (index === null) {
      this.index = 0;
    } else {
      this.index = index;
    }
    return this.index;
  }
  getDiscrepanciaColor() {
    const discrepanciaColor = localStorage.getItem('discrepanciaColor');
    if (discrepanciaColor === null) {
      this.discrepanciaColor = null;
    } else {
      this.discrepanciaColor = discrepanciaColor;
    }
    return discrepanciaColor;
  }
  updateRoom(room: object) { // actualizar cuarto
    const json = JSON.stringify(room);
    const params = '{"dsCuartos":{"ttCuartos":[' + json + ']}}';
    const headers = new Headers({ 'Content-Type': 'application/json' });

    return this.http.put(this.url + '/manttos/llaves', params, { headers: headers })
      .map((res: Response) => this.oneRooms = res.json().response.siCuarto.dsCuartos.ttCuartos);
  }
}
