<<<<<<< HEAD
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, GroupDescriptor, process } from '@progress/kendo-data-query';
import swal from 'sweetalert2';
import { CrudService } from '../../mantto-srv.services';
import { PdfService } from '../../mantto-pdf.service';

const flatten = filter => {
  const filters = filter.filters;
  if (filters) {
    return filters.reduce((acc, curr) => acc.concat(curr.filters ? flatten(curr) : [curr]), []);
  }
  return [];
};

@Component({
  selector: 'app-camaristas-operadores',
  templateUrl: './camaristas-operadores.component.html',
  styleUrls: ['../../manttos-desing.css']
})
export class CamaristasOperadoresComponent implements OnInit {
  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  public flagShowForm: boolean;
  private flagEdit: boolean;
  public dataComplete: any = [];
  public checked: boolean;
  public groups: GroupDescriptor[] = [];
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'punto', dir: 'asc' }];

  private urlSec: string = '/manttos/camaristas';
  private ds: string = 'dsCamaristas'; // siCamarista.dsCamaristas.ttCamaristas
  private tt: string = 'ttCamaristas';
  public gridFields = [];
  public pdfGridFields = [];
  public pdfGrid = [];
  seccion: string = 'Camaristas-Operadores';
  errorMesj: string = null;
  formStatus: string = null;
  public dsSecciones: any = [];
  public dsSeccionesComplete: any = [];
  public dsDeptos: any = [];
  public dsDeptosComplete: any = [];
  public dDescanso = [];
  public localizacion = [];
  public siTrabajos = [];

  constructor(
    private _crudservice: CrudService,
    private _pdfService: PdfService
  ) {
    this.gridFields = [
      { title: '#Cam', dataKey: 'camarista', width: '50', filtro: false },
      { title: 'Nombre', dataKey: 'cnombre', width: '150', filtro: true },
      { title: 'Sección', dataKey: 'seccion', width: '', filtro: false },
      { title: 'Depto', dataKey: 'depto', width: '', filtro: false },
      { title: 'Descanso', dataKey: 'cdd', width: '', filtro: false },
      { title: 'C.T', dataKey: 'cct', width: '', filtro: false },
      { title: 'Local', dataKey: 'clocal', width: '', filtro: false },
      { title: 'Labora', dataKey: 'clab', width: '', filtro: false },
      { title: 'Trabajo', dataKey: 'trabajo', width: '', filtro: false },
    ];
    this.buildFormGroup();
    this._crudservice.getData('/manttos/secciones')
      .map(data => data = data.siSeccion.dsSecciones.ttSecciones).subscribe(data => {
        this.dsSecciones = data;
        // this.dsSeccionesComplete = data;
      });
    this._crudservice.getData('/manttos/deptos')
      .map(data => data = data.siDepto.dsDeptos.ttDeptos).subscribe(data => {
        this.dsDeptos = data;
        // this.dsDeptosComplete = data;
      });
    this.dDescanso = [
      { title: 'Dom', value: '1' }, { title: 'Lun', value: '2' },
      { title: 'Mar', value: '3' }, { title: 'Mier', value: '4' },
      { title: 'Juev', value: '5' }, { title: 'Vie', value: '6' },
      { title: 'Sab', value: '7' }
    ]
    this.localizacion = [
      { loc: 'Hotel', abrev: 'H' }, { loc: 'Tiempo Compartido', abrev: 'T' }
    ]
    this._crudservice.getData('/manttos/trabajos')
      .map(data => data = data.siTrabajo.dsTrabajos.ttTrabajos).subscribe(data => {
        this.siTrabajos = data;
        // this.dsDeptosComplete = data;
      });
  }

  ngOnInit() {
    this.getDataSource();
  }

  private buildFormGroup() {
    this.myFormGroup = new FormGroup({
      camarista: new FormControl('', Validators.required),
      cnombre: new FormControl('', Validators.required),
      seccion: new FormControl('', Validators.required),
      cSeccion: new FormControl(''),
      depto: new FormControl(''),
      cDepto: new FormControl(''),
      cdd: new FormControl(''),
      cDiaD: new FormControl(''),
      cct: new FormControl(''),
      cCturno: new FormControl(''),
      clocal: new FormControl(''),
      clab: new FormControl(''),
      trabajo: new FormControl(''),
      cTrabajo: new FormControl(''),
      cErrDes: new FormControl(''),
      lError: new FormControl(false),
      cRowID: new FormControl(''),
    });
  }

  private getDataSource() {
    this._crudservice.getData(this.urlSec)
      .map(data => data = data.siCamarista.dsCamaristas.ttCamaristas)
      .subscribe(data => {
        this.dataGrid = data;
        this.dataComplete = data;
        this.dataGrid = {
          data: orderBy(this.dataGrid, this.sort),
          total: this.dataGrid.length
        };
      });
  }

  public newRow() {
    this.buildFormGroup();
    this.flagShowForm = true;
    this.flagEdit = false;
    this.formStatus = 'Nuevo';
  }

  public guardarForm() {
    (this.flagEdit) ? this.updateForm() : this.saveRow();
  }

  public editRow({ dataItem }) {
    this.myFormGroup.setValue(dataItem);
    this.flagEdit = true;
    this.flagShowForm = true;
    this.formStatus = 'Editar';
  }

  private saveRow() {
    const form = this.myFormGroup.value;
    if (form.seccion.Seccion) { form.seccion = form.seccion.Seccion }
    if (form.depto.depto) { form.depto = form.depto.depto }
    if (form.cdd.value) { form.cdd = form.cdd.value }
    if (form.cct.camarista) { form.cct = form.cct.camarista }
    if (form.clocal.abrev) { form.clocal = form.clocal.abrev }
    if (form.trabajo.trabajo) { form.trabajo = form.trabajo.trabajo }
    this._crudservice.postInfo(form, this.urlSec, this.ds, this.tt)
      .subscribe(data => {
        this.errorMesj = (data.siCamarista.dsMensajes.ttMensError == null) ?
          null : data.siCamarista.dsMensajes.ttMensError[0].cMensTxt;
        if (this.errorMesj) {
          swal('ERROR!', this.errorMesj, 'error');
        } else {
          swal({
            position: 'center',
            type: 'success',
            title: 'Registro guardado!!',
            showConfirmButton: false,
            timer: 1500
          })
        }
        this.flagShowForm = false;
        this.getDataSource();
        this.closeForm();
      })
  }

  private updateForm() {
    const form = this.myFormGroup.value;
    if (form.seccion.Seccion) { form.seccion = form.seccion.Seccion }
    if (form.depto.depto) { form.depto = form.depto.depto }
    if (form.cdd.value) { form.cdd = form.cdd.value }
    if (form.cct.camarista) { form.cct = form.cct.camarista }
    if (form.clocal.abrev) { form.clocal = form.clocal.abrev }
    if (form.trabajo.trabajo) { form.trabajo = form.trabajo.trabajo }
    this._crudservice.putInfo(form, this.urlSec, this.ds, this.tt).subscribe(data => {
      this.getDataSource();
      this.closeForm();
    });
  }

  public deleteRow({ dataItem }) {
    this._crudservice.deleteData(dataItem, this.urlSec, this.tt).subscribe(data => {
      this.errorMesj = (data.siCamarista.dsMensajes.ttMensError == null) ?
        null : data.siCamarista.ttMensError[0].cMensTxt;
      if (this.errorMesj) {
        swal({
          position: 'center',
          type: 'error',
          title: 'Error!!',
          text: this.errorMesj,
          showConfirmButton: false,
          timer: 1500
        })
      } else {
        this.getDataSource();
      }
      this.closeForm();
    });
  }

  public closeForm() {
    this.myFormGroup.reset();
    this.flagShowForm = false;
    this.flagEdit = false;
  }
  public close(component) {
    this[component + 'Opened'] = false;
    this.closeForm();
  }
  // Generear PDF 
  public printPDF() {
    const columns = this.pdfGrid.concat(this.gridFields, this.pdfGridFields);
    this._pdfService.printPDF(this.seccion, columns, this.dataGrid.data);
  }

  // Funciones para la Grid de Kendo
  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.dataGrid = process(this.dataComplete, { group: this.groups });
  }
  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }
  public switchChange(checked: boolean): void {
    const root = this.filter || { logic: 'and', filters: [] };
    const [filter] = flatten(root).filter(x => x.field === 'Inactivo');
    if (!filter) {
      root.filters.push({
        field: 'Inactivo',
        operator: 'eq',
        value: checked
      });
    } else {
      filter.value = checked;
    }
    this.checked = checked;
    this.filterChange(root);
  }
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }
}
=======
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { CrudService } from '../../mantto-srv.services';
import { PdfService } from '../../mantto-pdf.service';
import { GLOBAL } from '../../../services/global';

@Component({
  selector: 'app-camaristas-operadores',
  templateUrl: './camaristas-operadores.component.html',
  styleUrls: ['../../manttos-desing.css']
})
export class CamaristasOperadoresComponent implements OnInit {
  heading = 'Camaristas Operadores';
  subheading: string;
  icon: string;
  public myFormGroup: FormGroup;
  public dataGrid: any = [];
  private flagEdit: boolean;
  public dataComplete: any = [];
  public filter: CompositeFilterDescriptor;
  loadingColor: any = GLOBAL.loadingColor;
  toast = GLOBAL.toast;
  private api: Array<string> = ['/manttos/camaristas', 'siCamarista', 'dsCamaristas', 'ttCamaristas'];
  public gridFields = [];
  public pdfGridFields = [];
  public pdfGrid = [];
  errorMesj: string = null;
  public dsSecciones = [];
  public dsDeptos: any = [];
  public dDescanso = [];
  public localizacion = [];
  public siTrabajos = [];
  loading = false;
  pageSize = GLOBAL.pageSize;

  constructor(private _crudservice: CrudService,
    private _pdfService: PdfService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _modalService: NgbModal) {
    this.gridFields = [
      { title: '#Cam', dataKey: 'camarista', width: '100', filtro: true },
      { title: 'Nombre', dataKey: 'cnombre', width: '200', filtro: true },
      { title: 'Sección', dataKey: 'seccion', width: '100', filtro: false },
      { title: 'Depto', dataKey: 'depto', width: '100', filtro: false },
      { title: 'Descanso', dataKey: 'cdd', width: '100', filtro: false },
      { title: 'C.T', dataKey: 'cct', width: '100', filtro: false },
      { title: 'Local', dataKey: 'clocal', width: '100', filtro: false },
      { title: 'Labora', dataKey: 'clab', width: '100', filtro: false },
      { title: 'Trabajo', dataKey: 'trabajo', width: '100', filtro: false },
    ];
    this.buildFormGroup();
    this._crudservice.getData('/manttos/secciones')
      .map(data => data = data.siSeccion.dsSecciones.ttSecciones).subscribe(data => {
        this.dsSecciones = data;
      });
    this._crudservice.getData('/manttos/deptos')
      .map(data => data = data.siDepto.dsDeptos.ttDeptos).subscribe(data => {
        this.dsDeptos = data;
      });
    this.dDescanso = [
      { title: 'Dom', value: '1' }, { title: 'Lun', value: '2' },
      { title: 'Mar', value: '3' }, { title: 'Mier', value: '4' },
      { title: 'Juev', value: '5' }, { title: 'Vie', value: '6' },
      { title: 'Sab', value: '7' }
    ]
    this.localizacion = [
      { loc: 'Hotel', abrev: 'H' }, { loc: 'Tiempo Compartido', abrev: 'T' }
    ]
    this._crudservice.getData('/manttos/trabajos')
      .map(data => data = data.siTrabajo.dsTrabajos.ttTrabajos).subscribe(data => {
        this.siTrabajos = data;
      });
    this.getDataSource();
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  private buildFormGroup() {
    this.myFormGroup = new FormGroup({
      camarista: new FormControl('', [Validators.required, Validators.min(0), Validators.max(9999)]),
      cnombre: new FormControl('', [Validators.required, Validators.maxLength(40)]),
      seccion: new FormControl(''),
      cSeccion: new FormControl(''),
      depto: new FormControl('', Validators.required),
      cDepto: new FormControl(''),
      cdd: new FormControl(''),
      cDiaD: new FormControl(''),
      cct: new FormControl(''),
      cCturno: new FormControl(''),
      clocal: new FormControl(''),
      clab: new FormControl(true),
      trabajo: new FormControl('', Validators.required),
      cTrabajo: new FormControl(''),
      cErrDes: new FormControl(''),
      lError: new FormControl(false),
      cRowID: new FormControl(''),
    });
  }

  private getDataSource() {
    this.loading = true;
    this._crudservice.getData(this.api[0])
      .map(data => data = data[this.api[1]][this.api[2]][this.api[3]])
      .subscribe(data => {
        if (data) {
          this.dataGrid = data;
          this.dataComplete = data;
        }else {
          this.dataGrid = [];
          this.dataComplete = [];
        }
        this.loading = false;
      });
  }

  public newRow(modal) {
    this.buildFormGroup();
    this.flagEdit = false;
    this.openModalData(modal);
  }

  public guardarForm() {
    this.flagEdit ? this.updateForm() : this.saveRow();
  }

  public editRow({ dataItem }, modal) {
    this.myFormGroup.patchValue(dataItem);
    this.flagEdit = true;
    this.openModalData(modal);
  }

  private saveRow() {
    this.loading = true;
    this._crudservice.postInfo(this.myFormGroup.value, this.api[0], this.api[2], this.api[3])
      .subscribe(data => {
        this.errorMesj = (data[this.api[1]].dsMensajes.ttMensError == null) ?
          null : data[this.api[1]].dsMensajes.ttMensError[0].cMensTxt;
        if (this.errorMesj) {
          this.toast({ title: GLOBAL.textError, text: this.errorMesj, type: 'error' });
        } else {
          this.toast({ type: 'success', title: GLOBAL.textSuccess });
        }
        this.getDataSource();
        this.closeForm();
        this._modalService.dismissAll();
      })
  }

  private updateForm() {
    this.loading = false;
    this._crudservice.putInfo(this.myFormGroup.value, this.api[0], this.api[2], this.api[3]).subscribe(data => {
      this.getDataSource();
      this.closeForm();
      this._modalService.dismissAll();
    });
  }

  public deleteRow({ dataItem }) {
    this.loading = true;
    this._crudservice.deleteData(dataItem, this.api[0], this.api[3]).subscribe(data => {
      this.errorMesj = (data.siCamarista.dsMensajes.ttMensError == null) ?
        null : data.siCamarista.ttMensError[0].cMensTxt;
      if (this.errorMesj) {
        this.toast({
          type: 'error', title: GLOBAL.textError, text: this.errorMesj
        });
      } else {
        this.getDataSource();
      }
      this.closeForm();
    });
  }

  public closeForm() {
    this.myFormGroup.reset();
    this.flagEdit = false;
  }

  public printPDF() {
    const columns = this.pdfGrid.concat(this.gridFields, this.pdfGridFields);
    this._pdfService.printPDF(this.heading, columns, this.dataGrid);
  }

  openModalData(content) {
    this._modalService.open(content, { size: 'md', centered: true });
  }
}
>>>>>>> dev-alberto
