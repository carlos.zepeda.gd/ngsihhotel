<<<<<<< HEAD
import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
// Librerias Kendo
import { GridDataResult } from '@progress/kendo-angular-grid';
import {
  SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor,
  GroupDescriptor, process, aggregateBy
} from '@progress/kendo-data-query';
// Servicios
import { CrudService } from '../../mantto-srv.services';
import { PdfService } from '../../mantto-pdf.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
// Modelo
import { Tgeneral } from '../../../models/tbl-general';
// Librerias
import swal from 'sweetalert2';
import * as XLSX from 'xlsx';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

// Esta función se ejecuta cuando en la grid el parametro [filterable] es igual a true
const flatten = filter => {
  const filters = filter.filters;
  if (filters) {
    return filters.reduce((acc, curr) => acc.concat(curr.filters ? flatten(curr) : [curr]), []);
  }
  return [];
};

const createFormCamaSec = dataItem => new FormGroup({
  'seccion': new FormControl(dataItem.seccion)
});
const createFormCamaOper = dataItem => new FormGroup({
  'camarista': new FormControl(dataItem.camarista)
});

@Component({
  selector: 'app-asignacion-trabajo',
  templateUrl: './asignacion-trabajo.component.html',
  styleUrls: ['../../manttos-desing.css']
})
export class AsignacionTrabajoComponent implements OnInit {
  public view: Observable<GridDataResult>;
  public formSend: Tgeneral;
  public formSearch: FormGroup;
  public myFormGroup: FormGroup;
  public displayDateFormat: any;
  public accion: string = '';
  public dsTcuartos: any = []; // Tipos de Cuarto activos
  public dsTcuartosTotal: any = []; // Tipos de cuarto completa
  public dsTcuartosSuscribe: any = []; // Tipos de cuarto completa
  public tipoCuartos: any = []; // Filtro Tipos de Cuarto
  public tCuartoElement: any = []; // Arreglo base busqueda Tipo Cuarto

  public dsIngagrp: any = []; // Ingreso Agrupado
  public tipoIngreso: any = []; // Filtro Ingreso Agrupado
  public tIngresoElement: any = []; // Arreglo base busqueda Tipo Ingreso Agrupado
  public ttIngreso: any = [];

  public dTBusqueda: any = [];
  public ttTcamaristas: any = [];
  public ttTcamaoper: any = [];
  public params: any;
  public dataSection: any = [];
  public infoParametros: any = [];
  public infoSecciones: any = [];

  public tCamOper: any = { camarista: 0 };

  // Variables para vincular con componentes de Kendo
  public dataGrid: any = [];
  public dataComplete: any = [];
  // Variables para obtener información del usuario y del hotel
  public ttUsuario: any = [];
  public ttInfocia: any = [];
  // Variables para funciones de Kendo Grid
  public filter: CompositeFilterDescriptor;
  public groups: GroupDescriptor[] = [];
  public sort: SortDescriptor[] = [{ field: 'cuarto', dir: 'asc' }]; // Order asc o desc 
  public aggregates: any[] = [ // Totalizar columnas de la Grid
    { field: 'tmont1', aggregate: 'sum' },
    { field: 'tmont2', aggregate: 'sum' },
    { field: 'tmont3', aggregate: 'sum' }
  ];
  // Banderas para ocultar o mostrar
  public flagChart: boolean;
  public flagForm: boolean = true;
  public flagViewDetail: boolean;
  public flagViewCamaOper: boolean;
  public total: any;
  public windowLeft: 0;
  public groupable: boolean = true;
  // Data en codigo duro
  public localizacion = [
    { loc: 'Todo', abrev: 1 }, { loc: 'Hotel', abrev: 2 }, { loc: 'Tiempo Compartido', abrev: 3 }
  ];

  private urlSec: string = '/llaves/asignacion/';
  public gridFields = [];
  public pdfGridFields = [];
  public pdfGrid = [];
  seccion: string = 'Asignación de Trabajo';
  public dtfCamaristas = [];
  public dtfCamaOper = [];
  public listCamaRepo: any = [];
  public listCuartoRepo: any = [];

  incFI = false; // Inc F/I
  incCtoSCred = false; // Inc Ctos S/Cred
  ocup = false; // Edos. Ocupados
  cout = false; // Check Out
  vacsuc = false; // Vacantes Sucios
  camc = false; // Cambios de cuarto
  disc = false; // Discrepancias
  fserv = false; // Fuera de Servicio
  all = false; // Todos

  public iagrpEstatus: any = null;

  dFcxc: any;
  iFcam: any;
  dFcasig: any;
  dsEstcam: any;
  public formGroup: FormGroup;
  private editedRowIndex: number;

  constructor(
    private _crudservice: CrudService,
    private _pdfService: PdfService,
    private _info: InfociaGlobalService,
    private formBuilder: FormBuilder,
    private sanitizer: DomSanitizer
  ) {
    this._crudservice.getData('/manttos/tcuartos')
      .map(data => data = data.siTcuarto.dsTcuartos.ttTcuartos)
      .subscribe(data => {
        this.dsTcuartos = data;
        this.dsTcuartos = filterBy(this.dsTcuartos, {
          logic: 'and',
          filters: [{ field: 'inactivo', operator: 'eq', value: false }]
        })
      });
    this._crudservice.getData('/manttos/ingagrp')
      .map(data => data = data.siIngagrp.dsIngagrp.ttIngagrp).subscribe(data => {
        this.dsIngagrp = data;
      });
    this._crudservice.getData('/manttos/camaristas')
      .map(data => data = data.siCamarista.dsCamaristas.ttCamaristas).subscribe(data => {
        this.ttTcamaoper = data;
        this.ttTcamaoper.push({ camarista: 0 });
      });
    this._crudservice.getData('/ayuda/parametros®llaves')
      .subscribe(data => this.infoParametros = data.siAyuda.dsAyuda.ttParametros);
    this._crudservice.getData('/manttos/secciones')
      .subscribe(data => this.infoSecciones = data.siSeccion.dsSecciones.ttSecciones);
  }

  ngOnInit() {
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.displayDateFormat = localStorage.getItem('globalFormatoFecha');
    this.buildFormGroup();

    this.pdfGridFields = [
      { title: 'Seccion', dataKey: 'seccion' },
      { title: 'Cuarto', dataKey: 'cuarto' },
      { title: 'Tipo Cuarto', dataKey: 'tcuarto' },
      { title: 'Creditos', dataKey: 'ccred' },
      { title: 'Fec. Sal.', dataKey: 'cfsal' },
      { title: 'Ad', dataKey: 'cadul' },
      { title: 'Jr', dataKey: 'cjun' },
      { title: 'Men', dataKey: 'cmen' },
      { title: 'Rec', dataKey: 'cerecep' },
      { title: 'AL', dataKey: 'cellaves' },
      { title: 'Disc', dataKey: 'cdisc' },
      { title: 'Sis', dataKey: 'cecomp' },
      { title: 'Rev', dataKey: 'cerev' },
      { title: 'Bot', dataKey: 'ceboton' },
      { title: 'F/I', dataKey: 'cfi' },
      { title: 'F/S', dataKey: 'cfs' },
      { title: 'Razón', dataKey: 'crazon' },
      { title: 'Vip', dataKey: 'cvip' },
      { title: 'Hora', dataKey: 'chora' },
    ];

    this.gridFields = [
      { title: 'Nombre Huesped', dataKey: 'cnh' }
    ];

    this.dtfCamaristas = [
      { title: 'C/O', dataKey: 'camarista', width: '120', filtro: true },
      { title: 'Nombre', dataKey: 'cnombre', width: '', filtro: true },
      { title: 'Sección', dataKey: 'seccion', width: '', filtro: true, editor: '' },
      { title: 'Creditos', dataKey: 'ccred', width: '', filtro: true }
    ];

    this.dtfCamaOper = [
      { title: 'C/O', dataKey: 'camarista', width: '120', filtro: true },
      { title: 'Nombre', dataKey: 'cnombre', width: '', filtro: true },
      { title: 'Sección', dataKey: 'seccion', width: '', filtro: true },
      { title: 'Descanso', dataKey: 'cDiaD', width: '', filtro: true },
      { title: 'Cubre Turno', dataKey: 'cCturno', width: '', filtro: true },
      { title: 'Local', dataKey: 'clocal', width: '', filtro: true },
      { title: 'Labora', dataKey: 'clab', width: '', filtro: true },
    ];
  }

  public infoParaLlaves() {
    return this.infoParametros === undefined ? '1' : this.infoParametros[0].pchr1;
  }

  private buildFormGroup() {
    this.myFormGroup = new FormGroup({
      iInt1: new FormControl(1, Validators.required),
      lLog1: new FormControl(true),
      lLog2: new FormControl(true),
      lLog3: new FormControl(true),
      lLog4: new FormControl(true),
      lLog5: new FormControl(true),
      lLog6: new FormControl(false),
      lLog7: new FormControl(false),
      lLog8: new FormControl(false),
      lLog9: new FormControl(false),
      lLog10: new FormControl(true),
      // ttTotal
      iFcam: new FormControl(0),
      dFcasig: new FormControl(0),
      dFcxc: new FormControl(0),
      // ttCuartos
      tcuartos: new FormControl(''),
      iagrp: new FormControl(''),
      fecha: new FormControl(this._info.toDate(this.ttInfocia.fday))
    });
  }

  public getInfo() {
    const val = this.myFormGroup.value;
    const local = val.iInt1.abrev;
    this.all = val.lLog9; // Todos los Estados de Cuarto
    this.incFI = val.lLog1;
    this.incCtoSCred = val.lLog2;
    const fecha = this._info.formatDate(val.fecha);
    if (this.all) {
      this.ocup = false; // Edos. Ocupados
      this.cout = false; // Check Out
      this.vacsuc = false; // Vacantes Sucios
      this.camc = false; // Cambios de cuarto
      this.disc = false; // Discrepancias
      this.fserv = false; // Fuera de Servicio
    } else {
      this.ocup = val.lLog3; // Edos. Ocupados
      this.cout = val.lLog4; // Check Out
      this.vacsuc = val.lLog5; // Vacantes Sucios
      this.camc = val.lLog6; // Cambios de cuarto
      this.disc = val.lLog7; // Discrepancias
      this.fserv = val.lLog8; // Fuera de Servicio
    }
    this.formSend = new Tgeneral(
      '', '', '', '', '', '', '', '', '', '', '',
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      fecha, null, null, null, null, null, null, null, null, null, null,
      local, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      this.incFI, this.incCtoSCred, this.ocup, this.cout, this.vacsuc, this.camc, this.disc, this.fserv, this.all, false, false,
      null, null, null, null, null, null, null, null, null, null, null
    );

    this.tipoCuartos = [];
    if (val.tcuartos) {
      val.tcuartos.forEach(obj => {
        this.tCuartoElement = { 'ttcto': obj.tcuarto };
        this.tipoCuartos.push(this.tCuartoElement);
      })
    }

    this.tipoIngreso = [];
    if (val.iagrp && val.iagrp.length > 0) {
      this.tipoCuartos = []
      val.iagrp.forEach(obj => {
        this.tIngresoElement = { 'iagrp': obj.iagrp };
        this.tipoIngreso.push(this.tIngresoElement);
      })
    }
    this.update('B');
  }

  ingActivo(inga) {
    this.iagrpEstatus = (inga.length > 0) ? true : null;
  }

  asignar() {
    this.update('A');
  }

  borrar() {
    this.update('I');
  }

  public update(accion: any): void {
    switch (accion) {
      case 'B':
        const json = JSON.stringify(this.formSend);
        const jsonTcuarto = JSON.stringify(this.tipoCuartos);
        const jsonTingreso = JSON.stringify(this.tipoIngreso);
        this.params = '{"dsEstcam":{"ttTgeneral":[' + json + '], "ttTcto":' + jsonTcuarto + ', "ttTiagrp":' + jsonTingreso + ' }}';
        break;
      case 'A':
        this.assignValues(this.dTBusqueda, { ttTgeneral: [{ iInt1: this.dFcasig, iInt2: this.dFcxc }] });
        this.params = JSON.stringify(this.dTBusqueda);
        break;
      case 'I':
        this.assignValues(this.dTBusqueda, { ttTgeneral: [{ iInt1: this.dFcasig }] });
        this.params = JSON.stringify(this.dTBusqueda);
        break;
      default:
        this.assignValues(this.dataGrid, { ttTcuarto: [accion] });
        this.params = JSON.stringify(this.dTBusqueda);
        accion = '';
        break
    }
    // console.log(this.params);
    this._crudservice.putInfoReporte(this.urlSec + accion, this.params)
      .map(data => data = data.siEstcam.dsEstcam).subscribe(data => {
        this.params = {};
        // console.log(data);
        this.dTBusqueda = data;
        this.iFcam = data.ttTotal[0].iFcam;
        this.dFcasig = data.ttTotal[0].dFcasig;
        this.dFcxc = this.infoParaLlaves();

        if (data.ttTcuarto) {
          this.dataGrid = data.ttTcuarto;
          this.dataComplete = data.ttTcuarto;
          this.dataGrid = {
            data: orderBy(this.dataGrid, this.sort),
            total: this.dataGrid.length
          };
          this.total = aggregateBy(this.dataComplete, this.aggregates);
          this.flagForm = false;
          this.ttTcamaristas = data.ttTcam;
          this.listCamaRepo = this.camaReporte();
        } else {
          this.dataGrid = [];
          this.dataComplete = [];
          swal('Error!', 'No se encontraron registros!!', 'error');
          return;
        }
      });
    this.dataSection = [];
  }

  public assignValues(target: any, source: any): void {
    return Object.assign(target, source);
  }

  public cellClickHandler({ sender, rowIndex, columnIndex, dataItem, isEdited }) {
    if (!isEdited) {
      let formSection;
      formSection = (dataItem.cuarto) ? createFormCamaOper(dataItem) : createFormCamaSec(dataItem);
      sender.editCell(rowIndex, columnIndex, formSection);
    }
  }

  public cellCloseHandler(args: any) {
    const { formGroup, dataItem } = args;
    if (!formGroup.valid) {
      // prevent closing the edited cell if there are invalid values.
      args.preventDefault();
    } else if (formGroup.dirty) {
      // let nCampo: any;
      // for (nCampo in formGroup.value) {
      //   if (formGroup.value.hasOwnProperty(nCampo)) {
      //     console.log(nCampo, ' ', formGroup.value[nCampo]);
      //   }
      // }
      let lCama: any = [];
      if (formGroup.value.camarista || formGroup.value.camarista === 0) {
        lCama = filterBy(this.ttTcamaoper, {
          logic: 'and',
          filters: [{ field: 'camarista', operator: 'eq', value: formGroup.value.camarista }]
        })
        if (lCama.length >= 1 || formGroup.value.camarista === 0) {
          this.assignValues(dataItem, formGroup.value);
          this.assignValues(dataItem, { 'c-mod': 'X' });
          this.update(dataItem);
        } else {
          swal('Error!', 'Camarista No Existe!!', 'error');
        }
      } else {
        this.assignValues(dataItem, formGroup.value);
      }
    }
  }

  public createFormGroup(dataItem: any): FormGroup {
    return this.formBuilder.group({
      'camarista': dataItem.camarista,
    });
  }

  public colorCode(code: number): SafeStyle {
    let result;
    result = code > 0 ?
      'padding: 0.75rem 0.75rem; display: block; background-color: #B2F699' : 'padding: 0.75rem 0.75rem; background-color: transparent';
    return this.sanitizer.bypassSecurityTrustStyle(result);
  }

  viewDetail(window: any) {
    switch (window) {
      case 'cmrsa':
        this.flagViewDetail = true;
        break;
      case 'cmropr':
        this.flagViewCamaOper = true;
        break;
      default:
        break
    }
  }

  public dataReporte(asigPdf: any = []) {
    if (asigPdf.length > 0) {
      asigPdf = filterBy(asigPdf, {
        logic: 'and',
        filters: [{ field: 'camarista', operator: 'neq', value: 0 }]
      })
    }
    return asigPdf;
  }
  public camaReporte() {
    let asigCamaPdf: any;
    asigCamaPdf = {
      data: orderBy(this.ttTcamaristas, [{ field: 'camarista', dir: 'asc' }])
    };
    return asigCamaPdf.data;
  }
  public countData(data, search, param, oper: string = 'eq') {
    const result = filterBy(data, {
      logic: 'and',
      filters: [{ field: search, operator: oper, value: param }]
    })
    return result.length;
  }
  public campoNomHuesped() {
    return this.myFormGroup.value.lLog10 === true ?
      this.pdfGrid.concat(this.pdfGridFields, this.gridFields) : this.pdfGridFields;
  }

  public printPDF() {
    const columns = this.campoNomHuesped();
    this._pdfService.printAsignaCamaPdf(this.seccion, columns, this.dataReporte(this.dataGrid.data), this.listCamaRepo);
  }
  public exportExcel() {
    const tbl = document.getElementById('tblExcel');
    const wb = XLSX.utils.table_to_book(tbl);
    XLSX.writeFile(wb, 'Reporte ' + this.seccion + '.xlsx');
  }

  // Cierra ventana en Kendo Window
  public close(component) {
    this[component + 'Opened'] = false;
    switch (component) {
      case 'cmrsa':
        this.flagViewDetail = false;
        break;
      case 'cmropr':
        this.flagViewCamaOper = false;
        break;
      default:
        break
    }
  }
  // Procesa la grid a mostrar cuando se agrupa por un campo. Grid debe tener Groupable = true
  public groupChange(groups: GroupDescriptor[]): void {
    this.groups = groups;
    this.dataGrid = process(this.dataComplete, { group: this.groups });
  }
  // Procesa la grid cuando se filtra en un campo. Grid debe tener Filterable = true
  public filterChange(filter: CompositeFilterDescriptor): void {
    this.filter = filter;
    this.dataGrid.data = filterBy(this.dataComplete, filter);
  }
  // Procesa la grid en asc o desc en el campo seleccionado
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.dataGrid = {
      data: orderBy(this.dataGrid.data, this.sort),
      total: this.dataGrid.data.length
    };
  }

}
=======
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { Router } from '@angular/router';
import {
  SortDescriptor, orderBy, filterBy, CompositeFilterDescriptor, aggregateBy
} from '@progress/kendo-data-query';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { CrudService } from '../../mantto-srv.services';
import { Tgeneral } from '../../../models/tbl-general';
import { PdfService } from '../../mantto-pdf.service';
import { GLOBAL } from '../../../services/global';
import * as XLSX from 'xlsx';
import moment from 'moment';
import swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';
const toast = GLOBAL.toast;


// Esta función se ejecuta cuando en la grid el parametro [filterable] es igual a true
const createFormCamaSec = dataItem => new FormGroup({
  'seccion': new FormControl(dataItem.seccion)
});
const createFormCamaOper = dataItem => new FormGroup({
  'camarista': new FormControl(dataItem.camarista)
});

@Component({
  selector: 'app-asignacion-trabajo',
  templateUrl: './asignacion-trabajo.component.html',
  styleUrls: ['../../manttos-desing.css']
})
export class AsignacionTrabajoComponent implements OnInit {
  heading = 'Asignación de Trabajo';
  subheading: string;
  icon: string;
  @ViewChild('modalCam') modalCam: TemplateRef<any>;
  loading = false;
  public formSend: Tgeneral;
  public ngForm: FormGroup;
  public dsTcuartos: any = []; // Tipos de Cuarto activos
  public tipoCuartos: any = []; // Filtro Tipos de Cuarto
  public tCuartoElement: any = []; // Arreglo base busqueda Tipo Cuarto
  public dsIngagrp: any = []; // Ingreso Agrupado
  public tipoIngreso: any = []; // Filtro Ingreso Agrupado
  public tIngresoElement: any = []; // Arreglo base busqueda Tipo Ingreso Agrupado
  public dTBusqueda: any = [];
  public ttTcamaristas: any = [];
  public ttTcamaoper: any = [];
  public params: any;
  public infoParametros: any = [];
  public infoSecciones: any = [];
  // Variables para vincular con componentes de Kendo
  public dataGrid: any = [];
  public dataCam: any = [];
  public dataComplete: any = [];
  // Variables para obtener información del usuario y del hotel
  public ttUsuario: any = [];
  public ttInfocia: any = [];
  // Variables para funciones de Kendo Grid
  public filter: CompositeFilterDescriptor;
  public sort: SortDescriptor[] = [{ field: 'cuarto', dir: 'asc' }]; // Order asc o desc
  public aggregates: any[] = [ // Totalizar columnas de la Grid
    { field: 'tmont1', aggregate: 'sum' },
    { field: 'tmont2', aggregate: 'sum' },
    { field: 'tmont3', aggregate: 'sum' }
  ];
  // Banderas para ocultar o mostrar
  public flagForm: boolean = true;
  public total: any;
  // Data en codigo duro
  public localizacion = [
    { loc: 'Todo', abrev: 1 }, { loc: 'Hotel', abrev: 2 }, { loc: 'Tiempo Compartido', abrev: 3 }
  ];
  private urlSec: string = '/llaves/asignacion/';
  public gridFields = [];
  public pdfGridFields = [];
  public pdfGrid = [];
  public dtfCamaristas = [];
  public dtfCamaOper = [];
  public listCamaRepo: any = [];
  incFI = false; // Inc F/I
  incCtoSCred = false; // Inc Ctos S/Cred
  ocup = false; // Edos. Ocupados
  cout = false; // Check Out
  vacsuc = false; // Vacantes Sucios
  camc = false; // Cambios de cuarto
  disc = false; // Discrepancias
  fserv = false; // Fuera de Servicio
  all = false; // Todos
  public iagrpEstatus: any = null;
  dFcxc: number;
  iFcam: any;
  dFcasig: any;
  dsEstcam: any;
  norecords = GLOBAL.noRecords;
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  loadingColor: any = GLOBAL.loadingColor;
  step = 0;
  dataTotal: any;
  api: Array<string> = ['/manttos/camaristas', 'siCamarista', 'dsCamaristas', 'ttCamaristas'];
  dsCamaristas: any = [];
  cont: number;

  constructor(private _crudservice: CrudService,
    private _pdfService: PdfService,
    private _info: InfociaGlobalService,
    private _formBuilder: FormBuilder,
    private _sanitizer: DomSanitizer,
    private _menuServ: ManttoMenuService,
    private _modal: NgbModal,
    private http: HttpClient,
    private _router: Router) { }

  ngOnInit() {
    this.initialData();
    this.buildFormGroup();
    this.setupMenu();
  }

  private buildFormGroup() {
    this.ngForm = this._formBuilder.group({
      iInt1: [1, Validators.required],
      lLog1: [true],
      lLog2: [true],
      lLog3: [true],
      lLog4: [true],
      lLog5: [true],
      lLog6: [false],
      lLog7: [false],
      lLog8: [false],
      lLog9: [false],
      lLog10: [true],
      lLog11: [true],
      iFcam: [0],
      dFcasig: [0],
      dFcxc: [0],
      tcuartos: [''],
      iagrp: [''],
      fecha: [moment(this.ttInfocia.fday).toDate()]
    });

    this.onChangesForm();
  }
  onChangesForm() {
    this.ngForm.get('lLog9').valueChanges.subscribe(change => {
      if (change) {
        this.ngForm.get('lLog3').disable();
        this.ngForm.get('lLog4').disable();
        this.ngForm.get('lLog5').disable();
        this.ngForm.get('lLog6').disable();
        this.ngForm.get('lLog7').disable();
        this.ngForm.get('lLog8').disable();
      } else {
        this.ngForm.get('lLog3').enable();
        this.ngForm.get('lLog4').enable();
        this.ngForm.get('lLog5').enable();
        this.ngForm.get('lLog6').enable();
        this.ngForm.get('lLog7').enable();
        this.ngForm.get('lLog8').enable();
      }
    });

  }

  public getInfo() {
    this.loading = true;
    this.step = 1;
    const val = this.ngForm.value;
    const local = val.iInt1.abrev;
    this.all = val.lLog9; // Todos los Estados de Cuarto
    this.incFI = val.lLog1;
    this.incCtoSCred = val.lLog2;
    const fecha = moment(val.fecha).format(GLOBAL.formatIso);
    if (this.all) {
      this.ocup = false; // Edos. Ocupados
      this.cout = false; // Check Out
      this.vacsuc = false; // Vacantes Sucios
      this.camc = false; // Cambios de cuarto
      this.disc = false; // Discrepancias
      this.fserv = false; // Fuera de Servicio
    } else {
      this.ocup = val.lLog3; // Edos. Ocupados
      this.cout = val.lLog4; // Check Out
      this.vacsuc = val.lLog5; // Vacantes Sucios
      this.camc = val.lLog6; // Cambios de cuarto
      this.disc = val.lLog7; // Discrepancias
      this.fserv = val.lLog8; // Fuera de Servicio
    }
    this.formSend = new Tgeneral(
      '', '', '', '', '', '', '', '', '', '', '',
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      fecha, null, null, null, null, null, null, null, null, null, null,
      local, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      this.incFI, this.incCtoSCred, this.ocup, this.cout, this.vacsuc, this.camc, this.disc, this.fserv, this.all, false, false,
      null, null, null, null, null, null, null, null, null, null, null
    );

    this.tipoCuartos = [];
    if (val.tcuartos) {
      val.tcuartos.forEach(obj => {
        this.tCuartoElement = { 'ttcto': obj.tcuarto };
        this.tipoCuartos.push(this.tCuartoElement);
      })
    }

    this.tipoIngreso = [];
    if (val.iagrp && val.iagrp.length > 0) {
      this.tipoCuartos = [];
      val.iagrp.forEach(obj => {
        this.tIngresoElement = { 'iagrp': obj.iagrp };
        this.tipoIngreso.push(this.tIngresoElement);
      })
    }
    this.update('B');
  }

  ingActivo(inga) {
    this.iagrpEstatus = (inga.length > 0) ? true : null;
  }


  public update(accion: any): void {
    this.loading = true;

    switch (accion) {
      case 'B':
        const json = JSON.stringify(this.formSend);
        const jsonTcuarto = JSON.stringify(this.tipoCuartos);
        const jsonTingreso = JSON.stringify(this.tipoIngreso);
        this.params = '{"dsEstcam":{"ttTgeneral":[' + json + '], "ttTcto":' + jsonTcuarto + ', "ttTiagrp":' + jsonTingreso + ' }}';
        break;
      case 'A':
        this.assignValues(this.dTBusqueda, { ttTgeneral: [{ iInt1: this.dFcasig, iInt2: this.dFcxc }] });
        this.params = JSON.stringify(this.dTBusqueda);
        break;
      case 'I':
        this.assignValues(this.dTBusqueda, { ttTgeneral: [{ iInt1: this.dFcasig }] });
        this.params = JSON.stringify(this.dTBusqueda);
        break;
      default:
        this.assignValues(this.dataGrid, { ttTcuarto: [accion] });
        this.params = JSON.stringify(this.dTBusqueda);
        accion = '';
        break
    }

    this._crudservice.putInfoReporte(this.urlSec + accion, this.params)
      .subscribe(data => {
        const result = data.siEstcam.dsEstcam
        this.params = {};
        this.loading = false;
        this.dTBusqueda = result;
        this.dataTotal = result.ttTotal[0];
        this.iFcam = this.dataTotal.iFcam;
        this.dFcasig = this.dataTotal.dFcasig;
        this.dFcxc = this.infoParametros === undefined ? 1 : this.infoParametros[0].pchr1;

        this.loading = false;

        if (result.ttTcuarto) {
          this.dataGrid = result.ttTcuarto;
          this.dataCam = result.ttTcam;
          this.dataComplete = result.ttTcuarto;
          this.total = aggregateBy(this.dataComplete, this.aggregates);
          this.flagForm = false;
          if (result.ttTcam) {
            this.ttTcamaristas = result.ttTcam;
          }
          this.listCamaRepo = this.camaReporte();
        } else {
          this.dataGrid = [];
          this.dataComplete = [];
          this.dataCam = [];
          toast('Error!', 'No se encontraron registros!!', 'error');
        }
      });
  }

  public assignValues(target: any, source: any): void {
    return Object.assign(target, source);
  }

  public cellClickHandler({ sender, rowIndex, columnIndex, dataItem, isEdited }) {
    if (!isEdited) {
      let formSection;
      formSection = (dataItem.cuarto) ? createFormCamaOper(dataItem) : createFormCamaSec(dataItem);
      sender.editCell(rowIndex, columnIndex, formSection);
    }
  }

  public cellCloseHandler(args: any) {
    const { formGroup, dataItem } = args;
    if (!formGroup.valid) {
      // prevent closing the edited cell if there are invalid values.
      args.preventDefault();
    } else if (formGroup.dirty) {
      let lCama: any = [];
      if (formGroup.value.camarista || formGroup.value.camarista === 0) {
        lCama = filterBy(this.ttTcamaoper, {
          logic: 'and',
          filters: [{ field: 'camarista', operator: 'eq', value: formGroup.value.camarista }]
        })
        if (lCama.length >= 1 || formGroup.value.camarista === 0) {
          this.assignValues(dataItem, formGroup.value);
          this.assignValues(dataItem, { 'c-mod': 'X' });
          this.update(dataItem);
        } else {
          toast('Error!', 'Camarista No Existe!!', 'error');
        }
      } else {
        this.assignValues(dataItem, formGroup.value);
      }
    }
  }

  public createFormGroup(dataItem: any): FormGroup {
    return this._formBuilder.group({
      'camarista': dataItem.camarista,
    });
  }

  public colorCode(code: number): SafeStyle {
    let result;
    result = code > 0 ?
      'padding: 0.75rem 0.75rem; display: block; background-color: #B2F699' : 'padding: 0.75rem 0.75rem; background-color: transparent';
    return this._sanitizer.bypassSecurityTrustStyle(result);
  }

  viewDetail() {
    this._crudservice.getData(this.api[0])
      .map(data => data = data[this.api[1]][this.api[2]][this.api[3]])
      .subscribe(data => {
        this.dsCamaristas = data;
        this._modal.open(this.modalCam, { size: 'lg', centered: true });
      });
  }

  public dataReporte(asigPdf: any = []) {
    if (asigPdf.length > 0) {
      asigPdf = filterBy(asigPdf, {
        logic: 'and',
        filters: [{ field: 'camarista', operator: 'neq', value: 0 }]
      })
    }
    return asigPdf;
  }

  public camaReporte() {
    let asigCamaPdf: any;
    asigCamaPdf = {
      data: orderBy(this.ttTcamaristas, [{ field: 'camarista', dir: 'asc' }])
    };
    return asigCamaPdf.data;
  }

  public countData(data, search, param, oper: string = 'eq') {
    const result = filterBy(data, {
      logic: 'and',
      filters: [{ field: search, operator: oper, value: param }]
    })
    return result.length;
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  public campoNomHuesped() {
    return this.ngForm.value.lLog10 === true ?
      this.pdfGrid.concat(this.pdfGridFields, this.gridFields) : this.pdfGridFields;
  }

  public printPDF() {
    let nombre = '';
    if (this.ngForm.value.lLog11) {
    nombre = '005-ASIGNA-TRAB-GLOB';
    const form = JSON.parse(JSON.stringify(this.ngForm.value));
    form.fFec1 = moment(form.fFec1).format(this.formatMoment);
    form.fFec2 = moment(form.fFec2).format(this.formatMoment);
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: nombre,
      dataGrid: this.dataGrid,
      dataCam: this.dataCam,
      infocia: this.ttInfocia,
      usuario: this.ttUsuario,
      hora: moment().format('HH:mm:ss'),
      form: form,
      tCuartos: 0,
      tCreditos: 0,
      totArray: this.dataCam.length,
      contArray: 0
    }
    this.http.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = this.heading + '.pdf';
        download.click();
      },
      error => {
        swal(GLOBAL.errorPdf[0],
          GLOBAL.errorPdf[1], 'info');
      }
    );
    }else{
    const columns = this.campoNomHuesped();
      this._pdfService.printAsignaCamaPdf(this.heading, columns, this.dataReporte(this.dataGrid), this.listCamaRepo);
   }
  }

  public exportExcel() {
    const tbl = document.getElementById('tblExcel');
    const wb = XLSX.utils.table_to_book(tbl);
    XLSX.writeFile(wb, 'Reporte ' + this.heading + '.xlsx');
  }

  initialData() {
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();

    this.pdfGridFields = [
      { title: 'Seccion', dataKey: 'seccion' },
      { title: 'Cuarto', dataKey: 'cuarto' },
      { title: 'Tipo Cuarto', dataKey: 'tcuarto' },
      { title: 'Creditos', dataKey: 'ccred' },
      { title: 'Fec. Sal.', dataKey: 'cfsal' },
      { title: 'Ad', dataKey: 'cadul' },
      { title: 'Jr', dataKey: 'cjun' },
      { title: 'Men', dataKey: 'cmen' },
      { title: 'Rec', dataKey: 'cerecep' },
      { title: 'AL', dataKey: 'cellaves' },
      { title: 'Disc', dataKey: 'cdisc' },
      { title: 'Sis', dataKey: 'cecomp' },
      { title: 'Rev', dataKey: 'cerev' },
      { title: 'Bot', dataKey: 'ceboton' },
      { title: 'F/I', dataKey: 'cfi' },
      { title: 'F/S', dataKey: 'cfs' },
      { title: 'Razón', dataKey: 'crazon' },
      { title: 'Vip', dataKey: 'cvip' },
      { title: 'Hora', dataKey: 'chora' },
    ];

    this.gridFields = [
      { title: 'Nombre Huesped', dataKey: 'cnh' }
    ];

    this.dtfCamaristas = [
      { title: 'C/O', dataKey: 'camarista', width: '120', filtro: true },
      { title: 'Nombre', dataKey: 'cnombre', width: '', filtro: true },
      { title: 'Sección', dataKey: 'seccion', width: '', filtro: true, editor: '' },
      { title: 'Creditos', dataKey: 'ccred', width: '', filtro: true }
    ];

    this.dtfCamaOper = [
      { title: 'C/O', dataKey: 'camarista', width: '120', filtro: true },
      { title: 'Nombre', dataKey: 'cnombre', width: '', filtro: true },
      { title: 'Sección', dataKey: 'seccion', width: '', filtro: true },
      { title: 'Descanso', dataKey: 'cDiaD', width: '', filtro: true },
      { title: 'Cubre Turno', dataKey: 'cCturno', width: '', filtro: true },
      { title: 'Local', dataKey: 'clocal', width: '', filtro: true },
      { title: 'Labora', dataKey: 'clab', width: '', filtro: true },
    ];

    this._crudservice.getData('/manttos/tcuartos')
      .map(data => data = data.siTcuarto.dsTcuartos.ttTcuartos)
      .subscribe(data => {
        this.dsTcuartos = data;
        this.dsTcuartos = filterBy(this.dsTcuartos, {
          logic: 'and',
          filters: [{ field: 'inactivo', operator: 'eq', value: false }]
        });
      });
    this._crudservice.getData('/manttos/ingagrp')
      .map(data => data = data.siIngagrp.dsIngagrp.ttIngagrp).subscribe(data => {
        this.dsIngagrp = data;
      });
    this._crudservice.getData('/manttos/camaristas')
      .map(data => data = data.siCamarista.dsCamaristas.ttCamaristas).subscribe(data => {
        if (data) {
          this.ttTcamaoper = data;
          this.ttTcamaoper.push({ camarista: 0 });
        }
      });
    this._crudservice.getData('/ayuda/parametros®llaves')
      .subscribe(data => this.infoParametros = data.siAyuda.dsAyuda.ttParametros);
    this._crudservice.getData('/manttos/secciones')
      .subscribe(data => this.infoSecciones = data.siSeccion.dsSecciones.ttSecciones);
  }

  setupMenu() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  deleteCamarista(index) {
    swal({
      title: '¿Deseas borrar camarista y cuartos asignados?', type: 'question',
      showCancelButton: true, confirmButtonText: 'Si', cancelButtonText: 'No',
    }).then((result) => {
      if (result) {
        this.dataGrid.forEach(item => {
          if (item.camarista === this.ttTcamaristas[index].camarista) {
            item.camarista = 0;
            item.cnombre = '';
          }
        });
        this.ttTcamaristas.splice(index, 1);
      }
    });
  }

  addCamarista(camarista) {
    let lExiste = false;
    this.ttTcamaristas.forEach(item => {
      if (item.camarista === camarista.camarista) {
        lExiste = true;
      }
    });
    if (!lExiste) {
      this.ttTcamaristas.push(camarista);
    }
  }
}
>>>>>>> dev-alberto
