import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { CrudService } from '../../mantto-srv.services';
import { zip } from 'rxjs/observable/zip';

const CREATE_ACTION = 'create';
const UPDATE_ACTION = 'update';
const REMOVE_ACTION = 'destroy';

const itemIndex = (item: any, data: any[]): number => {
  for (let idx = 0; idx < data.length; idx++) {
    if (data[idx].cuarto === item.cuarto) {
      return idx;
    }
  }
  return -1;
};

const cloneData = (data: any[]) => data.map(item => Object.assign({}, item));
@Injectable()
export class EditInLine extends BehaviorSubject<any[]> {
  private data: any[] = [];
  private originalData: any[] = [];
  private createdItems: any[] = [];
  private updatedItems: any[] = [];
  private deletedItems: any[] = [];
  private tarjet: any = [];
  constructor(
    private http: Http,
    private _crudservice: CrudService) {
    super([]);
  }

  public read() {
    if (this.data.length) {
      return super.next(this.data);
    }
  }

  public create(item: any): void {
    this.createdItems.push(item);
    this.data.unshift(item);
    super.next(this.data);
  }

  public update(item: any): void {
    if (!this.isNew(item)) {
      const index = itemIndex(item, this.updatedItems);
      if (index !== -1) {
        this.updatedItems.splice(index, 1, item);
      } else {
        this.updatedItems.push(item);
      }
    } else {
      const index = this.createdItems.indexOf(item);
      this.createdItems.splice(index, 1, item);
    }
  }

  public remove(item: any): void {
    let index = itemIndex(item, this.data);
    this.data.splice(index, 1);
    index = itemIndex(item, this.createdItems);
    if (index >= 0) {
      this.createdItems.splice(index, 1);
    } else {
      this.deletedItems.push(item);
    }
    index = itemIndex(item, this.updatedItems);
    if (index >= 0) {
      this.updatedItems.splice(index, 1);
    }
    super.next(this.data);
  }

  public isNew(item: any): boolean {
    return !item.cuarto;
  }

  public hasChanges(): boolean {
    return Boolean(this.deletedItems.length || this.updatedItems.length || this.createdItems.length);
  }

  public saveChanges(): void {
    if (!this.hasChanges()) {
      return;
    }
    const completed = [];
    if (this.deletedItems.length) {
      completed.push(
        // this.fetch(REMOVE_ACTION, this.deletedItems)
        console.log('REMOVE_ACTION: ' + this.deletedItems)
      );
      console.log(JSON.stringify(this.deletedItems));
    }
    if (this.updatedItems.length) {
      completed.push(
        // this.fetch(UPDATE_ACTION, this.updatedItems)
        console.log('UPDATE_ACTION: ' + this.updatedItems)
      );
      console.log(JSON.stringify(this.updatedItems));
    }
    if (this.createdItems.length) {
      completed.push(
        // this.fetch(CREATE_ACTION, this.createdItems)
        console.log('CREATE_ACTION: ' + this.createdItems)
      );
      console.log(JSON.stringify(this.createdItems));
    }
    this.reset();
    // zip(...completed).subscribe(() => this.read());
  }

  public cancelChanges(): void {
    this.reset();

    this.data = this.originalData;
    this.originalData = cloneData(this.originalData);
    super.next(this.data);
  }

  public assignValues(target: any, source: any): void {
    return Object.assign(target, source);
  }

  private reset() {
    this.data = [];
    this.deletedItems = [];
    this.updatedItems = [];
    this.createdItems = [];
  }

  private fetch(action: string = '', data?: any): Observable<any[]> {
    switch (action) {
      case 'create':
        alert('Selected Case Number is 1');
        break;
      case 'update':
        alert('Selected Case Number is 2');
        break;
      case 'destroy':
        alert('Selected Case Number is 1');
        break;
      default:
        alert('Sin Acción Válida');
    }
    return;
    // this.http
    //   .jsonp(`https://demos.telerik.com/kendo-ui/service/Products/${action}?${this.serializeModels(data)}`, 'callback')
    //   .pipe(map(res => <any[]>res));
  }

  private serializeModels(data?: any): string {
    return data ? `&models=${JSON.stringify(data)}` : '';
  }
  

}
