export class FueraServicio {
    constructor(
        public cuarto: string,
        public cfini: string,
        public cfter: string,
        public crazon: string,
        public cuartoh: string,
        public cRowID: string,
        public cErrDes = '',
        public lError = false
    ) {}
}
