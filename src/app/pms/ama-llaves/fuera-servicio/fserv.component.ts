import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { FservService } from './services/fserv.service';
import { GLOBAL } from '../../../services/global';
import { FueraServicio } from './fserv.model';
import moment from 'moment';

declare var jsPDF: any;

@Component({
  selector: 'app-fserv',
  templateUrl: './fserv.component.html',
  styleUrls: ['./fserv.component.css'],
  providers: [FservService]
})
export class FservComponent implements OnInit {
  heading = 'Cuartos Fuera de Servicio';
  subheading: string;
  icon: string;
  norecords = GLOBAL.noRecords;
  loading: boolean = true;
  room = '';
  loadingColor: any = GLOBAL.loadingColor;
  public update: boolean;
  public selectedRow: any;
  public rooms: Array<{ cuarto: string }> = [];
  public gridData: Array<any> = [];
  public ttUsuario: Array<any> = [];
  public fueraServicio: FueraServicio;
  public ttInfocia: any;
  public actualizar: boolean; // Si esta en la edición del formulario
  public toRoom: string;
  public fromRoom: string;
  public toFday: Date;
  public fromFday: Date;
  public crazon: string;
  public totalDays: number;
  public dayOfWeek1: string;
  public dayOfWeek2: string;
  private index1: number;
  private index2: number;
  formatKendo = localStorage.getItem('formatKendo');
  pageSize = GLOBAL.pageSize;
  swal = GLOBAL.swal;

  constructor(public _info: InfociaGlobalService,
    private _fserv: FservService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _modalService: NgbModal) {
    this.ttInfocia = this._info.getSessionInfocia();
    this._fserv.getCuartos().subscribe(data => this.rooms = data);
    this.initialData();
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  newRecord(modal) {
    this.update = false;
    this.dateAndDays(this.ttInfocia.fday, this.ttInfocia.fday);
    this.crazon = '';
    this.toRoom = '';
    this.fromRoom = '';
    this.totalDays = 1;
    this.openModalData(modal);
  }

  dateAndDays(date1, date2) {
    this.toFday = moment(date1).toDate();
    this.fromFday = moment(date2).toDate();
    this.dayOfWeek1 = moment(this.toFday).locale('es').format('dddd');
    this.dayOfWeek2 = moment(this.fromFday).locale('es').format('dddd');
  }


  valueRow({ selectedRows }, modal) {
    this.selectedRow = selectedRows[0].dataItem;
    this.loading = true;
    this.toFday = moment(this.selectedRow.cfini).toDate();
    this.fromFday = moment(this.selectedRow.cfter).toDate();
    this.dateAndDays(this.selectedRow.cfini, this.selectedRow.cfter);
    this.totalDays = this._info.totalDaysTwoDates(this.selectedRow.cfini, this.selectedRow.cfter);
    this.totalDays = this.totalDays + 1;
    this.toRoom = this.selectedRow.cuarto;
    this.fromRoom = this.selectedRow.cuarto;
    this.crazon = this.selectedRow.crazon;
    this.update = true;
    this.room = this.selectedRow.cuarto;
    this.loading = false;
    this.openModalData(modal);
  }

  toRoomValue(value: any) {
    this.fromRoom = value;
    let a = 0;
    for (const val of this.rooms) {
      if (val.cuarto === value.cuarto) {
        this.index1 = a;
      }
      a++;
    }
  }

  fromRoomValue(value) {
    let a = 0;
    const room1 = value;
    for (const val of this.rooms) {
      if (val.cuarto === value.cuarto) {
        this.index2 = a;
      }
      a++;
    }
    if (this.index2 < this.index1) {
      this.toRoom = room1;
    }
  }

  toDateValue(value: Date) {
    this.fromFday = value;
    this.totalDays = 1;
    this.dateAndDays(moment(this.fromFday).format(GLOBAL.formatIso), moment(this.fromFday).format(GLOBAL.formatIso));
  }

  fromDateValue(value: Date) {
    const fday1 = moment(this.toFday).format(GLOBAL.formatIso);
    const fday2 = moment(this.fromFday).format(GLOBAL.formatIso);
    this.totalDays = this._info.totalDaysTwoDates(fday1, fday2);
    this.totalDays = this.totalDays + 1;
    this.dateAndDays(fday1, fday2);
  }

  totalOfDays(days, ope?: string) {
    if (days !== '') {
      parseInt(days, 10);

      if (ope === '+') {
        days++;
      } else if (ope === '-') {
        days--;
      }

      this.totalDays = days;

      if (days < 0) {
        this.totalDays = 1;
        return;
      }

      const fday1 = moment(this.toFday).format(GLOBAL.formatIso);
      const fday2: any = moment(fday1, GLOBAL.formatIso).add(days - 1, 'days').toDate();
      this.fromFday = fday2;
      this.dateAndDays(fday1, moment(this.fromFday).format(GLOBAL.formatIso));
    }
  }

  saveRecord(form: NgForm) {
    this.loading = true;

    if (this.update) { // Si se actualiza enviar al metodo updateRecord
      this.updateRecord(form);
      return;
    }
    const val = form.value;
    const cfter = moment(val.fromFday).format(GLOBAL.formatIso);
    const cuarto = val.toRoom.cuarto;
    const cfini = moment(val.toFday).format(GLOBAL.formatIso);
    if (val.toRoom.cuarto === val.fromRoom.cuarto) { // Si solo se guardara un registro ejecutar aqui.

      this.fueraServicio = new FueraServicio(cuarto, cfini, cfter, val.crazon, '', '');

      this._fserv.postCuartoFso(this.fueraServicio).subscribe(data => {
        if (data[0].cMensTxt) {
          this.swal(data[0].cMensTit, data[0].cMensTxt, 'error');
        } else {
          this.initialData();
          this._modalService.dismissAll();
          form.reset();
        }
        this.loading = false;
      });
    }else {
      let a = 0;
      for (const val2 of this.rooms) {
        if (a >= this.index1) {
          if (a > this.index2) {
            return;
          }
          this.fueraServicio = new FueraServicio(val2.cuarto, cfini, cfter, val.crazon, '', '');
  
          this._fserv.postCuartoFso(this.fueraServicio).subscribe(data => {
            if (data[0].cMensTxt) {
              this.swal(data[0].cMensTit, data[0].cMensTxt, 'error');
            } else {
              this.initialData();
              form.reset();
              this._modalService.dismissAll();
            }
            this.loading = false;
          });
        }
        a++;
      }
    }

  }

  updateRecord(form: NgForm) {
    const val = form.value;
    const cfter = moment(val.fromFday).format(GLOBAL.formatIso);
    const cuarto = this.selectedRow.cuarto;
    const cfini = moment(this.selectedRow.cfini).format(GLOBAL.formatIso);
    const cRowID = this.selectedRow.cRowID;

    this.fueraServicio = new FueraServicio(cuarto, cfini, cfter, val.crazon, '', cRowID);

    this._fserv.putCuartosFso(this.fueraServicio).subscribe(data => {
      if (data[0].cMensTxt) {
        this.swal(data[0].cMensTit, data[0].cMensTxt, 'error');
      } else {
        this.initialData();
        form.reset();
        this._modalService.dismissAll();
      }
      this.loading = false;
    });
  }

  deleteRecord(dataItem) { // Comprobar
    // this.loading = true;
    this.gridData = [];
    this._fserv.deteleCuartoFso(dataItem).subscribe(data => {
      if (data) {
        if (data[0].cMensTxt) {
          this.swal(data[0].cMensTit, data[0].cMensTxt, 'error');
        } else {
          this.initialData();
        }
      }
      // this.loading = false;
    });
  }

  printPDF() {
    const encabezado = this.ttInfocia.encab;
    const columns = [
      { title: 'Cuarto', dataKey: 'cuarto' },
      { title: 'Fecha Inicio', dataKey: 'cfini' },
      { title: 'Fecha Termino', dataKey: 'cfter' },
      { title: 'Razón', dataKey: 'crazon' }
    ];
    const rows = this.gridData;
    const doc = new jsPDF('p', 'pt');
    doc.setFontSize(8);
    doc.text(encabezado, 40, 30);
    doc.setFontType('normal');
    doc.setFontSize(8);
    doc.setTextColor(100);
    doc.autoTable(columns, rows, {
      theme: 'striped',
      styles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 8,
        font: 'helvetica' // helvetica, times, courier
      },
    });
    doc.output('save', 'Fuera de Servicio.pdf');
  }

  openModalData(content) {
    this._modalService.open(content, { size: 'md', centered: true });
  }

  initialData() {
    this._fserv.getCuartosFso().subscribe(data => {
      this.loading = false;
      if (data) {
        this.gridData = data;
      } else {
        this.gridData = [];
      }
    });
  }
}
