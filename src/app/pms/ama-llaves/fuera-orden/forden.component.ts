import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { FordenService } from './services/forden.service';
import { GLOBAL } from '../../../services/global';
import { FueraOrden } from './forden.model';
import moment from 'moment';
declare var jsPDF: any;

@Component({
  selector: 'app-forden',
  templateUrl: './forden.component.html',
  styleUrls: ['./forden.component.css'],
  providers: [FordenService]
})
export class FordenComponent implements OnInit {
  heading = 'Cuartos Fuera de Orden';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';

  norecords = GLOBAL.noRecords;
  swal = GLOBAL.swal;
  public rooms: Array<{ cuarto: string }> = [];
  room = '';
  public gridData: Array<any> = [];
  private selectedRow: FueraOrden;
  private fueraOrden: FueraOrden;
  private ttInfocia: any;
  public update: boolean; // Si esta en la edición del formulario
  public loading: boolean = true; // Spinner de espera
  public toRoom: string;
  public fromRoom: string;
  public toFday: Date;
  public fromFday: Date;
  public crazon: string;
  public totalDays: number;
  public dayOfWeek1: string;
  public dayOfWeek2: string;
  private index1: number;
  private index2: number;
  formatKendo = localStorage.getItem('formatKendo');
  loadingColor: any = GLOBAL.loadingColor;
  pageSize = GLOBAL.pageSize;

  constructor(public _info: InfociaGlobalService,
    private _fserv: FordenService,
    private _menuServ: ManttoMenuService,
    private _router: Router,
    private _modalService: NgbModal) {
    this._fserv.getCuartosFso().subscribe(data => {
      if (data) {
        this.gridData = data;
        for (let i = 0; i < this.gridData.length; i++) {
          this.gridData[i].cfini = moment(this.gridData[i].cfini).format(GLOBAL.formatIso);
          this.gridData[i].cfter = moment(this.gridData[i].cfter).format(GLOBAL.formatIso);
        }
      } else {
        this.gridData = [];
      }

      this.loading = false;
    });

    this._fserv.getCuartos().subscribe(data => this.rooms = data);
    this.ttInfocia = this._info.getSessionInfocia();
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  newRecord(modal) {
    this.update = false;
    this.dateAndDays(this.ttInfocia.fday, this.ttInfocia.fday);
    this.crazon = '';
    this.toRoom = '';
    this.fromRoom = '';
    this.totalDays = 1;
    this.openModalData(modal);
  }

  dateAndDays(date1, date2) {
    this.toFday = moment(date1).toDate();
    this.fromFday = moment(date2).toDate();
    this.dayOfWeek1 = moment(this.toFday).locale('es').format('dddd');
    this.dayOfWeek2 = moment(this.fromFday).locale('es').format('dddd');
  }

  valueRow(event: any, modal) {
    this.loading = true;
    this.selectedRow = event.selectedRows[0].dataItem;
    this.toFday = this._info.toDate(this.selectedRow.cfini);
    this.fromFday = this._info.toDate(this.selectedRow.cfter);
    this.dateAndDays(this.selectedRow.cfini, this.selectedRow.cfter);
    this.totalDays = this._info.totalDaysTwoDates(this.selectedRow.cfini, this.selectedRow.cfter);
    this.totalDays = this.totalDays + 1;
    this.toRoom = this.selectedRow.cuarto;
    this.fromRoom = this.selectedRow.cuarto;
    this.crazon = this.selectedRow.crazon;
    this.update = true;
    this.room = this.selectedRow.cuarto;
    this.loading = false;
    this.openModalData(modal);
  }

  toRoomValue(value: any) {
    this.fromRoom = value;
    let a = 0;
    for (const val of this.rooms) {
      if (val.cuarto === value.cuarto) {
        this.index1 = a;
      }
      a++;
    }
  }

  fromRoomValue(value) {
    let a = 0;
    const room1 = value;
    for (const val of this.rooms) {
      if (val.cuarto === value.cuarto) {
        this.index2 = a;
      }
      a++;
    }
    if (this.index2 < this.index1) {
      this.toRoom = room1;
    }
  }

  toDateValue(value: Date) {
    this.fromFday = value;
    this.totalDays = 1;
    this.dateAndDays(moment(this.fromFday).format(GLOBAL.formatIso), moment(this.fromFday).format(GLOBAL.formatIso));
  }

  fromDateValue(value: Date) {
    const fday1 = moment(this.toFday).format(GLOBAL.formatIso);
    const fday2 = moment(this.fromFday).format(GLOBAL.formatIso);
    this.totalDays = this._info.totalDaysTwoDates(fday1, fday2);
    this.totalDays = this.totalDays + 1;
    this.dateAndDays(fday1, fday2);
  }

  plusDays() {
    this.totalDays = this.totalDays + 1;
    this.totalOfDays(this.totalDays);
  }

  minusDays() {
    this.totalDays = this.totalDays - 1;
    this.totalOfDays(this.totalDays);
  }

  totalOfDays(days: number) {
    if (days <= 0) {
      this.totalDays = 1;
      return;
    }
    const fday1 = moment(this.toFday).format(GLOBAL.formatIso);
    const fday2: any = moment(fday1, GLOBAL.formatIso).add(days - 1, 'days').toDate();
    this.fromFday = fday2;
    this.dateAndDays(moment(fday1, GLOBAL.formatIso).format(GLOBAL.formatIso), moment(this.fromFday).format(GLOBAL.formatIso));
  }

  saveRecord(form: NgForm) {
    this.loading = true;

    if (this.update) { // Si se actualiza enviar al metodo updateRecord
      this.updateRecord(form);
    } else {
      const val = form.value;
      const cfter = moment(val.fromFday).format(GLOBAL.formatIso);
      const cuarto = val.toRoom.cuarto;
      const cfini = moment(val.toFday).format(GLOBAL.formatIso);
      const toCuarto = val.fromRoom.cuarto;

      this.fueraOrden = new FueraOrden(cuarto, cfini, cfter, val.crazon, toCuarto, '');

      this._fserv.postCuartoFso(this.fueraOrden).subscribe(data => {
        if (data[0].cMensTxt) {
          this.swal(data[0].cMensTit, data[0].cMensTxt, 'error');
        } else {
          this.gridData = data;
          this._modalService.dismissAll();
          form.reset();
        }
        this.loading = false;
      });
    }

  }

  updateRecord(form: NgForm) {
    const val = form.value;
    const cfter = moment(val.fromFday).format(GLOBAL.formatIso);
    const cuarto = this.selectedRow.cuarto;
    const cfini = moment(this.selectedRow.cfini).format(GLOBAL.formatIso);
    const cRowID = this.selectedRow.cRowID;

    this.fueraOrden = new FueraOrden(cuarto, cfini, cfter, val.crazon, '', cRowID);

    this._fserv.putCuartosFso(this.fueraOrden).subscribe(data => {
      if (data[0].cMensTxt) {
        this.swal(data[0].cMensTit, data[0].cMensTxt, 'error');
      } else {
        this.gridData = data;
        form.reset();
      }
      this.loading = false;
    });
  }

  deleteRecord() {
    // this.loading = true;
    this._fserv.deteleCuartoFso(this.selectedRow).subscribe(data => {
      if (data) {
        if (data[0].cMensTxt) {
          this.swal(data[0].cMensTit, data[0].cMensTxt, 'error');
        } else {
          this.gridData = data;
        }
      } else {
        this.gridData = [];
      }
      // this.loading = false;
    });
  }

  printPDF() {
    const encabezado = this.ttInfocia.encab;
    const columns = [
      { title: 'Cuarto', dataKey: 'cuarto' },
      { title: 'Fecha Inicio', dataKey: 'cfini' },
      { title: 'Fecha Termino', dataKey: 'cfter' },
      { title: 'Razón', dataKey: 'crazon' }
    ];
    const rows = this.gridData;
    const doc = new jsPDF('p', 'pt');
    doc.setFontSize(8);
    doc.text(encabezado, 40, 30);
    doc.setFontType('normal');
    doc.setFontSize(8);
    doc.setTextColor(100);
    doc.autoTable(columns, rows, {
      theme: 'striped',
      styles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 8,
        font: 'helvetica' // helvetica, times, courier
      },
    });
    doc.output('save', 'Fuera de Orden.pdf');
  }

  openModalData(content) {
    this._modalService.open(content, { size: 'md', centered: true });
  }

}
