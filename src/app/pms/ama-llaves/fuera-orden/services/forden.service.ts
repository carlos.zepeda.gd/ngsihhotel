import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GLOBAL } from '../../../../services/global';
import 'rxjs/add/operator/map'

const headers = new Headers({ 'Content-Type': 'application/json' });

@Injectable()
export class FordenService {

  public url: string;

  constructor(private http: Http) {
    this.url = GLOBAL.url;
  }

  getCuartosFso() {
    return this.http.get(this.url + '/manttos/ctofso/o')
      .map(res => res.json().response.siCtofso.dsCtofso.ttCtofso);
  }

  getCuartos() {
    return this.http.get(this.url + '/manttos/ctofso/o')
      .map(res => res.json().response.siCtofso.dsCtofso.ttCuartos);
  }

  putCuartosFso(cto) {
    const json = JSON.stringify(cto);
    const params = '{"ttCtofso":[' + json + ']}';

    return this.http.put(this.url + '/manttos/ctofso/o', params, { headers: headers })
      .map(res => {
        const succes = res.json().response.siCtofso.dsCtofso.ttCtofso;
        const error = res.json().response.siCtofso.dsMensajes.ttMensError;
        if (error) {
          return error;
        } else {
          return succes;
        }
      });
  }

  postCuartoFso(cto) {
    const json = JSON.stringify(cto);
    const params = '{"dsCtofso":{"ttCtofso":[' + json + ']}}';

    return this.http.post(this.url + '/manttos/ctofso/o', params, { headers: headers })
      .map(res => {
        const succes = res.json().response.siCtofso.dsCtofso.ttCtofso;
        const error = res.json().response.siCtofso.dsMensajes.ttMensError;
        if (error) {
          return error;
        } else {
          return succes;
        }
      });
  }

  deteleCuartoFso(cto) {
    const json = JSON.stringify(cto);
    const params = '{"ttCtofso":[' + json + ']}';
    const options = new RequestOptions({ headers: headers });
    return this.http.delete(this.url + '/manttos/ctofso/o', { body: params, headers })
      .map(res => {
        const data = [];
        if (!res) {
          return data;
        } else {
          return res.json().response.siCtofso.dsCtofso.ttCtofso;
        }
      });
  }
}
