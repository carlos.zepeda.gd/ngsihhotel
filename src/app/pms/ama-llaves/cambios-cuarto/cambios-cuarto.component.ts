import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { CrudService } from '../../mantto-srv.services';
import { Tgeneral } from '../../../models/tbl-general';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-cambios-cuarto',
  templateUrl: './cambios-cuarto.component.html',
  styleUrls: ['./cambios-cuarto.component.css']
})
export class CambiosCuartoComponent implements OnInit {
  heading = 'Cambios a Estado de Cuartos';
  subheading: string;
  icon: string;
  api: Array<string> = ['/reportes/general/CambiosCuartos', 'dsReporte', 'ttTgeneral'];
  gridData: any = [];
  noRecords = GLOBAL.noRecords;
  textSuccess = GLOBAL.textSuccess;
  textError = GLOBAL.textError;
  pageSize = 10;
  skip = 0;
  formatKendo = localStorage.getItem('formatKendo');
  loading: boolean;
  loadingColor: any = GLOBAL.loadingColor;
  dataGrid = [];
  toast = GLOBAL.toast;
  ngForm: FormGroup;
  ttInfocia: any = [];
  ttUsuario: any = [];
  dsOperadores: any = [];
  dsCuartos: any = [];
  dsTipos: any = [
    { desc: 'Cuarto', tipo: 1 },
    { desc: 'Operador', tipo: 2 }
  ]

  constructor(private _menuServ: ManttoMenuService,
    private _router: Router,
    private _info: InfociaGlobalService,
    private _fb: FormBuilder,
    private _crudservice: CrudService) {
  }

  ngOnInit() {
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
    this._crudservice.getData('/manttos/llaves').subscribe(data => {
      this.dsCuartos = data.siCuarto.dsCuartos.ttCuartos;
    });
    this._crudservice.getData('/ayuda/camaristas').subscribe(data => {
      this.dsOperadores = data.siAyuda.dsAyuda.ttCamaristas;
    });
    this.createForm();
  }

  getDataSource() {
    this.dataGrid = [];
    this.loading = true;
    this._crudservice.getData('').subscribe(res => {
      const result = res.siReporte.dsReporte.tmpCtos;
      if (result) {
        this.dataGrid = result;
      }
      this.loading = false;
    });
  }

  createForm() {
    this.ngForm = this._fb.group({
      tipo: [1],
      fechaIni: [moment(this.ttInfocia.fday).toDate(), Validators.required],
      fechaFin: [moment(this.ttInfocia.fday).toDate(), Validators.required],
      codigo: ['', Validators.required],
    });
  }

  buscar(form) {
    this.loading = true;
    const formSend = new Tgeneral(
      form.codigo, '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      moment(form.fechaIni).format(GLOBAL.formatIso), moment(form.fechaFin).format(GLOBAL.formatIso), null, null, null, null,
      null, null, null, null, null, form.tipo
    );

    this._crudservice.putInfo(formSend, this.api[0], this.api[1], this.api[2]).subscribe(res => {
      const result = res.siReporte.dsReporte.tmpCtos;
      if (result) {
        this.dataGrid = result;
      } else {
        this.dataGrid = [];
      }
      this.loading = false;
    });
  }
}
