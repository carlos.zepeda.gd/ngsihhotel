// Imports Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Modules
import { KendouiModule } from '../../shared/kendo.module';
import { SharedAppsModule } from '../../shared/shared-apps.module';
// Fuera de Orden y Servicio
import { FservComponent } from './fuera-servicio/fserv.component';
import { FordenComponent } from './fuera-orden/forden.component';
// Estado de cuartos
<<<<<<< HEAD
import { HeaderComponent } from './estado-cuartos/components/header/header.component';
import { ActionsComponent } from './estado-cuartos/components/actions/actions.component';
import { FooterComponent } from './estado-cuartos/components/footer/footer.component';
import { RoomComponent } from './estado-cuartos/components/home/home.component';
// Requerimientos Especiales
import { RequirementsComponent } from './requerimientos/components/home/home.component';
import { HeaderReqComponent } from './requerimientos/components/header/header.component';
import { ActionsReqComponent } from './requerimientos/components/actions/actions.component';
import { GridComponent } from './requerimientos/components/grid/grid.component';
import { FormReqComponent } from './requerimientos/components/form-req/form-req.component';
=======
import { ActionsComponent } from './estado-cuartos/components/actions/actions.component';
import { EcFooterComponent } from './estado-cuartos/components/footer/ec.footer.component';
import { RoomComponent } from './estado-cuartos/components/home/home.component';

>>>>>>> dev-alberto
// Ama de Llaves
import { LostFoundComponent } from './lost-found/lost-found.component';
import { CamaristasOperadoresComponent } from './camaristas-operadores/camaristas-operadores.component';
import { AsignacionTrabajoComponent } from './asignacion-trabajo/asignacion-trabajo.component';
// Entrada de Requerimientos
import { EntradaRequerimientosComponent } from './entrada-requerimientos/entrada-requerimientos.component';
// Consulta de Cuartos
import { ConsultaCuartosComponent } from './consulta-cuartos/consulta-cuartos.component';
// Servicios
import { RoomStatusService } from './estado-cuartos/services/estado-cuarto.service';
<<<<<<< HEAD
import { RequirementsService } from './requerimientos/services/requirements.service';
import { EditInLine } from './asignacion-trabajo/asignacion-trabajo.service';
import { TipoCuartosComponent } from './tipo-cuartos/tipo-cuartos.component';
=======

import { EditInLine } from './asignacion-trabajo/asignacion-trabajo.service';
import { RouterModule } from '@angular/router';
import { ArchitectModule } from '../../_architect/architect.module';
import { RequerimientosService } from './requerimientos/requerimientos.service';
import { CambiosCuartoComponent } from './cambios-cuarto/cambios-cuarto.component';
import { MsjsComputadorComponent } from './msjs-computador/msjs-computador.component';
import { AMALLAVES_ROUTING } from './amallaves.routes';
import { EcHeaderComponent } from './estado-cuartos/components/header/ec.header.component';
import { RequerimientosEspComponent } from './requerimientos/requerimientos-esp.component';
import { DisponibilidadRequerimientosEspComponent } from '../consultas/disponibilidad-requerimientos-esp/disponibilidad-requerimientos-esp.component';
import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';
>>>>>>> dev-alberto

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    KendouiModule,
<<<<<<< HEAD
    SharedAppsModule
=======
    SharedAppsModule,
    RouterModule,
    AMALLAVES_ROUTING,
    GridModule,
    ExcelModule,
    ArchitectModule
>>>>>>> dev-alberto
  ],
  declarations: [
    HeaderComponent, // Estado-cuartos
    ActionsComponent, // Estado-cuartos
    FooterComponent, // Estado-cuartos
    RoomComponent, // Estado-cuartos
    RequirementsComponent,
    HeaderReqComponent,
    ActionsReqComponent,
    GridComponent,
    FormReqComponent,
    FservComponent,
    FordenComponent,
    ConsultaCuartosComponent,
    EntradaRequerimientosComponent,
    // Ama de LLaves
    CamaristasOperadoresComponent,
    LostFoundComponent,
    AsignacionTrabajoComponent,
<<<<<<< HEAD
    TipoCuartosComponent,
=======
    CambiosCuartoComponent,
>>>>>>> dev-alberto
  ],
  exports: [
    RequirementsComponent,
    HeaderReqComponent,
    ActionsReqComponent,
    GridComponent
  ],
  providers: [
    ActionsReqComponent,
    RoomStatusService,
    RequirementsService,
    EditInLine
  ],
})
export class AmallavesModule { }
