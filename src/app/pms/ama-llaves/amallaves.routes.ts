import {RouterModule, Routes} from '@angular/router';
import {BaseLayoutComponent} from 'app/_architect/Layout/base-layout/base-layout.component';
import {LostFoundComponent} from './lost-found/lost-found.component';
import {CamaristasOperadoresComponent} from './camaristas-operadores/camaristas-operadores.component';
import {AsignacionTrabajoComponent} from './asignacion-trabajo/asignacion-trabajo.component';
import {FservComponent} from './fuera-servicio/fserv.component';
import {FordenComponent} from './fuera-orden/forden.component';
import {EntradaRequerimientosComponent} from './entrada-requerimientos/entrada-requerimientos.component';
import {CambiosCuartoComponent} from './cambios-cuarto/cambios-cuarto.component';
import {MsjsComputadorComponent} from './msjs-computador/msjs-computador.component';
import {VerifyInfociaGuardService} from 'app/services/verify-infocia-guard.service';
import {ConsultaCuartosComponent} from './consulta-cuartos/consulta-cuartos.component';
import {RoomComponent} from './estado-cuartos/components/home/home.component';
import {RequerimientosEspComponent} from './requerimientos/requerimientos-esp.component';
import {DisponibilidadRequerimientosEspComponent} from '../consultas/disponibilidad-requerimientos-esp/disponibilidad-requerimientos-esp.component';
import { RoutesGuard } from '../../services/routes-guard.service';


const AMALLAVES_ROUTES: Routes = [
  {
    path: '',
    component: BaseLayoutComponent,
    children: [
      {path: 'lost-found', component: LostFoundComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'camaristas-operadores', component: CamaristasOperadoresComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'asignacion-trabajo', component: AsignacionTrabajoComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'fuera-servicio', component: FservComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'fuera-orden', component: FordenComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'entrada-requerimientos', component: EntradaRequerimientosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'requerimientos', component: RequerimientosEspComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'disponibilidad-req-esp', component: DisponibilidadRequerimientosEspComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'cambios-cuarto', component: CambiosCuartoComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'msjs-computador', component: MsjsComputadorComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'consulta-cuartos', component: ConsultaCuartosComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}},
      {path: 'cuartos', component: RoomComponent, canActivate: [RoutesGuard], data: {extraParameter: ''}}
    ],
    canActivateChild: [
      VerifyInfociaGuardService
    ]
  }
];

export const AMALLAVES_ROUTING = RouterModule.forChild(AMALLAVES_ROUTES);

