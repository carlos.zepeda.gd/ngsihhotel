<<<<<<< HEAD
import { Component, OnInit } from '@angular/core';
import { orderBy, SortDescriptor, GroupDescriptor, DataResult, process, State } from '@progress/kendo-data-query';
import { RoomStatusService } from '../estado-cuartos/services/estado-cuarto.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { GridDataResult } from '@progress/kendo-angular-grid';
declare var jsPDF: any;

@Component({
  selector: 'app-consulta-cuartos',
  templateUrl: './consulta-cuartos.component.html',
  styleUrls: ['./consulta-cuartos.component.css']
})
export class ConsultaCuartosComponent implements OnInit {
  public gridData: GridDataResult = { data: [], total: 0 };
  private statusSelected: string = '';
  private tcuartoSelected: string = '';
  private selected: boolean;
  public defaultItem: { cedo: string, cdesc: string } = { cedo: '', cdesc: 'Todos' };
  public defaultItem2 = 'Todos' ;
  public tipoCuarto: any[];
  public status: Array<{ cedo: string, cdesc: string }> = [];
  public loading: boolean;
  // public sort: SortDescriptor[] = [{
  //     field: 'cuarto',
  //     dir: 'asc'
  // }];
  state: State = {
    sort: [{
        field: 'cuarto',
        dir: 'asc'
    }],
    group: [],
};
  constructor(private _room: RoomStatusService, private _info: InfociaGlobalService) { }

  ngOnInit() {
    this.loading = true;
    this._room.allCtoEdos().subscribe(data => {
      if (!data) { data = []; }
      this.status = data;
    });
    const rooms = [];
    this._room.arrayRooms().subscribe( res => {
        for (const room of res) {
          rooms.push(room.tcuarto);
        }
        const val = rooms.filter(function(este, i) {
          return rooms.indexOf(este) === i;
        })
        this.tipoCuarto = val;
        if (!res) { res = []; }
        this.loading = false;
        this.gridData = process(res, this.state);
        // this.gridData = {
        //     data: orderBy(res, this.sort),
        //     total: res.length
        // };
    });
  }

  public toStatusValue($value) {
    this.statusSelected = $value.cedo;
    if (this.statusSelected === 'Todos') {
        this.statusSelected = '';
    }
    this._room.roomsForStatus(this.statusSelected, this.tcuartoSelected).subscribe(res => {
      if (!res) { res = []; }
      this.gridData = process(res, this.state);
    });
  }
  public toRoomValue($value) {
    this.tcuartoSelected = $value;
    if (this.tcuartoSelected === 'Todos') {
        this.tcuartoSelected = '';
    }
    this._room.roomsForStatus(this.statusSelected, this.tcuartoSelected).subscribe(res => {
      if (!res) { res = []; }
      this.gridData = process(res, this.state);
    });
  }
  onGridDataStateChange(state: State) {
    this.state = state;
    this.gridData = process(this.gridData.data, this.state);
    // this._room.roomsForStatus(this.statusSelected, this.tcuartoSelected).subscribe(res => {
    //   if (!res) { res = []; }
    //   this.gridData = process(res, this.state);
    // });
}
  public sortChange(sort: SortDescriptor[]): void {
    // this.sort = sort;
    // this.gridData = {
    //     data: orderBy(this.gridData.data, this.sort),
    //     total: this.gridData.data.length
    // };
    this.gridData = process(this.gridData.data, this.state);
  }
  public orderForStatus(event: string) {

  }
  printPDF() {
    const ttInfocia = this._info.getSessionInfocia();
    const encabezado = ttInfocia.encab;
    const columns = [
        { title: 'Cuarto',   dataKey: 'cuarto'   },
        { title: 'Tipo',     dataKey: 'tcuarto'  },
        { title: 'Rec.',     dataKey: 'cerecep'  },
        { title: 'A.L.',     dataKey: 'cellaves' },
        { title: 'Sist.',    dataKey: 'cecomp'   },
        { title: 'Rev.',     dataKey: 'cerev'    },
        { title: 'Bot.',     dataKey: 'ceboton'  },
        { title: 'Nota',     dataKey: 'cnota'    },
        { title: 'Limpieza', dataKey: 'climp'    }
    ];
    const rows = this.gridData.data;
    const doc = new jsPDF('p', 'pt');
          doc.setFontSize(8);
          doc.text(encabezado, 40, 30);
          doc.setFontType('normal');
          doc.setFontSize(8);
          doc.setTextColor(100);
          doc.autoTable(columns, rows, {
              theme: 'striped',
              styles: {
                  cellPadding: 5,
                  fontSize: 8,
                  font: 'helvetica'
              },
          });
          doc.output('save', 'Estado de Cuartos.pdf');
  }
}
=======
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { CompositeFilterDescriptor, distinct, filterBy, process } from '@progress/kendo-data-query';
import { RoomStatusService } from '../estado-cuartos/services/estado-cuarto.service';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { GLOBAL } from '../../../services/global';

declare var jsPDF: any;

@Component({
  selector: 'app-consulta-cuartos',
  templateUrl: './consulta-cuartos.component.html',
})
export class ConsultaCuartosComponent implements OnInit {
  heading = 'Consulta Estado de Cuartos';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  filter: CompositeFilterDescriptor;
  dataGrid: any = []
  dsEstatus: any = [];
  loading: boolean;
  pageSize = 10;
  skip = 0;
  dataFilter: any[];
  toast = GLOBAL.toast;

  constructor(private _room: RoomStatusService,
    private _info: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _router: Router) {
    this.loading = true;
    this.dataFilter = filterBy([this.dataGrid], this.filter);
    this._room.getCuartos().subscribe(data => {
      if (data.ttCtoedos) {
        data.ttCtoedos.forEach(item => item.cdesc = item.cedo + ' - ' + item.cdesc);
        this.dsEstatus = data.ttCtoedos;
      }
      if (data.ttCuartos) {
        this.dataGrid = data.ttCuartos;
      }
      this.loading = false;
    });
    this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  changeSearch(cuarto, status) {
    this._room.roomsForStatus(status, cuarto).subscribe(res => {
      if (res) {
        this.dataGrid = res;
      }
    });
  }

  distinctPrimitive(fieldName: string) {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  distinctNoPrimitive(fieldName: string) {
    const distintos = this.distinctPrimitive(fieldName);
    const data = [];
    this.dsEstatus.forEach(item => {
      distintos.forEach(val => {
        if (val === item.cedo) {
          data.push({ cedo: item['cedo'], cdesc: item['cdesc'] })
        }
      })
    });
    return data;
  }

  printPDF() {
    const ttInfocia = this._info.getSessionInfocia();
    const encabezado = ttInfocia.encab;
    const columns = [
      { title: 'Cuarto', dataKey: 'cuarto' },
      { title: 'Tipo', dataKey: 'tcuarto' },
      { title: 'Rec.', dataKey: 'cerecep' },
      { title: 'A.L.', dataKey: 'cellaves' },
      { title: 'Sist.', dataKey: 'cecomp' },
      { title: 'Rev.', dataKey: 'cerev' },
      { title: 'Bot.', dataKey: 'ceboton' },
      { title: 'Nota', dataKey: 'cnota' },
      { title: 'Limpieza', dataKey: 'climp' }
    ];
    const rows = this.dataGrid.data;
    const doc = new jsPDF('p', 'pt');
    doc.setFontSize(8);
    doc.text(encabezado, 40, 30);
    doc.setFontType('normal');
    doc.setFontSize(8);
    doc.setTextColor(100);
    doc.autoTable(columns, rows, {
      theme: 'striped',
      styles: {
        cellPadding: 5,
        fontSize: 8,
        font: 'helvetica'
      },
    });
    doc.output('save', 'Estado de Cuartos.pdf');
  }

  public allData(): ExcelExportData {
    if (this.filter) {
      const result: ExcelExportData = {
        data: process(this.dataFilter, { sort: [{ field: 'cuarto', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    } else {
      const result: ExcelExportData = {
        data: process(this.dataGrid, { sort: [{ field: 'cuarto', dir: 'asc' }], filter: this.filter }).data
      };
      return result;
    }
  }

}
>>>>>>> dev-alberto
