import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DecimalPipe } from '@angular/common';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InfociaGlobalService } from 'app/services/infocia.service';
import { RequerimientosService } from './requerimientos.service';
import { ManttoMenuService } from 'app/pms/manttos/mantto-menu/mantto-menu.service';
import { SharedService } from 'app/services/shared.service';
import { TblGeneral } from '../../../models/tgeneral';
import { HourPipe } from '../../../pipes/hour.pipe';
import { GLOBAL } from '../../../services/global';
import moment from 'moment';

@Component({
  selector: 'app-requirementos-esp',
  templateUrl: './requerimientos-esp.component.html',
  styleUrls: ['./requerimientos-esp.component.css'],
  providers: [HourPipe, DecimalPipe]
})
export class RequerimientosEspComponent implements OnInit {
  heading = 'Requerimientos de Huéspedes';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  @ViewChild('modalHelp') modalHelp: TemplateRef<any>;
  public searchFormGroup: FormGroup;
  public dataGrid: any = [];
  public dataTotal: any = [];
  private ttInfocia: any = [];
  private ttUsuario: any = [];
  public dsCamaristas: any = [];
  public mySelection: number[] = [2, 4];
  public dsSatisfacciones: any = [];
  public requerimiento: any;
  public dsDeptos: any = [];
  public dsEstatus: { desc: string; estatus: string; }[];
  public reqSelected: any = [];
  public darSatisf: boolean;
  public cancelado: boolean;
  public darEnterado: boolean;
  public reqInicio: boolean;
  public reqTermino: boolean;
  rini: any;
  rter: any;
  rnotas: any;
  dmensaje: string;
  dEnterado: string;
  dInicio: string;
  dTerm: string;
  dCancelar: string;
  dSatisf: string;
  dBorrar: string;
  reqnotas: boolean;
  reqOperador: boolean;
  reqSatisf: boolean;
  norecords = GLOBAL.noRecords;
  formatKendo = localStorage.getItem('formatKendo');
  formatMoment = localStorage.getItem('formatMoment');
  loading: boolean;
  toast = GLOBAL.toast;
  swal = GLOBAL.swal;

  constructor(private _infocia: InfociaGlobalService,
    private _menuServ: ManttoMenuService,
    private _shared: SharedService,
    public _reqs: RequerimientosService,
    private _modal: NgbModal,
    private _router: Router,
    private _pf: FormBuilder,
    private _hour: HourPipe,
    private _decimal: DecimalPipe,
    private _httpClient: HttpClient) {
      this.buildFormGroup();
      this.ttInfocia = this._infocia.getSessionInfocia();
      this.ttUsuario = this._infocia.getSessionUser();
      this.dsEstatus = this._reqs.getEstatus();
    
      this._shared.getAyuda('camaristas').subscribe(data => {
        this.dsCamaristas = data.ttCamaristas;
        this._shared.getAyuda('satisfacciones').subscribe(satisf => {
          this.dsSatisfacciones = satisf.ttSatisfacciones;
          this._shared.getAyuda('deptos').subscribe(deptos => { // Obtenemos los departamentos para buscar requerimientos
            this.dsDeptos = deptos.ttDeptos; // Data que recogera el dropdownlist
            this.searchFormGroup.patchValue({
              depto: this.ttUsuario.Depto,
              fent: moment(this.ttInfocia.fday).toDate(),
              fsal: moment(this.ttInfocia.fday).toDate(),
              eactual: 'P'
            })
            this.search(this.searchFormGroup.value);
          });
        });
      });
    
      const cReq = this.ttUsuario.cReq.split('');
      this.dmensaje = cReq[0];
      this.dEnterado = cReq[1];
      this.dInicio = cReq[2];
      this.dTerm = cReq[3];
      this.dCancelar = cReq[4];
      this.dSatisf = cReq[5];
      this.dBorrar = cReq[6];
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  search(e) {
    this.mySelection = [];
    this._reqs.getRequerimientos(e).subscribe(data => {
      this.reqInicio = false;
      this.reqTermino = false;
      this.darEnterado = false;
      this.darSatisf = false;
      this.cancelado = false;

      if (!data.ttTmpreq) {
        this.toast({ type: 'info', title: 'No se encontraron registros.' });
        this.dataGrid = [];
      } else {
        this.dataGrid = data.ttTmpreq;
        this.dataTotal = data.ttTtg;
      }
    });
  }

  buildFormGroup() {
    this.searchFormGroup = this._pf.group({
      depto: ['', Validators.required],
      fent: [null, Validators.required],
      fsal: [null, Validators.required],
      eactual: ['P', Validators.required],
    });
  }

  selectRow({ selectedRows }) {
    this.mySelection = [];
    this.requerimiento = selectedRows[0].dataItem;
    this.habilitarAcciones(this.requerimiento);
    this.mySelection.push(this.requerimiento.hnumreq);
  }

  enterado() {
    this.mySelection = [];
    this.requerimiento.eactual = 'E';
    this.requerimiento.hentregado = 'E';
    if (this.rnotas[0] === 'S') { // Requiere notas de enterado
      this.openModal();
    } else { // NO REQUIERE NOTAS Y ACTUALIZA DIRECTO
      this.update(this.requerimiento);
    }
  }

  openModal() {
    this._modal.open(this.modalHelp, { size: 'lg', centered: true });
  }

  inicio() {
    this.mySelection = [];
    if (this.rnotas[1] === 'S' || this.rini[1] === 'S' || this.rnotas[1] === 'S' && this.rini[1] === 'S') {
      this.requerimiento.eactual = 'I';
      this.requerimiento.hentregado = 'I';
      if (this.rnotas[1] === 'S') { // Requiere notas para dar inicio
        this.reqnotas = true;
      } else if (this.rini[1] === 'S') { // Reqiiere operador para dar inicio
        this.reqOperador = true;
        this.reqnotas = true;
      } else if (this.rnotas[1] === 'S' && this.rini[1] === 'S') { // Requiere notas y operador para dar inicio
        this.reqOperador = true;
        this.reqnotas = true;
      }
      if (this.reqnotas) {
        this.openModal();
      }
    } else { // NO REQUIERE NOTAS Y ACTUALIZA DIRECTO
      this.update(this.requerimiento);
    }
  }

  guardarNota(value) {
    switch (this.requerimiento.eactual) {
      case 'E': // Nota de enterado
        this.requerimiento.hnenter = value.nota;
        break;
      case 'I':  // Nota de inicio
        this.requerimiento.hnini = value.nota;
        break;
      case 'T': // Nota de termino
        this.requerimiento.hnterm = value.nota;
        break;
      case 'S': // Nota de satisfaccion
        this.requerimiento.hnsatisf = value.nota;
        break;
      case 'C':
        this.requerimiento.hnespecial = value.nota;
        break;
    }
    this.reqnotas = false;
    // Mandamos la data actualizada a grabar al servicio y notificamos el cambio
    this.update(this.requerimiento);
  }

  cancelar() {
    this.mySelection = [];
    this.requerimiento.eactual = 'C';
    this.requerimiento.hentregado = 'C';
    this.reqnotas = true;
    this.openModal();
  }

  update(data) {
    this._reqs.updateRequirements(data).subscribe(res => {
      if (res.cErrDesc) {
        this.swal(res.cErrDesc, res.cMensTxt, 'error');
      } else {
        this.toast({ type: 'success', title: 'Registro guardado!!' });
        this.search(this.searchFormGroup.value);
      }
    });
  }

  satisfaccion() {
    this.mySelection = [];
    this.reqSatisf = true;
    this.reqOperador = false;
    this.openModal();
    this.requerimiento.eactual = 'S';
    this.requerimiento.hentregado = 'S';
  }

  terminar() {
    this.mySelection = [];
    this.reqSatisf = false;
    if (this.rnotas[2] === 'S' || this.rter[1] === 'S' || this.rnotas[2] === 'S' && this.rter[1] === 'S') {
      this.requerimiento.eactual = 'T';
      this.requerimiento.hentregado = 'T';
      if (this.rnotas[2] === 'S') { // Requiere notas para dar inicio
        this.reqnotas = true;
      } else if (this.rter[1] === 'S') { // Reqiiere operador para dar inicio
        this.reqOperador = true;
        this.reqnotas = true;
      } else if (this.rnotas[2] === 'S' && this.rter[1] === 'S') { // Requiere notas y operador para dar inicio
        this.reqOperador = true;
        this.reqnotas = true;
      }
      if (this.reqnotas) {
        this.openModal();
      }
    } else { // NO REQUIERE NOTAS Y ACTUALIZA DIRECTO
      this.update(this.requerimiento);
    }
  }

  cerrarForm() {
    this.reqnotas = false;
    this.search(this.searchFormGroup.value);
  }

  habilitarAcciones(selected) {
    // Deshabilitamos acciones no permitidas de acuerdo al estado del requerimiento y privilegios del usuario
    // Si no hay data no se debe ejecutar esta funcion ya que no se tendran los requerimientos
    this._reqs.getListaRequerimientos().subscribe(ttReqesp => {
      if (ttReqesp) {
        for (const val of ttReqesp) {
          if (val.rreqesp === selected.hreqesp) {
            this.rini = val.rini.split('');
            this.rter = val.rter.split('');
            this.rnotas = val.rnotas.split('');
          }
        }
        if (selected.hentregado === 'T' && selected.satisfaccion === '' && this.dSatisf === 'S') {
          this.darSatisf = true; // Asignar satisfacción
        } else {
          this.darSatisf = false; // Ocultar todo
        }
        if (selected.hentregado === 'C') {
          this.cancelado = false; // Ocultar todo
        } else {
          this.cancelado = false;
        }
        if (selected.hentregado === 'P' && this.dEnterado === 'S') {
          this.darEnterado = true; // show Cancelar y Enterado
          this.cancelado = true; // Ocultar todo
        } else {
          this.darEnterado = false;
        }
        if (selected.hentregado === 'E') {
          if (this.rini[0] === 'S' && this.dInicio === 'S') {
            this.reqInicio = true; // show Inicio
          } else {
            this.reqInicio = false;
          }
          if (this.rini[1] === 'S') {
            this.reqOperador = true;
          } else {
            this.reqOperador = false;
          }
        } else {
          this.reqInicio = false;
        }
        if (selected.hentregado === 'I') {
          if (this.rter[0] === 'S' && this.dTerm === 'S') {
            this.reqTermino = true; // show terminar
          } else {
            this.reqTermino = false;
          }
          if (this.rter[1] === 'S') {
            this.reqOperador = true;
          } else {
            this.reqOperador = false;
          }
        } else {
          this.reqTermino = false;
        }
        if (this.dCancelar !== 'S') {
          this.cancelado = false;
        }
      }
    });
  }
  printExcel() {
    this.loading = true;
    let estatus;
    switch (this.searchFormGroup.value.eactual) {
      case 'P': estatus = 1; break;
      case 'E': estatus = 2; break;
      case 'I': estatus = 5; break;
      case 'T': estatus = 3; break;
      case '*': estatus = 4; break;
    }
    const data = [...[this.searchFormGroup.value]][0];

    const tbl = new TblGeneral();
    tbl.iInt1 = 3;
    tbl.iInt2 = estatus;
    tbl.iInt3 = 0;
    tbl.iInt4 = 1;
    tbl.fFec1 = moment(data.fent).format(GLOBAL.formatIso);
    tbl.fFec2 = moment(data.fsal).format(GLOBAL.formatIso);
    tbl.cChr1 = this.searchFormGroup.value.depto;

    const json = JSON.parse('{"ttTgeneral":[' + JSON.stringify(tbl) + ']}');
    this._httpClient.post<any>(GLOBAL.url + '/manttos/repreq/ReporteReq', json).subscribe(
      response => {
        const res = response.response.siEnvioExcel.dsEnvioExcel.RespuestaExcel;
        if (res[0].ArchivoExportado) {
          const linkSource = 'data:application/xlsx;base64,' + res[0].ArchivoExportado;
          const download = document.createElement('a');
          download.href = linkSource;
          download.download = res[0].Archivo;
          download.click();
        }
        this.loading = false;
      },
      error => {
        this.swal('Error al generar Excel', GLOBAL.errorPdf[1], 'info');
        this.loading = false;
      }
    );
  }

  exportPDF() {
    const data = JSON.parse(JSON.stringify(this.dataGrid));
    const total = JSON.parse(JSON.stringify(this.dataTotal));
    const infocia = JSON.parse(JSON.stringify(this.ttInfocia));
    const form = JSON.parse(JSON.stringify(this.searchFormGroup.value));
    infocia.fday = moment(infocia.fday).format(GLOBAL.formatIso);
    data.forEach(item => {
      item.hhenv = this._hour.transform(item.hhenv);
      item.hhej = this._hour.transform(item.hhej);
      item.hhi = this._hour.transform(item.hhi);
      item.hht = this._hour.transform(item.hht);
      const fecha = moment(item.hfecha).format(this.formatMoment);
      item.hfecha = fecha.substring(0, 5).replace('-', '/');
      if (item.hft) {
        const termino = moment(item.hft).format(this.formatMoment);
        item.hft = termino.substring(0, 5).replace('-', '/');
      }
    });
    total.forEach(item => item.hcant = this._decimal.transform(item.hcant, '1.2-2'));
    const json = {
      Propiedad: this.ttInfocia.propiedad,
      iSec: '004-REQUERIMIENOS',
      nombrepdf: 'Requerimientos huespedes',
      infocia: infocia,
      usuario: this.ttUsuario,
      requerimientos: data,
      totales: total,
      heading: this.heading,
      fechas: moment(form.fent).format(this.formatMoment) + ' - ' + moment(form.fsal).format(this.formatMoment),
      hora: moment().format('HH:mm:ss'),
    }
    this._httpClient.post<any>(GLOBAL.urlPhp + '/api/pdf/reportes', json).subscribe(
      response => {
        const linkSource = 'data:application/pdf;base64,' + response.base64;
        const download = document.createElement('a');
        download.href = linkSource;
        download.download = 'Requerimientos huespedes.pdf';
        download.click();
        this.loading = false;
      },
      error => {
        this.swal(GLOBAL.errorPdf[0], GLOBAL.errorPdf[1], 'info');
        this.loading = false;
      });
  }
}
