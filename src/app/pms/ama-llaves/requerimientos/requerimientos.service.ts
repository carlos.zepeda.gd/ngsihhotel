import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { GLOBAL } from '../../../services/global';
import 'rxjs/add/operator/map'
import { Subject } from 'rxjs/Subject';
const headers = new Headers({ 'Content-Type': 'application/json' });
const url = GLOBAL.url;
const esp = GLOBAL.char174;
import moment from 'moment';

@Injectable()
export class RequerimientosService {

  private notify = new Subject<any>();
  public notifyObservable$ = this.notify.asObservable();

  constructor(private http: Http) { }

  public notifyOther(data: any) {
    if (data) { this.notify.next(data); }
  }
  getEstatus() {
    const dataStatus = [
      { desc: 'Pendientes', estatus: 'P' },
      { desc: 'Enterados', estatus: 'E' },
      { desc: 'Iniciados', estatus: 'I' },
      { desc: 'Terminados', estatus: 'T' },
      { desc: 'Cancelados', estatus: 'C' },
      { desc: 'Todos', estatus: '*' }
    ];
    return dataStatus;
  }
  getListaRequerimientos() {
    return this.http.get(url + '/manttos/reqesp/')
      .map(res => res.json().response.siReqesp.dsReqesp.ttReqesp);
  }
  searchDataGrid(api: string) {
    return this.http.get(url + '/manttos/repreq/' + api)
      .map(res => res.json().response.siTmpreq.dsTmpreq.ttTmpreq);
  }
  getRequerimientos(e) {
    e.fent = moment(e.fent).format(GLOBAL.formatIso);
    e.fsal = moment(e.fsal).format(GLOBAL.formatIso);
    const api = `${e.fent}/${e.fsal}/${e.depto + esp + e.eactual}`;
    return this.http.get(url + '/manttos/repreq/' + api).map(res => res.json().response.siTmpreq.dsTmpreq);
  }
  updateRequirements(e) {
    const params = '{"dsTmpreq":{"ttTmpreq":[' + JSON.stringify(e) + ']}}';
    return this.http.put(url + '/manttos/repreq/', params, { headers: headers })
      .map(res => res.json().response.siTmpreq.dsTmpreq.ttTmpreq);
  }

}
