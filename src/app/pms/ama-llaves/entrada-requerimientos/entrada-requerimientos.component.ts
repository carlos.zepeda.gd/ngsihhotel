<<<<<<< HEAD
import { Component, OnInit } from '@angular/core';
import { CargosService, RequerimientosService } from '../../recepcion/check-in/services/service-checkin.index';
import { Requerimiento } from '../../recepcion/check-in/models/requerimientos.model';
import { GLOBAL } from '../../../services/global';
import { InfociaGlobalService } from '../../../services/infocia.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-entrada-requerimientos',
  templateUrl: './entrada-requerimientos.component.html',
  styleUrls: ['./entrada-requerimientos.component.css']
})

export class EntradaRequerimientosComponent implements OnInit {
    private ttInfocia: any;
    public dsCodigos: Array<{ rdesc: string, rreqesp: string}> = [];
    public rooms: Array<{ cuarto: string }> = [];
    public dsRooms: any[] = [];
    public toRoom: any;
    public cantidad: number = 1;
    public codigoReq: any;
    public crazon: string;
    constructor(
      private _req: RequerimientosService,
      private _cargos: CargosService,
      private _info: InfociaGlobalService
    ) { }

    ngOnInit() {
      this.ttInfocia = this._info.getSessionInfocia();
      this._req.getCuartos().subscribe( data => this.rooms = data);
      this._req.getRequerimientos().subscribe( data => this.dsCodigos = data );
    }
    saveRecord(myForm) {
      const val = myForm.value;
      const requerimiento = new Requerimiento ('', '', '', val.toRoom.cuarto, 1, val.cantidad, val.codigoReq.rdesc, '',
                                              this.ttInfocia.fday, '', 0, '', 0, val.crazon, '', '', '', '', 1,
                                              val.codigoReq.rreqesp, '', '', false, '');
      this._req.entradaRequerimiento(requerimiento).subscribe(
        data => {
            if (data[0].cMensTxt) {
              swal(data[0].cMensTit, data[0].cMensTxt, 'error');
            }else {
                myForm.reset();
                this.cantidad = 0;
                this.toRoom = '';
                this.codigoReq = '';
            }
        }
    );
    }
    plusDays() {
      this.cantidad = this.cantidad + 1;
    }
    minusDays() {
      this.cantidad = this.cantidad - 1;
      if (this.cantidad  <= 0) { this.cantidad = 1; }
    }
}
=======
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompositeFilterDescriptor, distinct } from '@progress/kendo-data-query';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { CrudService } from '../../manttos/mantto-srv.services';
import { GLOBAL } from '../../../services/global';

@Component({
  selector: 'app-entrada-requerimientos',
  templateUrl: './entrada-requerimientos.component.html',
  styleUrls: ['./entrada-requerimientos.component.css']
})

export class EntradaRequerimientosComponent implements OnInit {
  heading = 'Entrada de Requerimientos';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  loading = false;
  pageSize = GLOBAL.pageSize;
  skip = 0;
  noRecords = GLOBAL.noRecords;
  ngForm: FormGroup;
  dataGrid: any = [];
  toast = GLOBAL.toast;
  modalTitle = '';
  modalType = '';
  public filter: CompositeFilterDescriptor;
  public formatKendo = localStorage.getItem('formatKendo');
  loadingColor: any = GLOBAL.loadingColor;

  constructor(private _menuServ: ManttoMenuService,
    private _router: Router,
    private _crudservice: CrudService,
    private _formBuild: FormBuilder,
    private _modal: NgbModal) {
    this.ngForm = this.createForm();
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  createForm() {
    return this._formBuild.group({
      hfolio: [0],
      cuarto: ['', [Validators.required]],
      hfecha: [''],
      hnumreq: [0],
      hreqesp: ['', [Validators.required]],
      hdesc: [''],
      htcargo: [''],
      htfolio: [''],
      hcantidad: [0.0, [Validators.required]],
      hmonto: [0.0],
      hmon: [''],
      hnespecial: ['', [Validators.required]],
      hfecreq: [''],
      hentregado: [''],
      depto: [''],
      ubicacion: [''],
      Satisfaccion: [''],
      hnsatisf: [''],
      hnterm: [''],
      hnenter: [''],
      hnini: [''],
      cRowID: [''],
      lError: [false],
      cErrDesc: [''],
      hreqespNom: ['']
    });
  }

  public distinctPrimitive(fieldName: string): any {
    return distinct(this.dataGrid, fieldName).map(item => item[fieldName]);
  }

  openModal(content, type) {
    switch (type) {
      case 'crt':
        this.modalType = type;
        this.modalTitle = 'Cuartos';
        break;
      case 'cod':
        this.modalType = type;
        this.modalTitle = 'Requerimiento Especial';
        break;
    }
    this._modal.open(content, { size: 'xl', centered: true });
  }

  getDataModal(data, type) {
    switch (type) {
      case 'crt':
        this.ngForm.patchValue({ cuarto: data.cuarto });
        this.getDataCuarto(data.cuarto);
        break;
      case 'cod':
        this.ngForm.patchValue({
          hreqesp: data.rreqesp,
          hreqespNom: data.rdesc
        });
        break;
    }
  }

  getDataCuarto(cuarto) {
    this._crudservice.getData('/llaves/entradareq/' + cuarto).subscribe(res => {
      if (res.siHuesreq.dsHuesreq.ttHuesreq !== undefined) {
        this.dataGrid = res.siHuesreq.dsHuesreq.ttHuesreq;
      } else {
        this.dataGrid = [];
      }
    });
  }

  guardar() {
    this.loading = true;
    this._crudservice.postInfo(this.ngForm.value, '/llaves/entradareq/3', 'dsHuesreq', 'ttHuesreq')
      .subscribe(res => {
        const error = res.siHuesreq.dsMensajes.ttMensError;
        if (error && error[0]) {
          this.toast({ type: 'error', title: GLOBAL.textError, text: error[0].cMensTxt });
        } else {
          this.toast({ type: 'success', title: GLOBAL.textSuccess });
          this.getDataCuarto(this.ngForm.value.cuarto);
        }
        this.loading = false;
      });
  }

  nuevo() {
    this.dataGrid = [];
    this.ngForm = this.createForm();
  }
}
>>>>>>> dev-alberto
