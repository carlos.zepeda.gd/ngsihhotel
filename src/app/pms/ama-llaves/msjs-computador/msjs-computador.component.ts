import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { GLOBAL } from '../../../services/global';
import { CrudService } from '../../mantto-srv.services';
import { InfociaGlobalService } from '../../../services/infocia.service';
import { ManttoMenuService } from '../../manttos/mantto-menu/mantto-menu.service';
import { TblGeneral } from '../../../models/tgeneral';
import moment from 'moment';

@Component({
  selector: 'app-msjs-computador',
  templateUrl: './msjs-computador.component.html',
  styleUrls: ['./msjs-computador.component.css']
})
export class MsjsComputadorComponent implements OnInit, OnChanges {
  heading = 'Mensajes del Computador';
  subheading = 'Property Management System.';
  icon = 'icon-gradient bg-tempting-azure';
  @Input() proc: string = 'ama';
  noRecords = GLOBAL.noRecords;
  textSuccess = GLOBAL.textSuccess;
  textError = GLOBAL.textError;
  formatKendo: string = localStorage.getItem('formatKendo');
  api: Array<string> = ['/reportes/general/MensajesComp', 'dsReporte', 'ttTgeneral'];
  loading = false;
  dataGrid = [];
  toast = GLOBAL.toast;
  ttInfocia: any = [];
  ttUsuario: any = [];
  fecha: Date;
  formSend: any;
  ngForm: FormGroup;
  ttGeneral = new TblGeneral();

  constructor(private _menuServ: ManttoMenuService,
    private _router: Router,
    private _info: InfociaGlobalService,
    private _crudservice: CrudService,
    private _formBuild: FormBuilder) {
    this.ttInfocia = this._info.getSessionInfocia();
    this.ttUsuario = this._info.getSessionUser();
    this.fecha = moment(this.ttInfocia.fday).toDate();
    this.ngForm = this.createFormGroup();
    this.ngForm.patchValue({ cChr1: 'L' });
  }

  ngOnChanges(change: SimpleChanges): void {
    switch (change.proc.currentValue) {
      case 'tel':
        this.ngForm.patchValue({ cChr1: '' });
        break;
      case 'res':
        this.heading = 'Mensajes Interfaces';
        this.ngForm.patchValue({ cChr1: 'R' });
        break;
    }
  }

  ngOnInit() {
    const title = { icon: this.icon, subheading: this.subheading };
    this._menuServ.cuerrentObservableMenu.subscribe(data => {
      const resp = this._menuServ.serviceMenu(data, this._router.url.split('/'), title);
      if (resp) {
        this.icon = resp.icon;
        this.subheading = resp.subheading;
      }
    });
  }

  createFormGroup() {
    return this._formBuild.group(this.ttGeneral);
  }

  buscar() {
    this.loading = true;
    this.ngForm.patchValue({ fFec1: moment(this.fecha).format(GLOBAL.formatIso)});
    
    this._crudservice.putInfo(this.ngForm.value, this.api[0], this.api[1], this.api[2]).subscribe(data => {
      if (data.siReporte.dsReporte.ttZmsg !== undefined) {
        this.dataGrid = data.siReporte.dsReporte.ttZmsg;
      } else {
        this.dataGrid = [];
      }
      this.loading = false;
    });
  }
}
