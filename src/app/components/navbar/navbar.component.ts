import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ManttoMenuService } from '../../pms/manttos/mantto-menu/mantto-menu.service';
import { InfociaGlobalService } from '../../services/infocia.service';
declare var $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./sidebar.component.scss']

})
export class NavbarComponent implements OnInit, OnChanges {
  @Input() sistema: string;
  public submenu: any = [];
  public ttUsuario: any = [];
  public menuPos: any = [];

  constructor(
    private _info: InfociaGlobalService, private _menu: ManttoMenuService
  ) {
    this.ttUsuario = this._info.getSessionUser();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.sistema !== undefined) {
      if (changes.sistema.currentValue) {
        this._menu.getMenu(this.sistema).subscribe(data => this.menuPos = data);
      }
    }
  }
  ngOnInit() {
    this._info.getUsuario().subscribe(data => {
      localStorage.setItem('globalUser', JSON.stringify(data[0]));
    });
  }
  sidebarCollapse() {
    $('#sidebar, #content').toggleClass('active');
    $('.collapse.in').toggleClass('in');
    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
  }
  menuSelected(menu) {
    this.submenu = menu.SubMenu;
    $('#navbarSupportedContent').collapse('hide');
  }
}
