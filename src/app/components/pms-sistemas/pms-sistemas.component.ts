<<<<<<< HEAD
import { Component, OnInit, OnDestroy } from '@angular/core';
import { CheckinService } from '../../pms/recepcion/check-in/services/checkin.service';
import { InfociaGlobalService } from '../../services/infocia.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pms-sistemas',
  templateUrl: './pms-sistemas.component.html',
  styleUrls: ['./pms-sistemas.component.css']
})
export class PmsSistemasComponent implements OnInit, OnDestroy {
  public userSys: any;
  public ttInfocia: any;
  public ttUsuarios: any;
  public sistemas: any;
  public navbar: boolean = true;
  public loading: boolean;
  public encabezado: any = [];
  public myButtons: any = [
    { value: 'guardar' },
    { value: 'cerrar' }
  ];
  constructor(
    private _nav: CheckinService,
    private _info: InfociaGlobalService,
    private _router: Router
  ) { }
  ngOnInit() {
    // Se obtienen los sitemas que tiene acceso el usuario y se muestran en /home
    // Se utiliza el campo lError en true para mostrar u ocultar el sistema.
    this._info.getUserCia().subscribe(data => {
      this.ttInfocia = data.ttInfocia[0];
      this.ttUsuarios = data.ttUsuarios[0];
      this.encabezado = this.ttInfocia.encab.split('®');
      // this._router.navigate(['/pms/home']);
      this.userSys = this.ttUsuarios.cVal32.split('®');
      this._info.getSistemas().subscribe(sistemas => {
        if (sistemas) {
          this.sistemas = sistemas;
          this.sistemas.filter(item => {
            for (const sistema of this.userSys) {
              if (item.cSistema === sistema) {
                item.lError = true;
                if (this.userSys.length === 1) {
                  this._router.navigate(['/' + item.cSistema + '/home']);
                }
              }
            }
          });
        }
      });
      localStorage.setItem('globalUser', JSON.stringify(this.ttUsuarios));
      localStorage.setItem('globalInfocia', JSON.stringify(this.ttInfocia));
    });
  }
  reporteAuditoria() {
    const report = {
      'response': {
        'siProcAud': {
          'dsProcAud': {
            'ttRepProcE': [
              {
                'Propiedad': '',
                'cNombre': 'AN',
                'fRep': '2019-07-02',
                'iSec': 7,
                'inRep': 0,
                'cRsocial': 'SiHoteles, S.A. de C.V.',
                'cEncab': 'Entradas Manuales',
                'cHora': '16:44:25',
                'cUsuario': '',
                'cImp': [
                  '#Ent',
                  'D   e   s   c   r   i   p   c   i   o   n',
                  'Ope',
                  'TM',
                  'Cod',
                  'Debito',
                  'Credito'
                ],
                'ttRepProcL': [
                  {
                    'iLinea': 1,
                    'cImp': [
                      '0001',
                      'RESTAURANTE CANTAROS(CANTAROS DESAYUNO-CHEQUE NORMAL)',
                      'sys',
                      '',
                      'IAB',
                      '77.25',
                      ''
                    ]
                  },
                  {
                    'iLinea': 2,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'PRC',
                      '40',
                      ''
                    ]
                  },
                  {
                    'iLinea': 3,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'RC',
                      '482.75',
                      ''
                    ]
                  },
                  {
                    'iLinea': 4,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'RC',
                      '',
                      '-600'
                    ]
                  },
                  {
                    'iLinea': 5,
                    'cImp': [
                      '',
                      '',
                      'Totales:',
                      '',
                      '',
                      '600',
                      '-600'
                    ]
                  },
                  {
                    'iLinea': 6,
                    'cImp': [
                      '0002',
                      'BAR PAPAGAYO(PAPAGAYO-CHEQUE NORMAL)',
                      'sys',
                      '',
                      'BEP',
                      '162.07',
                      ''
                    ]
                  },
                  {
                    'iLinea': 7,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'IAB',
                      '25.93',
                      ''
                    ]
                  },
                  {
                    'iLinea': 8,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'PRP',
                      '25',
                      ''
                    ]
                  },
                  {
                    'iLinea': 9,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'BEP',
                      '',
                      '-213'
                    ]
                  },
                  {
                    'iLinea': 10,
                    'cImp': [
                      '',
                      '',
                      'Totales:',
                      '',
                      '',
                      '213',
                      '-213'
                    ]
                  },
                  {
                    'iLinea': 11,
                    'cImp': [
                      '0003',
                      'Entrada Global',
                      'sys',
                      '',
                      '',
                      '1400.85',
                      ''
                    ]
                  },
                  {
                    'iLinea': 12,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '1979.74',
                      ''
                    ]
                  },
                  {
                    'iLinea': 13,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '155.175',
                      ''
                    ]
                  },
                  {
                    'iLinea': 14,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '0',
                      ''
                    ]
                  },
                  {
                    'iLinea': 15,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '4094.82',
                      ''
                    ]
                  },
                  {
                    'iLinea': 16,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '656.77',
                      ''
                    ]
                  },
                  {
                    'iLinea': 17,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '646.55',
                      ''
                    ]
                  },
                  {
                    'iLinea': 18,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '852.23',
                      ''
                    ]
                  },
                  {
                    'iLinea': 19,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '129',
                      ''
                    ]
                  },
                  {
                    'iLinea': 20,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '106.9',
                      ''
                    ]
                  },
                  {
                    'iLinea': 21,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '20.64',
                      ''
                    ]
                  },
                  {
                    'iLinea': 22,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '724.14',
                      ''
                    ]
                  },
                  {
                    'iLinea': 23,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '189.66',
                      ''
                    ]
                  },
                  {
                    'iLinea': 24,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '4.31',
                      ''
                    ]
                  },
                  {
                    'iLinea': 25,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'CCE',
                      '',
                      '-1625'
                    ]
                  },
                  {
                    'iLinea': 26,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'CDE',
                      '',
                      '-2296.5'
                    ]
                  },
                  {
                    'iLinea': 27,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'CTL',
                      '',
                      '-180'
                    ]
                  },
                  {
                    'iLinea': 28,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '0',
                      ''
                    ]
                  },
                  {
                    'iLinea': 29,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'EST',
                      '',
                      '-4750'
                    ]
                  },
                  {
                    'iLinea': 30,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'ICT',
                      '',
                      '-750'
                    ]
                  },
                  {
                    'iLinea': 31,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'LAV',
                      '',
                      '-170.28'
                    ]
                  },
                  {
                    'iLinea': 32,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'MIS',
                      '',
                      '-124'
                    ]
                  },
                  {
                    'iLinea': 33,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'TBE',
                      '',
                      '-840'
                    ]
                  },
                  {
                    'iLinea': 34,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'TCT',
                      '',
                      '-220'
                    ]
                  },
                  {
                    'iLinea': 35,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'TLC',
                      '',
                      '-5'
                    ]
                  },
                  {
                    'iLinea': 36,
                    'cImp': [
                      '',
                      '',
                      'Totales:',
                      '',
                      '',
                      '10960.78',
                      '-10960.78'
                    ]
                  },
                  {
                    'iLinea': 37,
                    'cImp': [
                      '0004',
                      'Rentas Manuales',
                      'sys',
                      '',
                      '',
                      '157.34',
                      ''
                    ]
                  },
                  {
                    'iLinea': 38,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '9.58',
                      ''
                    ]
                  },
                  {
                    'iLinea': 39,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '51.13',
                      ''
                    ]
                  },
                  {
                    'iLinea': 40,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '319.54',
                      ''
                    ]
                  },
                  {
                    'iLinea': 41,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '983.41',
                      ''
                    ]
                  },
                  {
                    'iLinea': 42,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'RML',
                      '656.77',
                      '-380.25'
                    ]
                  },
                  {
                    'iLinea': 43,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'UPG',
                      '646.55',
                      '-1140.75'
                    ]
                  },
                  {
                    'iLinea': 44,
                    'cImp': [
                      '',
                      '',
                      'Totales:',
                      '',
                      '',
                      '1521',
                      '-1521'
                    ]
                  },
                  {
                    'iLinea': 45,
                    'cImp': [
                      '0005',
                      'Cargos Adicionales y Req. Especiales',
                      'sys',
                      '',
                      'IOT',
                      '144.83',
                      ''
                    ]
                  },
                  {
                    'iLinea': 46,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'SGE',
                      '905.17',
                      ''
                    ]
                  },
                  {
                    'iLinea': 47,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'SGE',
                      '',
                      '-1050'
                    ]
                  },
                  {
                    'iLinea': 48,
                    'cImp': [
                      '',
                      '',
                      'Totales:',
                      '',
                      '',
                      '1050',
                      '-1050'
                    ]
                  },
                  {
                    'iLinea': 49,
                    'cImp': [
                      '0006',
                      'Rentas Automatica',
                      'sys',
                      '',
                      '',
                      '92355.5',
                      ''
                    ]
                  },
                  {
                    'iLinea': 50,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '360.84',
                      ''
                    ]
                  },
                  {
                    'iLinea': 51,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '64645.55',
                      ''
                    ]
                  },
                  {
                    'iLinea': 52,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '252.67',
                      ''
                    ]
                  },
                  {
                    'iLinea': 53,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '27704.42',
                      ''
                    ]
                  },
                  {
                    'iLinea': 54,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '108.3',
                      ''
                    ]
                  },
                  {
                    'iLinea': 55,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '56200',
                      ''
                    ]
                  },
                  {
                    'iLinea': 56,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '1790.23',
                      ''
                    ]
                  },
                  {
                    'iLinea': 57,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '39215.98',
                      ''
                    ]
                  },
                  {
                    'iLinea': 58,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '48.52',
                      ''
                    ]
                  },
                  {
                    'iLinea': 59,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '12950.68',
                      ''
                    ]
                  },
                  {
                    'iLinea': 60,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '45010.73',
                      ''
                    ]
                  },
                  {
                    'iLinea': 61,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '14486.66',
                      ''
                    ]
                  },
                  {
                    'iLinea': 62,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '175.87',
                      ''
                    ]
                  },
                  {
                    'iLinea': 63,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'RP',
                      '',
                      '-355305.95'
                    ]
                  },
                  {
                    'iLinea': 64,
                    'cImp': [
                      '',
                      '',
                      'Totales:',
                      '',
                      '',
                      '355305.95',
                      '-355305.95'
                    ]
                  }
                ],
                'ttRepProcF': [
                  {
                    'iLinea': 65,
                    'cImp': [
                      'Total General:',
                      '369650.73',
                      '-369650.73'
                    ]
                  },
                  {
                    'iLinea': 65,
                    'cImp': [
                      'LINEA',
                      '',
                      ''
                    ]
                  },
                  {
                    'iLinea': 65,
                    'cImp': [
                      'Total General:',
                      '369650.73',
                      '-369650.73'
                    ]
                  }
                ]
              },
              {
                'Propiedad': '',
                'cNombre': 'AN',
                'fRep': '2019-07-02',
                'iSec': 7,
                'inRep': 0,
                'cRsocial': 'SiHoteles, S.A. de C.V.',
                'cEncab': 'Entradas Manuales',
                'cHora': '16:44:25',
                'cUsuario': '',
                'cImp': [
                  '#Ent',
                  'D   e   s   c   r   i   p   c   i   o   n',
                  'Ope',
                  'TM',
                  'Cod',
                  'Debito',
                  'Credito'
                ],
                'ttRepProcL': [
                  {
                    'iLinea': 1,
                    'cImp': [
                      '0001',
                      'RESTAURANTE CANTAROS(CANTAROS DESAYUNO-CHEQUE NORMAL)',
                      'sys',
                      '',
                      'IAB',
                      '77.25',
                      ''
                    ]
                  },
                  {
                    'iLinea': 2,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'PRC',
                      '40',
                      ''
                    ]
                  },
                  {
                    'iLinea': 3,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'RC',
                      '482.75',
                      ''
                    ]
                  },
                  {
                    'iLinea': 4,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'RC',
                      '',
                      '-600'
                    ]
                  },
                  {
                    'iLinea': 5,
                    'cImp': [
                      '',
                      '',
                      'Totales:',
                      '',
                      '',
                      '600',
                      '-600'
                    ]
                  },
                  {
                    'iLinea': 6,
                    'cImp': [
                      '0002',
                      'BAR PAPAGAYO(PAPAGAYO-CHEQUE NORMAL)',
                      'sys',
                      '',
                      'BEP',
                      '162.07',
                      ''
                    ]
                  },
                  {
                    'iLinea': 7,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'IAB',
                      '25.93',
                      ''
                    ]
                  },
                  {
                    'iLinea': 8,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'PRP',
                      '25',
                      ''
                    ]
                  },
                  {
                    'iLinea': 9,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'BEP',
                      '',
                      '-213'
                    ]
                  },
                  {
                    'iLinea': 10,
                    'cImp': [
                      '',
                      '',
                      'Totales:',
                      '',
                      '',
                      '213',
                      '-213'
                    ]
                  },
                  {
                    'iLinea': 11,
                    'cImp': [
                      '0003',
                      'Entrada Global',
                      'sys',
                      '',
                      '',
                      '1400.85',
                      ''
                    ]
                  },
                  {
                    'iLinea': 12,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '1979.74',
                      ''
                    ]
                  },
                  {
                    'iLinea': 13,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '155.175',
                      ''
                    ]
                  },
                  {
                    'iLinea': 14,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '0',
                      ''
                    ]
                  },
                  {
                    'iLinea': 15,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '4094.82',
                      ''
                    ]
                  },
                  {
                    'iLinea': 16,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '656.77',
                      ''
                    ]
                  },
                  {
                    'iLinea': 17,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '646.55',
                      ''
                    ]
                  },
                  {
                    'iLinea': 18,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '852.23',
                      ''
                    ]
                  },
                  {
                    'iLinea': 19,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '129',
                      ''
                    ]
                  },
                  {
                    'iLinea': 20,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '106.9',
                      ''
                    ]
                  },
                  {
                    'iLinea': 21,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '20.64',
                      ''
                    ]
                  },
                  {
                    'iLinea': 22,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '724.14',
                      ''
                    ]
                  },
                  {
                    'iLinea': 23,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '189.66',
                      ''
                    ]
                  },
                  {
                    'iLinea': 24,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '4.31',
                      ''
                    ]
                  },
                  {
                    'iLinea': 25,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'CCE',
                      '',
                      '-1625'
                    ]
                  },
                  {
                    'iLinea': 26,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'CDE',
                      '',
                      '-2296.5'
                    ]
                  },
                  {
                    'iLinea': 27,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'CTL',
                      '',
                      '-180'
                    ]
                  },
                  {
                    'iLinea': 28,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '0',
                      ''
                    ]
                  },
                  {
                    'iLinea': 29,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'EST',
                      '',
                      '-4750'
                    ]
                  },
                  {
                    'iLinea': 30,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'ICT',
                      '',
                      '-750'
                    ]
                  },
                  {
                    'iLinea': 31,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'LAV',
                      '',
                      '-170.28'
                    ]
                  },
                  {
                    'iLinea': 32,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'MIS',
                      '',
                      '-124'
                    ]
                  },
                  {
                    'iLinea': 33,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'TBE',
                      '',
                      '-840'
                    ]
                  },
                  {
                    'iLinea': 34,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'TCT',
                      '',
                      '-220'
                    ]
                  },
                  {
                    'iLinea': 35,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'TLC',
                      '',
                      '-5'
                    ]
                  },
                  {
                    'iLinea': 36,
                    'cImp': [
                      '',
                      '',
                      'Totales:',
                      '',
                      '',
                      '10960.78',
                      '-10960.78'
                    ]
                  },
                  {
                    'iLinea': 37,
                    'cImp': [
                      '0004',
                      'Rentas Manuales',
                      'sys',
                      '',
                      '',
                      '157.34',
                      ''
                    ]
                  },
                  {
                    'iLinea': 38,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '9.58',
                      ''
                    ]
                  },
                  {
                    'iLinea': 39,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '51.13',
                      ''
                    ]
                  },
                  {
                    'iLinea': 40,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '319.54',
                      ''
                    ]
                  },
                  {
                    'iLinea': 41,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '983.41',
                      ''
                    ]
                  },
                  {
                    'iLinea': 42,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'RML',
                      '656.77',
                      '-380.25'
                    ]
                  },
                  {
                    'iLinea': 43,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'UPG',
                      '646.55',
                      '-1140.75'
                    ]
                  },
                  {
                    'iLinea': 44,
                    'cImp': [
                      '',
                      '',
                      'Totales:',
                      '',
                      '',
                      '1521',
                      '-1521'
                    ]
                  },
                  {
                    'iLinea': 45,
                    'cImp': [
                      '0005',
                      'Cargos Adicionales y Req. Especiales',
                      'sys',
                      '',
                      'IOT',
                      '144.83',
                      ''
                    ]
                  },
                  {
                    'iLinea': 46,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'SGE',
                      '905.17',
                      ''
                    ]
                  },
                  {
                    'iLinea': 47,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'SGE',
                      '',
                      '-1050'
                    ]
                  },
                  {
                    'iLinea': 48,
                    'cImp': [
                      '',
                      '',
                      'Totales:',
                      '',
                      '',
                      '1050',
                      '-1050'
                    ]
                  },
                  {
                    'iLinea': 49,
                    'cImp': [
                      '0006',
                      'Rentas Automatica',
                      'sys',
                      '',
                      '',
                      '92355.5',
                      ''
                    ]
                  },
                  {
                    'iLinea': 50,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '360.84',
                      ''
                    ]
                  },
                  {
                    'iLinea': 51,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '64645.55',
                      ''
                    ]
                  },
                  {
                    'iLinea': 52,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '252.67',
                      ''
                    ]
                  },
                  {
                    'iLinea': 53,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '27704.42',
                      ''
                    ]
                  },
                  {
                    'iLinea': 54,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '108.3',
                      ''
                    ]
                  },
                  {
                    'iLinea': 55,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '56200',
                      ''
                    ]
                  },
                  {
                    'iLinea': 56,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '1790.23',
                      ''
                    ]
                  },
                  {
                    'iLinea': 57,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '39215.98',
                      ''
                    ]
                  },
                  {
                    'iLinea': 58,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '48.52',
                      ''
                    ]
                  },
                  {
                    'iLinea': 59,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '12950.68',
                      ''
                    ]
                  },
                  {
                    'iLinea': 60,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '45010.73',
                      ''
                    ]
                  },
                  {
                    'iLinea': 61,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '14486.66',
                      ''
                    ]
                  },
                  {
                    'iLinea': 62,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      '',
                      '175.87',
                      ''
                    ]
                  },
                  {
                    'iLinea': 63,
                    'cImp': [
                      '',
                      '',
                      '',
                      '',
                      'RP',
                      '',
                      '-355305.95'
                    ]
                  },
                  {
                    'iLinea': 64,
                    'cImp': [
                      '',
                      '',
                      'Totales:',
                      '',
                      '',
                      '355305.95',
                      '-355305.95'
                    ]
                  }
                ],
                'ttRepProcF': [
                  {
                    'iLinea': 65,
                    'cImp': [
                      'Total General:',
                      '369650.73',
                      '-369650.73'
                    ]
                  },
                  {
                    'iLinea': 65,
                    'cImp': [
                      'LINEA',
                      '',
                      ''
                    ]
                  },
                  {
                    'iLinea': 65,
                    'cImp': [
                      'Total General:',
                      '369650.73',
                      '-369650.73'
                    ]
                  }
                ]
              }
            ]
          },
          'dsMensajes': {}
        }
      }
    }
  }
  hideNavbar() {
    this.navbar = !this.navbar;
    this._nav.notifyOther({ option: 'hideNavbar', value: this.navbar });
  }
  clickBtn(e: string) {
    console.log(e);
  }
  ngOnDestroy(): void {
    this.loading = false;
  }
  endSession() {
    // this.router.navigate(['login']);
    this._info.removeLocalStorage();
=======
import { Component, OnInit, OnDestroy, HostListener, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { InfociaGlobalService } from '../../services/infocia.service';
import { GLOBAL } from '../../services/global';

@Component({
  selector: 'app-pms-sistemas',
  templateUrl: './pms-sistemas.component.html'
})
export class PmsSistemasComponent implements OnInit, OnDestroy {
  public sistemas: any;
  public loading: boolean = true;
  elem: any;

  @HostListener('window:beforeunload', ['$event'])
  handleClose($event) {
    $event.returnValue = false;
  }
  @HostListener('window:unload', ['$event'])
  doUnload() {
    localStorage.clear();
    return this._http.post(GLOBAL.iplogout, { logout: '' }).subscribe();
  }
  constructor(private _info: InfociaGlobalService,
    private _router: Router,
    private _http: HttpClient,
    @Inject(DOCUMENT) private document: any) {
    this.elem = document.documentElement;

  }

  ngOnInit() {

    this._info.getUserCia().subscribe(data => {
      const ttUsuario = data.ttUsuarios[0];
      const userSys = ttUsuario.cVal32.split('®');
      if (ttUsuario.cPrograma) {
        const programa = ttUsuario.cPrograma.split('®');
        if (programa.length) {
          return this._router.navigate(['/' + programa[0].toLowerCase() + '/' + programa[1].toLowerCase()]);
        }
      } else {
        if (userSys.length === 1) {
          this._router.navigate(['/' + userSys[0].toLowerCase() + '/home']);
        }
      }
      this._info.getSistemas().subscribe(sistemas => {
        if (sistemas) {
          sistemas.filter(item => {
            for (const sistema of userSys) {
              if (item.cSistema === sistema) {
                item.lError = true;
                // if (userSys.length === 1) {
                //     this._router.navigate(['/' + item.cSistema.toLowerCase() + '/home']);
                // }
              }
            }
          });
          this.sistemas = sistemas;
        }
        this.loading = false;
      });
    });

  }

  ngOnDestroy(): void {
    this.loading = false;
  }

  endSession() {
>>>>>>> dev-alberto
    this._info.logoutProgress().subscribe();
  }
}
