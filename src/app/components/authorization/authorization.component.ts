<<<<<<< HEAD
import { Component, OnInit, EventEmitter, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-authorization', 
  template: `<kendo-dialog>
          <div style="text-align: center; margin: 30px;">
            <p class="text-muted">
              <ng-content></ng-content>
            </p>
            <h4>Usuario Autorizado</h4>
            <p>
              <input type="text" class="k-textbox" name="user" [(ngModel)]="userAuth.user" placeholder="Usuario" style="width: 300px;" />
            </p>
            <p>
              <input type="password" class="k-textbox" name="pass" [(ngModel)]="userAuth.pass"
                   placeholder="Password" style="width: 300px;" />
            </p>
            <p>
              <button kendoButton primary="true" style="width: 300px;" (click)="btnAuth()"
                      [disabled]="!userAuth.user || !userAuth.pass">Autorizar
              </button>
            </p>
            <button kendoButton (click)="closeAuth()">Cerrar</button>
          </div>
</kendo-dialog>`,
styles: [`
  kendo-dialog >>> .k-window-content{
    background: #212529 !important;
    color: white !important;`
  ]
=======
import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.html',
>>>>>>> dev-alberto
})
export class AuthorizationComponent implements OnInit {
  public userAuth: any = {};
  @Output() user: any = new EventEmitter;
  @Output() close: any = new EventEmitter;

  constructor() {
  }

  ngOnInit() {
    this.userAuth = ({
      user: this.userAuth.user,
      pass: this.userAuth.pass,
      valid: false
  })
  }
  btnAuth() {
    this.user.emit(this.userAuth);
  }
  closeAuth() {
    this.close.emit(true);
  }
}
