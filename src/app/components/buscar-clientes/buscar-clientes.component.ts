import { Component, OnInit, SimpleChanges, OnChanges, Output, Input, EventEmitter } from '@angular/core';
import { HistoricoClientesServices } from 'app/pms/consultas/historico-clientes/historico-clientes.service';

@Component({
  selector: 'app-buscar-clientes',
  templateUrl: './buscar-clientes.component.html',
  styleUrls: ['./buscar-clientes.component.css']
})
export class BuscarClientesComponent implements OnInit, OnChanges {
  @Output() result = new EventEmitter();
  @Input() send: string;
  public dataGrid: any = [];
  clienteSelected: any;
  apellido: string;
  nombre: string;
  contrato: string;
<<<<<<< HEAD
  loading: boolean;
  
  constructor(private _serv: HistoricoClientesServices) { }
=======
  loading = false;
  norecords = GLOBAL.noRecords;
  dataGrid: GridDataResult;
  pageSize = 10;
  skip = 0;
  private data: Object[];
  private items: any[] = [];

  constructor(private _serv: HistoricoClientesServices) {
  }
>>>>>>> dev-alberto

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['send'] && changes['send'].currentValue) {
        this.getCliente();
    }
  }
  private getCliente() {

  }
  // Enviamos el registro seleccionado al servicio
  selectedHuesp() {
      this.result.emit(this.clienteSelected);
  }
  selectedRow({ selectedRows }) {
    this.clienteSelected = selectedRows[0].dataItem;
  }
  // Cada que se pase por un input se limpiara el formulario
  onchange(value) {
    if (value !== 'apellido' && value !== 'nombre') {
      this.apellido = ''; this.nombre = ''; 
    }
    if (value !== 'contrato') { this.contrato = ''; }
    this.dataGrid = [];
  }
  searchButton(form?) {
    const val = form.value;
<<<<<<< HEAD
    if (val.apellido) { this.searchValue(val.apellido, 'nombre', val.nombre ); }
    if (val.nombre) { this.searchValue(val.apellido, 'nombre', val.nombre ); }
    if (val.contrato) { this.searchValue(val.contrato, 'contrato'); }
  }
  searchValue(value ?, field ?, value2 ?) {

    this._serv.getClientes(value, field, value2).subscribe(data => this.dataGrid = data);
=======
    let all = true;
    if (val.apellido) {
      this.searchValue(val.apellido, 'nombre', val.nombre);
      all = false;
    }
    if (val.nombre) {
      this.searchValue(val.apellido, 'nombre', val.nombre);
      all = false;
    }
    if (val.contrato) {
      this.searchValue(val.contrato, 'contrato', '');
      all = false;
    }

    if (all) {
      this.searchValue('*', '*', '*');
    }
  }


  searchValue(value ?, field ?, value2 ?) {
    this.loading = true;
    this._serv.getClientes(value, field, value2).subscribe(data => {
      if (data) {
        this.items = data;
        this.loadItems();
      }else {
        this.items = [];
      }
      this.loading = false;
    });
>>>>>>> dev-alberto
  }

}
