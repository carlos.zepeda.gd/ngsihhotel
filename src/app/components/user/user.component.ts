import { Component, OnInit, OnDestroy } from '@angular/core';
import { InfociaGlobalService } from '../../services/infocia.service';
import { GLOBAL } from '../../services/global';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  private ttInfocia: any;
  private ttUsuario: any;
  public userId: string;
  public hotel: string;
  public sysDate: string;
  public version: string;
  public hora: string;
  public activeReq = false;
  public intervalReq: any;

  constructor(public info: InfociaGlobalService, private router: Router) {}

  ngOnInit() {
      setTimeout(() => {
        this.ttUsuario = this.info.getSessionUser();
        if (this.ttUsuario) {
          this.userId = this.ttUsuario.cUserId;
          GLOBAL.userLogin = this.userId;
          this.updateInfocia();
        }
      }, 1000);


      // setInterval(() => this.updateInfocia(), 60 * 3000);
  }
  updateInfocia() {
    this.ttInfocia = this.info.getSessionInfocia();
    if (this.ttInfocia) {
        this.hotel = this.ttInfocia.cnombre;
        this.sysDate = this.ttInfocia.fday;
        this.version = this.ttInfocia.Version;
        this.hora = this.ttInfocia.chora;
        this.activeReq = this.ttInfocia.lreq;
    }
  }
  endSession() {
      // this.router.navigate(['login']);
      this.info.removeLocalStorage();
      this.info.logoutProgress().subscribe();
      clearInterval(this.intervalReq);
  }
}
