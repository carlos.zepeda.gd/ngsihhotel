import { Component, OnInit, Output, OnDestroy, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { NgForm } from '@angular/forms';
import { CheckinService, BuscarService } from '../../pms/recepcion/check-in/services/service-checkin.index';

@Component({
  selector: 'app-huespedes',
  templateUrl: './huespedes.component.html',
  styleUrls: ['./huespedes.component.css']
})
export class HuespedesComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  @Output() data = new EventEmitter
  public loading = false;
  public disabled = true;
  public checkOut = false;
  public nombre = '';
  public apellido = '';
  public resultUsers: any = [];
  public folioSelected: any;
  public noCuenta = 'D';
  constructor(public buscar: BuscarService, public _actServ: CheckinService ) { }

  ngOnInit(): void {
    this.subscription = this.buscar.notifyObservable$.subscribe((res) => {
      if (res.option === 'noCuenta') {
        this.noCuenta = res.value;
      }
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  searchHuesped(apellido?, nombre?) {
    this.loading = true;
    this.buscar.getHuesped(apellido, 'nombre', nombre).subscribe(data => {
      this.loading = false;
      if (data) {
        this.resultUsers = data;
      }
    });
  }
  refreshForm() {
    this.apellido = '';
    this.nombre = '';
    this.resultUsers = '';
  }
  selectRow({ selectedRows }) {
    this.disabled = false;
    this.folioSelected = selectedRows[0].dataItem;
    if (this.folioSelected.heactual === 'CO') { this.disabled = true; }
  }
  selectedHuesp() {
    this.data.emit(this.folioSelected);

    // localStorage.setItem('folioTransferir', this.folioSelected.hfolio);
    // const folioTransferir = [{
    //   cuenta: this.noCuenta,
    //   folio: this.folioSelected.hfolio
    // }];
    // this.buscar.notifyOther({ option: 'folioTransferir', value: folioTransferir });
    // this._actServ.notifyOther({ option: 'banderaBuscarFolio', value: false });
  }
}
