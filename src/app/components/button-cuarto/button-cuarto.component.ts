import { Component, OnInit, Input, SimpleChanges, OnChanges, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-button-cuarto',
  templateUrl: './button-cuarto.component.html',
  styleUrls: ['./button-cuarto.component.css']
})
export class ButtonCuartoComponent implements OnInit, OnChanges {
  @Input() button: string;
  @Output() rclick = new EventEmitter;
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['button']) {
      console.log(this.button);
    }
  }
  emitClick(e) {
    this.rclick.emit(e);
  }
}
