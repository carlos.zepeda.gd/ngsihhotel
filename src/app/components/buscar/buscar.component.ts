import { Component, OnChanges, ViewEncapsulation, Output, EventEmitter, Input, SimpleChanges } from '@angular/core';
import { SafeStyle, DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BuscarService } from '../../pms/recepcion/check-in/services/childs/buscar.service';
<<<<<<< HEAD
import swal from 'sweetalert2';
=======
import { GLOBAL } from '../../services/global';
import { distinct } from '@progress/kendo-data-query';
>>>>>>> dev-alberto

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./buscar.component.css']
})
export class BuscarComponent implements OnChanges {
  @Output() resultFolio = new EventEmitter();
  @Input() typeSearch: string;
  @Input() huesped: any;
  @Input() ttfolio: any = [];
  @Output() resultSearch = new EventEmitter();
  public activeUrl: string;
  public resultUsers: any = [];
  public loading: boolean;
<<<<<<< HEAD
  public selRow: Array<any> = [];
  public empty = true; 
  public apellido: string = ''; public nombre: string = ''; public contrato: string = '';
  public agencia: string = ''; public cuarto: string = ''; public folio: string = '';
  public conf: string = ''; public Grupo: string = ''; public empresa: string = ''; 

  constructor(
    private buscarServ: BuscarService,
    private router: Router,
    private sanitizer: DomSanitizer
  ) { }
=======
  public selectedRow: Array<any> = [];
  public empty = true;
  public apellido: string = '';
  public nombre: string = '';
  public contrato: string = '';
  public agencia: string = '';
  public cuarto: string = '';
  public folio: string = '';
  public conf: string = '';
  public Grupo: string = '';
  public empresa: string = '';
  public email: string = '';
  public cliente: string = '';
  swal = GLOBAL.swal;
  norecords = GLOBAL.noRecords;
  uso: string;
  consulta: boolean;

  constructor(private buscarServ: BuscarService,
    private sanitizer: DomSanitizer,
    private _active: ActivatedRoute) {
    this._active.data.subscribe(data => {
      this.consulta = data.consulta;
      this.uso = data.uso;
    });
  }
>>>>>>> dev-alberto

  ngOnChanges(changes: SimpleChanges) {
    if (changes['typeSearch'] !== undefined) {
      if (changes.typeSearch.currentValue) {
        if (this.typeSearch === 'R' || this.typeSearch === 'N' || this.typeSearch === 'F' || this.typeSearch === 'M') {
            this.activeUrl = 'reservacion';
        }
      }
    }
    if (changes['huesped'] !== undefined) {
      if (changes.huesped.currentValue) {
        if (changes.huesped.currentValue === '*') {
          this.searchValue('', 'nombre', '');
        }else {
          this.searchValue(this.huesped.hapellido, 'nombre', this.huesped.hnombre );
        }
      }
    }
    if (changes['ttfolio'] !== undefined) {
      if (changes.ttfolio.currentValue) {
        this.resultUsers = this.ttfolio;
      }
    }
  }
  // Esta función onSubmit evaluara los valores recibidos y los enviara al servicio.
  // la función termina si y solo si no se envia ninun valor del formulario.
  searchButton(form?) {
      const val = form.value;
      if (val.apellido) { this.searchValue(val.apellido, 'nombre', val.nombre ); }
      if (val.nombre) { this.searchValue(val.apellido, 'nombre', val.nombre ); }
      if (val.agencia) { this.searchValue(val.agencia, 'agencia'); }
      if (val.cuarto) { this.searchValue(val.cuarto, 'cuarto'); }
      if (val.folio) { this.searchValue(val.folio, 'folio'); }
      if (val.conf) { this.searchValue(val.conf, 'conf'); }
      if (val.Grupo) { this.searchValue(val.Grupo, 'Grupo'); }
      if (val.empresa) { this.searchValue(val.empresa, 'empresa'); }
      if (val.contrato) { this.searchValue(val.contrato, 'contrato'); }
  }
  // Cada que se pase por un input se limpiara el formulario
  onchange(value) {
    if (value !== 'apellido' && value !== 'nombre') {
      this.apellido = ''; this.nombre = ''; 
    }
    if (value !== 'agencia') { this.agencia = ''; }
    if (value !== 'cuarto') { this.cuarto = ''; }
    if (value !== 'folio') { this.folio = ''; }
    if (value !== 'conf') { this.conf = ''; }
    if (value !== 'Grupo') { this.Grupo = ''; }
    if (value !== 'empresa') { this.empresa = ''; }
    if (value !== 'agencia') { this.agencia = ''; }
    if (value !== 'contrato') { this.contrato = ''; }
    this.empty = true;
    this.resultUsers = '';
  }
<<<<<<< HEAD
// Por este metodo se envian todas las consultas al servicio
// Mediante resultUsers se actualiza el listado de huespedes en casa
  searchValue(value ?, field ?, value2 ?) {
      this.empty = true;
      this.loading = true;
      if ( this.activeUrl === 'reservacion') {
          this.buscarServ.getReservacion(value, field, value2).subscribe(data => {
            if (data) {
              this.loading = false;
              if (data[0].cMensTxt) {
                swal('Error!!!', data[0].cMensTxt, 'error');
                return;
              }
              this.resultUsers = data;
            }else {
              this.loading = false;
              this.resultUsers = [];
            }
          });
      }else {
          this.buscarServ.getHuesped(value, field, value2).subscribe(data => {
            if (data) {
              this.loading = false;
              if (data[0].cMensTxt) {
                swal('Error!!!', data[0].cMensTxt, 'error');
                return;
              }
              this.resultUsers = data;
              if (this.resultUsers) { this.resultSearch.emit(true); }
            }else {
              this.loading = false;
              this.resultUsers = [];
            }
          });
      }
=======

  // Por este metodo se envian todas las consultas al servicio
  // Mediante resultUsers se actualiza el listado de huespedes en casa
  searchValue(value?, field?, value2?) {
    if (this.loading) {
      return;
    }
    this.empty = true;
    this.loading = true;
    if (this.uso === 'R') {
      this.buscarServ.getReservacion(value, field, value2).subscribe(data => {
        this.loading = false;
        if (data) {
          if (data[0].cMensTxt) {
            this.swal(data[0].cMensTit, data[0].cMensTxt, 'error');
            return;
          }
          this.resultUsers = data;
        } else {
          this.resultUsers = [];
        }
      });
    } else {
      this.buscarServ.getHuesped(value, field, value2).subscribe(data => {
        this.loading = false;
        if (data) {
          if (data[0].cMensTxt) {
            return this.swal(data[0].cMensTit, data[0].cMensTxt, 'error');
          }
          this.resultUsers = data;
          this.resultSearch.emit(true);
        } else {
          this.resultUsers = [];
        }
      });
    }
>>>>>>> dev-alberto
  }
  // Funcion para obtener el registro seleccionado en la grid de Kendo
<<<<<<< HEAD
  selectedRow({ selectedRows }) {
    this.empty = false;
    this.selRow = selectedRows[0].dataItem;
=======
  selectRow({ selectedRows }) {
    if (selectedRows && selectedRows[0].dataItem) {
      this.empty = false;
      this.selectedRow = selectedRows[0].dataItem;
    }
>>>>>>> dev-alberto
  }
  // Enviamos el registro seleccionado al servicio
  selectedHuesp() {
    this.resultFolio.emit(this.selRow);
  }
  public colorCode(code: string): SafeStyle {
    let result;
    switch (code) {
     case 'CM' :
       result = '#f3cb0a';
       break;
     case 'MA' :
       result = '#f3cb0a';
       break;
     case 'CO' :
       result = '#dc3545';
       break;
     default:
       result = 'transparent';
       break;
    }
    return this.sanitizer.bypassSecurityTrustStyle(result);
  }

  distinctPrimitive(fieldName: string) {
    return distinct(this.resultUsers, fieldName).map(item => item[fieldName]);
  }


}
