import { RouterModule, Routes } from '@angular/router';
import { PmsSistemasComponent } from './components/pms-sistemas/pms-sistemas.component';
import { PmsComponent } from './pms/pms.component';
import { PosComponent } from './pos/pos.component';
import { VerifySessionService } from './services/verify-session';
<<<<<<< HEAD

const APP_ROUTES: Routes = [
        { path: 'home', component: PmsSistemasComponent, canActivate: [VerifySessionService] },
        { path: 'pos', component: PosComponent, loadChildren: './pos/pos.module#PosModule', canActivate: [VerifySessionService] },
        { path: 'pms', component: PmsComponent, loadChildren: './pms/pms.module#PmsModule', canActivate: [VerifySessionService] },
        { path: '**', pathMatch: 'full', redirectTo: 'home' }
=======
import { LoginLayoutComponent } from './_architect/Layout/login-layout/login-layout.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './services/auth-guard.service';

const APP_ROUTES: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'sistemas',
    component: LoginLayoutComponent,
    children: [
      {
        path: '',
        component: PmsSistemasComponent,
        canActivate: [
          VerifySessionService
        ]
      },
    ],

  },
  { path: 'pos', component: PosComponent, loadChildren: './pos/pos.module#PosModule', canActivate: [VerifySessionService] },
  { path: 'pms', component: PmsComponent, loadChildren: './pms/pms.module#PmsModule', canActivate: [VerifySessionService] },
  { path: '**', pathMatch: 'full', redirectTo: 'login' }
>>>>>>> dev-alberto
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: true });
