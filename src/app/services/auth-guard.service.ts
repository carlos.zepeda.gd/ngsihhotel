import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import swal from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        // if (!localStorage.getItem('globalInfocia') || !localStorage.getItem('globalUser')) {
        //     return true;
        // }
        if (localStorage.getItem('logout') === 'si') {
            localStorage.clear();
            return true;
        } else {
            const ttInfocia = JSON.parse(localStorage.getItem('globalInfocia'));
            // Si tiene una sesión no lo permito dirigirse al login
            if (route.routeConfig.path && route.routeConfig.path === 'login' && ttInfocia) {
                return false;
            } else {
                if (ttInfocia && ttInfocia.cNotifica) {
                    swal('Información importante!!', ttInfocia.cNotifica, 'warning');
                }
                return true;
                // // sino tiene una sesión hay que dirigirlo al login
                // localStorage.clear();
                // if (route.routeConfig.path && route.routeConfig.path !== 'login') {
                //     this.router.navigate(['/login']);
                //     return true;
                // }
            }
        }
    }
}
