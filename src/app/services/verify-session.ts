import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import 'rxjs/add/operator/map';
import { GLOBAL } from './global';
import { InfociaGlobalService } from './infocia.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/catch';

import { Observable } from 'rxjs';

interface ResInterface {
    'response': any
}

@Injectable()
export class VerifySessionService implements CanActivate {

    public url: string;

    constructor(private http: HttpClient,
        private _info: InfociaGlobalService,
        private router: Router) {
        this.url = GLOBAL.url;
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.http.get<ResInterface>(this.url + '/manttos/infocia')
            .map(res => {
                localStorage.clear();
                localStorage.setItem('globalInfocia', JSON.stringify(res.response.siInfocia.dsInfocia.ttInfocia[0]));
                localStorage.setItem('globalUser', JSON.stringify(res.response.siInfocia.dsInfocia.ttUsuarios[0]));

                const format = res.response.siInfocia.dsInfocia.ttInfocia[0].formatfecha.split('');
                let formatKendo = '';
                let formatMoment = '';
                for (let i = 0; i < format.length; i++) {
                    if (format[i] === 'y' || format[i] === 'Y') {
                        formatKendo += 'yyyy';
                        formatMoment += 'YYYY';
                    } else if (format[i] === 'm' || format[i] === 'M') {
                        formatKendo += 'MM';
                        formatMoment += 'MM';
                    } else if (format[i] === 'd' || format[i] === 'D') {
                        formatKendo += 'dd';
                        formatMoment += 'DD';
                    }

                    if (i !== format.length - 1) {
                        formatKendo += '-';
                        formatMoment += '-';
                    }

                    localStorage.setItem('formatKendo', formatKendo);
                    localStorage.setItem('formatMoment', formatMoment);
                }

                return true;
            }
            );
    }
}
