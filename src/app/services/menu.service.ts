import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';

@Injectable()
export class MenuPmsService {
  private sistema = new BehaviorSubject<any>([]);
  cuerrentSistema = this.sistema.asObservable();

  public url: string;

  constructor(private http: Http, private router: Router) {
    this.url = GLOBAL.url;
    this.sistema.next(this.router.url.split('/')[1].toUpperCase());
  }

  getMenu(sistema) {
    return this.http.get(this.url + '/admin/ayuda/' + environment.urlMenu + '/' + sistema + GLOBAL.char174 + GLOBAL.char174)
      .map(res => res.json().response.siMenuGral.dsMenuGral.ttMenuGral)
      .catch(e => {
        if (e.status === 401) {
          alert('No Autorizado!!... Sesión finalizada!!');
          return Observable.throw('Unauthorized');
        }
      });
  }

  getSubMenu(menu) {
    return this.http.get(this.url + '/admin/ayuda/' + environment.urlMenu + '/PMS' + GLOBAL.char174 + menu + GLOBAL.char174)
      .map(res => res.json().response.siMenuGral.dsMenuGral.ttMenuGral)
      .catch(e => {
        if (e.status === 401) {
          alert('No Autorizado!!... Sesión finalizada!!');
          return Observable.throw('Unauthorized');
        }
      });
  }

  getOpciones(menu, submenu) {
    return this.http.get(this.url + '/admin/ayuda/' + environment.urlMenu + '/PMS' + GLOBAL.char174 + menu + GLOBAL.char174 + submenu)
      .map(res => res.json().response.siMenuGral.dsMenuGral.ttMenuGral);
  }

  getEstadoCuartos() {
    return this.http.get(this.url + '/manttos/ctoedos')
      .map(res => res.json().response.siCtoedos.dsCtoedos.ttCtoedos);
  }
}
