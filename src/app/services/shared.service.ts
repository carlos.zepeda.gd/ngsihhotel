import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {GLOBAL} from './global';
import {OrderPipe} from 'ngx-order-pipe';
import {environment} from 'environments/environment';

const esp = GLOBAL.char174;
const url = GLOBAL.url;

@Injectable()
export class SharedService {
  public user: any;
  public infocia: any;
  public url: string;

  constructor(private http: Http, private _orderPipe: OrderPipe) {
  }

  getAyuda(param) {
    return this.http.get(url + '/ayuda/' + param).map(res => res.json().response.siAyuda.dsAyuda);
  }

  getCodigos(tipo, num, activos, codigo?) {
    // tipo = d(debito) /c (credito) // num = Orden segun mantto codigos
    // activos = Y / N // codigo ? un codigo en especifico
    codigo ? codigo = esp + codigo : codigo = '';
    return this.http.get(url + '/ayuda/codigos' + esp + tipo + esp + num + esp + activos + codigo)
      .map(res => res.json().response.siAyuda.dsAyuda.ttCodigos);
  }

  getMonedas() {
    return this.http.get(url + '/ayuda/monedas').map(res => res.json().response.siAyuda.dsAyuda.ttMonedas);
  }

  getTipoCambio() {
    return this.http.get(url + '/ayuda/tcambio').map(res => res.json().response.siAyuda.dsAyuda.ttTcambios);
  }

  getTipoCambioDia(moneda) {
    return this.http.get(url + '/ayuda/tcambio' + esp + moneda)
      .map(res => res.json().response.siAyuda.dsAyuda.ttTcambios);
  }

  getParamsBotones(menu) {
    return this.http.get(url + '/parametros/sistema').map(res => {
      const botones = res.json().response.siSistema.dsSistema.ttBotsistema;
      if (botones) {
        const asyncLoad = [];
        botones.forEach(item => {
          if (item.cMenu === menu) {
            if (item.lNotas) {
              asyncLoad.push('Notas');
            }
            if (item.lReqEsp) {
              asyncLoad.push('Requerimientos');
            }
            if (item.lCuentas) {
              asyncLoad.push('Cuentas');
            }
            if (item.lCargos) {
              asyncLoad.push('Cargos');
            }
            if (item.lBandas) {
              asyncLoad.push('Bandas');
            }
            if (item.lFallado) {
              asyncLoad.push('Fallado');
            }
            if (item.lCxC) {
              asyncLoad.push('C x C');
            }
            if (item.lInfAdicional) {
              asyncLoad.push('Adicional');
            }
            if (item.lLlaves) {
              asyncLoad.push('C x C');
            }
            if (item.lEntradaMovtos) {
              asyncLoad.push('Movimientos');
            }
          }
        });
        return asyncLoad;
      }
    });
  }

  getConvertirMontoTexto(monto, moneda, idioma) {
    return this.http.get(url + '/ayuda/numlet/' + monto + esp + moneda + esp + idioma).map(res => res.json().response);
  }

  getTipoCargos() {
    const dataTiposReq = [
      {tipoCargo: 'D', tcdesc: 'DIARIO'},
      {tipoCargo: 'E', tcdesc: 'ENTRADA'},
      {tipoCargo: 'S', tcdesc: 'SALIDA'},
      {tipoCargo: 'N', tcdesc: 'NINGUNO'},
    ];
    return dataTiposReq;
  }

  getCuentas() {
    const dataCuentas = [
      {tipoCuenta: 'A', tcuentadesc: 'A', field: 'htfola'},
      {tipoCuenta: 'B', tcuentadesc: 'B', field: 'htfolb'},
      {tipoCuenta: 'C', tcuentadesc: 'C', field: 'htfolc'},
      {tipoCuenta: 'D', tcuentadesc: 'D', field: 'htfold'},
      {tipoCuenta: 'E', tcuentadesc: 'E', field: 'htfole'},
      {tipoCuenta: 'F', tcuentadesc: 'F', field: 'htfolf'},
    ];
    return dataCuentas;
  }

  exportExcel(dataTable, nameFile) {
    this.http.post(this.url + 'excel', dataTable).subscribe((res: any) => {
      const linkSource = 'data:application/vnd.ms-excel;base64,' + res.base64;
      const download = document.createElement('a');
      // Concatenar extensión factura
      const fileName = nameFile + '.xlsx';
      download.href = linkSource;
      download.download = fileName;
      download.click();
      return 'ok';
    });
  }

  exportDataGrid(grid, allData, dataFilter) {
    let data;
    if (grid.filter && grid.filter.filters.length) {
      data = dataFilter;
    } else {
      if (grid.pageSize) {
        data = allData;
      } else {
        if (grid.data.data === undefined) {
          data = JSON.parse(JSON.stringify(grid.data));
        } else {
          data = JSON.parse(JSON.stringify(grid.data.data));
        }
      }
    }

    const keys = Object.keys(data[0]);
    grid.columns.map((column: any) => {
      if (column.field && column.isVisible) {
        keys.splice(keys.indexOf(column.field), 1);
      }
    });
    keys.forEach(key => data.forEach(item => delete item[key]));
    if (grid.pageSize) {
      grid.sort.forEach(item => {
        if (item.dir === 'desc') {
          data = this._orderPipe.transform(data, item.field, true);
        }
        if (item.dir === 'asc') {
          data = this._orderPipe.transform(data, item.field, false);
        }
      });
    }
    return data;
  }
}
