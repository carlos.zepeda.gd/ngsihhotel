<<<<<<< HEAD
import { Component, OnInit } from '@angular/core';
import { InfociaGlobalService } from './services/infocia.service';
import { GLOBAL } from './services/global';
=======
import { DOCUMENT } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { GLOBAL } from './services/global';
import { InfociaGlobalService } from './services/infocia.service';
>>>>>>> dev-alberto

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [InfociaGlobalService],
})
export class AppComponent implements OnInit {
<<<<<<< HEAD
  private infoCia: any;
  public formatoFecha: any;
  public displayDateFormat: any;
  public loading: boolean = true;
  constructor(private _infocia: InfociaGlobalService) {

=======
  // @HostListener('window:beforeunload', ['$event'])
  // handleClose($event) {
  //     $event.returnValue = 'Tus cambios';
  // }
  elem;
  public subscription: Subscription;
  flagLogout: boolean;
  hasNetworkConnection: boolean;
  hasInternetAccess: boolean;
  status: string;

  constructor(private _info: InfociaGlobalService,
    private _router: Router,
    private _http: HttpClient,
    @Inject(DOCUMENT) private document: any) {
>>>>>>> dev-alberto
  }
  ngOnInit() {
<<<<<<< HEAD
    this.infoCia = JSON.parse(localStorage.getItem('globalInfocia'));
    this._infocia.getUsuario().subscribe(data => localStorage.setItem('globalUser', JSON.stringify(data[0])));
    this._infocia.getInfocia().subscribe(data => {
      GLOBAL.fday = data[0].fday;
      localStorage.setItem('globalInfocia', JSON.stringify(data[0]));
    });
    this.loading = false;
    this.displayDateFormat = localStorage.getItem('globalFormatoFecha');
    if (!this.displayDateFormat) {
      this._infocia.formatoFechaGeneral().subscribe(data => {
        this.formatoFecha = data[0]['pchr1'];
        switch (this.formatoFecha) {
          case 'dmy':
          case 'DMY':
            this.displayDateFormat = 'dd-MM-yyyy';
            break;
          case 'mdy':
          case 'MDY':
            this.displayDateFormat = 'MM-dd-yyyy';
            break;
          case 'ymd':
          case 'YMD':
            this.displayDateFormat = 'yyyy-MM-dd';
            break;
          default:
            this.displayDateFormat = 'MM-dd-yyyy';
            break
        }
        localStorage.setItem('globalFormatoFecha', this.displayDateFormat);
      });
=======
    this.elem = document.documentElement;

  }
  doBeforeUnload() {
    if (localStorage.getItem('logout') !== 'si') {
      return false;
    }
    localStorage.clear();
  }

  doUnload() {
    if (localStorage.getItem('logout') !== 'si') {
      localStorage.clear();
      this._info.deleteTempRsrv().subscribe();
      return this._http.post(GLOBAL.iplogout, { logout: '' }).subscribe();
>>>>>>> dev-alberto
    }
  }

  openFullscreen() {
    if (!this.document.fullscreenElement) {
      if (this.elem.requestFullscreen) {
        this.elem.webkitRequestFullscreen();
      } else if (this.elem.mozRequestFullScreen) {
        /* Firefox */
        this.elem.mozRequestFullScreen();
      } else if (this.elem.webkitRequestFullscreen) {
        /* Chrome, Safari and Opera */
        this.elem.webkitRequestFullscreen();
      } else if (this.elem.msRequestFullscreen) {
        /* IE/Edge */
        this.elem.msRequestFullscreen();
      }
    }
  }
  /* Close fullscreen */
  closeFullscreen() {
    if (this.document.exitFullscreen) {
      this.document.exitFullscreen();
    } else if (this.document.mozCancelFullScreen) {
      /* Firefox */
      this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      this.document.msExitFullscreen();
    }
  }
}
