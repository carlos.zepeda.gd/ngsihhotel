import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
// Modulos Apps
import { KendouiModule } from './kendo.module';
import { ArchitectModule } from '../_architect/architect.module';
// Pipes
import { HourPipe } from '../pipes/hour.pipe';
import { StatusPipe } from '../pipes/status.pipe';
import { RoundPipe } from '../pipes/floor.pipe';
import { IconsPipe } from '../pipes/icons.pipe';
import { CreditCardMaskPipe } from '../pipes/creditcardmask.pipe';
import { CreditCardMask2Pipe } from '../pipes/creditcardmask2.pipe';
import { TipoHabitacionPipe } from '../pipes/tipo-habitacion.pipe';
import { AuthInterceptorService } from '../services/auth-interceptor.service';
// Directivas
import { AutofocusDirective } from '../directives/autofocus.directive';
// Components
import { RequerimientosComponent } from '../pms/recepcion/check-in/subcomponents/requerimientos/requerimientos.component';
import { AdicionalesComponent } from '../pms/recepcion/check-in/subcomponents/adicionales/adicionales.component';
import { MensajesComponent } from '../pms/recepcion/check-in/subcomponents/mensajes/mensajes.component';
import { CuentasComponent } from '../pms/recepcion/check-in/subcomponents/cuentas/cuentas.component';
import { CargosComponent } from '../pms/recepcion/check-in/subcomponents/cargos/cargos.component';
import { NotasComponent } from '../pms/recepcion/check-in/subcomponents/notas/notas.component';
import { AuthorizationComponent } from '../components/authorization/authorization.component';
import { LoadingComponent } from '../components/loading/loading.component';
import { SearchComponent } from '../components/search/search.component';
import { BuscarComponent } from '../components/buscar/buscar.component';
import { CxcComponent } from '../pms/recepcion/check-in/subcomponents/cxc/cxc.component';
import { TarifasComponent } from '../pms/recepcion/check-in/subcomponents/tarifas/tarifas.component';
import { HelpRfcComponent } from '../pms/recepcion/check-in/helps-components/help-rfc/help-rfc.component';
import { HelpTarifasComponent } from '../pms/recepcion/check-in/helps-components/help-tarifas/help-tarifas.component';
import { HelpEstadoComponent } from '../pms/recepcion/check-in/helps-components/help-estado/help-estado.component';
import { HuespedesComponent } from '../components/huespedes/huespedes.component';
import { BuscarClientesComponent } from '../components/buscar-clientes/buscar-clientes.component';
import { HelpVipComponent } from '../pms/recepcion/check-in/helps-components/help-vip/help-vip.component';
import { HelpAgenciaComponent } from '../pms/recepcion/check-in/helps-components/help-agencia/help-agencia.component';
import { HelpGrupoComponent } from '../pms/recepcion/check-in/helps-components/help-grupo/help-grupo.component';
import { HelpEstadComponent } from '../pms/recepcion/check-in/helps-components/help-estad/help-estad.component';
import { HelpEmpresaComponent } from '../pms/recepcion/check-in/helps-components/help-empresa/help-empresa.component';
import { HelpSegmentosComponent } from '../pms/recepcion/check-in/helps-components/help-segmentos/help-segmentos.component';
import { HelpTipoCuartosComponent } from '../pms/recepcion/check-in/helps-components/help-tipo-cuartos/help-tipo-cuartos.component';
import { MaestrasHuespedesComponent } from '../pms/recepcion/check-in/helps-components/maestras-huespedes/maestras-huespedes.component';
import { HistoricoClientesComponent } from '../pms/consultas/historico-clientes/historico-clientes.component';
import { DetalleClienteComponent } from '../pms/consultas/historico-clientes/detalle-cliente/detalle-cliente.component';
import { HelpAvailableComponent } from '../pms/recepcion/check-in/helps-components/help-available/help-available.component';
import { HelpCharacteristicsComponent } from '../pms/recepcion/check-in/helps-components/help-characteristics/help-characteristics.component';
import { HelpRatesComponent } from '../pms/recepcion/check-in/helps-components/help-rates/help-rates.component';
import { HelpDetailratesComponent } from '../pms/recepcion/check-in/helps-components/help-detailrates/help-detailrates.component';
import { SecuenciasComponent } from '../pms/reservaciones/reservacion/subcomponents/secuencias/secuencias.component';
import { HelpRoomComponent } from '../pms/recepcion/check-in/helps-components/help-room/help-room.component';
import { TipoRegistroComponent } from '../pms/reservaciones/reservacion/components/tipo-registro/tipo-registro.component';
import { ViajandoComponent } from '../pms/recepcion/check-in/subcomponents/viajando/viajando.component';
import { CompartiendoComponent } from '../pms/recepcion/check-in/subcomponents/compartiendo/compartiendo.component';
import { BandasComponent } from '../pms/recepcion/check-in/subcomponents/bandas/bandas.component';
import { FalladoComponent } from '../pms/recepcion/check-in/subcomponents/fallado/fallado.component';
import { HelpCondicionesCreditoComponent } from '../pms/recepcion/check-in/helps-components/help-condiciones-credito/help-condiciones-credito.component';
import { HelpClasificacionAgenciaComponent } from '../pms/recepcion/check-in/helps-components/help-clasificacion-agencia/help-clasificacion-agencia.component';
import { HelpCodigosCargoAbonoComponent } from '../pms/recepcion/check-in/helps-components/help-codigos-cargo-abono/help-codigos-cargo-abono.component';
import { HelpTipoMovimientosComponent } from '../pms/recepcion/check-in/helps-components/help-tipo-movimientos/help-tipo-movimientos.component';
import { HelpNCuartoComponent } from '../pms/recepcion/check-in/helps-components/help-ncuarto/help-ncuarto.component';
import { MsjsComputadorComponent } from '../pms/ama-llaves/msjs-computador/msjs-computador.component';
import {
  ValidationsService,
  TarifasService,
  HelpsCheckInService,
  RequerimientosService,
  CargosService,
  CuentasService,
  AdicionalesService,
  BandasService,
  FalladoService,
  CxcService,
  NotasService,
  MassagesService
} from '../pms/recepcion/check-in/services/service-checkin.index';
import { AutorizarCambiosComponent } from '../components/autorizar-cambios/autorizar-cambios.component';
import { HelpRequerimientosComponent } from '../pms/recepcion/check-in/helps-components/help-requerimientos/help-requerimientos.component';
import { AllCuartosComponent } from '../pms/recepcion/check-in/helps-components/all-cuartos/all-cuartos.component';
import { HelpBandasComponent } from '../pms/recepcion/check-in/helps-components/help-bandas/help-bandas.component';
import { HelpOrigenReservacionComponent } from '../pms/recepcion/check-in/helps-components/help-origen-reservacion/help-origen-reservacion.component';
import { BloqueoCuartosComponent } from '../pms/recepcion/bloqueo-cuartos/bloqueo-cuartos.component';
import { CambiosReservacionesComponent } from '../pms/consultas/cambios-reservaciones/cambios-reservaciones.component';
import { HelpSuplementosComponent } from '../pms/manttos/mantto-tarifas/help-suplementos/help-suplementos.component';
import { PerfilUserComponent } from '../components/perfil-user/perfil-user.component';
import { SignaturePadComponent } from '../components/signature-pad/signature-pad.component';
import { WebcamComponent } from 'app/components/webcam/webcam.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    KendouiModule,
    ArchitectModule
  ],
  declarations: [
    HistoricoClientesComponent,
    DetalleClienteComponent,
    RequerimientosComponent,
    MensajesComponent,
    NotasComponent,
    CargosComponent,
    CuentasComponent,
    SecuenciasComponent,
    SearchComponent,
    AutofocusDirective,
    AdicionalesComponent,
    HelpRfcComponent,
    HelpEstadoComponent,
    HelpVipComponent,
    HelpAgenciaComponent,
    HelpGrupoComponent,
    HelpEstadComponent,
    HelpEmpresaComponent,
    HelpSegmentosComponent,
    HelpTipoCuartosComponent,
    HelpRoomComponent,
    HelpAvailableComponent,
    HelpCharacteristicsComponent,
    HelpRatesComponent,
    HelpDetailratesComponent,
    LoadingComponent,
    HuespedesComponent,
    AuthorizationComponent,
    AutorizarCambiosComponent,
    BuscarComponent,
    HourPipe,
    StatusPipe,
    RoundPipe,
    IconsPipe,
    TipoHabitacionPipe,
    CreditCardMaskPipe,
    CreditCardMask2Pipe,
    CxcComponent,
    TarifasComponent,
    BuscarClientesComponent,
    MaestrasHuespedesComponent,
    SecuenciasComponent,
    TipoRegistroComponent,
    CompartiendoComponent,
    ViajandoComponent,
    BandasComponent,
    FalladoComponent,
    BloqueoCuartosComponent,
    HelpCondicionesCreditoComponent,
    HelpClasificacionAgenciaComponent,
    HelpCodigosCargoAbonoComponent,
    HelpTipoMovimientosComponent,
    HelpNCuartoComponent,
    MsjsComputadorComponent,
    HelpRequerimientosComponent,
    AllCuartosComponent,
    HelpBandasComponent,
    HelpOrigenReservacionComponent,
    HelpTarifasComponent,
    CambiosReservacionesComponent,
    HelpSuplementosComponent,
    PerfilUserComponent,
    SignaturePadComponent,
    WebcamComponent
  ],
  exports: [
    HistoricoClientesComponent,
    DetalleClienteComponent,
    RequerimientosComponent,
    MensajesComponent,
    NotasComponent,
    CargosComponent,
    CuentasComponent,
    SecuenciasComponent,
    SearchComponent,
    AutofocusDirective,
    AdicionalesComponent,
    HelpRfcComponent,
    HelpEstadoComponent,
    HelpVipComponent,
    HelpAgenciaComponent,
    HelpGrupoComponent,
    HelpEstadComponent,
    HelpEmpresaComponent,
    HelpSegmentosComponent,
    HelpTipoCuartosComponent,
    HelpRoomComponent,
    HelpAvailableComponent,
    HelpCharacteristicsComponent,
    HelpRatesComponent,
    HelpDetailratesComponent,
    LoadingComponent,
    HuespedesComponent,
    AuthorizationComponent,
    AutorizarCambiosComponent,
    BuscarComponent,
    HourPipe,
    StatusPipe,
    RoundPipe,
    CreditCardMaskPipe,
    TipoHabitacionPipe,
    IconsPipe,
    CxcComponent,
    TarifasComponent,
    BuscarClientesComponent,
    MaestrasHuespedesComponent,
    SecuenciasComponent,
    TipoRegistroComponent,
    CompartiendoComponent,
    ViajandoComponent,
    BandasComponent,
    FalladoComponent,
    BloqueoCuartosComponent,
    HelpCondicionesCreditoComponent,
    HelpClasificacionAgenciaComponent,
    HelpCodigosCargoAbonoComponent,
    HelpTipoMovimientosComponent,
    HelpNCuartoComponent,
    MsjsComputadorComponent,
    HelpRequerimientosComponent,
    AllCuartosComponent,
    HelpBandasComponent,
    HelpOrigenReservacionComponent,
    HelpTarifasComponent,
    CambiosReservacionesComponent,
    HelpSuplementosComponent,
    PerfilUserComponent,
    SignaturePadComponent,
    WebcamComponent
  ],
  providers: [
    ValidationsService,
    TarifasService,
    HelpsCheckInService,
    RequerimientosService,
    CargosService,
    CuentasService,
    AdicionalesService,
    BandasService,
    FalladoService,
    CxcService,
    NotasService,
    MassagesService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
  ]
})
export class SharedAppsModule {
}
