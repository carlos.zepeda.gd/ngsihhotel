// Modulos Angular
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
// Modulos Apps
import {KendouiModule} from './kendo.module';
// Components
import {PmsSistemasComponent} from '../components/pms-sistemas/pms-sistemas.component';
import {HomeComponent} from '../components/home/home.component';
import {PmsComponent} from '../pms/pms.component';
import {PosComponent} from '../pos/pos.component';
import {OrderModule} from 'ngx-order-pipe';
import {ArchitectModule} from '../_architect/architect.module';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthInterceptorService} from '../services/auth-interceptor.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    KendouiModule,
    OrderModule,
    ArchitectModule
  ],
  declarations: [
    PmsSistemasComponent,
    PmsComponent,
    PosComponent,
    HomeComponent,
  ],
  exports: [
    PmsSistemasComponent,
    PmsComponent,
    PosComponent,
    HomeComponent,
    ArchitectModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
  ]
})
export class SystemModule {
}
