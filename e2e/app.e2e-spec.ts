import { EstadoCuartosPage } from './app.po';

describe('estado-cuartos App', () => {
  let page: EstadoCuartosPage;

  beforeEach(() => {
    page = new EstadoCuartosPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
